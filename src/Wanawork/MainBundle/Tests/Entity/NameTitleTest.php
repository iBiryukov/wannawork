<?php
namespace Wanawork\MainBundle\Tests\Entity;

use Wanawork\UserBundle\Tests\Entity\WebUnitTestBase;
use Wanawork\MainBundle\Entity\NameTitle;

class NameTitleTest extends WebUnitTestBase
{
    
    private $validator;
    
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    
    protected function setUp()
    {
        $client = self::createClient();
        $container = $client->getContainer();
        
        $this->validator = $container->get('validator');
        $this->em = $container->get('doctrine')->getManager();
        
        $this->em->getConnection()->beginTransaction();
    }
    
    protected function tearDown()
    {
        $this->validator = null;
        $this->em->clear();
        $this->em->getConnection()->rollback();
        $this->em->getConnection()->close();
    }
    
    
    public function testEntityOk()
    {
        $nameTitle = new NameTitle($id = -1, $name = 'Test');
    
        $this->assertSame($id, $nameTitle->getId());
        $this->assertSame('Test', $nameTitle->getTitle());
        $this->assertSame($nameTitle->getTitle(), (string)$nameTitle);
    }
    
    public function testEmptyName()
    {
        $nameTitle = new NameTitle(-1, $name = '');
    
        $validator = $this->validator;
        $errors = $validator->validate($nameTitle);
    
        $this->assertCount(1, $errors);
        $this->assertSame('title', $errors[0]->getPropertyPath());
    }
    
    public function testNameIsTooLong()
    {
        $nameTitle = new NameTitle(-1, $name = str_repeat('a', 11));
    
        $validator = $this->validator;
        $errors = $validator->validate($nameTitle);
    
        $this->assertCount(1, $errors);
        $this->assertSame('title', $errors[0]->getPropertyPath());
    }
    
//     public function testNameIsCapitalised()
//     {
//         $nameTitle = new NameTitle(-1, $name = 'test');
//         $this->assertSame('Test', $nameTitle->getTitle());
//     }
    
}