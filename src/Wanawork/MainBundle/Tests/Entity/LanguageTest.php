<?php
namespace Wanawork\MainBundle\Tests\Entity;

use Wanawork\UserBundle\Tests\Entity\WebUnitTestBase;
use Wanawork\MainBundle\Entity\Language;

class LanguageTest extends WebUnitTestBase
{
    private $validator;
    
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    
    protected function setUp()
    {
        $client = self::createClient();
        $container = $client->getContainer();
        
        $this->validator = $container->get('validator');
        $this->em = $container->get('doctrine')->getManager();
        
        $this->em->getConnection()->beginTransaction();
    }
    
    protected function tearDown()
    {
        $this->validator = null;
        $this->em->clear();
        $this->em->getConnection()->rollback();
        $this->em->getConnection()->close();
    }
    
    public function testEntityOk()
    {
        $language = new Language($id = -1, $name = 'test');
    
        $this->assertSame($id, $language->getId());
        $this->assertSame('test', $language->getName());
        $this->assertSame($language->getName(), (string)$language);
    }
    
    public function testEmptyName()
    {
        $language = new Language(-1, $name = '');
    
        $validator = $this->validator;
        $errors = $validator->validate($language);
    
        $this->assertCount(1, $errors);
        $this->assertSame('name', $errors[0]->getPropertyPath());
    }
    
    public function testNameIsTooLong()
    {
        $language = new Language(-1, $name = str_repeat('a', 71));
    
        $validator = $this->validator;
        $errors = $validator->validate($language);
    
        $this->assertCount(1, $errors);
        $this->assertSame('name', $errors[0]->getPropertyPath());
    }
}