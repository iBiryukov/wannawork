<?php
namespace Wanawork\MainBundle\Tests\Command\Mock;

use Wanawork\MainBundle\Command\LockableCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class LockableCommandMock extends LockableCommand
{
    public function execute(InputInterface $input, OutputInterface $output)
    {
        // left blank intentionally
    }
}