<?php
namespace Wanawork\MainBundle\Tests\Command;

use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\StreamOutput;
class LockableCommandTest extends \PHPUnit_Framework_TestCase
{
    
    protected function tearDown()
    {
        $di = new \DirectoryIterator(sys_get_temp_dir());
        foreach($di as $file) {
            $fileName = $file->getFilename();
            
            if ($di->isFile() && substr($fileName, -strlen('.lock')) === '.lock') {
                unlink($file->getPathname());
            }
        }
        
    }
    
    public function testLockAndUnlock()
    {
        $mock = $this->getMockForAbstractClass("Wanawork\MainBundle\Command\LockableCommand", array(
        	'test_command'
        ));
        
        $this->assertTrue($mock->acquireLock());
        $this->assertTrue($mock->isLocked());
        
        $mock->releaseLock();
        $this->assertFalse($mock->isLocked());
    }
    
    public function testDoubleAcquireFails()
    {
        $mock = $this->getMockForAbstractClass("Wanawork\MainBundle\Command\LockableCommand", array(
            'test_command'
        ));
        
        $this->assertTrue($mock->acquireLock());
        $this->assertTrue($mock->isLocked());
        
        $this->assertFalse($mock->acquireLock());
        $this->assertTrue($mock->isLocked());
        
        $mock->releaseLock();
    }
    
    public function testAcquireLockOnTimedOutProcess()
    {
        $mock = $this->getMockForAbstractClass("Wanawork\MainBundle\Command\LockableCommand", array(
            'test_command'
            ),
            $mockClassName = '', $callOriginalConstructor = TRUE, $callOriginalClone = TRUE, $callAutoload = TRUE, 
            $mockedMethods = array('killProcess', 'releaseLock')
        );
        
        $mock->expects($this->once())
        ->method('killProcess');
        $mock->expects($this->exactly(2))
        ->method('releaseLock');
        
        $mock->setTimeout(1);
        $this->assertTrue($mock->acquireLock());
        $this->assertTrue($mock->isLocked());
        sleep(1);
        
        $this->assertFalse($mock->isLocked());
        $this->assertTrue($mock->acquireLock());
        $this->assertTrue($mock->isLocked());
        
        $mock->releaseLock();
    }
    
    
//     public function testExecuteCommand()
//     {
//         $mock = $this->getMock("Wanawork\MainBundle\Tests\Command\Mock\LockableCommandMock",
//             $mockedMethods = array('initialize', 'acquireLock',),
//             array('test_command')
//         );
        
//         $mock->expects($this->once())
//          ->method('initialize');
        
//         $mock->expects($this->once())
//          ->method('acquireLock');
        
//         $input = new ArrayInput(array());
//         $output = new StreamOutput(fopen('php://memory', 'w', false));
//         $mock->run($input, $output);
//     }
}