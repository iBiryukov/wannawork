<?php

namespace Wanawork\MainBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Wanawork\UserBundle\Entity\User;
use Symfony\Component\Routing\RouterInterface;

class DefaultControllerTest extends WebTestCase
{
    /**
     *
     * @var \Doctrine\ORM\EntityManager
     */
    private $doctrine;
    
    protected function setUp()
    {
        $client = self::createClient();
        $client->injectConnection();
        $this->doctrine = $client->getContainer()->get('doctrine')->getManager();
    }
    
    public function testIndex() 
    {
        $client = self::createClient();
        $crawler = $client->request('GET', '/');
        $container = $client->getContainer();
        $router = $container->get('router');
        
        
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        
//         $this->assertEquals(1, $crawler->filter('#wanawork_mainbudle_jobsearch_profession')->count());
//         $this->assertEquals(1, $crawler->filter('#wanawork_mainbudle_jobsearch_industries')->count());
        
//         $this->assertEquals(1, $crawler->filter('#wanawork_mainbudle_candidatesearch_profession')->count());
//         $this->assertEquals(1, $crawler->filter('#wanawork_mainbudle_candidatesearch_industries')->count());
	}
	
	public function testLoginWithLoggedInAdmin()
	{
	    $client = self::createClient();
	    $container = $client->getContainer();
	    $router = $container->get('router');
	    
	    $client->logout();
	    
	    $client->connect($container->getParameter('admin_email'), $container->getParameter('admin_password'));
	    
	    $crawler = $client->connect($container->getParameter('admin_email'), $container->getParameter('admin_password'));
	    $this->assertSame(302, $client->getResponse()->getStatusCode());
	    $this->assertTrue(
	        $client->getResponse()->isRedirect($router->generate('daily_details'))
        );
	}
	
	public function testLoginWithLoggedInEmployer()
	{
	    $client = self::createClient();
	    $container = $client->getContainer();
	    $router = $container->get('router');
	     
	    $client->logout();
	     
	    $client->connect($container->getParameter('employer_email'), $container->getParameter('employer_password'));
	     
	    $crawler = $client->connect($container->getParameter('employer_email'), $container->getParameter('employer_password'));
	    $this->assertSame(302, $client->getResponse()->getStatusCode());
	    $this->assertTrue(
	        $client->getResponse()->isRedirect($router->generate('wanawork_main_search'))
	    );
	}
	
	public function testLoginWithLoggedInEmployee()
	{
	    $client = self::createClient();
	    $container = $client->getContainer();
	    $router = $container->get('router');
	
	    $client->logout();
	
	    $client->connect($container->getParameter('candidate_email'), $container->getParameter('candidate_password'));
	
	    $crawler = $client->connect($container->getParameter('candidate_email'), $container->getParameter('candidate_password'));
	    $this->assertSame(302, $client->getResponse()->getStatusCode());
	    $this->assertTrue(
	        $client->getResponse()->isRedirect($router->generate('my_ads'))
	    );
	}
	
	public function testLoginWithUserThatHasNoRoles()
	{
	    $client = self::createClient();
	    $container = $client->getContainer();
	    $router = $container->get('router');
	    $doctrine = $this->doctrine;
	    
	    $client->logout();
	    
	    $user = new User();
	    $user->setName('Test Name');
	    $user->setEmail($email = "user-no-role@wannawork.ie");
	    $user->setPassword($password = '123456');
	    
	    $encoder = $container->get('security.encoder_factory')->getEncoder($user);
	    $encodedPassword = $encoder->encodePassword($user->getPassword(), $user->getSalt());
	    $user->setPassword($encodedPassword);
	    $doctrine->persist($user);
	    $doctrine->flush();
	    
	    
	    $client->connect($email, $password);
	    
	    $crawler = $client->connect($email, $password);
	    $this->assertSame(302, $client->getResponse()->getStatusCode());
	    $this->assertTrue(
	        $client->getResponse()->isRedirect($router->generate('wanawork_main_homepage'))
	    );
	}
	
	public function testLoginWithCandidate()
	{
	    $client = self::createClient();
	    $container = $client->getContainer();
	    $router = $container->get('router');
	    
	    $client->logout();
	    
	    $client->connect($container->getParameter('candidate_email'), $container->getParameter('candidate_password'));
	    
	    $this->assertSame(302, $client->getResponse()->getStatusCode());
	    $this->assertTrue(
	        $client->getResponse()->isRedirect(
	            $router->generate('wanawork_main_homepage', [], RouterInterface::ABSOLUTE_URL)
            ),
	        $client->getResponse()->headers->get('Location')
        );
	    
	    $client->followRedirect();
	    
	    $this->assertSame(302, $client->getResponse()->getStatusCode());
	    $this->assertTrue(
	        $client->getResponse()->isRedirect($router->generate('my_ads'))
	    );
	}
	
	public function testLoginWithEmployer()
	{
	    $client = self::createClient();
	    $container = $client->getContainer();
	    $router = $container->get('router');
	     
	    $client->logout();
	     
	    $client->connect($container->getParameter('employer_email'), $container->getParameter('employer_password'));
	     
	    $this->assertSame(302, $client->getResponse()->getStatusCode());
	    $this->assertTrue(
	        $client->getResponse()->isRedirect(
	            $router->generate('wanawork_main_homepage', [], RouterInterface::ABSOLUTE_URL)
	        ),
	        $client->getResponse()->headers->get('Location'));
	     
	    $client->followRedirect();
	     
	    $this->assertSame(302, $client->getResponse()->getStatusCode());
	    $this->assertTrue(
	        $client->getResponse()->isRedirect($router->generate('wanawork_main_search'))
	    );
	}
	
	public function testLoginWithAdmin()
	{
	    $client = self::createClient();
	    $container = $client->getContainer();
	    $router = $container->get('router');
	
	    $client->logout();
	
	    $client->connect($container->getParameter('admin_email'), $container->getParameter('admin_password'));
	
	    $this->assertSame(302, $client->getResponse()->getStatusCode());
	    $this->assertTrue(
	        $client->getResponse()->isRedirect(
	            $router->generate('wanawork_main_homepage', [], RouterInterface::ABSOLUTE_URL)
	        ),
	        $client->getResponse()->headers->get('Location'));
	
	    $client->followRedirect();
	
	    $this->assertSame(302, $client->getResponse()->getStatusCode());
	    $this->assertTrue(
	        $client->getResponse()->isRedirect($router->generate('daily_details'))
	    );
	}
	
	public function testLoginWithRedirectParameter()
	{
	    $client = self::createClient();
	    $container = $client->getContainer();
	    $router = $container->get('router');
	    
	    $redirectTo = $router->generate('wanawork_main_contactus', [], RouterInterface::ABSOLUTE_URL);
	    $crawler = $client->request('GET', $router->generate('login', ['login_redirect' => $redirectTo]));
	    
        $form = $crawler->selectButton('login-button')->form();
        $form['_username'] = $container->getParameter('candidate_email');
        $form['_password'] = $container->getParameter('candidate_password');
	    $client->submit($form);
	    
	    
	    $this->assertSame(302, $client->getResponse()->getStatusCode());
	    $this->assertTrue(
	        $client->getResponse()->isRedirect($redirectTo), 
	        $client->getResponse()->headers->get('Location')
	    );
	}
	
}
