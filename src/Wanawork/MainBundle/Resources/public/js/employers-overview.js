$(function(){
	
	var $quoteSlider = $('#testimonials').bxSlider({
		pager: false,
		controls: false,
		adaptiveHeight: true
	});
	
	$('#quote-scroll-left').on('click', function(e){
		e.preventDefault();
		$quoteSlider.goToNextSlide();
	});
	
	$('#quote-scroll-right').on('click', function(e){
		e.preventDefault();
		$quoteSlider.goToPrevSlide();
	});
	
});