$(function(){
	var isMobile = ( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) );
	
	var $homeBackgrounds = $('#home-backgrounds');
	
	if (!isMobile) {
		setInterval(function(){
			var $current = $homeBackgrounds.find('li:visible');
			var $next = $current.next();
			if ($next.length === 0) {
				$next = $homeBackgrounds.find('li').first();
			}
			
			$next.css('z-index', -1).show();
			$current.fadeTo(900, 0, function(){
				$next.css('z-index', 0);
				$current.fadeTo(0, 1).hide();
			});
			
		}, 10000);
	} else {
		$homeBackgrounds.remove();
		$('#section-home').css('background-image', "url('/bundles/wanaworkmain/images/index/backgrounds/bg-1.jpg')");
	}
	
	$('#full-page').fullpage({
		autoScrolling: !isMobile,
		verticalCentered: false,
		navigation: !isMobile,
        navigationPosition: 'right',
        navigationTooltips: ['Home', 'Features', 'Sign up'],
		afterLoad: function(anchor, index){
			if (index === 2) {
				$('#feature-list a.active').click();
			}
		},
		scrollingSpeed: 400
	});
	
	$('#feature-list a').click(function(e){
		e.preventDefault();
		$('.feature-slide').hide();
		var targetId = $(this).attr('href');
		$(targetId).css('display', 'table-cell');
		
		$('#feature-list a.active').removeClass('active');
		$(this).addClass('active');
	});
	
	var $activeSection = $('.section.active');
	if ($activeSection.attr('id') === 'section-features') {
		$('#feature-list a.active').click();
	}
	
	$('#footerLinks').hover(function(e){
		$(this).find('ul li ul').show();
	}, function(e){
		$(this).find('ul li ul').hide();
	});
	
	
	$('.signup-form').on('submit', function(e){
		var value = $(this).find('input[name="candidate[account]"]:checked').val();
		if (value === 'employer') {
			e.preventDefault();
			var name = $(this).find('input[name="candidate[name]"]').val();
			var email = $(this).find('input[name="candidate[email]"]').val();
			var password = $(this).find('input[name="candidate[password]"]').val();
			var $employerForm = $('#signup-employer');
			
			$('#employer_name').val(name);
			$('#employer_email').val(email);
			$('#employer_password').val(password);
			$employerForm.submit();
		} else {
			$(this).find('input[name="candidate[account]"]').remove();
			$(this).submit();
		}
		
	});
	
	$('#advert-link').on('click', function(e){
		e.preventDefault();
		$.fn.fullpage.moveSectionDown();
	});
	
	
//	var $headerWrapper = $('#header-wrapper');
//	var $mainWrapper = $('#index-wrapper');
//	$mainWrapper.height($(document.body).height() - $headerWrapper.height());
//	$(document).on('resize', function(){
//		$mainWrapper.height($(document.body).height() - $headerWrapper.height());
//	});
	
//	var $employerSlider = $('.employer-slides').bxSlider({
//		pager: false,
//		controls: false,
//		adaptiveHeight: false
//	});
//	
//	$('#employer-scroll-left').on('click', function(e){
//		e.preventDefault();
//		$employerSlider.goToNextSlide();
//	});
//	
//	$('#employer-scroll-right').on('click', function(e){
//		e.preventDefault();
//		$employerSlider.goToPrevSlide();
//	});
//	
//	var $quoteSlider = $('#testimonials').bxSlider({
//		pager: false,
//		controls: false,
//		adaptiveHeight: true
//	});
//	
//	$('#quote-scroll-left').on('click', function(e){
//		e.preventDefault();
//		$quoteSlider.goToNextSlide();
//	});
//	
//	$('#quote-scroll-right').on('click', function(e){
//		e.preventDefault();
//		$quoteSlider.goToPrevSlide();
//	});
//	
//	
//	$('#title-row h1 a').on('click', function(e){
//		e.preventDefault();
//		
//	});
//	
});