$(function(){
	var dateFormat = 'Y-m-d H:i';
	
	$('#start-date').datetimepicker({
		format: dateFormat
	});
	
	$('#finish-date').datetimepicker({
		format: dateFormat
	});
});