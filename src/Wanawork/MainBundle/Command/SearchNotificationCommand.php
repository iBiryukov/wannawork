<?php
namespace Wanawork\MainBundle\Command;

use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\HttpFoundation\Request;

class SearchNotificationCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('wanawork:search-notifications:update')
        ->setDescription('Process all pending search notifications');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $request = new Request();
        $this->getContainer()->enterScope('request');
        $this->getContainer()->set('request', $request, 'request');
        $context = $this->getContainer()->get('router')->getContext();
        $context->setHost($this->getContainer()->getParameter('router.request_context.host'));
        $context->setscheme($this->getContainer()->getParameter('router.request_context.scheme'));
        
        $searchService = $this->getContainer()->get('wanawork.search_service');
        $searchService->updateNotifications($output);
    }
}
