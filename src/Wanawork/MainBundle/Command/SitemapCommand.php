<?php
namespace Wanawork\MainBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;


class SitemapCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('wanawork:sitemap:generate')
        ->setDescription('Update the sitemap and submit it to search engines');
    }
    
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $request = new Request();
        $this->getContainer()->enterScope('request');
        $this->getContainer()->set('request', $request, 'request');
        $context = $this->getContainer()->get('router')->getContext();
        $context->setHost($this->getContainer()->getParameter('router.request_context.host'));
        $context->setscheme($this->getContainer()->getParameter('router.request_context.scheme'));
    
        $templatingService = $this->getContainer()->get('templating');
        $sitemapService = $this->getContainer()->get('wanawork.sitemap');

        $output->writeln("Generating the map");
        $map = $sitemapService->generateMap();
        
        $output->writeln("Rendering the map");
        $mapCode = $templatingService->render("WanaworkUserBundle:Sitemap:sitemap.xml.twig", array(
        	'sitemap' => $map,
        ));
        
        $sitemapLocation = $this->getContainer()->getParameter('sitemap.location');
        
        if (is_file($sitemapLocation) && is_readable($sitemapLocation) && 
            md5(file_get_contents($sitemapLocation)) === md5($mapCode)) {
            $output->writeln('The map is already up-to-date');
        } else {
            $output->writeln("Saving the map");
            file_put_contents($sitemapLocation, $mapCode);
            
            $output->writeln("Notifying search engines");
            
            if ($this->getContainer()->get('kernel')->getEnvironment() === 'prod') {
                $searchEngines = array(
                    'Google' => 'https://www.google.com/webmasters/tools/ping?sitemap=',
                    'Bing' => 'http://www.bing.com/ping?sitemap=',
                );
                
                $router = $this->getContainer()->get('router');
                $sitemapUrl = $router->generate('sitemap', array('_format' => 'xml'), RouterInterface::ABSOLUTE_URL);
                $output->writeln("Submitting sitemap: {$sitemapUrl}");
                
                $logger = $this->getContainer()->get('logger');
                foreach ($searchEngines as $name => $searchEngineUrl) {
                    $output->writeln("Sending request to {$name}");
                
                    $url = $searchEngineUrl . urlencode($sitemapUrl);
                    $ch = curl_init($url);
                
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_exec($ch);
                
                    $returnCode = null;
                    if (!curl_errno($ch)) {
                        $info = curl_getinfo($ch);
                        $returnCode = isset($info['http_code']) ? $info['http_code'] : null;
                    } else {
                        $output->write(curl_error($ch));
                        $logger->alert("Failed to submit sitemap", array(
                        	'search engine' => $name,
                            'error' => curl_error($ch),
                        ));
                    }
                
                    if ($returnCode === 200) {
                        $output->writeln("200 OK");
                    } else {
                        $output->writeln("Fail: {$returnCode}");
                        $logger->alert("Failed to submit sitemap", array(
                            'search engine' => $name,
                            'return code' => $returnCode,
                        ));
                    }
                    
                    curl_close($ch);
                }
            } else {
                $output->writeln("\t...Skipping for dev environment");   
            }
            
        }
    }
}