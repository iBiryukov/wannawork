<?php
namespace Wanawork\MainBundle\Command;

use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\HttpFoundation\Request;

class ReceiptGenerationCommand extends LockableCommand
{
    protected function configure()
    {
        $this->setName('wanawork:receipts:generate')
        ->setDescription('Generate PDF receipts for all finished and paid orders');
    }
    
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("Generating receipts");
        
        $request = new Request();
        $this->getContainer()->enterScope('request');
        $this->getContainer()->set('request', $request, 'request');
        $context = $this->getContainer()->get('router')->getContext();
        $context->setHost($this->getContainer()->getParameter('router.request_context.host'));
        $context->setscheme($this->getContainer()->getParameter('router.request_context.scheme'));
        
        $em = $this->getContainer()->get('doctrine')->getManager();
        $orders = $em->getRepository('WanaworkUserBundle:Billing\Order')->findOrdersPendingReceipts($limit = 50);

        $output->writeln(
	       sprintf("Found %d receipts", sizeof($orders))
        );
        
        $receiptService = $this->getContainer()->get('wanawork.receipt_service');
        $mailService = $this->getContainer()->get('wanawork.mail');
        
        foreach ($orders as $order) {
            $output->writeln(sprintf("Generating receipt for order id: '%d'", $order->getId()));
            $path = $receiptService->generateReceipt($order, $overwrite = false);
            
            if (!is_file($path)) {
                $output->writeln(
                    sprintf("Failed to generate receipt for order id: '%d'", $order->getId())
                );
            } else {
                $mailService->emailPaymentReceipt($order, $path);
            }
        }
        
        $output->writeln("Done");
    }
    
}