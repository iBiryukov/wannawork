<?php
namespace Wanawork\MainBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TwitterJobFairyCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('wanawork:twitter:jobfairy')
        ->setDescription('Process tweets from jobfairy');
    }
    
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $jobFairyService = $this->getContainer()->get('wanawork.job_fairy_service');
        $jobFairyService->processTweets($output);
    }
    
}