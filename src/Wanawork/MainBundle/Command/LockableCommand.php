<?php
namespace Wanawork\MainBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

abstract class LockableCommand extends ContainerAwareCommand
{
    
    /**
     * Path to the lock file to use
     * @var string
     */
    private $lockFile;
    
    /**
     * When to consider other instances of this process dead
     * @var integer
     */
    private $timeout = 600;
    
    public function acquireLock()
    {
        $lockAquired = true;
        $lockFile = $this->getLockFile();
        if (is_file($lockFile)) {
            
            $data = explode('|', file_get_contents($lockFile));
            $createTime = (int)$data[1];
            
            $timeout = $createTime + $this->getTimeout();
            if(time() < $timeout) {
                $lockAquired = false;
            } else {
                $processId = (int)rtrim(file_get_contents($lockFile), "\n");
                $this->killProcess($processId);
                $this->releaseLock();
            }
        }     

        if ($lockAquired) {
            $fp = fopen($lockFile, 'w');
            $data = $this->getProcessId() . '|' . time();
            fwrite($fp, $data);
            fclose($fp);
        }
        return $lockAquired;
    } 
    
    public function releaseLock()
    {
        if (file_exists($this->getLockFile())) {
            unlink($this->getLockFile());
        }
    }
    
    public function isLocked()
    {
        $lockFile = $this->getLockFile();
        
        if (!is_file($lockFile)) {
            return false;
        }
        
        $data = explode('|', file_get_contents($lockFile));
        $createTime = (int)$data[1];
        
        $timeout = $createTime + $this->getTimeout();
        return time() < $timeout;
    }
    
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        set_time_limit($this->getTimeout());

        if ($this->acquireLock()) {
            $lockFile = $this->getLockFile();
            
            declare(ticks = 1);
            pcntl_signal(SIGTERM, function() use($lockFile){
                unlink($lockFile);
                exit();
            });
            
            $that = $this;
            register_shutdown_function(function() use($that){
            	$that->releaseLock();
            });
            
        } else {
            $this->log("Another process is currently running");
            exit();
        }
    }
    
    public function getLockFile()
    {
        if ($this->lockFile === null) {
            $className = preg_replace('/[^a-zA-Z0-9]/', '_', get_class());
            $this->lockFile = sys_get_temp_dir() . DIRECTORY_SEPARATOR . $className . '.lock';
        }
        
        return $this->lockFile;
    }
    
    public function getTimeout()
    {
        if ($this->timeout === null) {
            $this->timeout = 600;
        }
        return $this->timeout;
    }
    
    public function setTimeout($timeout)
    {
        $this->timeout = $timeout;
    }
    
    private function getProcessId()
    {
        return posix_getpid();
    }
    
    protected function killProcess($pid)
    {
        if ($pid !== posix_getpid()) {
            posix_kill($pid, SIGTERM);
        }
    }
    
    protected function log($message)
    {
        echo date('d/M/Y h:i:s') . " $message \n";
    }
}