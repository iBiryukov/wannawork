<?php
namespace Wanawork\MainBundle\Security;

use JMS\SecurityExtraBundle\Security\Acl\Voter\AclVoter as BaseAclVoter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Doctrine\Common\Util\ClassUtils;

class AclVoter extends BaseAclVoter {
	public function vote(TokenInterface $token, $object, array $attributes) {
		// vote for object first
		$objectVote = parent::vote ( $token, $object, $attributes );
		
		if (self::ACCESS_GRANTED === $objectVote) {
			return self::ACCESS_GRANTED;
		} else {
			// then for object's class
			$oid = new ObjectIdentity ( 'class', ClassUtils::getRealClass ( get_class ( $object ) ) );
			$classVote = parent::vote ( $token, $oid, $attributes );
			
			if (self::ACCESS_ABSTAIN === $objectVote) {
				if (self::ACCESS_ABSTAIN === $classVote) {
					return self::ACCESS_ABSTAIN;
				} else {
					return $classVote;
				}
			} else if (self::ACCESS_DENIED === $objectVote) {
				if (self::ACCESS_ABSTAIN === $classVote) {
					return self::ACCESS_DENIED;
				} else {
					return $classVote;
				}
			}
		}
		
		return self::ACCESS_ABSTAIN;
	}
}