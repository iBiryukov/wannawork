<?php
namespace Wanawork\MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotNull;

class ContactUsType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->setAttribute('method', 'POST');
        
        $builder->add('name', 'text', array(
        	'constraints' => array(
        	    new NotBlank(array('message' => 'Please enter your name')),
            )
        ));
        $builder->add('email', 'email', array(
        	'constraints' => array(
        	    new NotBlank(array('message' => 'Please enter your email')),
        	    new Email(array('message' => 'Please enter a valid email'))
            )
        ));
        $builder->add('phone', 'number', array(
        	'required' => false,
            'label' => 'Phone Number'
        ));
        $builder->add('message', 'textarea');
        $builder->add('submit', 'submit', array(
        	'label' => 'Send',
            'attr' => array(
        	     'class' => 'btn btn-primary',
            )
        ));
        
    }
    
    public function getName()
    {
        return 'wanawork_main_contactus';
    }
}