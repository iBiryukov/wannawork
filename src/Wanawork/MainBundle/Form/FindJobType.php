<?php
namespace Wanawork\MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Wanawork\UserBundle\Entity\SectorJob;
use Wanawork\UserBundle\Entity\Sector;
use Doctrine\ORM\EntityRepository;
use Wanawork\UserBundle\Repository\SectorJobRepository;
use Wanawork\UserBundle\Repository\SectorRepository;

/**
 * Form that allows candidates to find a job
 * 
 * @author Ilya Biryukov <ilya@wanawork.ie>
 */
class FindJobType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $builder
            ->add('profession', 'entity', array(
                'class' => 'WanaworkUserBundle:SectorJob',
                'query_builder' => function(SectorJobRepository $er){
                    $qb = $er->createQueryBuilder('profession');
                    $qb->addOrderBy('profession.name', 'ASC');
                    return $qb;
                },
                'group_by' => 'sectorName',
                'attr' => array(
                    'data-placeholder' => 'Profession'
                ),
                'empty_value' => 'Profession'
            ))
            ->add('industries', 'entity', array(
                'class' => 'WanaworkUserBundle:Sector',
                'property' => 'name',
                'query_builder' => function(SectorRepository $rep) {
                    return $rep->createQueryBuilder('industry')->orderBy('industry.name');
                },
                'multiple' => true,
                'attr' => array(
                    'data-placeholder' => 'Industries'
                ),
                'required' => false,
            ));
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Wanawork\MainBundle\Entity\blanks\JobSearch'
        ));
    }
    
    public function getName()
    {
        return 'wanawork_mainbudle_jobsearch';
    }

}
