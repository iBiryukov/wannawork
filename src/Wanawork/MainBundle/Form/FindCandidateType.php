<?php
namespace Wanawork\MainBundle\Form;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Wanawork\UserBundle\Repository\SectorJobRepository;
use Wanawork\UserBundle\Repository\SectorRepository;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Form that allows employers to find candidates
 * @author Ilya Biryukov <ilya@wanawork.ie>
 */
class FindCandidateType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{

		$builder
		->add('profession', 'entity', array(
				'class' => 'WanaworkUserBundle:SectorJob',
				'query_builder' => function(SectorJobRepository $er) {
					$qb = $er->createQueryBuilder('profession');
					$qb->addOrderBy('profession.name', 'ASC');
					return $qb;
				},
				'attr' => array(
				    'data-placeholder' => 'Profession'
				),
				'empty_value' => 'Profession'
		))
		->add('industries', 'entity', array(
				'class' => 'WanaworkUserBundle:Sector',
				'property' => 'name',
				'query_builder' => function(SectorRepository $rep) {
					return $rep->createQueryBuilder('industry')->orderBy('industry.name');
				},
				'multiple' => true,
				'attr' => array(
				    'data-placeholder' => 'Industries'
				),
				'required' => false,
		));
	}
	
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
				'data_class' => 'Wanawork\MainBundle\Entity\blanks\CandidateSearch'
		));
	}
	
	public function getName()
	{
		return 'wanawork_mainbudle_candidatesearch';
	}
}
