<?php
namespace Wanawork\MainBundle\Scripts;


class DependencyChecker
{
    public static function runChecks()
    {
        $gsPath = trim(`which gs`);
                
        if (!is_file($gsPath)) {
            throw new \Exception("Unable to find gs command. Run 'sudo apt-get install ghostscript'");
        }
        
        if (!is_executable($gsPath)) {
            throw new \Exception("Execute rights are not give for ghostscript at path '$gsPath'");
        }
        
        $pathToBin = realpath(__DIR__ . '/../../../../app/Resources/bin');
        if (!is_dir($pathToBin)) {
            throw new \Exception('Cannot find the bin directory in app/Resources/bin');
        }
        
        $gsSymlinkPath = $pathToBin . '/gs';
        if ((is_link($gsSymlinkPath) || file_exists($gsSymlinkPath)) && !unlink($gsSymlinkPath)) {
            throw new \Exception("Cannot remove the old gs symlink in {$gsSymlinkPath}");
        }
        
        `ln -s $gsPath $gsSymlinkPath`;
        
        if (!is_link($gsSymlinkPath)) {
            throw new \Exception("Unable to create gs symlink in {$gsSymlinkPath}");
        }
        
    }    
}