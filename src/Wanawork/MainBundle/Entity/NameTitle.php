<?php
namespace Wanawork\MainBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Name Titles
 * @author Ilya Biryukov <ilya@goparty.ie>
 * @ORM\Entity(repositoryClass="Wanawork\MainBundle\Repository\NameTitleRepository")
 * @ORM\ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 */
class NameTitle
{
    const TITLE_MR     =   1;
    const TITLE_MRS    =   2;
    const TITLE_MISS   =   3;
    const TITLE_MS     =   4;
    const TITLE_DOCTOR =   5;
    
    static $validStatuses = array(
    	'mr'    => self::TITLE_MR,
        'mrs'   => self::TITLE_MRS,
        'miss'  => self::TITLE_MISS,
        'ms'    => self::TITLE_MS,
        'doctor'=> self::TITLE_DOCTOR,
    );
	
	/**
	 * Id 
	 * @var integer
	 * @ORM\Column(type="smallint")
	 * @ORM\Id
	 */
	protected $id;

	/**
	 * The title
	 * @var string 
	 * @ORM\Column(type="string", length=10)
	 * @Assert\NotBlank()
	 * @Assert\Length(max=10, maxMessage="Name Title cannot be longer than {{ limit }} characters")
	 */
	protected $title;
	
	public function __construct($id, $title) 
	{
		$this->setId($id);
		$this->setTitle($title);
	}
	
	public function setId($id)
	{
	    $this->id = $id;
	}
	
	public function setTitle($title)
	{
	    $this->title = $title;
	}
	
	public function getId()
	{
	    return $this->id;
	}
	
	public function getTitle() 
	{
		return $this->title;
	}
	
	public function __toString()
	{
		return $this->getTitle();
	}
	
	public function __toArray()
	{
	    return array(
	    	'id' => $this->getId(),
	        'title' => $this->getTitle(),
	    );
	}
}

