<?php
namespace Wanawork\MainBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Irish County
 * 
 * @author Ilya Biryukov <ilya@goparty.ie>
 * @ORM\Entity(repositoryClass="Wanawork\MainBundle\Repository\CountyRepository")
 * @ORM\ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 */
class County
{
    /**
     * County id
     * 
     * @var integer 
     * @ORM\Column(type="integer")
     * @ORM\Id
     */
    protected $id;

    /**
     * County name
     * 
     * @var string 
     * @ORM\Column(type="string", length=70)
     * @Assert\NotBlank(message="County name is empty")
     * @Assert\Length(max=70, maxMessage="County name cannot be longer than {{ limit }} characters")
     */
    protected $name;

    public function __construct($id, $name)
    {
        $this->setName($name);
        $this->setId($id);
    }
    
    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }
    
    public function setName($name)
    {
        $this->name = $name;
    }

    public function __toString()
    {
        return $this->getName();
    }
    
    public function __toArray()
    {
        return array(
        	'id' => $this->getId(),
            'name' => $this->getName(),
        );
    }
}

