<?php
namespace Wanawork\MainBundle\Entity\blanks;
use Symfony\Component\Validator\Constraints as Assert;
use Wanawork\UserBundle\Entity\SectorJob;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Represents a search for a job done by a user from the front page
 * @author Ilya Biryukov <ilya@wanawork.ie>
 */
class JobSearch
{
    /**
     * Selected profession
     * @var Wanawork\UserBundle\Entity\SectorJob
     * @Assert\NotNull()
     * @Assert\Valid()
     */
    protected $profession;

    /**
     * List of selected industries
     * 0 or ome
     * @var Doctrine\Common\Collections\ArrayCollection
     */
    protected $industries;

//     public function __construct(SectorJob $profession, array $industries = array())
//     {
//         $this->profession = $profession;
//         $this->industries = new ArrayCollection($industries);
//     }

    public function getProfession()
    {
        return $this->profession;
    }

    public function setProfession(SectorJob $profession)
    {
        $this->profession = $profession;
    }

    public function getIndustries()
    {
        return $this->industries;
    }

    public function setIndustries($industries)
    {
        $this->industries = $industries;
    }
}
