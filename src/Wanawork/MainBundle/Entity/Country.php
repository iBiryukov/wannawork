<?php
namespace Wanawork\MainBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Country entity
 * @author Ilya Biryukov <ilya@goparty.ie>
 * @ORM\Entity(repositoryClass="Wanawork\MainBundle\Repository\CountryRepository")
 * @ORM\ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 */
class Country {
	
	/**
	* County id
	* @var integer
	* @ORM\Column(type="integer")
	* @ORM\Id
	*/
	protected $id;
	
	/**
	 * Country name
	 * @var string
	 *
	 * @ORM\Column(type="string", length=70)
	 * @Assert\NotBlank(message="Country name is empty")
	 */
	protected $name;
	
	/**
	 * Short 2 character code
	 * @var string 
	 * @ORM\Column(type="string", length=2)
	 * @Assert\NotBlank(message="Country code cannot be empty")
	 * @Assert\Length(min=2, max=2, 
	 * 					minMessage="Country code needs to be 2 characters long", 
	 * 					maxMessage="Country code needs to be 2 characters long")
	 */
	protected $code;
	
	public function __construct($id, $name, $code)
	{
	    $this->setId($id);
		$this->setName($name);
		$this->setCode($code);
	}
	
	public function setId($id)
	{
	    $this->id = $id;
	}
	
	/**
	 * @return the $id
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @return the $name
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @return the $code
	 */
	public function getCode()
	{
		return $this->code;
	}
	
	public function __toString()
	{
		return $this->getName();
	}
	
	public function __toArray()
	{
	    return array(
	    	'id' => $this->getId(),
	        'name' => $this->getName(),
	        'code' => $this->getCode(),
	    );
	}

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }
}

