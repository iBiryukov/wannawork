<?php
namespace Wanawork\MainBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Wanawork\MainBundle\Entity\County;
use Wanawork\MainBundle\Form\CountyType;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * County controller.
 *
 * @Route("/entity/county")
 */
class CountyController extends Controller
{
    /**
     * @Route("/export", name="counry_export")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("GET")
     */
    public function exportAction()
    {
        $trader = $this->get('wanawork.country_trader');
        $data = $trader->export();
    
        $file = sys_get_temp_dir() . '/' . uniqid();
        $fp = fopen($file, 'w');
        fwrite($fp, prettyPrint(json_encode($data)));
        fclose($fp);
    
        $response = new BinaryFileResponse(
            $file, $status = 200, $headers = array(), $public = false,
            $contentDisposition = null, $autoEtag = false, $autoLastModified = true
        );
        $response->headers->set('Content-Type', 'application/json');
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, "county".date('Y_m_d_H_i').".json");
        return $response;
    }

    /**
     * Lists all County entities.
     *
     * @Route("/", name="entity_county")
     * @Method("GET")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('WanaworkMainBundle:County')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new County entity.
     *
     * @Route("/", name="entity_county_create")
     * @Method("POST")
     * @Template("WanaworkMainBundle:County:new.html.twig")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function createAction(Request $request)
    {
        $entity = new County();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('entity_county_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a County entity.
    *
    * @param County $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(County $entity)
    {
        $form = $this->createForm(new CountyType(), $entity, array(
            'action' => $this->generateUrl('entity_county_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new County entity.
     *
     * @Route("/new", name="entity_county_new")
     * @Method("GET")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function newAction()
    {
        $id = $this->getDoctrine()->getRepository('WanaworkMainBundle:County')->findNextId();
        $entity = new County($id);
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a County entity.
     *
     * @Route("/{id}", name="entity_county_show")
     * @Method("GET")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WanaworkMainBundle:County')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find County entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing County entity.
     *
     * @Route("/{id}/edit", name="entity_county_edit")
     * @Method("GET")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WanaworkMainBundle:County')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find County entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a County entity.
    *
    * @param County $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(County $entity)
    {
        $form = $this->createForm(new CountyType(), $entity, array(
            'action' => $this->generateUrl('entity_county_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing County entity.
     *
     * @Route("/{id}", name="entity_county_update")
     * @Method("PUT")
     * @Template("WanaworkMainBundle:County:edit.html.twig")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WanaworkMainBundle:County')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find County entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->persist($editForm->getData());
            $em->flush();

            return $this->redirect($this->generateUrl('entity_county_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a County entity.
     *
     * @Route("/{id}", name="entity_county_delete")
     * @Method("DELETE")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('WanaworkMainBundle:County')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find County entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('entity_county'));
    }

    /**
     * Creates a form to delete a County entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('entity_county_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
