<?php
namespace Wanawork\MainBundle\Controller;

use Wanawork\UserBundle\Form\SearchNotificationType;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Wanawork\MainBundle\Security\AclManager;
use Wanawork\UserBundle\Entity\Search;
use Wanawork\UserBundle\Form\SearchType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;
use Wanawork\UserBundle\Entity\SectorJob;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;

class SearchController extends Controller
{
    
    /**
     * Search action
     * @param string $searchId
     * @Route("/search/{searchId}", name="wanawork_main_search", defaults={"searchId"=0})
     * @Template()
     */
    public function indexAction($searchId)
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();
        
        $security = $this->get('security.context');
        $user = $this->getUser();
        $isAdmin = ($security->isGranted('ROLE_ADMIN', $user));
        
        $searchCandidateForm = $request->query->get('wanawork_mainbudle_candidatesearch', array());
        $professionId = isset($searchCandidateForm['profession']) ? $searchCandidateForm['profession'] : null; 
        if (preg_match('/^\d+$/', $professionId)) {
            $profession = $em->getRepository('WanaworkUserBundle:SectorJob')->find($professionId);
            if($profession instanceof SectorJob) {
                
                $searchEntity = new Search();
                $searchEntity->setProfession($profession);
                
                if(!is_null($user)) {
                    $searchEntity->setUser($user);
                }
                
                if (isset($searchCandidateForm['industries']) && sizeof($searchCandidateForm['industries'])) {
                    $industriesRepository = $em->getRepository('WanaworkUserBundle:Sector');
                    $industriesId = array_filter($searchCandidateForm['industries'], function($industryId){
                    	return preg_match('/^\d+$/', $industryId);
                    });
                    $industries = $industriesRepository->findById($industriesId);
                    $searchEntity->setIndustries($industries);
                }
                
                $em->persist($searchEntity);
                $em->flush();
                
                if(!is_null($user)) {
                    $aclManager = new AclManager($this->get('security.acl.provider'), $this->get('security.context'));
                    $aclManager->grant($searchEntity, MaskBuilder::MASK_OPERATOR);
                }
                return $this->redirect($this->generateUrl('wanawork_main_search', array('searchId' => $searchEntity->getId())));
            }
            
        }
        
        $searchEntity = $em->getRepository('WanaworkUserBundle:Search')->find($searchId);
        $form = $this->createForm(new SearchType($isAdmin), $searchEntity);
        
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            
            if($form->isValid()) {
				$searchEntity = $form->getData();
				
				if(!is_null($user)) {
					$searchEntity->setUser($user);
				}				
				
				$em->persist($searchEntity);
				$em->flush();
				
				if(!is_null($user)) {
					$aclManager = new AclManager($this->get('security.acl.provider'), $this->get('security.context'));
					$aclManager->grant($searchEntity, MaskBuilder::MASK_OPERATOR);
				}
				return $this->redirect($this->generateUrl('wanawork_main_search', array('searchId' => $searchEntity->getId())));
			}
        }
        
        $viewOptions = array(
            'form' => $form->createView(),
            'pagination' => array(),
            'search' => null,
        );
        
        if($searchEntity instanceof Search) {
            $pageNumber = $request->query->get('page', 1);
            
            $paginator = $this->get('knp_paginator');
            $searchService = $this->get('wanawork.search_service');
            $pagination = $searchService->findAds($searchEntity, $paginator, $pageNumber);
            
            $viewOptions['pagination'] = $pagination;
            $viewOptions['search'] = $searchEntity;
            
            if (!$searchEntity->hasNotifier() && (!$searchEntity->getUser() || $security->isGranted("EDIT", $searchEntity))) {
                $bookmarkForm = $this->createForm(new SearchNotificationType());
                $viewOptions['bookmarkForm'] = $bookmarkForm->createView();
            }
        }
        
        
        return $this->render('WanaworkMainBundle:Search:index.html.twig', $viewOptions);
    }
    
	private function createDeleteForm($id)
	{
		return $this->createFormBuilder(array('id' => $id))
		->add('id', 'hidden')
		->getForm()
		;
	}
	
	/**
	 * @Route("/admin/searches/stats", name="search_stats")
	 * @Template()
	 * @Secure(roles="ROLE_ADMIN")
	 */
	public function statsAction()
	{
	    return array(
	    	
	    );
	}
	
	/**
	 * @Route("/admin/searches/list", name="search_list")
	 * @Template()
	 * @Secure(roles="ROLE_ADMIN")
	 */
	public function listAction()
	{
	    $dql = "SELECT s
				FROM WanaworkUserBundle:Search s
				ORDER BY s.date DESC";
	    
	    $query = $this->getDoctrine()->getManager()->createQuery($dql);
	    
	    $perPage = 10;
	    $pageNumber = $this->get('request')->query->get('page', 1);
	    
	    $paginator = $this->get('knp_paginator');
	    $pagination = $paginator->paginate($query, $pageNumber, $perPage);
	    
	    return array(
	        'pagination' => $pagination,
	    );
	} 
    
    /**
     * @Route("/admin/searches/statistics", name="search_statistics")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function statisticsAction()
    {
        $dateRegex = '/^2\d{3}\-\d{2}\-\d{2}\s\d{2}:\d{2}$/';
        $allowedGroupings = array('day', 'month');
        
        $request = $this->getRequest();
        $startDate = $request->query->get('start-date');
        $finishDate = $request->query->get('finish-date');
        $canReset = false;
        
        
        if ($startDate === null || !preg_match($dateRegex, $startDate) || strtotime($startDate) === false) {
            $startDate = date('Y-m-01 00:00');
        }
        
        if ($finishDate === null || !preg_match($dateRegex, $finishDate) || strtotime($finishDate) === false) {
            $finishDate = date('Y-m-d H:i');
        }
        
    	$em = $this->getDoctrine()->getManager();
    	$repos = $em->getRepository('Wanawork\UserBundle\Entity\Search');
    	
    	$professionsByCount = $repos->findMostUsedProfessions($startDate,$finishDate);
    	$industryByCount = $repos->findMostUsedIndustires($startDate,$finishDate);
    	$languageByCount = $repos->findMostUsedLanguage($startDate,$finishDate);
    	$locationByCount = $repos->findMostUsedLocation($startDate,$finishDate);
    	$educationLevelByCount = $repos->findMostUsedEducationLevel($startDate,$finishDate);
    	$experienceByCount = $repos->findMostUsedExperience($startDate,$finishDate);
    	$rolesByCount = $repos->findMostUsedRole($startDate,$finishDate);
    	
    	return array(
        	'start_date'     => $startDate,
            'finish_date'    => $finishDate,
    	    'professionsData' => $professionsByCount,
    	    'industryData' => $industryByCount,
    	    'languageData' => $languageByCount,
    	    'locationData' => $locationByCount,
    	    'educationData' => $educationLevelByCount,
    	    'experienceData' => $experienceByCount,
    	    'rolesData' => $rolesByCount,
    	);
    }
}

