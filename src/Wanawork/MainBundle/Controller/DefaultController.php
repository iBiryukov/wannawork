<?php
namespace Wanawork\MainBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Wanawork\MainBundle\Form\ContactUsType;
use Wanawork\UserBundle\Entity\User;
use Wanawork\UserBundle\Form\Type\EmployerType;
use Wanawork\UserBundle\Entity\Role;
use Wanawork\UserBundle\Form\Type\CandidateType;

class DefaultController extends Controller
{

	public function indexAction()
    {
        
        $user = $this->getUser();
        if ($user instanceof User && $this->decideHomePage()) {
    	    return $this->redirect($this->decideHomePage());
    	}
        
        
        $accountsService = $this->get('wanawork.accounts_service');
        $candidate = new User();
        $candidate->addRole($accountsService->getRoleForAccountType('employee'));
         
        $formEmployee = $this->createForm(new CandidateType(), $candidate);
        $formEmployer = $this->createForm(new EmployerType());
        
        return $this->render('WanaworkMainBundle:Default:index.html.twig', array(
            'form_employee' => $formEmployee->createView(),
            'form_employee2' => $formEmployee->createView(),
            'form_employer' => $formEmployer->createView(),
        ));
    }
    
    public function loginAction()
    {
    	$request = $this->getRequest();
    	$session = $request->getSession();
    	$user = $this->getUser();
    	
    	if ($user instanceof User && $this->decideHomePage()) {
    	    return $this->redirect($this->decideHomePage());
    	}
    	
    	// get the login error if there is one
    	if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
    		$error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
    	} else {
    		$error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
    		$session->remove(SecurityContext::AUTHENTICATION_ERROR);
    	}
    	
    	$viewOptions = array(
    	    // last username entered by the user
    	    'last_username' => $session->get(SecurityContext::LAST_USERNAME),
    	    'error'         => $error,
    	);
    	
    	if ($request->query->has('login_redirect')) {
    	    $viewOptions['login_redirect'] = $request->query->get('login_redirect');
    	}
    	
    	
    	return $this->render('WanaworkMainBundle:Default:login.html.twig', $viewOptions);
    }
    
    /**
     * Decide where to redirect the given user for a home page
     * @return NULL | string
     */
    private function decideHomePage()
    {
        $url = null;
        $user = $this->getUser();
        if($user instanceof User) {
            $roles = $user->getRoles();
            $url = null;
            if (array_key_exists(Role::ADMIN, $roles)) {
                $url = $this->generateUrl('daily_details');
            } elseif (array_key_exists(Role::EMPLOYEE, $roles)) {
                $url = $this->generateUrl('my_ads');
            } elseif (array_key_exists(Role::EMPLOYER, $roles)) {
                $url = $this->generateUrl('wanawork_main_search');
            } else {
                $url = $this->generateUrl('wanawork_main_homepage');
                $this->get('logger')->error('Unable to generate a specific after login URL', array(
                    'user' => $user->getId(),
                ));
            }
        }
        
        return $url;
    }
    
    public function contactUsAction()
    {
        $form = $this->createForm(new ContactUsType());
        
        $viewOptions = array(
            'contact_form' => $form->createView(),
        );
        
        if ($this->getRequest()->isMethod('POST')) {
            $form->handleRequest($this->getRequest());
            
            if ($form->isValid()) {
                $data = $form->getData();
                
                $text = nl2br($data['message']);
                $body = "
                <html><body>
                Name: {$data['name']}<br />
                Email: {$data['email']}<br />
                Phone: {$data['phone']}<br />
                Text: {$text}
                </body></html>
                ";
                $subject = 'Email from contact us form';
                $message = \Swift_Message::newInstance()->setContentType('text/html')
                ->setSubject($subject)
                ->setFrom("robot@wannawork.ie")
                ->setTo("hello@wannawork.ie")
                ->setBody($body);
                
                $this->get('swiftmailer.mailer.default')->send($message);
                $viewOptions['message_sent'] = true;
            }
        }
        
        return $this->render('WanaworkMainBundle:Default:contact-us.html.twig', $viewOptions);
    }
    
    public function employersOverviewAction()
    {
        $accountsService = $this->get('wanawork.accounts_service');
        $employer = new User();
        $employer->addRole($accountsService->getRoleForAccountType('employer'));
         
        $formEmployer = $this->createForm(new EmployerType(), $employer);
        
        return $this->render('WanaworkMainBundle:Default:employers-overview.html.twig', array(
        	'form_employer' => $formEmployer->createView(),
        ));
    }
}
