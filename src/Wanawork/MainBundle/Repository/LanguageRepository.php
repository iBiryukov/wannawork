<?php
namespace Wanawork\MainBundle\Repository;

use Doctrine\ORM\EntityRepository;


class LanguageRepository extends EntityRepository
{

    public function findRandom($limit = 1)
    {
        $dql = "SELECT COUNT(c) FROM {$this->getEntityName()} c";
        $count = $this->getEntityManager()->createQuery($dql)
        ->getSingleScalarResult();
    
        return $this->createQueryBuilder("c")->setMaxResults($limit)
        ->setFirstResult(rand(0, $count - 1))->getQuery()->getResult();
    }
    
    public function deleteWhereNotInId(array $ids)
    {
        $dql = "DELETE {$this->_entityName} e WHERE e.id NOT IN(:ids)";
        $query = $this->getEntityManager()->createQuery($dql);
        $query->setParameter('ids', $ids);
        return $query->execute();
    }
    
    /**
     * Get the next id
     * @return number
     */
    public function findNextId()
    {
        $dql = "SELECT MAX(s) FROM {$this->getEntityName()} s";
        $query = $this->getEntityManager()->createQuery($dql);
        $id = $query->getSingleScalarResult();
        if(!is_int($id)) {
            $id = 0;
        }
        return ($id + 1);
    }
    
}