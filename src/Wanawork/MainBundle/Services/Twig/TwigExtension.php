<?php
namespace Wanawork\MainBundle\Services\Twig;
class TwigExtension extends \Twig_Extension {

	public function getFunctions() {
		return array(
			'current_url' => new \Twig_Function_Method($this, 'currentUrl'),
		    'current_path' => new \Twig_Function_Method($this, 'currentPath'),
			'ckeditor_clean' => new \Twig_Function_Method($this, 'cleanCkeditorOutput'),
			'site_host' => new \Twig_Function_Method($this, 'siteHost'),
			'begins_with' => new \Twig_Function_Method($this, 'beginsWith'),
		    'clean_url' => new \Twig_Function_Method($this, 'cleanUrl'),
		    'ceil' => new \Twig_Function_Method($this, 'ceil'),
		    'floor' => new \Twig_Function_Method($this, 'floor'),
		    'pp_json' => new \Twig_Function_Method($this, 'json_format'),
		);
	}
	
	public function floor($number)
	{
	    return floor($number);
	}
	
	public function ceil($number)
	{
	    return ceil($number);
	}
	
	public function cleanUrl($str)
	{
	    $sluggableText = $str;
	    $urlized = strtolower( 
	        trim( 
	           preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', iconv('UTF-8', 'ASCII//TRANSLIT', $sluggableText) ), '-' 
		    ) 
		);
	    $urlized = preg_replace("/[\/_|+ -]+/", '-', $urlized);
	    
	    // @todo fix this
	    if(strlen($str) > 0 && strlen($urlized) === 0) {
	        $urlized = 'na';
	    }
    
    	return $urlized;
	}
	
	/**
	 * Check if the string begins with the given token
	 * @param string $haystack
	 * @param string $needle
	 * @return boolean
	 */
	public function beginsWith($haystack, $needle) 
	{
		return strpos($haystack, $needle) === 0;
	}
	
	public function cleanCkeditorOutput($text) {
		$allowedTags = array(
            '<p>', '<b>', '<i>', '<u>',
		);
		return strip_tags($text, implode('', $allowedTags));
	}

	public function currentUrl()
	{
		$pageURL = (isset($_SERVER['HTTPS']) && $_SERVER["HTTPS"] == "on") ? "https://" : "http://";
		if (isset($_SERVER["SERVER_PORT"]) && $_SERVER["SERVER_PORT"] != "80") {
			$pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"]
					. $_SERVER["REQUEST_URI"];
		} else {
			$pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
		}
		return $pageURL;
	}
	
	public function currentPath()
	{
	    return isset($_SERVER["REQUEST_URI"]) ? $_SERVER["REQUEST_URI"] : null;
	}
	
	public function siteHost() {
		$pageURL = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
		if ($_SERVER["SERVER_PORT"] != "80") {
			$pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"];
		} else {
			$pageURL .= $_SERVER["SERVER_NAME"];
		}
		return $pageURL;
	}

	public function getName() {
		return 'wanaworkmainbundle_twig_extension';
	}
	
	// Pretty print some JSON
	public function json_format($json)
	{
	    $tab = "  ";
	    $new_json = "";
	    $indent_level = 0;
	    $in_string = false;
	
	    $json_obj = json_decode($json);
	
	    if($json_obj === false)
	        return false;
	
	    $json = json_encode($json_obj);
	    $len = strlen($json);
	
	    for($c = 0; $c < $len; $c++)
	    {
	        $char = $json[$c];
	        switch($char)
	        {
	        	case '{':
	        	case '[':
	        	    if(!$in_string)
	        	    {
	        	        $new_json .= $char . "\n" . str_repeat($tab, $indent_level+1);
	        	        $indent_level++;
	        	    }
	        	    else
	        	    {
	        	        $new_json .= $char;
	        	    }
	        	    break;
	        	case '}':
	        	case ']':
	        	    if(!$in_string)
	        	    {
	        	        $indent_level--;
	        	        $new_json .= "\n" . str_repeat($tab, $indent_level) . $char;
	        	    }
	        	    else
	        	    {
	        	        $new_json .= $char;
	        	    }
	        	    break;
	        	case ',':
	        	    if(!$in_string)
	        	    {
	        	        $new_json .= ",\n" . str_repeat($tab, $indent_level);
	        	    }
	        	    else
	        	    {
	        	        $new_json .= $char;
	        	    }
	        	    break;
	        	case ':':
	        	    if(!$in_string)
	        	    {
	        	        $new_json .= ": ";
	        	    }
	        	    else
	        	    {
	        	        $new_json .= $char;
	        	    }
	        	    break;
	        	case '"':
	        	    if($c > 0 && $json[$c-1] != '\\')
	        	    {
	        	        $in_string = !$in_string;
	        	    }
	        	default:
	        	    $new_json .= $char;
	        	    break;
	        }
	    }
	
	    return $new_json;
	}
}

