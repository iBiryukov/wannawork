<?php
namespace Wanawork\MainBundle\Services;
use Symfony\Component\Security\Acl\Model\MutableAclProviderInterface;

use Symfony\Component\Security\Acl\Domain\ObjectIdentity;

class AclService {
	
	private $acl;
	
	public function __construct(MutableAclProviderInterface $acl) {
		$this->acl = $acl;	
	}

	/**
	 * Preload ACL for all the given objects with 1 query
	 * @param mixed $objects
	 * @return \SplObjectStorage mapping the passed object identities to ACLs
	 */
	public function preloadAcls($objects) 
	{
		$oids = array();
		foreach($objects as $object) {
			$oids[] = ObjectIdentity::fromDomainObject($object);	
		}
		return $this->acl->findAcls($oids);
	}

}
