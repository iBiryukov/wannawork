<?php
namespace Wanawork\MainBundle\Services\Trader;

use Wanawork\UserBundle\Interfaces\Trader;
use Doctrine\ORM\EntityManager;
use Wanawork\MainBundle\Entity\NameTitle;

class NameTitleTrader implements Trader
{
    private $entityManager;
    
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }
    
    public function export()
    {
        $nameTitles = $this->entityManager->getRepository('WanaworkMainBundle:NameTitle')->findAll();
        $data = array();
        foreach ($nameTitles as $nameTitle)
        {
            $data[] = $nameTitle->__toArray();
        }
        return $data;
    }
    
    public function import(\SplFileInfo $file, $callback = null)
    {
        $result = array(
            self::INSERTED => 0,
            self::DELETED  => 0,
            self::UPDATED  => 0,
            self::PROCESSED => 0,
        );
        
        $data = json_decode(file_get_contents($file->getRealPath()), $assoc = true);
        if (!is_array($data)) {
        
            throw new \Exception(
                sprintf("Unable to read the file: '%s'. Error: %s", $file->getRealPath(), json_last_error_msg())
            );
        }
        
        $nameTitleRepository = $this->entityManager->getRepository('WanaworkMainBundle:NameTitle');
        $ids = array();
        
        foreach ($data as $nameTitleData) {
            $id = $nameTitleData['id'];
            $title = $nameTitleData['title'];
        
            if (in_array($id, $ids)) {
                throw new \Exception("ID: '$id' is duplicated");
            }
        
            $ids[] = $id;
        
            $nameTitle = $nameTitleRepository->find($id);
            if (!$nameTitle instanceof NameTitle) {
                $nameTitle = new NameTitle($id, $title);
                $this->entityManager->persist($nameTitle);
                ++$result[self::INSERTED];
            } else {
        
                $changed = false;
                if ($title !== $nameTitle->getTitle()) {
                    $nameTitle->setTitle($title);
                    $changed = true;
                }
        
                if ($changed) {
                    ++$result[self::UPDATED];
                    $this->entityManager->persist($nameTitle);
                }
            }
        
            if ($callback !== null) {
                $callback($nameTitle);
            }
        
            ++$result[self::PROCESSED];
        }
        
        $result[self::DELETED] = $nameTitleRepository->deleteWhereNotInId($ids);
        
        $this->entityManager->flush();

        return $result;
    }
}