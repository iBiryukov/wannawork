<?php
namespace Wanawork\MainBundle\Services\Trader;

use Wanawork\UserBundle\Interfaces\Exporter;
use Doctrine\ORM\EntityManager;
use Wanawork\UserBundle\Interfaces\Trader;
use Wanawork\MainBundle\Entity\Country;

class CountryTrader implements Trader
{
    private $entityManager;
    
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }
    
    public function export()
    {
        $countries = $this->entityManager->getRepository('WanaworkMainBundle:Country')->findAll();
        $data = array();
        foreach ($countries as $country)
        {
            $data[] = $country->__toArray();
        }
        return $data;
    }
    
    public function import(\SplFileInfo $file, $callable = null)
    {
        $result = array(
        	self::INSERTED => 0,
            self::DELETED  => 0,
            self::UPDATED  => 0,
            self::PROCESSED => 0,
        );
        
        $data = json_decode(file_get_contents($file->getRealPath()), $assoc = true);
        if (!is_array($data)) {
            
            throw new \Exception(
        	   sprintf("Unable to read the file: '%s'. Error: %s", $file->getRealPath(), json_last_error_msg())
            );
        }
        
        $countryRepository = $this->entityManager->getRepository('WanaworkMainBundle:Country');
        $ids = array();
        
        foreach ($data as $countryData) {
            $id = $countryData['id'];
            $name = $countryData['name'];
            $code = $countryData['code'];
            $ids[] = $id;
            
            $country = $countryRepository->find($id);
            if (!$country instanceof Country) {
                $country = new Country($id, $name, $code);
                $this->entityManager->persist($country);
                ++$result[self::INSERTED];
            } else {
                
                $changed = false;
                if ($name !== $country->getName()) {
                    $country->setName($name);
                    $changed = true;
                }
                
                if ($code !== $country->getCode()) {
                    $country->setCode($code);
                    $changed = true;
                }
                
                if ($changed) {
                    ++$result[self::UPDATED];
                    $this->entityManager->persist($country);
                }
            }
                        
            if ($callable !== null) {
                $callable($country);
            }
            
            ++$result[self::PROCESSED];
        }
        
        $result[self::DELETED] = $countryRepository->deleteWhereNotInId($ids);
        
        
        $this->entityManager->flush();
        
        return $result;
    }
}