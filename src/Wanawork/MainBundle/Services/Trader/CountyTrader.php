<?php
namespace Wanawork\MainBundle\Services\Trader;

use Wanawork\UserBundle\Interfaces\Trader;
use Doctrine\ORM\EntityManager;
use Wanawork\MainBundle\Entity\County;

class CountyTrader implements Trader
{
    private $entityManager;
    
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }
    
    public function export()
    {
        $counties = $this->entityManager->getRepository('WanaworkMainBundle:County')->findAll();
        $data = array();
        foreach ($counties as $county)
        {
            $data[] = $county->__toArray();
        }
        return $data;
    }
    
    public function import(\SplFileInfo $file, $callable = null)
    {
        $result = array(
            self::INSERTED => 0,
            self::DELETED  => 0,
            self::UPDATED  => 0,
            self::PROCESSED => 0,
        );
    
        $data = json_decode(file_get_contents($file->getRealPath()), $assoc = true);
        if (!is_array($data)) {
    
            throw new \Exception(
            	   sprintf("Unable to read the file: '%s'. Error: %s", $file->getRealPath(), json_last_error_msg())
            );
        }
    
        $countyRepository = $this->entityManager->getRepository('WanaworkMainBundle:County');
        $ids = array();
    
        foreach ($data as $countyData) {
            $id = $countyData['id'];
            $name = $countyData['county_name'];
            
            if (in_array($id, $ids)) {
                throw new \Exception("ID: '$id' is duplicated");    
            }
            
            $ids[] = $id;
    
            $county = $countyRepository->find($id);
            if (!$county instanceof County) {
                $county = new County($id, $name);
                $this->entityManager->persist($county);
                ++$result[self::INSERTED];
            } else {
    
                $changed = false;
                if ($name !== $county->getName()) {
                    $county->setName($name);
                    $changed = true;
                }
    
                if ($changed) {
                    ++$result[self::UPDATED];
                    $this->entityManager->persist($county);
                }
            }
    
            if ($callable !== null) {
                $callable($county);
            }
    
            ++$result[self::PROCESSED];
        }
    
        $result[self::DELETED] = $countyRepository->deleteWhereNotInId($ids);
    
        $this->entityManager->flush();
        return $result;
    }
}