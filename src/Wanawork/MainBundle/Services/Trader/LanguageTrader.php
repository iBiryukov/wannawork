<?php
namespace Wanawork\MainBundle\Services\Trader;

use Wanawork\UserBundle\Interfaces\Trader;
use Doctrine\ORM\EntityManager;
use Wanawork\MainBundle\Entity\Language;

class LanguageTrader implements Trader
{
    private $entityManager;
    
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }
    
    public function export()
    {
        $languages = $this->entityManager->getRepository('WanaworkMainBundle:Language')->findAll();
        $data = array();
        foreach ($languages as $language)
        {
            $data[] = $language->__toArray();
        }
        return $data;
    }
    
    public function import(\SplFileInfo $file, $callable = null)
    {
        $result = array(
            self::INSERTED => 0,
            self::DELETED  => 0,
            self::UPDATED  => 0,
            self::PROCESSED => 0,
        );
    
        $data = json_decode(file_get_contents($file->getRealPath()), $assoc = true);
        if (!is_array($data)) {
    
            throw new \Exception(
                sprintf("Unable to read the file: '%s'. Error: %s", $file->getRealPath(), json_last_error_msg())
            );
        }
    
        $languageRepository = $this->entityManager->getRepository('WanaworkMainBundle:Language');
        $ids = array();
    
        foreach ($data as $languageData) {
            $id = $languageData['id'];
            $name = $languageData['name'];
    
            if (in_array($id, $ids)) {
                throw new \Exception("ID: '$id' is duplicated");
            }
    
            $ids[] = $id;
    
            $language = $languageRepository->find($id);
            if (!$language instanceof Language) {
                $language = new Language($id, $name);
                $this->entityManager->persist($language);
                ++$result[self::INSERTED];
            } else {
    
                $changed = false;
                if ($name !== $language->getName()) {
                    $language->setName($name);
                    $changed = true;
                }
    
                if ($changed) {
                    ++$result[self::UPDATED];
                    $this->entityManager->persist($language);
                }
            }
    
            if ($callable !== null) {
                $callable($language);
            }
    
            ++$result[self::PROCESSED];
        }
    
        $result[self::DELETED] = $languageRepository->deleteWhereNotInId($ids);
    
        $this->entityManager->flush();
        return $result;
    }
}