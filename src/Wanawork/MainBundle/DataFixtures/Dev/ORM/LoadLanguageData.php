<?php
namespace Wanawork\MainBundle\DataFixtures\Dev\ORM;

use Wanawork\MainBundle\Entity\Language;
use Wanawork\MainBundle\DataFixtures\DataLoader;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Wanawork\UserBundle\Interfaces\Importer;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

class LoadLanguageData extends DataLoader implements ContainerAwareInterface
{

    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    private $container;
    
	public function load(ObjectManager $manager)
	{
		$file = new \SplFileInfo(__DIR__ . '/../data/language.json');
		
		$trader = $this->container->get('wanawork.language_trader');
		
		$that = $this;
		$result =$trader->import($file, function(Language $language) use($that){
		    $that->addReference("language-{$language->getName()}", $language);
		});
		
	    echo sprintf(
	        "Language result - Processed: %d, Inserted: %d, Updated: %d, Deleted: %d\n",
	        $result[Importer::PROCESSED], $result[Importer::INSERTED], $result[Importer::UPDATED],
	        $result[Importer::DELETED]
	    );
	}
	
	public function setContainer(ContainerInterface $container = null)
	{
	    $this->container = $container;
	}

}

