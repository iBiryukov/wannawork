<?php
namespace Wanawork\MainBundle\DataFixtures\Dev\ORM;

use Wanawork\MainBundle\DataFixtures\DataLoader;
use Wanawork\MainBundle\Entity\County;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Wanawork\UserBundle\Interfaces\Importer;

class LoadCountyData extends DataLoader implements ContainerAwareInterface
{
	/**
	 * @var \Symfony\Component\DependencyInjection\ContainerInterface
	 */
    private $container;
    
	public function load(ObjectManager $manager)
	{
		
		$file = new \SplFileInfo(__DIR__ . '/../data/county.json');
		
		$trader = $this->container->get('wanawork.county_trader');
		
		$that = $this;
		$result =$trader->import($file, function(County $county) use($that){
		    $that->addReference("county-{$county->getName()}", $county);
		});
		
	    echo sprintf(
	        "County result - Processed: %d, Inserted: %d, Updated: %d, Deleted: %d\n",
	        $result[Importer::PROCESSED], $result[Importer::INSERTED], $result[Importer::UPDATED],
	        $result[Importer::DELETED]
	    );
	}
	
	public function setContainer(ContainerInterface $container = null)
	{
	    $this->container = $container;
	}

}

