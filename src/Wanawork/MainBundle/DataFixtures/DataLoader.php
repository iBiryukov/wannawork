<?php
namespace Wanawork\MainBundle\DataFixtures;

use Symfony\Component\Yaml\Parser;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Abstract class to load json fixtures files
 * @author Ilya Biryukov <ilya@goparty.ie>
 *
 */
abstract class DataLoader extends AbstractFixture
{

	protected function loadFile($file) {
		
		if(!defined("IS_CMD")) {
			define("IS_CMD", true);	
		}
		
		if(!file_exists($file)) {
			throw new FileNotFoundException($file);
		}
		
		if(!is_readable($file)) {
			throw new IOException("Fixtures file is not readable: '$file'");
		}
		
		$data = json_decode(file_get_contents($file), $assoc = true);
		
		if($data === null) {
			
			switch (json_last_error()) {
				case JSON_ERROR_NONE:
					echo ' - No errors';
					break;
				case JSON_ERROR_DEPTH:
					echo ' - Maximum stack depth exceeded';
					break;
				case JSON_ERROR_STATE_MISMATCH:
					echo ' - Underflow or the modes mismatch';
					break;
				case JSON_ERROR_CTRL_CHAR:
					echo ' - Unexpected control character found';
					break;
				case JSON_ERROR_SYNTAX:
					echo ' - Syntax error, malformed JSON';
					break;
				case JSON_ERROR_UTF8:
					echo ' - Malformed UTF-8 characters, possibly incorrectly encoded';
					break;
				default:
					echo ' - Unknown error';
					break;
			}
			
			throw new \Exception("Fixtures file contains invalid json: '$file'. \n" . json_last_error());
		}
		
		return $data;
		
	}
}

?>