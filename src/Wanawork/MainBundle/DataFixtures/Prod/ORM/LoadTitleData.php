<?php
namespace Wanawork\MainBundle\DataFixtures\Prod\ORM;

use Wanawork\MainBundle\Entity\NameTitle;

use Doctrine\Common\Persistence\ObjectManager;

use Wanawork\MainBundle\DataFixtures\DataLoader;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Wanawork\UserBundle\Interfaces\Importer;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadTitleData extends DataLoader implements ContainerAwareInterface
{
    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    private $container;
    
	public function load(ObjectManager $manager)
	{
		$file = new \SplFileInfo(__DIR__ . '/../data/titles.json');
		
		$trader = $this->container->get('wanawork.name_title_trader');
		
		$that = $this;
		$result = $trader->import($file, function(NameTitle $nameTitle) use($that){
		    $this->addReference("title-{$nameTitle->getTitle()}", $nameTitle);
		});
		
	    echo sprintf(
	        "Language result - Processed: %d, Inserted: %d, Updated: %d, Deleted: %d\n",
	        $result[Importer::PROCESSED], $result[Importer::INSERTED], $result[Importer::UPDATED],
	        $result[Importer::DELETED]
	    );
	}
	
	public function setContainer(ContainerInterface $container = null)
	{
	    $this->container = $container;
	}
}

