<?php
namespace Wanawork\MainBundle\DataFixtures\Prod\ORM;

use Wanawork\MainBundle\Entity\Country;
use Doctrine\Common\Persistence\ObjectManager;
use Wanawork\MainBundle\DataFixtures\DataLoader;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Wanawork\UserBundle\Interfaces\Importer;

class LoadCountryData extends DataLoader implements ContainerAwareInterface
{
    private $container;

	public function load(ObjectManager $manager)
	{
		$file = new \SplFileInfo(__DIR__ . '/../data/country.json');

		$trader = $this->container->get('wanawork.country_trader');
		
		$that = $this;
		$result =$trader->import($file, function(Country $country) use($that){
		    $that->addReference("country-{$country->getName()}", $country);
		});
		
		echo sprintf(
	       "Country result - Processed: %d, Inserted: %d, Updated: %d, Deleted: %d\n",
		    $result[Importer::PROCESSED], $result[Importer::INSERTED], $result[Importer::UPDATED],
		    $result[Importer::DELETED]
		);
	}
	
	public function setContainer(ContainerInterface $container = null)
	{
	    $this->container = $container;    
	}
}

