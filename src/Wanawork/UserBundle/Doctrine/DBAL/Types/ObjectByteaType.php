<?php
namespace Wanawork\UserBundle\Doctrine\DBAL\Types;

use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Platforms\PostgreSqlPlatform;

class ObjectByteaType extends Type
{
    const TYPE = 'object_bytea';
    
    /**
     * {@inheritdoc}
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        if (!$platform instanceof PostgreSqlPlatform) {
            throw new \Exception("Only Postgres is supported");
        }
        
        return 'BYTEA';
    }
    
    /**
     * {@inheritdoc}
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (!$platform instanceof PostgreSqlPlatform) {
            throw new \Exception("Only Postgres is supported");
        }
        
        return serialize($value);
    }
    
    /**
     * {@inheritdoc}
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if (!$platform instanceof PostgreSqlPlatform) {
            throw new \Exception("Only Postgres is supported");
        }
        
        if ($value === null) {
            return null;
        }
        
        $value = (is_resource($value)) ? stream_get_contents($value) : $value;
        $val = unserialize($value);
        if ($val === false && $value !== 'b:0;') {
            throw ConversionException::conversionFailed($value, $this->getName());
        }
    
        return $val;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return self::TYPE;
    }
    
    /**
     * {@inheritdoc}
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform)
    {
        return true;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getBindingType()
    {
        return \PDO::PARAM_LOB;
    }
}