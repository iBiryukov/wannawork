<?php
namespace Wanawork\UserBundle\Interfaces;

interface Exporter
{
    /**
     * @return array
     */
    public function export();
}