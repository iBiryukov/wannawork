<?php
namespace Wanawork\UserBundle\Interfaces;

interface EntityWithFileInterface
{
    /**
     * Perform oeprations on the file before the entity is persisted
     */
    public function preUpload();
    
    /**
     * Perform operations on the file after the entity is peristed
     */
    public function upload();
    
    /**
     * Perform operations on the file after the enitity was wiped from the database
     */
    public function removeUpload();
    
    /**
     * Get the full path on the server where the file is stored
     * @return string
     */
    public function getAbsolutePath();
    
    /**
     * Get the full path where the file can be downloaded by general public
     * @return string
     */
    public function getWebPath();
    
}