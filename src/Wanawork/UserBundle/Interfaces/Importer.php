<?php
namespace Wanawork\UserBundle\Interfaces;

interface Importer
{
    const UPDATED  = 1;
    const DELETED  = 2;
    const INSERTED = 3;
    const PROCESSED = 4;
    
    /**
     * Import the file into the database
     * @param \SplFileInfo  $file      File to import
     * @param callable      $callback  Gets called for each imported entity
     * 
     * @return void
     */
    public function import(\SplFileInfo $file, $callable = null);
}