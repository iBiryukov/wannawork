<?php
namespace Wanawork\UserBundle\Interfaces;

interface Trader extends Importer, Exporter
{
}