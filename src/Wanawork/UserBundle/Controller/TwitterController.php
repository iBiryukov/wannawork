<?php
namespace Wanawork\UserBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;


/**
 * 
 * @Route("/admin/social")
 */
class TwitterController extends Controller
{
    /**
     * @Route("/twitter", name="social_twitter")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function tweetsAction()
    {
        $dql = "SELECT tweet
				FROM Wanawork\UserBundle\Entity\Social\Twitter\JobsFairyTweet tweet
                JOIN tweet.tweet t
				ORDER BY t.createdAt DESC";
        
        $query = $this->getDoctrine()->getManager()->createQuery($dql);
        
        $perPage = 10;
        $pageNumber = $this->get('request')->query->get('page', 1);
        
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate($query, $pageNumber, $perPage, ['distinct' => false]);
        
        return array(
            'entities' => $pagination,
            'adRepositry' => $this->getDoctrine()->getManager()->getRepository("WanaworkUserBundle:Ad"),
        );
    }
}