<?php
namespace Wanawork\UserBundle\Controller;

use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Wanawork\UserBundle\Entity\SystemEmail;
use Symfony\Component\HttpFoundation\Response;
use Wanawork\UserBundle\Entity\UnsubscribedEmail;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Wanawork\UserBundle\Form\SystemEmailType;

/**
 * SystemEmail controller.
 *
 */
class SystemEmailController extends Controller
{

    /**
     * Lists all SystemEmail entities.
     *
     * @Route("/admin/mail/system-mail/", name="systememail")
     * @Method("GET")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function indexAction()
    {
        $dql = "SELECT email
				FROM WanaworkUserBundle:SystemEmail email
				ORDER BY email.dateCreated DESC, email.dateSent DESC";
        
        $query = $this->getDoctrine()->getManager()->createQuery($dql);
        
        $perPage = 10;
        $pageNumber = $this->get('request')->query->get('page', 1);
        
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate($query, $pageNumber, $perPage);
        
        return array(
            'entities' => $pagination,
        );
    }
    
    /**
     *
     * @Route("/admin/mail/system-mail/reports/{date}", name="systememail_reports")
     * @Method("GET")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function reportsAction($date = null)
    {
        if ($date !== null && !preg_match('/^\d{1,2}\-\d{4}$/', $date)) {
            $this->createNotFoundException("Invalid date provided");
        }
        
        $em = $this->getDoctrine()->getManager();
        $dates = $em->getRepository('WanaworkUserBundle:SystemEmail')->findReportDates();
        $results = $em->getRepository('WanaworkUserBundle:SystemEmail')->findStats($date);
        
        return array(
            'selectedDate' => $date,
        	'dates' => $dates,
            'results' => $results,
        );
    }

    /**
     * Finds and displays a SystemEmail entity.
     *
     * @Route("/admin/mail/system-mail/{id}", name="systememail_show")
     * @Method("GET")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function showAction(SystemEmail $systemEmail)
    {
        // Remove tracker
        $text = $systemEmail->getText();
        $text = preg_replace('/<img.*id="email\-tracker".*\/>/', '', $text);
        $systemEmail->setText($text);
        
        return new Response($text);
    }
    
    /**
     * Finds and displays a SystemEmail entity.
     *
     * @Route("/admin/mail/system-mail/{id}/open-stats", name="systememail_open_stats")
     * @Method("GET")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function emailOpenStatAction(SystemEmail $systemEmail)
    {
        return array(
        	'systemEmail' => $systemEmail,
        );   
    }
    
    /**
     * Finds and displays a SystemEmail entity.
     *
     * @Route("/admin/mail/system-mail/{id}/link-stats", name="systememail_link_stats")
     * @Method("GET")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function emailLinkStatAction(SystemEmail $systemEmail)
    {
        return array(
            'systemEmail' => $systemEmail,
        );
    }
    
    
    /**
     * @Route("/unsubscribe/{id}", name="systememail_unsubscribe", requirements={"id"="\w+"})
     * @param SystemEmail $email
     * @Template()
     */
    public function unsubscribeAction(SystemEmail $systemEmail)
    {
        $mailService = $this->get('wanawork.mail');
        $unsub = $mailService->unsubscribe($systemEmail->getEmail(), $systemEmail);
        
        return array(
        	'systemEmail' => $systemEmail,
        );
    }
    
    /**
     * @Route("/resubscribe/{id}", name="systememail_resubscribe", requirements={"id"="\w+"})
     * @param SystemEmail $email
     * @Template()
     */
    public function resubscribeAction(SystemEmail $systemEmail)
    {
        $mailService = $this->get('wanawork.mail');
        $unsub = $mailService->resubscribe($systemEmail->getEmail(), $systemEmail);
    
        return array(
            'systemEmail' => $systemEmail,
        );
    }
    
    /**
     * Track email opens
     * @param SystemEmail $systemEmail
     * @Route("/email/track/{id}", name="systememail_track", requirements={"id"="\w+"})
     */
    public function trackAction(SystemEmail $systemEmail)
    {
        // @todo remove hack for admins
        $security = $this->get('security.context');
        if ($systemEmail->getStatus() === SystemEmail::STATUS_SENT && !$security->isGranted("ROLE_ADMIN")) {
            $ip = $this->getRequest()->getClientIp();
            $tracker = $systemEmail->addTracker($ip);
            $this->getDoctrine()->getManager()->flush();
        }
        
        
        $file = $this->container->getParameter('kernel.root_dir') . '/../web/images/1px.png';
        $response = new BinaryFileResponse($file);
        $response->headers->set('Content-Type', 'image/png');
        $response->setPrivate();
        $response->setMaxAge(0);
        $response->setLastModified(new \DateTime());
        $response->headers->addCacheControlDirective('must-revalidate', true);
        
        return $response;
    }

    /**
     * @Route("/admin/mail/system-email/new/{type}", name="systememail_new", requirements={"type"="\d+"})
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function newAction($type)
    {
        if (!array_key_exists($type, SystemEmail::$templates)) {
            throw $this->createNotFoundException("Invalid email type requested");
        }
        $subject = SystemEmail::$subjects[$type];
        
        $systemEmail = new SystemEmail(null, $subject, $type);
        $templateName = SystemEmail::$templates[$type];
        $template = "::/email/{$templateName}.html.twig";
        
        $text = $this->renderView($template, array(
        	'systemEmail' => $systemEmail,
        ));
        
        $systemEmail->setText($text, $trackLinks = false);
        
        $mailers = $this->container->getParameter('swiftmailer.mailers');
        $form = $this->createForm(new SystemEmailType($mailers), $systemEmail);
        
        $request = $this->getRequest();
        
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            
            if ($form->isValid()) {
                $systemEmail->setText($systemEmail->getText());
                $mailerNameBits = explode('.', $form->get('mail_account')->getData());
                $mailerName = "wanawork.mail." . $mailerNameBits[sizeof($mailerNameBits) - 1];
                
                $mailer = $this->get($mailerName);
                $mailer->emailCustomMessage($systemEmail);
                
                return $this->redirect($this->generateUrl('systememail'));
            }
        }
        
        return array(
        	'form' => $form->createView(),
        );
    }
}
