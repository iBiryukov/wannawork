<?php
namespace Wanawork\UserBundle\Controller;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Wanawork\UserBundle\Entity\CV;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Wanawork\UserBundle\Entity\Reference;
use Wanawork\UserBundle\Form\ReferenceType;

/**
 * Reference controller.
 *
 */
class ReferenceController extends Controller
{
    /**
     * Lists all Reference entities.
     *
     * @Route("/reference", name="reference")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('WanaworkUserBundle:Reference')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Finds and displays a Reference entity.
     *
     * @Route("/reference/{id}/show", name="reference_show")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WanaworkUserBundle:Reference')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Reference entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to create a new Reference entity.
     *
     * @Route("/cv/{cvId}/new-reference", name="reference_new")
     * @Template()
     * @Secure(roles="ROLE_EMPLOYEE")
     * @Method("GET")
     */
    public function newAction($cvId)
    {
    	$em = $this->getDoctrine();
    	$cvRepository = $em->getRepository('WanaworkUserBundle:CV');
    	$cvEntity = $cvRepository->find($cvId);
    	 
    	if(!$cvEntity instanceof CV) {
    		throw $this->createNotFoundException('Cv was not found');
    	}
    	 
    	$security = $this->get('security.context');
    	if(!$security->isGranted('EDIT', $cvEntity)) {
    		throw new AccessDeniedException("You don't have permission to access this page");
    	}
    	
        $entity = new Reference();
        $form   = $this->createForm(new ReferenceType(), $entity);

        return array(
            'cv' => $cvEntity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a new Reference entity.
     *
     * @Route("/cv/{cvId}/create-reference", name="reference_create")
     * @Method("POST")
     * @Template("WanaworkUserBundle:Reference:new.html.twig")
     * @Secure(roles="ROLE_EMPLOYEE")
     */
    public function createAction(Request $request, $cvId)
    {
    	$em = $this->getDoctrine();
    	$cvRepository = $em->getRepository('WanaworkUserBundle:CV');
    	$cvEntity = $cvRepository->find($cvId);
    	
    	if(!$cvEntity instanceof CV) {
    		throw $this->createNotFoundException('Cv was not found');
    	}
    	
    	$security = $this->get('security.context');
    	if(!$security->isGranted('EDIT', $cvEntity)) {
    		throw new AccessDeniedException("You don't have permission to access this page");
    	}
    	
        $referenceEntity  = new Reference();
        $referenceEntity->setCv($cvEntity);
        
        $form = $this->createForm(new ReferenceType(), $referenceEntity);
        $form->bind($request);

        if ($form->isValid()) {
        	
        	$employeeService = $this->get('wanawork.employee_service');
        	$employeeService->createReference($referenceEntity, $cvEntity);
        	
        	
            $this->get('session')->getFlashBag()->add('messages', 'New Reference Saved');
            return $this->redirect($this->generateUrl('cv_show', 
            			array('id' => $cvEntity->getId())));
        }

        return array(
            'cv' => $cvEntity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Reference entity.
     *
     * @Route("/reference/{id}/edit", name="reference_edit")
     * @Template()
     * @Secure(roles="ROLE_EMPLOYEE")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $referenceEntity = $em->getRepository('WanaworkUserBundle:Reference')->find($id);

        if (!$referenceEntity instanceof Reference) {
            throw $this->createNotFoundException('Unable to find Reference entity.');
        }
        
        $security = $this->get('security.context');
        if(!$security->isGranted('EDIT', $referenceEntity->getCv())) {
        	throw new AccessDeniedException("You don't have permission to access this page");
        }
        
        $editForm = $this->createForm(new ReferenceType(), $referenceEntity);
        return array(
            'reference' => $referenceEntity,
            'form'   => $editForm->createView(),
        );
    }

    /**
     * Edits an existing Reference entity.
     *
     * @Route("/reference/{id}/update", name="reference_update")
     * @Method("POST")
     * @Template("WanaworkUserBundle:Reference:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $referenceEntity = $em->getRepository('WanaworkUserBundle:Reference')->find($id);

        if (!$referenceEntity instanceof Reference) {
            throw $this->createNotFoundException('Unable to find Reference entity.');
        }

        $security = $this->get('security.context');
        if(!$security->isGranted('EDIT', $referenceEntity->getCv())) {
        	throw new AccessDeniedException("You don't have permission to access this page");
        }

        $editForm = $this->createForm(new ReferenceType(), $referenceEntity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($referenceEntity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('messages', 'Reference Updated');
            return $this->redirect($this->generateUrl(
            			'cv_show',
            			array('id' => $referenceEntity->getCv()->getId())
            		));
        }

        return array(
            'entity'      => $referenceEntity,
            'edit_form'   => $editForm->createView(),
        );
    }

    /**
     * Deletes a Reference entity.
     *
     * @Route("/reference/{id}/delete", name="reference_delete")
     * @Method("POST")
     * @Secure(roles="ROLE_EMPLOYEE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $referenceEntity = $em->getRepository('WanaworkUserBundle:Reference')->find($id);

            if (!$referenceEntity instanceof Reference) {
                throw $this->createNotFoundException('Unable to find Reference entity.');
            }
            
            $security = $this->get('security.context');
            if(!$security->isGranted('EDIT', $referenceEntity->getCv())) {
            	throw new AccessDeniedException("You don't have permission to access this page");
            }
            
            $cvId = $referenceEntity->getCv()->getId();
            
            $employeeService = $this->get('wanawork.employee_service');
            $employeeService->deleteReference($referenceEntity);
            
            $this->get('session')->getFlashBag()->add('messages', 'Reference Removed');
            return $this->redirect($this->generateUrl('cv_show',
            		array('id' => $cvId)));
        } else {
        	throw new AccessDeniedException();
        }
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
