<?php
namespace Wanawork\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Wanawork\UserBundle\Entity\SectorExperience;
use Wanawork\UserBundle\Form\SectorExperienceType;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * SectorExperience controller.
 *
 * @Route("/entity/experience")
 */
class SectorExperienceController extends Controller
{
    /**
     * @Route("/export", name="entity_sector_experience_export")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("GET")
     */
    public function exportAction()
    {
        $exporter = $this->get('wanawork.sector_experience_trader');
        $data = $exporter->export();
    
        $file = sys_get_temp_dir() . '/' . uniqid();
        $fp = fopen($file, 'w');
        fwrite($fp, prettyPrint(json_encode($data)));
        fclose($fp);
    
        $response = new BinaryFileResponse(
            $file, $status = 200, $headers = array(), $public = false,
            $contentDisposition = null, $autoEtag = false, $autoLastModified = true
        );
        $response->headers->set('Content-Type', 'application/json');
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, "sector_experience_".date('Y_m_d_H_i').".json");
        
        return $response;
    }

    /**
     * Lists all SectorExperience entities.
     *
     * @Route("/", name="entity_sector_experience")
     * @Method("GET")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('WanaworkUserBundle:SectorExperience')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new SectorExperience entity.
     *
     * @Route("/", name="entity_sector_experience_create")
     * @Method("POST")
     * @Template("WanaworkUserBundle:SectorExperience:new.html.twig")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function createAction(Request $request)
    {
        $id = $this->getDoctrine()->getRepository('WanaworkUserBundle:SectorExperience')->findNextId();
        $entity = new SectorExperience($id, '');
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('entity_sector_experience'));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a SectorExperience entity.
    *
    * @param SectorExperience $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(SectorExperience $entity)
    {
        $form = $this->createForm(new SectorExperienceType(), $entity, array(
            'action' => $this->generateUrl('entity_sector_experience_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new SectorExperience entity.
     *
     * @Route("/new", name="entity_sector_experience_new")
     * @Method("GET")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function newAction()
    {
        $id = $this->getDoctrine()->getRepository('WanaworkUserBundle:SectorExperience')->findNextId();
        $entity = new SectorExperience($id, '');
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a SectorExperience entity.
     *
     * @Route("/{id}", name="entity_sector_experience_show")
     * @Method("GET")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WanaworkUserBundle:SectorExperience')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SectorExperience entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing SectorExperience entity.
     *
     * @Route("/{id}/edit", name="entity_sector_experience_edit")
     * @Method("GET")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WanaworkUserBundle:SectorExperience')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SectorExperience entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a SectorExperience entity.
    *
    * @param SectorExperience $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(SectorExperience $entity)
    {
        $form = $this->createForm(new SectorExperienceType(), $entity, array(
            'action' => $this->generateUrl('entity_sector_experience_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing SectorExperience entity.
     *
     * @Route("/{id}", name="entity_sector_experience_update")
     * @Method("PUT")
     * @Template("WanaworkUserBundle:SectorExperience:edit.html.twig")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WanaworkUserBundle:SectorExperience')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SectorExperience entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->persist($editForm->getData());
            $em->flush();

            return $this->redirect($this->generateUrl('entity_sector_experience'));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a SectorExperience entity.
     *
     * @Route("/{id}", name="entity_sector_experience_delete")
     * @Method("DELETE")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('WanaworkUserBundle:SectorExperience')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find SectorExperience entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('entity_sector_experience'));
    }

    /**
     * Creates a form to delete a SectorExperience entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('entity_sector_experience_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
