<?php
namespace Wanawork\UserBundle\Controller;

use Wanawork\UserBundle\Entity\TemporaryAvatar;
use Wanawork\UserBundle\Entity\CvRequest;
use Wanawork\UserBundle\Form\Type\EmployeeProfileType;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Symfony\Component\Security\Acl\Domain\RoleSecurityIdentity;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Model\AclInterface;
use Wanawork\UserBundle\Entity\CV;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Wanawork\UserBundle\Entity\Role;
use Wanawork\UserBundle\Entity\User;
use Symfony\Component\BrowserKit\Response;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Wanawork\UserBundle\Entity\EmployeeProfile;


/**
 * EmployeeProfile controller.
 *
 */
class EmployeeProfileController extends Controller
{
    /**
     * Lists all EmployeeProfile entities.
     *
     * @Route("/candidate", name="candidate")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('WanaworkUserBundle:EmployeeProfile')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Finds and displays a EmployeeProfile entity.
     * 
     * @param int $id User ID to show
     * 
     * @Route("/candidate/{id}/show", name="candidate_show", requirements={"id" = "\d+"})
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $security = $this->get('security.context');
        $userRepository = $em->getRepository('WanaworkUserBundle:User');
        
        $userEntity = $userRepository->find($id);
        
        // No such user? => 404
        if(!$userEntity instanceof User) {
        	throw $this->createNotFoundException('Candidate profile not found');
        }
        
        $security = $this->get('security.context');
        if(!$security->isGranted('VIEW', $userEntity)) {
        	throw new AccessDeniedException();
        }
        
        $profileEntity = $userEntity->getEmployeeProfile();
        
        // No profile (nothing to view) AND no right to create a new profile on this user?
        if((!$profileEntity instanceof EmployeeProfile && 
        		!$security->isGranted('CREATE', new EmployeeProfile())) || 
        	(!$profileEntity instanceof EmployeeProfile && 
        			!$security->isGranted('EDIT', $userEntity))) {
        	
        	throw $this->createNotFoundException('Candidate profile not found');
        }
        
        // Profile exists, but no view permission? => 403
        if($profileEntity && !$security->isGranted('VIEW', $profileEntity)) {
        	throw new AccessDeniedException("You don't have permission to view this candidate's profile");
        }
        
        if(!$profileEntity) {
        	return $this->forward('WanaworkUserBundle:EmployeeProfile:new', array(
        		'id' => $userEntity->getId()
        	));
        }
        
        return array(
        	'user' => $userEntity,
        );
    }

    /**
     * Displays a form to create a new EmployeeProfile entity.
     *
     * @Route("/candidate/{id}/new", name="candidate_new")
     * @Template()
     * @Secure(roles="ROLE_EMPLOYEE")
     */
    public function newAction($id)
    {
    	$em = $this->getDoctrine()->getManager();
        $security = $this->get('security.context');
        $userRepository = $em->getRepository('WanaworkUserBundle:User');
        
        $userEntity = $userRepository->find($id);
        
        // No such user? => 404
        if(!$userEntity instanceof User) {
        	throw $this->createNotFoundException('Candidate profile not found');
        }
        
        if(!$security->isGranted('CREATE', new EmployeeProfile())) {
        	throw new AccessDeniedException("You don't have permission to access this page");
        }
        
        if(!$security->isGranted('EDIT', $userEntity)) {
        	throw new AccessDeniedException("You don't have permission to access this page");
        }
    	
        // Profile already exists? redirect to edit page
    	if($userEntity->getEmployeeProfile()) {
    		return 
    		$this->forward('WanaworkUserBundle:EmployeeProfile:edit', array(
    			'id' => $userEntity->getEmployeeProfile()->getId()
    		));
    		//return $this->redirect($this->generateUrl('candidate_edit', array('id' => $userEntity->getEmployeeProfile()->getId())));
    	}
    	
    	$profileEntity = new EmployeeProfile();
    	$profileEntity->setDob(new \DateTime('01-01-1990'));
    	$profileEntity->setName($userEntity->getName());
        $form  = $this->createForm(new EmployeeProfileType(), $profileEntity);
        return array(
        	'user' => $userEntity,
            'form'   => $form->createView(),
        	'emptyProfile' => new EmployeeProfile()
        );
    }
    
    /**
     * Creates a new EmployeeProfile entity.
     *
     * @Route("/candidate/{id}/create.{_format}", name="candidate_create", defaults={"_format"="html"}, requirements={"_format"="html|json"} )
     * @Method("POST")
     * @Template("WanaworkUserBundle:EmployeeProfile:new.html.twig")
     * @Secure(roles="ROLE_EMPLOYEE")
     */
    public function createAction(Request $request, $id)
    {
    	$em = $this->getDoctrine()->getManager();
        $security = $this->get('security.context');
        $userRepository = $em->getRepository('WanaworkUserBundle:User');
        
        $userEntity = $userRepository->find($id);
        
        // No such user? => 404
        if(!$userEntity instanceof User) {
        	throw $this->createNotFoundException('Candidate profile not found');
        }
        
        if(!$security->isGranted('CREATE', new EmployeeProfile())) {
        	throw new AccessDeniedException("You don't have permission to access this page");
        }
        
        // Profile already exists? redirect to edit page
        if($userEntity->getEmployeeProfile()) {
        	return; // @todo fix
        	//return $this->redirect($this->generateUrl('candidate_edit', array('id' => $userEntity->getEmployeeProfile()->getId())));
        }
    	
    	$employeeService = $this->get('wanawork.employee_service');
    	
        $form = $this->createForm(new EmployeeProfileType());
        $form->bind($request);

        $tempAvatarPath = null;
        $profile = null;
        if ($form->isValid()) {
        	
        	$profile = $form->getData();
        	$employeeService->createProfile($profile, $this->getUser(), $form->get('temp_avatar')->getData());
        	
            $this->get('session')->getFlashBag()->add('messages', 'Profile Saved');
            $nextTo = $request->attributes->get('nextTo') . '?profile=' . $profile->getId();
            return $this->redirect($nextTo);
        } else {
        	$temporaryAvatarid = $form->get('temp_avatar')->getData();
        	if(preg_match('/^\d$/', $temporaryAvatarid)) {
        		$temporaryAvatar = 
        		$em->getRepository('WanaworkUserBundle:TemporaryAvatar')->findOneBy(array(
        			'id' => $temporaryAvatarid,
        			'owner' => $userEntity		
        		));
        		if($temporaryAvatar instanceof TemporaryAvatar) {
        			$tempAvatarPath = $temporaryAvatar->getWebPath();
        		}
        	}
        }

        return array(
            'user' => $userEntity,
            'form'   => $form->createView(),
        	'emptyProfile' => new EmployeeProfile(),
        	'tempAvatarPath' => $tempAvatarPath
        );
    }

    /**
     * Displays a form to edit an existing EmployeeProfile entity.
     *
     * @Route("/candidate/{id}/edit", name="candidate_edit")
     * @Template()
     * @Secure(roles="ROLE_EMPLOYEE")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $profile = $em->getRepository('WanaworkUserBundle:EmployeeProfile')->find($id);

        if (!$profile instanceof EmployeeProfile) {
            throw $this->createNotFoundException('Unable to find EmployeeProfile entity.');
        }
        
        $security = $this->get('security.context');
        if(!$security->isGranted('EDIT', $profile)) {
        	throw new AccessDeniedException("You don't have permission to edit this profile");
        }
        
        $editForm = $this->createForm(new \Wanawork\UserBundle\Form\Type\EmployeeProfileType(), $profile);
        return array(
        	'user' => $profile->getUser(),
            'entity'      => $profile,
            'edit_form'   => $editForm->createView()
        );
    }

    /**
     * Edits an existing EmployeeProfile entity.
     *
     * @Route("/candidate/{id}/update.{_format}", name="candidate_update", defaults={"_format"="html"}, requirements={"_format"="html|json"} )
     * @Method("POST")
     * @Secure(roles="ROLE_EMPLOYEE")
     */
    public function updateAction(Request $request, $id)
    {
    	$format = $request->getRequestFormat();
    	
    	$returnData = array('outcome' => false);
    	
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('WanaworkUserBundle:EmployeeProfile')->find($id);

        if (!$entity instanceof EmployeeProfile) {
            throw $this->createNotFoundException('Unable to find EmployeeProfile entity.');
        }
        
        $security = $this->get('security.context');
        if(!$security->isGranted('EDIT', $entity)) {
        	throw new AccessDeniedException("You don't have permission to edit this profile");
        }

        $editForm = $this->createForm(new \Wanawork\UserBundle\Form\Type\EmployeeProfileType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();
            
            if(!$this->getRequest()->isXmlHttpRequest()) {
            	$nextTo = $request->attributes->get('nextTo') . '?profile=' . $entity->getId();
            	return $this->redirect($nextTo);
//             	return $this->redirect(
//             		$this->generateUrl()	
//             	);
            	//$this->get('session')->getFlashBag()->add('messages', 'Profile Saved');
            	//return $this->redirect($this->generateUrl('candidate_show', array('id' => $entity->getUser()->getId())));
            }
            
            $returnData['outcome'] = true;
        }
        
        return
        $response = $this->render("WanaworkUserBundle:EmployeeProfile:edit.$format.twig", array(
            'user'      => $entity->getUser(),
            'edit_form'   => $editForm->createView(),
        ));
    }

    /**
     * Deletes a EmployeeProfile entity.
     *
     * @Route("/candidate/{id}/delete", name="candidate_delete")
     * @Method("POST")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('WanaworkUserBundle:EmployeeProfile')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find EmployeeProfile entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('candidate'));
    }
    
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
    
    
    /**
     * @param integer $id 	User ID (to fetch mailboxes for)
     * @param integer|null $mailboxId 	Mailbox to open as default when the page loads
     * @Route("/candidate/{id}/mailbox/{mailboxId}", name="candidate_mailbox", 
     * 	requirements={"id"="\d+", "mailboxId"="\d+"}, defaults={"mailboxId"="0"})
     * @Secure(roles="ROLE_EMPLOYEE")
     * @Template()
     */
    public function mailboxAction($id, $mailboxId) {
    	
    	$user = $this->getDoctrine()->getRepository('WanaworkUserBundle:User')->find($id);
    	if(!$user instanceof User) {
    		throw $this->createNotFoundException("Account is not found");
    	}
    	 
    	$security = $this->get('security.context');
    	if(!$security->isGranted('EDIT', $user)) {
    		throw new AccessDeniedException("You don't have permission this access this page");
    	}
    	 
    	return array(
    		'user' => $user,
    		'mailboxId' => $mailboxId
    	);
    }
}
