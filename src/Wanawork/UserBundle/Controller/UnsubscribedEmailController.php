<?php
namespace Wanawork\UserBundle\Controller;

use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Wanawork\UserBundle\Entity\UnsubscribedEmail;
use Wanawork\UserBundle\Form\UnsubscribedEmailType;

/**
 * UnsubscribedEmail controller.
 *
 * @Route("/admin/mail/blocked")
 */
class UnsubscribedEmailController extends Controller
{

    /**
     * Lists all UnsubscribedEmail entities.
     *
     * @Route("/", name="admin_blockedemail")
     * @Method("GET")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('WanaworkUserBundle:UnsubscribedEmail')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new UnsubscribedEmail entity.
     *
     * @Route("/", name="admin_blockedemail_create")
     * @Method("POST")
     * @Template("WanaworkUserBundle:UnsubscribedEmail:new.html.twig")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function createAction(Request $request)
    {
        $entity = new UnsubscribedEmail();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_blockedemail_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a UnsubscribedEmail entity.
    *
    * @param UnsubscribedEmail $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(UnsubscribedEmail $entity)
    {
        $form = $this->createForm(new UnsubscribedEmailType(), $entity, array(
            'action' => $this->generateUrl('admin_blockedemail_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new UnsubscribedEmail entity.
     *
     * @Route("/new", name="admin_blockedemail_new")
     * @Method("GET")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function newAction()
    {
        $entity = new UnsubscribedEmail();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a UnsubscribedEmail entity.
     *
     * @Route("/{id}", name="admin_blockedemail_show")
     * @Method("GET")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WanaworkUserBundle:UnsubscribedEmail')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find UnsubscribedEmail entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing UnsubscribedEmail entity.
     *
     * @Route("/{id}/edit", name="admin_blockedemail_edit")
     * @Method("GET")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WanaworkUserBundle:UnsubscribedEmail')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find UnsubscribedEmail entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a UnsubscribedEmail entity.
    *
    * @param UnsubscribedEmail $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(UnsubscribedEmail $entity)
    {
        $form = $this->createForm(new UnsubscribedEmailType(), $entity, array(
            'action' => $this->generateUrl('admin_blockedemail_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing UnsubscribedEmail entity.
     *
     * @Route("/{id}", name="admin_blockedemail_update")
     * @Method("PUT")
     * @Template("WanaworkUserBundle:UnsubscribedEmail:edit.html.twig")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WanaworkUserBundle:UnsubscribedEmail')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find UnsubscribedEmail entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('admin_blockedemail_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a UnsubscribedEmail entity.
     *
     * @Route("/{id}", name="admin_blockedemail_delete")
     * @Method("DELETE")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('WanaworkUserBundle:UnsubscribedEmail')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find UnsubscribedEmail entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_blockedemail'));
    }

    /**
     * Creates a form to delete a UnsubscribedEmail entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_blockedemail_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
