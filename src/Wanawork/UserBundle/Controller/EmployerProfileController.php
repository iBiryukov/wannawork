<?php
namespace Wanawork\UserBundle\Controller;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Wanawork\UserBundle\Entity\User;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Wanawork\UserBundle\Entity\EmployerProfile;
use Wanawork\UserBundle\Form\EmployerProfileType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Wanawork\UserBundle\Entity\Billing\VerificationOrder;

/**
 * EmployerProfile controller.
 *
 * @Route("/company")
 */
class EmployerProfileController extends Controller
{
    /**
     * Lists all EmployerProfile entities.
     *
     * @Route("/", name="company")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function indexAction()
    {
        $dql = "SELECT employer FROM WanaworkUserBundle:EmployerProfile employer ORDER BY employer.id DESC";
        
        $query = $this->getDoctrine()->getManager()->createQuery($dql);
        
        $perPage = 10;
        $pageNumber = $this->get('request')->query->get('page', 1);
        
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate($query, $pageNumber, $perPage);
        
        
        return array(
            'employers' => $pagination,
        );
    }

    /**
     * Displays a form to create a new EmployerProfile entity.
     *
     * @Route("/new", name="company_new")
     * @Template()
     * @Secure(roles="ROLE_USER")
     */
    public function newAction()
    {
        $user = $this->getUser();
		
        $employerProfile = new EmployerProfile();
        $employerProfile->setContactName($user->getName());
        $employerProfile->addUser($user, $isAdmin = true);
        $form  = $this->createForm(new EmployerProfileType(), $employerProfile);
        
        if ($this->getRequest()->isMethod('POST')) {
            $form->bind($this->getRequest());
            
            if ($form->isValid()) {
                $employerService = $this->get('wanawork.employer_service');
                $employerService->createProfile($employerProfile, $user);
                return $this->redirect($this->generateUrl('company_show', array('slug' => $employerProfile->getSlug())));
            }
        }

        return array(
        	'user' => $user,
            'employerProfile' => $employerProfile,
            'form'   => $form->createView(),
        );
    }

//     public function createAction($id, Request $request)
//     {
//     	$user = $this->getDoctrine()->getRepository('WanaworkUserBundle:User')->find($id);
//     	if(!$user instanceof User) {
//     		throw $this->createNotFoundException("Account is not found");
//     	}
    	
//     	$security = $this->get('security.context');
//     	if(!$security->isGranted('EDIT', $user)) {
//     		throw new AccessDeniedException("You don't have permission this access this page");
//     	}
    	
//         $entity  = new EmployerProfile();
//         $form = $this->createForm(new EmployerProfileType(), $entity);
//         $form->bind($request);

//         if ($form->isValid()) {
// 			$employerService = $this->get('wanawork.employer_service');
// 			$employerService->createProfile($entity, $user);
			
// 			$this->get('session')->getFlashBag()->add('messages', 'Profile Saved');
			
//             return $this->redirect($this->generateUrl('company_show', array('id' => $user->getId())));
//         }

//         return array(
//         	'user' => $user,
//             'entity' => $entity,
//             'form'   => $form->createView(),
//         );
//     }

    /**
     * Displays a form to edit an existing EmployerProfile entity.
     *
     * @Route("/{slug}/edit", name="company_edit")
     * @Template()
     * @Secure(roles="ROLE_EMPLOYER")
     * @ParamConverter("profile", class="WanaworkUserBundle:EmployerProfile", options={"mapping": {"slug":"slug"}})
     */
    public function editAction(EmployerProfile $profile)
    {
        $em = $this->getDoctrine()->getManager();

        $security = $this->get('security.context');
        if(!$security->isGranted('EDIT', $profile)) {
        	throw new AccessDeniedException("You don't have permission to access this page");
        }

        $editForm = $this->createForm(new EmployerProfileType(), $profile);
        
        if($this->getRequest()->isMethod('POST')) {
            $editForm->bind($this->getRequest());
            
            if ($editForm->isValid()) {
                $em->flush();
                return $this->redirect($this->generateUrl('company_show', array('slug' => $profile->getSlug())));
            }
        }
        
        return array(
            'profile'      => $profile,
            'form'   => $editForm->createView(),
        );
    }
    
    /**
     * Submit request to be verified
     * @param EmployerProfile $profile
     * @Route("/{slug}/verification-request", name="employer_verification_request")
     * @Template()
     * @Method({"GET", "POST"})
     * @Secure(roles="ROLE_USER")
     */
    public function verificationAction(EmployerProfile $profile)
    {
        $security = $this->get('security.context');
        if (!$security->isGranted("EDIT", $profile)) {
            throw new AccessDeniedException();
        }
        
        if($profile->isVerified()) {
            return $this->redirect($this->generateUrl('my_employer_profile'));
        }
        
        if ($profile->isVerificationPaid()) {
            return $this->redirect($this->generateUrl('employer_verification_request_pending', array(
            	'slug' => $profile->getSlug(),
            )));
        }
        
        return array(
        	'profile' => $profile,
        );
    }
    
    /**
     * @Route("/{slug}/verification-request", name="employer_verification_request_pending")
     * @Template()
     */
    public function verificationPendingAction(EmployerProfile $profile)
    {
        return array();
    }

//     /**
//      * Deletes a EmployerProfile entity.
//      *
//      * @Route("/{id}/delete", name="company_delete")
//      * @Method("POST")
//      * @Secure(roles="ROLE_ADMIN")
//      */
//     public function deleteAction(Request $request, $id)
//     {
//         $form = $this->createDeleteForm($id);
//         $form->bind($request);

//         if ($form->isValid()) {
//             $em = $this->getDoctrine()->getManager();
//             $entity = $em->getRepository('WanaworkUserBundle:EmployerProfile')->find($id);

//             if (!$entity) {
//                 throw $this->createNotFoundException('Unable to find EmployerProfile entity.');
//             }

//             $em->remove($entity);
//             $em->flush();
//         }

//         return $this->redirect($this->generateUrl('company'));
//     }

//     private function createDeleteForm($id)
//     {
//         return $this->createFormBuilder(array('id' => $id))
//             ->add('id', 'hidden')
//             ->getForm()
//         ;
//     }
    
//     public function profileStepAction(User $user) {
//     	$security = $this->get('security.context');
//     	$em = $this->getDoctrine()->getManager();
    	
//     	if((!$employerProfile && !$security->isGranted('EDIT', $user)) || 
//     		($employerProfile && !$security->isGranted('EDIT', $employerProfile))) {
//     		throw new AccessDeniedException('You do not have permission to access this page');
//     	}
    	
//     	if(!$employerProfile) {
//     		$employerProfile = new EmployerProfile();
//     		$employerProfile->setEmail($user->getEmail());
//     	}
    	
//     	$form = $this->createForm(new EmployerProfileType(), $employerProfile);
//     	$request = $this->getRequest();
//     	if($request->isMethod('POST')) {
//     		$form->bind($request);
//     		if($form->isValid()) {
    			
    			
//     			if(!$user->getEmployerProfile()) {
//     				$employerService = $this->get('wanawork.employer_service');
//     				$employerService->createProfile($form->getData(), $user);
//     			} else {
//     				$user->setEmployerProfile($employerProfile);
//     				$em->persist($employerProfile);
//     				$em->flush();
//     			}
//     			return $this->redirect($this->generateUrl('employer_profile_saved'));
//     		}
//     	}
    	
//     	return array(
//     		'form' => $form->createView(),
//     		'user' => $user,
//     		'path' => $this->generateUrl('employer_step2', array('id' => $user->getId()))
//     	);
//     }
    
//     /**
//      * @Route("/profile-saved", name="employer_profile_saved")
//      * @Template()
//      */
//     public function profileSavedStepAction() {
//     	return array(
    			
//     	);
//     }
    
    /**
     * @Route("/{slug}/mark-verified", name="company_mark_verified")
     * @Method("POST")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function markVerifiedAction(EmployerProfile $profile)
    {
        $profile->setIsVerified(true);
        $profile->setVerifiedBy($this->getUser());
        $this->getDoctrine()->getManager()->flush();
        
        return $this->redirect($this->generateUrl('company'));
    }
    
    /**
     * @Route("/{slug}/revoke-verification", name="company_mark_unverified")
     * @Method("POST")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function revokeVerificationAction(EmployerProfile $profile)
    {
        $profile->setIsVerified(false);
        $profile->setVerifiedBy(null);
        $this->getDoctrine()->getManager()->flush();
    
        return $this->redirect($this->generateUrl('company'));
    }
    
    /**
     * Finds and displays a EmployerProfile entity.
     *
     * @Route("/{slug}", name="company_show")
     * @ParamConverter("profile", class="WanaworkUserBundle:EmployerProfile", options={"mapping": {"slug":"slug"}})
     * @Template()
     */
    public function showAction(EmployerProfile $profile)
    {
        $security = $this->get('security.context');
    
        return array(
            'profile' => $profile,
        );
    }
    
}
