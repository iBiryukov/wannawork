<?php
namespace Wanawork\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Wanawork\UserBundle\Entity\JobSpec;
use Wanawork\UserBundle\Form\JobSpecType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Wanawork\UserBundle\Entity\EmployerProfile;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Wanawork\UserBundle\Entity\SectorJob;

/**
 * JobSpec controller.
 *
 * @Route("/jobs")
 */
class JobSpecController extends Controller
{
    /**
     * Lists all JobSpec entities.
     *
     * @Route("/", name="jobs")
     * @Method("GET")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function indexAction()
    {
       $dql = "SELECT job FROM WanaworkUserBundle:JobSpec job ORDER BY job.createdOn DESC";
        
        $query = $this->getDoctrine()->getEntityManager()->createQuery($dql);
        
        $perPage = 10;
        $pageNumber = $this->get('request')->query->get('page', 1);
        
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate($query, $pageNumber, $perPage);
        
        return array(
			'pagination' => $pagination,
        );
    }
    
    /**
     * Lists all JobSpec entities that belong to the given employer
     *
     * @Route("/company/{id}", name="jobs_employer")
     * @Method("GET")
     * @Template()
     * @Secure(roles="ROLE_USER")
     */
    public function employersAction(EmployerProfile $profile)
    {
        $security = $this->get('security.context');
        if(!$security->isGranted('VIEW', $profile)) {
            throw new AccessDeniedException();
        }
        
        $jobs = $profile->getJobSpecs();
        
        return array(
        	'jobs' => $jobs,
        );
    }
    

    /**
     * Displays a form to create a new JobSpec entity.
     *
     * @Route("/new.{_format}", name="jobs_new", defaults={"_format"="html"}, requirements={"_format"="html|json"})
     * @Template()
     * @Secure(roles="ROLE_EMPLOYER")
     */
    public function newAction()
    {
        $profile = $this->getUser()->getDefaultProfile();
        if (!$profile instanceof EmployerProfile) {
            throw new AccessDeniedException();
        }
        
        $job = new JobSpec();
        $job->setEmployer($profile);
        $job->setUser($this->getUser());
        
        $form = $this->createForm(new JobSpecType(), $job);
        
        if ($this->getRequest()->isMethod('POST')) {
            $form->handleRequest($this->getRequest());
            
            if($form->isValid()) {
                $jobService = $this->get('wanawork.job_service');
                $jobService->createJob($job);
                
                $response = $this->getRequest()->getRequestFormat() === 'html' ?
                    $this->redirect($this->generateUrl('my_job_list')) : new JsonResponse();
                
                $response->setStatusCode(201);
                $response->headers->set('ww_entity_id', $job->getId());
                return $response;
            }
        }

        return array(
            'form'   => $form->createView(),
        );
    }
    
    /**
     * Displays a form to edit an existing JobSpec entity.
     *
     * @Route("/{id}/edit", name="jobs_edit", requirements={"id"="\d+"})
     * @ParamConverter("job", class="WanaworkUserBundle:JobSpec")
     * @Template()
     */
    public function editAction(JobSpec $job)
    {
        $security = $this->get('security.context');
        if (!$security->isGranted('EDIT', $job)) {
            throw new AccessDeniedException();
        }
        
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new JobSpecType(), $job);
    
        if ($this->getRequest()->isMethod('POST')) {
            $form->handleRequest($this->getRequest());
            
            if($form->isValid()) {
                $em->flush();
                return $this->redirect($this->generateUrl('my_job_list'));
            }
        }
    
        return array(
            'form'   => $form->createView(),
            'job' => $job,
        );
    }

    /**
     * Finds and displays a JobSpec entity.
     * @Route("/{id}/{title}", name="jobs_show", requirements={"id"="\d+"})
     * @ParamConverter("job", class="WanaworkUserBundle:JobSpec")
     * @Method("GET")
     * @Template()
     */
    public function showAction(JobSpec $job, $title = null)
    {
        $security = $this->get('security.context');
        
        if (!$job->isPublic() && !$security->isGranted('VIEW', $job)) {
            throw new AccessDeniedException();
        }
        
        return array(
            'job'      => $job,
        );
    }

    /**
     * Deletes a JobSpec entity.
     *
     * @Route("/{id}", name="jobs_delete")
     * @Method("DELETE")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('WanaworkUserBundle:JobSpec')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find JobSpec entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('jobs'));
    }

    /**
     * Creates a form to delete a JobSpec entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('jobs_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
    
    /**
     * @Route("/by-profession", name="jobs_by_profession")
     * @Template()
     */
    public function professionsAction()
    {
        $jobs = $this->getDoctrine()->getRepository('WanaworkUserBundle:SectorJob')->findJobsCountByProfession();
    
        return array(
            'jobs' => $jobs,
        );
    }
    
    /**
     * @Route("/recent", name="jobs_recent")
     * @Template()
     */
    public function recentJobsAction()
    {
        $dql =
        "SELECT job FROM WanaworkUserBundle:JobSpec job
         WHERE job.public=:public
         ORDER BY job.createdOn DESC";
    
        $query = $this->getDoctrine()->getEntityManager()->createQuery($dql);
        $query->setParameter('public', true);
    
        $perPage = 10;
        $pageNumber = $this->get('request')->query->get('page', 1);
    
        $paginator = $this->get('knp_paginator');
        $jobs = $paginator->paginate($query, $pageNumber, $perPage);
    
        return array(
            'jobs' => $jobs,
        );
    }
    
    /**
     * @Route("/jobSpecsStatistics", name="jobSpecs_statistics")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function jobSpecsStatisticsAction()
    {
        $dateRegex = '/^2\d{3}\-\d{2}\-\d{2}\s\d{2}:\d{2}$/';
        $allowedGroupings = array('day', 'month');
    
        $request = $this->getRequest();
        $startDate = $request->query->get('start-date');
        $finishDate = $request->query->get('finish-date');
        $canReset = false;
    
    
        if ($startDate === null || !preg_match($dateRegex, $startDate) || strtotime($startDate) === false) {
            $startDate = date('Y-m-01 00:00');
        }
    
        if ($finishDate === null || !preg_match($dateRegex, $finishDate) || strtotime($finishDate) === false) {
            $finishDate = date('Y-m-d H:i');
        }
    
        $em = $this->getDoctrine()->getManager();
        $repos = $em->getRepository('Wanawork\UserBundle\Entity\JobSpec');
        
        $numberOfJobs           = $repos->findNumberofJobs($startDate, $finishDate);
        $mostUsedProfession     = $repos->findMostUsedProfession($startDate, $finishDate);
        $mostUsedIndustry       = $repos->findMostUsedIndustry($startDate, $finishDate);
        $mostUsedLocation       = $repos->findMostUsedLocation($startDate, $finishDate);
        $mostUsedPosition       = $repos->findMostUsedPosition($startDate, $finishDate);
        $mostUsedEducationLevel = $repos->findMostUsedEducationLevel($startDate, $finishDate);
        $mostUsedLanguage       = $repos->findMostUsedLanguage($startDate, $finishDate);
        $mostUsedExperience     = $repos->findMostUsedExperience($startDate, $finishDate);
        
        return $this->render('WanaworkUserBundle:JobSpec:jobspecs-statistics.html.twig', array(
            'start_date'                => $startDate,
            'finish_date'               => $finishDate,
            'numberOfJobsData'          => $numberOfJobs,
            'mostUsedProfessionData'    => $mostUsedProfession,
            'mostUsedIndustryData'      => $mostUsedIndustry,
            'mostUsedLocationData'      => $mostUsedLocation,
            'mostUsedPositionData'      => $mostUsedPosition,
            'mostUsedEducationLevelData'=> $mostUsedEducationLevel,
            'mostUsedLanguageData'      => $mostUsedLanguage,
            'mostUsedExperienceData'    => $mostUsedExperience,
        ));
    }

    /**
     * @Route("/{profession_slug}", name="jobs_for_profession")
     * @ParamConverter("profession", class="WanaworkUserBundle:SectorJob", options={"mapping": {"profession_slug":"slug"}})
     * @Template()
     */
    public function jobsForProfessionAction(SectorJob $profession)
    {
        $dql =
        "SELECT job FROM WanaworkUserBundle:JobSpec job
         WHERE job.profession=:profession and job.public=:public
         ORDER BY job.createdOn DESC";
    
        $query = $this->getDoctrine()->getEntityManager()->createQuery($dql);
        $query->setParameters(array(
            'profession' => $profession,
            'public' => true,
        ));
    
        $perPage = 10;
        $pageNumber = $this->get('request')->query->get('page', 1);
    
        $paginator = $this->get('knp_paginator');
        $jobs = $paginator->paginate($query, $pageNumber, $perPage);
    
        return array(
            'jobs' => $jobs,
            'profession' => $profession,
        );
    }
}
