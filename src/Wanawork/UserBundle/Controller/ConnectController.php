<?php
namespace Wanawork\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpFoundation\Response;
use JMS\SecurityExtraBundle\Annotation\Secure;
use ZendService\Twitter\Twitter;

/**
 * Controller to connect user's social accounts 
 * to their wannawork account
 * 
 * @author Ilya Biryukov <ilya@wannawork.ie>
 * @Route("/social-connect")
 */
class ConnectController extends Controller
{

    const TWITTER_REQUEST_TOKEN_SESSION_KEY = 'twitter-request-tokey';
    const TWITTER_REDIRECT_AFTER_CONNECT = 'twitter-redirect-after-connect';
    
    /**
     * @Route("/twitter/start", name="connect_twitter_begin")
     * @Secure(roles="ROLE_USER")
     */    
    public function twitterBeginAction()
    {
        $redirectTo = $this->getRequest()->query->get('redirect');
        if ($redirectTo === null) {
            $redirectTo = $this->generateUrl('wanawork_main_homepage');
        }
        
        $this->getRequest()->getSession()->set(self::TWITTER_REDIRECT_AFTER_CONNECT, $redirectTo);
        
        $consumer = $this->getTwitterConsumer();
        $httpClient = new \Zend\Http\Client(null, $this->container->getParameter('zend_http_client_config'));
        $consumer->setHttpClient($httpClient);
        
        try {
            $requestToken = $consumer->getRequestToken();
            if ($requestToken->getResponse()->getStatusCode() !== 200) {
                throw new \Exception("Unable to get the request token");
            }
            $this->getRequest()->getSession()->set(self::TWITTER_REQUEST_TOKEN_SESSION_KEY, $requestToken);
            
            return $this->redirect($consumer->getRedirectUrl());
        } catch (\Exception $e) {
            $this->get('logger')->alert("Unable to get a request token", array(
            	'exception' => $e->getMessage(),
                'user' => $this->getUser()->getId(),
            ));
            throw $e;
        }        
    }
    
    /**
     * @Route("/twitter/finish", name="connect_twitter_finish")
     * @Secure(roles="ROLE_USER")
     */
    public function twitterFinishAction()
    {
        $request = $this->getRequest();
        $returnedRequestTokenString = $request->query->get('oauth_token'); 
        $storedRequestToken = $request->getSession()->get(self::TWITTER_REQUEST_TOKEN_SESSION_KEY);

        if ($storedRequestToken instanceof \ZendOAuth\Token\Request && $returnedRequestTokenString !== null && 
            $storedRequestToken->getToken() === $returnedRequestTokenString) {
            
            try {
                $consumer = $this->getTwitterConsumer();
                $httpClient = new \Zend\Http\Client(null, $this->container->getParameter('zend_http_client_config'));
                $consumer->setHttpClient($httpClient);
                
                $accessToken = $consumer->getAccessToken($request->query->all(), $storedRequestToken);

                $twitterClient = 
                $accessToken->getHttpClient(array(
                    'consumerKey'      =>  $this->container->getParameter('twitter_api_key'),
                    'consumerSecret'   =>  $this->container->getParameter('twitter_api_secret'),
                ), null, $this->container->getParameter('zend_http_client_config'));
                
                $twitterClient->setMethod('GET');
                $twitterClient->setUri('https://api.twitter.com/1.1/account/verify_credentials.json');
                $twitterClient->setParameterGet(array(
                	'skip_status' => true,
                    'include_entities' => true,
                ));
                $response = $twitterClient->send();
                
                if ($response->getStatusCode() !== 200) {
                    throw new \Exception("Unable to verify account creditials: " . $response->getBody());
                }
                
                $userData = json_decode($response->getBody(), true);
                
                if (!$userData) {
                    throw new \Exception("Unable to fetch user data");
                }
                
                $id = $userData['id_str'];
                $username = $userData['screen_name'];
                
                $this->getUser()->addTwitterAccount($id, $username, $accessToken);
                $this->getDoctrine()->getManager()->flush();
                
                $redirectTo = $this->getRequest()->getSession()->get(self::TWITTER_REDIRECT_AFTER_CONNECT);
                if ($redirectTo === null) {
                    $redirectTo = $this->generateUrl('wanawork_main_homepage');
                }
                
                return $this->redirect($redirectTo);
                
            } catch (\Exception $e) {
                $this->get('logger')->alert("Failed to get an access token", array(
                	'exception' => $e->__toString(),
                    'user' => $this->getUser()->getId(),
                ));
                throw $e;
            }
            
        } else {
            return $this->redirect($this->generateUrl('wanawork_main_homepage'));
        }
                
    }
    
    /**
     * Generate Twitter consumer (oauth client for twitter)
     * 
     * @return \ZendOAuth\Consumer
     */
    private function getTwitterConsumer()
    {
        $config = array(
            'consumerKey'      =>  $this->container->getParameter('twitter_api_key'),
            'consumerSecret'   =>  $this->container->getParameter('twitter_api_secret'),
            'callbackUrl'      =>  $this->generateUrl('connect_twitter_finish', array(), UrlGeneratorInterface::ABSOLUTE_URL),
            'siteUrl'          =>  'https://api.twitter.com/oauth/request_token',
            'authorizeUrl'     =>  'https://api.twitter.com/oauth/authenticate',
            'accessTokenUrl'   =>  'https://api.twitter.com/oauth/access_token',
        );
        
        return new \ZendOAuth\Consumer($config);
    }
}