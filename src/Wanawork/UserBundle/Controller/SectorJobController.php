<?php

namespace Wanawork\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Wanawork\UserBundle\Entity\SectorJob;
use Wanawork\UserBundle\Form\SectorJobType;
use Wanawork\UserBundle\Entity\Sector;
use Wanawork\UserBundle\Form\SectorType;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * SectorJob controller.
 *
 * @Route("/profession")
 */
class SectorJobController extends Controller
{
    /**
     * Creates a new SectorJob entity.
     *
     * @Route("/.{_format}", name="profession_create", requirements={"_format"="json"}, defaults={"_format"="json"})
     * @Method("POST")
     * @Template("WanaworkUserBundle:SectorJob:new.html.twig")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function createAction($_format)
    {
        $request = $this->getRequest();
        
        $id = $this->getDoctrine()->getRepository('WanaworkUserBundle:SectorJob')->findNextId();
        $profession = new SectorJob($id, '');
        $form = $this->createCreateForm($profession);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($profession);
            $em->flush();
            return
            new JsonResponse(null, 201, array(
                'ww_resource' => $this->generateUrl('profession_show', array(
                    'id' => $profession->getId(),
                ))
            ));
        }
    
        return $this->render("WanaworkUserBundle:SectorJob:new.{$_format}.twig", array(
            'profession' => $profession,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a SectorJob entity.
    *
    * @param SectorJob $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(SectorJob $entity)
    {
        $form = $this->createForm(new SectorJobType(), $entity, array(
            'action' => $this->generateUrl('profession_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new SectorJob entity.
     *
     * @Route("/new", name="profession_new")
     * @Method("GET")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function newAction()
    {
        $id = $this->getDoctrine()->getRepository('WanaworkUserBundle:SectorJob')->findNextId();
        $profession = new SectorJob($id, '');
        $form   = $this->createCreateForm($profession);

        return array(
            'profession' => $profession,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a SectorJob entity.
     *
     * @Route("/{id}.{_format}", name="profession_show", defaults={"_format"="json"}, 
     *   requirements={"id"="\d+", "_format"="json"})
     * @Method("GET")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function showAction(SectorJob $profession, $_format)
    {
        return array(
            'profession' => $profession,
        );
    }

    /**
     * Displays a form to edit an existing SectorJob entity.
     *
     * @Route("/{id}/edit.{_format}", name="profession_edit", defaults={"_format"="html"}, 
     *   requirements={"id"="\d+", "_format"="html"})
     * @Method("GET")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function editAction(SectorJob $profession, $_format)
    {
        $editForm = $this->createEditForm($profession);
        $deleteForm = $this->createDeleteForm($profession->getId());
        
        return array(
            'profession'      => $profession,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a SectorJob entity.
    *
    * @param SectorJob $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(SectorJob $entity)
    {
        $form = $this->createForm(new SectorJobType(), $entity, array(
            'action' => $this->generateUrl('profession_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));
        $form->add('cancel', 'button', array(
            'label' => 'Cancel',
            'attr' => array(
                'class' => 'cancel'
            ),
        ));

        return $form;
    }
    /**
     * Edits an existing SectorJob entity.
     *
     * @Route("/{id}.{_format}", name="profession_update", requirements={"id"="\d+", "_format"="json"}, 
     *   defaults={"_format"="json"})
     * @Method("PUT")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function updateAction(SectorJob $profession, $_format)
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $editForm = $this->createEditForm($profession);
        $editForm->handleRequest($request);
        $deleteForm = $this->createDeleteForm($profession->getId());

        if ($editForm->isValid()) {
            $em->flush();
            return new JsonResponse(null, 204, array(
            	'ww_resource' => $this->generateUrl('profession_show', array('id' => $profession->getId())),
            ));
        }

        return 
        $this->render("WanaworkUserBundle:SectorJob:edit.{$_format}.twig",array(
            'profession'      => $profession,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    
    /**
     * Deletes a Qualification entity.
     *
     * @Route("/{id}", name="profession_delete")
     * @Method("DELETE")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);
    
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('WanaworkUserBundle:SectorJob')->find($id);
    
            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Profession entity.');
            }
    
            $em->remove($entity);
            $em->flush();
            
            return new Response(null, 204);
        }
    }
    
    /**
     * Creates a form to delete a Qualification entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
        ->setAction($this->generateUrl('profession_delete', array('id' => $id)))
        ->setMethod('DELETE')
        ->add('submit', 'submit', array(
            'label' => 'Delete',
            'attr' => array(
        	    'class' => 'delete',
            )
        ))
        ->getForm()
        ;
    }
}
