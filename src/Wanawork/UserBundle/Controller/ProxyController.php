<?php
namespace Wanawork\UserBundle\Controller;

use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Wanawork\UserBundle\Entity\EmployerProfile;
use Wanawork\UserBundle\Entity\CvRequest;
use Wanawork\UserBundle\Entity\EmployeeProfile;
use Wanawork\UserBundle\Entity\Role;

/**
 *
 * @Route("/my")
 */
class ProxyController extends Controller
{
    /**
     * @Route("/account", name="my_account")
     * @Secure(roles="ROLE_USER")
     */
    public function accountAction()
    {
        return 
        $this->forward('WanaworkUserBundle:User:show', array(
        	'id' => $this->getUser()->getId(),
        ));
    }
    
    /**
     * @Route("/account/receipts", name="my_account_receipts")
     * @Secure(roles="ROLE_USER")
     */
    public function receiptsAction()
    {
        return
        $this->forward('WanaworkUserBundle:User:receipts', array(
            'id' => $this->getUser()->getId(),
        ));
    }
    
    /**
     * Display employer's profile
     * @Route("/profile", name="my_employer_profile")
     * @Secure(roles="ROLE_USER")
     */
    public function profileAction()
    {
        $user = $this->getUser();
        if ($user->getDefaultProfile() instanceof EmployerProfile) {
            return $this->forward('WanaworkUserBundle:EmployerProfile:show', array(
                'slug' => $user->getDefaultProfile()->getSlug(),   
            )); 
        }
        
        if ($user->hasRoleName(Role::EMPLOYER)) {
            return $this->redirect($this->generateUrl('company_new'));
        }
        
        throw $this->createNotFoundException('Profile not found');
    }
    
    /**
     * Display list of jobs
     * @Route("/jobs", name="my_job_list")
     * @Secure(roles="ROLE_USER")
     */
    public function jobSpecsAction()
    {
        $user = $this->getUser();
        if ($user->getDefaultProfile() instanceof EmployerProfile) {
            return $this->forward('WanaworkUserBundle:JobSpec:employers', array(
                'id' => $user->getDefaultProfile()->getId(),
            ));
        }
        
        if ($user->hasRoleName(Role::EMPLOYER)) {
            return $this->render('WanaworkUserBundle:Proxy:no-job-specs.html.twig');
        }
        
        throw $this->createNotFoundException('Profile not found');
    }
    
    /**
     * Display cv requests sent by employer
     * @Route("/cvs/{status}", name="my_shortlisted_cvs", requirements={"status"="\w+"})
     * @Secure(roles="ROLE_USER")
     */
    public function shortelistedCvsAction($status = null)
    {
        if ($status !== null && !array_key_exists($status, CvRequest::$statuses)) {
            throw $this->createNotFoundException();
        }
        
        $user = $this->getUser();
        if ($user->getDefaultProfile() instanceof EmployerProfile) {
            return $this->forward('WanaworkUserBundle:CvRequest:list', array(
                'id' => $user->getDefaultProfile()->getId(),
                'status' => $status,
            ));
        }
        
        if ($user->hasRoleName(Role::EMPLOYER)) {
            return $this->render('WanaworkUserBundle:Proxy:no-cv-requests-employer.html.twig');
        }
        
        throw $this->createNotFoundException('Profile not found');
    }
    
    /**
     * Display cv requests sent by employer
     * @Route("/cv-requests/{status}", name="my_cv_requests", requirements={"status"="\w+"})
     * @Secure(roles="ROLE_USER")
     */
    public function shortlistRequestsAction($status = null)
    {
        if ($status !== null && !array_key_exists($status, CvRequest::$statuses)) {
            throw $this->createNotFoundException();
        }
        
        $user = $this->getUser();
        if ($user->getDefaultProfile() instanceof EmployeeProfile) {
            return $this->forward('WanaworkUserBundle:CvRequest:list', array(
                'id' => $user->getDefaultProfile()->getId(),
                'status' => $status,
            ));
        }
        
        if ($user->hasRoleName(Role::EMPLOYEE)) {
            return $this->render('WanaworkUserBundle:Proxy:no-cv-requests-employee.html.twig');
        }
        
        throw $this->createNotFoundException('Profile not found');
    }
    
    /**
     * @Route("/saved-searches", name="my_saved_searches")
     * @Secure(roles="ROLE_USER")
     */
    public function savedSeachesAction()
    {
        return $this->forward("WanaworkUserBundle:SearchNotification:savedSearches", array(
        	   'id' => $this->getUser()->getId(),
            )
        );
    }
}