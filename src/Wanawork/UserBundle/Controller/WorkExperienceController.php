<?php
namespace Wanawork\UserBundle\Controller;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Wanawork\UserBundle\Entity\CV;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Wanawork\UserBundle\Entity\WorkExperience;
use Wanawork\UserBundle\Form\WorkExperienceType;
use JMS\SecurityExtraBundle\Annotation\Secure;

/**
 * WorkExperience controller.
 *
 */
class WorkExperienceController extends Controller
{
    /**
     * Lists all WorkExperience entities.
     *
     * @Route("/work-experience", name="work_experience")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('WanaworkUserBundle:WorkExperience')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Finds and displays a WorkExperience entity.
     *
     * @Route("/work-experience/{id}/show", name="work_experience_show")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WanaworkUserBundle:WorkExperience')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find WorkExperience entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to create a new WorkExperience entity.
     *
     * @Route("/cv/{cvId}/new-work-experience", name="work_experience_new")
     * @Method("GET")
     * @Template()
     * @Secure(roles="ROLE_EMPLOYEE")
     */
    public function newAction($cvId)
    {
    	$em = $this->getDoctrine()->getManager();
    	$security = $this->get('security.context');
    	
    	$cvEntity = $em->getRepository('WanaworkUserBundle:CV')->find($cvId);
    	
    	if(!$cvEntity instanceof CV) {
    		throw $this->createNotFoundException('CV not found');
    	}
    	
    	if(!$security->isGranted('EDIT', $cvEntity)) {
    		throw new AccessDeniedException("You don't have permission to access this page");
    	}
    	
        $entity = new WorkExperience();
        $form   = $this->createForm(new WorkExperienceType(), $entity);

        return array(
            'cv' => $cvEntity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a new WorkExperience entity.
     *
     * @Route("/cv/{cvId}/create-work-experience.{_format}", name="work_experience_create", 	
     * 			defaults={"_format"="html"}, requirements={"_format"="html|json"})
     * @Method("POST")
     * @Template("WanaworkUserBundle:WorkExperience:new.html.twig")
     * @Secure(roles="ROLE_EMPLOYEE")
     */
    public function createAction(Request $request, $cvId)
    {
    	$em = $this->getDoctrine()->getManager();
    	$security = $this->get('security.context');
    	 
    	$cvEntity = $em->getRepository('WanaworkUserBundle:CV')->find($cvId);
    	 
    	if(!$cvEntity instanceof CV) {
    		throw $this->createNotFoundException('CV not found');
    	}
    	 
    	if(!$security->isGranted('EDIT', $cvEntity)) {
    		throw new AccessDeniedException("You don't have permission to access this page");
    	}
    	
    	$returnData = array('outcome' => false, 'messages' => array(), 'form' => null);
    	
        $workExperienceEntity  = new WorkExperience();
        $workExperienceEntity->setCv($cvEntity);
        
        $form = $this->createForm(new WorkExperienceType(), $workExperienceEntity);
        $form->bind($request);
        
//         if($form->get('currently_here')->getData() === true) {
//         	$workExperienceEntity->setFinish(null);
//         }

        if ($form->isValid()) {
        	$employeeService = $this->get('wanawork.employee_service');
        	$employeeService->createWorkExperience($workExperienceEntity, $cvEntity);
            
            $this->get('session')->getFlashBag()->add('messages', 'New Job Saved');
            return $this->redirect($this->generateUrl('cv_show', 
            			array('id' => $workExperienceEntity->getCv()->getId())));
        }

        return array(
            'cv' => $cvEntity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to edit an existing WorkExperience entity.
     *
     * @Route("/work-experience/{id}/edit", name="work_experience_edit")
     * @Template()
     * @Secure(roles="ROLE_EMPLOYEE, ROLE_ADMIN")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $workExperienceEntity = $em->getRepository('WanaworkUserBundle:WorkExperience')->find($id);
        
        if (!$workExperienceEntity instanceof WorkExperience) {
            throw $this->createNotFoundException('Unable to find WorkExperience entity.');
        }
        
        $security = $this->get('security.context');
        if(!$security->isGranted('EDIT', $workExperienceEntity->getCv())) {
        	throw new AccessDeniedException("You don't have permission to access this page");
        }

        $editForm = $this->createForm(new WorkExperienceType(), $workExperienceEntity);

        return array(
            'entity'      => $workExperienceEntity,
            'edit_form'   => $editForm->createView()
        );
    }

    /**
     * Edits an existing WorkExperience entity.
     *
     * @Route("/work-experience/{id}/update", name="work_experience_update")
     * @Method("POST")
     * @Template("WanaworkUserBundle:WorkExperience:edit.html.twig")
     * @Secure(roles="ROLE_EMPLOYEE")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $workExperienceEntity = $em->getRepository('WanaworkUserBundle:WorkExperience')->find($id);

        if (!$workExperienceEntity instanceof WorkExperience) {
            throw $this->createNotFoundException('Unable to find WorkExperience entity.');
        }
        
   	 	$security = $this->get('security.context');
        if(!$security->isGranted('EDIT', $workExperienceEntity->getCv())) {
        	throw new AccessDeniedException("You don't have permission to access this page");
        }

        $editForm = $this->createForm(new WorkExperienceType(), $workExperienceEntity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($workExperienceEntity);
            $em->flush();
            
            $this->get('session')->getFlashBag()->add('messages', 'Job Updated');
            return $this->redirect($this->generateUrl('cv_show', 
            			array('id' => $workExperienceEntity->getCv()->getId())));
        }

        return array(
            'entity'      => $workExperienceEntity,
            'edit_form'   => $editForm->createView(),
        );
    }

    /**
     * Deletes a WorkExperience entity.
     *
     * @Route("/work-experience/{id}/delete", name="work_experience_delete")
     * @Method("POST")
     * @Secure(roles="ROLE_EMPLOYEE")
     */
    public function deleteAction(Request $request, $id)
    {
    	$security = $this->get('security.context');
    	$em = $this->getDoctrine()->getManager();
    	
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
        	
            $workExperienceEntity = $em->getRepository('WanaworkUserBundle:WorkExperience')->find($id);

            if (!$workExperienceEntity instanceof WorkExperience) {
                throw $this->createNotFoundException('Unable to find WorkExperience entity.');
            }
            
            if(!$security->isGranted('EDIT', $workExperienceEntity->getCv())) {
            	throw new AccessDeniedException("You don't have permission to access this page");
            }
            $cvId = $workExperienceEntity->getCv()->getId();
            
            $employeeService = $this->get('wanawork.employee_service');
            $employeeService->deleteWorkExperience($workExperienceEntity);
            
            $em->remove($workExperienceEntity);
            $em->flush();
            
            $this->get('session')->getFlashBag()->add('messages', 'Job Removed');
            return $this->redirect($this->generateUrl('cv_show', array('id' => $cvId)));
        } else {
        	throw new AccessDeniedException();
        }
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
