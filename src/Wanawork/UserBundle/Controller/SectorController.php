<?php
namespace Wanawork\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Wanawork\UserBundle\Entity\Sector;
use Wanawork\UserBundle\Form\SectorType;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Sector controller.
 *
 * @Route("/sector")
 */
class SectorController extends Controller
{

    /**
     * Creates a new Sector entity.
     * 
     * @Route("/.{_format}", name="sector_create", requirements={"_format"="json"}, defaults={"_format"="json"})
     * @Method("POST")
     * 
     * @Secure(roles="ROLE_ADMIN")
     */
    public function createAction($_format)
    {
        $request = $this->getRequest();
        $sector = new Sector();
        $form = $this->createCreateForm($sector);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($sector);
            $em->flush();
            
            return 
            new JsonResponse(null, 201, array(
            	'ww_resource' => $this->generateUrl('sector_show', array(
            		'id' => $sector->getId(),
            	))
            ));
        }

        return $this->render("WanaworkUserBundle:Sector:new.{$_format}.twig", array(
            'sector' => $sector,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Sector entity.
    *
    * @param Sector $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Sector $entity)
    {
        $form = $this->createForm(new SectorType(), $entity, array(
            'action' => $this->generateUrl('sector_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Sector entity.
     *
     * @Route("/new", name="sector_new")
     * @Method("GET")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function newAction()
    {
        $id = $this->getDoctrine()->getRepository('WanaworkUserBundle:Sector')->findNextId();
        
        $sector = new Sector($id, null);
        $form   = $this->createCreateForm($sector);

        return array(
            'sector' => $sector,
            'form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Sector entity.
     *
     * @Route("/{id}/edit", name="sector_edit", requirements={"id"="\d+"})
     * @Method("GET")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function editAction(Sector $sector)
    {
        $em = $this->getDoctrine()->getManager();

        $editForm = $this->createEditForm($sector);
        $deleteForm = $this->createDeleteForm($sector->getId());

        return array(
            'sector'      => $sector,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Sector entity.
    *
    * @param Sector $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Sector $entity)
    {
        $form = $this->createForm(new SectorType(), $entity, array(
            'action' => $this->generateUrl('sector_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));
        $form->add('cancel', 'button', array(
            'label' => 'Cancel',
            'attr' => array(
        	    'class' => 'cancel'
            )
        ));

        return $form;
    }
    /**
     * Edits an existing Sector entity.
     *
     * @Route("/{id}.{_format}", name="sector_update",  requirements={"id"="\d+", "_format"="json"},
     * defaults={"_format"="json"})
     * @Method("PUT")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function updateAction(Sector $sector, $_format)
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $editForm = $this->createEditForm($sector);
        $editForm->handleRequest($request);
        $deleteForm = $this->createDeleteForm($sector->getId());

        if ($editForm->isValid()) {
            $em->flush();
            return new JsonResponse(null, 204, array(
            	'ww_resource' => $this->generateUrl('sector_show', array('id' => $sector->getId())),
            ));
        }

        return 
        $this->render("WanaworkUserBundle:Sector:edit.{$_format}.twig",array(
            'sector'      => $sector,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    
    /**
     * @Route("/{id}.{_format}", name="sector_show", requirements={"id"="\d+", "_format"="json"}, 
     * defaults={"_format"="json"})
     * @Method("GET")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function showAction(Sector $sector, $_format)
    {
        return array(
        	'sector' => $sector,
        );
    }
    
    /**
     * Creates a form to delete a Qualification entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
        ->setAction($this->generateUrl('sector_delete', array('id' => $id)))
        ->setMethod('DELETE')
        ->add('submit', 'submit', array(
            'label' => 'Delete',
            'attr' => array(
                'class' => 'delete',
            )
        ))
        ->getForm()
        ;
    }
    
    /**
     * Deletes a Qualification entity.
     *
     * @Route("/{id}", name="sector_delete", requirements={"id"="\d+"})
     * @Method("DELETE")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);
    
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('WanaworkUserBundle:Sector')->find($id);
    
            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Sector entity.');
            }
    
            $em->remove($entity);
            $em->flush();
    
            return new Response(null, 204);
        }
    }
}
