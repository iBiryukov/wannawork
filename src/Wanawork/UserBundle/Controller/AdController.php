<?php
namespace Wanawork\UserBundle\Controller;
use Wanawork\UserBundle\Form\Type\EmployeeProfileType;
use Wanawork\UserBundle\Entity\EmployeeProfile;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Wanawork\UserBundle\Entity\User;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Wanawork\UserBundle\Entity\Ad;
use Wanawork\UserBundle\Form\AdType;
use Wanawork\UserBundle\Entity\AdView;
use Wanawork\UserBundle\Entity\EmployerProfile;
use Wanawork\UserBundle\Entity\CVFile;
use Doctrine\Common\Collections\ArrayCollection;
use Wanawork\UserBundle\Entity\Sector;
use Symfony\Component\HttpFoundation\Cookie;
use Wanawork\UserBundle\Entity\TemporaryAvatar;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Process\Process;
use Wanawork\UserBundle\Entity\SectorJob;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Wanawork\UserBundle\Entity\CV;
use Wanawork\UserBundle\Entity\CVForm;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Wanawork\UserBundle\Entity\Role;

use Wanawork\UserBundle\Entity\Billing\AdOrder;
/**
 * Ad controller.
 *
 */
class AdController extends Controller
{
    /**
     * @Route("/candidates", name="ad_candidates_profession_list")
     * @Template()
     */
    public function candidateProfessionListAction()
    {
        $doctrine = $this->getDoctrine()->getManager();
        $sectorJobRepository = $doctrine->getRepository('WanaworkUserBundle:SectorJob');
        
        $professionsAggregate = $sectorJobRepository->findCandidateCountByProfession();
        
        return array(
        	'profession_aggregate' => $professionsAggregate,
        );
    }
    
    /**
     * @Route("/candidates/recent", name="ad_candidates_recent")
     * @Template()
     */
    public function recentCandidatesAction()
    {
        $doctrine = $this->getDoctrine()->getManager();
        
        $qb = $doctrine->createQueryBuilder();
        $qb->select('ad');
        $qb->from('WanaworkUserBundle:Ad', 'ad');
        $qb->where('ad.expiryDate >= :expiry_date');
        $qb->andWhere('ad.isPublished = :publish_flag');
        $qb->orderBy('ad.expiryDate', 'DESC');
         
        $qb->setParameters(array(
            'expiry_date' => new \DateTime(),
            'publish_flag' => true,
        ));
         
        $query = $qb->getQuery();
        $perPage = 10;
        $pageNumber = $this->get('request')->query->get('page', 1);
        
        $paginator = $this->get('knp_paginator');
        $ads = $paginator->paginate($query, $pageNumber, $perPage);
        
        return array(
            'ads' => $ads,
        );
    }
    
    /**
     * @Route("/candidates/{profession_slug}", name="ad_candidates_by_profession")
     * @ParamConverter("profession", class="WanaworkUserBundle:SectorJob", options={"mapping": {"profession_slug":"slug"}})
     * @Template()
     */
    public function candidatesByProfessionAction(SectorJob $profession)
    {
        $doctrine = $this->getDoctrine()->getManager();
            
        $qb = $doctrine->createQueryBuilder();
        $qb->select('ad');
        $qb->from('WanaworkUserBundle:Ad', 'ad');
        $qb->where('ad.profession=:profession');
        $qb->andWhere('ad.expiryDate >= :expiry_date');
        $qb->andWhere('ad.isPublished = :publish_flag');
        $qb->orderBy('ad.expiryDate', 'DESC');
         
        $qb->setParameters(array(
            'profession' => $profession,
            'expiry_date' => new \DateTime(),
            'publish_flag' => true,
        ));
         
        $query = $qb->getQuery();
        $perPage = 10;
        $pageNumber = $this->get('request')->query->get('page', 1);
        
        $paginator = $this->get('knp_paginator');
        $ads = $paginator->paginate($query, $pageNumber, $perPage);
        
        return array(
        	'ads' => $ads,
            'profession' => $profession,
        );
    }
    
	/**
	 * @Route("/ads", name="ad_list")
	 * @Template()
	 * @Secure(roles="ROLE_ADMIN")
	 */
	public function indexAction() 
	{
		$dql = "SELECT ad, cv, profile 
				FROM WanaworkUserBundle:Ad ad 
				LEFT JOIN ad.cv cv 
				LEFT JOIN ad.profile profile 
				ORDER BY ad.createdAt DESC";
		
		$query = $this->getDoctrine()->getManager()->createQuery($dql);
		
		$perPage = 10;
		$pageNumber = $this->get('request')->query->get('page', 1);
		
		$paginator = $this->get('knp_paginator');
		$pagination = $paginator->paginate($query, $pageNumber, $perPage);
		
		$this->get('wanawork.acl_service')->preloadAcls($pagination);
		
		$deleteForms = array();
		foreach($pagination as $p) {
			$deleteForms[] = $this->createDeleteForm($p->getId())->createView();
		}
		
		return array(
			'pagination' => $pagination,
			'delete_forms' => $deleteForms,
		);	
	}
	
    /**
     * Lists all Ad entities.
     *
     * @Route("/candidate/{userId}/ads", name="ad", requirements={"userId"="\d+"})
     * @Route("/my/ads", name="my_ads", defaults={"userId"=""})
     * @Template()
     * @Secure(roles="ROLE_EMPLOYEE")
     */
    public function userAdsAction($userId)
    {
        $em = $this->getDoctrine()->getManager();
        
        if($userId) {
        	$user = $em->getRepository('WanaworkUserBundle:User')->find($userId);
        } else {
        	$user = $this->getUser();
        }
        
        if(!$user instanceof User) {
        	throw $this->createNotFoundException("Page not found");
        }
        
        $security = $this->get('security.context');
        if(!$security->isGranted('EDIT', $user)) {
        	throw new AccessDeniedException("You don't have permission to access this page");
        }
        
        $aclManager = $this->get('wanawork.acl_service');
        $adDeleteForms = array();
        $ads = $this->getDoctrine()->getRepository('WanaworkUserBundle:Ad')->findAdsByUser($user);
        $aclManager->preloadAcls($ads);
        
        array_map(function(Ad $ad) use($security){
        	return $security->isGranted("VIEW", $ad);
        }, $ads);
        
        foreach ($ads as $ad) {
            $adDeleteForms[] = $this->createDeleteForm($ad->getId())->createView();
        }
        
        return array(
        	'user' => $user,
            'ads' => $ads,
        	'delete_forms' => $adDeleteForms
        );
    }

    /**
     * Displays a form to create a new Ad entity.
     * @Route("/new-advert", name="ad_new")
     * @Template()
     */
    public function newAction()
    {
        $user = $this->getUser();
        if ($user === null || !$user instanceof User) {
            return $this->redirect($this->generateUrl('wanawork_register'));
        }
        
    	$security = $this->get('security.context');
    	$entityManager = $this->getDoctrine()->getManager();
    	
    	$request = $this->getRequest();
    	
    	$adEntity = new Ad();
    	$profile = $user->getEmployeeProfile();
    	if(!$profile) {
    	    $profile = new EmployeeProfile();
    	    $profile->setName($user->getName());
    	    $user->setEmployeeProfile($profile);
    	    $role = $entityManager->getRepository('WanaworkUserBundle:Role')->find('ROLE_EMPLOYEE');
    	    if (!$role instanceof Role) {
    	        throw new \Exception('Unable to find candidate role');
    	    }
    	    $profile->getUser()->addRole($role);
    	    
    	    $entityManager->persist($profile);
    	}
    	$adEntity->setProfile($profile);
    	
        $form = $this->createForm(new AdType(), $adEntity, array(
        	'validation_groups' => 'step1',
            'em' => $entityManager,
        ));
        
        if($request->isMethod('POST')) {
            
            $form->bind($request);
            
            if($form->isValid()) {
                
                // ensure that the attached cv belongs to the user
                if ($adEntity->getCv() instanceof CV && !$security->isGranted("EDIT", $adEntity->getCv())) {
                    throw new AccessDeniedHttpException();
                }
                
                $duration = $this->container->getParameter('ad.standard.length_days');
                $bumps = $this->container->getParameter('ad.standard.bumps');
                $adEntity->publishStandard($duration, $bumps);
                
                $entityManager->persist($adEntity);
                $entityManager->flush();                    
                
                return $this->redirect($this->generateUrl('ad_pubslihed', ['slug' => $adEntity->getSlug()]));
            }
        }
        
        return
        $this->render('WanaworkUserBundle:Ad:new.html.twig', array(
            'form'   => $form->createView(),
            'ad' => $adEntity,
        ));
    }
    
    /**
     * @Route("/ads/{slug}/published", name="ad_pubslihed")
     * @Secure(roles="ROLE_USER")
     * @Template()
     */
    public function adPublishedAction(Ad $ad)
    {
        $security = $this->get('security.context');
        if (!$security->isGranted('EDIT', $ad)) {
            throw new AccessDeniedHttpException();
        }
        
        if (!$ad->isPublished()) {
            return $this->redirect($this->generateUrl('my_ads'));
        }
    
        return array(
            'ad' => $ad,
        );
    }
    
    /**
     * Displays a form to edit an existing Ad entity.
     *
     * @Route("/ads/{slug}/edit", name="ad_edit")
     * @Template()
     * @Secure(roles="ROLE_EMPLOYEE")
     * @ParamConverter("ad", class="WanaworkUserBundle:Ad", options={"mapping": {"slug":"slug"}})
     */
    public function editAction(Ad $ad)
    {
        $em = $this->getDoctrine()->getManager();
    
        $security = $this->get('security.context');
        if(!$security->isGranted('EDIT', $ad)) {
            throw new AccessDeniedException("You don't have permission to access this page");
        }
    
        $form = $this->createForm(new AdType(), $ad, array(
            'validation_groups' => 'step1',
            'em' => $em,
        ));
    
        if ($this->getRequest()->isMethod('POST')) {
            $form->bind($this->getRequest());
            if($form->isValid()) {
                // ensure that the attached cv belongs to the user
                if ($ad->getCv() instanceof CV && !$security->isGranted("EDIT", $ad->getCv())) {
                    throw new AccessDeniedHttpException();
                }
                
                if (!$ad->isPublished()) {
                    $duration = $this->container->getParameter('ad.standard.length_days');
                    $bumps = $this->container->getParameter('ad.standard.bumps');
                    $ad->publishStandard($duration, $bumps);
                    
                    $em->flush();
                    
                    return $this->redirect($this->generateUrl('ad_pubslihed', ['slug' => $ad->getSlug()]));
                } elseif (!$ad->isPremium()) {
                    $em->flush();
                    return $this->redirect($this->generateUrl('ad_pubslihed', ['slug' => $ad->getSlug()]));
                } else {
                    $em->flush();
                    return $this->redirect($this->generateUrl('ad_updated', ['slug' => $ad->getSlug()]));
                }
            }
        }
    
        return array(
            'ad' => $ad,
            'form'   => $form->createView(),
        );
    }
    
    /**
     * @Route("/ads/{slug}/updated", name="ad_updated")
     * @Secure(roles="ROLE_EMPLOYEE")
     * @Template()
     */
    public function adUpdatedAction(Ad $ad)
    {
        $security = $this->get('security.context');
        if (!$security->isGranted('EDIT', $ad)) {
            throw new AccessDeniedException("You don't have permission to access this page");
        }
        
        return array(
        	'ad' => $ad,
        );
    }

    /**
     * @Route("/ads/{slug}/extend", name="ad_extend")
     * @Secure(roles="ROLE_EMPLOYEE")
     * @Template()
     */
    public function extendAction(Ad $ad)
    {
        $security = $this->get('security.context');
        if (!$security->isGranted('EDIT', $ad)) {
            throw new AccessDeniedException("You don't have permission to access this page");
        }
        
        if (!$ad->isNearExpiry()) {
            return $this->render('WanaworkUserBundle:Ad:extension-too-early.html.twig');
        }
        
        $form =
        $this->createFormBuilder(array('id' => 'place_choice'))
        ->add('plan', 'choice', array(
            'choices' => array('standard' => 'standard', 'premium' => 'premium')
        ))->getForm();
        
        if ($this->getRequest()->isMethod('POST')) {
            $form->bind($this->getRequest());
        
            if ($form->isValid()) {
        
                $plan = '';
                switch ($form->get('plan')->getData()) {
                	case 'standard':
                	    $plan = Ad::PLAN_STANDARD;
                	    break;
        
                	case 'premium':
                	    $plan = Ad::PLAN_PREMIUM;
                	    break;
        
                	default:
                	    throw new \Exception('Unexpected plan selected ' . $form->get('plan')->getData());
                }
        
                if ($plan === Ad::PLAN_STANDARD) {
                    $duration = $this->container->getParameter('ad.standard.length_days');
                    $bumps = $this->container->getParameter('ad.standard.bumps');
                    $ad->publishStandard($duration, $bumps);
                    
                    $this->getDoctrine()->getManager()->flush();
                    
                    return $this->redirect(
        	           $this->generateUrl('ad_pubslihed', ['slug' => $ad->getSlug()])
                    );
                    
                    
                } else {
                    return $this->redirect($this->generateUrl('new_ad_order', array(
                        'id' => $ad->getId(),
                        'plan' => array_search($plan, Ad::getPricePlans()),
                    )));
                }
                
            }
        }
        
        return array(
            'ad' => $ad,
            'form' => $form->createView(),
        );
    }
    
    /**
     * Deletes a Ad entity.
     *
     * @Route("/ads/{id}/delete", name="ad_delete")
     * @Method("POST")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);
        
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $adEntity = $em->getRepository('WanaworkUserBundle:Ad')->find($id);

            if (!$adEntity) {
                throw $this->createNotFoundException('Unable to find Ad entity.');
            }
            
            $security = $this->get('security.context');
            if(!$security->isGranted('DELETE', $adEntity)) {
            	throw new AccessDeniedException("You don't have permission to access this page");
            }
            
            $userId = $adEntity->getCv()->getProfile()->getUser()->getId();
            
            $employeeService = $this->get('wanawork.employee_service');
            $employeeService->deleteAd($adEntity, $this->getUser());
            $this->get('session')->getFlashBag()->add('messages', 'Ad Deleted');
            
            $throwBack = $this->get('request')->query->get(
            				'throwBack', 
            				$this->generateUrl('ad', array('userId' => $userId)) 
            				);
            return $this->redirect($throwBack);
        }
		throw $this->createNotFoundException();        
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
    
    /**
     * @Route("/ads/statistics", name="all_ads_statistics")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function allAdsStatisticsAction()
    {
        $dateRegex = '/^2\d{3}\-\d{2}\-\d{2}\s\d{2}:\d{2}$/';
        $allowedGroupings = array('day', 'month');
    
        $request = $this->getRequest();
        $startDate = $request->query->get('start-date');
        $finishDate = $request->query->get('finish-date');
        $canReset = false;
    
        if ($startDate === null || !preg_match($dateRegex, $startDate) || strtotime($startDate) === false) {
            $startDate = date('Y-m-d 00:00');
        }
    
        if ($finishDate === null || !preg_match($dateRegex, $finishDate) || strtotime($finishDate) === false) {
            $finishDate = date('Y-m-d H:i');
        }
    
        $em = $this->getDoctrine()->getManager();
        $repos = $em->getRepository('Wanawork\UserBundle\Entity\Ad');
         
        $professionsByCount = $repos->findMostUsedProfessions($startDate,$finishDate);
        $industryByCount = $repos->findMostUsedIndustires($startDate,$finishDate);
        $languageByCount = $repos->findMostUsedLanguage($startDate,$finishDate);
        $locationByCount = $repos->findMostUsedLocation($startDate,$finishDate);
        $educationLevelByCount = $repos->findMostUsedEducationLevel($startDate,$finishDate);
        $experienceByCount = $repos->findMostUsedExperience($startDate,$finishDate);
        $rolesByCount = $repos->findMostUsedRole($startDate,$finishDate);
         
        return $this->render('WanaworkUserBundle:Ad:all-ads-statistics.html.twig', array(
            'start_date'     => $startDate,
            'finish_date'    => $finishDate,
            'professionsData'=> $professionsByCount,
            'industryData'   => $industryByCount,
            'languageData'   => $languageByCount,
            'locationData'   => $locationByCount,
            'educationData'  => $educationLevelByCount,
            'experienceData' => $experienceByCount,
            'rolesData'      => $rolesByCount,
        ));
    }
    
    /**
     * @Route("/ads/{slug}/download-cv", name="ad_download_cv")
     * @Secure(roles="ROLE_USER")
     */
    public function downloadCVAction(Ad $ad)
    {
        $security = $this->get('security.context');
        if (!$security->isGranted("VIEW", $ad)) {
            throw new AccessDeniedException();
        }
        
        $cv = $ad->getCv();
        if (!$cv instanceof CV) {
            throw $this->createNotFoundException();
        }
        
        $response = null;
        if ($cv instanceof CVForm) {
            $html = $this->renderView('WanaworkUserBundle:Ad:show.mini.twig', ['ad' => $ad]);
            
            $pdfFileName = $ad->getProfile()->getName() . '_' . $ad->getId() . '.pdf';
            $response =
                new Response(
                    $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
                    200,
                    array(
                    'Content-Type'          => 'application/pdf',
                    'Content-Disposition'   => 'attachment; filename="'.$pdfFileName.'"'
                        )
                );
        } elseif ($cv instanceof CVFile) {
            $response = new BinaryFileResponse($cv->getFile());
            $response->headers->set('Content-Type', $cv->getFile()->getMimeType());
            $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $cv->getFileName());
        } else {
            throw new \Exception("CV of unknow type supplied");
        }
        
        $ad->addView(AdView::VIEW_PDF, $this->getUser());
        $this->getDoctrine()->getManager()->flush();
        
        return $response;
    }
    
    /**
     * Finds and displays a Ad entity.
     * @Route("/ads/{slug}", name="ad_show")
     */
    public function showAction(Ad $adEntity)
    {
    	$em = $this->getDoctrine()->getManager();
    	$security = $this->get('security.context');
    	
    	$response = null;
    	if(!$security->isGranted('VIEW', $adEntity)) {
    	    $response = $this->render('WanaworkUserBundle:Ad:show-public.html.twig', array(
    			'ad' => $adEntity,
    		));
    	    $response->setPublic();
    	} else {
    	    
        	$deleteForm = $this->createDeleteForm($adEntity->getId());
        	$viewParams = array(
    	    	'ad'      => $adEntity,
    	    	'deleteForm' => $deleteForm->createView(),
        	);
        	
    		$adEntity->addView(AdView::VIEW_FULL, $this->getUser());
    		$em->flush();
    		$response = $this->render("WanaworkUserBundle:Ad:show.html.twig", $viewParams);
        }
        
        $eTagBits = array(
            $adEntity->getId(), 
            (string)(true === $response->headers->hasCacheControlDirective('public')),
            $adEntity->getUpdatedAt()->getTimestamp(),
            (string)$security->isGranted('VIEW', $adEntity),
        );
        
        $eTag = md5(implode('-', $eTagBits));
        $response->setEtag($eTag);
        return $response;
    }
    
    /**
     * View statistics for the ad
     * @param integer $id Ad ID
     * 
     * @Secure(roles="ROLE_EMPLOYEE")
     * @Method("GET")
     * @Route("/candidate-window/{slug}/statistics.{_format}", name="ad_stats", 
     * requirements={"_format"="html|json"}, defaults={"_format"="html"})
     * @ParamConverter("ad", class="WanaworkUserBundle:Ad", options={"mapping": {"slug":"slug"}})
     * @Template()
     */
    public function statisticsAction(Ad $ad, $_format, $title = null) {
    	$security = $this->get('security.context');
    	if(!$security->isGranted('EDIT', $ad)) {
    		throw new AccessDeniedException();
    	}
    	
    	$month = $this->getRequest()->get('month');
    	$year = $this->getRequest()->get('year');
    	$type = $this->getRequest()->get('type');
    	
    	if (!strlen($type)) {
    	    $type = null;
    	}
    	
    	$views = array();
    	if (preg_match('/^([1-9]|1[0-2])$/', $month) && preg_match('/^((19\d{2})|(20\d{2}))$/', $year)) {
    	    $views = $ad->getDailyViews($month, $year, $type);
    	}
    	
    	return array(
    		'ad' => $ad,
    	    'views' => $views,
    		'user' => $this->getUser(),
    		'viewTypes' => AdView::$viewTypes
    	);
    }
    
    /**
     * @Route("/candidate-window/{slug}/bump.{_format}", name="ad_bump", 
     * requirements={"id"="\d+", "_format"="json"}, defaults={"_format"="json"})
     * @ParamConverter("ad", class="WanaworkUserBundle:Ad", options={"mapping": {"slug":"slug"}})
     * @Method("POST")
     */
    public function bumpAction(Ad $ad, $_format)
    {
        $security = $this->get('security.context');

        if (!$security->isGranted('EDIT', $ad)) {
            throw new AccessDeniedException("You cannot view this page");
        }
        
        $em = $this->getDoctrine()->getManager();
        $ad->bump();
        $em->flush();
        
        return new JsonResponse($data = null, $status = 201, array(
        	'ww_bumps_left' => $ad->getCountBumpsLeft(),
        ));
    }
    
    /**
     * @Route("/candidate-window/{slug}/update-badge.{_format}", name="ad_update_badge",
     * requirements={"id"="\d+", "_format"="json"}, defaults={"_format"="json"})
     * @ParamConverter("ad", class="WanaworkUserBundle:Ad", options={"mapping": {"slug":"slug"}})
     * @Method("POST")
     */
    public function updateBadgeAction(Ad $ad)
    {
        $security = $this->get('security.context');

        if (!$security->isGranted('EDIT', $ad)) {
            throw new AccessDeniedException("You cannot view this page");
        }
        
        $em = $this->getDoctrine()->getManager();
        if ($this->getRequest()->request->has('badge')) {
            $badgeText = $this->getRequest()->request->get('badge');
            $ad->setBadge($badgeText);
            
            $validator = $this->get('validator');
            if ($validator->validate($ad)) {
                $em->flush();
            }
        }
        
        return new JsonResponse($data = null, $status = 204);
    }
    
    /**
     * @param Ad $ad
     * @Route("/ads/{slug}/upgrade-premium", name="ad_upgrade_premium")
     * @Secure(roles="ROLE_USER")
     * @Template()
     */
    public function upgradeToPremiumAction(Ad $ad)
    {
        $security = $this->get('security.context');
        
        if (!$security->isGranted('EDIT', $ad)) {
            throw new AccessDeniedException("You cannot view this page");
        }
        
        if ($ad->isPremium() || !$ad->isPublished()) {
            return $this->redirect($this->generateUrl('my_ads'));
        }
        
        return array(
        	'ad' => $ad,
        );
    }
}
