<?php
namespace Wanawork\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Wanawork\UserBundle\Entity\Sector;
use Wanawork\UserBundle\Form\SectorType;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Sector controller.
 *
 * @Route("/admin")
 */
class AdminController extends Controller
{

    /**
     * Daily details
     * 
     * @Route("/daily-details", name="daily_details")
     * @Method("GET")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function indexAction()
    {
        $dateRegex = '/^2\d{3}\-\d{2}\-\d{2}\s\d{2}:\d{2}$/';
        $allowedGroupings = array('day', 'month');
        
        $request = $this->getRequest();
        $startDate = $request->query->get('start-date');
        $finishDate = $request->query->get('finish-date');
        $canReset = false;
        
        if ($startDate === null || !preg_match($dateRegex, $startDate) || strtotime($startDate) === false) {
            $startDate = date('Y-m-d 00:00');
        }
        
        if ($finishDate === null || !preg_match($dateRegex, $finishDate) || strtotime($finishDate) === false) {
            $finishDate = date('Y-m-d H:i');
        }
        
        $em = $this->getDoctrine()->getManager();
        
        $repos = $em->getRepository('Wanawork\UserBundle\Entity\User');
        $numberOfUsers = $repos->getUserCount(null, $startDate,$finishDate);
        $numberOfUsersByGender = $repos->getUsersCountByGender($startDate,$finishDate);

        $repos = $em->getRepository('Wanawork\UserBundle\Entity\Ad');
        $numberOfAds = $repos->getAdsPosted($startDate,$finishDate);
        
        $repos = $em->getRepository('Wanawork\UserBundle\Entity\Billing\Payment');
        $numberOfPayments = $repos->getPaymentsMade($startDate,$finishDate);
        $numberOfPayPalPayments = $repos->getPayPalPayments($startDate,$finishDate);
        $numberOfVoucherPayments = $repos->getVoucherPayments($startDate,$finishDate);
        $numberOfTwitterPayments = $repos->getTwitterPayments($startDate,$finishDate);

        $repos = $em->getRepository('Wanawork\UserBundle\Entity\Billing\AdOrder');
        $numberOfStandardOrders = $repos->getStandardOrders($startDate,$finishDate);
        $numberOfPremiumOrders = $repos->getPremiumOrders($startDate,$finishDate);
        $numberOfPaymentsByGender = $repos->getPaymentsByGender($startDate,$finishDate);
        $numberOfPaymentAmount = $repos->getPaymentAmount($startDate,$finishDate);
        $numberOfPayPalPaymentsByGender = $repos->getPayPalPaymentsByGender($startDate,$finishDate);
        $numberOfVoucherPaymentsByGender = $repos->getVoucherPaymentsByGender($startDate,$finishDate);
        $numberOfTwitterPaymentsByGender = $repos->getTwitterPaymentsByGender($startDate,$finishDate);
        
        $repos = $em->getRepository('Wanawork\UserBundle\Entity\Search');
        $numberOfSearches = $repos->getSearchesMade($startDate,$finishDate);
        
        $repos = $em->getRepository('Wanawork\UserBundle\Entity\CvRequest');
        $numberOfCVRequests = $repos->getCVsRequested($startDate,$finishDate);

        $repos = $em->getRepository('Wanawork\UserBundle\Entity\Message');
        $numberOfMessagesSent = $repos->getMessagesSent($startDate,$finishDate);

        return $this->render('WanaworkUserBundle:Admin:daily-details.html.twig', array(
        	'start_date'                         => $startDate,
            'end_date'                           => $finishDate,
            'NumberOfUsersData'                  => $numberOfUsers,
            'NumberOfUsersByGenderData'          => $numberOfUsersByGender,
            'NumberOfAdsData'                    => $numberOfAds,
            'NumberOfPaymentsData'               => $numberOfPayments,
            'NumberOfPaymentsByGenderData'       => $numberOfPaymentsByGender,
            'NumberOfPaymentAmountData'          => $numberOfPaymentAmount,
            'NumberOfSearchesData'               => $numberOfSearches,
            'NumberOfCVRequestsData'             => $numberOfCVRequests,
            'NumberOfMessagesData'               => $numberOfMessagesSent,
            'NumberOfStandardOrdersData'         => $numberOfStandardOrders,
            'NumberOfPremiumOrdersData'          => $numberOfPremiumOrders,
            'NumberOfPayPalPaymentsData'         => $numberOfPayPalPayments,
            'NumberOfPayPalPaymentsByGenderData' => $numberOfPayPalPaymentsByGender,
            'NumberOfVoucherPaymentsData'        => $numberOfVoucherPayments,
            'NumberOfVoucherPaymentsByGenderData'=> $numberOfVoucherPaymentsByGender,
            'NumberOfTwitterPaymentsData'        => $numberOfTwitterPayments,
            'NumberOfTwitterPaymentsByGenderData'=> $numberOfTwitterPaymentsByGender,
        ));
    }
}
