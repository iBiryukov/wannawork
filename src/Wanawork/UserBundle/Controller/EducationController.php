<?php
namespace Wanawork\UserBundle\Controller;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Wanawork\UserBundle\Entity\CV;
use Wanawork\UserBundle\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Wanawork\UserBundle\Entity\Education;
use Wanawork\UserBundle\Form\EducationType;
use JMS\SecurityExtraBundle\Annotation\Secure;

/**
 * Education controller.
 *
 */
class EducationController extends Controller
{
    /**
     * Lists all Education entities.
     *
     * @Route("/education", name="education")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('WanaworkUserBundle:Education')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Finds and displays a Education entity.
     *
     * @Route("/{id}/show", name="education_show")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('WanaworkUserBundle:Education')->find($id);

        if (!$entity instanceof Education) {
            throw $this->createNotFoundException('Unable to find Education entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to create a new Education entity.
     * 
     * @param integer $cvId  
     * @Route("/cv/{cvId}/new-education", name="education_new")
     * @Template()
     * @Method("GET")
     * @Secure(roles="ROLE_EMPLOYEE")
     */
    public function newAction($cvId)
    {
    	$em = $this->getDoctrine();
    	$cvRepository = $em->getRepository('WanaworkUserBundle:CV');
    	$cvEntity = $cvRepository->find($cvId);
    	
    	if(!$cvEntity instanceof CV) {
    		throw $this->createNotFoundException('Cv was not found');
    	}
    	
    	$security = $this->get('security.context');
    	if(!$security->isGranted('EDIT', $cvEntity)) {
    		throw new AccessDeniedException("You don't have permission to access this page");
    	}
    	
        $form = $this->createForm(new EducationType());
        
        $educationSectors =
        array_map(function(EducationSector $sector){
        	return $sector->__toArray();
        }, $em->getRepository("WanaworkUserBundle:EducationSector")->findAllWithCourses());

        return array(
            'form'   => $form->createView(),
        	'education' => null,
        	'cv' => $cvEntity,
        	'education_sectors' => $educationSectors
        );
    }

    /**
     * Creates a new Education entity.
     *
     * @Route("/cv/{cvId}/create-education.{_format}", name="education_create", defaults={"_format"="html"}, requirements={"_format"="html|json"})
     * @Method("POST")
     * 
     * @Secure(roles="ROLE_EMPLOYEE")
     */
    public function createAction(Request $request, $cvId)
    {
    	$security = $this->get('security.context');
    	$em = $this->getDoctrine();
    	$cvRepository = $em->getRepository('WanaworkUserBundle:CV');
    	$cvEntity = $cvRepository->find($cvId);
    	 
    	if(!$cvEntity instanceof CV) {
    		throw $this->createNotFoundException('Cv was not found');
    	}
    	 
    	if(!$security->isGranted('EDIT', $cvEntity)) {
    		throw new AccessDeniedException("You don't have permission to access this page");
    	}
    	
    	$format = $this->getRequest()->getRequestFormat();
    	$returnData = array('outcome' => false, 'messages' => array());
    	
        $educationEntity  = new Education();
        $form = $this->createForm(new EducationType(), $educationEntity);
        $form->bind($request);

        if ($form->isValid()) {
        	
        	$employeeService = $this->get('wanawork.employee_service');
        	$employeeService->createEducation($educationEntity, $cvEntity);
        	
            $returnData['outcome'] = true;
            if($format !== 'json') {
            	$this->get('session')->getFlashBag()->add('messages', 'Education Saved');
            	return $this->redirect($this->generateUrl('cv_show', array('id' => $cvEntity->getId())));
            }
            
            $educationDeleteForm = $this->createFormBuilder()->add('id', 'hidden')->getForm();
            $educationEditForm = $this->createForm(new EducationType($em, $educationEntity, $educationEntity->getCv()), $educationEntity);
            
            $educationRow = 
            $this->renderView("WanaworkUserBundle:Education:_education-row.html.twig", array(
            	'education' => $educationEntity	,
            	'education_delete_form' => $educationDeleteForm->createView(),
            	'education_edit_form' => $educationEditForm->createView()
            ));
            
            $returnData['row'] = $educationRow;
            
        } else {
        	if($format === 'json') {
        		$errors = $this->get('validator')->validate($educationEntity);
        		foreach($errors as $error) {
        			$returnData['messages'][] = $error->getMessage();
        		}
        	}
        }
        
        $educationSectors =
        array_map(function(EducationSector $sector){
        	return $sector->__toArray();
        }, $em->getRepository("WanaworkUserBundle:EducationSector")->findAllWithCourses());

        $template = "WanaworkUserBundle:Education:new.$format.twig";
        return
        $this->render($template, array(
            'cv' => $cvEntity,
        	'education' => null,
            'form'   => $form->createView(),
        	'returnData' => $returnData,
        	'education_sectors' => $educationSectors
        ));
    }

    /**
     * Displays a form to edit an existing Education entity.
     *
     * @Route("/education/{id}/edit", name="education_edit")
     * @Template()
     * @Secure(roles="ROLE_EMPLOYEE")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $security = $this->get('security.context');
        
        $educationEntity = $em->getRepository('WanaworkUserBundle:Education')->find($id);
        if (!$educationEntity instanceof Education) {
            throw $this->createNotFoundException('Unable to find Education entity.');
        }
        
        if(!$security->isGranted('EDIT', $educationEntity->getCv())) {
        	throw new AccessDeniedException("You don't have permission to access this page");
        }
        $editForm = $this->createForm(new EducationType(), $educationEntity);
        
        $educationSectors =
        array_map(function(EducationSector $sector){
        	return $sector->__toArray();
        }, $em->getRepository("WanaworkUserBundle:EducationSector")->findAllWithCourses());

        return array(
            'education'      => $educationEntity,
            'form'   => $editForm->createView(),
        	'education_sectors' => $educationSectors
        );
    }

    /**
     * Edits an existing Education entity.
     *
     * @Route("/education/{id}/update.{_format}", name="education_update", defaults={"_format"="html"}, requirements={"_format"="html|json"})
     * @Method("POST")
     * @Secure(roles="ROLE_USER, ROLE_ADMIN")
     */
    public function updateAction(Request $request, $id)
    {
    	$returnData = array('outcome' => false, 'messages' => array());
    	$security = $this->get('security.context');
    	$format = $this->getRequest()->getRequestFormat();
        $em = $this->getDoctrine()->getManager();
        $educationEntity = $em->getRepository('WanaworkUserBundle:Education')->find($id);

        if (!$educationEntity instanceof Education) {
            throw $this->createNotFoundException('Unable to find Education entity.');
        }
        
        if(!$security->isGranted('EDIT', $educationEntity->getCv())) {
        	throw new AccessDeniedException("You don't have permission to access this page");
        }
        
        $editForm = $this->createForm(new EducationType(), $educationEntity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($educationEntity);
            $em->flush();

            $returnData['outcome'] = true;
            
            if($format !== 'json') {
            	$this->get('session')->getFlashBag()->add('messages', 'Education Updated');
            	return $this->redirect($this->generateUrl('cv_show', array('id' => $educationEntity->getCv()->getId())));
            }
            
            $educationDeleteForm = $this->createFormBuilder()->add('id', 'hidden')->getForm();
            $educationEditForm = $this->createForm(new EducationType($em, $educationEntity, $educationEntity->getCv()), $educationEntity);
            
            $educationRow =
            $this->renderView("WanaworkUserBundle:Education:_education-row.html.twig", array(
            		'education' => $educationEntity	,
            		'education_delete_form' => $educationDeleteForm->createView(),
            		'education_edit_form' => $educationEditForm->createView()
            ));
            
            $returnData['row'] = $educationRow;
            
        } else {
        	if($format === 'json') {
        		$errors = $this->get('validator')->validate($educationEntity);
        		foreach($errors as $error) {
        			$returnData['messages'][] = $error->getMessage();
        		}
        	}
        }
        
        $educationSectors =
        array_map(function(EducationSector $sector){
        	return $sector->__toArray();
        }, $em->getRepository("WanaworkUserBundle:EducationSector")->findAllWithCourses());
        
        return $this->render("WanaworkUserBundle:Education:edit.$format.twig", array(
        	'education' => $educationEntity,
        	'returnData' => $returnData,
        	'entity' => $educationEntity,
        	'form'   => $editForm->createView(),
        	'education_sectors' => $educationSectors
        ));

        
    }

    /**
     * Deletes a Education entity.
     * 
     * @Route("/education/{id}/delete.{_format}", name="education_delete", defaults={"_format"="html"}, requirements={"_format"="html|json"})
     * @Method("POST")
     * @Secure(roles="ROLE_USER, ROLE_ADMIN")
     */
    public function deleteAction(Request $request, $id)
    {
    	$returnData = array('outcome' => false, 'messages' => array());
    	$format = $this->getRequest()->getRequestFormat();
    	
    	$em = $this->getDoctrine()->getManager();
    	$educationEntity = $em->getRepository('WanaworkUserBundle:Education')->find($id);
    	
    	if (!$educationEntity instanceof Education) {
    		throw $this->createNotFoundException('Unable to find Education record');
    	}
    	
    	$security = $this->get('security.context');
    	if(!$security->isGranted('EDIT', $educationEntity->getCv())) {
    		throw new AccessDeniedException("You don't have permission to access this page");
    	}
    	
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $cv = $educationEntity->getCv();
            $employeeService = $this->get('wanawork.employee_service');
            $employeeService->deleteEducation($educationEntity);
            $this->get('session')->getFlashBag()->add('messages', 'Education Deleted');
            
            $returnData['outcome'] = true;
            
        }
        
        if($format === 'html') {
        	return $this->redirect($this->generateUrl('cv_show', array('id' => $cv->getId())));
        }
        return $this->render('::base.json.twig', array(
        	'returnData' => $returnData		
        ));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
