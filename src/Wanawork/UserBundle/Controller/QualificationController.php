<?php
namespace Wanawork\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Wanawork\UserBundle\Entity\Qualification;
use Wanawork\UserBundle\Form\QualificationType;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * Qualification controller.
 *
 * @Route("/entity/qualification")
 */
class QualificationController extends Controller
{
    /**
     * @Route("/export", name="entity_qualification_export")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("GET")
     */
    public function exportAction()
    {
        $exporter = $this->get('wanawork.qualifications_trader');
        $data = $exporter->export();
    
        $file = sys_get_temp_dir() . '/' . uniqid();
        $fp = fopen($file, 'w');
        fwrite($fp, prettyPrint(json_encode($data)));
        fclose($fp);
    
        $response = new BinaryFileResponse(
            $file, $status = 200, $headers = array(), $public = false,
            $contentDisposition = null, $autoEtag = false, $autoLastModified = true
        );
        $response->headers->set('Content-Type', 'application/json');
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, "qualifications_".date('Y_m_d_H_i').".json");
        return $response;
    }

    /**
     * Lists all Qualification entities.
     *
     * @Route("/", name="entity_qualification")
     * @Method("GET")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('WanaworkUserBundle:Qualification')->findBy(array(), array('name' => 'ASC'));

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Qualification entity.
     *
     * @Route("/", name="entity_qualification_create")
     * @Method("POST")
     * @Template("WanaworkUserBundle:Qualification:new.html.twig")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function createAction(Request $request)
    {
        $id = $this->getDoctrine()->getRepository('WanaworkUserBundle:Sector')->findNextId();
        $entity = new Qualification($id, '', false);
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('entity_qualification'));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Qualification entity.
    *
    * @param Qualification $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Qualification $entity)
    {
        $form = $this->createForm(new QualificationType(), $entity, array(
            'action' => $this->generateUrl('entity_qualification_create'),
            'method' => 'POST',
            'attr' => array(
        	   'novalidate' => '',
            ),
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Qualification entity.
     *
     * @Route("/new", name="entity_qualification_new")
     * @Method("GET")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function newAction()
    {
        $id = $this->getDoctrine()->getRepository('WanaworkUserBundle:Sector')->findNextId();
        $entity = new Qualification($id, '', false);
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Qualification entity.
     *
     * @Route("/{id}", name="entity_qualification_show")
     * @Method("GET")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WanaworkUserBundle:Qualification')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Qualification entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Qualification entity.
     *
     * @Route("/{id}/edit", name="entity_qualification_edit")
     * @Method("GET")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WanaworkUserBundle:Qualification')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Qualification entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Qualification entity.
    *
    * @param Qualification $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Qualification $entity)
    {
        $form = $this->createForm(new QualificationType(), $entity, array(
            'action' => $this->generateUrl('entity_qualification_update', array('id' => $entity->getId())),
            'method' => 'PUT',
            'attr' => array(
                'novalidate' => '',
            ),
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Qualification entity.
     *
     * @Route("/{id}", name="entity_qualification_update")
     * @Method("PUT")
     * @Template("WanaworkUserBundle:Qualification:edit.html.twig")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WanaworkUserBundle:Qualification')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Qualification entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('entity_qualification'));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Qualification entity.
     *
     * @Route("/{id}", name="entity_qualification_delete")
     * @Method("DELETE")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('WanaworkUserBundle:Qualification')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Qualification entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('entity_qualification'));
    }

    /**
     * Creates a form to delete a Qualification entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('entity_qualification_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
