<?php
namespace Wanawork\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Wanawork\UserBundle\Entity\SystemEmailLink;

/**
 * 
 * @Route("/redirect")
 */
class RedirectController extends Controller
{
    /**
     * @Route("/{id}", name="link_redirect")
     */
    public function redirectAction(SystemEmailLink $link)
    {
        $ip = $this->getRequest()->getClientIp();
        $link->addClick($ip);
        
        $this->getDoctrine()->getManager()->flush();
        
        return $this->redirect($link->getHref());
    }
}