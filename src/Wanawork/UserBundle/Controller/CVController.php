<?php
namespace Wanawork\UserBundle\Controller;

use Wanawork\UserBundle\Entity\Reference;
use Wanawork\UserBundle\Entity\WorkExperience;
use Wanawork\UserBundle\Entity\Education;
use Symfony\Component\HttpFoundation\Response;
use Wanawork\UserBundle\Form\WorkExperienceType;
use Wanawork\UserBundle\Entity\EmployeeProfile;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Wanawork\UserBundle\Form\EducationType;
use Wanawork\UserBundle\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Wanawork\UserBundle\Entity\CV;
use Wanawork\UserBundle\Form\CVType;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Wanawork\UserBundle\Entity\Ad;
use Wanawork\UserBundle\Entity\CVForm;
use Wanawork\UserBundle\Entity\CVFile;
use Wanawork\UserBundle\Form\CVFileType;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Wanawork\UserBundle\Entity\Role;

/**
 * CV controller.
 *
 */
class CVController extends Controller
{
    /**
     * Lists all CV entities.
     *
     * @Route("/candidate/{userId}/cvs", name="cv")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function indexAction($userId)
    {
    	$userEntity = $this->getDoctrine()->getRepository('WanaworkUserBundle:User')->find($userId);
    	if(!$userEntity instanceof User) {
    		throw $this->createNotFoundException('Candidate not found');
    	}
    	
    	$security = $this->get('security.context');
    	
    	if(!$userEntity->getEmployeeProfile()) {
    		
    		if(!$security->isGranted('CREATE', new EmployeeProfile())) {
    			throw $this->createNotFoundException('Profile not found');
    		} elseif(!$security->isGranted('EDIT', $userEntity)) {
    			throw $this->createNotFoundException('Profile not found');
    		} else {
    			return $this->render('WanaworkUserBundle:CV:_profile-required.html.twig', array(
    				'user' => $userEntity
    			));
    		}
    	} else {
    		
    		if($userEntity->getEmployeeProfile()->getCvs()->count() === 0) {
    			return $this->forward('WanaworkUserBundle:CV:new', array('userId' => $userEntity->getId()));
    		}
    	}

        return array(
        	'user' => $userEntity,
        );
    }

    /**
     * Finds and displays a CV entity.
     *
     * @Route("/candidate/show-cv/{id}", name="cv_show")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
		$security = $this->get('security.context');
		
        $cv = $em->getRepository('WanaworkUserBundle:CV')->find($id);

        if (!$cv instanceof CV) {
            throw $this->createNotFoundException('Unable to find CV entity.');
        }
		
        if(!$security->isGranted('VIEW', $cv)) {
        	throw new AccessDeniedException("You don't have permission to access this page");
        }
        
        // Education delete forms 
        $educationDeleteForms = array();
        foreach($cv->getEducations() as $education) {
        	$educationDeleteForms[] = $this->createDeleteForm($education->getId())->createView();
        }
        
        // Job Delete Forms
        $jobDeleteForms = array();
        foreach($cv->getJobs() as $job) {
        	$jobDeleteForms[] = $this->createDeleteForm($job->getId())->createView();
        }
        
        // Reference delete forms
        $referenceDeleteForms = array();
        foreach($cv->getReferences() as $reference) {
        	$referenceDeleteForms[] = $this->createDeleteForm($reference->getId())->createView();
        }
        
        return
        $this->render('WanaworkUserBundle:CV:show.html.twig',
	        array(
	            'cv' => $cv,
	        	'education_delete_forms' => $educationDeleteForms,
	        	'job_delete_forms' => $jobDeleteForms,
	        	'reference_delete_forms' => $referenceDeleteForms
	        ));
    }

    
    /**
     * Deletes a CV entity.
     *
     * @Route("/cv/{id}/delete", name="cv_delete")
     * @Method("DELETE")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('WanaworkUserBundle:CV')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find CV entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('cv'));
    }
    
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
    
    /**
     * @Route("/cv/{id}/download", name="cv_download")
     * @Method("GET")
     * @Secure(roles="ROLE_USER")
     * @Template()
     */
    public function downloadCvAction(CV $cv)
    {
        $security = $this->get('security.context');
        if(!$security->isGranted('VIEW', $cv)) {
            throw new AccessDeniedException();
        }
        
        $response = null;
        if ($cv instanceof CVFile) {
            $file = $cv->getFile();
            if (!$file instanceof \Symfony\Component\HttpFoundation\File\File || !$file->isReadable()) {
                throw $this->createNotFoundException('The requested CV was not found');
            }
            
            $response = new BinaryFileResponse($file);
            $response->headers->set('Content-Type', $file->getMimeType());
            $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $cv->getFileName());
            
        } elseif($cv instanceof CVForm) {
            $html = $this->renderView('WanaworkUserBundle:CV:download-cv-form.html.twig', ['cv' => $cv]);
            
            $pdfFileName = $cv->getName() . '.pdf';
            $response =
                new Response(
                    $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
                    200,
                    array(
                        'Content-Type'          => 'application/pdf',
                        'Content-Disposition'   => 'attachment; filename="'.$pdfFileName.'"'
                    )
                );
        }
        
        return $response;
    }
    
    /**
     * @Route("/cv/create.{_format}", name="cv_file_create", requirements={"_format"="json"}, defaults={"_format"="json"})
     * @Method("POST")
     */
    public function createFileAction($_format)
    {
        $user = $this->getUser(); 
        
        if (!$user) {
            throw new AccessDeniedHttpException();
        }
        
        $response = new JsonResponse();
        $uploadedFile = $this->getRequest()->files->get('cv-file');
        if(!$uploadedFile instanceof UploadedFile) {
            $response->setStatusCode(400);
            $response->setData(array(
            	'errors' => ['No File uploaded']
            ));
        } elseif ($uploadedFile instanceof UploadedFile && $uploadedFile->isValid() === false) {
            $response->setStatusCode(422);
            $response->setData(array(
                'errors' => [uploadErrorToMessage($uploadedFile->getError())],
            ));
        } else {
        
            $em = $this->getDoctrine()->getManager();
            $validator = $this->get('validator');
        
            $profile = $user->getEmployeeProfile();
            if ($user instanceof User && !$profile) {
                $profile = new EmployeeProfile();
                $profile->setName($user->getName());
                $user->setEmployeeProfile($profile);
                
                $role = $em->getRepository('WanaworkUserBundle:Role')->find('ROLE_EMPLOYEE');
                if (!$role instanceof Role) {
                    throw new \Exception('Unable to find candidate role');
                }
                $profile->getUser()->addRole($role);
                
                $em->persist($profile);
            }
        
            $cv = new CVFile();
            $cv->setFile($uploadedFile);
            $cv->setName($uploadedFile->getClientOriginalName());
            $cv->setProfile($profile);
            
            $validationErrors = $validator->validate($cv, 'step1');
            if (sizeof($validationErrors) > 0) {
                
                $response->setStatusCode(422);
                $errors = [];
                foreach ($validationErrors as $error) {
                    $errors[] = $error->getMessage();
                }
                
                $response->setData(array(
                	'errors' => $errors,
                ));
            } else {
                $em->persist($cv);
                $em->flush();
                
                $response->setStatusCode(201);
                $response->setData(array(
                    'cv' => $cv->getId(),
                    'name' => $cv->getName(),
                    'download_url' => $this->generateUrl('cv_download', ['id' => $cv->getId()]),
                ));
            }
        }
        
        
        return $response;
    }
}
