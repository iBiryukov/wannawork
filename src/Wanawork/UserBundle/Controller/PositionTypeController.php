<?php
namespace Wanawork\UserBundle\Controller;

use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Wanawork\UserBundle\Entity\PositionType;
use Wanawork\UserBundle\Form\PositionTypeType;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * PositionType controller.
 *
 * @Route("/entity/positiontype")
 */
class PositionTypeController extends Controller
{
    /**
     * @Route("/export", name="entity_positiontype_export")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("GET")
     */
    public function exportAction()
    {
        $trader = $this->get('wanawork.position_type_trader');
        $data = $trader->export();
    
        $file = sys_get_temp_dir() . '/' . uniqid();
        $fp = fopen($file, 'w');
        fwrite($fp, prettyPrint(json_encode($data)));
        fclose($fp);
    
        $response = new BinaryFileResponse(
            $file, $status = 200, $headers = array(), $public = false,
            $contentDisposition = null, $autoEtag = false, $autoLastModified = true
        );
        $response->headers->set('Content-Type', 'application/json');
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, "positiontype".date('Y_m_d_H_i').".json");
        return $response;
    }

    /**
     * Lists all PositionType entities.
     *
     * @Route("/", name="entity_positiontype")
     * @Method("GET")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('WanaworkUserBundle:PositionType')->findBy(array(), array('order' => 'ASC'));

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new PositionType entity.
     *
     * @Route("/", name="entity_positiontype_create")
     * @Method("POST")
     * @Template("WanaworkUserBundle:PositionType:new.html.twig")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function createAction(Request $request)
    {
        $entity = new PositionType();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('entity_positiontype_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a PositionType entity.
    *
    * @param PositionType $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(PositionType $entity)
    {
        $form = $this->createForm(new PositionTypeType(), $entity, array(
            'action' => $this->generateUrl('entity_positiontype_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new PositionType entity.
     *
     * @Route("/new", name="entity_positiontype_new")
     * @Method("GET")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function newAction()
    {
        $id = $this->getDoctrine()->getRepository('WanaworkUserBundle:PositionType')->findNextId();
        
        $entity = new PositionType($id);
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a PositionType entity.
     *
     * @Route("/{id}", name="entity_positiontype_show")
     * @Method("GET")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WanaworkUserBundle:PositionType')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PositionType entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing PositionType entity.
     *
     * @Route("/{id}/edit", name="entity_positiontype_edit")
     * @Method("GET")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WanaworkUserBundle:PositionType')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PositionType entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a PositionType entity.
    *
    * @param PositionType $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(PositionType $entity)
    {
        $form = $this->createForm(new PositionTypeType(), $entity, array(
            'action' => $this->generateUrl('entity_positiontype_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing PositionType entity.
     *
     * @Route("/{id}", name="entity_positiontype_update")
     * @Method("PUT")
     * @Template("WanaworkUserBundle:PositionType:edit.html.twig")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WanaworkUserBundle:PositionType')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PositionType entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->persist($editForm->getData());
            $em->flush();

            return $this->redirect($this->generateUrl('entity_positiontype_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a PositionType entity.
     *
     * @Route("/{id}", name="entity_positiontype_delete")
     * @Method("DELETE")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('WanaworkUserBundle:PositionType')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find PositionType entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('entity_positiontype'));
    }
    
    
    
    /**
     * Creates a form to delete a PositionType entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('entity_positiontype_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
