<?php

namespace Wanawork\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('WanaworkUserBundle:Default:index.html.twig', array('name' => $name));
    }
}
