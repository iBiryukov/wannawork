<?php
namespace Wanawork\UserBundle\Controller;

use Wanawork\UserBundle\Form\SearchNotificationType;
use Wanawork\UserBundle\Entity\SearchNotification;
use Wanawork\UserBundle\Entity\Search;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Wanawork\UserBundle\Entity\User;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Wanawork\UserBundle\Entity\Ad;
use Wanawork\UserBundle\Form\AdType;

/**
 * SearchNotification controller.
 */
class SearchNotificationController extends Controller
{
    /**
     * Lists all SearchNotification entities.
     *
     * @Route("/search-nofiticaions", name="search_nofitication")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('WanaworkUserBundle:SearchNotification')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    

    /**
     * Finds and displays a SearchNotification entity.
     *
     * @Route("/search-notification/{id}", name="search_nofitication_show")
     * @Template()
     */
    public function showAction(SearchNotification $notification)
    {
        if($notification->getUser()) {
            if (!$this->getUser()) {
                return $this->redirect($this->generateUrl('login', array(
                    'login_redirect' => $this->generateUrl('search_nofitication_show', array(
                        'id' => $notification->getId(),
                    ))
                )));
            }
        
            $security = $this->get('security.context');
            if (!$security->isGranted("EDIT", $notification->getSearch()->getUser())) {
                throw new AccessDeniedException();
            }
        }
        
        $deleteForm = $this->createDeleteForm($notification->getSearch()->getId());
        
        return array(
            'entity'      => $notification,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to create a new SearchNotification entity.
     *
     * @Route("/search/{id}/create-notification", name="search_nofitication_new")
     * @Template()
     * @Method({"GET", "POST"})
     */
    public function newAction(Search $searchEntity)
    {
        $user = $this->getUser();
        $security = $this->get('security.context');
        $em = $this->getDoctrine()->getManager();
        
        if($searchEntity->getUser()) {
            if (!$user) {
                return $this->redirect($this->generateUrl('login'));
            } elseif(!$security->isGranted('EDIT', $searchEntity)) {
                throw new AccessDeniedException();
            }
        }
    	
    	if($searchEntity->hasNotifier()) {
    		$this->get('session')->getFlashBag()->add('infoMessage', 'You already have notification set up for this search');
    		return $this->redirect($this->generateUrl('wanawork_main_search', array('searchId' => $searchEntity->getId())));
    	}
    	
    	$name = $searchEntity->getProfession()->getName();
    	$searchNotification = new SearchNotification($searchEntity, $name, $user);
    	
        $form = $this->createForm(new SearchNotificationType($user), $searchNotification);
        
        if ($this->getRequest()->isMethod('POST')) {
            $form->handleRequest($this->getRequest());
            
            if ($form->isValid()) {
                $em->persist($searchNotification);
                $em->flush();
                
                $this->get('session')->getFlashBag()->add('okMessage', 'We subscribed you to this search');
                return $this->redirect($this->generateUrl('wanawork_main_search', array('searchId' => $searchEntity->getId())));
            }
        }

        return array(
            'entity' => $searchNotification,
            'form'   => $form->createView(),
        );
    }
    
    /**
     * Displays a form to edit an existing SearchNotification entity.
     *
     * @Route("/search/{id}/edit-nofitication", name="search_nofitication_edit")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WanaworkUserBundle:SearchNotification')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SearchNotification entity.');
        }

        $editForm = $this->createForm(new SearchNotificationType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing SearchNotification entity.
     *
     * @Route("/search/{id}/update-notification", name="search_nofitication_update")
     * @Method("POST")
     * @Template("WanaworkUserBundle:SearchNotification:edit.html.twig")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WanaworkUserBundle:SearchNotification')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SearchNotification entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new SearchNotificationType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('search_nofitication_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a SearchNotification entity.
     *
     * @Route("/search-notification/{id}/delete", name="search_nofitication_delete")
     */
    public function deleteAction(SearchNotification $notification)
    {
        if($notification->getUser()) {
            if (!$this->getUser()) {
                return $this->redirect($this->generateUrl('login', array(
                    'login_redirect' => $this->generateUrl('search_nofitication_show', array(
                        'id' => $notification->getId(),
                    ))
                )));
            }
        
            $security = $this->get('security.context');
            if (!$security->isGranted("EDIT", $notification->getSearch()->getUser())) {
                throw new AccessDeniedException();
            }
        }
                
        $request = $this->getRequest();
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->remove($notification);
            $em->flush();
            
            $this->get('session')->getFlashBag()->add('okMessage', 'We deleted your saved search');
        }

        $url = $this->generateUrl('wanawork_main_homepage');
        if ($this->getUser()) {
            $url = $this->generateUrl('my_saved_searches');
        }
        return $this->redirect($url);
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
    
    /**
     * @Route("/user/{id}/saved-searches", name="user_saved_searches")
     * @Secure(roles="ROLE_EMPLOYER")
     * @Template()
     */
    public function savedSearchesAction(User $user)
    {
        $security = $this->get('security.context');
        if(!$security->isGranted('EDIT', $user)) {
            throw new AccessDeniedException("You don't have permission this access this page");
        }
         
        return array(
            'user' => $user,
        );
    }
    
    /**
     * @Route("/search-notification/{id}/disable", name="search_nofitication_disable")
     */
    public function disableNotificationAction(SearchNotification $notification)
    {
        if($notification->getUser()) {
            if (!$this->getUser()) {
                return $this->redirect($this->generateUrl('login', array(
                	'login_redirect' => $this->generateUrl('search_nofitication_disable', array(
                	    'id' => $notification->getId(),
                    ))
                )));
            }
            
            $security = $this->get('security.context');
            if (!$security->isGranted("EDIT", $notification->getSearch()->getUser())) {
                throw new AccessDeniedException();
            }
        }
        
        $notification->markActive(false);
        $this->getDoctrine()->getManager()->flush();
        
        if ($this->getUser()) {
            $this->get('session')->getFlashBag()->add('okMessage', 'We unsubscribed you from this saved search');
            return $this->redirect($this->generateUrl('my_saved_searches'));
        } else {
            return $this->redirect($this->generateUrl('search_nofitication_show', array(
            	'id' => $notification->getId(),
            )));
        }
    }
    
    /**
     * @Route("/search-notification/{id}/enable", name="search_nofitication_enable")
     */
    public function enableNotificationAction(SearchNotification $notification)
    {
        if($notification->getUser()) {
            if (!$this->getUser()) {
                return $this->redirect($this->generateUrl('login', array(
                    'login_redirect' => $this->generateUrl('search_nofitication_disable', array(
                        'id' => $notification->getId(),
                    ))
                )));
            }
    
            $security = $this->get('security.context');
            if (!$security->isGranted("EDIT", $notification->getSearch()->getUser())) {
                throw new AccessDeniedException();
            }
        }
    
        $notification->markActive(true);
        $this->getDoctrine()->getManager()->flush();
    
        if ($this->getUser()) {
            $this->get('session')->getFlashBag()->add('okMessage', 'We re-subscribed you to your saved search');
            return $this->redirect($this->generateUrl('my_saved_searches'));
        } else {
            return $this->redirect($this->generateUrl('search_nofitication_show', array(
                'id' => $notification->getId(),
            )));
        }
    }
    
}
