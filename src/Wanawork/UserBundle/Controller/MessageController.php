<?php
namespace Wanawork\UserBundle\Controller;

use Wanawork\UserBundle\Entity\User;
use Wanawork\UserBundle\Entity\AbstractProfile;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Wanawork\MainBundle\Security\AclManager;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Wanawork\UserBundle\Entity\MailThread;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Wanawork\UserBundle\Entity\Message;
use Wanawork\UserBundle\Form\MessageType;

/**
 * Message controller.
 *
 * @Route("/mailbox")
 */
class MessageController extends Controller
{
	/**
	 * @Route("/{selectedThread}", name="mailbox_index", requirements={"selectedThread"="\d+"}, defaults={"selectedThread"=0})
	 * @Secure(roles="ROLE_USER")
	 * @Template()
	 */
	public function indexAction($selectedThread)
	{
		$em = $this->getDoctrine()->getManager();
		$user = $this->getUser();
		$profile = $user->getDefaultProfile();
		
		if (!$profile instanceof AbstractProfile) {
		    return array(
		    	'threads' => array(),
		    );    
		}
		
		$security = $this->get('security.context');
		$aclManager = $this->get('wanawork.acl_service');
		$threads = $profile->getMailboxes();
		$aclManager->preloadAcls($threads);
		
		$threads = 
		array_filter($threads, function(MailThread $thread) use($security){
		    return $security->isGranted('VIEW', $thread);
		});
 		
		$form = null;
		$selectedThreadEntity = null;
        if ($selectedThread > 0) {
            $selectedThreadEntity = $em->getRepository('WanaworkUserBundle:MailThread')->find($selectedThread);
            if($selectedThreadEntity instanceof MailThread && !$security->isGranted('VIEW', $selectedThreadEntity)) {
                $selectedThreadEntity = null;
            } elseif ($selectedThreadEntity instanceof MailThread && $security->isGranted('VIEW', $selectedThreadEntity)) {
                $message = new Message($text = '', $this->getUser(), $this->getUser()->getDefaultProfile(), $selectedThreadEntity);
                $form = $this->createForm(new MessageType(), $message)->createView();
                $aclManager->preloadAcls($selectedThreadEntity->getMessages());
            }
        }	

        if($selectedThreadEntity === null && sizeof($threads) > 0) {
            $selectedThreadEntity = $threads[0];
            $message = new Message($text = '', $this->getUser(), $this->getUser()->getDefaultProfile(), $selectedThreadEntity);
            $form = $this->createForm(new MessageType(), $message)->createView();
            $aclManager->preloadAcls($selectedThreadEntity->getMessages());
        }
        
		return array(
			'threads' => $threads,
			'selectedMailbox' => $selectedThreadEntity,
		    'form' => $form,
		);
		
	}
	
    /**
     * Lists all Message entities.
     *
     * @Route("/{id}/messages", name="mailbox", requirements={"id"="\d+"})
     * @Template()
     * @Secure(roles="ROLE_USER")
     */
    public function mailboxContentsAction(MailThread $thread)
    {
        $em = $this->getDoctrine()->getManager();
        
        $security = $this->get('security.context');
        if(!$security->isGranted('VIEW', $thread)) {
        	throw new AccessDeniedHttpException();
        }
        
        $form = null;
        if($security->isGranted('CREATE', $thread)) {
        	$message = new Message('', $this->getUser(), $this->getUser()->getDefaultProfile(), $thread);
        	$form = $this->createForm(new MessageType(), $message)->createView();
        }
        
        $aclService = $this->get('wanawork.acl_service');
        $aclService->preloadAcls($thread->getMessages());
        
        $deleteForms = array();
        foreach($thread->getMessages() as $message) {
        	if($security->isGranted('DELETE', $message)) {
        		$deleteForms[$message->getId()] = $this->createDeleteForm($message->getId())->createView();
        	}
        }
        
        return array(
            'thread' => $thread,
        	'form' => $form,
        	'deleteForms' => $deleteForms
        );
    }

    /**
     * Finds and displays a Message entity.
     * NOT USED
     * @Route("/{id}/show", name="mailbox_show")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WanaworkUserBundle:Message')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Message entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a new Message entity.
     *
     * @Route("/{id}/create-message.{_format}", name="mailbox_create", 
     *  requirements={"id"="\d+", "_format"="json"}, defaults={"_format"="json"})
     * @Method("POST")
     * @Secure(roles="ROLE_USER")
     */
    public function createAction(MailThread $thread, $_format)
    {
    	$em = $this->getDoctrine()->getManager();
    	
    	$security = $this->get('security.context');
    	if(!$security->isGranted('CREATE', $thread)) {
    		throw new AccessDeniedHttpException();
    	}
    	
    	$response = new \Symfony\Component\HttpFoundation\JsonResponse();
    	$message = new Message('', $this->getUser(), $this->getUser()->getDefaultProfile(), $thread);
    	$form = $this->createForm(new MessageType(), $message);
        $form->handleRequest($this->getRequest());    
        
        if ($form->isValid()) {
        	try {
        		$em->getConnection()->beginTransaction();
        		$em->persist($message);
        		$em->flush();
        		$aclManager = new AclManager($this->get('security.acl.provider'), $this->get('security.context'));
        		$aclManager->grant($message, MaskBuilder::MASK_OPERATOR, $this->getUser());
        		
        		$response->setStatusCode(201);
        		$response->setData(array(
        			'message' => $this->renderView('WanaworkUserBundle:Message:_message.html.twig', array(
        			    'message' => $message,
        		    )),
        		));
        		$mailer = $this->get('wanawork.mail');
        		$mailer->emailNewMessage($message);
        		
        		$em->getConnection()->commit();
        	} catch (\Exception $e) {
        		$em->getConnection()->rollback();
        		throw $e;
        	}
        } else {
            $response->setStatusCode(400);
            $response->setData(array(
            	'errors' => $form->getErrorsAsString(),
            ));
        }

        return $response;
    }

    /**
     * Displays a form to edit an existing Message entity.
     *
     * @Route("/message/{id}/edit", name="mailbox_edit")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WanaworkUserBundle:Message')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Message entity.');
        }

        $editForm = $this->createForm(new MessageType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Message entity.
     *
     * @Route("/message/{id}/update", name="mailbox_update")
     * @Method("POST")
     * @Template("WanaworkUserBundle:Message:edit.html.twig")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WanaworkUserBundle:Message')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Message entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new MessageType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('mailbox_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Message entity.
     *
     * @Route("/message/{id}/delete", name="message_delete")
     * @Method("POST")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function deleteAction(Request $request, $id)
    {
    	
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('WanaworkUserBundle:Message')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Message entity.');
            }
            
            $security = $this->get('security.context');
            if(!$security->isGranted("DELETE", $entity)) {
            	throw new AccessDeniedException();
            }
			$mailbox = $entity->getThread()->getId();
			
            $em->remove($entity);
            $em->flush();
            
            $this->get('session')->getFlashBag()->add('messages', 'Message Deleted');
            return $this->redirect($this->generateUrl('mailbox', array(
            	'mailbox' => 	$mailbox	
            )));
        }
        throw new AccessDeniedException();
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
    
    /**
     * 
     * @Route("/{id}/mark-read.{_format}", name="mailbox_mark_read", defaults={"_format"="json"}, 
     *   requirements={"id"="\d+", "_format"="json"})
     * @Secure(roles="ROLE_USER")
     */
    public function markReadAction(MailThread $mailbox)
    {
        $security = $this->get('security.context');
        
        if (!$security->isGranted('VIEW', $mailbox)) {
            throw new AccessDeniedHttpException("You don't have permission to view this mailbox");
        }
        
        $profile = $this->getUser()->getDefaultProfile();
        if ($profile !== $mailbox->getEmployee() && $profile !== $mailbox->getEmployer()) {
            throw new AccessDeniedHttpException("You are not among the participants of this conversation");
        }
        
    	$em = $this->getDoctrine()->getManager();
    	$markCount = $mailbox->markReadForUser($this->getUser()->getDefaultProfile());
    	$em->flush();
    	
        return new \Symfony\Component\HttpFoundation\JsonResponse(
            $data = null,
            204,
            array(
        	    'ww_marked_count' => $markCount,
            )                        
        );
    }
}
