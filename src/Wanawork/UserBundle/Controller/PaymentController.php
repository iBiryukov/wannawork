<?php
namespace Wanawork\UserBundle\Controller;

use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Wanawork\UserBundle\Entity\Ad;
use Wanawork\UserBundle\Entity\EmployerProfile;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Wanawork\UserBundle\Entity\Billing\AdOrder;
use Wanawork\UserBundle\Entity\Billing\Order;
use Wanawork\UserBundle\Entity\Billing\Transaction;
use Wanawork\UserBundle\Entity\Billing\OrderItem;
use Wanawork\UserBundle\Entity\Billing\Payment;
use Wanawork\UserBundle\Entity\Billing\VerificationOrder;
use Wanawork\UserBundle\Entity\Billing\exception\ApprovalRequiredException;
use Wanawork\UserBundle\Entity\Billing\exception\PaymentAlreadyProcessedException;
use Symfony\Component\HttpFoundation\Response;
use Wanawork\UserBundle\Entity\Billing\Voucher\AdVoucher;
use Symfony\Component\Form\FormError;

/**
 * @author Ilya Biryukov <ilya@wanawork.ie>
 * @Route("/payment")
 */
class PaymentController extends Controller
{
    
    /**
     * @Route("/list", name="payment_index")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function indexAction()
    {
        $dql = "SELECT o
				FROM Wanawork\UserBundle\Entity\Billing\Order o
				ORDER BY o.createdAt DESC";
        
        $query = $this->getDoctrine()->getManager()->createQuery($dql);
        
        $perPage = 10;
        $pageNumber = $this->get('request')->query->get('page', 1);
        
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate($query, $pageNumber, $perPage);
        
        return array(
            'pagination' => $pagination,
        );
    }
    
    /**
     * @Route("/report", name="payment_report")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function reportAction()
    {
        $repository = $this->getDoctrine()->getRepository('Wanawork\UserBundle\Entity\Billing\Payment');
        
        return array(
        	'data' => $repository->findDepositedPaymentsByMonth(),
        );
    }
    
    /**
     * @Route("/statistics", name="payment_stats")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function statisticsAction()
    {
        $dateRegex = '/^2\d{3}\-\d{2}\-\d{2}\s\d{2}:\d{2}$/';
        $allowedGroupings = array('day', 'month');
        
        $request = $this->getRequest();
        $startDate = $request->query->get('start-date');
        $finishDate = $request->query->get('finish-date');
        $grouping = $request->query->get('grouping');
        $canReset = false;
        
        
        if ($startDate === null || !preg_match($dateRegex, $startDate) || strtotime($startDate) === false) {
            $startDate = date('Y-m-01 00:00');
        }
        
        if ($finishDate === null || !preg_match($dateRegex, $finishDate) || strtotime($finishDate) === false) {
            $finishDate = date('Y-m-d H:i');
        }
        
        if (!in_array($grouping, $allowedGroupings)) {
            $grouping = 'day';
        }
        
        $paymentRepository = $this->getDoctrine()->getRepository('Wanawork\UserBundle\Entity\Billing\Payment');
        $paymentsByType = $paymentRepository->findPaymentsGroupByType($startDate, $finishDate, $grouping);
        
        $paymentSourceIds = array_values(Payment::$validSources);
        sort($paymentSourcesIds);
        $paymentSources = [];
        foreach ($paymentSourceIds as $id) {
            $paymentSources[] = Payment::getSourceById($id);
        }
        
        $paymentsByStatus = $paymentRepository->findPaymentsGroupByStatus($startDate, $finishDate, $grouping);
        $states = array_flip(Payment::$validStates);
        
        $paymentsByAmount = $paymentRepository->findPaymentsGroupedByAmount($startDate, $finishDate, $grouping);
        $amounts = [];
        if (sizeof($paymentsByStatus)) {
            $firstEntry = array_shift(array_values($paymentsByAmount));
            foreach ($firstEntry as $k => $v) {
                $amounts[] = $k;
            }
            sort($amounts);
        } 
        
        return array(
        	'start_date'                =>      $startDate,
            'finish_date'               =>      $finishDate,
            'grouping'                  =>      $grouping,
            'payments_by_type'          =>      $paymentsByType,
            'payment_sources'           =>      $paymentSources,
            'states'                    =>      $states,
            'payments_by_status'        =>      $paymentsByStatus,
            'payments_by_amount'        =>      $paymentsByAmount,
            'amounts'                   =>      $amounts,
         );
    }
    
    
    
    /**
     * @Route("/ad/{id}/create-order/{plan}", name="new_ad_order", 
     *  requirements={"id"="\d+", "plan"="standard|premium"})
     * @Secure(roles="ROLE_USER")
     */
    public function newAdOrderAction(Ad $ad, $plan)
    {
        $security = $this->get('security.context');
        if (!$security->isGranted("EDIT", $ad)) {
            throw new AccessDeniedException("You don't have permission to access this page");
        }
        
        $paypalService = $this->get('wanawork.billing.paypal');
        $validator = $this->get('validator');
        
        if (!$validator->validate($ad)) {
            return $this->render('WanaworkUserBundle:Payment:_ad-not-finished.html.twig', array(
            	'ad' => $ad,
            ));
        }
        
        $bumps = 0;
        $duration = 0;
        $amount = 0;
        switch ($plan) {
        	case 'standard':
        	case 'premium':
        	    $amount = $this->container->getParameter("ad.{$plan}.price");
        	    $duration = $this->container->getParameter("ad.{$plan}.length_months");
        	    $durationDays = $this->container->getParameter("ad.{$plan}.length_days");
        	    $bumps = $this->container->getParameter("ad.{$plan}.bumps");
        	    break;
        	default:
        	    throw new \Exception(sprintf('Unknown plan type provided: "%s"', $plan));
        }
        $vatRate = $this->container->getParameter('vat_rate');
        $plans = Ad::getPricePlans();
        
        $isExtension = $ad->isPublished();
        $order = new AdOrder($ad, $amount, $vatRate, $plans[$plan], $durationDays, $bumps, $isExtension);
        
        $planName = ucfirst($plan);
        $orderItem = new OrderItem(
            $name = "$planName subscription for $durationDays days", 
            $price = $amount, 
            $quantity = 1, 
            $order
        );
        $order->addItem($orderItem);
        
        if ($bumps > 0) {
            $name = ("Free bump") . ($bumps == 1 ? '' : 's') . (' included') ;
            $orderItem = new OrderItem(
            	$name, 
                $price = 0,
                $quantity = $bumps,
                $order
            ); 
            $order->addItem($orderItem);
        }
        
        $transaction = $paypalService->createPayment($order);
        
        if ($transaction->getState() === Transaction::STATE_SUCCESS) {
            return $this->redirect($transaction->getDataByKey('approval_url'));
        } else {
            throw new \Exception("Failed to create your order: " . $transaction->getFailReason());            
        }
    }
    
    /**
     * @Route("/company/{id}/purchase-verification", name="payment_verification_purchase", requirements={"id"="\d+"})
     */
    public function newVerificaitonOrderAction(EmployerProfile $profile)
    {
        $security = $this->get('security.context');
        if (!$security->isGranted('EDIT', $profile)) {
            throw new AccessDeniedException("You don't have permission to access this page");
        }
        
        if ($profile->isVerified()) {
            return $this->render('WanaworkUserBundle:Payment:profile-already-verified.htm.twig');
        }
        
        if ($profile->isVerificationPaid()) {
            return $this->render('WanaworkUserBundle:Payment:profile-verification-paid.htm.twig');
        }
        
        $paypalService = $this->get('wanawork.billing.paypal');
        $vatRate = $this->container->getParameter('vat_rate');
        $amount = $this->container->getParameter('employer.verification_price');
        
        $order = new VerificationOrder($profile, $amount, $vatRate);
        $orderItem = new OrderItem(
            $name = "Wannawork Verification",
            $price = $amount,
            $quantity = 1,
            $order
        );
        $order->addItem($orderItem);
        $transaction = $paypalService->createPayment($order);
        
        if ($transaction->getState() === Transaction::STATE_SUCCESS) {
            return $this->redirect($transaction->getDataByKey('approval_url'));
        } else {
            throw new \Exception("Failed to create your order: " . $transaction->getFailReason());
        }
    }
    
    /**
     * @Route("/success", name="payment_success")
     * @Route("/fail", name="payment_fail")
     */
    public function paymentResultAction()
    {
        $paymentId = $this->getRequest()->query->get('payment');
        $paymentRepository = $this->getDoctrine()->getRepository('Wanawork\UserBundle\Entity\Billing\Payment');
        $payment = $paymentRepository->find($paymentId);
        
        if (!$payment instanceof Payment) {
            throw $this->createNotFoundException('Payment not found');
        }

        $security = $this->get('security.context');
        $order = $payment->getOrder();
        
        $suffix = null;
        if ($order instanceof AdOrder) {
            $suffix = 'ad';
            if (!$security->isGranted("EDIT", $order->getAd())) {
                throw new AccessDeniedException("You don't have permission to access this page");
            }
        } elseif ($order instanceof VerificationOrder) {
            $suffix = 'verification';
            if (!$security->isGranted("EDIT", $order->getEmployer())) {
                throw new AccessDeniedException("You don't have permission to access this page");
            }
        }
        
        $payerId = $this->getRequest()->query->get('PayerID');
        $success = $this->getRequest()->get('_route') === 'payment_success';
        $paypalService = $this->get('wanawork.billing.paypal');
        
        try {
            $transaction = $paypalService->depositPayment($payment, $payerId, $success);
            
            $view = null;
            switch ($order->getStatus()) {
            	case Order::STATUS_CANCELLED:
            	    $view = "WanaworkUserBundle:Payment:_{$suffix}result-order-cancelled.html.twig";
            	    break;
            	    
            	case Order::STATUS_FAILED:
            	    $view = "WanaworkUserBundle:Payment:_{$suffix}result-order-failed.html.twig";
            	    break;
            	    
            	case Order::STATUS_PAID:
            	    $paymentService = $this->get('wanawork.billing.payment');
            	    $paymentService->processPayment($order, $payment);
            	    $view = "WanaworkUserBundle:Payment:_{$suffix}result-order-paid.html.twig";
            	    break;
            	    
            	case Order::STATUS_PENDING:
            	    if ($payment->getState() === Payment::STATE_CLEARING) {
            	        $view = "WanaworkUserBundle:Payment:_{$suffix}result-order-clearing.html.twig";
            	    } else {
            	        throw new \Exception("Got pending order with payment not in 'clearing' state");
            	    }
            	    break;
            	    
            	default:
            	    throw new \Exception("Unknown order status: {$order->getStatus()}");
            }
            
            return $this->render($view, array(
            	'order' => $order,
            ));
            
        } catch(ApprovalRequiredException $ex) {
            return $this->redirect($ex->getUrl());
        } catch(PaymentAlreadyProcessedException $ex2) {
        	return $this->render('WanaworkUserBundle:Payment:payment-already-processed.html.twig');
        } catch (\Exception $e) {
            throw $e;
        }
        
    }
    
    /**
     * @Route("/order/{id}/receipt.{_format}", name="order_receipt_download", 
     *  requirements={"id"="\d+", "_format"="html|pdf"}, defaults={"_format"="pdf"})
     * @Secure(roles="ROLE_USER")
     * @param Order $order
     */
    public function receiptAction(Order $order, $_format)
    {
        $security = $this->get('security.context');
        $permissionEntity = null;
        $templateSuffix = null;        
        
        if ($order instanceof AdOrder) {
            $permissionEntity = $order->getAd();
            $templateSuffix = 'ad';
        } elseif ($order instanceof VerificationOrder) {
            $permissionEntity = $order->getEmployer();
            $templateSuffix = 'verification';
        } else {
            throw new \Exception(
            	sprintf(
            	    "Unknown order type specified: '%s'",
            	    get_class($order)
                )
            );
        }
        
        if (!$security->isGranted('EDIT', $permissionEntity)) {
            throw new AccessDeniedException("You don't have permission to access receipts for this order");
        }
        
        if ($_format === 'html') {
            $template = $this->renderView("WanaworkUserBundle:Payment:receipt_{$templateSuffix}.html.twig", array(
                'order' => $order,
            ));
            return new Response($template);
        } elseif($_format === 'pdf') {
            
            $receiptService = $this->get('wanawork.receipt_service');
            $receiptPath = $receiptService->generateReceipt($order);
            
            $response = new BinaryFileResponse($receiptPath);
            $response->headers->set('Content-Type', 'application/pdf');
            $response->headers->set('Content-Disposition', "attachment; filename=\"{$order->getReceiptFileName()}\"");
            return $response;
            
        } else {
            throw $this->createNotFoundException(
        	    sprintf(
        	        "Inknown format requested: '%s'",
                    $_format
                )
            );
        }
    }
}

