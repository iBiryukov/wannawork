<?php
namespace Wanawork\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Wanawork\UserBundle\Entity\CvRequest;
use Wanawork\UserBundle\Form\CvRequestType;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Wanawork\UserBundle\Entity\Ad;
use Wanawork\UserBundle\Entity\EmployerProfile;
use Wanawork\UserBundle\Entity\AbstractProfile;
use Wanawork\UserBundle\Entity\EmployeeProfile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * CvRequest controller.
 *
 * @Route("/cv_request")
 */
class CvRequestController extends Controller
{

    /**
     * Lists all CvRequest entities.
     *
     * @Route("/", name="cv_request")
     * @Method("GET")
     * @Template()
     * @Secure(roles={"ROLE_ADMIN"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('WanaworkUserBundle:CvRequest')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    
    /**
     * @Route("/profile/{id}/{status}", name="cv_requests_by_profile", 
     *  requirements={"id"="\d+", "status"="\w+"}
     * )
     * @Method("GET")
     * @Secure(roles={"ROLE_USER"})
     */
    public function listAction(AbstractProfile $profile, $status = null)
    {
        if ($status !== null && !array_key_exists($status, CvRequest::$statuses)) {
            throw $this->createNotFoundException();
        }
        $statusId = ($status === null) ? null : CvRequest::$statuses[$status];
        
        $security = $this->get('security.context');
        if (!$security->isGranted("EDIT", $profile)) {
            throw new AccessDeniedException();
        }
        
        $requests = $profile->getCvRequests($statusId);
        $template = $profile instanceof EmployerProfile ? 'employer' : 'candidate';
        
        $updateStatusForms = array();
        $deleteForms = array();
        foreach ($requests as $request) {
            $updateStatusForms[$request->getId()] = $this->createUpdateStatusForm($request->getId())->createView();
            $deleteForms[$request->getId()] = $this->createDeleteForm($request->getId())->createView();
        }
        
        $aclManager = $this->get('wanawork.acl_service');
        $aclManager->preloadAcls($requests);
        
        
        $viewOptions = array(
        	'requests' => $requests,
            'status' => $status,
            'update_status_forms' => $updateStatusForms,
            'delete_forms' => $deleteForms,
        );
        
        return $this->render("WanaworkUserBundle:CvRequest:list-{$template}.html.twig", $viewOptions);
    }


    /**
     * Displays a form to create a new CvRequest entity.
     * 
     * @Route("/new/ad/{id}", name="cv_request_new", requirements={"id"="\d+"})
     * @ParamConverter("ad", class="WanaworkUserBundle:Ad")
     */
    public function newAction(Ad $ad)
    {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();
        
        // Check login ?
        if (!$user) {
            return $this->render('WanaworkUserBundle:CvRequest:login-required.html.twig');                
        }
        
        // No company profiles?
        if (sizeof($user->getEmployerProfiles()) === 0) {
            return $this->render('WanaworkUserBundle:CvRequest:company-profile-required.html.twig');
        }
        
        // Default profile is not employer profile?
        if(!$user->getDefaultProfile() instanceof EmployerProfile) {
            if (sizeof($user->getEmployerProfiles()) === 1) {
                $user->setDefaultProfile($user->getEmployerProfiles()->first());
                $em->flush();
                
                // refresh the page
                return $this->redirect($this->generateUrl('cv_request_new', array(
                	'id' => $ad->getId(),
                )));
                
            } else {
                return $this->render('WanaworkUserBundle:CvRequest:select-profile.html.twig');
            }
        }
        
        if ($user->getDefaultProfile()->getRemainingCVRequests() === 0) {
            return $this->render('WanaworkUserBundle:CvRequest:no-cv-request-left.html.twig');
        }
        
        if($user->getDefaultProfile()->getCvRequests()->get($ad->getId())) {
            return $this->render('WanaworkUserBundle:CvRequest:request-already-exists.html.twig', array(
            	'request' => $user->getDefaultProfile()->getCvRequests()->get($ad->getId())
            ));            
        }
        
        if ($ad->isExpired() || !$ad->isPublished()) {
            return $this->render('WanaworkUserBundle:CvRequest:_ad-expired.html.twig');
        }
        
        $cvRequest = new CvRequest();
        $cvRequest->setEmployerProfile($user->getDefaultProfile());
        $cvRequest->setAd($ad);
        $cvRequest->setUser($user);
        
        $form = $this->createForm(new CvRequestType($user->getDefaultProfile()), $cvRequest);
        
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $cvService = $this->get('wanawork.cv_service');
                $cvService->createCvRequest($cvRequest);

                $url = $this->generateUrl('wanawork_main_search');
                if ($request->query->get('redirect')) {
                    $url = $request->query->get('redirect') . '#' . 'ad-box-' . $ad->getId();
                }
                return $this->redirect($url, 201);
            }
        }
        
        $viewOptions = array(
        	'form' => $form->createView(),
            'ad' => $ad,
        );
        return $this->render('WanaworkUserBundle:CvRequest:new.html.twig', $viewOptions);
    }

    /**
     * Finds and displays a CvRequest entity.
     *
     * @Route("/{id}", name="cv_request_show")
     * @Method("GET")
     * @Template()
     * @Secure(roles={"ROLE_ADMIN"})
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WanaworkUserBundle:CvRequest')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CvRequest entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing CvRequest entity.
     *
     * @Route("/{id}/edit", name="cv_request_edit")
     * @Method("GET")
     * @Template()
     * @Secure(roles={"ROLE_ADMIN"})
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WanaworkUserBundle:CvRequest')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CvRequest entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a CvRequest entity.
    *
    * @param CvRequest $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(CvRequest $entity)
    {
        $form = $this->createForm(new CvRequestType($entity->getEmployerProfile()), $entity, array(
            'action' => $this->generateUrl('cv_request_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing CvRequest entity.
     *
     * @Route("/{id}", name="cv_request_update")
     * @Method("PUT")
     * @Template("WanaworkUserBundle:CvRequest:edit.html.twig")
     * @Secure(roles={"ROLE_ADMIN"})
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WanaworkUserBundle:CvRequest')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CvRequest entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('cv_request_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a CvRequest entity.
     *
     * @Route("/{id}.{format}", name="cv_request_delete",
     *  defaults={"format"="html"}, requirements={"id"="\d+", "format"="html|json"})
     * @Method("DELETE")
     * @Secure(roles={"ROLE_USER"})
     */
    public function deleteAction(CvRequest $cvRequest)
    {
        $security = $this->get('security.context');
        if (!$security->isGranted("DELETE", $cvRequest)) {
            throw new AccessDeniedException();
        }
        
        $format = $this->getRequest()->getRequestFormat();
        
        $form = $this->createDeleteForm($cvRequest->getId());
        $form->handleRequest($this->getRequest());

        if ($form->isValid()) {
            $cvService = $this->get('wanawork.cv_service');
            $cvService->deleteCvRequest($cvRequest);
            
            $response = $format === 'html' ?
                $this->redirect($this->generateUrl('my_shortlisted_cvs')) : new JsonResponse();
            
            $response->setStatusCode(204);
            return $response;
        } 
        
        $response = new Response();
        $response->setStatusCode(400);
        $response->setContent('Invalid request');
        return $response;

    }

    /**
     * Creates a form to delete a CvRequest entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->get('form.factory')->createNamedBuilder("delete_request_{$id}", 'form',  null)
            ->setAction($this->generateUrl('cv_request_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
    
    /**
     * @Route("/{id}/set-status", name="cv_request_set_status", 
     *   requirements={"id"="\d+", "status"="\d+"})
     * @Method("POST")
     * @Secure(roles={"ROLE_USER"})
     */
    public function updateStatusAction(CvRequest $request)
    {
        $form = $this->createUpdateStatusForm($request->getId());
        $form->handleRequest($this->getRequest());
        if ($form->isValid()) {
            $data = $form->getData();
            $status = (int)$data['status'];
            
            $security = $this->get('security.context');
            if (!$security->isGranted('EDIT', $request)) {
                throw new AccessDeniedException();
            }
            
            $cvService = $this->get('wanawork.cv_service');
            if ($status === CvRequest::GRANTED) {
                $cvService->approveCvRequest($request);
            } elseif ($status === CvRequest::DENIED) {
                $cvService->denyCvRequest($request);
            } else {
                throw $this->createNotFoundException('Invalid Status code provided: ' . $code);  
            }
            
            $response = $this->redirect($this->generateUrl('my_cv_requests'));
            $response->setStatusCode(204);
            return $response;
        }
        
        $response = new Response();
        $response->setStatusCode(400);
        $response->setContent('Invalid request');
        return $response;
    }
    
    private function createUpdateStatusForm($id)
    {
        return $this->get('form.factory')->createNamedBuilder("update_status_form_{$id}", 'form',  null)
            ->setAction($this->generateUrl('cv_request_set_status', array('id' => $id)))
            ->setMethod('POST')
            ->add('status', 'hidden', array(
        	   'attr' => array(
            	    'class' => 'status-id'
                )
            ))
            ->getForm();
    }
}
