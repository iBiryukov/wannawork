<?php
namespace Wanawork\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;

class SitemapController extends Controller
{

    /**
     * @Route("/sitemap.{_format}", name="sitemap", requirements={"_format"="xml"})
     */
    public function sitemapAction($_format)
    {
        $sitemapService = $this->get('wanawork.sitemap');
        $sitemapLocation = $this->container->getParameter('sitemap.location');
        
        $response = null;
        $lastModified = new \DateTime();
        if (is_file($sitemapLocation) && is_readable($sitemapLocation)) {
            $response = new Response();
            $response->setContent(file_get_contents($sitemapLocation));
            $lastModified = new \DateTime('@' . filemtime($sitemapLocation));
        } else {
            $response =
            $this->render("WanaworkUserBundle:Sitemap:sitemap.{$_format}.twig", array(
                'sitemap' => $sitemapService->generateMap(),
            ));
        }
        
        $expirationSeconds = 3600;
        $expiresDate = new \DateTime();
        $expiresDate->modify("+{$expirationSeconds} seconds");
        
        $response->setPublic();
        $response->setEtag(md5($response->getContent()));
        $response->setLastModified($lastModified);
        $response->setExpires($expiresDate);
        $response->setMaxAge($expirationSeconds);
        $response->setSharedMaxAge($expirationSeconds);
        
        if ($response->isNotModified($this->getRequest())) {
            $response->setContent(null);
        }
        
        return $response;
    }
}