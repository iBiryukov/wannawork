<?php
namespace Wanawork\UserBundle\Controller;

use Wanawork\UserBundle\Entity\AbstractProfile;
use Wanawork\UserBundle\Entity\TemporaryAvatar;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Wanawork\UserBundle\Form\NewPasswordType;
use Wanawork\UserBundle\Entity\PasswordReminder;
use Wanawork\UserBundle\Form\RestorePasswordType;
use Wanawork\UserBundle\Entity\Role;
use Wanawork\UserBundle\Form\Type\EmployeeProfileType;
use Wanawork\UserBundle\Entity\EmployeeProfile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Wanawork\UserBundle\Entity\User;
use Wanawork\UserBundle\Form\UserType;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Doctrine\DBAL\Query\QueryBuilder;
use Wanawork\UserBundle\Form\PasswordChangeType;
use Wanawork\UserBundle\Entity\Containers\ChangePasswordContainer;
use Wanawork\UserBundle\Form\Type\EmployerWithGeneratedPasswordType;
use Symfony\Component\Security\Core\Util\SecureRandom;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Wanawork\UserBundle\Entity\EmployerProfile;

/**
 * User controller.
 *
 */
class UserController extends Controller
{
    /**
     * Lists all User entities.
     *
     * @Route("/users/", name="users_list")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function indexAction()
    {
    	$request = $this->get('request');
    	
    	$filtersSet = false;
    	
    	// List of search criteria
    	$cEmail = mb_strtolower($request->query->get('email', ''));
    	$cName = mb_strtolower($request->query->get('name', ''));
    	$cAccountType = mb_strtoupper($request->get('account-type', ''));
    	$cProfiles = mb_strtolower($request->get('profiles', ''));
    	
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $qb->select('u');
        $qb->from('WanaworkUserBundle:User', 'u');
        
        $whereParts = array();
        if(mb_strlen($cEmail)) {
        	$whereParts[] = 'LOWER(u.email) like :email';
        	$qb->setParameter('email', $cEmail . '%');
        	
        }
        
        if(mb_strlen($cName)) {
        	$whereParts[] = 'LOWER(u.name) like :name';
        	$qb->setParameter('name', '%' . $cName . '%', \PDO::PARAM_STR);
        }
        
        switch($cAccountType) {
        	case 'ROLE_EMPLOYER':
        	case 'ROLE_EMPLOYEE':
        	case 'ROLE_ADMIN':
        		$qb->join('u.roles', 'roles');
        		$whereParts[] = 'roles.id IN (:roleName)';
        		$qb->setParameter('roleName', $cAccountType);
        		break;
        	
        }
        
        switch($cProfiles) {
        	case 'with':
        		$qb->leftJoin('u.employeeProfile', 'employee_profile');
        		$qb->leftJoin('u.employerProfile', 'employer_profile');
        		$whereParts[] = '(employee_profile IS NOT NULL OR employer_profile IS NOT NULL)';
        		break;
        		
        	case 'without':
        		$qb->leftJoin('u.employeeProfile', 'employee_profile');
        		$qb->leftJoin('u.employerProfile', 'employer_profile');
        		$whereParts[] = '(employee_profile IS NULL AND employer_profile IS NULL)';
        		break;
        		
        }
        
        foreach($whereParts as $wherePart) {
        	if(!$filtersSet) {
        		$filtersSet = true;
        		$qb->where($wherePart);
        	} else {
        		$qb->andWhere($wherePart);
        	}
        }
        
        $qb->orderBy('u.id', 'DESC');
        $query = $qb->getQuery();
        
        $perPage = 10;
        $pageNumber = $request->query->get('page', 1);
        
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate($query, $pageNumber, $perPage);

        return array(
			'pagination' => $pagination,
        	'filtersSet' => $filtersSet
		);	
    }
    
    /**
     * 
     * @Route("/users/statistics", name="users_statistics")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function statisticsAction()
    {
    	$em = $this->getDoctrine();
    	$userRepository = $em->getRepository('WanaworkUserBundle:User');
    	
    	$employeeCount = $userRepository->getUserCount(Role::EMPLOYEE);
    	$employerCount = $userRepository->getUserCount(Role::EMPLOYER);
    	
    	$registrationsEmployee = $userRepository->findRegistrationByDate(Role::EMPLOYEE);
    	$registrationsEmployer = $userRepository->findRegistrationByDate(Role::EMPLOYER);
    	
    	$months = array(
    		'01' => 'January',
    		'02' => 'February',
    		'03' => 'March',
    		'04' => 'April',
    		'05' => 'May',
    		'06' => 'June',
    		'07' => 'July',
    		'08' => 'August',
    		'09' => 'September',
    		'10' => 'October',
    		'11' => 'November',
    		'12' => 'December'
    	);
    	
    	$years = array();
    	foreach(array_keys($registrationsEmployee) as $date) {
    		$dateParts = explode('-', $date);
    		$year = $dateParts[0];
    		if(!in_array($year, $years)) {
    			$years[] = $year;
    		}
    	}
    	
    	return array(
    		'employeeCount' => $employeeCount,
    		'employerCount' => $employerCount,
    		'registrationsEmployee' => $registrationsEmployee,
    		'registrationsEmployer' => $registrationsEmployer,
    		'months' => $months,
    		'years' => $years,
    	);
    }
    
    /**
     * @Route("/users/{id}/disable", name="users_disable")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function disableAction($id) {
    	$em = $this->getDoctrine()->getManager();
    	$user = $em->getRepository('WanaworkUserBundle:User')->find($id);
    	
    	if(!$user instanceof User) {
    		throw $this->createNotFoundException();
    	}
    	
    	if($this->getUser() === $user) {
    		$this->get('session')->getFlashBag()->add('errorMessages', 'You cannot disable your own account');
    	} else {
    		$user->disable();
    		$em->flush();
    		$this->get('session')->getFlashBag()->add('successMessages', 'Account Disabled');
    	}
    	
    	return $this->redirect($this->generateUrl('users_list'));
    }
    
    /**
     * @Route("/users/{id}/enable", name="users_enable")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function enableAction($id) {
    	$em = $this->getDoctrine()->getManager();
    	$user = $em->getRepository('WanaworkUserBundle:User')->find($id);
    	 
    	if(!$user instanceof User) {
    		throw $this->createNotFoundException();
    	}
    	 
    	$user->enable();
    	$em->flush();
    	$this->get('session')->getFlashBag()->add('successMessages', 'Account Enabled');
    	return $this->redirect($this->generateUrl('users_list'));
    }

    /**
     * Finds and displays a User entity.
     *
     * @Route("/users/{id}/show", name="user_show", requirements={"id"="\d+"})
     * @ParamConverter("user", class="WanaworkUserBundle:User")
     * @Template()
     * @Secure(roles="ROLE_USER")
     */
    public function showAction(User $user)
    {
        $security = $this->get('security.context');
        if (!$security->isGranted('VIEW', $user)) {
            throw new AccessDeniedException();
        }       

        return array(
            'user' => $user,
        );
    }
    
    /**
     * Finds and displays a User entity.
     *
     * @Route("/users/{id}/receipts", name="user_receipts", requirements={"id"="\d+"})
     * @ParamConverter("user", class="WanaworkUserBundle:User")
     * @Template()
     * @Secure(roles="ROLE_USER")
     */
    public function receiptsAction(User $user)
    {
        $security = $this->get('security.context');
        if (!$security->isGranted('EDIT', $user)) {
            throw new AccessDeniedException();
        }
        
        return array(
            'user' => $user,
        );
        
    }
    
    /**
     * Displays a form to create a new User entity.
     *
     * @Route("/users/user/new", name="user_new")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function newAction()
    {
        $entity = new User();
        $form   = $this->createForm(new UserType(), $entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a new User entity.
     *
     * @Route("/users/user/create", name="user_create")
     * @Method("POST")
     * @Template("WanaworkUserBundle:User:new.html.twig")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function createAction(Request $request)
    {
        $entity  = new User();
        $form = $this->createForm(new UserType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('user_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("/users/user/{id}/edit", name="user_edit")
     * @Template()
     * @Secure(roles="ROLE_USER")
     */
    public function editAction(User $user)
    {
        $security = $this->get('security.context');
        if (!$security->isGranted('EDIT', $user)) {
            throw new AccessDeniedException("You cannot edit this user");
        }
        
        $isAdmin =  $this->getUser()->hasRoleName(Role::ADMIN);
        
        $editForm = $this->createForm(new UserType($isAdmin), $user);

        return array(
            'user'        => $user,
            'edit_form'   => $editForm->createView(),
        );
    }

    /**
     * Edits an existing User entity.
     *
     * @Route("/users/user/{id}/update", name="user_update")
     * @Method("POST")
     * @Template("WanaworkUserBundle:User:edit.html.twig")
     * @Secure(roles="ROLE_USER")
     */
    public function updateAction(User $user)
    {
        $security = $this->get('security.context');
        
        if (!$security->isGranted('EDIT', $user)) {
            throw new AccessDeniedException("You cannot edit this account");
        }
        
        $request = $this->getRequest();
        
        $em = $this->getDoctrine()->getManager();

        $isAdmin =  $this->getUser()->hasRoleName(Role::ADMIN);
        $editForm = $this->createForm(new UserType($isAdmin), $user);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($user);
            $em->flush();

            return $this->redirect($this->generateUrl('user_show', array('id' => $user->getId())));
        }

        return array(
            'user'      => $user,
            'edit_form'   => $editForm->createView(),
        );
    }

    /**
     * Deletes a User entity.
     *
     * @Route("/users/user/{id}/delete", name="user_delete")
     * @Method("POST")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('WanaworkUserBundle:User')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find User entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('user'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
    
    /**
     * Restore forgotten password
     * 
     * @Route("/users/restore-password", name="restore_password")
     * @Template()
     */
    public function requestRestorePasswordAction(Request $request) {
    	
    	$form = $this->createForm(new RestorePasswordType());
    	$successMessage = null;
    	$errorMessage = null;
    	
    	if($this->getRequest()->query->has('expired')) {
    	    $errorMessage = 'Your password restore token has expired. Please try again';
    	}
    	
    	if($request->isMethod('post')) {
    		$form->bind($request);
    		
    		if($form->isValid()) {

    			$accountsService = $this->get('wanawork.accounts_service');
    			$ip = $this->get('request')->getClientIp();
    			try {
    				$passwordReminder = $accountsService->restorePassword($form->getData(), $ip);
    				$successMessage = 'Please check your email for further instructions';
    			} catch(\Wanawork\UserBundle\Exception\EmailNotFound $e) {
    				$errorMessage = 'The provided email was not found';
    			} catch (\Exception $e) {
    				throw $e;
    			}
    		}
    	}
    	
    	return array(
    		'form' => $form->createView(),
    	    'successMessage' => $successMessage,
    	    'errorMessage' => $errorMessage,
    	);
    }
    
    /**
     * Reset password page
     * 
     * @Route("/users/reset-password/{token}", name="reset_password")
     * @Template()
     */
    public function resetPasswordAction($token) {
    	$request = $this->getRequest();
    	$em = $this->getDoctrine()->getManager();
    	
    	$passwordReminder = $em->getRepository('WanaworkUserBundle:PasswordReminder')->find($token);
    	if(!$passwordReminder instanceof PasswordReminder) {
    		throw $this->createNotFoundException();
    	}
    	
    	if($passwordReminder->isValid()) {
    		$ip = $this->get('request')->getClientIp();
    		$successMessage = null;
    		$form = $this->createForm(new NewPasswordType());
    		
    		if($this->getRequest()->isMethod('POST')) {
    			
    			$form->bind($this->getRequest());
    			if($form->isValid()) {
					$password = $form->getData()->getPassword();

					$accountsService = $this->get('wanawork.accounts_service');
					$accountsService->resetPassword($passwordReminder, $password, $ip);
					
					$successMessage = 'New Password Saved';
    			}
    		}
    		return array(
    			'form' => $form->createView(),
    		    'successMessage' => $successMessage,	
    		);
    		
    	} else {
    		return $this->redirect($this->generateUrl('restore_password') . '?expired');
    	}
    }
    
    /**
     * @Route("/users/{id}/change-password", name="user_change_password", requirements={"id"="\d+"})
     * @Secure(roles="ROLE_USER")
     * @Template()
     */
    public function changePasswordAction(User $user)
    {
        $security = $this->get('security.context');
        if (!$security->isGranted("EDIT", $user)) {
            throw new AccessDeniedException();
        }
        
        $isAdmin = $security->isGranted('ROLE_ADMIN');
        
        $encoderFactory = $this->get('security.encoder_factory');
        
        $container = new ChangePasswordContainer($user, $encoderFactory);
        $form = $this->createForm(new PasswordChangeType($isAdmin), $container, array(
        	'validation_groups' => $isAdmin === true ? 'admin' : 'user'
        ));
        
        $request = $this->getRequest();
        
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            
            if ($form->isValid()) {
                $encoder = $encoderFactory->getEncoder($user);
                $encodedPassword = $encoder->encodePassword($container->getNewPassword(), $user->getSalt());
                $user->setPassword($encodedPassword);
                $this->getDoctrine()->getManager()->flush();
                return $this->redirect($this->generateUrl('user_show', array('id' => $user->getId())));
            }
        }
        
        return array(
            'form' => $form->createView(),	
        );
    }
    	
    /** 
     * @Route("/users/profile/{id}/{name}", name="user_profile_generic")
     */
    public function profileAction($id, $name) {
    	$user = $this->getDoctrine()->getRepository('WanaworkUserBundle:User')->find($id);
    	if(!$user instanceof User) {
    		throw $this->createNotFoundException();
    	}
    	
    	$route = null;
    	if($user->getEmployeeProfile()) {
    		$route = 
    		$this->forward('WanaworkUserBundle:EmployeeProfile:show', array(
    			'id' => $user->getEmployeeProfile()->getId()
    		));
    	} elseif($user->getEmployerProfile()) {
    		
    		$route = 
    		$this->forward('WanaworkUserBundle:EmployerProfile:show', array(
    			'id' => $user->getEmployerProfile()->getId()
    		));
    	} else {
    		throw $this->createNotFoundException();
    	}
    	
    	return $route;
    }
    
    /**
     * @Route("/users/{id}/update-avatar.{_format}", name="update_avatar", 
     * 	defaults={"_format"="json"}, requirements={"id"="\d+", "_format"="json"})
     * @Method("POST")
     * @Secure(roles="ROLE_USER")
     */
    public function updateAvatar($id) 
    {
    	$em = $this->getDoctrine()->getManager();
    	$user = $this->getDoctrine()->getRepository('WanaworkUserBundle:User')->find($id);
    	if(!$user instanceof User) {
    		throw $this->createNotFoundException('Requested page not found');
    	}
    	
    	$security = $this->get('security.context');
    	if(!$security->isGranted('EDIT', $user)) {
    		throw new AccessDeniedException("You do not have permission to access this page");
    	}
    	
    	$profile = $user->getDefaultProfile();
    	
    	$file = isset($_FILES['avatar-file']) ? $_FILES['avatar-file'] : null;
    	if(!$file) {
    		throw $this->createNotFoundException('File not found');
    	}
    	$uploadedFile = new UploadedFile($file['tmp_name'], $file['name'], $file['type'], $file['size'], $file['error']);
    	
    	$data = array();
    	if($uploadedFile->isValid()) {
    		
    		try {
    			$em->getConnection()->beginTransaction();
    			
    			$validator = $this->get('validator');
    			$entity = null;
    			if($profile) {
    				$profile->setAvatarFile($uploadedFile);
    				$entity = $profile;
    			} else {
    				$entity = new TemporaryAvatar($uploadedFile, $user);
    			}
    			
    			$errors = $validator->validate($entity);
    			if(sizeof($errors)) {
    				throw new \Exception(implode(', ', $errors));
    			}
    			$em->persist($entity);
    			$em->flush();
    			
    			$avalanceService = $this->get('imagine.cache.path.resolver');
    			
    			$data['path'] = $avalanceService->getBrowserPath($entity->getWebPath(), 'thumbnail_192');
    			if($entity instanceof TemporaryAvatar) {
    				$data['id'] = $entity->getId();
    			}
    			
    			$em->getConnection()->commit();
    		} catch (\Exception $e) {
    			$em->getConnection()->rollback();
    			throw $e;
    		}
    		
    		
    	} else {
    		throw new \Exception('An error occurred while uploading the file. Please try again');
    	}
    	
    	return new JsonResponse($data);
    }
    
    /**
     * @Route("/users/{id}/update-setting.{_format}", name="user_update_setting", 
     * 	defaults={"_format"="json"}, requirements={"id"="\d+", "_format"="json"})
     * @ParamConverter("user", class="WanaworkUserBundle:user")
     * @Method("POST")
     * @Secure(roles="ROLE_USER")
     */
    public function updateSetting(User $user)
    {
        $security = $this->get('security.context');
        if (!$security->isGranted('EDIT', $user)) {
           throw new AccessDeniedException("You're not allowed to edit this account");
        }
        
        $returnData = array(
            'outcome' => false,
            'message' => '',            
        );
        
        $post = $this->getRequest()->request;
        if (!$post->has('key') || !$post->has('value')) {
            $returnData['msg'] = 'Key or Value is missing';
        } else {
            $user->addSetting($post->get('key'), $post->get('value'));
            $this->getDoctrine()->getManager()->flush();
            $returnData['outcome'] = true;
        }
        
        return new JsonResponse($returnData);
    }
    
    /**
     * @Route("/users/{id}/set-profile/{profile_id}", name="user_set_default_profile", requirements={"id"="\d+", "profile_id"="\d+"})
     * @ParamConverter("profile", class="WanaworkUserBundle:AbstractProfile", options={"id" = "profile_id"})
     * @Method("GET")
     * @Secure(roles="ROLE_USER")
     */
    public function setDefaultProfileAction(User $user, AbstractProfile $profile) 
    {
        
        if ($user->getEmployeeProfile() !== $profile && !$user->getEmployerProfiles()->contains($profile)) {
            throw new AccessDeniedException();  
        }
        
        $user->setDefaultProfile($profile);
        $this->getDoctrine()->getManager()->flush();

        $url = $this->generateUrl('my_account');
        if ($this->getRequest()->query->get('redirect')) {
            $url = $this->getRequest()->query->get('redirect');
        }
        
        return $this->redirect($url);
    }
    
    /**
     * @Route("/users/{id}/unsubscribe", name="user_unsubscribe", requirements={"id"="\d+"})
     * @param User $user
     * @Secure(roles="ROLE_USER")
     */
    public function unsubscribeAction(User $user)
    {
        $security = $this->get('security.context');
        
        if (!$security->isGranted('EDIT', $user)) {
            throw new AccessDeniedException();
        }
        
        $mailService = $this->get('wanawork.mail');
        $unsub = $mailService->unsubscribe($user->getEmail());
        
        return $this->redirect($this->generateUrl('user_email_subscriptions', array(
            'id' => $user->getId(),
        )));
    }
    
    /**
     * @Route("/users/{id}/resubscribe", name="user_resubscribe", requirements={"id"="\d+"})
     * @param User $user
     * @Secure(roles="ROLE_USER")
     */
    public function resubscribeAction(User $user)
    {
        $security = $this->get('security.context');
        
        if (!$security->isGranted('EDIT', $user)) {
            throw new AccessDeniedException();
        }
        
        $mailService = $this->get('wanawork.mail');
        $unsub = $mailService->resubscribe($user->getEmail());
        
        return $this->redirect($this->generateUrl('user_email_subscriptions', array(
        	'id' => $user->getId(),
        )));
    }
    
    /**
     * @Route("/users/{id}/email-subscriptions", name="user_email_subscriptions", requirements={"id"="\d+"})
     * @param User $user
     * @Secure(roles="ROLE_USER")
     * @Template()
     */
    public function subscriptionAction(User $user)
    {
        $security = $this->get('security.context');
        
        if (!$security->isGranted('EDIT', $user)) {
            throw new AccessDeniedException();
        }
        
        $mailService = $this->get('wanawork.mail');
        $isUnsubscribed = $mailService->isUnsubscribed($user->getEmail());
        
        return array(
            'isUnsubscribed' => $isUnsubscribed,
            'user' => $user,	
        );
    }
    
    /**
     * @Route("/users/{id}/ad-list", name="user_list_of_ads", requirements={"id"="\d+"})
     * @param User $user
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function adListAction(User $user)
    {
        $security = $this->get('security.context');
        
        if (!$security->isGranted('EDIT', $user)) {
            throw new AccessDeniedException();
        }
        
        $dql = "SELECT ad, cv, profile
				FROM WanaworkUserBundle:Ad ad
				LEFT JOIN ad.cv cv
				LEFT JOIN ad.profile profile
				LEFT JOIN profile.user user_1
                WHERE user_1 = :user
				ORDER BY ad.createdAt DESC";
        
        $query = $this->getDoctrine()->getManager()->createQuery($dql);
        $query->setParameter('user',$user);
        
        $perPage = 10;
        $pageNumber = $this->get('request')->query->get('page', 1);
        
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate($query, $pageNumber, $perPage);
        
        $this->get('wanawork.acl_service')->preloadAcls($pagination);
        
        $deleteForms = array();
        foreach($pagination as $p) {
            $deleteForms[] = $this->createDeleteForm($p->getId())->createView();
        }
        
        $doctrine = $this->getDoctrine()->getManager();
        $adOrderRepository = $doctrine->getRepository('Wanawork\UserBundle\Entity\Billing\AdOrder');
        
        return array(
            'user' => $user,
            'pagination' => $pagination,
            'delete_forms' => $deleteForms,
        );
    }
    
    /**
     * @Route("/users/{id}/cv-list", name="user_list_of_cvs", requirements={"id"="\d+"})
     * @param User $user
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function cvListAction(User $user)
    {
        $security = $this->get('security.context');
        
        if (!$security->isGranted('EDIT', $user)) {
            throw new AccessDeniedException();
        }
        
        $dql = "SELECT cv FROM WanaworkUserBundle:Cv cv
				LEFT JOIN cv.profile profile
				LEFT JOIN profile.user user_1
                WHERE user_1 = :user
				ORDER BY cv.created DESC";
        
        $query = $this->getDoctrine()->getManager()->createQuery($dql);
        $query->setParameter('user',$user);
        
        $perPage = 10;
        $pageNumber = $this->get('request')->query->get('page', 1);
        
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate($query, $pageNumber, $perPage);
        
        $this->get('wanawork.acl_service')->preloadAcls($pagination);
        
        $deleteForms = array();
        foreach($pagination as $p) {
            $deleteForms[] = $this->createDeleteForm($p->getId())->createView();
        }
        
        return array(
            'user' => $user,
            'pagination' => $pagination,
            'delete_forms' => $deleteForms
        );
    }
    
    /**
     * @Route("/users/new-employer", name="user_new_employer")
     * @Method({"GET", "POST"})
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function newEmployerAction()
    {
        $password = substr(md5(uniqid()), 0, rand(7,12));
        $em = $this->getDoctrine()->getManager();
        $role = $em->getRepository('WanaworkUserBundle:Role')->find(Role::EMPLOYER);
        $user = new User();
        $user->addRole($role);
        
        $user->setPassword($password);
        $form = $this->createForm(new EmployerWithGeneratedPasswordType(), $user);
        $request = $this->getRequest();
        
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            
            if ($form->isValid()) {
                
                try {
                    $em->getConnection()->beginTransaction();
                    
                    echo $rawPassword = $user->getPassword();
                    $encoder = $this->get('security.encoder_factory')->getEncoder($user);
                    $encodedPassword = $encoder->encodePassword($rawPassword, $user->getSalt());
                    $user->setPassword($encodedPassword);
                    $user->createAutoLoginToken();
                    
                    $em->persist($user);
                    $em->flush();
                    
                    $aclProvider = $this->get('security.acl.provider');
                    
                    // Grant permissions on User Object
                    $objectIdentity = ObjectIdentity::fromDomainObject($user);
                    $acl = $aclProvider->createAcl($objectIdentity);
                    $securityIdentity = UserSecurityIdentity::fromAccount($user);
                    
                    $acl->insertObjectAce($securityIdentity, MaskBuilder::MASK_OPERATOR);
                    $aclProvider->updateAcl($acl);
                    
                    $mailService = $this->get('wanawork.mail');
                    $mailService->emailAdminRegisteredEmployer($user, $rawPassword);
                    
                    $em->getConnection()->commit();
                    
                    return $this->redirect($this->generateUrl('users_list'));
                    
                } catch (\Exception $e) {
                    $em->getConnection()->rollback();
                    throw $e;
                }
                
            }
        }
        
        
        return array(
        	'form' => $form->createView(),
        );
        
    }
    
    /**
     * @Route("/users/token-login/{id}/{token}", name="user_auto_login", requirements={"id"="\d+", "token"="\w+"})
     */
    public function autoLoginAction(User $user, $token)
    {
        $redirect = null;
        if($user->getAutoLoginToken() === $token && $user->getAutoLoginTokenExpiry() > new \DateTime()) {
            $token = new UsernamePasswordToken($user, null, 'secured_area', $user->getRoles());
            $this->get('security.context')->setToken($token);
            
            $event = new InteractiveLoginEvent($this->getRequest(), $token);
            $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);
            $redirect = $this->generateUrl('my_employer_profile');            
        } else {
            $redirect = $this->generateUrl('login');
        }
        return $this->redirect($redirect);
    }
    
}
