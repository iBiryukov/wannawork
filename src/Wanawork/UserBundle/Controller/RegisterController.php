<?php
namespace Wanawork\UserBundle\Controller;

use Doctrine\Bundle\DoctrineBundle\Twig\DoctrineExtension;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Wanawork\UserBundle\Services\AccountService;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Wanawork\UserBundle\Entity\Role;
use Symfony\Component\HttpFoundation\Request;
use Wanawork\UserBundle\Entity\User;
use Wanawork\UserBundle\Form\Type\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Wanawork\UserBundle\Form\Type\CandidateType;
use Wanawork\UserBundle\Form\Type\EmployerType;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class RegisterController extends Controller 
{

    /**
     * Registration action
     * @param string $type
     * @throws \Exception
     * @Route("/register/{type}", name="wanawork_register", defaults={"type"=null}, requirements={"type"="employer|employee"})
     */
	public function registerAction($type = null)
	{
	    $accountsService = $this->get('wanawork.accounts_service');
	    
	    $candidate = new User();
	    $candidate->addRole($accountsService->getRoleForAccountType('employee'));
	    
	    $employer = new User();
	    $employer->addRole($accountsService->getRoleForAccountType('employer'));
	    
		$formEmployee = $this->createForm(new CandidateType(), $candidate);
		$formEmployer = $this->createForm(new EmployerType(), $employer);
		
		$request = $this->getRequest();
		if($request->isMethod('POST')) {
			
			$role = $accountsService->getRoleForAccountType($type);
			
			if(!$role instanceof Role) {
				throw $this->createNotFoundException('Account type not found');
			}
			
			if($role->getRole() === Role::EMPLOYEE) {
				$form = $formEmployee;
			} elseif($role->getRole() === Role::EMPLOYER) {
				$form = $formEmployer;
			} else {
			    throw new \Exception("Unknown role specified");
			}
			
			$form->bind($request);
			
			if($form->isValid()) {
				$user = $form->getData();
				$accountsService->register($user, array($role));
				$token = new UsernamePasswordToken($user, null, 'secured_area', $user->getRoles());
				$this->get('security.context')->setToken($token);
				
				$event = new InteractiveLoginEvent($request, $token);
				$this->get("event_dispatcher")->dispatch("security.interactive_login", $event);
				
				switch($type) {
					case 'employer':
					    return $this->redirect($this->generateUrl('register_success_employer'));
						break;
						
					case 'employee':
					    return $this->redirect($this->generateUrl('register_success_candidate'));
						break;
				}
			}
		}
		
		return $this->render('WanaworkUserBundle:Register:register.html.twig', array(
			'type' => $type,
			'form_employee' => $formEmployee->createView(),
			'form_employer' => $formEmployer->createView()
 		));
	}
	
	/**
	 * @Route("/register/success/candidate", name="register_success_candidate")
	 * @Template()
	 * @Method("GET")
	 */
	public function registrationSuccessCandidateAction()
	{
	    return array(
	    	
	    );
	}
	
	/**
	 * @Route("/register/success/employer", name="register_success_employer")
	 * @Template()
	 */
	public function registrationSuccessEmployerAction()
	{
	     return array(
	     	
	     );
	}
}
