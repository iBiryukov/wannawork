<?php
namespace Wanawork\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * Entity controller.
 * Used in admin panel only
 * @Route("/entity")
 */
class EntityController extends Controller
{
    /**
     * @Route("/", name="entity_index")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function indexAction()
    {
        return array();
    }
    
    /**
     * @Route("/sectors-professions.{_format}", name="entity_sectors", requirements={"_format"="html|file"}, 
     * defaults={"_format"="html"})
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function sectorsAction($_format)
    {
        $em = $this->getDoctrine()->getManager();
        $sectors = $em->getRepository('WanaworkUserBundle:Sector')->findBy(array(), array('name' => 'ASC'));
        $professions = $em->getRepository('WanaworkUserBundle:SectorJob')->findBy(array(), array('name' => 'ASC'));
        
        $response = null;
        if ($_format === 'file') {
            $sectorExporter = $this->get('wanawork.sectors_trader');
            $data1 = $sectorExporter->export();
            
            $sectorExporter = $this->get('wanawork.sector_jobs_trader');
            $data2 = $sectorExporter->export();
            
            
            $file = sys_get_temp_dir() . '/' . uniqid();
            $fp = fopen($file, 'w');
            fwrite($fp, prettyPrint(json_encode($data1)) . "\n\n\n" . prettyPrint(json_encode($data2)));
            fclose($fp);
            
            $response = new BinaryFileResponse(
        	    $file, $status = 200, $headers = array(), $public = false, 
        	    $contentDisposition = null, $autoEtag = false, $autoLastModified = true
        	);
            $response->headers->set('Content-Type', 'application/json');
            $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, "sectors_and_jobs_".date('Y_m_d_H_i').".json");
        } else {
            $response = array(
                'sectors' => $sectors,
                'professions' => $professions,
            );
        }

        return $response;
    }
}