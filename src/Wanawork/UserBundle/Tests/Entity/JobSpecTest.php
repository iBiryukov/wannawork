<?php
namespace Wanawork\UserBundle\Tests\Entity;

use Wanawork\UserBundle\Entity\JobSpec;
use Wanawork\UserBundle\Entity\User;
use Wanawork\UserBundle\Entity\EmployerProfile;
use Doctrine\Common\Collections\ArrayCollection;
use Wanawork\UserBundle\Entity\SectorJob;
use Wanawork\UserBundle\Entity\Sector;
use Wanawork\UserBundle\Entity\PositionType;
use Wanawork\MainBundle\Entity\County;
use Wanawork\MainBundle\Entity\Language;
use Wanawork\UserBundle\Entity\SectorExperience;
use Wanawork\UserBundle\Entity\Qualification;

class JobSpecTest extends WebUnitTestBase
{
    private $validator;
    
    private $jobSpec;
    
    /**
     *
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    
    protected function setUp()
    {
        $client = self::createClient();
        $container = $client->getContainer();
        $this->validator = $container->get('validator');
        $this->em = $container->get('doctrine')->getManager();
        $this->em->getConnection()->beginTransaction();
        
        $sectorJob = new SectorJob($id = -1, $name = 'test');
        $sectorJob->addSector(new Sector($id=-2, $name = 'test'));
        
        $jobSpec = new JobSpec();
        $jobSpec->setTitle($title = 'abc');
        $jobSpec->setDescription($description = 'abc');
        $jobSpec->setUser($user = new User());
        $jobSpec->setEmployer($employer = new EmployerProfile());
        $jobSpec->setProfession($sectorJob);
        
        $this->jobSpec = $jobSpec;
    }
    
    protected function tearDown()
    {
        $this->validator = null;
        $this->em->getConnection()->rollBack();
        $this->em->clear();
        $this->em->getConnection()->close();
    }
    
    public function testEntityOK()
    {
        $jobSpec = new JobSpec();
        
        $this->assertNull($jobSpec->getId());
        
        // Title
        $jobSpec->setTitle($title = 'abc');
        $this->assertSame($title, $jobSpec->getTitle());
        
        // description
        $jobSpec->setDescription($description = 'abc');
        $this->assertSame($description, $jobSpec->getDescription());

        // User
        $jobSpec->setUser($user = new User());
        $this->assertSame($user, $jobSpec->getUser());
        
        // Employer
        $jobSpec->setEmployer($employer = new EmployerProfile());
        $this->assertSame($employer, $jobSpec->getEmployer());
        
        $this->assertTrue($jobSpec->getCreatedOn() instanceof \DateTime);
        $this->assertEquals($jobSpec->getCreatedOn(), $jobSpec->getCreatedOn());
        $this->assertNotSame($jobSpec->getCreatedOn(), $jobSpec->getCreatedOn());
        
        $this->assertTrue($jobSpec->isPublic());
        
        $this->assertNull($jobSpec->getProfession());
        
        $this->assertTrue($jobSpec->getIndustries() instanceof ArrayCollection);
        $this->assertCount(0, $jobSpec->getIndustries());
        
        $this->assertTrue($jobSpec->getPositions() instanceof ArrayCollection);
        $this->assertCount(0, $jobSpec->getPositions());
        
        $this->assertTrue($jobSpec->getLocations() instanceof ArrayCollection);
        $this->assertCount(0, $jobSpec->getLocations());
        
        $this->assertTrue($jobSpec->getLanguages() instanceof ArrayCollection);
        $this->assertCount(0, $jobSpec->getLanguages());
        
        $this->assertNull($jobSpec->getExperience());
        
        $this->assertNull($jobSpec->getEducationLevel());
        
        $this->assertNull($jobSpec->getSalary());
    }
    
    public function testTitleIsRequired()
    {
        $jobSpec = clone $this->jobSpec;
        
        $jobSpec->setTitle($title = '');
        
        $validator = $this->validator;
        $errors = $validator->validate($jobSpec);
        
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('title', $errors[0]->getPropertyPath());
    }
    
    public function testTitleIsTooLong()
    {
        // 1 above
        $jobSpec = clone $this->jobSpec;
        
        $jobSpec->setTitle($title = str_repeat('a', 256));
        
        $validator = $this->validator;
        $errors = $validator->validate($jobSpec);
        
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('title', $errors[0]->getPropertyPath());
        
        // exact
        $jobSpec->setTitle($title = str_repeat('a', 255));
        
        $validator = $this->validator;
        $errors = $validator->validate($jobSpec);
        
        $this->assertCount(0, $errors, $this->validationErrorsToString($errors));
    }
    
    public function testDescriptionIsRequired()
    {
        $jobSpec = clone $this->jobSpec;
        
        $jobSpec->setDescription($description = '');
        
        $validator = $this->validator;
        $errors = $validator->validate($jobSpec);
        
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('description', $errors[0]->getPropertyPath());
    }
    
    public function testDescriptionIsTooLong()
    {
        // 1 above
        $jobSpec = clone $this->jobSpec;
    
        $jobSpec->setDescription($description = str_repeat('a', 50001));
    
        $validator = $this->validator;
        $errors = $validator->validate($jobSpec);
    
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('description', $errors[0]->getPropertyPath());
    
        // exact
        $jobSpec->setDescription($description = str_repeat('a', 50000));
    
        $validator = $this->validator;
        $errors = $validator->validate($jobSpec);
    
        $this->assertCount(0, $errors, $this->validationErrorsToString($errors));
    }
    
    public function testUserIsRequired()
    {
        $jobSpec = clone $this->jobSpec;
    
        $jobSpec->setUser($user = null);
    
        $validator = $this->validator;
        $errors = $validator->validate($jobSpec);
    
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('user', $errors[0]->getPropertyPath());
    }
    
    public function setUserOfCorrectTypeIsRequired()
    {
        $jobSpec = clone $this->jobSpec;
        
        $jobSpec->setUser($user = new \DateTime());
        
        $validator = $this->validator;
        $errors = $validator->validate($jobSpec);
        
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('user', $errors[0]->getPropertyPath());
    }
    
    public function testEmployerIsRequired()
    {
        $jobSpec = clone $this->jobSpec;
    
        $jobSpec->setEmployer($employer = null);
    
        $validator = $this->validator;
        $errors = $validator->validate($jobSpec);
    
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('employer', $errors[0]->getPropertyPath());
    }
    
    public function testSetEmployerOfCorrectTypeIsRequired()
    {
        $jobSpec = clone $this->jobSpec;
    
        $jobSpec->setEmployer(new \DateTime());
    
        $validator = $this->validator;
        $errors = $validator->validate($jobSpec);
    
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('employer', $errors[0]->getPropertyPath());
    }
    
    public function testProfessionIsRequired()
    {
        $jobSpec = clone $this->jobSpec;
        $jobSpec->setProfession(null);
        
        $validator = $this->validator;
        $errors = $validator->validate($jobSpec);
        
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('profession', $errors[0]->getPropertyPath());
    }
    
    public function testIndustries()
    {
        $jobSpec = clone $this->jobSpec;
        $this->assertTrue($jobSpec->getIndustries() instanceof ArrayCollection);
        $this->assertCount(0, $jobSpec->getIndustries());
    
        $industry = new Sector(1, 'abc');
        $jobSpec->addIndustry($industry);
        $this->assertCount(1, $jobSpec->getIndustries());
        $this->assertContainsOnly($industry, $jobSpec->getIndustries());
        $jobSpec->removeIndustry($industry);
        $this->assertCount(0, $jobSpec->getIndustries());
    
        $jobSpec->addIndustry(new Sector(1, 'abc'));
        $jobSpec->setIndustries(array($industry));
        $this->assertCount(1, $jobSpec->getIndustries());
        $this->assertContainsOnly($industry, $jobSpec->getIndustries());
        $jobSpec->addIndustry($industry);
        $this->assertCount(1, $jobSpec->getIndustries());
    }
    
    public function testPositions()
    {
        $jobSpec = clone $this->jobSpec;
        $this->assertTrue($jobSpec->getPositions() instanceof ArrayCollection);
        $this->assertCount(0, $jobSpec->getPositions());
    
        $position = new PositionType($id = -1, $name = 'test', $order = 1);
        $jobSpec->addPosition($position);
        $this->assertCount(1, $jobSpec->getPositions());
        $this->assertContainsOnly($position, $jobSpec->getPositions());
        $jobSpec->removePosition($position);
        $this->assertCount(0, $jobSpec->getPositions());
    
        $jobSpec->addPosition(new PositionType($id = -1, $name = 'test2', $order = 2));
        $jobSpec->setPositions(array($position));
        $this->assertCount(1, $jobSpec->getPositions());
        $this->assertContainsOnly($position, $jobSpec->getPositions());
        $jobSpec->addPosition($position);
        $this->assertCount(1, $jobSpec->getPositions());
    }
    
    
    public function testLocations()
    {
        $jobSpec = clone $this->jobSpec;
        $this->assertTrue($jobSpec->getLocations() instanceof ArrayCollection);
        $this->assertCount(0, $jobSpec->getLocations());
    
        $location = new County(-1, $name = 'test');
        $jobSpec->addLocation($location);
        $this->assertCount(1, $jobSpec->getLocations());
        $this->assertContainsOnly($location, $jobSpec->getLocations());
        $jobSpec->removeLocation($location);
        $this->assertCount(0, $jobSpec->getLocations());
    
        $jobSpec->addLocation(new County(-1, $name = 'test2'));
        $jobSpec->setLocations(array($location));
        $this->assertCount(1, $jobSpec->getLocations());
        $this->assertContainsOnly($location, $jobSpec->getLocations());
        $jobSpec->addLocation($location);
        $this->assertCount(1, $jobSpec->getLocations());
    }
    
    public function testLanguages()
    {
        $jobSpec = clone $this->jobSpec;
        $this->assertTrue($jobSpec->getLanguages() instanceof ArrayCollection);
        $this->assertCount(0, $jobSpec->getLanguages());
    
        $language = new Language($id = -1, 'test');
        $jobSpec->addLanguage($language);
        $this->assertCount(1, $jobSpec->getLanguages());
        $this->assertContainsOnly($language, $jobSpec->getLanguages());
        $jobSpec->removeLanguage($language);
        $this->assertCount(0, $jobSpec->getLanguages());
    
        $jobSpec->addLanguage(new Language($id = -1, $name = 'test2'));
        $jobSpec->setLanguages(array($language));
        $this->assertCount(1, $jobSpec->getLanguages());
        $this->assertContainsOnly($language, $jobSpec->getLanguages());
        $jobSpec->addLanguage($language);
        $this->assertCount(1, $jobSpec->getLanguages());
    }
    
    public function testPublic()
    {
        $jobSpec = new JobSpec();
        $this->assertTrue($jobSpec->isPublic());
        
        $jobSpec->setPublic(false);
        $this->assertFalse($jobSpec->isPublic());
    }
    
    public function testSetExperience()
    {
        $jobSpec = clone $this->jobSpec;
        $this->assertNull($jobSpec->getExperience());
        
        $experience = new SectorExperience($id = 1, $name = 'test');
        $jobSpec->setExperience($experience);
        
        $this->assertSame($experience, $jobSpec->getExperience());
    }
    
    public function testSetEducationLevel()
    {
        $jobSpec = clone $this->jobSpec;
        $this->assertNull($jobSpec->getEducationLevel());
        
        $educationLevel = new Qualification($id = -1, $name = 'test', $isPopular = true);
        $jobSpec->setEducationLevel($educationLevel);
        
        $this->assertSame($educationLevel, $jobSpec->getEducationLevel());
    }
    
    public function testSetSalary()
    {
        $jobSpec = clone $this->jobSpec;
        $this->assertNull($jobSpec->getSalary());
        
        $salary = 'test';
        $jobSpec->setSalary($salary);
        
        $this->assertSame($salary, $jobSpec->getSalary());
    }
}