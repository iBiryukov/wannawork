<?php
namespace Wanawork\UserBundle\Tests\Entity;

use Wanawork\UserBundle\Entity\Education;
use Wanawork\UserBundle\Entity\CV;
use Wanawork\UserBundle\Entity\CVForm;
class EducationTest extends WebUnitTestBase
{
    private $validator;
    
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    
    protected function setUp()
    {
        $client = self::createClient();
        $container = $client->getContainer();
        $this->validator = $container->get('validator');
        $this->em = $container->get('doctrine')->getManager();
        $this->em->getConnection()->beginTransaction();
    }
    
    protected function tearDown()
    {
        $this->validator = null;
        $this->em->getConnection()->rollBack();
        $this->em->clear();
        $this->em->getConnection()->close();
    }
    
    public function testEntityOk()
    {
        $education = new Education();
        
        $this->assertNull($education->getId());
        
        // Start 
        $education->setStart($start = new \DateTime());
        $this->assertEquals($start, $education->getStart());
        $this->assertNotSame($start, $education->getStart());
        
        // Finish
        $education->setFinish($finish = new \DateTime());
        $this->assertEquals($finish, $education->getFinish());
        $this->assertNotSame($finish, $education->getFinish());
        
        // College 
        $education->setCollege($college = 'Carlow IT');
        $this->assertSame($college, $education->getCollege());
        
        // Course
        $education->setCourse($course = 'Soft Dev');
        $this->assertSame($course, $education->getCourse());
        
        // Award 
        $education->setAward($award = 'Degree');
        $this->assertSame($award, $education->getAward());
        
        // CV
        $education->setCv($cv = new CVForm());
        $this->assertSame($cv, $education->getCv());
    }
    
    public function testStartDateMissing()
    {
        $education = new Education();
        $education->setFinish($finish = new \DateTime());
        $education->setCollege($college = 'Carlow IT');
        $education->setCourse($course = 'Soft Dev');
        $education->setAward($award = 'Degree');
        $education->setCv($cv = new CVForm());
        
        $validator = $this->validator;
        $errors = $validator->validate($education);
                
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('start', $errors[0]->getPropertyPath());
    }
    
    public function testFinishDateMissing()
    {
        $education = new Education();
        $education->setStart($start = new \DateTime());
        $education->setCollege($college = 'Carlow IT');
        $education->setCourse($course = 'Soft Dev');
        $education->setAward($award = 'Degree');
        $education->setCv($cv = new CVForm());
    
        $validator = $this->validator;
        $errors = $validator->validate($education);
    
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('finish', $errors[0]->getPropertyPath());
    }
    
    public function testStartDateGreaterThanFinishDate()
    {
        $education = new Education();
        $education->setStart($start = new \DateTime('tomorrow'));
        $education->setFinish($finish = new \DateTime());
        $education->setCollege($college = 'Carlow IT');
        $education->setCourse($course = 'Soft Dev');
        $education->setAward($award = 'Degree');
        $education->setCv($cv = new CVForm());
    
        $validator = $this->validator;
        $errors = $validator->validate($education);
    
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('finish', $errors[0]->getPropertyPath());
    }
    
    public function testStartAndFinishDatesCannotBeEqual()
    {
        $date = new \DateTime();
        $education = new Education();
        $education->setStart($start = $date);
        $education->setFinish($finish = $date);
        $education->setCollege($college = 'Carlow IT');
        $education->setCourse($course = 'Soft Dev');
        $education->setAward($award = 'Degree');
        $education->setCv($cv = new CVForm());
        
        $validator = $this->validator;
        $errors = $validator->validate($education);
        
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('finish', $errors[0]->getPropertyPath());
        
    }
    
    public function testCollegeNameIsMissing()
    {
        $education = new Education();
        $education->setStart($start = new \DateTime());
        $education->setFinish($finish = new \DateTime('tomorrow'));
        $education->setCourse($course = 'Soft Dev');
        $education->setAward($award = 'Degree');
        $education->setCv($cv = new CVForm());
        
        $validator = $this->validator;
        $errors = $validator->validate($education);
        
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('college', $errors[0]->getPropertyPath());
    }
    
    public function testCollegeNameIsTooLong()
    {
        // 1 above
        $education = new Education();
        $education->setStart($start = new \DateTime());
        $education->setFinish($finish = new \DateTime('tomorrow'));
        $education->setCollege($college = str_repeat('a', 201));
        $education->setCourse($course = 'Soft Dev');
        $education->setAward($award = 'Degree');
        $education->setCv($cv = new CVForm());
        
        $validator = $this->validator;
        $errors = $validator->validate($education);
        
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('college', $errors[0]->getPropertyPath());
        
        // boundary
        $education->setCollege($college = str_repeat('a', 200));
        $errors = $validator->validate($education);
        $this->assertCount(0, $errors, $this->validationErrorsToString($errors));
    }
    
    public function testCourseNameIsMissing()
    {
        $education = new Education();
        $education->setStart($start = new \DateTime());
        $education->setFinish($finish = new \DateTime('tomorrow'));
        $education->setCollege($college = 'Carlow IT');
        $education->setAward($award = 'Degree');
        $education->setCv($cv = new CVForm());
    
        $validator = $this->validator;
        $errors = $validator->validate($education);
    
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('course', $errors[0]->getPropertyPath());
    }
    
    public function testCourseNameIsTooLong()
    {
        // 1 above
        $education = new Education();
        $education->setStart($start = new \DateTime());
        $education->setFinish($finish = new \DateTime('tomorrow'));
        $education->setCollege('abc');
        $education->setCourse($course = str_repeat('a', 201));
        $education->setAward($award = 'Degree');
        $education->setCv($cv = new CVForm());
    
        $validator = $this->validator;
        $errors = $validator->validate($education);
    
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('course', $errors[0]->getPropertyPath());
    
        // boundary
        $education->setCourse($college = str_repeat('a', 200));
        $errors = $validator->validate($education);
        $this->assertCount(0, $errors, $this->validationErrorsToString($errors));
    }
    
    public function testAwardNameIsMissing()
    {
        $education = new Education();
        $education->setStart($start = new \DateTime());
        $education->setFinish($finish = new \DateTime('tomorrow'));
        $education->setCollege($college = 'Carlow IT');
        $education->setCourse($course = 'abc');
        $education->setCv($cv = new CVForm());
    
        $validator = $this->validator;
        $errors = $validator->validate($education);
    
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('award', $errors[0]->getPropertyPath());
    }
    
    public function testAwardNameIsTooLong()
    {
        // 1 above
        $education = new Education();
        $education->setStart($start = new \DateTime());
        $education->setFinish($finish = new \DateTime('tomorrow'));
        $education->setCollege('abc');
        $education->setCourse($course = 'aa');
        $education->setAward($award = str_repeat('a', 201));
        $education->setCv($cv = new CVForm());
    
        $validator = $this->validator;
        $errors = $validator->validate($education);
    
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('award', $errors[0]->getPropertyPath());
    
        // boundary
        $education->setAward( str_repeat('a', 200));
        $errors = $validator->validate($education);
        $this->assertCount(0, $errors, $this->validationErrorsToString($errors));
    }
    
    public function testEmptyCV()
    {
        $education = new Education();
        $education->setStart($start = new \DateTime());
        $education->setFinish($finish = new \DateTime('tomorrow'));
        $education->setCollege($college = 'Carlow IT');
        $education->setCourse($course = 'abc');
        $education->setAward($award = 'aa');
        
        $validator = $this->validator;
        $errors = $validator->validate($education);
        
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('cv', $errors[0]->getPropertyPath());
    }
    
    public function testDurationUsesGenericDurationCalculator()
    {
        $education = new Education();
        $education->setStart($startDate = new \DateTime('2012-10-01'));
        $education->setFinish($finishDate = new \DateTime('2012-12-01'));
        
        $dateCalculatorMock = $this->getMockClass('\Wanawork\UserBundle\Util\Date', array(
        	'diffInMonthsAndYears'
        ));
        
        $dateCalculatorMock::staticExpects($this->once())
            ->method('diffInMonthsAndYears')
            ->with($this->equalTo($finishDate), $this->equalTo($startDate));
        
        $education->dateUtility = ($dateCalculatorMock);
        $education->getDuration();
    }
}