<?php
namespace Wanawork\UserBundle\Tests\Entity;

use Wanawork\UserBundle\Entity\AdView;
use Wanawork\UserBundle\Entity\Ad;
use Wanawork\UserBundle\Entity\User;
use Wanawork\UserBundle\Entity\EmployerProfile;
class AdViewTest extends WebUnitTestBase
{
    
    public function testEntityOk()
    {
        $userEmployer = new User();
        $profileEmployer = new EmployerProfile();
        $userEmployer->addEmployerProfile($profileEmployer, $isAdmin = true);
        
        $ad = new Ad();
        $type = AdView::VIEW_IN_SEARCH;
        $adView = new AdView($ad, $type, $userEmployer, $profileEmployer);
        
        $this->assertNull($adView->getId());
        
        // Ad
        $this->assertSame($ad, $adView->getAd());
        
        // user
        $this->assertSame($userEmployer, $adView->getUser());
        
        // Employer
        $this->assertSame($profileEmployer, $adView->getEmployerProfile());
        
        // Timestamp
        $this->assertInstanceOf('\DateTime', $adView->getTimestamp());
        $this->assertNotSame($adView->getTimestamp(), $adView->getTimestamp());
        
        // Type
        $this->assertSame($type, $adView->getType());
    }
    
    /** 
     * @expectedException \InvalidArgumentException
     */
    public function testInvalidTypeThrowsException()
    {
        $ad = new Ad();
        new AdView($ad, $type = -1);
    }
}