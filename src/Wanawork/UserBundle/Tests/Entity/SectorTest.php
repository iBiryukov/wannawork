<?php
namespace Wanawork\UserBundle\Tests\Entity;

use Wanawork\UserBundle\Entity\Sector;
use Symfony\Component\Validator\Validation;
use Wanawork\UserBundle\Entity\SectorJob;

/**
 * Unit tests for Sector entity
 * 
 * @author Ilya Biryukov <ilya@wanawork.ie>
 */
class SectorTest extends WebUnitTestBase
{
    /**
     * Validator
     * @var \Symfony\Component\Validator\ValidatorInterface
     */
    private $validator;
    
    
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    
    public function setUp()
    {
        $client = self::createClient();
        $container = $client->getContainer();
        $this->validator = $container->get('validator');
        $this->em = $container->get('doctrine')->getManager();
        $this->em->getConnection()->beginTransaction();
    }
    
    public function tearDown()
    {
        $this->validator = null;
        $this->em->getConnection()->rollBack();
        $this->em->clear();
        $this->em->getConnection()->close();
    }
    
    public function testEntity()
    {
        $sector = new Sector(1, 'test');
        $sectorJob = new SectorJob(-1, 'test');
        $sector->addJob($sectorJob);
        
        $this->assertSame(1, $sector->getId());
        $this->assertSame('test', $sector->getName());
        $this->assertEquals(1, $sector->getJobs()->count());
        
        $this->assertTrue(
	       is_array($sector->__toArray())
        );
    }
    
    public function testConstructorWithJobs()
    {
        $job = new SectorJob(-1, 'test');
        $sector = new Sector(1, 'test', array(
        	$job
        ));
        
        $this->assertCount(1, $sector->getJobs());
        $this->assertSame($sector->getJobs()->count(), $job->getSectors()->count());
        $this->assertContainsOnly($job, $sector->getJobs());
    }
    
    public function testToStringMethodEqualsToName()
    {
        $sector = new Sector(1, 'test');
        $this->assertSame($sector->getName(), (string)$sector);
        
    }
    
    public function testValidationOK()
    {
        
        $validator = $this->validator;
        
        $sector = new Sector(1, 'test');
        $sectorJob = new SectorJob(-1, 'test');
        $sector->addJob($sectorJob);
        
        $errors = $validator->validate($sector);
        $this->assertTrue(sizeof($errors) === 0, $this->validationErrorsToString($errors));
    }
    
    public function testNullId()
    {
        $validator = $this->validator;
        $sector = new Sector(null, 'test');
        $sectorJob = new SectorJob(-1, 'test');
        $sector->addJob($sectorJob);
        
        $errors = $validator->validate($sector);
        $this->assertTrue(sizeof($errors) === 1, 
            "Expected 1 error, got: " . sizeof($errors) . "\n" . $this->validationErrorsToString($errors));
        $this->assertSame('id', $errors[0]->getPropertyPath());
    }
    
    
//     public function testStringId()
//     {
//         $validator = $this->validator;
//         $sector = new Sector('test', 'test');
//         $sectorJob = new SectorJob(-1, 'test');
//         $sector->addJob($sectorJob);
    
//         $errors = $validator->validate($sector);
//         $this->assertTrue(sizeof($errors) === 1, 
//             "Expected 1 error, got: " . sizeof($errors) . "\n" . $this->validationErrorsToString($errors));
//         $this->assertSame('id', $errors[0]->getPropertyPath());
//     }
    
//     public function testEmptyStringId()
//     {
//         $validator = $this->validator;
//         $sector = new Sector('', 'test');
//         $sectorJob = new SectorJob(-1, 'test');
//         $sector->addJob($sectorJob);
    
//         $errors = $validator->validate($sector);
//         $this->assertTrue(sizeof($errors) === 1, 
//             "Expected 1 error, got: " . sizeof($errors) . "\n" . $this->validationErrorsToString($errors));
//         $this->assertSame('id', $errors[0]->getPropertyPath());
//     }
    
    public function testEmptyName()
    {
        $validator = $this->validator;
        $sector = new Sector(1, '');
        $sectorJob = new SectorJob(-1, 'test');
        $sector->addJob($sectorJob);
    
        $errors = $validator->validate($sector);
        $this->assertTrue(sizeof($errors) === 1, 
            "Expected 1 error, got: " . sizeof($errors) . "\n" . $this->validationErrorsToString($errors));
        $this->assertSame('name', $errors[0]->getPropertyPath());
    }  

    public function testInvalidNameType()
    {
        $validator = $this->validator;
        $sector = new Sector(1, 123);
        $sectorJob = new SectorJob(-1, 'test');
        $sector->addJob($sectorJob);
    
        $errors = $validator->validate($sector);
        $this->assertTrue(sizeof($errors) === 1, 
            "Expected 1 error, got: " . sizeof($errors) . "\n" . $this->validationErrorsToString($errors));
        $this->assertSame('name', $errors[0]->getPropertyPath());
    }
    
    public function testLongName()
    {
        $validator = $this->validator;
        $sector = new Sector(1, str_repeat('a', 256));
        $sectorJob = new SectorJob(-1, 'test');
        $sector->addJob($sectorJob);
    
        $errors = $validator->validate($sector);
        $this->assertTrue(sizeof($errors) === 1, 
            "Expected 1 error, got: " . sizeof($errors) . "\n" . $this->validationErrorsToString($errors));
        $this->assertSame('name', $errors[0]->getPropertyPath());
    }
    
    public function testEmptyJobs()
    {
        $validator = $this->validator;
        $sector = new Sector(1, 'name');
        $sectorJob = new SectorJob(-1, 'test');
    
        $errors = $validator->validate($sector);
        $this->assertTrue(sizeof($errors) === 1, 
            "Expected 1 error, got: " . sizeof($errors) . "\n" . $this->validationErrorsToString($errors));
        $this->assertSame('jobs', $errors[0]->getPropertyPath());
    } 

    public function testReferencialIntegrity()
    {
        $validator = $this->validator;
        $sector = new Sector(1, 'name');
        $sectorJob = new SectorJob(-1, 'test');
        
        $this->assertCount(0, $sector->getJobs());
        $this->assertSame($sectorJob->getSectors()->count(), $sector->getJobs()->count());
        $sector->addJob($sectorJob);
        
        $this->assertCount(1, $sector->getJobs());
        $this->assertSame($sectorJob->getSectors()->count(), $sector->getJobs()->count());
        
        $sector->removeJob($sectorJob);
        $this->assertCount(0, $sector->getJobs());
        $this->assertSame($sectorJob->getSectors()->count(), $sector->getJobs()->count());
    }
}