<?php
namespace Wanawork\UserBundle\Tests\Entity;

use Wanawork\UserBundle\Entity\MailThread;
use Wanawork\UserBundle\Entity\CvRequest;
use Wanawork\UserBundle\Entity\User;
use Wanawork\UserBundle\Entity\EmployerProfile;
use Wanawork\UserBundle\Entity\Ad;
use Wanawork\UserBundle\Entity\EmployeeProfile;
use Wanawork\UserBundle\Entity\Message;
use Wanawork\UserBundle\Entity\JobSpec;
use Wanawork\UserBundle\Tests\Entity\Mocks\EmployerProfileMock;

class MailThreadTest extends WebUnitTestBase
{

    public function testEntityOk()
    {
        $user = new User();
        $employerProfile = new EmployerProfileMock();
        $employerProfile->setId(-1);
        
        $user->addEmployerProfile($employerProfile, $isAdmin = true);
        $ad = new Ad();
        
        $employeeProfile = new EmployeeProfile();
        $ad->setProfile($employeeProfile);
        $cvRequest = new CvRequest($user, $ad, $employerProfile, new JobSpec());
        
        $thread = new MailThread($cvRequest);
        $this->assertNull($thread->getId());
        
        // Messages
        $this->assertInstanceOf('\Doctrine\Common\Collections\ArrayCollection', $thread->getMessages());
        $this->assertCount(0, $thread->getMessages());
        
        $message = new Message($text = 'text', $author = $user, $profile = $employerProfile, $thread);
        
        $updatedOn = $thread->getUpdatedOn();
        sleep(1); // to ensure that updated timestamp changes
        $thread->addMessage($message);
        $this->assertNotEquals($updatedOn, $thread->getUpdatedOn());
        
        $this->assertCount(1, $thread->getMessages());
        
        $this->assertCount(1, $thread->getUnreadMessages());
        $this->assertCount(1, $thread->getUnreadMessages($employeeProfile));
        $this->assertCount(0, $thread->getUnreadMessages($employerProfile));
        
        $thread->markReadForUser($employerProfile);
        $this->assertCount(1, $thread->getUnreadMessages());
        $this->assertCount(1, $thread->getUnreadMessages($employeeProfile));
        $this->assertCount(0, $thread->getUnreadMessages($employerProfile));
        
        $thread->markReadForUser($employeeProfile);
        $this->assertCount(0, $thread->getUnreadMessages());
        $this->assertCount(0, $thread->getUnreadMessages($employeeProfile));
        $this->assertCount(0, $thread->getUnreadMessages($employerProfile));
        
        $thread->removeMessage($message);
        $this->assertCount(0, $thread->getMessages());
        
        // Created On
        $this->assertInstanceOf('\DateTime', $thread->getCreatedOn());
        $this->assertNotSame($thread->getCreatedOn(), $thread->getCreatedOn());
        $this->assertEquals($thread->getCreatedOn(), $thread->getcreatedOn());
        
        // Updated On
        $this->assertInstanceOf('\DateTime', $thread->getUpdatedOn());
        $this->assertNotSame($thread->getUpdatedOn(), $thread->getUpdatedOn());
        $this->assertEquals($thread->getUpdatedOn(), $thread->getUpdatedOn());
        
        $thread->setUpdatedOn($date = null);
        $this->assertInstanceOf('\DateTime', $thread->getUpdatedOn());
        
        // Employee
        $this->assertSame($employeeProfile, $thread->getEmployee());
        $this->assertSame($ad->getProfile(), $thread->getEmployee());
        
        // Employer
        $this->assertSame($employerProfile, $thread->getEmployer());
        
        // CV Request 
        $this->assertSame($cvRequest, $thread->getCvRequest());
    }
    
    // @todo
//     public function testNonLastMessageRemoval()
//     {
//         $user = new User();
//         $employerProfile = new EmployerProfile();
//         $user->addEmployerProfile($employerProfile, $isAdmin = true);
//         $ad = new Ad();
        
//         $employeeProfile = new EmployeeProfile();
//         $ad->setProfile($employeeProfile);
//         $cvRequest = new CvRequest($user, $ad, $employerProfile, new JobSpec());
        
//         $thread = new MailThread($cvRequest);
        
//         $message1 = new Message($text = 'text', $author = $user, $profile = $employerProfile, $thread);
//         $message2 = new Message($text = 'text', $author = $user, $profile = $employerProfile, $thread);
        
//         $thread->addMessage($message1);
//         $thread->addMessage($message2);
        
//     }
    
}