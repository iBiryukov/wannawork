<?php
namespace Wanawork\UserBundle\Tests\Entity;

use Wanawork\UserBundle\Entity\SearchNotificationCheck;
use Wanawork\UserBundle\Entity\SearchNotification;
use Wanawork\UserBundle\Entity\Search;
use Doctrine\Common\Collections\ArrayCollection;

class SearchNotificationCheckTest extends WebUnitTestBase
{
    public function testConstructor()
    {
        $search = new Search();
        $name = 'test';
        $notification = new SearchNotification($search, $name);
        $check = new SearchNotificationCheck($notification, $newAds = array());
        
        $this->assertNull($check->getId());
        
        $this->assertTrue($check->getDate() instanceof \DateTime);
        $this->assertEquals($check->getDate(), $check->getDate());
        $this->assertNotSame($check->getDate(), $check->getDate());
        
        $this->assertSame($notification, $check->getNotification());
        
        $this->assertCount(sizeof($newAds), $check->getNewAds());
        
        $this->assertTrue($check->getNewAds() instanceof ArrayCollection);
    }
    
}