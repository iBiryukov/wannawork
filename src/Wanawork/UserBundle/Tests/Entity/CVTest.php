<?php
namespace Wanawork\UserBundle\Tests\Entity;

use Wanawork\UserBundle\Entity\EmployeeProfile;
use Wanawork\UserBundle\Entity\CV;
use Doctrine\Common\Collections\ArrayCollection;
use Wanawork\UserBundle\Entity\Education;
use Wanawork\UserBundle\Entity\Reference;
use Wanawork\UserBundle\Entity\WorkExperience;
use Wanawork\UserBundle\Entity\Ad;
use Wanawork\MainBundle\Entity\County;
use Wanawork\MainBundle\Entity\NameTitle;
use Wanawork\UserBundle\Entity\User;
use Wanawork\UserBundle\Entity\CVForm;

class CVTest extends WebUnitTestBase
{
    private $validator;
    
    private $education;
    
    private $reference;
    
    private $workExperience;
    
    private $cv;
    
    private $em;
    
    protected function setUp()
    {
        $client = self::createClient();
        $this->validator = $client->getContainer()->get('validator');
        
        $this->education = $education = new Education();
        $education->setStart(new \DateTime());
        $education->setFinish(new \DateTime('tomorrow'));
        $education->setCollege($college = 'abc');
        $education->setAward($award = 'abc');
        $education->setCourse($course = 'abc');
        
        $this->reference = $reference = new Reference();
        $reference->setContact($contact = 'abc');
        $reference->setEmail($email = 'a@a.com');
        $reference->setEmployer($employer = 'abc');
        $reference->setTelephone($telephone = 123456);
        
        $this->workExperience = $workExperience = new WorkExperience();
        $workExperience->setCompany($company = 'abc');
        $workExperience->setDescription($description = 'abc');
        $workExperience->setStart(new \DateTime());
        $workExperience->setFinish(new \DateTime('tomorrow'));
        $workExperience->setPosition($position = 'abc');
        
        $profile = new EmployeeProfile();
        $profile->setName($name = 'abc');
        $profile->setAddress($address = 'abc');
        $profile->setCity($city = 'abc');
        $profile->setCounty(new County(-1, 'abc'));
        $profile->setDob(new \DateTime());
        $profile->setPhoneNumber($phoneNumber = 1234566);
        $profile->setTitle(new NameTitle(-1, 'abc'));
        
        $user = new User();
        $user->setName($name = 'abc');
        $user->setPassword($password = 'abc');
        $user->setEmail($email = 'a@a.com');
        $user->setEmployeeProfile($profile);
        
        $this->cv = $cv = new CVForm();
        $cv->setName($name = 'abc');
        $cv->setProfile($profile);
        $cv->addEducation($education = $this->education);
        $cv->addReference($reference = $this->reference);
        $cv->addJob($this->workExperience);
        $cv->setAchievements($achievements = 'abc');
        $cv->setCoverNote($coverNote = 'test');
        
        $this->em = $client->getContainer()->get('doctrine')->getManager();
        $this->em->getConnection()->beginTransaction();
    }
    
    protected function tearDown()
    {
        $this->em->clear();
        $this->em->getConnection()->rollback();
        $this->em->getConnection()->close();
    }
    
    public function testId()
    {
        $cv = new CVForm();
        $this->assertNull($cv->getId());
    }
    
    public function testName()
    {
        $cv = new CVForm();
        $cv->setName($name = 'name');
        $this->assertSame($name, $cv->getName());
    }
    
    public function testProfile()
    {
        $cv = new CVForm();
        $cv->setProfile($profile = new EmployeeProfile());
        $this->assertSame($profile, $cv->getProfile());
        $this->assertCount(1, $profile->getCvs());
        $this->assertSame($cv, $profile->getCvs()->first());
        
        $cv->setProfile(null);
        $this->assertNull($cv->getProfile());
        
        $this->assertCount(0, $profile->getCvs());
    }
    
    public function testEducations()
    {
        $cv = new CVForm();
        $this->assertTrue($cv->getEducations() instanceof ArrayCollection);
        $this->assertCount(0, $cv->getEducations());
        
        $education = new Education();
        $cv->addEducation($education);
        $this->assertCount(1, $cv->getEducations());
        $this->assertSame($education, $cv->getEducations()->first());
        $this->assertSame($cv, $education->getCv());
        
        $cv->removeEducation($education);
        $this->assertCount(0, $cv->getEducations());
        $this->assertNull($education->getCv());
        
        $education2 = new Education();
        $cv->addEducation($education);
        $cv->setEducations(array($education2));
        $this->assertCount(1, $cv->getEducations());
        $this->assertSame($education2, $cv->getEducations()->first());
        $this->assertSame($cv, $education2->getCv());
    }
    
    public function testReferences()
    {
        $cv = new CVForm();
        $this->assertTrue($cv->getReferences() instanceof ArrayCollection);
        $this->assertCount(0, $cv->getReferences());
        
        $reference = new Reference();
        $cv->addReference($reference);
        $this->assertCount(1, $cv->getReferences());
        $this->assertSame($reference, $cv->getReferences()->first());
        $this->assertSame($cv, $reference->getCv());
        
        $cv->removeReference($reference);
        $this->assertCount(0, $cv->getReferences());
        $this->assertNull($reference->getCv());
        
        $reference2 = new Reference();
        $cv->addReference($reference);
        $cv->setReferences(array($reference2));
        $this->assertCount(1, $cv->getReferences());
        $this->assertSame($reference2, $cv->getReferences()->first());
        $this->assertSame($cv, $reference2->getCv());
        
    }
    
    public function testWorkExperience()
    {
        $cv = new CVForm();
        $this->assertTrue($cv->getJobs() instanceof ArrayCollection);
        $this->assertCount(0, $cv->getJobs());
        
        $job = new WorkExperience();
        $cv->addJob($job);
        $this->assertCount(1, $cv->getJobs());
        $this->assertSame($job, $cv->getJobs()->first());
        $this->assertSame($cv, $job->getCv());
        
        $cv->removeJob($job);
        $this->assertCount(0, $cv->getJobs());
        $this->assertNull($job->getCv());
        
        $job2 = new WorkExperience();
        $cv->addJob($job);
        $cv->setJobs(array($job2));
        $this->assertCount(1, $cv->getJobs());
        $this->assertSame($job2, $cv->getJobs()->first());
        $this->assertSame($cv, $job2->getCv());
    }
    
    public function testAchievements()
    {
        $cv = new CVForm();
        $this->assertNull($cv->getAchievements());
        $cv->setAchievements($achievements = 'test');
        $this->assertSame($achievements, $cv->getAchievements());
    }
    
    public function testCoverNote()
    {
        $cv = new CVForm();
        $this->assertNull($cv->getCoverNote());
        $cv->setCoverNote($coverNote = 'test');
        $this->assertSame($coverNote, $cv->getCoverNote());
    }
    
    public function testAds()
    {
        $cv = new CVForm();
        $this->assertTrue($cv->getAds() instanceof ArrayCollection);
        $this->assertCount(0, $cv->getAds());
        
        $ad = new Ad();
        $cv->addAd($ad);
        $this->assertCount(1, $cv->getAds());
        $this->assertSame($ad, $cv->getAds()->first());
        $this->assertSame($cv, $ad->getCv());
        
        $cv->removeAd($ad);
        $this->assertCount(0, $cv->getAds());
        $this->assertNull($ad->getCv());
        
        $ad2 = new Ad();
        $cv->addAd($ad);
        $cv->setAds(array($ad2));
        $this->assertCount(1, $cv->getAds());
        $this->assertSame($ad2, $cv->getAds()->first());
        $this->assertSame($cv, $ad2->getCv());
    }
    
    public function testToString()
    {
        $cv = new CVForm();
        $cv->setName($name = 'test');
        $this->assertSame($name, (string)$cv);
        $this->assertSame($cv->getName(), (string)$cv);
    }
    
    public function testAtLeastOneEducationIsRequired()
    {
        $cv = clone $this->cv;
        $cv->setEducations(array());
        
        $validator = $this->validator;
        $errors = $validator->validate($cv);
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('educations', $errors[0]->getPropertyPath());
    }
    
    public function testCVNameIsRequired()
    {
        $cv = clone $this->cv;
        $cv->setName($name = '');
        
        $validator = $this->validator;
        $errors = $validator->validate($cv);
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('name', $errors[0]->getPropertyPath());
    }
    
    public function testNameIsTooLong()
    {
        // Above
        $cv = clone $this->cv;
        $cv->setName($name = str_repeat('a', 201));
        
        $validator = $this->validator;
        $errors = $validator->validate($cv);
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('name', $errors[0]->getPropertyPath());
        
        $cv->setName($name = str_repeat('a', 200));
        $errors = $validator->validate($cv);
        $this->assertCount(0, $errors, $this->validationErrorsToString($errors));
    }
    
    public function testProfileIsRequired()
    {
        $cv = clone $this->cv;
        $cv->setProfile($profile = null);
        
        $validator = $this->validator;
        $errors = $validator->validate($cv);
        
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('profile', $errors[0]->getPropertyPath());
    }
    
    public function testCoverNoteIsRequired()
    {
        $cv = clone $this->cv;
        $cv->setCoverNote($coverNote = '');
        $validator = $this->validator;
        $errors = $validator->validate($cv);
        
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('coverNote', $errors[0]->getPropertyPath());
    }
    
    public function testCoverNoteIsTooLong()
    {
        // Above
        $cv = clone $this->cv;
        $cv->setCoverNote($coverNote = str_repeat('a', 2001));
        $validator = $this->validator;
        $errors = $validator->validate($cv);
        
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('coverNote', $errors[0]->getPropertyPath());
        
        // Exact
        $cv->setCoverNote($coverNote = str_repeat('a', 2000));
        $validator = $this->validator;
        $errors = $validator->validate($cv);
        $this->assertCount(0, $errors, $this->validationErrorsToString($errors));
    }
    
    public function testAchivementsIsRequired()
    {
        $cv = clone $this->cv;
        $cv->setAchievements($achievements = '');
        
        $validator = $this->validator;
        $errors = $validator->validate($cv);
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('achievements', $errors[0]->getPropertyPath());
    }
    
    public function testAchievementsIsTooLong()
    {
        // Above
        $cv = clone $this->cv;
        $cv->setAchievements($achievements = str_repeat('a', 2001));
        
        $validator = $this->validator;
        $errors = $validator->validate($cv);
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('achievements', $errors[0]->getPropertyPath());
        
        // Exact
        $cv->setAchievements($achievements = str_repeat('a', 2000));
        $validator = $this->validator;
        $errors = $validator->validate($cv);
        $this->assertCount(0, $errors, $this->validationErrorsToString($errors));
    }
    
}