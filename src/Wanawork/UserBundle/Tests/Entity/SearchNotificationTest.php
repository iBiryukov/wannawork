<?php
namespace Wanawork\UserBundle\Tests\Entity;

use Wanawork\UserBundle\Entity\SearchNotification;
use Wanawork\UserBundle\Entity\Search;
use Wanawork\UserBundle\Entity\EmployerProfile;
use Wanawork\UserBundle\Entity\SectorJob;
use Wanawork\UserBundle\Entity\Sector;
use Doctrine\Common\Collections\ArrayCollection;
use Wanawork\UserBundle\Entity\SearchNotificationCheck;
use Wanawork\UserBundle\Entity\User;

class SearchNotificationTest extends WebUnitTestBase
{
    private $validator;
    
    private $search;

    /**
     * Entity Manager
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    
    protected function setUp()
    {
        $client = self::createClient();
        $this->validator = $client->getContainer()->get('validator');
        $this->em = $client->getContainer()->get('doctrine')->getManager();
        
        $this->em->getConnection()->beginTransaction();
        
        $this->search = new Search();
        $profession = new SectorJob(-1, 'test');
        $profession->addSector(new Sector(-1, 'test'));
        $this->search->setProfession($profession);
    }
    
    protected function tearDown()
    {
        $this->validator = null;
        $this->em->getConnection()->rollBack();
        $this->em->clear();
    }
    
    public function testConstructor()
    {
        $search = new Search();
        $name = 'test';
        $employer = new EmployerProfile();
        $user = new User();
        $email = 'test@test.com';
        
        $notification = new SearchNotification($search, $name, $user, $email, $employer);
        $this->assertSame($name, $notification->getSearchName());
        $this->assertSame($search, $notification->getSearch());
        $this->assertSame($employer, $notification->getEmployer());
        $this->assertSame($email, $notification->getEmail());
        
        $this->assertTrue($notification->getCreatedOn() instanceof \DateTime);
        $this->assertNotSame($notification->getCreatedOn(), $notification->getCreatedOn());
        $this->assertEquals($notification->getCreatedOn(), $notification->getCreatedOn());
        
        $this->assertSame($search->getId(), $notification->getId());
        
        $this->assertTrue($notification->getSearchChecks() instanceof ArrayCollection);
        $this->assertCount(0, $notification->getSearchChecks());
        
        $this->assertSame($user, $notification->getUser());
        
    }
    
    public function testValidateNameIsRequired()
    {
        $validator = $this->validator;
        
        $search = clone $this->search;
        $name = '';
        $user = new User();
        $email = 'test@test.com';
        $notification = new SearchNotification($search, $name, $user, $email);
        
        $errors = $validator->validate($notification);
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('searchName', $errors[0]->getPropertyPath(), $this->validationErrorsToString($errors));
    }
    
    public function testValidateNameLength()
    {
        $validator = $this->validator;
        
        // exact
        $search = clone $this->search;
        $name = str_repeat('a', 255);
        $user = new User();
        $email = 'test@test.com';
        $notification = new SearchNotification($search, $name, $user, $email);
        
        $errors = $validator->validate($notification);
        $this->assertCount(0, $errors, $this->validationErrorsToString($errors));
        
        // 1 above
        $name = str_repeat('a', 256);
        $newestAd = new \DateTime();
        $notification = new SearchNotification($search, $name, $user, $email);
        $errors = $validator->validate($notification);
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('searchName', $errors[0]->getPropertyPath(), $this->validationErrorsToString($errors));

        // 1 below
        $name = str_repeat('a', 254);
        $notification = new SearchNotification($search, $name, $user, $email);
        $errors = $validator->validate($notification);
        $this->assertCount(0, $errors, $this->validationErrorsToString($errors));
    }
    
    public function testEmailOrUserIsRequired()
    {
        $validator = $this->validator;
        
        $search = clone $this->search;
        $name = str_repeat('a', 255);
        $notification = new SearchNotification($search, $name, $user = null, $email = null);

        $errors = $validator->validate($notification);
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertEmpty($errors[0]->getPropertyPath(), $this->validationErrorsToString($errors));
    }
    
    public function testChecks()
    {
        $search = clone $this->search;
        $name = str_repeat('a', 255);
        $notification = new SearchNotification($search, $name, $user = null, $email = null);
        
        $this->assertCount(0, $notification->getSearchChecks());
        $searchCheck = $notification->addSearchCheck($newAds = array());
        
        $this->assertTrue($searchCheck instanceof SearchNotificationCheck);
        $this->assertCount(1, $notification->getSearchChecks());
    }
    
    public function testLastCheckDate()
    {
        $search = clone $this->search;
        $notification = new SearchNotification($search, $name = 'test', $user = null, $email = null);
        
        $this->assertEquals($notification->getLastCheck(), $notification->getCreatedOn());
        $this->assertNotSame($notification->getLastCheck(), $notification->getCreatedOn());
        
        sleep(1);
        $notification->addSearchCheck($newAds = array());
        $this->assertNotEquals($notification->getLastCheck(), $notification->getCreatedOn());
        $this->assertTrue($notification->getCreatedOn() < $notification->getLastCheck());
        
    }
    
    public function testAddCheck()
    {
        $search = clone $this->search;
        $notification = new SearchNotification($search, $name = 'test', $user = null, $email = null);
        
        $this->assertCount(0, $notification->getSearchChecks());
        
        $check = $notification->addSearchCheck($newAds = array());
        $this->assertCount(1, $notification->getSearchChecks());
        $this->assertTrue($check instanceof SearchNotificationCheck);
        
        $this->assertSame($notification, $check->getNotification());
    }
    
}