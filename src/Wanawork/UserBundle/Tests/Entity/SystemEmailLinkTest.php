<?php
namespace Wanawork\UserBundle\Tests\Entity;

use Wanawork\UserBundle\Entity\SystemEmailLink;
use Wanawork\UserBundle\Entity\SystemEmail;
use Doctrine\Common\Collections\ArrayCollection;
use Wanawork\UserBundle\Entity\SystemEmailLinkClick;

class SystemEmailLinkTest extends WebUnitTestBase
{
    
    public function testConstructor()
    {
        $systemEmail = new SystemEmail($user = null, $subject = null, $type = SystemEmail::TYPE_CV_REQUEST);
        $href = "href";
        $text = "text";
        $link = new SystemEmailLink($systemEmail, $href, $text);
        
        $this->assertNotNull($link->getId());
        $this->assertSame($systemEmail, $link->getSystemEmail());
        $this->assertSame($href, $link->getHref());
        $this->assertSame($text, $link->getText());
        $this->assertTrue($link->getClicks() instanceof ArrayCollection);
        $this->assertCount(0, $link->getClicks());
    }

    public function testAddClick()
    {
        $systemEmail = new SystemEmail($user = null, $subject = null, $type = SystemEmail::TYPE_CV_REQUEST);
        $href = "href";
        $text = "text";
        $link = new SystemEmailLink($systemEmail, $href, $text);
        
        $click = $link->addClick($ip = "192.168.0.1");
        $this->assertTrue($click instanceof SystemEmailLinkClick);
    }
    
}