<?php
namespace Wanawork\UserBundle\Tests\Entity;

use Wanawork\UserBundle\Entity\User;
use Wanawork\UserBundle\Entity\EmployeeProfile;
use Wanawork\UserBundle\Entity\EmployerProfile;
use Doctrine\ORM\Tools\Setup;
use Doctrine\Common\Collections\ArrayCollection;
use Wanawork\UserBundle\Entity\Role;
use Symfony\Component\Validator\Validation;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\Validator\ConstraintValidatorFactory;
use Doctrine\ORM\EntityManager;
use Wanawork\UserBundle\Entity\Billing\AdOrder;
use Wanawork\UserBundle\Entity\Ad;
use Wanawork\UserBundle\Entity\Billing\Order;
use Wanawork\UserBundle\Entity\Billing\VerificationOrder;
use Wanawork\UserBundle\Tests\Entity\Mocks\VerificationOrderMock;
use Wanawork\UserBundle\Entity\Social\Twitter;
use Wanawork\UserBundle\Entity\CvRequest;
use Wanawork\UserBundle\Entity\MailThread;
use Wanawork\UserBundle\Entity\Message;

/**
 * Test the User entity
 * 
 * @author Ilya Biryukov <ilya@wanawork.ie>
 */
class UserTest extends WebUnitTestBase
{
    private $user;
    
    private $validator;
    
    /**
     * 
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    
    protected function setUp()
    {
        $user = new User();
        $user->setEmail('ilya@live.ie');
        $user->setPassword('testpassword');
        $user->setName('Name');
        
        $user->addRole(new Role('role', 'role')); 
        
        $this->user = $user;
        
        $client = self::createClient();
        $this->validator = $client->getContainer()->get('validator');
        $this->em = $client->getContainer()->get('doctrine')->getManager();
        $this->em->getConnection()->beginTransaction();
    }
    
    protected function tearDown()
    {
        $this->user = null;
        $this->validator = null;
        $this->em->clear();
        $this->em->getConnection()->rollback();
        $this->em->getConnection()->close();
    }
    
    /**
     * Ensure that default profiles are assigned 
     * when the profile is set on the user account
     */
    public function testDefaultProfile()
    {
        // Employee Default test
        $profile1 = new EmployeeProfile();
        $user = new User();
        $user->setEmployeeProfile($profile1);
        $this->assertSame($profile1, $user->getDefaultProfile());

        $profile2 = new EmployeeProfile();
        $user->setEmployeeProfile($profile2);
        $this->assertSame($profile2, $user->getDefaultProfile());

        // Employer Default test
        $user2 = new User();
        $profile3 = new EmployerProfile();
        $user2->getEmployerProfiles()->add($profile3);
        $this->assertSame($profile3, $user2->getDefaultProfile());
    }
    
    public function testSettingEmployeeProfileSetsUserOnThatProfile()
    {
        $user = new User();
        $profile = new EmployeeProfile();
        
        $user->setEmployeeProfile($profile);
        
        $this->assertSame($profile, $user->getEmployeeProfile());
    }
    
    public function testConstructor()
    {
        $user = new User();
        
        // ID
        $this->assertNull($user->getId());
        
        // Email
        $user->setEmail('ilya@live.ie');
        $this->assertSame('ilya@live.ie', $user->getEmail());
        $this->assertSame($user->getUsername(), $user->getEmail());
        $this->assertFalse($user->isEmailVerified());
        $this->assertNull($user->getEmailVerificationDate());
        $this->assertNotNull($user->getEmailVerificationToken());
        
        $user->markEmailAsVerified(true);
        $this->assertTrue($user->isEmailVerified());
        $this->assertInstanceOf('\DateTime', $user->getEmailVerificationDate());
        
        $user->markEmailAsVerified(false);
        $this->assertFalse($user->isEmailVerified());
        $this->assertInstanceOf('\DateTime', $user->getEmailVerificationDate());
        
        // Passwords
        $user->setPassword('testpassword');
        $this->assertSame('testpassword', $user->getPassword());
        $this->assertNotNull($user->getSalt());
        
        //Name
        $user->setName('name');
        $this->assertSame('name', $user->getName());
        
        // Active flag
        $this->assertTrue($user->getIsActive());
        
        // Roles
        $role = new Role($name = 'employee', $role = 'ROLE_EMPLOYEE');
        $this->assertTrue(is_array($user->getRoles()));
        $user->addRole($role);
        $this->assertCount(1, $user->getRoles());
        $this->assertContainsOnly($role, $user->getRoles());
        $this->assertTrue($user->hasRole($role));
        $this->assertTrue($user->hasRoleName('ROLE_EMPLOYEE'));
        $this->assertFalse($user->hasRoleName('NON_EXISTING'));
        $user->removeRole($role);
        $this->assertCount(0, $user->getRoles());
        
        $user->setRoles(array($role));
        $this->assertCount(1, $user->getRoles());
        $this->assertContainsOnly($role, $user->getRoles());
        $this->assertTrue($user->hasRole($role));
        $this->assertTrue($user->hasRoleName('ROLE_EMPLOYEE'));
        
        // enabled flag
        $this->assertTrue($user->isEnabled());
        $user->disable();
        $this->assertFalse($user->isEnabled());
        $user->enable();
        $this->assertTrue($user->isEnabled());
        
        // Registration date
        $this->assertTrue($user->getRegistrationDate() instanceof \DateTime);
        
        // Password reminders
        $this->assertTrue($user->getPasswordReminders() instanceof ArrayCollection);
        $this->assertCount(0, $user->getPasswordReminders());
        $pr = $user->addPasswordReminder('192.168.1.1');
        $this->assertInstanceOf(
            '\Wanawork\UserBundle\Entity\PasswordReminder', 
            $pr
        );
        $this->assertCount(1, $user->getPasswordReminders());
        $user->removePasswordReminder($pr);
        $this->assertCount(0, $user->getPasswordReminders());
        
        // Twitter Accounts
        $this->assertTrue($user->getTwitterAccounts() instanceof ArrayCollection);
        $this->assertCount(0, $user->getTwitterAccounts());

        // System emails
        $this->assertTrue($user->getSystemEmails() instanceof ArrayCollection);
    }
    
    public function testMinimumValidation()
    {
        $user = clone $this->user;
        
        $validator = $this->validator;
        $errors = $validator->validate($user);
        $this->assertEquals(0, sizeof($errors), $this->validationErrorsToString($errors));
    }
    
    public function testEmptyEmail()
    {
        $user = clone $this->user;
        $user->setEmail('');
        
        $validator = $this->validator;
        $errors = $validator->validate($user);
        $this->assertEquals(1, sizeof($errors), $this->validationErrorsToString($errors));
        $this->assertSame('email', $errors[0]->getPropertyPath(), $this->validationErrorsToString($errors));
    }
    
    public function testEmailTooLong()
    {
        $user = clone $this->user;
        $user->setEmail("a@a.com" . str_repeat('a', 54));
        
        $validator = $this->validator;
        $errors = $validator->validate($user);
        $this->assertEquals(1, sizeof($errors), $this->validationErrorsToString($errors));
        $this->assertSame('email', $errors[0]->getPropertyPath());
    }
    
    public function testNameEmpty()
    {
        $user = clone $this->user;
        $user->setName('');
        
        $validator = $this->validator;
        $errors = $validator->validate($user);
        $this->assertEquals(1, sizeof($errors), $this->validationErrorsToString($errors));
        $this->assertSame('name', $errors[0]->getPropertyPath());
    }
    
    public function testNameTooLong()
    {
        $user = clone $this->user;
        $user->setName(str_repeat('a', 251));
    
        $validator = $this->validator;
        $errors = $validator->validate($user);
        $this->assertEquals(1, sizeof($errors), $this->validationErrorsToString($errors));
        $this->assertSame('name', $errors[0]->getPropertyPath());
    }
    
    public function testEmptyRoles()
    {
        $user = clone $this->user;
        $user->setRoles(array());
        
        $validator = $this->validator;
        $errors = $validator->validate($user);
        $this->assertEquals(1, sizeof($errors), $this->validationErrorsToString($errors));
        $this->assertSame('roles', $errors[0]->getPropertyPath());
    }
    
    public function testRemovingDefaultEmployerProfileSetsItToNullWithNoOtherProfiles()
    {
        $user = new User();
        $user2 = new User();
        $profile = new EmployerProfile();
        $profile->addUser($user, true);
        $profile->addUser($user2, true);
        
        $this->assertSame($user->getDefaultProfile(), $profile);
        $user->removeEmployerProfile($profile);
        $this->assertNull($user->getDefaultProfile());
    }
    
    public function testGetOrders()
    {
        $user = new User();
        $profileEmployee = new EmployeeProfile();
        
        $ad = new Ad();
        $adOrder = new AdOrder($ad, $amount = 200, $vatRate = 23, $plan = Ad::PLAN_STANDARD, $duration = 60, $bumps = 5);
        $adOrder->setStatus(Order::STATUS_PAID);
        $user->setEmployeeProfile($profileEmployee);
        $ad->setProfile($profileEmployee);
        $ad->addOrder($adOrder);
        
        $profileEmployer = new EmployerProfile();
        $user->addEmployerProfile($profileEmployer, true);
        
        $orderCancelled = new VerificationOrder($profileEmployer, $amount = 200, $vatRate = 23);
        $orderCancelled->setStatus(Order::STATUS_CANCELLED);
        $profileEmployer->addVerificationOrder($orderCancelled);
        
        $orderFailed = new VerificationOrder($profileEmployer, $amount = 200, $vatRate = 23);
        $orderFailed->setStatus(Order::STATUS_FAILED);
        $profileEmployer->addVerificationOrder($orderFailed);
        
        $orderPaid = new VerificationOrder($profileEmployer, $amount = 200, $vatRate = 23);
        $orderPaid->setStatus(Order::STATUS_PAID);
        $profileEmployer->addVerificationOrder($orderPaid);
        
        $orderPending = new VerificationOrder($profileEmployer, $amount = 200, $vatRate = 23);
        $orderPending->setStatus(Order::STATUS_PENDING);
        $profileEmployer->addVerificationOrder($orderPending);
        
        $this->assertCount(5, $user->getOrders());
        
        
        $this->assertCount(1, $user->getOrders(Order::STATUS_CANCELLED));
        $this->assertContains($orderCancelled, $user->getOrders(Order::STATUS_CANCELLED));
        
        $this->assertCount(1, $user->getOrders(Order::STATUS_FAILED));
        $this->assertContains($orderFailed, $user->getOrders(Order::STATUS_FAILED));
        
        $this->assertCount(2, $user->getOrders(Order::STATUS_PAID));
        $this->assertContains($orderPaid, $user->getOrders(Order::STATUS_PAID));
        $this->assertContains($adOrder, $user->getOrders(Order::STATUS_PAID));
        
        $this->assertCount(1, $user->getOrders(Order::STATUS_PENDING));
        $this->assertContains($orderPending, $user->getOrders(Order::STATUS_PENDING));
        
    }
    
    public function testGetOrdersSorting()
    {
        $user = new User();
        $profileEmployer = new EmployerProfile();
        $user->addEmployerProfile($profileEmployer, true);
        
        $orderCancelled = new VerificationOrderMock($profileEmployer, $amount = 200, $vatRate = 23);
        $orderCancelled->setCreatedAt(new \DateTime("@1306123200"));
        $profileEmployer->addVerificationOrder($orderCancelled);
        
        $orderFailed = new VerificationOrderMock($profileEmployer, $amount = 200, $vatRate = 23);
        $orderFailed->setCreatedAt(new \DateTime("@1306123201"));
        $profileEmployer->addVerificationOrder($orderFailed);
        
        $orderPaid = new VerificationOrderMock($profileEmployer, $amount = 200, $vatRate = 23);
        $orderPaid->setCreatedAt(new \DateTime("@1306123202"));
        $profileEmployer->addVerificationOrder($orderPaid);
        
        $orderPending = new VerificationOrderMock($profileEmployer, $amount = 200, $vatRate = 23);
        $orderPending->setCreatedAt(new \DateTime("@1306123203"));
        $profileEmployer->addVerificationOrder($orderPending);
        
        $orders = $user->getOrders();
        $this->assertSame($orderPending, $orders[0]);
        $this->assertSame($orderCancelled, $orders[sizeof($orders) - 1]);
    }
    
    public function testTwitterAccounts()
    {
        $user = new User();
        $this->assertCount(0, $user->getTwitterAccounts());
        
        $id = 50;
        $username = 'WannaworkIrl';
        $token = new \ZendOAuth\Token\Access();
        
        $account1 = $user->addTwitterAccount($id, $username, $token);
        $this->assertTrue($account1 instanceof Twitter);
        $this->assertCount(1, $user->getTwitterAccounts());
        
        $this->assertSame($id, $account1->getId());
        $this->assertSame($username, $account1->getUsername());
        $this->assertSame($token, $account1->getToken());
        $this->assertSame($user, $account1->getUser());
        
        $username2 = 'WannaworkIrl2';
        $token2 = new \ZendOAuth\Token\Access();
        $account2 = $user->addTwitterAccount($id, $username2, $token2);
        $this->assertTrue($account2 instanceof Twitter);
        $this->assertCount(1, $user->getTwitterAccounts());
        $this->assertSame($account1, $account2);
        $this->assertSame($username2, $account1->getUsername());
        $this->assertSame($token2, $account2->getToken());
        
        $account3 = $user->addTwitterAccount($id = 60, $username2, $token2);
        $this->assertTrue($account3 instanceof Twitter);
        $this->assertCount(2, $user->getTwitterAccounts());
    }
    
    public function testGetNumberOfUnreadEmails()
    {
        $userNull = new User();
        $this->assertSame($userNull->getNumberOfUnreadEmails(),0);
        
        $userEmployee = new User();
        $userEmployee->setEmployeeProfile( new EmployeeProfile());
        $this->assertSame($userEmployee->getNumberOfUnreadEmails(),0);
        
        $userEmployer = new User();
        $userEmployer->addEmployerProfile( new EmployerProfile(), true);
        $this->assertSame($userEmployer->getNumberOfUnreadEmails(),0);
        
        $employeeAd = new Ad();
        $employeeAd->setProfile($userEmployee->getDefaultProfile());
        $userEmployer->getDefaultProfile()->setCvRequests(array(new CvRequest($userEmployer, $employeeAd, $userEmployer->getDefaultProfile())));
        $this->assertTrue(sizeof($userEmployer->getDefaultProfile()->getCvRequests()) == 1);
        $mailThread = $userEmployer->getDefaultProfile()->getCvRequests()->first()->createMailThread();
        
        $this->assertSame($userEmployer->getNumberOfUnreadEmails(),0);
        $this->assertSame($userEmployee->getNumberOfUnreadEmails(),0);
        
        $mailThread->addMessage(new Message($text = 'Employer message to Employee', $userEmployer, 
            $userEmployer->getDefaultProfile(),$userEmployer->getDefaultProfile()->getCvRequests()->first()->getMailThread()));
        $this->assertSame($userEmployee->getNumberOfUnreadEmails(),1);
        
        $mailThread->addMessage(new Message($text = 'Employee message to Employer', $userEmployee,
            $userEmployee->getDefaultProfile(),$userEmployer->getDefaultProfile()->getCvRequests()->first()->getMailThread()));
        $this->assertSame($userEmployer->getNumberOfUnreadEmails(),1);
        
    }
   
// @todo reactivate these tests later for user management
//     public function testRemovingNonLastEmployerProfileUsesAnotherEmployerProfileAsDefault()
//     {
//         $user = new User();
//         $user2 = new User();
//         $profile1 = new EmployerProfile();
//         $profile2 = new EmployerProfile();
        
//         $user->addEmployerProfile($profile1, true);
//         $user->addEmployerProfile($profile2, true);
        
//         $user2->addEmployerProfile($profile1, true);
        
//         $this->assertSame($profile2, $user->getDefaultProfile());
        
//         $user->removeEmployerProfile($profile2);
//         $this->assertSame($profile1, $user->getDefaultProfile());
//     }
    
//     public function testRemovingLastEmployerProfileWillSetEmployeeProfileAsDefaultIfExists()
//     {
//         $user = new User();
//         $user2 = new User();
//         $profile1 = new EmployerProfile();
//         $employeeProfile = new EmployeeProfile();
        
//         $user->addEmployerProfile($profile1, true);
//         $user2->addEmployerProfile($profile1, true);
//         $user->setEmployeeProfile($employeeProfile);
        
        
//         $this->assertSame($employeeProfile, $user->getDefaultProfile());
        
//         $user->removeEmployerProfile($profile1);
//         $this->assertSame($employeeProfile, $user->getDefaultProfile());
//     }
}
