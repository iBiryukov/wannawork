<?php
namespace Wanawork\UserBundle\Tests\Entity\containers;

use Wanawork\UserBundle\Tests\Entity\WebUnitTestBase;
use Wanawork\UserBundle\Entity\Containers\EmailContainer;

class EmailContainerTest extends WebUnitTestBase
{
    public function testEntityOK()
    {
        $container = new EmailContainer();
        $container->setEmail($email = 'test');
        $this->assertSame($email, $container->getEmail());
    }
}