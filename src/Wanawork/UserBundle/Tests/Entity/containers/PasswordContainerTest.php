<?php
namespace Wanawork\UserBundle\Tests\Entity\containers;

use Wanawork\UserBundle\Entity\Containers\PasswordContainer;
use Wanawork\UserBundle\Tests\Entity\WebUnitTestBase;

class PasswordContainerTest extends WebUnitTestBase
{
    public function testEntityOK()
    {
        $container = new PasswordContainer();
        $container->setPassword($pass = 'test');
        $this->assertSame($pass, $container->getPassword());
    }
}