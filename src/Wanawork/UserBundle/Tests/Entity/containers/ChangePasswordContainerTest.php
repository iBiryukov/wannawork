<?php
namespace Wanawork\UserBundle\Tests\Entity\containers;

use Wanawork\UserBundle\Entity\Role;
use Wanawork\UserBundle\Entity\User;
use Wanawork\UserBundle\Entity\Containers\ChangePasswordContainer;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Wanawork\UserBundle\Tests\Entity\WebUnitTestBase;

class ChangePasswordContainerTest extends WebUnitTestBase
{
    private $validator;
    
    /**
     * 
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    
    private $container;
    
    protected function setUp()
    {
    
        $client = self::createClient();
        $this->container = $client->getContainer();
        $this->validator = $this->container->get('validator');
        $this->em = $this->container->get('doctrine')->getManager();
        $this->em->getConnection()->beginTransaction();
    }
    
    protected function tearDown()
    {
        $this->user = null;
        $this->validator = null;
        
        $this->em->getConnection()->rollBack();
        $this->em->clear();
        $this->em->getConnection()->close();
    }
    
    public function testValidateCurrentPassword()
    {
        $user = new User();
        $user->setEmail('test@wannawork.ie');
        $user->setPassword('testpassword');
        $user->setName('Name');
        
        $encoderFactory = $this->container->get('security.encoder_factory');
        $encoder = $encoderFactory->getEncoder($user);
        $password = $encoder->encodePassword($user->getPassword(), $user->getSalt());
        $user->setPassword($password);
        
        $container = new ChangePasswordContainer($user, $encoderFactory);
        $container->setNewPassword('test');
        $container->setCurrentPassword('testpassword');
        
        $validator = $this->validator;
        $errors = $validator->validate($container);
        
        $this->assertCount(0, $errors, $this->validationErrorsToString($errors));
    }
    
    public function testCurrentPasswordIsRequired()
    {
        $user = new User();
        $user->setEmail('test@wannawork.ie');
        $user->setPassword('testpassword');
        $user->setName('Name');
        
        $encoderFactory = $this->container->get('security.encoder_factory');
        $container = new ChangePasswordContainer($user, $encoderFactory);
        $container->setNewPassword('testpassword1');
        
        $validator = $this->validator;
        $errors = $validator->validate($container, array('user'));
        
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('currentPassword', $errors[0]->getPropertyPath());
    }
    
    public function testInvalidCurrentPasswordFailsValidation()
    {
        $user = new User();
        $user->setEmail('test@wannawork.ie');
        $user->setPassword('testpassword');
        $user->setName('Name');
        
        $encoderFactory = $this->container->get('security.encoder_factory');
        $encoder = $encoderFactory->getEncoder($user);
        $password = $encoder->encodePassword($user->getPassword(), $user->getSalt());
        $user->setPassword($password);
        
        $container = new ChangePasswordContainer($user, $encoderFactory);
        $container->setNewPassword('test');
        $container->setCurrentPassword('testpassword1');
        
        $validator = $this->validator;
        $errors = $validator->validate($container, array('user'));
        
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('currentPassword', $errors[0]->getPropertyPath());
    }
    
    public function testNewPasswordIsRequired()
    {
        $user = new User();
        $user->setEmail('test@wannawork.ie');
        $user->setPassword('testpassword');
        $user->setName('Name');
        
        $encoderFactory = $this->container->get('security.encoder_factory');
        $encoder = $encoderFactory->getEncoder($user);
        $password = $encoder->encodePassword($user->getPassword(), $user->getSalt());
        $user->setPassword($password);
        
        $container = new ChangePasswordContainer($user, $encoderFactory);
        $container->setCurrentPassword('testpassword');
        
        $validator = $this->validator;
        $errors = $validator->validate($container, array('user'));
        
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('newPassword', $errors[0]->getPropertyPath());
    }
}