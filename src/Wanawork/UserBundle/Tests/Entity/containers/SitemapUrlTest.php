<?php
namespace Wanawork\UserBundle\Tests\Entity\containers;

use Wanawork\UserBundle\Tests\Entity\WebUnitTestBase;
use Wanawork\UserBundle\Entity\Containers\SitemapUrl;


class SitemapUrlTest extends WebUnitTestBase
{
    public function testConstructor()
    {
        $url = 'http://wannawork.ie';
        $sitemapUrl = new SitemapUrl($url); 
        $this->assertSame($url, $sitemapUrl->getLocation());
    }    
    
    public function testSetLocation()
    {
        $url = 'http://wannawork.ie';
        $sitemapUrl = new SitemapUrl($url);
        
        $baseDomain = 'http://wannawork.ie/';
        $validValues = array(
            'http://wannawork.ie',
            'https://wannawork.ie',
            'http://wanawork.ie',
            'https://wanawork.ie',
            'http://wannawork.local',
            'https://wannawork.local',
            'http://wanawork.local',
            'https://wanawork.local',
            'http://wannawork.ie/',
            'http://wannawork.ie/test',
            'http://wannawork.ie/test/',
            $baseDomain . str_repeat('a', SitemapUrl::URL_MAX_LENGTH - strlen($baseDomain) - 1),
            $baseDomain . str_repeat('a', SitemapUrl::URL_MAX_LENGTH - strlen($baseDomain)),
        );
        
        $invalidValues = array(
        	null,
            $baseDomain . str_repeat('a', SitemapUrl::URL_MAX_LENGTH - strlen($baseDomain) + 1),
        );
        
        
        foreach ($validValues as $url) {
            try {
            	$sitemapUrl->setLocation($url);
            	$this->assertSame($url, $sitemapUrl->getLocation());
            } catch (\Exception $e) {
                $this->fail("Unexpected Exception: " . $e->__toString());
            }
        }
        
        foreach ($invalidValues as $url) {
            try {
                $sitemapUrl->setLocation($url);
                $this->fail(sprintf("Expected an exception to be thrown for value: '%s'", $url));
            } catch (\Exception $e) {
                $this->assertTrue($e instanceof \InvalidArgumentException);
            }
        }
    }
    
    public function testLastModified()
    {
        $url = 'http://wannawork.ie';
        $sitemapUrl = new SitemapUrl($url);
        
        $date = new \DateTime();
        $sitemapUrl->setLastModified($date);
        $this->assertSame($date, $sitemapUrl->getLastModified());

        $date = null;
        $sitemapUrl->setLastModified($date);
        $this->assertSame($date, $sitemapUrl->getLastModified());
    }
    
    public function testPriority()
    {
        $url = 'http://wannawork.ie';
        $sitemapUrl = new SitemapUrl($url);
        
        $validValues = array(
        	'0.0', '0.1', '0.9', '1.0', null
        );
        
        $invalidValues = array(
        	'-0.1', '0.01', '1.1', '1.01', 'abc',
        );
        
        foreach ($validValues as $priority) {
            try {
                $sitemapUrl->setPriority($priority);
                $this->assertSame($priority, $sitemapUrl->getPriority());
            } catch (\Exception $e) {
                $this->fail(
        	       sprintf("Unexpected exception with value: '%s'. '%s'", $priority, $e->__toString())
                );
            }       
        }
        
        foreach ($invalidValues as $priority) {
            try {
                $sitemapUrl->setPriority($priority);
                $this->fail(
                    sprintf("Expected exception with value: '%s'.", $priority)
                );
            } catch (\Exception $e) {
                $this->assertTrue($e instanceof \InvalidArgumentException);
            }
        }
    }
    
    public function testChangeFrequency()
    {
        $url = 'http://wannawork.ie';
        $sitemapUrl = new SitemapUrl($url);
        
        foreach (SitemapUrl::$validChangeFrequencies as $frequency) {
            $sitemapUrl->setChangeFrequency($frequency);
            $this->assertSame($frequency, $sitemapUrl->getChangeFrequency());
        }
        
        foreach (SitemapUrl::$validChangeFrequencies as $frequency) {
            try {
                $sitemapUrl->setChangeFrequency($frequency . '1');
                $this->fail(
                    sprintf("Expected exception with value: '%s'.", $frequency)
                );
            } catch (\Exception $e) {
                $this->assertTrue($e instanceof \InvalidArgumentException);
            }
        }
        
    }
}