<?php
namespace Wanawork\UserBundle\Tests\Entity;

use Wanawork\UserBundle\Entity\EmployerProfile;
use Wanawork\MainBundle\Entity\NameTitle;
use Wanawork\UserBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Wanawork\UserBundle\Entity\CvRequest;
use Wanawork\UserBundle\Entity\Ad;
use Wanawork\UserBundle\Entity\Role;
use Wanawork\UserBundle\Entity\JobSpec;
use Wanawork\MainBundle\Entity\County;
use Wanawork\UserBundle\Tests\Entity\Mocks\EmployerProfileMock;
use Wanawork\UserBundle\Entity\Billing\VerificationOrder;
use Wanawork\UserBundle\Entity\Billing\Order;
use Wanawork\UserBundle\Entity\Sector;
use Wanawork\UserBundle\Entity\SectorJob;

class EmployerProfileTest extends WebUnitTestBase
{
    private $validator;
    
    private $profile;
    
    protected function setUp()
    {
        $client = self::createClient();
        $this->validator = $client->getContainer()->get('validator');
        
        $profile = new EmployerProfile();
        $profile->setCompanyName($companyName = 'test');
        $profile->setContactName($contactName = 'test');
        $profile->setEmail($email = 'test@test.com');
        $profile->setWebsite($website = 'http://wannawork.ie');
        $profile->setFacebook($facebook = 'wannawork');
        $profile->setTwitter($twitter = 'wannawork');
        $profile->setLinkedin($linkedin = 'http://linkedin.com/wannawork');
        $profile->setAbout($about = 'test');
        $profile->setVerifiedBy($verifiedBy = new User());
        $profile->setCompanyNumber($companyNumber = 'test');
        $profile->addUser(new User(), $isAdmin = true);
        $profile->addIndustry(new Sector(-1, "Test"));
        $profile->addProfession(new SectorJob(-1, "Test"));
        
        $profile->setPhoneNumber($phoneNumber = '123');
        $profile->setAddress($address = 'abc');
        $profile->setCity($city = 'abc');
        $profile->setCounty(new County(-1, 'abc'));
        
        $this->profile = $profile;
        
    }
    

    public function testEntityOK()
    {
        $profile = new EmployerProfileMock();
        $profile->setId(-1);
        
        $this->assertFalse($profile->isVerificationPaid());
        
        // Roles
        $this->assertTrue($profile->getAdminRole() instanceof Role);
        $this->assertTrue($profile->getUserRole() instanceof Role);
        $this->assertNotSame($profile->getAdminRole(), $profile->getUserRole());
        
        // id
        $this->assertSame(-1, $profile->getId());
        
        // Company name
        $profile->setCompanyName($companyName = 'test');
        $this->assertSame($companyName, $profile->getCompanyName());

        // contact name
        $profile->setContactName($contactName = 'test');
        $this->assertSame($contactName, $profile->getContactName());
        
        // Email
        $profile->setEmail($email = 'test');
        $this->assertSame($email, $profile->getEmail());
        
        // website
        $profile->setWebsite($website = 'test');
        $this->assertSame($website, $profile->getWebsite());
        
        // facebook 
        $profile->setFacebook($facebook = 'test');
        $this->assertSame($facebook, $profile->getFacebook());
        
        // twitter
        $profile->setTwitter($twitter = 'test');
        $this->assertSame($twitter, $profile->getTwitter());
        
        // linkedin
        $profile->setLinkedin($linkedin = 'test');
        $this->assertSame($linkedin, $profile->getLinkedin());
        
        // About
        $profile->setAbout($about = 'test');
        $this->assertSame($about, $profile->getAbout());
        
        // Is verified
        $profile->setIsVerified($isVerified = true);
        $this->assertTrue($profile->isVerified());
        
        $profile->setIsVerified(false);
        $this->assertFalse($profile->isVerified());
        
        // verified by
        $profile->setVerifiedBy($verifiedBy = new User());
        $this->assertSame($verifiedBy, $profile->getVerifiedBy());
        
        // Company number 
        $profile->setCompanyNumber($companyNumber = 'test');
        $this->assertSame($companyNumber, $profile->getCompanyNumber());

        // Job specs
        $this->assertTrue($profile->getJobSpecs() instanceof ArrayCollection);
        $this->assertCount(0, $profile->getJobSpecs());
        
        $profile->addJobSpec($jobSpec = new JobSpec());
        $this->assertCount(1, $profile->getJobSpecs());
        $this->assertSame($jobSpec, $profile->getJobSpecs()->first());
        
        $profile->removeJobSpec($jobSpec);
        $this->assertCount(0, $profile->getJobSpecs());
        $this->assertNull($jobSpec->getEmployer());
        
        $jobSpec2 = new JobSpec();
        $profile->addJobSpec($jobSpec);
        $profile->setJobSpecs(array($jobSpec2));
        $this->assertCount(1, $profile->getJobSpecs());
        $this->assertSame($jobSpec2, $profile->getJobSpecs()->first());
        
        $industry = new Sector(-1, "Test");
        $profile->setIndustries(array($industry));
        $this->assertCount(1, $profile->getIndustries());
        $profile->addIndustry(new Sector(-1, "Test"));
        $this->assertCount(2, $profile->getIndustries());
        $profile->removeIndustry($industry);
        $this->assertCount(1, $profile->getIndustries());
        
        $profession = new SectorJob(-1, "Test");
        $profile->setProfessions(array($profession));
        $this->assertCount(1, $profile->getProfessions());
        $profile->addProfession(new SectorJob(-1, "Test"));
        $this->assertCount(2, $profile->getProfessions());
        $profile->removeProfession($profession);
        $this->assertCount(1, $profile->getProfessions());
        
        // Users
        $this->assertTrue($profile->getUsers() instanceof ArrayCollection);
        $this->assertCount(0, $profile->getUsers());


        // add admin user
        $userAdmin = new User();
        $profile->addUser($userAdmin, $isAdmin = true);
        $this->assertCount(1, $profile->getUsers());
        $this->assertSame($userAdmin, $profile->getUsers()->first());
        $this->assertContains($profile, $userAdmin->getEmployerProfiles());
        $this->assertCount(1, $profile->getAdminUsers());
        $this->assertContains($profile->getUserRole(), $userAdmin->getRoles());
        $this->assertContains($profile->getAdminRole(), $userAdmin->getRoles());
        
        // add standard user
        $user = new User();
        $profile->addUser($user);
        $this->assertCount(2, $profile->getUsers());
        $this->assertSame($user, $profile->getUsers()->last());
        $this->assertContains($profile, $user->getEmployerProfiles());    
        $this->assertCount(1, $profile->getAdminUsers());
        $this->assertContains($profile->getUserRole(), $user->getRoles());
        $this->assertNotContains($profile->getAdminRole(), $user->getRoles());
        
        $profile->removeUser($user);
        $this->assertCount(1, $profile->getUsers());
        $this->assertContains($userAdmin, $profile->getUsers());
        $this->assertNotContains($profile->getAdminRole(), $user->getRoles());
        $this->assertNotContains($profile->getUserRole(), $user->getRoles());
        $this->assertNotContains($profile, $user->getEmployerProfiles());
        
        
        // cv requests -- keep this below users
        $this->assertTrue($profile->getCvRequests() instanceof ArrayCollection);
        $this->assertCount(0, $profile->getCvRequests());
        
        $user = $profile->getUsers()->first();
        $cvRequest = new CvRequest($user, $ad = new Ad(), $profile, new JobSpec());
        
        $profile->addCvRequest($cvRequest);
        $this->assertCount(1, $profile->getCvRequests());
        $this->assertSame($cvRequest, $profile->getCvRequests()->first());
        $profile->removeCvRequest($cvRequest);
        $this->assertCount(0, $profile->getCvRequests());
        $this->assertNull($cvRequest->getEmployerProfile());
        
        $cvRequest2 = new CvRequest($user, $ad = new Ad(), $profile, new JobSpec());
        $profile->addCvRequest($cvRequest);
        $this->assertCount(1, $profile->getCvRequests());
        
        $profile->setCvRequests(array($cvRequest2));
        $this->assertCount(1, $profile->getCvRequests());
        $this->assertSame($cvRequest2, $profile->getCvRequests()->first());
    }
    
    /**
     * @expectedException \Wanawork\UserBundle\Exception\employerProfile\MissingEmployerAdminException
     */
    public function testFirstUserMustBeAdmin()
    {
        $profile = new EmployerProfile();
        $user = new User();
        $profile->addUser($user, $isAdmin = false);
    }
    
    /**
     * @expectedException \Wanawork\UserBundle\Exception\employerProfile\MissingEmployerAdminException
     */
    public function testCannotRemoveLastAdmin()
    {
        $profile = new EmployerProfile();
        $user = new User();
        $profile->addUser($user, $isAdmin = true);
        
        $profile->removeUser($user);
    }
    
    public function testCompanyNameIsRequired()
    {
        $profile = clone $this->profile;
        $profile->setCompanyName($companyName = '');
        
        $validator = $this->validator;
        $errors = $validator->validate($profile);
        $this->assertEquals(1, sizeof($errors), $this->validationErrorsToString($errors));
        $this->assertSame('companyName', $errors[0]->getPropertyPath());
    }
    
    public function testCompanyNameIsTooLong()
    {
        // 1 above
        $profile = clone $this->profile;
        $profile->setCompanyName($companyName = str_repeat('a', 256));
        
        $validator = $this->validator;
        $errors = $validator->validate($profile);
        $this->assertEquals(1, sizeof($errors), $this->validationErrorsToString($errors));
        $this->assertSame('companyName', $errors[0]->getPropertyPath());
        
        $profile->setCompanyName($companyName = str_repeat('a', 255));
        $errors = $validator->validate($profile);
        $this->assertEquals(0, sizeof($errors), $this->validationErrorsToString($errors));
    }
    
    public function testContactNameIsRequired()
    {
        $profile = clone $this->profile;
        $profile->setContactName($contactName = '');
    
        $validator = $this->validator;
        $errors = $validator->validate($profile);
        $this->assertEquals(1, sizeof($errors), $this->validationErrorsToString($errors));
        $this->assertSame('contactName', $errors[0]->getPropertyPath());
    }
    
    public function testContactNameIsTooLong()
    {
        // 1 above
        $profile = clone $this->profile;
        $profile->setContactName(str_repeat('a', 251));
    
        $validator = $this->validator;
        $errors = $validator->validate($profile);
        $this->assertEquals(1, sizeof($errors), $this->validationErrorsToString($errors));
        $this->assertSame('contactName', $errors[0]->getPropertyPath());
    
        // exact
        $profile->setContactName(str_repeat('a', 250));
        $errors = $validator->validate($profile);
        $this->assertEquals(0, sizeof($errors), $this->validationErrorsToString($errors));
    }
    
    public function testEmailIsRequired()
    {
        $profile = clone $this->profile;
        $profile->setEmail('');
        
        $validator = $this->validator;
        $errors = $validator->validate($profile);
        $this->assertEquals(1, sizeof($errors), $this->validationErrorsToString($errors));
        $this->assertSame('email', $errors[0]->getPropertyPath());
    }
    
    public function testEmailMustBevalid()
    {
        $profile = clone $this->profile;
        $profile->setEmail('test@test');
        
        $validator = $this->validator;
        $errors = $validator->validate($profile);
        $this->assertEquals(1, sizeof($errors), $this->validationErrorsToString($errors));
        $this->assertSame('email', $errors[0]->getPropertyPath());
    }
    
    public function testWebsiteAddressMustBeValidIfPresent()
    {
        $profile = clone $this->profile;
        $profile->setWebsite('test');
        
        $validator = $this->validator;
        $errors = $validator->validate($profile);
        $this->assertEquals(1, sizeof($errors), $this->validationErrorsToString($errors));
        $this->assertSame('website', $errors[0]->getPropertyPath());
    }
    
//     public function testFacebookAddressMustBeValidIfPresent()
//     {
//         $profile = clone $this->profile;
//         $profile->setFacebook('test');
    
//         $validator = $this->validator;
//         $errors = $validator->validate($profile);
//         $this->assertEquals(1, sizeof($errors), $this->validationErrorsToString($errors));
//         $this->assertSame('facebook', $errors[0]->getPropertyPath());
//     }
    
//     public function testTwitterAddressMustBeValidIfPresent()
//     {
//         $profile = clone $this->profile;
//         $profile->setTwitter('test');
    
//         $validator = $this->validator;
//         $errors = $validator->validate($profile);
//         $this->assertEquals(1, sizeof($errors), $this->validationErrorsToString($errors));
//         $this->assertSame('twitter', $errors[0]->getPropertyPath());
//     }

    public function testFacebookAddressLength()
    {
        $profile = clone $this->profile;
        $validator = $this->validator;
    
        // Exact
        $profile->setFacebook($facebook = str_repeat('a', 200));
        $errors = $validator->validate($profile);
        $this->assertCount(0, $errors);
    
        // 1 below
        $profile->setFacebook($facebook = str_repeat('a', 199));
        $errors = $validator->validate($profile);
        $this->assertCount(0, $errors);
    
        // 1 above
        $profile->setFacebook($facebook = str_repeat('a', 201));
        $errors = $validator->validate($profile);
        $this->assertCount(1, $errors);
        $this->assertSame('facebook', $errors[0]->getPropertyPath());
    }
    
    public function testTwitterAddressLength()
    {
        $profile = clone $this->profile;
        $validator = $this->validator;
        
        // Exact
        $profile->setTwitter($twitter = str_repeat('a', 200));
        $errors = $validator->validate($profile);
        $this->assertCount(0, $errors);
        
        // 1 below
        $profile->setTwitter($twitter = str_repeat('a', 199));
        $errors = $validator->validate($profile);
        $this->assertCount(0, $errors);
        
        // 1 above
        $profile->setTwitter($twitter = str_repeat('a', 201));
        $errors = $validator->validate($profile);
        $this->assertCount(1, $errors);
        $this->assertSame('twitter', $errors[0]->getPropertyPath());
    }
    
    public function testTwitterUsernameValidity()
    {
        $profile = clone $this->profile;
        $validator = $this->validator;
        
        $valid = [
        	'iBiryukov',
        	'01',
        	'_12B',
        ];
        
        $invalid = [
        	'iBiryukov ',
        	'abc-1',
        	'/abc/'
        ];
        
        foreach ($valid as $username) {
            $profile->setTwitter($username);
            $errors = $validator->validate($profile);
            $this->assertCount(0, $errors, $this->validationErrorsToString($errors));
        }
        
        foreach($invalid as $username) {
            $profile->setTwitter($username);
            $errors = $validator->validate($profile);
            $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
            $this->assertSame('twitter', $errors[0]->getPropertyPath());
        }
    }
    
    public function testLinkedAddressMustBeValidIfPresent()
    {
        $profile = clone $this->profile;
        $profile->setLinkedin('test');
    
        $validator = $this->validator;
        $errors = $validator->validate($profile);
        $this->assertEquals(1, sizeof($errors), $this->validationErrorsToString($errors));
        $this->assertSame('linkedin', $errors[0]->getPropertyPath());
    }
    
    public function testMaxAboutDescription()
    {
        // 1 above
        $profile = clone $this->profile;
        $profile->setAbout(str_repeat('a', 2001));
        
        $validator = $this->validator;
        $errors = $validator->validate($profile);
        $this->assertEquals(1, sizeof($errors), $this->validationErrorsToString($errors));
        $this->assertSame('about', $errors[0]->getPropertyPath());
        
        // exact
        $profile->setAbout(str_repeat('a', 2000));
        $errors = $validator->validate($profile);
        $this->assertEquals(0, sizeof($errors), $this->validationErrorsToString($errors));
    }
    
    public function testCompanyNumberMaxLength()
    {
        // 1 above
        $profile = clone $this->profile;
        $profile->setCompanyNumber(str_repeat('a', 31));
        
        $validator = $this->validator;
        $errors = $validator->validate($profile);
        $this->assertEquals(1, sizeof($errors), $this->validationErrorsToString($errors));
        $this->assertSame('companyNumber', $errors[0]->getPropertyPath());
        
        // exact
        $profile->setCompanyNumber(str_repeat('a', 30));
        $errors = $validator->validate($profile);
        $this->assertEquals(0, sizeof($errors), $this->validationErrorsToString($errors));
    }

    public function testAddressIsRequired()
    {
        $profile = clone $this->profile;
        $profile->setAddress($address = '');
        
        $validator = $this->validator;
        $errors = $validator->validate($profile);
        $this->assertEquals(1, sizeof($errors), $this->validationErrorsToString($errors));
        $this->assertSame('address', $errors[0]->getPropertyPath());
    }
    
    public function testGetOrders()
    {
        $profile = clone $this->profile;
        
        $orderCancelled = new VerificationOrder($profile, $amount = 200, $vatRate = 23);
        $orderCancelled->setStatus(Order::STATUS_CANCELLED);
        $profile->addVerificationOrder($orderCancelled);
        
        $orderFailed = new VerificationOrder($profile, $amount = 200, $vatRate = 23);
        $orderFailed->setStatus(Order::STATUS_FAILED);
        $profile->addVerificationOrder($orderFailed);
        
        $orderPaid = new VerificationOrder($profile, $amount = 200, $vatRate = 23);
        $orderPaid->setStatus(Order::STATUS_PAID);
        $profile->addVerificationOrder($orderPaid);
        
        $orderPending = new VerificationOrder($profile, $amount = 200, $vatRate = 23);
        $orderPending->setStatus(Order::STATUS_PENDING);
        $profile->addVerificationOrder($orderPending);
        
        $this->assertCount(4, $profile->getVerificationOrders());
        $this->assertCount(4, $profile->getOrders());
        
        $this->assertCount(1, $profile->getOrders(Order::STATUS_CANCELLED));
        $this->assertContains($orderCancelled, $profile->getOrders(Order::STATUS_CANCELLED));
        
        $this->assertCount(1, $profile->getOrders(Order::STATUS_FAILED));
        $this->assertContains($orderFailed, $profile->getOrders(Order::STATUS_FAILED));
        
        $this->assertCount(1, $profile->getOrders(Order::STATUS_PAID));
        $this->assertContains($orderPaid, $profile->getOrders(Order::STATUS_PAID));
        
        $this->assertCount(1, $profile->getOrders(Order::STATUS_PENDING));
        $this->assertContains($orderPending, $profile->getOrders(Order::STATUS_PENDING));
        
        $this->assertTrue($profile->isVerificationPaid());
        $this->assertSame($orderPaid, $profile->getVerificationOrder());
                
    }
    
    public function testAtLeastOneIndustryIsRequired()
    {
    	$profile = clone $this->profile;
    	
    	$profile->getIndustries()->clear();
    	
    	$validator = $this->validator; 
    	$errors = $validator->validate($profile);
        $this->assertEquals(1, sizeof($errors), $this->validationErrorsToString($errors));
        $this->assertSame('industries', $errors[0]->getPropertyPath());
    }


    public function testAtLeastOneProfessionIsRequired()
    {
    	$profile = clone $this->profile;
    	 
    	$profile->getProfessions()->clear();
    	 
    	$validator = $this->validator;
    	$errors = $validator->validate($profile);
    	$this->assertEquals(1, sizeof($errors), $this->validationErrorsToString($errors));
    	$this->assertSame('professions', $errors[0]->getPropertyPath());
    }
    
    public function testEmptyAddress()
    {
        $profile = clone $this->profile;
        $validator = $this->validator;
    
        $profile->setAddress($address = '');
        $errors = $validator->validate($profile);
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('address', $errors[0]->getPropertyPath());
    }
    
    
    public function testAddressIsTooLong()
    {
        $profile = clone $this->profile;
        $validator = $this->validator;

        // 1 above
        $profile->setAddress(str_repeat('1', 501));
        $errors = $validator->validate($profile);
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('address', $errors[0]->getPropertyPath());

        // Boundary
        $profile->setAddress(str_repeat('1', 500));
        $errors = $validator->validate($profile);
        $this->assertCount(0, $errors, $this->validationErrorsToString($errors));
    }
    
    public function testEmptyCity()
    {
        $profile = clone $this->profile;
        $validator = $this->validator;
    
        $profile->setCity($city = '');
        $errors = $validator->validate($profile);
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('city', $errors[0]->getPropertyPath());
    }
    
    public function testCityIsTooLong()
    {
        $profile = clone $this->profile;
        $validator = $this->validator;
        
        $profile->setCity(str_repeat('1', 151));
        $errors = $validator->validate($profile);
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('city', $errors[0]->getPropertyPath());
        
        // Boundary
        $profile->setCity(str_repeat('1', 150));
        $errors = $validator->validate($profile);
        $this->assertCount(0, $errors, $this->validationErrorsToString($errors));
    }
    
    public function testEmptyPhoneNumber()
    {
        $profile = clone $this->profile;
        $validator = $this->validator;
    
        $profile->setPhoneNumber($phoneNumber = '');
        $errors = $validator->validate($profile);
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('phoneNumber', $errors[0]->getPropertyPath());
    }
    
    public function testPhoneNumberIsTooLong()
    {
        $profile = clone $this->profile;
        $validator = $this->validator;

        // 1 above
        $profile->setPhoneNumber($phoneNumber = str_repeat('1', 26));
        $errors = $validator->validate($profile);
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('phoneNumber', $errors[0]->getPropertyPath());

        // Boundary
        $profile->setPhoneNumber($phoneNumber = str_repeat('1', 25));
        $errors = $validator->validate($profile);
        $this->assertCount(0, $errors, $this->validationErrorsToString($errors));
    }
    
    public function testNullCounty()
    {
        $profile = clone $this->profile;
        $validator = $this->validator;
    
        $profile->setCounty($county = null);
        $errors = $validator->validate($profile);
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('county', $errors[0]->getPropertyPath());
    }
    
    public function testWrongCountyType()
    {
        $profile = clone $this->profile;
        $validator = $this->validator;

        $profile->setCounty($county = new \stdClass());
        $errors = $validator->validate($profile);
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('county', $errors[0]->getPropertyPath());
    }
    
}

