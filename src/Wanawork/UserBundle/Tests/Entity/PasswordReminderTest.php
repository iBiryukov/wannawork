<?php
namespace Wanawork\UserBundle\Tests\Entity;

use Wanawork\UserBundle\Entity\User;
use Wanawork\UserBundle\Entity\PasswordReminder;

/**
 * Test the password reminder entity
 * 
 * @author Ilya Biryukov <ilya@wanawork.ie>
 */
class PasswordReminderTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Ensure that password reminder retains its validity till expiry
     */
    public function testValidity()
    {
        $passwordReminder = new PasswordReminder(new User(), $ip = '');
        $this->assertTrue($passwordReminder instanceof PasswordReminder);
        $this->assertNull($passwordReminder->getUsedOn());
        $this->assertEquals(PasswordReminder::LIFETIME, $passwordReminder->getLifetime());
        $this->assertTrue($passwordReminder->isValid());
        $passwordReminder->markUsed($ip = '');
        $this->assertFalse($passwordReminder->isValid());
        $this->assertTrue($passwordReminder->getUsedOn() instanceof \DateTime);
    }

    /**
     * Ensure that password reminder expires when due
     */
    public function testExpiry()
    {
        $passwordReminder = new PasswordReminder(new User(), $ip = '', $lifetime = 'PT1S');
        sleep(1);
        $this->assertFalse($passwordReminder->isValid());
    }
}
