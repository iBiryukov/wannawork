<?php
namespace Wanawork\UserBundle\Tests\Entity;

use Wanawork\UserBundle\Entity\Billing\AdOrder;
use Wanawork\UserBundle\Entity\Ad;
use Wanawork\UserBundle\Entity\Billing\Payment;
use Wanawork\UserBundle\Entity\Billing\Transaction;
use Wanawork\UserBundle\Entity\Billing\Order;

class AdOrderTest extends \PHPUnit_Framework_TestCase
{
    public function testExpiryDate()
    {
        $ad = new Ad();
        $bumps = 2;
        $amount = '5.00';
        $vatRate = '23';
        $plan = Ad::PLAN_PREMIUM;
        $duration = 60;
        $adOrder = new AdOrder($ad, $amount, $vatRate, $plan, $duration, $bumps);
        
        $runFrom = new \DateTime();
        $adOrder->setRunFrom($runFrom);
        $expiryDate = $adOrder->getExpiry();
        
        $this->assertTrue($expiryDate instanceof \DateTime);
        $this->assertTrue($runFrom != $expiryDate);
        $this->assertEquals($runFrom->add(new \DateInterval("P{$duration}D")), $expiryDate);
    }
    
    public function testFileName()
    {
        $ad = new Ad();
        $bumps = 2;
        $amount = '5.00';
        $vatRate = '23';
        $plan = Ad::PLAN_PREMIUM;
        $duration = 60;
        $adOrder = new AdOrder($ad, $amount, $vatRate, $plan, $duration, $bumps);
        $adOrder->setStatus(Order::STATUS_PAID);
        
        $payment = new Payment($adOrder, $amount, Payment::SOURCE_PAYPAL);
        $payment->setState(Payment::STATE_DEPOSITED);
        $transaction = new Transaction($payment);
        $transaction->setType(Transaction::TRANSACTION_TYPE_DEPOSIT);
        $transaction->setState(Transaction::STATE_SUCCESS);
        $payment->addTransaction($transaction);
        
        $fileName = sprintf(
            "Ad_%d_%s.pdf",
            $adOrder->getId(),
            $adOrder->getPayDate()->format('Y_m_d')
        );
        
        $this->assertSame($fileName, $adOrder->getReceiptFileName());
    }
}