<?php
namespace Wanawork\UserBundle\Tests\Entity;

use Wanawork\UserBundle\Entity\User;
use Wanawork\UserBundle\Entity\EmployeeProfile;
use Wanawork\MainBundle\Entity\NameTitle;
use Doctrine\Common\Collections\ArrayCollection;
use Wanawork\UserBundle\Entity\Ad;
use Wanawork\MainBundle\Entity\County;
use Symfony\Component\HttpFoundation\File\File;
use Wanawork\UserBundle\Entity\CVForm;
use Wanawork\UserBundle\Entity\CVFile;
use Wanawork\UserBundle\Entity\CvRequest;
use Wanawork\UserBundle\Entity\EmployerProfile;
use Wanawork\UserBundle\Tests\Entity\Mocks\EmployeeProfileMock;
use Wanawork\UserBundle\Tests\Entity\Mocks\EmployerProfileMock;
use Wanawork\UserBundle\Entity\Billing\AdOrder;
use Wanawork\UserBundle\Entity\Billing\Order;
use Wanawork\UserBundle\Entity\Billing\Payment;
class EmployeeProfileTest extends WebUnitTestBase
{
    private $user;
    
    private $profile;
    
    private $validProfile;
    
    private $bareProfile;
    
    private $validator;
    
    private $nonImageFilePath;
    
    protected function setUp()
    {
        $this->user = new User();
        $this->user->setName("Test Name");
        $this->profile = new EmployeeProfileMock();
        $this->profile->setId(-1);
        
        
        $bareProfile = new EmployeeProfile();
        $bareProfile->setUser($this->user);
        $bareProfile->setName($this->user->getName());
        $this->bareProfile = $bareProfile;
        
        $profile = new EmployeeProfile();
        $profile->setName('Ilya Biryukov');
        $profile->setTitle(new NameTitle(-1, 'Mr'));
        $profile->setDob(new \DateTime());
        $profile->setPhoneNumber('086 232 5482');
        $profile->setAddress('New address');
        $profile->setCity('Carlow');
        $profile->setCounty(new County(-1, 'Carlow'));
        $profile->setUser(new User());
        $this->validProfile = $profile;
        
        $client = self::createClient();
        $this->validator = $client->getContainer()->get('validator');
        
        $this->nonImageFilePath = __DIR__ . '/file.txt';
        $fp = fopen($this->nonImageFilePath, 'a+');
        fwrite($fp, 'abc');
        fclose($fp);
    }
    
    protected function tearDown()
    {
        unlink($this->nonImageFilePath);
    }
    
    public function testConstructor()
    {
        $user = clone $this->user;
        $profile = clone $this->profile;
        
        // User        
        $profile->setUser($user);
        $this->assertSame($user, $profile->getUser());
        $this->assertNull($user->getEmployeeProfile());
        $this->assertSame($user->getEmployeeProfile(), $user->getDefaultProfile());
                
        // Name
        $profile->setName($name = 'name');
        $this->assertSame($name, $profile->getName());
        
        // Title
        $nameTitle = new NameTitle(-1, $title = 'test');
        $profile->setTitle($nameTitle);
        $this->assertSame($nameTitle, $profile->getTitle());
        
        // DOB
        $dob = new \DateTime();
        $profile->setDob($dob);
        $this->assertSame($dob, $profile->getDob());
        
        // Ads
        $this->assertTrue($profile->getAds() instanceof ArrayCollection);
        $this->assertCount(0, $profile->getAds());
        
        $ad = new Ad();
        $profile->addAd($ad);
        $this->assertCount(1, $profile->getAds());
        $this->assertContains($ad, $profile->getAds());
        $this->assertTrue($profile->hasAds());
        
        $profile->removeAd($ad);
        $this->assertCount(0, $profile->getAds());
    }
    
    public function testFullValidation()
    {
        $profile = $this->validProfile;
    
        $validator = $this->validator;
        $errors = $validator->validate($profile, 'step1');
        $this->assertCount(0, $errors, $this->validationErrorsToString($errors));
    }
    
    public function testBareValidation()
    {
        $profile = $this->bareProfile;
        
        $validator = $this->validator;
        $errors = $validator->validate($profile, 'step1');
        $this->assertCount(0, $errors, $this->validationErrorsToString($errors));
    }
    
    public function testEmptyName()
    {
        $profile = clone $this->bareProfile;
        $profile->setName($name = '');
        $validator = $this->validator;
        
        $errors = $validator->validate($profile, 'step1');
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('name', $errors[0]->getPropertyPath());
    }
    
    public function testNameTooLong()
    {
        $profile = clone $this->bareProfile;
        $validator = $this->validator;

        // 1 above
        $profile->setName($name = str_repeat('a', 256)); 
        
        $errors = $validator->validate($profile, 'step1');
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('name', $errors[0]->getPropertyPath());
        
        // Exact
        $profile->setName($name = str_repeat('a', 255));
        
        $errors = $validator->validate($profile, 'step1');
        $this->assertCount(0, $errors, $this->validationErrorsToString($errors));
    }
    
    public function testNullUser()
    {
        $profile = clone $this->bareProfile;
        $validator = $this->validator;
    
        $profile->setUser($user = null);
        $errors = $validator->validate($profile, 'step1');
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('user', $errors[0]->getPropertyPath());
    
        $errors = $validator->validate($profile, 'step1');
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('user', $errors[0]->getPropertyPath());
    }
    
    public function testWrongUserType()
    {
        $profile = clone $this->bareProfile;
        $validator = $this->validator;
    
        $profile->setUser($user = new \stdClass());
        $errors = $validator->validate($profile, 'step1');
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('user', $errors[0]->getPropertyPath());
    
        $errors = $validator->validate($profile, 'step1');
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('user', $errors[0]->getPropertyPath());
    }
    
    
    public function testMaxiumumAvatarSize()
    {
        // @todo        
    }
    
//     public function testNonImageAvatarFile()
//     {
//         $file = new File($this->nonImageFilePath);
//         $profile = clone $this->validProfile;
//         $validator = $this->validator;
        
//         $profile->setAvatarFile($file);
        
//         $errors = $validator->validate($profile);
//         $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
//         $this->assertSame('avatarFile', $errors[0]->getPropertyPath());
        
//         $errors = $validator->validate($profile, 'step1');
//         $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
//         $this->assertSame('avatarFile', $errors[0]->getPropertyPath());
//     }
    
    public function testCV()
    {
        $user = clone $this->user;
        $profile = clone $this->profile;
        
        $cvs = array(
        	new CVForm(),
            new CVFile(),
        );
        
        $profile->setCvs($cvs);
        $this->assertCount(2, $profile->getCvs());
        $this->assertSame($cvs[0], $profile->getCvs()->first());
        $this->assertSame($cvs[1], $profile->getCvs()->last());
    }
    
    public function testCanPayWithTwitter()
    {
        $profile = clone $this->profile;
        
        $this->assertTrue($profile->canPayWithTwitter());
        
        $ad = new Ad();
        $profile->addAd($ad);
        
        $amount = '150';
        $vatRate = '23';
        $plan = Ad::PLAN_STANDARD;
        $duration = 60;
        $bumps = 5;
        
        $order = new AdOrder($ad, $amount, $vatRate, $plan, $duration, $bumps);
        $order->setStatus(Order::STATUS_PAID);
        $source = Payment::SOURCE_TWITTER;
        $payment = new Payment($order, $amount, $source);
        $payment->setState(Payment::STATE_DEPOSITED);

        $this->assertFalse($profile->canPayWithTwitter());
    }
    
    
    
//     public function testCVRequests()
//     {
//         $user = clone $this->user;
//         $profile = clone $this->profile;
        
//         $ad1 = new Ad();
//         $profile->addAd($ad1);
        
//         $r1 = new CvRequest();
//         $r1->setEmployerProfile($p1 = new EmployerProfileMock());
//         $p1->setId(-1);
//         $ad1->addAccessRequest($r1);
//         $r1->setStatus(CvRequest::AWAITING);
        
        
//         $r2 = new CvRequest();
//         $r2->setEmployerProfile($p2 = new EmployerProfileMock());
//         $p2->setId(-2);
//         $ad1->addAccessRequest($r2);
//         $r2->setStatus(CvRequest::DENIED);
        
//         $r3 = new CvRequest();
//         $r3->setEmployerProfile($p3 = new EmployerProfileMock());
//         $p3->setId(-3);
//         $ad1->addAccessRequest($r3);
        
//         $r3->setStatus(CvRequest::GRANTED);

//         $this->assertCount(3, $profile->getCvRequests());
        
//         $this->assertCount(1, $profile->getCvRequests(CvRequest::AWAITING));
//         $this->assertSame($r1, $profile->getCvRequests(CvRequest::AWAITING)->first());
        
//         $this->assertCount(1, $profile->getCvRequests(CvRequest::DENIED));
//         $this->assertSame($r2, $profile->getCvRequests(CvRequest::DENIED)->first());
        
//         $this->assertCount(1, $profile->getCvRequests(CvRequest::GRANTED));
//         $this->assertSame($r3, $profile->getCvRequests(CvRequest::GRANTED)->first());
//     }

    public function testTitleNotRequired()
    {
        $profile = clone $this->bareProfile;
        $validator = $this->validator;
        
        $profile->setTitle(null);
        $errors = $validator->validate($profile, 'step1');
            
        $this->assertCount(0, $errors, $this->validationErrorsToString($errors));
    }
    
    public function testTitleNotNullAndWrongType()
    {
        $profile = clone $this->bareProfile;
        $validator = $this->validator;
        
        $profile->setTitle(123);
        $errors = $validator->validate($profile, 'step1');
        
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('title', $errors[0]->getPropertyPath());
    }
    
    public function testDobIsNotRequired()
    {
        $profile = clone $this->bareProfile;
        $validator = $this->validator;
        
        $profile->setDob(null);
        $errors = $validator->validate($profile, 'step1');
        $this->assertCount(0, $errors, $this->validationErrorsToString($errors));
    }
    
    public function testDobIsWrongType()
    {
        $profile = clone $this->bareProfile;
        $validator = $this->validator;
        
        $profile->setDob(123);
        $errors = $validator->validate($profile, 'step1');
        
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('dob', $errors[0]->getPropertyPath());
    }
    
    public function testSkypeIsNotRequired()
    {
        $profile = clone $this->bareProfile;
        $validator = $this->validator;
        
        $profile->setSkype(null);
        $errors = $validator->validate($profile, 'step1');
        $this->assertCount(0, $errors, $this->validationErrorsToString($errors));
    }
    
    // 255 chars
    public function testSkypeLength()
    {
        $profile = clone $this->bareProfile;
        $validator = $this->validator;
        
        // 1 above
        $profile->setSkype(str_repeat('a', 256));
        $errors = $validator->validate($profile, 'step1');
        $this->assertCount(1, $errors);
        $this->assertSame('skype', $errors[0]->getPropertyPath());
        
        // exact
        $profile->setSkype(str_repeat('a', 255));
        $errors = $validator->validate($profile, 'step1');
        $this->assertCount(0, $errors);
    }
    
    public function testPhoneNumberIsNotRequired()
    {
        $profile = clone $this->bareProfile;
        $validator = $this->validator;
        
        $profile->setPhoneNumber(null);
        $errors = $validator->validate($profile, 'step1');
        $this->assertCount(0, $errors, $this->validationErrorsToString($errors));
    }
    
    public function testPhoneNumberIsTooLong()
    {
        $profile = clone $this->bareProfile;
        $validator = $this->validator;
    
        // 1 above
        $profile->setPhoneNumber($phoneNumber = str_repeat('1', 26));
        $errors = $validator->validate($profile, 'step1');
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('phoneNumber', $errors[0]->getPropertyPath());
    
        // Boundary
        $profile->setPhoneNumber($phoneNumber = str_repeat('1', 25));
        $errors = $validator->validate($profile, 'step1');
        $this->assertCount(0, $errors, $this->validationErrorsToString($errors));
    }
    
    public function testAddressIsNotRequired()
    {
        $profile = clone $this->bareProfile;
        $validator = $this->validator;
        
        $profile->setAddress(null);
        $errors = $validator->validate($profile, 'step1');
        $this->assertCount(0, $errors, $this->validationErrorsToString($errors));
    }
    
    public function testAddressIsTooLong()
    {
        $profile = clone $this->bareProfile;
        $validator = $this->validator;
    
        // 1 above
        $profile->setAddress(str_repeat('1', 501));
        $errors = $validator->validate($profile, 'step1');
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('address', $errors[0]->getPropertyPath());
    
        // Boundary
        $profile->setAddress(str_repeat('1', 500));
        $errors = $validator->validate($profile, 'step1');
        $this->assertCount(0, $errors, $this->validationErrorsToString($errors));
    }
    
    public function testCityIsNotRequired()
    {
        $profile = clone $this->bareProfile;
        $validator = $this->validator;
        
        $profile->setCity(null);
        $errors = $validator->validate($profile, 'step1');
        $this->assertCount(0, $errors, $this->validationErrorsToString($errors));
    }

    public function testCityIsTooLong()
    {
        $profile = clone $this->bareProfile;
        $validator = $this->validator;
    
        // 1 above
        $profile->setCity(str_repeat('1', 151));
        $errors = $validator->validate($profile, 'step1');
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('city', $errors[0]->getPropertyPath());
    
        // Boundary
        $profile->setCity(str_repeat('1', 150));
        $errors = $validator->validate($profile, 'step1');
        $this->assertCount(0, $errors, $this->validationErrorsToString($errors));
    }
    
    
    public function testCountyCanBeNull()
    {
        $profile = clone $this->bareProfile;
        $validator = $this->validator;
        
        $profile->setCounty(null);
        $errors = $validator->validate($profile, 'step1');
        $this->assertCount(0, $errors, $this->validationErrorsToString($errors));
    }
    
    public function testWrongCountyType()
    {
        $profile = clone $this->bareProfile;
        $validator = $this->validator;
    
        $profile->setCounty($county = new \stdClass());
        $errors = $validator->validate($profile, 'step1');
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('county', $errors[0]->getPropertyPath());
    }
    
    
    
}