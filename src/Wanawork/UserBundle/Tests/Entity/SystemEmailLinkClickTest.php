<?php
namespace Wanawork\UserBundle\Tests\Entity;

use Wanawork\UserBundle\Entity\SystemEmailLinkClick;
use Wanawork\UserBundle\Entity\SystemEmailLink;
use Wanawork\UserBundle\Entity\SystemEmail;
class SystemEmailLinkClickTest extends WebUnitTestBase
{
    public function testConstructor()
    {
        $systemEmail = new SystemEmail($user = null, $subject = null, $type = SystemEmail::TYPE_BLANK, $email = null);
        
        $href = 'test1';
        $text = 'test2';
        $link = new SystemEmailLink($systemEmail, $href, $text);
        
        $ip = 'ip';
        $click = new SystemEmailLinkClick($link, $ip);
        
        $this->assertNull($click->getId());
        
        $this->assertTrue($click->getDate() instanceof \DateTime);
        $this->assertEquals($click->getDate(), $click->getDate());
        $this->assertNotSame($click->getDate(), $click->getDate());
        
        $this->assertSame($ip, $click->getIp());
        
        $this->assertSame($link, $click->getLink());
    }
    
    public function testIdUniqueness()
    {
        $ids = array();
        $systemEmail = new SystemEmail($user = null, $subject = null, $type = SystemEmail::TYPE_BLANK, $email = null);
        for($i = 1; $i < 100; $i++) {
            $href = 'test1';
            $text = 'test2';
            $link = $systemEmail->addLink($href, $text);
            $ids[] = $link->getId();
        }
        
        $this->assertTrue(sizeof($ids) > 0);
        $this->assertCount(sizeof($ids), array_unique($ids));
    }
}