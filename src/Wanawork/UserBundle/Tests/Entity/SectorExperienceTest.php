<?php
namespace Wanawork\UserBundle\Tests\Entity;

use Wanawork\UserBundle\Entity\SectorExperience;
class SectorExperienceTest extends WebUnitTestBase
{
    private $validator;
    
    protected function setUp()
    {
        $client = self::createClient();
        $this->validator = $client->getContainer()->get('validator');
    }
    
    protected function tearDown()
    {
        $this->validator = null;
    }
    
    
    public function testEntityOk()
    {
        $sectorExp = new SectorExperience(-1, $name = 'test');
    
        $this->assertSame($sectorExp->getId(), -1);
        $this->assertSame('test', $sectorExp->getName());
        $this->assertSame($sectorExp->getName(), (string)$sectorExp);
    }
    
    public function testEmptyName()
    {
        $sectorExp = new SectorExperience(-1, $name = '');
    
        $validator = $this->validator;
        $errors = $validator->validate($sectorExp);
    
        $this->assertCount(1, $errors);
        $this->assertSame('name', $errors[0]->getPropertyPath());
    }
    
    public function testNameIsTooLong()
    {
        $sectorExp = new SectorExperience(-1, $name = str_repeat('a', 256), $order = 1);
    
        $validator = $this->validator;
        $errors = $validator->validate($sectorExp);
    
        $this->assertCount(1, $errors);
        $this->assertSame('name', $errors[0]->getPropertyPath());
    }
}