<?php
namespace Wanawork\UserBundle\Tests\Entity;

use Wanawork\UserBundle\Entity\UnsubscribedEmail;
use Wanawork\UserBundle\Entity\SystemEmail;
use Wanawork\UserBundle\Entity\User;

class UnsubscribedEmailTest extends WebUnitTestBase
{
    private $validator;
    
    private $em;
    
    protected function setUp()
    {
        $client = self::createClient();
        $this->validator = $client->getContainer()->get('validator');
        $this->em = $client->getContainer()->get('doctrine');
    }
    
    protected function tearDown()
    {
        $this->user = null;
        $this->validator = null;
        $this->em->getConnection()->close();
    }
    
    public function testConstructor()
    {
        $email = 'test@test.com';
        $unsub = new UnsubscribedEmail($email);
        
        $this->assertSame($email, $unsub->getEmail());
        $this->assertTrue($unsub->getDateUnsubscribed() instanceof \DateTime);
        $this->assertNull($unsub->getSystemEmail());
        $this->assertSame($unsub->getEmail(), $unsub->getId());
    }
    
    public function testConstructorWithSystemEmail()
    {
        $email = 'test@test.com';
        $subject = 'test';
        $type = SystemEmail::TYPE_REGISTRATION_CANDIDATE;
        
        $user = new User();
        $systemEmail = new SystemEmail($user, $subject, $type);
        $unsub = new UnsubscribedEmail($email, $systemEmail);
        
        $this->assertSame($email, $unsub->getEmail());
        $this->assertTrue($unsub->getDateUnsubscribed() instanceof \DateTime);
        $this->assertSame($systemEmail, $unsub->getSystemEmail());
    }
    
    public function testEmptyEmail()
    {
        $email = '';
        $unsub = new UnsubscribedEmail($email);
        $validator = $this->validator;
        $errors = $validator->validate($unsub);
        $this->assertEquals(1, sizeof($errors), $this->validationErrorsToString($errors));
        $this->assertSame('email', $errors[0]->getPropertyPath(), $this->validationErrorsToString($errors));
    }
    
    public function testEmailTooLong()
    {
        $email = "a@a.com" . str_repeat('a', 54);
        $unsub = new UnsubscribedEmail($email);
        
        $validator = $this->validator;
        $errors = $validator->validate($unsub);
        $this->assertEquals(1, sizeof($errors), $this->validationErrorsToString($errors));
        $this->assertSame('email', $errors[0]->getPropertyPath());
    }
}