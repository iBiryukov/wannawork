<?php
namespace Wanawork\UserBundle\Tests\Entity;

use Wanawork\UserBundle\Entity\SearchPageStat;
use Wanawork\UserBundle\Entity\Search;
class SearchPageStatTest extends WebUnitTestBase
{
    
    public function testConstructor()
    {
        $search = new Search();
        $page = 1;
        $adScores = array(99, 85);
        $avgScore = array_sum($adScores) / sizeof($adScores);
        $totalFound = 5;
        
        $searchPageStat = new SearchPageStat($search, $page, $adScores, $totalFound);
        
        $this->assertNull($searchPageStat->getId());
        $this->assertSame($page, $searchPageStat->getPage());
        $this->assertSame($search, $searchPageStat->getSearch());
        $this->assertSame($adScores, $searchPageStat->getAdScores());
        $this->assertSame($avgScore, $searchPageStat->getAverageAdScore());
        
        $this->assertTrue($searchPageStat->getDate() instanceof \DateTime);
        $this->assertNotSame($searchPageStat->getDate(), $searchPageStat->getDate());
        
        $this->assertSame($totalFound, $searchPageStat->getTotalFound());
    }
    
    public function testEmptyScoresYieldsNullAvgScore()
    {
        $search = new Search();
        $page = 1;
        $adScores = array();
        
        $searchPageStat = new SearchPageStat($search, $page, $adScores, 10);
        $this->assertNull($searchPageStat->getAverageAdScore());
    }
    
    
}