<?php
namespace Wanawork\UserBundle\Tests\Entity;

use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Wanawork\UserBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Validator\ConstraintViolation;

class WebUnitTestBase extends WebTestCase
{
    /**
     * Turn validation error object into string
     * @param ConstraintViolationListInterface $errors
     *
     * @return string
     */
    public function validationErrorsToString(ConstraintViolationListInterface $errors)
    {
        $errorString = "";
        foreach ($errors as $error) {
            if ($error instanceof ConstraintViolation) {
                $errorString .= "'{$error->getPropertyPath()}' -- '{$error->getMessage()}'\n";
            } else {
                throw new \Exception(
	               sprintf("Invalid class of error: '%s'. Expected 'ConstraintViolation'", get_class($error))
                );
            }
            
        }
        return $errorString;
    }
    
    protected function authenticateBlankCandidate()
    {
        $client = self::createClient();
        
        // Fill up the request for authentification purposes
        $client->request('GET', '/');
        $container = $client->getContainer();
        
        $em = $container->get('doctrine')->getManager();
        $blankCandidate = $em->getRepository('WanaworkUserBundle:User')->findOneByEmail('blank_candidate@wanawork.ie');
        
        if(!$blankCandidate instanceof User) {
            throw new \Exception('Blank candidate is missing from test DB');
        }
        
        $security = $container->get('security.context');
        
        // Test Login
        $token = new UsernamePasswordToken($user, null, 'secured_area', $user->getRoles());
        $security->setToken($token);
        
        $event = new InteractiveLoginEvent($client->getRequest(), $token);
        $container->get("event_dispatcher")->dispatch("security.interactive_login", $event);
        
        return $blankCandidate;
    }
    
}