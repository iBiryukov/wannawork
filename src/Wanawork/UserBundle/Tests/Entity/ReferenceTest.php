<?php
namespace Wanawork\UserBundle\Tests\Entity;

use Wanawork\UserBundle\Entity\Reference;
use Wanawork\UserBundle\Entity\CV;
use Wanawork\UserBundle\Entity\CVForm;

class ReferenceTest extends WebUnitTestBase
{
    
    private $validator;
    
    protected function setUp()
    {
        $client = self::createClient();
        $container = $client->getContainer();
        $this->validator = $container->get('validator');
        $this->em = $container->get('doctrine')->getManager();
        $this->em->getConnection()->beginTransaction();
    }
    
    protected function tearDown()
    {
        $this->em->getConnection()->rollBack();
        $this->em->clear();
        $this->em->getConnection()->close();
        $this->validator = null;
    }
    
    public function testEntityOK()
    {
        $reference = new Reference();
        
        // ID
        $this->assertNull($reference->getId());
        
        // Employer
        $reference->setEmployer($employer = 'test');
        $this->assertSame($employer, $reference->getEmployer());
        
        // Contact
        $reference->setContact($contact = 'test');
        $this->assertSame($contact, $reference->getContact());
        
        // Telephone
        $reference->setTelephone($telephone = '123');
        $this->assertSame($telephone, $reference->getTelephone());
        
        // Email
        $reference->setEmail($email = 'email');
        $this->assertSame($email, $reference->getEmail());
        
        // CV
        $reference->setCv($cv = new CVForm());
        $this->assertSame($cv, $reference->getCv());
    }
    
    public function testEmployerNameIsRequired()
    {
        $reference = new Reference();
        $reference->setEmployer($employer = '');
        $reference->setContact($contact = 'test');
        $reference->setTelephone($telephone = '123');
        $reference->setEmail($email = 'name@wanawork.ie');
        $reference->setCv($cv = new CVForm());
        
        $validator = $this->validator;
        $errors = $validator->validate($reference);
        
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('employer', $errors[0]->getPropertyPath());
    }
    
    public function testEmployerNameIsTooLong()
    {
        // Above
        $reference = new Reference();
        $reference->setEmployer($employer = str_repeat('a', 201));
        $reference->setContact($contact = 'test');
        $reference->setTelephone($telephone = '123');
        $reference->setEmail($email = 'name@wanawork.ie');
        $reference->setCv($cv = new CVForm());
        
        $validator = $this->validator;
        $errors = $validator->validate($reference);
        
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('employer', $errors[0]->getPropertyPath());
        
        // Exact
        $reference->setEmployer($employer = str_repeat('a', 200));
        $errors = $validator->validate($reference);
        $this->assertCount(0, $errors, $this->validationErrorsToString($errors));
    }
    
    public function testContactNameIsRequired()
    {
        $reference = new Reference();
        $reference->setEmployer($employer = 'abc');
        $reference->setContact($contact = '');
        $reference->setTelephone($telephone = '123');
        $reference->setEmail($email = 'name@wanawork.ie');
        $reference->setCv($cv = new CVForm());
    
        $validator = $this->validator;
        $errors = $validator->validate($reference);
    
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('contact', $errors[0]->getPropertyPath());
    }
    
    public function testContactNameIsTooLong()
    {
        // Above
        $reference = new Reference();
        $reference->setEmployer($employer = 'abc');
        $reference->setContact($contact = str_repeat('a', 201));
        $reference->setTelephone($telephone = '123');
        $reference->setEmail($email = 'name@wanawork.ie');
        $reference->setCv($cv = new CVForm());
    
        $validator = $this->validator;
        $errors = $validator->validate($reference);
    
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('contact', $errors[0]->getPropertyPath());
    
        // Exact
        $reference->setContact($contact = str_repeat('a', 200));
        $errors = $validator->validate($reference);
        $this->assertCount(0, $errors, $this->validationErrorsToString($errors));
    }
    
    public function testTelephoneIsRequired()
    {
        $reference = new Reference();
        $reference->setEmployer($employer = 'abc');
        $reference->setContact($contact = 'abc');
        $reference->setTelephone($telephone = '');
        $reference->setEmail($email = 'name@wanawork.ie');
        $reference->setCv($cv = new CVForm());
    
        $validator = $this->validator;
        $errors = $validator->validate($reference);
    
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('telephone', $errors[0]->getPropertyPath());
    }
    
    public function testTelephoneIsTooLong()
    {
        // Above
        $reference = new Reference();
        $reference->setEmployer($employer = 'abc');
        $reference->setContact($contact = 'aa');
        $reference->setTelephone($telephone = str_repeat('a', 21));
        $reference->setEmail($email = 'name@wanawork.ie');
        $reference->setCv($cv = new CVForm());
    
        $validator = $this->validator;
        $errors = $validator->validate($reference);
    
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('telephone', $errors[0]->getPropertyPath());
    
        // Exact
        $reference->setTelephone($telephone = str_repeat('a', 20));
        $errors = $validator->validate($reference);
        $this->assertCount(0, $errors, $this->validationErrorsToString($errors));
    }
    
    public function testEmailIsRequired()
    {
        $reference = new Reference();
        $reference->setEmployer($employer = 'abc');
        $reference->setContact($contact = 'abc');
        $reference->setTelephone($telephone = 'aa');
        $reference->setEmail($email = '');
        $reference->setCv($cv = new CVForm());
    
        $validator = $this->validator;
        $errors = $validator->validate($reference);
    
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('email', $errors[0]->getPropertyPath());
    }
    
    public function testValidEmailIsRequired()
    {
        $reference = new Reference();
        $reference->setEmployer($employer = 'abc');
        $reference->setContact($contact = 'abc');
        $reference->setTelephone($telephone = 'aa');
        $reference->setEmail($email = 'aaaa');
        $reference->setCv($cv = new CVForm());
    
        $validator = $this->validator;
        $errors = $validator->validate($reference);
    
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('email', $errors[0]->getPropertyPath());
    }
}
