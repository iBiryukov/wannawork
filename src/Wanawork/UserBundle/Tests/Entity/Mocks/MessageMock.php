<?php
namespace Wanawork\UserBundle\Tests\Entity\Mocks;

use Wanawork\UserBundle\Entity\Message;

class MessageMock extends Message
{
    
    public function setPostedOn(\DateTime $date)
    { 
        $this->postedOn = $date;
    }
}