<?php
namespace Wanawork\UserBundle\Tests\Entity\Mocks;

use Wanawork\UserBundle\Entity\Billing\Voucher\Voucher;

class AbstractVoucherMock extends Voucher
{
    public function supports(\Wanawork\UserBundle\Entity\Billing\Order $order)
    {
        return true;
    }
}