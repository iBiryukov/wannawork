<?php
namespace Wanawork\UserBundle\Tests\Entity\Mocks;

use Wanawork\UserBundle\Entity\Billing\Order as AbstractOrder;

class Order extends AbstractOrder
{
    public function getInternalDescription()
    {
        return 'test';
    }
    
    public function getOrderType()
    {
        return 'test';
    }
    
    public function getPublicDescription()
    {
        return 'test';
    }
    
    public function getPayer()
    {
        return null;
    }
    
    public function  getReceiptFileName()
    {
        return 'test';
    }
    
}