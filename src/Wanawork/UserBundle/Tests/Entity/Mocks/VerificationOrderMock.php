<?php
namespace Wanawork\UserBundle\Tests\Entity\Mocks;

use Wanawork\UserBundle\Entity\Billing\VerificationOrder;
class VerificationOrderMock extends VerificationOrder
{
    private $date;
    
    public function setCreatedAt($date)
    {
        $this->date = $date;
    }
    
    public function getCreatedAt()
    {
        return $this->date;
    }
}