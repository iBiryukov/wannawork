<?php
namespace Wanawork\UserBundle\Tests\Entity\Mocks;

use Wanawork\UserBundle\Entity\EmployerProfile;

class EmployerProfileMock extends EmployerProfile
{
    public function setId($id)
    {
        $this->id = $id;
    }
}