<?php
namespace Wanawork\UserBundle\Tests\Entity\Mocks;

use Wanawork\UserBundle\Entity\EmployeeProfile;

class EmployeeProfileMock extends EmployeeProfile
{
    public function setId($id)
    {
        $this->id = $id;
    }
}