<?php
namespace Wanawork\UserBundle\Tests\Entity;

use Wanawork\UserBundle\Entity\WorkExperience;
use Wanawork\UserBundle\Entity\CV;
use Wanawork\UserBundle\Entity\CVForm;
class WorkExperienceTest extends WebUnitTestBase
{
    
    private $validator;
    
    protected function setUp()
    {
        $client = self::createClient();
        $this->validator = $client->getContainer()->get('validator');
    }
    
    public function testEntityOK()
    {
        $workExperience = new WorkExperience();

        // ID
        $this->assertNull($workExperience->getId());
        
        // Start
        $workExperience->setStart($start = new \DateTime());
        $this->assertEquals($start, $workExperience->getStart());
        $this->assertNotSame($start, $workExperience->getStart());
        
        // Finish
        $workExperience->setFinish($finish = new \DateTime());
        $this->assertEquals($finish, $workExperience->getFinish());
        $this->assertNotSame($finish, $workExperience->getFinish());
        
        // Company
        $workExperience->setCompany($company = 'Test');
        $this->assertSame($company, $workExperience->getCompany());
        
        // Position
        $workExperience->setPosition($position = 'abc');
        $this->assertSame($position, $workExperience->getPosition());
        
        // Description
        $workExperience->setDescription($description = 'aa');
        $this->assertSame($description, $workExperience->getDescription());
        
        // CV
        $workExperience->setCv($cv = new CVForm());
        $this->assertSame($cv, $workExperience->getCv());
    }
    
    public function testStartDateMissing()
    {
        $workExperience = new WorkExperience();
        $workExperience->setFinish($finish = new \DateTime());
        $workExperience->setCompany($company = 'Test');
        $workExperience->setPosition($position = 'abc');
        $workExperience->setDescription($description = 'aa');
        $workExperience->setCv($cv = new CVForm());
        
        $validator = $this->validator;
        $errors = $validator->validate($workExperience);
                
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('start', $errors[0]->getPropertyPath());
    }
    
    public function testFinishDateIsNotRequired()
    {
        $workExperience = new WorkExperience();
        $workExperience->setStart($start = new \DateTime());
        $workExperience->setCompany($company = 'Test');
        $workExperience->setPosition($position = 'abc');
        $workExperience->setDescription($description = 'aa');
        $workExperience->setCv($cv = new CVForm());
    
        $validator = $this->validator;
        $errors = $validator->validate($workExperience);
    
        $this->assertCount(0, $errors, $this->validationErrorsToString($errors));
    }
    
    public function testFinishDateMustBeGreaterThanStartDate()
    {
        $workExperience = new WorkExperience();
        $workExperience->setStart($start = new \DateTime('tomorrow'));
        $workExperience->setFinish($finish = new \DateTime());
        $workExperience->setCompany($company = 'Test');
        $workExperience->setPosition($position = 'abc');
        $workExperience->setDescription($description = 'aa');
        $workExperience->setCv($cv = new CVForm());
        
        $validator = $this->validator;
        $errors = $validator->validate($workExperience);
        
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('finish', $errors[0]->getPropertyPath());
    }

    public function testCompanyNameIsMissing()
    {
        $workExperience = new WorkExperience();
        $workExperience->setStart($start = new \DateTime());
        $workExperience->setFinish($finish = new \DateTime('tomorrow'));
        $workExperience->setCompany($company = null);
        $workExperience->setPosition($position = 'abc');
        $workExperience->setDescription($description = 'aa');
        $workExperience->setCv($cv = new CVForm());
    
        $validator = $this->validator;
        $errors = $validator->validate($workExperience);
    
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('company', $errors[0]->getPropertyPath());
    }
    
    public function testCompanyNameIsTooLong()
    {
        // Above
        $workExperience = new WorkExperience();
        $workExperience->setStart($start = new \DateTime());
        $workExperience->setFinish($finish = new \DateTime('tomorrow'));
        $workExperience->setCompany($company = str_repeat('a', 201));
        $workExperience->setPosition($position = 'abc');
        $workExperience->setDescription($description = 'aa');
        $workExperience->setCv($cv = new CVForm());
        
        $validator = $this->validator;
        $errors = $validator->validate($workExperience);
        
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('company', $errors[0]->getPropertyPath());
        
        // Limit 
        $workExperience->setCompany($company = str_repeat('a', 200));
        $errors = $validator->validate($workExperience);
        $this->assertCount(0, $errors, $this->validationErrorsToString($errors));
    }
    
    
    public function testPositionNameIsMissing()
    {
        $workExperience = new WorkExperience();
        $workExperience->setStart($start = new \DateTime());
        $workExperience->setFinish($finish = new \DateTime('tomorrow'));
        $workExperience->setCompany($company = 'a');
        $workExperience->setPosition($position = '');
        $workExperience->setDescription($description = 'aa');
        $workExperience->setCv($cv = new CVForm());
    
        $validator = $this->validator;
        $errors = $validator->validate($workExperience);
    
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('position', $errors[0]->getPropertyPath());
    }
    
    public function testPositionNameIsTooLong()
    {
        // Above
        $workExperience = new WorkExperience();
        $workExperience->setStart($start = new \DateTime());
        $workExperience->setFinish($finish = new \DateTime('tomorrow'));
        $workExperience->setPosition($position = str_repeat('a', 201));
        $workExperience->setCompany($company = 'abc');
        $workExperience->setDescription($description = 'aa');
        $workExperience->setCv($cv = new CVForm());
    
        $validator = $this->validator;
        $errors = $validator->validate($workExperience);
    
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('position', $errors[0]->getPropertyPath());
    
        // Limit
        $workExperience->setPosition($position = str_repeat('a', 200));
        $errors = $validator->validate($workExperience);
        $this->assertCount(0, $errors, $this->validationErrorsToString($errors));
    }
    
    
    public function testDescriptionIsMissing()
    {
        $workExperience = new WorkExperience();
        $workExperience->setStart($start = new \DateTime());
        $workExperience->setFinish($finish = new \DateTime('tomorrow'));
        $workExperience->setCompany($company = 'a');
        $workExperience->setPosition($position = 'aa');
        $workExperience->setDescription($description = '');
        $workExperience->setCv($cv = new CVForm());
    
        $validator = $this->validator;
        $errors = $validator->validate($workExperience);
    
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('description', $errors[0]->getPropertyPath());
    }
    
    public function testDescriptionIsTooLong()
    {
        // Above
        $workExperience = new WorkExperience();
        $workExperience->setStart($start = new \DateTime());
        $workExperience->setFinish($finish = new \DateTime('tomorrow'));
        $workExperience->setPosition($position = 'aa');
        $workExperience->setCompany($company = 'abc');
        $workExperience->setDescription($description = str_repeat('a', 1001));
        $workExperience->setCv($cv = new CVForm());
    
        $validator = $this->validator;
        $errors = $validator->validate($workExperience);
    
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('description', $errors[0]->getPropertyPath());
    
        // Limit
        $workExperience->setDescription(str_repeat('a', 1000));
        $errors = $validator->validate($workExperience);
        $this->assertCount(0, $errors, $this->validationErrorsToString($errors));
    }
    
    public function testCVIsRequired()
    {
        
        $workExperience = new WorkExperience();
        $workExperience->setStart($start = new \DateTime());
        $workExperience->setFinish($finish = new \DateTime('tomorrow'));
        $workExperience->setPosition($position = 'aa');
        $workExperience->setCompany($company = 'abc');
        $workExperience->setDescription($description = str_repeat('a', 101));

        $validator = $this->validator;
        $errors = $validator->validate($workExperience);
        
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('cv', $errors[0]->getPropertyPath());
        
    }
    
    public function testDurationUsesGenericDurationCalculator()
    {
        $workExp = new WorkExperience();
        $workExp->setStart($startDate = new \DateTime('2012-10-01'));
        $workExp->setFinish($finishDate = new \DateTime('2012-12-01'));
    
        $dateCalculatorMock = $this->getMockClass('\Wanawork\UserBundle\Util\Date', array(
            'diffInMonthsAndYears'
        ));
    
        $dateCalculatorMock::staticExpects($this->once())
        ->method('diffInMonthsAndYears')
        ->with($this->equalTo($finishDate), $this->equalTo($startDate));
    
        $workExp->dateUtility = ($dateCalculatorMock);
        $workExp->getDuration();
    }
    
    public function testDurationUsesTodaysDateIfFinishDateIsEmpty()
    {
        $workExp = new WorkExperience();
        $workExp->setStart($startDate = new \DateTime('last year'));
        $workExp->setFinish($finishDate = null);
        $duration = $workExp->getDuration();
        $this->assertSame('1 year', $duration);
    }
}