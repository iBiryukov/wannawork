<?php
namespace Wanawork\UserBundle\Tests\Entity\billing;

use Wanawork\UserBundle\Tests\Entity\Mocks\Order;
use Wanawork\UserBundle\Tests\Entity\WebUnitTestBase;
use Wanawork\UserBundle\Entity\Billing\Payment;
use Wanawork\UserBundle\Entity\Billing\Transaction;
use Wanawork\UserBundle\Entity\User;

class OrderTest extends WebUnitTestBase
{
    
    public function testConstructor()
    {
        $amount = 244;
        $vatRate = 23;
        $vat = 56;
        
        $order = new Order($amount, $vatRate);
        $this->assertNull($order->getId());
        $this->assertSame($amount, $order->getAmount());
        $this->assertSame($amount, $order->getAmountExcludingVat());
        $this->assertSame($vat, $order->getVat(), "$vat != {$order->getVat()}");
        $this->assertSame($vatRate, $order->getVatRate());
        $this->assertSame($amount + $vat, $order->getAmountIncludingVat());
        $this->assertSame(Order::STATUS_PENDING, $order->getStatus());
        $this->assertSame(array_search($order->getStatus(), Order::$validStatuses), $order->getStatusName());
        $this->assertTrue($order->getCreatedAt() instanceof \DateTime);
        $this->assertCount(0, $order->getPayments());
        $this->assertCount(0, $order->getItems());
        $this->assertNull($order->getPayDate());
        $this->assertNull($order->getUser());
    }
    
    public function testPaidOrder()
    {
        $order = new Order(5, 6);
        $order->setStatus(Order::STATUS_PAID);
        $this->assertSame($order->getStatus(), Order::STATUS_PAID);
        
        $payment = new Payment($order, $amount = 10, $source = Payment::SOURCE_PAYPAL);
        $payment->setState(Payment::STATE_DEPOSITED);
        $order->addPayment($payment);
        $transaction = new Transaction($payment);
        $transaction->setType(Transaction::TRANSACTION_TYPE_DEPOSIT);
        $transaction->setState(Transaction::STATE_SUCCESS);
        $payment->addTransaction($transaction);
                
        $this->assertTrue($order->getPayDate() instanceof \DateTime);
        $this->assertCount(1, $order->getPayments());
    }
    
    /**
     * @expectedException \InvalidArgumentException
     */
    public function testInvalidStatus()
    {
        $order = new Order(5, 6);
        $order->setStatus(-1);
    }
    
    public function testSetUser()
    {
        $order = new Order(5, 6);
        $order->setUser($user = new User());
        
        $this->assertSame($user, $order->getUser());
    }

}