<?php
namespace Wanawork\UserBundle\Tests\Entity\billing\Voucher;

use Wanawork\UserBundle\Tests\Entity\WebUnitTestBase;
use Wanawork\UserBundle\Tests\Entity\Mocks\AbstractVoucherMock;
use Wanawork\UserBundle\Entity\Billing\AdOrder;
use Wanawork\UserBundle\Entity\Ad;
use Wanawork\UserBundle\Entity\Billing\Payment;

class AbstractVoucherTest extends WebUnitTestBase
{
    public function testConstructor()
    {
        $code = 'Wannawork1';
        $voucher = new AbstractVoucherMock($code);
        $this->assertSame($code, $voucher->getCode());
        $this->assertCount(0, $voucher->getRedemptions());
        
        $ad = new Ad();
        $amount = 100;
        $vatRate = 23;
        $plan = Ad::PLAN_PREMIUM;
        $duration = 60;
        $bumps = 5;
        $order = new AdOrder($ad, $amount, $vatRate, $plan, $duration, $bumps);
        $this->assertTrue($voucher->supports($order));
    }
    
    public function testRedemptions()
    {
        $voucher = new AbstractVoucherMock($code = 'Wannawork1');
        $this->assertCount(0, $voucher->getRedemptions());
        
        $ad = new Ad();
        $amount = 100;
        $vatRate = 23;
        $plan = Ad::PLAN_PREMIUM;
        $duration = 60;
        $bumps = 5;
        $order = new AdOrder($ad, $amount, $vatRate, $plan, $duration, $bumps);
        $payment = new Payment($order, $amount, $source = Payment::SOURCE_VOUCHER);
        $voucher->addRedemptions($payment);
        
        $this->assertCount(1, $voucher->getRedemptions());
        $this->assertSame($payment, $voucher->getRedemptions()->first());
    }
    
    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Vouchers can only accept payments with 'voucher' source
     */
    public function testInvalidPaymentRedemptions()
    {
        $voucher = new AbstractVoucherMock($code = 'Wannawork1');
        $this->assertCount(0, $voucher->getRedemptions());
        
        $ad = new Ad();
        $amount = 100;
        $vatRate = 23;
        $plan = Ad::PLAN_PREMIUM;
        $duration = 60;
        $bumps = 5;
        $order = new AdOrder($ad, $amount, $vatRate, $plan, $duration, $bumps);
        $payment = new Payment($order, $amount, $source = Payment::SOURCE_PAYPAL);
        $voucher->addRedemptions($payment);
    }
}