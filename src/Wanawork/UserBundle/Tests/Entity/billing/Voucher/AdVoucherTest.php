<?php
namespace Wanawork\UserBundle\Tests\Entity\billing\Voucher;

use Wanawork\UserBundle\Tests\Entity\WebUnitTestBase;
use Wanawork\UserBundle\Entity\Billing\Voucher\AdVoucher;
use Wanawork\UserBundle\Entity\Ad;
use Wanawork\UserBundle\Entity\Billing\AdOrder;
use Wanawork\UserBundle\Entity\Billing\Payment;
use Wanawork\UserBundle\Entity\EmployeeProfile;
use Wanawork\UserBundle\Entity\Billing\Transaction;
use Wanawork\UserBundle\Entity\Billing\Order;

class AdVoucherTest extends WebUnitTestBase
{

    public function testGlobalLimit()
    {
        $code = 'test';
        $voucher = new AdVoucher($code);
        $this->assertNull($voucher->getGlobalLimit());
        $this->assertFalse($voucher->globalLimitReached());
        
        $voucher->setGlobalLimit(1);
        
        $ad = new Ad();
        $amount = 100;
        $vatRate = 23;
        $plan = Ad::PLAN_PREMIUM;
        $duration = 60;
        $bumps = 5;
        $order = new AdOrder($ad, $amount, $vatRate, $plan, $duration, $bumps);
        $payment = new Payment($order, $amount, $source = Payment::SOURCE_VOUCHER);
        $voucher->addRedemptions($payment);
        
        $this->assertTrue($voucher->globalLimitReached());
    }
    
    public function testUserLimit()
    {
        $profile = new EmployeeProfile();
        $code = 'test';
        $voucher = new AdVoucher($code);
        $this->assertNull($voucher->getUserLimit());
        $this->assertFalse($voucher->userLimitReached($profile));
        
        $ad = new Ad();
        $ad->setProfile($profile);
        $amount = 100;
        $vatRate = 23;
        $plan = Ad::PLAN_PREMIUM;
        $duration = 60;
        $bumps = 5;
        
        $order = new AdOrder($ad, $amount, $vatRate, $plan, $duration, $bumps);
        $order->setStatus(Order::STATUS_PAID);
        $payment = new Payment($order, $amount, $source = Payment::SOURCE_VOUCHER);
        $payment->setState(Payment::STATE_DEPOSITED);
        $transaction = new Transaction($payment);
        $transaction->setState(Transaction::STATE_SUCCESS);
        $transaction->setType(Transaction::TRANSACTION_TYPE_DEPOSIT);
        $transaction->setTrackingId($voucher->getCode());
        $payment->addTransaction($transaction);
        $voucher->addRedemptions($payment);
        
        $voucher->setUserLimit(1);
        $this->assertTrue($voucher->userLimitReached($profile));
        $this->assertFalse($voucher->userLimitReached(new EmployeeProfile()));
        
        $voucher->setUserLimit(2);
        $this->assertFalse($voucher->userLimitReached($profile));
        $this->assertFalse($voucher->userLimitReached(new EmployeeProfile()));
    }
    
    public function testSupports()
    {
        $profile = new EmployeeProfile();
        $ad = new Ad();
        $ad->setProfile($profile);
        $amount = 100;
        $vatRate = 23;
        $plan = Ad::PLAN_PREMIUM;
        $duration = 60;
        $bumps = 5;
        
        $order = new AdOrder($ad, $amount, $vatRate, $plan, $duration, $bumps);
        $order->setStatus(Order::STATUS_PAID);
        $payment = new Payment($order, $amount, $source = Payment::SOURCE_VOUCHER);
        $payment->setState(Payment::STATE_DEPOSITED);
        $transaction = new Transaction($payment);
        $transaction->setState(Transaction::STATE_SUCCESS);
        $transaction->setType(Transaction::TRANSACTION_TYPE_DEPOSIT);
        $transaction->setTrackingId('code');
        $payment->addTransaction($transaction);
        
        $voucher = $this->getMock('\Wanawork\UserBundle\Entity\Billing\Voucher\AdVoucher',
            array('userLimitReached', 'globalLimitReached'), array('code')
        );
        
        $voucher->expects($this->once())
        ->method('userLimitReached')
        ->will($this->returnValue(false));
        
        $voucher->expects($this->once())
        ->method('userLimitReached')
        ->with($this->identicalTo($profile))
        ->will($this->returnValue(false));
        
        $this->assertTrue($voucher->supports($order));
    }
}