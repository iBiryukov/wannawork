<?php
namespace Wanawork\UserBundle\Tests\Entity;

use Wanawork\MainBundle\Entity\County;
class CountyTest extends WebUnitTestBase
{ 
    private $validator;
    
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    
    public function setUp()
    {
        //$this->validator = Validation::createValidatorBuilder()->enableAnnotationMapping()->getValidator();
        $client = self::createClient();
        $container = $client->getContainer();
        $this->validator = $container->get('validator');
        $this->em = $container->get('doctrine')->getManager();
        $this->em->getConnection()->beginTransaction();
    }
    
    public function tearDown()
    {
        $this->validator = null;
        $this->em->getConnection()->rollBack();
        $this->em->clear();
        $this->em->getConnection()->close();
    }
    
    public function testEntityOk()
    {
        $county = new County($id = -1, $name = 'test');
    
        $this->assertSame($id, $county->getId());
        $this->assertSame('test', $county->getName());
        $this->assertSame($county->getName(), (string)$county);
    }
    
    public function testEmptyName()
    {
        $county = new County(-1, $name = '');
    
        $validator = $this->validator;
        $errors = $validator->validate($county);
    
        $this->assertCount(1, $errors);
        $this->assertSame('name', $errors[0]->getPropertyPath());
    }
    
    public function testNameIsTooLong()
    {
        $county = new County(-1, $name = str_repeat('a', 71));
    
        $validator = $this->validator;
        $errors = $validator->validate($county);
    
        $this->assertCount(1, $errors);
        $this->assertSame('name', $errors[0]->getPropertyPath());
    }
}