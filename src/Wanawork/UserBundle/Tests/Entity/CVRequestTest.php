<?php
namespace Wanawork\UserBundle\Tests\Entity;

use Wanawork\UserBundle\Entity\CvRequest;
use Wanawork\UserBundle\Entity\Ad;
use Wanawork\UserBundle\Entity\EmployerProfile;
use Wanawork\UserBundle\Entity\User;
use Wanawork\UserBundle\Entity\JobSpec;
use Wanawork\UserBundle\Tests\Entity\Mocks\EmployerProfileMock;

class CVRequestTest extends WebUnitTestBase
{
    private $validator;
    
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    
    public function setUp()
    {
        $client = self::createClient();
        $container = $client->getContainer();
        $this->validator = $container->get('validator');
        $this->em = $container->get('doctrine')->getManager();
        $this->em->getConnection()->beginTransaction();
    }
    
    public function tearDown()
    {
        $this->validator = null;
        $this->em->getConnection()->rollBack();
        $this->em->clear();
        $this->em->getConnection()->close();
    }
    
    public function testEntityOK()
    {
        $user = new User();
        $ad = new Ad();
        $profile = new EmployerProfileMock();
        $profile->setId(-1);
        $user->addEmployerProfile($profile, $isAdmin = true);
        $cvRequest = new CvRequest($user, $ad, $profile, new JobSpec());
        
        // ID
        $this->assertNull($cvRequest->getid());
        
        // User
        $this->assertSame($user, $cvRequest->getUser());
        
        // Employer Profile
        $this->assertSame($profile, $cvRequest->getEmployerProfile());
        
        // Ad 
        $this->assertSame($ad, $cvRequest->getAd());   
        
        // Create Date
        $this->assertInstanceOf('\DateTime', $cvRequest->getCreateDate());

        // Response Date
        $this->assertNull($cvRequest->getResponseDate());

        // Mail Thread
        $this->assertNull($cvRequest->getMailThread());
        
        // Status
        $this->assertSame($cvRequest->getStatuses(), CvRequest::$statuses);
        $this->assertSame(CvRequest::AWAITING, $cvRequest->getStatus());
        $this->assertTrue($cvRequest->isPending());
        $this->assertSame('pending', $cvRequest->getStatusName());
        $this->assertNull($cvRequest->getMailThread());
        
        $cvRequest->approve();
        $this->assertSame(CvRequest::GRANTED, $cvRequest->getStatus());
        $this->assertInstanceOf('\DateTime', $cvRequest->getResponseDate());
        $this->assertTrue($cvRequest->isGranted());
        $this->assertSame('approved', $cvRequest->getStatusName());
        $this->assertInstanceOf('\Wanawork\UserBundle\Entity\MailThread', $cvRequest->getMailThread());
        
        $cvRequest->deny();
        $this->assertSame(CvRequest::DENIED, $cvRequest->getStatus());
        $this->assertInstanceOf('\DateTime', $cvRequest->getResponseDate());
        $this->assertTrue($cvRequest->isDenied());
        $this->assertSame('denied', $cvRequest->getStatusName());
        $this->assertInstanceOf('\Wanawork\UserBundle\Entity\MailThread', $cvRequest->getMailThread());
        
        try {
            $statuses = $cvRequest->getStatuses();
            $status = sizeof($statuses) + 1;
        	$cvRequest->setStatus($status);
        	$this->fail('Expected setStatus to throw exception');
        } catch (\InvalidArgumentException $e) {
            //ok
        }
        
        // message 
        $cvRequest->setMessage($message = 'test');
        $this->assertSame($message, $cvRequest->getMessage());
        
        // Job spec
        $cvRequest->setJob($job = new JobSpec());
        $this->assertSame($job, $cvRequest->getJob());
    }
    
    public function testMessageOrJobSpecIsRequired()
    {
        $user = new User();
        $profile = new EmployerProfileMock();
        $profile->setId(-1);
        $user->addEmployerProfile($profile, true);
        $ad = new Ad();
        $cvRequest = new CvRequest($user, $ad, $profile);
        
        $validator = $this->validator;
        $errors = $validator->validate($cvRequest);
        $this->assertCount(1, $errors);
        
        $this->assertEmpty($errors[0]->getPropertyPath());
        
    }
    
//     public function testNameForNonExistingStatus()
//     {
//         $user = new User();
//         $ad = new Ad();
//         $profile = new EmployerProfile();
//         $user->addEmployerProfile($profile, $isAdmin = true);
//         $cvRequest = new CvRequest($user, $ad, $profile);
        
//         CvRequest::$statuses[] = sizeof(CvRequest::$statuses);
//         $cvRequest->setStatus(4);
//         $this->assertSame('N/A', $cvRequest->getStatusName());
//     }
    
}