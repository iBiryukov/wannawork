<?php
namespace Wanawork\UserBundle\Tests\Entity;

use Wanawork\UserBundle\Entity\EmployeeProfile;
use Wanawork\MainBundle\Entity\County;
use Symfony\Component\HttpFoundation\File\File;
use Wanawork\UserBundle\Entity\CvRequest;
use Wanawork\UserBundle\Entity\User;
use Wanawork\UserBundle\Entity\EmployerProfile;
use Wanawork\UserBundle\Entity\JobSpec;
use Wanawork\UserBundle\Entity\Ad;
use Wanawork\UserBundle\Entity\Message;
use Wanawork\UserBundle\Tests\Entity\Mocks\MessageMock;
use Wanawork\UserBundle\Tests\Entity\Mocks\EmployerProfileMock;
class AbstractProfileTest extends WebUnitTestBase
{
    private $avatarFile;
    
    protected function setUp()
    {
        copy(__DIR__ . '/avatar.jpg', __DIR__ . '/avatar_test.jpg');
        $this->avatarFile = new File(__DIR__ . '/avatar_test.jpg');
    }
    
    protected function tearDown()
    {
        $path = $this->avatarFile->getPathname();
        if (file_exists($path)) {
           unlink($path); 
        }
    }
    
    public function testEntityOk()
    {
        $profile = new EmployeeProfile();
        
        // Id
        $this->assertNull($profile->getId());
        
        // Skype
        $this->assertNull($profile->getSkype());
        $profile->setSkype($skype = 'test');
        $this->assertSame($skype, $profile->getSkype());
        
        // Phone Number 
        $profile->setPhoneNumber($number = '123456');
        $this->assertSame($number, $profile->getPhoneNumber());
        
        // address
        $profile->setAddress($address = 'abc');
        $this->assertSame($address, $profile->getAddress());
        
        // City
        $profile->setCity($city = 'carlow');
        $this->assertSame($city, $profile->getCity());
        
        // County
        $county = new County(-1, $name = 'Wexford');
        $profile->setCounty($county);
        $this->assertSame($county, $profile->getCounty());
        
        // Test avatar upload
        $file = $this->avatarFile;
        $profile->setAvatarFile($file);
        $this->assertSame($file, $profile->getAvatarFile());
        $this->assertNotNull($profile->getAbsolutePath());
        $this->assertNotNull($profile->getWebPath());
        
        $currentPath = $file->getPathname();
        $profile->upload();        
        $this->assertNotEquals(realpath($currentPath), realpath($profile->getAbsolutePath()));
        $this->assertFileExists($profile->getAbsolutePath());
        
        $profile->removeUpload();
        $this->assertFileNotExists($profile->getAbsolutePath());
        
    }
    
    public function testUploadWithNoFile()
    {
        $profile = new EmployeeProfile();
        $profile->upload();
        
        $this->assertNull($profile->getAbsolutePath());
        $this->assertNull($profile->getWebPath());
    }
    
    
    public function testMailboxes()
    {
        $profile = new EmployeeProfile();
        
        $user = new User();
        $profile = new EmployerProfileMock();
        $profile->setId(-1);
        
        $user->addEmployerProfile($profile, $isAdmin = true);
        $cvRequest1 = new CvRequest($user, new Ad(), $profile, new JobSpec());
        $cvRequest1->approve();
        $cvRequest2 = new CvRequest($user, new Ad(), $profile, new JobSpec());
        $cvRequest2->approve();
        $cvRequest2->deny();
        $cvRequest3 = new CvRequest($user, new Ad(), $profile, new JobSpec());
        
        $profile->addCvRequest($cvRequest1);
        $profile->addCvRequest($cvRequest2);
        $profile->addCvRequest($cvRequest3);
        
        $this->assertCount(3, $profile->getCvRequests());
        $this->assertCount(2, $profile->getMailboxes());

        $thread1 = $cvRequest1->getMailThread();
        $thread1->setUpdatedOn(new \DateTime('yesterday'));
        
        $thread2 = $cvRequest2->getMailThread();
        $thread2->setUpdatedOn(new \DateTime());
        
        $threads = $profile->getMailboxes();
        
        $this->assertSame($threads[0], $thread2);
        $this->assertSame($threads[1], $thread1);
    }
}