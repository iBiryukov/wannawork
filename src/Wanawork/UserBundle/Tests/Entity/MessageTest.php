<?php
namespace Wanawork\UserBundle\Tests\Entity;

use Wanawork\UserBundle\Entity\Message;
use Wanawork\UserBundle\Entity\User;
use Wanawork\UserBundle\Entity\EmployeeProfile;
use Wanawork\UserBundle\Entity\MailThread;
use Wanawork\UserBundle\Entity\CvRequest;
use Wanawork\UserBundle\Entity\Ad;
use Wanawork\UserBundle\Entity\EmployerProfile;
use Wanawork\UserBundle\Entity\JobSpec;
use Wanawork\UserBundle\Tests\Entity\Mocks\MessageMock;
use Wanawork\UserBundle\Tests\Entity\Mocks\EmployerProfileMock;

class MessageTest extends WebUnitTestBase
{
    private $validator;
    
    /**
     *
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    
    
    protected function setUp()
    {
        $client = self::createClient();
        $container = $client->getContainer();
        $this->validator = $container->get('validator');
        $this->em = $container->get('doctrine')->getManager();
        $this->em->getConnection()->beginTransaction();
    }
    
    protected function tearDown()
    {
        $this->validator = null;
        $this->em->getConnection()->rollBack();
        $this->em->clear();
        $this->em->getConnection()->close();
    }
    
    public function testEntityOk()
    {
        $userEmployee = new User();
        $profileEmployee = new EmployeeProfile();
        $userEmployee->setEmployeeProfile($profileEmployee);
        
        $userEmployer = new User();
        $profileEmployer = new EmployerProfileMock();
        $profileEmployer->setId(-1);
        $userEmployer->addEmployerProfile($profileEmployer, $isAdmin = true);
        
        $ad = new Ad();
        $ad->setProfile($profileEmployee);
        
        $cvRequest = new CvRequest($userEmployer, $ad, $profileEmployer, new JobSpec());
        $thread = new MailThread($cvRequest);
        
        $text = 'text';
        $message = new Message($text, $author = $userEmployee, $profile = $profileEmployee, $thread);
        
        // ID
        $this->assertNull($message->getId());
        
        // Thread
        $this->assertSame($thread, $message->getThread());
        
        // Author 
        $this->assertSame($author, $message->getAuthor());
        
        // Profile
        $this->assertSame($profileEmployee, $message->getProfile());
        $this->assertTrue($message->isAuthorEmployee());
        $this->assertFalse($message->isAuthorEmployer());
        
        // Text 
        $this->assertSame($text, $message->getText());
        
        // Posted on
        $this->assertInstanceOf('\DateTime', $message->getPostedOn());
        $this->assertNotSame($message->getPostedOn(), $message->getPostedOn());
        
        // is read
        $this->assertFalse($message->isRead());
        
        $message->markRead();
        $this->assertTrue($message->isRead());
        
        $this->assertSame($text, $message->getShortText());
        
        $longText = str_repeat('a', 20);
        $message->setText($longText);
        $this->assertSame('aaa...', $message->getShortText(3));
        
    }
    
    /**
     * @expectedException Wanawork\UserBundle\Exception\MessageProfileMismatch
     */
    public function testMessageWithEmployeeProfileMismatch()
    {
        $mismatchProfile = new EmployeeProfile();
        
        $userEmployee = new User();
        $profileEmployee = new EmployeeProfile();
        $userEmployee->setEmployeeProfile($profileEmployee);
        
        $userEmployer = new User();
        $profileEmployer = new EmployerProfileMock();
        $profileEmployer->setId(-1);
        $userEmployer->addEmployerProfile($profileEmployer, $isAdmin = true);
        
        $ad = new Ad();
        $ad->setProfile($profileEmployee);
        
        $cvRequest = new CvRequest($userEmployer, $ad, $profileEmployer, new JobSpec());
        $thread = new MailThread($cvRequest);
        
        $text = 'text';
        $message = new Message($text, $author = $userEmployee, $profile = $mismatchProfile, $thread);
        
    }
    
    /**
     * @expectedException Wanawork\UserBundle\Exception\MessageProfileMismatch
     */
    public function testMessageWithEmployerProfileMismatch()
    {
        $mismatchProfile = new EmployerProfile();
    
        $userEmployee = new User();
        $profileEmployee = new EmployeeProfile();
        $userEmployee->setEmployeeProfile($profileEmployee);
    
        $userEmployer = new User();
        $profileEmployer = new EmployerProfileMock();
        $profileEmployer->setId(-1);
        $userEmployer->addEmployerProfile($profileEmployer, $isAdmin = true);
    
        $ad = new Ad();
        $ad->setProfile($profileEmployee);
    
        $cvRequest = new CvRequest($userEmployer, $ad, $profileEmployer, new JobSpec());
        $thread = new MailThread($cvRequest);
    
        $text = 'text';
        $message = new Message($text, $author = $userEmployee, $profile = $mismatchProfile, $thread);
    
    }
    
    public function testMessageTextIsRequired()
    {
        $validator = $this->validator;
        
        $userEmployee = new User();
        $profileEmployee = new EmployeeProfile();
        $userEmployee->setEmployeeProfile($profileEmployee);
        
        $userEmployer = new User();
        $profileEmployer = new EmployerProfileMock();
        $profileEmployer->setId(-1);
        $userEmployer->addEmployerProfile($profileEmployer, $isAdmin = true);
        
        $ad = new Ad();
        $ad->setProfile($profileEmployee);
        
        $cvRequest = new CvRequest($userEmployer, $ad, $profileEmployer, new JobSpec());
        $thread = new MailThread($cvRequest);
        
        $text = '';
        $message = new Message($text, $author = $userEmployee, $profile = $profileEmployee, $thread);
        
        $errors = $validator->validate($message);
        $this->assertCount(1, $errors);
        $this->assertSame('text', $errors[0]->getPropertyPath());
    }
    
    
    public function testPostDateInWords()
    {
        $yearLength = 31557600;
        $dayLength = 86400;
        $this->assertSame($dayLength, Message::DAY_LENGTH);
        $this->assertSame($yearLength, Message::YEAR_LENGTH);
        
        $text = 'test';
        $author = new User();
        $profile = new EmployerProfileMock();
        $profile->setId(-1);
        $cvRequest = new CvRequest();
        $cvRequest->setEmployerProfile($profile);
        $cvRequest->setAd(new Ad());
        $cvRequest->setUser($author);
        $cvRequest->setMessage($message = 'test');
        $thread = new MailThread($cvRequest);
        $message = new MessageMock($text, $author, $profile, $thread);
        
        // same day
        $message->setPostedOn($date = new \DateTime());
        $this->assertSame($date->format('G:i'), $message->getPostDateInWords());
        
        // same week, same year
        for ($i = 1; $i <= 7; $i++) {
            $date = new \DateTime();
            $date->setTimestamp(time() - $dayLength * $i);
            $message->setPostedOn($date);
            $this->assertSame($date->format('l'), $message->getPostDateInWords());
        }
        
        // same week, same year, but 1 second over a week
        $date = new \DateTime();
        $date->setTimestamp(time() - $dayLength * 7 - 1);
        $message->setPostedOn($date);
        $this->assertNotSame($date->format('l'), $message->getPostDateInWords());
        
        // within a year, under one second
        $date = new \DateTime();
        $date->setTimestamp(time() - $yearLength + 1);
        $message->setPostedOn($date);
        $this->assertSame($date->format('j F'), $message->getPostDateInWords());
        
        // within 1 year exact
        $date = new \DateTime();
        $date->setTimestamp(time() - $yearLength);
        $message->setPostedOn($date);
        $this->assertSame($date->format('d/m/Y'), $message->getPostDateInWords());
    }
    
    public function testConstructorBumpsThreadTime()
    {
        $userEmployee = new User();
        $profileEmployee = new EmployeeProfile();
        $userEmployee->setEmployeeProfile($profileEmployee);
        
        $userEmployer = new User();
        $profileEmployer = new EmployerProfileMock();
        $profileEmployer->setId(-1);
        $userEmployer->addEmployerProfile($profileEmployer, $isAdmin = true);
        
        $ad = new Ad();
        $ad->setProfile($profileEmployee);
        
        $cvRequest = new CvRequest($userEmployer, $ad, $profileEmployer, new JobSpec());
        
        $thread = new MailThread($cvRequest);
        $this->assertTrue($thread->getUpdatedOn() instanceof \DateTime);
        
        sleep(2);
        $message = new Message($text = '', $author = $userEmployee, $profileEmployee, $thread);
        $thread->addMessage($message);
        $this->assertEquals($message->getPostedOn(), $thread->getUpdatedOn());
        
        
    }
}