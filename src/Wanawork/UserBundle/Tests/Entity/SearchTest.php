<?php
namespace Wanawork\UserBundle\Tests\Entity;

use Wanawork\UserBundle\Entity\Search;
use Doctrine\Common\Collections\ArrayCollection;
use Wanawork\UserBundle\Entity\Ad;
use Wanawork\UserBundle\Entity\SearchPageStat;
use Wanawork\UserBundle\Entity\SectorJob;
use Wanawork\UserBundle\Entity\Sector;
use Wanawork\UserBundle\Entity\SectorExperience;
use Wanawork\UserBundle\Entity\PositionType;
use Wanawork\UserBundle\Entity\Qualification;
use Wanawork\MainBundle\Entity\Language;
use Symfony\Component\Validator\Constraints\Count;
use Wanawork\MainBundle\Entity\County;
use Wanawork\UserBundle\Entity\User;
use Wanawork\UserBundle\Entity\EmployerProfile;
use Wanawork\UserBundle\Entity\SearchNotification;

class SearchTest extends WebUnitTestBase
{
    
    public function testConstructor()
    {
        $search = new Search();
        $this->assertNotNull($search->getId());
        
        $this->assertNull($search->getProfession());
        $this->assertTrue($search->getIndustries() instanceof ArrayCollection);
        $this->assertTrue($search->getPositions() instanceof ArrayCollection);
        $this->assertTrue($search->getLanguages() instanceof ArrayCollection);
        $this->assertTrue($search->getLocations() instanceof ArrayCollection);
        $this->assertNull($search->getExperience());
        $this->assertNull($search->getEducationLevel());
        $this->assertNull($search->getUser());
        
        $this->assertTrue($search->getDate() instanceof \DateTime);
        $this->assertEquals($search->getDate(), $search->getDate());
        $this->assertNotSame($search->getDate(), $search->getDate());
        $this->assertNull($search->getUpdateDate());
        $this->assertNull($search->getNotification());
        $this->assertFalse($search->getIncludeExpired());
        
        $this->assertTrue($search->getPageStats() instanceof ArrayCollection);
        $this->assertSame(0, $search->getCountOfUsedFields());
        $this->assertFalse($search->hasNotifier());
    }

    public function testSetAds()
    {
        $search = new Search();
        $user = new User();
        
        $profile = new EmployerProfile();
        $user->addEmployerProfile($profile, true);
        $search->setUser($user);
        
        $matches = array();
        $ad1 = new Ad();
        $ad1->setMatch($matches[] = 50);
        
        $ad2 = new Ad();
        $ad2->setMatch($matches[] = 40);
        
        $ad3 = new Ad();
        $ad3->setMatch($matches[] = 60);
        
        $ads = array($ad1, $ad2, $ad3);
        $page = 1;
        $perPage = 3;
        $totalFound = 10;
        
        $this->assertCount(0, $search->getPageStats());
        $stat = $search->setAds($ads, $page, $perPage, $totalFound);
        $this->assertCount(1, $search->getPageStats());
        $this->assertTrue($stat instanceof SearchPageStat);
        
        $this->assertSame($matches, $stat->getAdScores());
        $this->assertSame(array_sum($matches) / sizeof($matches), $stat->getAverageAdScore());
        $this->assertSame($totalFound, $stat->getTotalFound());
        $this->assertSame($search, $stat->getSearch());
        $this->assertSame($page, $stat->getPage());
    }
    
    public function testCountOfFields()
    {
        $search = new Search();
        $this->assertSame(0, $search->getCountOfUsedFields());
        
        $search->setProfession(new SectorJob(-1, $name = 'test'));
        $search->setNumberOfUsedFields();
        $this->assertSame(1, $search->getCountOfUsedFields());
        
        $search->addIndustry(new Sector($id = -1, $name = 'test'));
        $search->setNumberOfUsedFields();
        $this->assertSame(2, $search->getCountOfUsedFields());
        
        $search->setExperience(new SectorExperience($id = -1, $name = 'test'));
        $search->setNumberOfUsedFields();
        $this->assertSame(3, $search->getCountOfUsedFields());
        
        $search->addPosition(new PositionType($id = -1,$name = 'test', $order = 1));
        $search->setNumberOfUsedFields();
        $this->assertSame(4, $search->getCountOfUsedFields());
        
        $search->setEducationLevel(new Qualification($id = -1, $name = 'test', $isPopular = false));
        $search->setNumberOfUsedFields();
        $this->assertSame(5, $search->getCountOfUsedFields());
        
        $search->addLanguage(new Language($id = -1, 'test'));
        $search->setNumberOfUsedFields();
        $this->assertSame(6, $search->getCountOfUsedFields());
        
        $search->addLocation(new County(-1, 'test'));
        $search->setNumberOfUsedFields();
        $this->assertSame(7, $search->getCountOfUsedFields());
    }
    
    public function testIndustries()
    {
        $search = new Search();
        $this->assertTrue($search->getIndustries() instanceof ArrayCollection);
        $this->assertCount(0, $search->getIndustries());
        
        $industry = new Sector(1, 'abc');
        $search->addIndustry($industry);
        $this->assertCount(1, $search->getIndustries());
        $this->assertContainsOnly($industry, $search->getIndustries());
        $search->removeIndustry($industry);
        $this->assertCount(0, $search->getIndustries());
        
        $search->addIndustry(new Sector(1, 'abc'));
        $search->setIndustries(array($industry));
        $this->assertCount(1, $search->getIndustries());
        $this->assertContainsOnly($industry, $search->getIndustries());
        $search->addIndustry($industry);
        $this->assertCount(1, $search->getIndustries());
    }
    
    public function testPositions()
    {
        $search = new Search();
        $this->assertTrue($search->getPositions() instanceof ArrayCollection);
        $this->assertCount(0, $search->getPositions());
        
        $position = new PositionType($id = -1, $name = 'test', $order = 1);
        $search->addPosition($position);
        $this->assertCount(1, $search->getPositions());
        $this->assertContainsOnly($position, $search->getPositions());
        $search->removePosition($position);
        $this->assertCount(0, $search->getPositions());
        
        $search->addPosition(new PositionType($id = -1, $name = 'test2', $order = 2));
        $search->setPositions(array($position));
        $this->assertCount(1, $search->getPositions());
        $this->assertContainsOnly($position, $search->getPositions());
        $search->addPosition($position);
        $this->assertCount(1, $search->getPositions());
    }
    
    
    public function testLocations()
    {
        $search = new Search();
        $this->assertTrue($search->getLocations() instanceof ArrayCollection);
        $this->assertCount(0, $search->getLocations());
    
        $location = new County(-1, $name = 'test');
        $search->addLocation($location);
        $this->assertCount(1, $search->getLocations());
        $this->assertContainsOnly($location, $search->getLocations());
        $search->removeLocation($location);
        $this->assertCount(0, $search->getLocations());
    
        $search->addLocation(new County(-1, $name = 'test2'));
        $search->setLocations(array($location));
        $this->assertCount(1, $search->getLocations());
        $this->assertContainsOnly($location, $search->getLocations());
        $search->addLocation($location);
        $this->assertCount(1, $search->getLocations());
    }
    
    public function testLanguages()
    {
        $search = new Search();
        $this->assertTrue($search->getLanguages() instanceof ArrayCollection);
        $this->assertCount(0, $search->getLanguages());
    
        $language = new Language($id = -1, 'test');
        $search->addLanguage($language);
        $this->assertCount(1, $search->getLanguages());
        $this->assertContainsOnly($language, $search->getLanguages());
        $search->removeLanguage($language);
        $this->assertCount(0, $search->getLanguages());
    
        $search->addLanguage(new Language($id = -1, $name = 'test2'));
        $search->setLanguages(array($language));
        $this->assertCount(1, $search->getLanguages());
        $this->assertContainsOnly($language, $search->getLanguages());
        $search->addLanguage($language);
        $this->assertCount(1, $search->getLanguages());
    }
    
    public function testIncludedFlag()
    {
        $search = new Search();
        $this->assertFalse($search->getIncludeExpired());
        
        $search->setIncludeExpired(true);
        $this->assertTrue($search->getIncludeExpired());
    }
    
    public function testSetNotification()
    {
        $search = new Search();
        $this->assertFalse($search->hasNotifier());
        $this->assertNull($search->getNotification());
        
        $notification = new SearchNotification($search, $name = 'test');
        $search->setNotification($notification);
        
        $this->assertTrue($search->hasNotifier());
        $this->assertSame($notification, $search->getNotification());
    }
}