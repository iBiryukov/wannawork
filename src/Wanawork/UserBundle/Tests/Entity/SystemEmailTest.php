<?php
namespace Wanawork\UserBundle\Tests\Entity;
use Wanawork\UserBundle\Entity\SystemEmail;
use Wanawork\UserBundle\Entity\User;

class SystemEmailTest extends WebUnitTestBase
{
    public function testConstructor()
    {
        $user = new User();
        $user->setEmail($email = 'test@test.com');
        $this->assertCount(0, $user->getSystemEmails());
        
        $subject = 'test';
        $type = SystemEmail::TYPE_REGISTRATION_CANDIDATE;
        $systemEmail = new SystemEmail($user, $subject, $type);
        
        $this->assertNotNull($systemEmail->getId());
        $this->assertSame(SystemEmail::STATUS_NEW, $systemEmail->getStatus());
        $this->assertSame($email, $systemEmail->getEmail());
        $this->assertSame($subject, $systemEmail->getSubject());
        $this->assertNull($systemEmail->getDateSent());
        $this->assertSame($user, $systemEmail->getUser());
        $this->assertSame(0, $systemEmail->getAttempts());
        $this->assertTrue($systemEmail->getDateCreated() instanceof \DateTime);
        $this->assertFalse($systemEmail->isSent());
        $this->assertNull($systemEmail->getText());
        $this->assertCount(1, $user->getSystemEmails());
        $this->assertSame($type, $systemEmail->getType());
    }

    public function testMarkAsSent()
    {
        $user = new User();
        $user->setEmail($email = 'test@test.com');
        
        $subject = 'test';
        $type = SystemEmail::TYPE_REGISTRATION_CANDIDATE;
        $systemEmail = new SystemEmail($user, $subject, $type);
        
        $systemEmail->markSent();
        
        $this->assertTrue($systemEmail->getDateSent() instanceof \DateTime);
        $this->assertSame(SystemEmail::STATUS_SENT, $systemEmail->getStatus());
        $this->assertTrue($systemEmail->isSent());
    }
    
    /**
     * @expectedException \InvalidArgumentException
     */
    public function testInvalidTypeThrowsException()
    {
        $user = new User();
        $user->setEmail($email = 'test@test.com');
        
        $subject = 'test';
        $type = -1;
        $systemEmail = new SystemEmail($user, $subject, $type);
    }
    
    /**
     * @expectedException \InvalidArgumentException
     */
    public function testInvalidStatusThrowsException()
    {
        $user = new User();
        $user->setEmail($email = 'test@test.com');
        
        $subject = 'test';
        $type = SystemEmail::TYPE_REGISTRATION_CANDIDATE;
        $systemEmail = new SystemEmail($user, $subject, $type);
        
        $systemEmail->setStatus(-1);
    }
    
    public function testIncreaseSendAttempts()
    {
        $user = new User();
        $user->setEmail($email = 'test@test.com');
        
        $subject = 'test';
        $type = SystemEmail::TYPE_REGISTRATION_CANDIDATE;
        $systemEmail = new SystemEmail($user, $subject, $type);
        
        $attempts = 0;
        $this->assertSame($attempts, $systemEmail->getAttempts());
        
        ++$attempts;
        $systemEmail->increaseAttempts();
        $this->assertSame($attempts, $systemEmail->getAttempts());
        
    }
    
    public function testStatusNameReturnsAString()
    {
        $user = new User();
        $user->setEmail($email = 'test@test.com');
        
        $subject = 'test';
        $type = SystemEmail::TYPE_REGISTRATION_CANDIDATE;
        $systemEmail = new SystemEmail($user, $subject, $type);
        
        $this->assertNotEmpty($systemEmail->getStatusName());
    }
    
    public function testTypeNameReturnsAString()
    {
        $user = new User();
        $user->setEmail($email = 'test@test.com');
    
        $subject = 'test';
        $type = SystemEmail::TYPE_REGISTRATION_CANDIDATE;
        $systemEmail = new SystemEmail($user, $subject, $type);
    
        $this->assertNotEmpty($systemEmail->getTypeName());
    }
    
    public function testSetText()
    {
        $user = new User();
        $user->setEmail($email = 'test@test.com');
        
        $subject = 'test';
        $type = SystemEmail::TYPE_REGISTRATION_CANDIDATE;
        $systemEmail = new SystemEmail($user, $subject, $type);
        
        $text = 'test';
        $systemEmail->setText($text);
        $this->assertSame($text, $systemEmail->getText());
    }
    
    public function testSetTextWithOneLink()
    {
        $user = new User();
        $user->setEmail($email = 'test@test.com');
        
        $subject = 'test';
        $type = SystemEmail::TYPE_REGISTRATION_CANDIDATE;
        $systemEmail = new SystemEmail($user, $subject, $type);
        
        $text = '
            <a href="http://wanawork.local">test</a>
            ';
        $systemEmail->setText($text);
        $this->assertCount(1, $systemEmail->getLinks());
        
        $expectedText = '
            <a href="http://wanawork.local/redirect/'.$systemEmail->getLinks()->first()->getId().'">test</a>
            ';
        
        $this->assertSame($expectedText, $systemEmail->getText());
    }
    
    public function testSetTextWithTwoLinks()
    {    
        $user = new User();
        $user->setEmail($email = 'test@test.com');
        
        $subject = 'test';
        $type = SystemEmail::TYPE_REGISTRATION_CANDIDATE;
        $systemEmail = new SystemEmail($user, $subject, $type);
        
        $text = '
            <a href="http://wanawork.local">test</a>
            <a href="http://wanawork.local">test</a>
            ';
        $systemEmail->setText($text);
        $this->assertCount(2, $systemEmail->getLinks());
        
        $expectedText = '
            <a href="http://wanawork.local/redirect/'.$systemEmail->getLinks()->first()->getId().'">test</a>
            <a href="http://wanawork.local/redirect/'.$systemEmail->getLinks()->last()->getId().'">test</a>
            ';
        
        $this->assertSame($expectedText, $systemEmail->getText());
        $this->assertNotEquals($systemEmail->getLinks()->first()->getId(), $systemEmail->getLinks()->last()->getId());
        
    }
    
    public function testSetTextWithInnerHtml()
    {
        $user = new User();
        $user->setEmail($email = 'test@test.com');
        
        $subject = 'test';
        $type = SystemEmail::TYPE_REGISTRATION_CANDIDATE;
        $systemEmail = new SystemEmail($user, $subject, $type);
        
        $text = '<a href="http://wanawork.local/">
								<img src="http://wanawork.local/images/logoFull.png?20">
							</a>';
        $systemEmail->setText($text);
        $this->assertCount(1, $systemEmail->getLinks());
        
        $expectedText = '<a href="http://wanawork.local/redirect/'.$systemEmail->getLinks()->first()->getId().'">
								<img src="http://wanawork.local/images/logoFull.png?20">
							</a>';
        $this->assertSame($expectedText, $systemEmail->getText());
        
    }

    public function testSetTextWithExistingRedirectLink()
    {
        $user = new User();
        $user->setEmail($email = 'test@test.com');
        
        $subject = 'test';
        $type = SystemEmail::TYPE_REGISTRATION_CANDIDATE;
        $systemEmail = new SystemEmail($user, $subject, $type);
        
        $text = '
            <a href="http://wanawork.local/">test</a>
            ';
        $systemEmail->setText($text);
        $this->assertCount(1, $systemEmail->getLinks());
        
        $expectedText = '
            <a href="http://wanawork.local/redirect/'.$systemEmail->getLinks()->first()->getId().'">test</a>
            ';
        $this->assertSame($expectedText, $systemEmail->getText());
        
        $systemEmail->setText($systemEmail->getText());
        $this->assertCount(1, $systemEmail->getLinks());
    }
    
    public function testSetTextWithLinkWithAttributes()
    {
        $user = new User();
        $user->setEmail($email = 'test@test.com');
        
        $subject = 'test';
        $type = SystemEmail::TYPE_REGISTRATION_CANDIDATE;
        $systemEmail = new SystemEmail($user, $subject, $type);
        
        $text = '
        <a href="http://wanawork.local/test" target="_blank" style="font-family: Arial; font-size: 11px; color: #457fbf;">Unsubscribe</a>
        ';
        $systemEmail->setText($text);
        $this->assertCount(1, $systemEmail->getLinks());
        
        $expectedText = '
        <a href="http://wanawork.local/redirect/'.$systemEmail->getLinks()->first()->getId().'" target="_blank" style="font-family: Arial; font-size: 11px; color: #457fbf;">Unsubscribe</a>
        ';
        $this->assertSame($expectedText, $systemEmail->getText());
        
        $systemEmail->setText($systemEmail->getText());
        $this->assertCount(1, $systemEmail->getLinks());
    }
}
