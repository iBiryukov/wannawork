<?php
namespace Wanawork\UserBundle\Tests\Entity;

use Wanawork\UserBundle\Entity\Sector;
use Symfony\Component\Validator\Validation;
use Wanawork\UserBundle\Entity\SectorJob;

/**
 * Test Sector Job Entity
 * @author Ilya Biryukov <ilya@wanawork.ie>
 */
class SectorJobTest extends WebUnitTestBase
{
    
    /**
     * Validator
     * @var \Symfony\Component\Validator\ValidatorInterface
     */
    private $validator;
    
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    
    public function setUp()
    {
        //$this->validator = Validation::createValidatorBuilder()->enableAnnotationMapping()->getValidator();
        $client = self::createClient();
        $container = $client->getContainer();
        $this->validator = $container->get('validator');
        $this->em = $container->get('doctrine')->getManager();
        $this->em->getConnection()->beginTransaction();
    }
    
    public function tearDown()
    {
        $this->validator = null;
        $this->em->getConnection()->rollBack();
        $this->em->clear();
        $this->em->getConnection()->close();
    }
    
    public function testConstructor()
    {
        $sectorJob = new SectorJob(-1, 'test');
        $sector = new Sector(1, 'test');
        $sectorJob->addSector($sector);
    
        $this->assertSame(-1, $sectorJob->getId());
        $this->assertSame('test', $sectorJob->getName());
        $this->assertEquals(1, $sectorJob->getSectors()->count());
        
        $this->assertTrue(
            is_array($sectorJob->__toArray())
        );
    }
    
    public function testConstructorWithSectors()
    {
        $sector = new Sector(1, 'test');
        $job = new SectorJob(-1, 'test', array($sector));
        
        $this->assertCount(1, $job->getSectors());
        $this->assertSame($sector->getJobs()->count(), $job->getSectors()->count());
        $this->assertContainsOnly($sector, $job->getSectors());
    }
    
    public function testToStringMethodEqualsToName()
    {
        $job = new SectorJob(-1, 'test');
        $this->assertSame($job->getName(), (string)$job);
    
    }
    
    public function testValidationOK()
    {
        $validator = $this->validator;
    
        $sector = new Sector(1, 'test');
        $sectorJob = new SectorJob(-1, 'test');
        $sectorJob->addSector($sector);
    
        $errors = $validator->validate($sectorJob);
        $this->assertTrue(sizeof($errors) === 0, $this->validationErrorsToString($errors));
    }
    
    public function testNullId()
    {
        $validator = $this->validator;
        $sector = new Sector(1, 'test');
        $sectorJob = new SectorJob(null, 'test');
        $sectorJob->addSector($sector);
    
        $errors = $validator->validate($sectorJob);
        $this->assertTrue(sizeof($errors) === 1,
            "Expected 1 error, got: " . sizeof($errors) . "\n" . $this->validationErrorsToString($errors));
        $this->assertSame('id', $errors[0]->getPropertyPath());
    }
    
    
//     public function testStringId()
//     {
//         $validator = $this->validator;
//         $sector = new Sector(1, 'test');
//         $sectorJob = new SectorJob('test', 'test');
//         $sectorJob->addSector($sector);
    
//         $errors = $validator->validate($sectorJob);
//         $this->assertTrue(sizeof($errors) === 1,
//             "Expected 1 error, got: " . sizeof($errors) . "\n" . $this->validationErrorsToString($errors));
//         $this->assertSame('id', $errors[0]->getPropertyPath());
//     }
    
//     public function testEmptyStringId()
//     {
//         $validator = $this->validator;
//         $sector = new Sector(1, 'test');
//         $sectorJob = new SectorJob('', 'test');
//         $sectorJob->addSector($sector);
    
//         $errors = $validator->validate($sectorJob);
//         $this->assertTrue(sizeof($errors) === 1,
//             "Expected 1 error, got: " . sizeof($errors) . "\n" . $this->validationErrorsToString($errors));
//         $this->assertSame('id', $errors[0]->getPropertyPath());
//     }
    
    public function testEmptyName()
    {
        $validator = $this->validator;
        $sector = new Sector(1, 'a');
        $sectorJob = new SectorJob(-1, '');
        $sectorJob->addSector($sector);
    
        $errors = $validator->validate($sectorJob);
        $this->assertTrue(sizeof($errors) === 1,
            "Expected 1 error, got: " . sizeof($errors) . "\n" . $this->validationErrorsToString($errors));
        $this->assertSame('name', $errors[0]->getPropertyPath());
    }
    
    public function testInvalidNameType()
    {
        $validator = $this->validator;
        $sector = new Sector(1, 'a');
        $sectorJob = new SectorJob(-1, 1);
        $sectorJob->addSector($sector);
    
        $errors = $validator->validate($sectorJob);
        $this->assertTrue(sizeof($errors) === 1,
            "Expected 1 error, got: " . sizeof($errors) . "\n" . $this->validationErrorsToString($errors));
        $this->assertSame('name', $errors[0]->getPropertyPath());
    }
    
    public function testLongName()
    {
        $validator = $this->validator;
        $sector = new Sector(1, 'a');
        $sectorJob = new SectorJob(-1, str_repeat('a', 256));
        $sectorJob->addSector($sector);
    
        $errors = $validator->validate($sectorJob);
        $this->assertTrue(sizeof($errors) === 1,
            "Expected 1 error, got: " . sizeof($errors) . "\n" . $this->validationErrorsToString($errors));
        $this->assertSame('name', $errors[0]->getPropertyPath());
    }
    
    public function testEmptySectors()
    {
        $validator = $this->validator;
        $sector = new Sector(1, 'name');
        $sectorJob = new SectorJob(-1, 'test');
    
        $errors = $validator->validate($sectorJob);
        $this->assertTrue(sizeof($errors) === 1,
            "Expected 1 error, got: " . sizeof($errors) . "\n" . $this->validationErrorsToString($errors));
        $this->assertSame('sectors', $errors[0]->getPropertyPath());
    }
    
    public function testReferencialIntegrity()
    {
        $validator = $this->validator;
        $sector = new Sector(1, 'name');
        $sectorJob = new SectorJob(-1, 'test');
    
        $this->assertSame($sectorJob->getSectors()->count(), $sector->getJobs()->count());
        $sectorJob->addSector($sector);
        $this->assertSame($sectorJob->getSectors()->count(), $sector->getJobs()->count());
        $sectorJob->removeSector($sector);
        $this->assertSame($sectorJob->getSectors()->count(), $sector->getJobs()->count());
    }
    
}