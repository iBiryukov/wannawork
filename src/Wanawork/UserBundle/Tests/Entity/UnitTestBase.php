<?php
namespace Wanawork\UserBundle\Tests\Entity;

use Symfony\Component\Validator\ConstraintViolationListInterface;
class UnitTestBase extends \PHPUnit_Framework_TestCase
{
    /**
     * Turn validation error object into string
     * @param ConstraintViolationListInterface $errors
     * 
     * @return string
     */
    public function validationErrorsToString(ConstraintViolationListInterface $errors)
    {
        $errorString = "";
        foreach ($errors as $error) {
            $errorString .= "{$error->getMessage()}\n";            
        }
        return $errorString;
    }
}