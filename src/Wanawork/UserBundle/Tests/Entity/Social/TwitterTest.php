<?php
namespace Wanawork\UserBundle\Tests\Entity\Social;

use Wanawork\UserBundle\Tests\Entity\WebUnitTestBase;
use Wanawork\UserBundle\Entity\Social\Twitter;
use Wanawork\UserBundle\Entity\User;


class TwitterTest extends WebUnitTestBase
{

    public function testConstructor()
    {
        $id = "50";
        $username = "WannaworkIrl";
        $token = new \ZendOAuth\Token\Access();
        $user = new User();
        
        $twitterAccount = new Twitter($id, $username, $token, $user);
        
        $traits = class_uses('\Wanawork\UserBundle\Entity\Social\Twitter');
        $this->assertContains(
            'Gedmo\Timestampable\Traits\TimestampableEntity', $traits, implode('--', $traits)
        );
        
        $this->assertSame($id, $twitterAccount->getId());
        $this->assertSame($username, $twitterAccount->getUsername());
        $this->assertSame($token, $twitterAccount->getToken());
        $this->assertSame($user, $twitterAccount->getUser());
    }
    
    public function testSerializeAccessToken()
    {
        $token = new \ZendOAuth\Token\Access();
        $token->setParam('test1', '1');
        $token->setParam('test2', '2');
        
        $token2 = unserialize(serialize($token));
        $this->assertSame('1', $token2->getParam('test1'));
        $this->assertSame('2', $token2->getParam('test2'));
    }
    
}