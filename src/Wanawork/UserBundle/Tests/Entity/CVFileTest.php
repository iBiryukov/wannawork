<?php
namespace Wanawork\UserBundle\Tests\Entity;

use Wanawork\UserBundle\Tests\Entity\WebUnitTestBase;
use Wanawork\UserBundle\Entity\CVFile;
use Symfony\Component\HttpFoundation\File\File;
use Wanawork\UserBundle\Entity\EmployeeProfile;
use Wanawork\UserBundle\Entity\User;
use Wanawork\MainBundle\Entity\County;
use Wanawork\MainBundle\Entity\NameTitle;

class CVFileTest extends WebUnitTestBase
{
    private $validator;
    
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    
    private $cvPathPDF;
    
    private $cvPathDOCX;
    
    private $cvPathDOC;
    
    private $cvPathODS;
    
    private $cvPathODT;
    
    private $profile;
    
    protected function setUp()
    {
        copy(__DIR__ . '/cv_file.pdf', __DIR__ . '/cv_file_test.pdf');
        $this->cvPathPDF = __DIR__ . '/cv_file_test.pdf';
        
        copy(__DIR__ . '/cv_file.docx', __DIR__ . '/cv_file_test.docx');
        $this->cvPathDOCX = __DIR__ . '/cv_file_test.docx';
        
        copy(__DIR__ . '/cv_file.doc', __DIR__ . '/cv_file_test.doc');
        $this->cvPathDOC = __DIR__ . '/cv_file_test.doc';
        
        copy(__DIR__ . '/cv_file.ods', __DIR__ . '/cv_file_test.ods');
        $this->cvPathODS = __DIR__ . '/cv_file_test.ods';
        
        copy(__DIR__ . '/cv_file.odt', __DIR__ . '/cv_file_test.odt');
        $this->cvPathODT = __DIR__ . '/cv_file_test.odt';
        
        $client = self::createClient();
        $container = $client->getContainer();
        $this->validator = $container->get('validator');
        $this->em = $container->get('doctrine')->getManager();
        $this->em->getConnection()->beginTransaction();
        
        $profile = new EmployeeProfile();
        $profile->setPhoneNumber(123);
        $profile->setAddress('test');
        $profile->setCity('Dublin');
        $profile->setCounty(new County(-1, 'test'));
        $profile->setName('Test');
        $profile->setTitle(new NameTitle(-1, 'test'));
        $profile->setDob(new \DateTime());
        $profile->setUser(new User());
        $this->profile = $profile;
    }
    
    protected function tearDown()
    {
        if (file_exists($this->cvPathPDF)) {
           unlink($this->cvPathPDF); 
        }
        
        if (file_exists($this->cvPathDOC)) {
            unlink($this->cvPathDOC);
        }
        
        if (file_exists($this->cvPathDOCX)) {
            unlink($this->cvPathDOCX);
        }
        
        if (file_exists($this->cvPathODS)) {
            unlink($this->cvPathODS);
        }
        
        if (file_exists($this->cvPathODT)) {
            unlink($this->cvPathODT);
        }
        
        $this->profile = null;
        $this->validator = null;
        $this->em->getConnection()->rollBack();
        $this->em->clear();
        $this->em->getConnection()->close();
    }
    
    public function testEntityOK()
    {
        $cv = new CVFile();
        
        $this->assertInstanceOf('\Wanawork\UserBundle\Entity\CV', $cv);
        $this->assertNull($cv->getId());
    }
    
    public function testUpload()
    {
        $cv = new CVFile();

        // Paths should be empty
        $this->assertNull($cv->getWebPath());
        $this->assertNull($cv->getAbsolutePath());
        
        // Set file
        $cv->setFile($file = new File($this->cvPathPDF));
        $this->assertSame($file, $cv->getFile());
        
        // Paths should be set
        $this->assertNotNull($cv->getWebPath());
        $this->assertNotNull($cv->getAbsolutePath());
        $this->assertFalse(file_exists($cv->getAbsolutePath()));
        
        // Upload the file
        $cv->upload();
        $this->assertTrue(file_exists($cv->getAbsolutePath()));
        $this->assertFalse(file_exists($this->cvPathPDF));
        
        // Remove file
        $cv->removeUpload();
        $this->assertFalse(file_exists($cv->getAbsolutePath()));
    }
    
    public function testValidationWithPDF()
    {
        $cv = new CVFile();
        $cv->setFile($file = new File($this->cvPathPDF));
        $cv->setName($name = 'test');
        $cv->setProfile($this->profile);
        
        $validator = $this->validator;
        $errors = $validator->validate($cv);
        $this->assertCount(0, $errors, $this->validationErrorsToString($errors));
    }
    
    public function testValidationWithDOC()
    {
        $cv = new CVFile();
        $cv->setFile($file = new File($this->cvPathDOC));
        $cv->setName($name = 'test');
        $cv->setProfile($this->profile);
    
        $validator = $this->validator;
        $errors = $validator->validate($cv);
        $this->assertCount(0, $errors, $this->validationErrorsToString($errors));
    }
    
    public function testValidationWithDOCX()
    {
        $cv = new CVFile();
        $cv->setFile($file = new File($this->cvPathDOCX));
        $cv->setName($name = 'test');
        $cv->setProfile($this->profile);
    
        $validator = $this->validator;
        $errors = $validator->validate($cv);
        $this->assertCount(0, $errors, $this->validationErrorsToString($errors));
    }
    
    public function testValidationWithODT()
    {
        $cv = new CVFile();
        $cv->setFile($file = new File($this->cvPathODT));
        $cv->setName($name = 'test');
        $cv->setProfile($this->profile);
    
        $validator = $this->validator;
        $errors = $validator->validate($cv);
        $this->assertCount(0, $errors, $this->validationErrorsToString($errors));
    }
    
    public function testValidationWithODS()
    {
        $cv = new CVFile();
        $cv->setFile($file = new File($this->cvPathODS));
        $cv->setName($name = 'test');
        $cv->setProfile($this->profile);
    
        $validator = $this->validator;
        $errors = $validator->validate($cv);
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('file', $errors[0]->getPropertyPath());
    }
    
    public function testEmptyUpload()
    {
        $cv = new CVFile();
        $cv->upload();
        $this->assertNull($cv->getAbsolutePath());
        $this->assertNull($cv->getWebPath());
    }
    
}