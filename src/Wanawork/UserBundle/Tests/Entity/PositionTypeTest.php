<?php
namespace Wanawork\UserBundle\Tests\Entity;

use Wanawork\UserBundle\Entity\PositionType;
class PositionTypeTest extends WebUnitTestBase
{
    
    private $validator;
    
    /**
    *
    * @var \Doctrine\ORM\EntityManager
    */
    private $em;
    
    
    protected function setUp()
    {
        $client = self::createClient();
        $container = $client->getContainer();
        $this->validator = $container->get('validator');
        $this->em = $container->get('doctrine')->getManager();
        $this->em->getConnection()->beginTransaction();
    }
    
    protected function tearDown()
    {
        $this->validator = null;
        $this->em->getConnection()->rollBack();
        $this->em->clear();
        $this->em->getConnection()->close();
    }
    
    
    public function testEntityOk()
    {
        $position = new PositionType($id = -1, $name = 'test', $order = 1);
        
        $this->assertSame($id, $position->getId());
        $this->assertSame('test', $position->getName());
        $this->assertSame(1, $position->getOrder());
        $this->assertSame($position->getName(), (string)$position);
    }
    
    public function testEmptyName()
    {
        $position = new PositionType(-1, $name = '', $order = 1);
        
        $validator = $this->validator;
        $errors = $validator->validate($position);
        
        $this->assertCount(1, $errors);
        $this->assertSame('name', $errors[0]->getPropertyPath());
    }
    
    public function testNameIsTooLong()
    {
        $position = new PositionType(-1, $name = str_repeat('a', 256), $order = 1);
    
        $validator = $this->validator;
        $errors = $validator->validate($position);
    
        $this->assertCount(1, $errors);
        $this->assertSame('name', $errors[0]->getPropertyPath());
    }
    
    public function testEmptyOrder()
    {
        $position = new PositionType(-1, $name = 'a', $order = null);
        
        $validator = $this->validator;
        $errors = $validator->validate($position);
        
        $this->assertCount(1, $errors);
        $this->assertSame('order', $errors[0]->getPropertyPath());
    }
}