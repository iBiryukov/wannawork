<?php
namespace Wanawork\UserBundle\Tests\Entity;

use Wanawork\UserBundle\Entity\Ad;
use Doctrine\Common\Collections\ArrayCollection;
use Wanawork\UserBundle\Entity\PositionType;
use Wanawork\UserBundle\Entity\CV;
use Wanawork\MainBundle\Entity\County;
use Wanawork\UserBundle\Entity\SectorJob;
use Wanawork\UserBundle\Entity\Sector;
use Wanawork\UserBundle\Entity\SectorExperience;
use Wanawork\UserBundle\Entity\User;
use Wanawork\UserBundle\Entity\EmployeeProfile;
use Wanawork\UserBundle\Entity\EmployerProfile;
use Wanawork\UserBundle\Entity\CvRequest;
use Wanawork\MainBundle\Entity\Language;
use Wanawork\UserBundle\Entity\Qualification;
use Wanawork\UserBundle\Entity\AdView;
use Wanawork\UserBundle\Entity\CVForm;
use Wanawork\UserBundle\Entity\JobSpec;
use Wanawork\UserBundle\Entity\Billing\AdOrder;
use Wanawork\UserBundle\Tests\Entity\Mocks\EmployerProfileMock;
use Wanawork\UserBundle\Entity\Role;
use Wanawork\UserBundle\Entity\Billing\Order;

class AdTest extends WebUnitTestBase
{
    private $validator;
    
    /**
     * Valid advert for validation testing
     * @var \Wanawork\UserBundle\Entity\Ad
     */
    private $ad;
    
    protected function setUp()
    {
        $client = self::createClient();
        $this->validator = $client->getContainer()->get('validator');
        
        $this->ad = $ad = new Ad();
        $ad->setHeadline('test');
        $ad->setPitch('test');
        $ad->setPositions([new PositionType(-1, 'test', 1)]);
        $ad->setLocations([new County(-1, 'test')]);
        $ad->setProfession(new SectorJob($id = -1, $name = 'test'));
        $ad->setExperience(new SectorExperience($id = -1, $name = 'test'));
        $ad->setLanguages([new Language($id = -1, $name = 'test')]);
        $ad->setEducationLevel(new Qualification($id = -1, $name = 'test', $isPopular = false));
        
        $user = new User();
        $profile = new EmployeeProfile();
        $profile->setUser($user);
        $user->setEmail('test@wannawork.ie');
        $user->setName('test');
        $profile->setName($user->getName());
        $ad->setProfile($profile);
    }
    
    public function validateEntity()
    {
        $ad = clone $this->ad;
        $validator = $this->validator;
        
        $errors = $validator->validate($ad, 'step1');
        $this->assertCount(0, $errors, $this->validationErrorsToString($errors));
    }
    
    public function testEntityOk()
    {
        $userEmployee = new User();
        $profileEmployee = new EmployeeProfile();
        $userEmployee->setEmployeeProfile($profileEmployee);
        
        $userEmployer = new User();
        $profileEmployer = new EmployerProfileMock();
        $profileEmployer->setId(-1);
        $userEmployer->addEmployerProfile($profileEmployer, $isAdmin = true);
        
        $ad = new Ad();
        $this->assertNull($ad->getId());
        
        // Published flag
        $this->assertFalse($ad->isPublished());
        
        $ad->markPublished($flag = true);
        $this->assertTrue($ad->isPublished());
        
        $ad->markPublished($flag = false);
        $this->assertFalse($ad->isPublished());
        
        // Profile
        $ad->setProfile($profileEmployee);
        $this->assertSame($profileEmployee, $ad->getProfile());
        
        // Headline
        $ad->setHeadline('headline');
        $this->assertSame('headline', $ad->getHeadline());
        
        // pitch 
        $ad->setPitch('pitch');
        $this->assertSame('pitch', $ad->getPitch());
        
        // positions
        $this->assertTrue($ad->getPositions() instanceof ArrayCollection);
        $this->assertCount(0, $ad->getPositions());
        
        $position = new PositionType(-1, $name = 'test', $order = 1);
        $ad->addPosition($position);
        $this->assertCount(1, $ad->getPositions());
        $this->assertContainsOnly($position, $ad->getPositions());
        $ad->removePosition($position);
        $this->assertCount(0, $ad->getPositions());
        
        $ad->addPosition(new PositionType(-1, $name = 'test2', $order = 2));
        $ad->setPositions(array($position));
        $this->assertCount(1, $ad->getPositions());
        $this->assertContainsOnly($position, $ad->getPositions());
        $ad->addPosition($position);
        $this->assertCount(1, $ad->getPositions());
        
        // CV
        $cv = new CVForm();
        $ad->setCv($cv);
        $this->assertSame($cv, $ad->getCv());
        
        // Locations
        $this->assertTrue($ad->getLocations() instanceof ArrayCollection);
        $this->assertCount(0, $ad->getLocations());
        
        $county = new County(-1, 'Carlow');
        $ad->addLocation($county);
        $this->assertCount(1, $ad->getLocations());
        $this->assertContainsOnly($county, $ad->getLocations());
        $ad->removeLocation($county);
        $this->assertCount(0, $ad->getLocations());
        
        $ad->addLocation(new County(-1, 'Dublin'));
        $ad->setLocations(array($county));
        $this->assertCount(1, $ad->getLocations());
        $this->assertContainsOnly($county, $ad->getLocations());
        $ad->addLocation($county);
        $this->assertCount(1, $ad->getLocations());
        
        // Post date
        
        
        // Expiry Date
        $expiryDate = new \DateTime('tomorrow');
        $ad->setExpiryDate($expiryDate);
        $this->assertEquals($expiryDate, $ad->getExpiryDate());
        $this->assertNotSame($expiryDate, $ad->getExpiryDate());
        $this->assertFalse($ad->isExpired());
        $ad->setExpiryDate(new \DateTime('yesterday'));
        $this->assertTrue($ad->isExpired());
        
        // Profession
        $profession = new SectorJob($id = 1, $name = 'test');
        $ad->setProfession($profession);
        $this->assertSame($profession, $ad->getProfession());
        
        // Industries
        $industry = new Sector($id = 1, $name = 'test');
        $this->assertTrue($ad->getIndustries() instanceof ArrayCollection);
        $this->assertCount(0, $ad->getIndustries());
        
        $ad->addIndustry($industry);
        $this->assertCount(1, $ad->getIndustries());
        $this->assertContainsOnly($industry, $ad->getIndustries());
        $ad->removeIndustry($industry);
        $this->assertCount(0, $ad->getIndustries());
        
        $ad->addIndustry(new Sector($id = 2, $name = 'test2'));
        $ad->setIndustries(array($industry));
        $this->assertCount(1, $ad->getIndustries());
        $this->assertContainsOnly($industry, $ad->getIndustries());
        $ad->addIndustry($industry);
        $this->assertCount(1, $ad->getIndustries());
        
        
        // Sector experience
        $sectorExp = new SectorExperience(-1, $name = 'test');
        $ad->setExperience($sectorExp);
        $this->assertSame($sectorExp, $ad->getExperience());
        
        // Access Requests
        $this->assertTrue($ad->getAccessRequests() instanceof ArrayCollection);
        $this->assertCount(0, $ad->getAccessRequests());
        
        $job = null;
        $accessRequest = new CvRequest($userEmployer, $ad, $profileEmployer, $job, $message = 'test');
        
        
        $ad->addAccessRequest($accessRequest);
        $this->assertCount(1, $ad->getAccessRequests());
        $this->assertCount(1, $ad->getTodayPendingAccessRequests());
        $this->assertTrue($accessRequest instanceof CvRequest);
        
        $ad->addAccessRequest($accessRequest);
        $this->assertCount(1, $ad->getAccessRequests());
        
        $ad->removeAccessRequest($accessRequest);
        $this->assertCount(0, $ad->getAccessRequests());
        
        // Badge
        $this->assertNull($ad->getBadge());
        $ad->setBadge($badge = 'testbadge');
        
        $this->assertSame($badge, $ad->getBadge());
        
        // Languages
        $this->assertTrue($ad->getLanguages() instanceof ArrayCollection);
        $this->assertCount(0, $ad->getLanguages());
        
        $language = new Language(-1, 'English');
        
        $ad->addLanguage($language);
        $this->assertCount(1, $ad->getLanguages());
        
        $ad->addLanguage($language);
        $this->assertCount(1, $ad->getLanguages());
        
        $ad->removeLanguage($language);
        $this->assertCount(0, $ad->getLanguages());
        
        // Education Level
        $educationLevel = new Qualification(-1, $name = 'test', $isPopular = false);
        $ad->setEducationLevel($educationLevel);
        $this->assertSame($educationLevel, $ad->getEducationLevel());
        
        // Views 
        $this->assertTrue($ad->getViews() instanceof ArrayCollection);
        $this->assertCount(0, $ad->getViews());
        
        $ad->addView($type = AdView::VIEW_FULL, $userEmployee);
        $this->assertCount(1, $ad->getViews());
        $this->assertCount(1, $ad->getViewsByType(AdView::VIEW_FULL));
        
        $ad->addView(AdView::VIEW_PDF, $userEmployee);
        
        $this->assertCount(1, $ad->getPdfViews());
    }
    
    public function testBadgeVisibility()
    {
        $ad = $this->getMock('Wanawork\UserBundle\Entity\Ad', array('isPremium'));
        $ad->expects($this->once())->method('isPremium')->will($this->returnValue(FALSE));
        $ad->canDisplayBadge();
    }
    
    public function testRunMonths()
    {
        $ad = new Ad();
        $ad->setCreatedAt(new \DateTime('2013-09-01'));
        $ad->setExpiryDate(new \DateTime('2013-10-25'));
        $months = $ad->getRunMonths();
        $this->assertCount(2, $months);
        $this->assertArrayHasKey('9-2013', $months);
        $this->assertArrayHasKey('10-2013', $months);
        
        $ad->setCreatedAt(new \DateTime('2012-12-01'));
        $ad->setExpiryDate(new \DateTime('2013-02-25'));
        $months = $ad->getRunMonths();
        $this->assertCount(3, $months, print_r($months, true));
        $this->assertArrayHasKey('12-2012', $months);
        $this->assertArrayHasKey('1-2013', $months);
        $this->assertArrayHasKey('2-2013', $months);
    }
    
    public function testIsPremium()
    {
        $ad = new Ad();
        $bumps = 5;
        $amount = '5';
        $vatRate = 23;
        $plan = Ad::PLAN_PREMIUM;
        $duration = 60;
        $adOrder = new AdOrder($ad, $amount, $vatRate, $plan, $duration, $bumps);
        $adOrder->setStatus(AdOrder::STATUS_PAID);
        
        $runFrom = new \DateTime();
        $adOrder->setRunFrom($runFrom);
        $this->assertTrue($ad->isPremium());
        
        $runFrom = new \DateTime();
        $runFrom->modify('yesterday');
        $adOrder->setRunFrom($runFrom);
        $this->assertTrue($ad->isPremium());
        
        $runFrom = new \DateTime();
        $runFrom->modify('tomorrow');
        $adOrder->setRunFrom($runFrom);
        $this->assertFalse($ad->isPremium());
        
        $ad2 = new Ad();
        $this->assertFalse($ad2->isPremium());
    }
    
    
    public function testBumps()
    {
        $ad = new Ad();
        $this->assertNull($ad->bump());
        $this->assertTrue(is_array($ad->getBumps()));
        $this->assertCount(0, $ad->getBumps());
        $this->assertNull($ad->getLastBumpDate());
        $this->assertSame(0, $ad->getCountBumpsAllowed());
        $this->assertSame(0, $ad->getCountBumpsUsed());
        $this->assertSame(0, $ad->getCountBumpsLeft());
        $this->assertFalse($ad->canBump());
        
        // Set bumps
        $ad->setCountBumpsAlowed($count = 2);
        $this->assertSame($count, $ad->getCountBumpsAllowed());
        $this->assertTrue(is_array($ad->getBumps()));
        $this->assertCount(0, $ad->getBumps());
        $this->assertNull($ad->getLastBumpDate());
        $this->assertSame(0, $ad->getCountBumpsUsed());
        $this->assertSame($count, $ad->getCountBumpsLeft());
        $this->assertTrue($ad->canBump());
        
        // Do 1 bump
        $ad->bump();
        --$count;
        $this->assertSame(2, $ad->getCountBumpsAllowed());
        $this->assertTrue(is_array($ad->getBumps()));
        $this->assertCount(1, $ad->getBumps());
        $this->assertTrue($ad->getLastBumpDate() instanceof \DateTime);
        $this->assertSame(1, $ad->getCountBumpsUsed());
        $this->assertSame(1, $ad->getCountBumpsLeft());
        $this->assertTrue($ad->canBump());
        
        // Do 1 bump
        $ad->bump();
        --$count;
        $this->assertSame(2, $ad->getCountBumpsAllowed());
        $this->assertTrue(is_array($ad->getBumps()));
        $this->assertCount(2, $ad->getBumps());
        $this->assertTrue($ad->getLastBumpDate() instanceof \DateTime);
        $this->assertSame(2, $ad->getCountBumpsUsed());
        $this->assertSame(0, $ad->getCountBumpsLeft());
        $this->assertFalse($ad->canBump());
    }
    
    public function testBumpMethodUsesCanBumpMethod()
    {
        $ad = $this->getMock('Wanawork\UserBundle\Entity\Ad', array('canBump'));
        $ad->expects($this->once())->method('canBump');
        $ad->bump();
    }
    
    public function testHasCvRequestByEmployer()
    {
        $ad = new Ad();
        $this->assertCount(0, $ad->getAccessRequests());
        
        $profile = new EmployerProfile();
        $cvRequest = new CvRequest();
        $cvRequest->setAd($ad);
        $cvRequest->setEmployerProfile($profile);
        $cvRequest->setMessage('test');

        $this->assertTrue($ad->hasCvRequestFromEmployer($profile));
    }
    
    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage User cannot be null for this type of view
     */
    public function testFullAdViewRequiresAUser()
    {
        $user = new User();
        $ad = new Ad();
        $ad->addView(AdView::VIEW_FULL, $user);

        $this->assertCount(1, $ad->getViews());
        $ad->addView(AdView::VIEW_FULL, null);
    }
    
    public function testFullAndPDFViewsAreNotRecordedFromAdmin()
    {
        $user = new User();
        $user->addRole(new Role('Admin', Role::ADMIN));
        
        $ad = new Ad();
        $view = $ad->addView(AdView::VIEW_FULL, $user);
        $this->assertNull($view);
        $this->assertCount(0, $ad->getViews());
        
        $view = $ad->addView(AdView::VIEW_PDF, $user);
        $this->assertNull($view);
        $this->assertCount(0, $ad->getViews());
    }
    
    public function testInSearchViewDoesntNeedAUser()
    {
        $ad = new Ad();
        $view = $ad->addView(AdView::VIEW_IN_SEARCH);
        $this->assertTrue($view instanceof AdView);
        $this->assertCount(1, $ad->getViews());
    }
    
    public function testHeadlineRequired()
    {
        $ad = clone $this->ad;
        $validator = $this->validator;
        $ad->setHeadline('');

        $errors = $validator->validate($ad, 'step1');
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('headline', $errors[0]->getPropertyPath());
    }
    
    public function testHeadlineIsTooLong()
    {
        $adHeadlineLength = 70;
        $ad = clone $this->ad;
        $validator = $this->validator;

        // 1 above
        $ad->setHeadline(str_repeat('a', $adHeadlineLength + 1));
        $errors = $validator->validate($ad, 'step1');
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('headline', $errors[0]->getPropertyPath());
        
        // exact
        $ad->setHeadline(str_repeat('a', $adHeadlineLength));
        $errors = $validator->validate($ad, 'step1');
        $this->assertCount(0, $errors, $this->validationErrorsToString($errors));
    }
    
    public function testPitchIsRequired()
    {
        $ad = clone $this->ad;
        $validator = $this->validator;
        $ad->setPitch('');
        
        $errors = $validator->validate($ad, 'step1');
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('pitch', $errors[0]->getPropertyPath());
    }
    
    public function testPitchIsTooLong()
    {
        $adHeadlineLength = 240;
        $ad = clone $this->ad;
        $validator = $this->validator;
    
        // 1 above
        $ad->setPitch(str_repeat('a', $adHeadlineLength + 1));
        $errors = $validator->validate($ad, 'step1');
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('pitch', $errors[0]->getPropertyPath());
    
        // exact
        $ad->setPitch(str_repeat('a', $adHeadlineLength));
        $errors = $validator->validate($ad, 'step1');
        $this->assertCount(0, $errors, $this->validationErrorsToString($errors));
    }
    
    public function testAtLeastOnePositionIsRequired()
    {
        $ad = clone $this->ad;
        $validator = $this->validator;
        $ad->setPositions([]);
        
        $errors = $validator->validate($ad, 'step1');
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('positions', $errors[0]->getPropertyPath());
    }
    
    public function testCVIsNotRequired()
    {
        $ad = clone $this->ad;
        $validator = $this->validator;
        $ad->setCv(null);
        
        $errors = $validator->validate($ad, 'step1');
        $this->assertCount(0, $errors, $this->validationErrorsToString($errors));
    }
    
    public function testAtLeastOneLocationIsRequired()
    {
        $ad = clone $this->ad;
        $validator = $this->validator;
        $ad->setLocations([]);
    
        $errors = $validator->validate($ad, 'step1');
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('locations', $errors[0]->getPropertyPath());
    }
    
    public function testExpiryDateIsNotRequired()
    {
        $ad = clone $this->ad;
        $validator = $this->validator;
        $ad->setExpiryDate(null);
        
        $errors = $validator->validate($ad, 'step1');
        $this->assertCount(0, $errors, $this->validationErrorsToString($errors));
    }

    public function testProfessionIsRequired()
    {
        $ad = clone $this->ad;
        $validator = $this->validator;
        
        $ad->setProfession(null);

        $errors = $validator->validate($ad, 'step1');
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('profession', $errors[0]->getPropertyPath());
    }
    
    public function testIndustriesAreNotRequired()
    {
        $ad = clone $this->ad;
        $validator = $this->validator;
        $ad->setIndustries([]);
        
        $errors = $validator->validate($ad, 'step1');
        $this->assertCount(0, $errors, $this->validationErrorsToString($errors));
    }
    
    public function testExperienceIsRequired()
    {
        $ad = clone $this->ad;
        $validator = $this->validator;
        $ad->setExperience(null);
        
        $errors = $validator->validate($ad, 'step1');
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('experience', $errors[0]->getPropertyPath());
    }
    
    public function testBadgeIsNotRequired()
    {
        $ad = clone $this->ad;
        $validator = $this->validator;
        $ad->setBadge(null);
        
        $errors = $validator->validate($ad, 'step1');
        $this->assertCount(0, $errors, $this->validationErrorsToString($errors));
    }
    
    public function testBadgeMaxLength()
    {
        $badgeMaxLength = 100;
        $ad = clone $this->ad;
        $validator = $this->validator;
    
        // 1 above
        $ad->setBadge(str_repeat('a', $badgeMaxLength + 1));
        $errors = $validator->validate($ad, 'step1');
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('badge', $errors[0]->getPropertyPath());
    
        // exact
        $ad->setBadge(str_repeat('a', $badgeMaxLength));
        $errors = $validator->validate($ad, 'step1');
        $this->assertCount(0, $errors, $this->validationErrorsToString($errors));
    }
    
    public function testAtLeastOneLanguageIsRequired()
    {
        $ad = clone $this->ad;
        $validator = $this->validator;
        $ad->setLanguages([]);
    
        $errors = $validator->validate($ad, 'step1');
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('languages', $errors[0]->getPropertyPath());
    }
    
    public function testEducationLevelIsRequired()
    {
        $ad = clone $this->ad;
        $validator = $this->validator;
        $ad->setEducationLevel(null);
        
        $errors = $validator->validate($ad, 'step1');
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('educationLevel', $errors[0]->getPropertyPath());
    }
    
    public function testProfileIsRequired()
    {
        $ad = clone $this->ad;
        $validator = $this->validator;
        $ad->setProfile(null);
        
        $errors = $validator->validate($ad, 'step1');
        $this->assertCount(1, $errors, $this->validationErrorsToString($errors));
        $this->assertSame('profile', $errors[0]->getPropertyPath());
    }
    
    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Duration must be a number. Given: 'a'
     */
    public function testPublishFreeAdRequiresIntegerDuration()
    {
        $ad = clone $this->ad;
        $ad->publishStandard($duration = 'a', $bumps = 3);
    }
    
    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Duration must be greater than 0. Given: '0'
     */
    public function testPublishFreeAdRequiresDurationGreaterThanZero()
    {
        $ad = clone $this->ad;
        $ad->publishStandard($duration = 0, $bumps = 3);
    }
    
    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Bumps must be a number. Given: 'a'
     */
    public function testPublishFreeAdRequiresIntegerBumps()
    {
        $ad = clone $this->ad;
        $ad->publishStandard($duration = 1, $bumps = 'a');
    }
    
    public function testPublishFreeAd()
    {
        $ad = clone $this->ad;
        $ad->publishStandard($duration = 60, $bumps = 3);
        
        // Test advert 
        $this->assertTrue($ad->isPublished());
        $this->assertFalse($ad->isPremium());
        $this->assertFalse($ad->isExpired());
        $this->assertSame($duration, $ad->getExpiryDateInDays());
        $this->assertSame($bumps, $ad->getCountBumpsLeft());
        
        // test order
        $this->assertCount(1, $ad->getOrders(AdOrder::STATUS_PAID));
        $order = $ad->getOrders(AdOrder::STATUS_PAID)->first();
        $this->assertTrue($order instanceof AdOrder);
        $this->assertSame($ad, $order->getAd());
        $this->assertSame(0, $order->getAmount());
        $this->assertSame($bumps, $order->getBumps());
        $this->assertSame($duration, $order->getDuration());
        $this->assertSame(date('Y-m-d h:i'), $order->getPayDate()->format('Y-m-d h:i'));
        $this->assertSame($ad->getProfile(), $order->getPayer());
        $this->assertSame(date('Y-m-d h:i'), $order->getRunFrom()->format('Y-m-d h:i'));
        $this->assertSame(0, $order->getVatRate());
        $this->assertCount(0, $order->getPayments());
        $this->assertCount(0, $order->getTransactions());
        $this->assertEquals($ad->getExpiryDate(), $order->getExpiry());
        
        // Test Order Items
        $this->assertCount(2, $order->getItems());
        $item1 = $order->getItems()->first();
        $this->assertSame("Standard subscription for $duration days", $item1->getName());
        
        $item2 = $order->getItems()->last();
        $this->assertSame("Free bumps included", $item2->getName());
        $this->assertSame($bumps, $item2->getQuantity());
        
        
        // Publish again
        $order = $ad->publishStandard($duration2 = 30, $bumps2 = 1);
        
        // Test advert
        $this->assertTrue($ad->isPublished());
        $this->assertFalse($ad->isPremium());
        $this->assertFalse($ad->isExpired());
        $this->assertSame($duration + $duration2, $ad->getExpiryDateInDays());
        $this->assertSame($bumps + $bumps2, $ad->getCountBumpsLeft());
        
        // test order
        $this->assertCount(2, $ad->getOrders(AdOrder::STATUS_PAID));
        $this->assertTrue($order instanceof AdOrder);
        $this->assertSame($ad, $order->getAd());
        $this->assertSame(0, $order->getAmount());
        $this->assertSame($bumps2, $order->getBumps());
        $this->assertSame($duration2, $order->getDuration());
        $this->assertSame(date('Y-m-d h:i'), $order->getPayDate()->format('Y-m-d h:i'));
        $this->assertSame($ad->getProfile(), $order->getPayer());
        $this->assertSame(
            $ad->getOrders(AdOrder::STATUS_PAID)->first()->getExpiry()->format('Y-m-d h:i'), 
            $order->getRunFrom()->format('Y-m-d h:i')
        );
        $this->assertSame(0, $order->getVatRate());
        $this->assertCount(0, $order->getPayments());
        $this->assertCount(0, $order->getTransactions());
        $this->assertEquals($ad->getExpiryDate(), $order->getExpiry());
        
    }
    
    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Cannot publish an advert with unpaid order
     */
    public function testPublishPremiumOrderMustBePaid()
    {
        $ad = clone $this->ad;
        
        $amount = 0;
        $vatRate = 0;
        $plan = Ad::PLAN_PREMIUM;
        $duration = 10;
        $bumps = 1;
        $order = new AdOrder($ad, $amount, $vatRate, $plan, $duration, $bumps);
        
        $ad->publishPremium($order);
    }
    
    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Order must be for a premium plan
     */
    public function testPublishPremiumOrderHavePremiumPlan()
    {
        $ad = clone $this->ad;
    
        $amount = 0;
        $vatRate = 0;
        $plan = Ad::PLAN_STANDARD;
        $duration = 10;
        $bumps = 1;
        $order = new AdOrder($ad, $amount, $vatRate, $plan, $duration, $bumps);
        $order->setStatus(Order::STATUS_PAID);
        $ad->publishPremium($order);
    }
    
    public function testPublishPremium()
    {
        $ad = clone $this->ad;
        
        $amount = 407;
        $vatRate = 23;
        $plan = Ad::PLAN_PREMIUM;
        $duration = 10;
        $bumps = 10;
        $order = new AdOrder($ad, $amount, $vatRate, $plan, $duration, $bumps);
        $order->setStatus(Order::STATUS_PAID);
        $ad->publishPremium($order);
        
        $this->assertTrue($ad->isPremium());
        $this->assertFalse($ad->isExpired());
        $this->assertTrue($ad->isPublished());
        $this->assertSame($bumps, $ad->getCountBumpsAllowed());
        
        $this->assertTrue($order->getRunFrom() instanceof \DateTime);
        $this->assertSame(date('Y-m-d h:i'), $order->getRunFrom()->format('Y-m-d h:i'));
        $this->assertEquals($ad->getExpiryDate(), $order->getExpiry());
    }
    
    public function testPublishPremiumAfterPublishingStandard()
    {
        $ad = clone $this->ad;
        
        $ad->publishStandard($duration = 10, $bumps = 1);
        
        $amount = 407;
        $vatRate = 23;
        $plan = Ad::PLAN_PREMIUM;
        $duration = 10;
        $bumps = 10;
        $order = new AdOrder($ad, $amount, $vatRate, $plan, $duration, $bumps);
        $order->setStatus(Order::STATUS_PAID);
        $ad->publishPremium($order);
        
        $this->assertSame(date('Y-m-d h:i'), $order->getRunFrom()->format('Y-m-d h:i'));
        
        $this->assertFalse($ad->isExpired());
        $this->assertTrue($ad->isPublished());
        $this->assertTrue($ad->isPremium());
        
    }
}