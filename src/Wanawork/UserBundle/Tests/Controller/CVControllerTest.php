<?php

namespace Wanawork\UserBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Wanawork\UserBundle\Entity\CVFile;
use Wanawork\UserBundle\Entity\User;
use Wanawork\UserBundle\Entity\CVForm;
use Symfony\Component\Routing\RouterInterface;
use Wanawork\UserBundle\Entity\EmployeeProfile;

class CVControllerTest extends WebTestCase
{
    /**
     *
     * @var \Doctrine\ORM\EntityManager
     */
    private $doctrine;
    
    protected function setUp()
    {
        $client = self::createClient();
        $client->injectConnection();
        $this->doctrine = $client->getContainer()->get('doctrine')->getManager();
    }
    
    public function testCreateActionWithInvalidFileName()
    {
        $client = static::createClient();
        $container = $client->getContainer();
        $router = $container->get('router');
        $doctrine = $this->doctrine;
        
        // Not Logged in
        $client->request('POST', $router->generate('cv_file_create'));
        $this->assertSame(403, $client->getResponse()->getStatusCode());
         
        $client->connect($container->getParameter('candidate_email'), $container->getParameter('candidate_password'));
         
        $fileName = str_repeat('a', 201);
        $file = new UploadedFile(__DIR__ . '/../sample-cv.pdf', $fileName, $mimeType = 'application/pdf', $size = filesize(__DIR__ . '/../sample-cv.pdf'), $error = null, $test = true);
        $this->assertTrue($file->isValid());
         
        $files = [
            'cv-file' => $file,
        ];
        $client->request('POST', $router->generate('cv_file_create'), [], $files);
         
        $response = $client->getResponse();
        $this->assertSame(422, $response->getStatusCode());
    }
    
    public function testCreateActionWithNoEmployeeProfile()
    {
        $client = self::createClient();
        $container = $client->getContainer();
        $router = $container->get('router');
        $doctrine = $this->doctrine;
        
        $crawler = $client->request('GET', $router->generate('wanawork_register'));
        
        $form = $crawler->selectButton('submit-candidate')->form();
        $form['candidate[name]'] = 'Ilya Biryukov';
        $form['candidate[email]'] = $email = 'candidate_test5@wanawork.ie';
        $form['candidate[password]'] = $password = '123456';
        $client->submit($form);
        
        copy(__DIR__ . '/../sample-cv.pdf', __DIR__ . '/../sample-cv1.pdf');
        $file = new UploadedFile(__DIR__ . '/../sample-cv1.pdf', 'sample-cv.pdf', $mimeType = 'application/pdf', $size = filesize(__DIR__ . '/../sample-cv.pdf'), $error = null, $test = true);
        $this->assertTrue($file->isValid());
         
        $files = [
            'cv-file' => $file,
        ];
        $client->request('POST', $router->generate('cv_file_create'), [], $files);
         
        $response = $client->getResponse();
        $this->assertSame(201, $response->getStatusCode(), $response->getContent());
        $this->assertSame('application/json', $response->headers->get('Content-Type'));
        $returnData = json_decode($response->getContent(), $assoc = true);
        $this->assertTrue(array_key_exists('cv', $returnData), print_r($returnData, true));
        $this->assertTrue(is_numeric($returnData['cv']));
         
        $cvFileRepository = $doctrine->getRepository('WanaworkUserBundle:CVFile');
        $cvId = $returnData['cv'];
        $cv = $cvFileRepository->find($cvId);
        $this->assertTrue($cv instanceof CVFile);
        $this->assertSame('sample-cv.pdf', $cv->getName());
        $this->assertTrue($cv->getProfile() instanceof EmployeeProfile);
         
        $security = $container->get('security.context');
        $security->isGranted('OPERATOR', $cv);
         
        $cv->removeUpload();
    }
    
	public function testCreateAction()
	{
	    $client = static::createClient();
	    $container = $client->getContainer();
	    $router = $container->get('router');
	    $doctrine = $this->doctrine;

	    // Not Logged in
 	    $client->request('POST', $router->generate('cv_file_create'));
 	    $this->assertSame(403, $client->getResponse()->getStatusCode());
	    
	    $client->connect($container->getParameter('candidate_email'), $container->getParameter('candidate_password'));
	    
	    copy(__DIR__ . '/../sample-cv.pdf', __DIR__ . '/../sample-cv1.pdf');
	    $file = new UploadedFile(__DIR__ . '/../sample-cv1.pdf', 'sample-cv.pdf', $mimeType = 'application/pdf', $size = filesize(__DIR__ . '/../sample-cv.pdf'), $error = null, $test = true);
	    $this->assertTrue($file->isValid());
	    
	    $files = [
	       'cv-file' => $file,
        ];
	    $client->request('POST', $router->generate('cv_file_create'), [], $files);
	    
	    $response = $client->getResponse();
	    $this->assertSame(201, $response->getStatusCode());
	    $this->assertSame('application/json', $response->headers->get('Content-Type'));
	    $returnData = json_decode($response->getContent(), $assoc = true);
	    $this->assertTrue(array_key_exists('cv', $returnData), print_r($returnData, true));
	    $this->assertTrue(is_numeric($returnData['cv']));
	    
	    $cvFileRepository = $doctrine->getRepository('WanaworkUserBundle:CVFile');
	    $cvId = $returnData['cv'];
	    $cv = $cvFileRepository->find($cvId);
	    $this->assertTrue($cv instanceof CVFile);
	    $this->assertSame('sample-cv.pdf', $cv->getName());
	    
	    $security = $container->get('security.context');
	    $security->isGranted('OPERATOR', $cv);
	    
	    $cv->removeUpload();
	}
	
	public function testCreateActionWithNoFile()
	{
	    $client = static::createClient();
	    $container = $client->getContainer();
	    $router = $container->get('router');
	    $client->connect($container->getParameter('candidate_email'), $container->getParameter('candidate_password'));
	    
	    $client->request('POST', $router->generate('cv_file_create'));
	    $this->assertSame(400, $client->getResponse()->getStatusCode());
	    $returnData = json_decode($client->getResponse()->getContent(), $assoc = true);
	    $this->assertArrayHasKey('errors', $returnData);
	    $this->assertTrue(sizeof($returnData['errors']) > 0);
	}
	
	public function testCreateActionWithUploadError()
	{
	    $client = static::createClient();
	    $container = $client->getContainer();
	    $router = $container->get('router');
	    $client->connect($container->getParameter('candidate_email'), $container->getParameter('candidate_password'));
	    
	    
	    $file = new UploadedFile('test', 'test', $mimeType = null, $size = null, $error = UPLOAD_ERR_PARTIAL, $test = true);
	    $client->request('POST', $router->generate('cv_file_create'), [], ['cv-file' => $file]);
	    
	    $this->assertSame(422, $client->getResponse()->getStatusCode());
	    $returnData = json_decode($client->getResponse()->getContent(), $assoc = true);
	    $this->assertArrayHasKey('errors', $returnData);
	    $this->assertTrue(sizeof($returnData['errors']) > 0);
	}
	
	public function testDownloadCVPermissions()
	{
	    $client = static::createClient();
	    $container = $client->getContainer();
	    $router = $container->get('router');
	    $doctrine = $this->doctrine;
	    $cvRepository = $doctrine->getRepository('WanaworkUserBundle:CVForm');
	    $cv = $cvRepository->findRandom();
	    $this->assertTrue($cv instanceof CVForm);
	     
	    $client->request('GET', $router->generate('cv_download', ['id' => $cv->getId()]));
	    $this->assertSame(302, $client->getResponse()->getStatusCode());
	    $this->assertTrue(
	        $client->getResponse()->isRedirect(
	            $router->generate('login', [], RouterInterface::ABSOLUTE_URL)
	        )
	    );
	    
	    $client->connect($container->getParameter('employer_email'), $container->getParameter('employer_password'));
	    $client->request('GET', $router->generate('cv_download', ['id' => $cv->getId()]));
	    $this->assertSame(403, $client->getResponse()->getStatusCode());
	    
	}
	
	public function testDownloadCVActionWithPDFFile()
	{
	    $client = static::createClient();
	    $container = $client->getContainer();
	    $router = $container->get('router');
	    $doctrine = $this->doctrine;
          
	    $client->connect($container->getParameter('candidate_email'), $container->getParameter('candidate_password'));
	     
	    copy(__DIR__ . '/../sample-cv.pdf', __DIR__ . '/../sample-cv1.pdf');
	    $file = new UploadedFile(__DIR__ . '/../sample-cv1.pdf', 'sample-cv.pdf', $mimeType = 'application/pdf', $size = filesize(__DIR__ . '/../sample-cv.pdf'), $error = null, $test = true);
	    $this->assertTrue($file->isValid());
	     
	    $files = [
	    'cv-file' => $file,
	    ];
	    $client->request('POST', $router->generate('cv_file_create'), [], $files);
	    $response = $client->getResponse();
	    $json = json_decode($response->getContent(), true);
	    
	    $client->request('GET', $router->generate('cv_download', ['id' => $json['cv']]));
	    $response = $client->getResponse();
	    $this->assertSame(200, $response->getStatusCode());
	    $this->assertSame('application/pdf', $response->headers->get('Content-Type'));
	    
	    // Remove file
	    $repository = $doctrine->getRepository('WanaworkUserBundle:CV');
	    $cv = $repository->find($json['cv']);
	    $this->assertTrue($cv instanceof CVFile);
	    $cv->removeUpload();
	    
	    $client->request('GET', $router->generate('cv_download', ['id' => $json['cv']]));
	    $this->assertSame(404, $client->getResponse()->getStatusCode());
	}
	
	public function testDownloadcVActionWithFormBuilder()
	{
	    $client = static::createClient();
	    $container = $client->getContainer();
	    $router = $container->get('router');
	    $doctrine = $this->doctrine;
	    $cvRepository = $doctrine->getRepository('WanaworkUserBundle:CVForm');
	    $cv = $cvRepository->findRandom();
	    $this->assertTrue($cv instanceof CVForm);
	    
	    $client->connect($cv->getProfile()->getUser()->getEmail(), 123456);
	    $client->request('GET', $router->generate('cv_download', ['id' => $cv->getId()]));
	    $response = $client->getResponse();
	    
	    $this->assertSame(200, $response->getStatusCode());
	}
}