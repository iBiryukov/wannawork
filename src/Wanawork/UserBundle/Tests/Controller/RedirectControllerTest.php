<?php
namespace Wanawork\UserBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Wanawork\UserBundle\Entity\SystemEmail;

class RedirectControllerTest extends WebTestCase
{
    /**
     *
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    
    private $client;
    
    /**
     * 
     * @var \Wanawork\UserBundle\Entity\SystemEmailLink
     */
    private $link;
    
    protected function setUp()
    {
        $this->client = $client = self::createClient();
        $container = $client->getContainer();
    
        $this->em = $container->get('doctrine')->getManager();
        $this->em->getConnection()->beginTransaction();
        
        $email = new SystemEmail($user = null, $subject = 'test', $type = SystemEmail::TYPE_BLANK, 'test@wanawork.ie');
        $email->setText('<a href="http://wanawork.local/">test link</a>');
        $this->link = $email->getLinks()->first();
        $this->em->persist($email);
        $this->em->flush();
        
    }
    
    protected function tearDown()
    {
        $this->em->clear();
        $this->em->getConnection()->rollBack();
        $this->em->getConnection()->close();
        
        $this->em = null;
        $this->client = null;
        $this->link = null;
    }
    
    
    public function testOkRedirect()
    {
        $this->assertCount(0, $this->link->getClicks());
        $client = $this->client;
        $client->request('GET', '/redirect/' . $this->link->getId());
        
        $this->assertSame(302, $client->getResponse()->getStatusCode());
        $this->assertTrue($client->getResponse()->isRedirect('http://wanawork.local/'));
        
        $this->assertCount(1, $this->link->getClicks());
        
    }
}