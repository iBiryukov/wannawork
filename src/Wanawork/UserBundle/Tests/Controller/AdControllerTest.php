<?php

namespace Wanawork\UserBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Wanawork\UserBundle\Tests\Entity\WebUnitTestBase;
use Wanawork\UserBundle\Entity\Ad;
use Wanawork\UserBundle\Entity\CVFile;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Routing\RouterInterface;
use Wanawork\UserBundle\Entity\CVForm;

class AdControllerTest extends WebUnitTestBase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $doctrine;
    
    protected function setUp()
    {
        $client = self::createClient();
        $client->injectConnection();
        $this->doctrine = $client->getContainer()->get('doctrine')->getManager();
    }
    
    public function testAdPageRedirectsToRegistrationIfNotLoggedIn()
    {
        $client = self::createClient();
        $container = $client->getContainer();
        $router = $container->get('router');
        
        $crawler = $client->request('GET', $router->generate('ad_new'));
        
        $this->assertTrue(
            $client->getResponse()->isRedirect(
                $router->generate('wanawork_register')
            )
        );
    }
    
    public function testNewAction()
    {
        $client = self::createClient();
        $container = $client->getContainer();
        $router = $container->get('router');
        $doctrine = $this->doctrine;
        $client->connect($container->getParameter('candidate_email'), $container->getParameter('candidate_password'));
        $crawler = $client->request('GET', $router->generate('ad_new'));
        
        $professionRepository = $doctrine->getRepository('WanaworkUserBundle:SectorJob');
        $profession = $professionRepository->findRandom();
        $industryRepository = $doctrine->getRepository('WanaworkUserBundle:Sector');
        $industry = $industryRepository->findRandom()[0];
        $experienceRepository = $doctrine->getRepository('WanaworkUserBundle:SectorExperience');
        $experience = $experienceRepository->findRandom();
        $positionTypeRepository = $doctrine->getRepository('WanaworkUserBundle:PositionType');
        $position = $positionTypeRepository->findRandom()[0];
        $qualificationRepository = $doctrine->getRepository('WanaworkUserBundle:Qualification');
        $qualification = $qualificationRepository->findRandom();
        $countyRepository = $doctrine->getRepository('WanaworkMainBundle:County');
        $county = $countyRepository->findRandom()[0];
        $languageRepository = $doctrine->getRepository('WanaworkMainBundle:Language');
        $language = $languageRepository->findRandom()[0];
        
        
        $formData['wanawork_userbundle_adtype[headline]'] = 'Headline';
        $formData['wanawork_userbundle_adtype[pitch]'] = 'Pitch';
        $formData['wanawork_userbundle_adtype[profession]'] = $profession->getId();
        $formData['wanawork_userbundle_adtype[industries]'] = $industry->getId();
        $formData['wanawork_userbundle_adtype[experience]'] = $experience->getId();
        $formData['wanawork_userbundle_adtype[positions]'] = $position->getId();
        $formData['wanawork_userbundle_adtype[educationLevel]'] = $qualification->getId();
        $formData['wanawork_userbundle_adtype[locations]'] = $county->getId();
        $formData['wanawork_userbundle_adtype[languages]'] = $language->getId();
        $formData['wanawork_userbundle_adtype[cv]'] = null;
        //$formData['wanawork_userbundle_adtype[_token]'] = $crawler->filter('#wanawork_userbundle_adtype__token')->attr('value');
        
//         $form = $crawler->selectButton('Publish')->form();
//         $crawler = $client->submit($form, $formData);
        
// //         $client->request(
// //             'POST', 
// //             $uri = $router->generate('ad_new'), 
// //             $parameters = $formData, 
// //             $files = array(), 
// //             $server = array(), 
// //             $content = null, 
// //             $changeHistory = true
// //         );
        
//         $this->assertSame(302, $client->getResponse()->getStatusCode());
        
//         $dql = "SELECT ad FROM WanaworkUserBundle:Ad ad WHERE ad.id=
//                     (SELECT MAX(ad2.id) FROM WanaworkUserBundle:Ad ad2)";
//         $ad = $doctrine->createQuery($dql)->getOneOrNullResult();
//         $this->assertTrue($ad instanceof Ad);
        
        
//         $url = $router->generate('ad_published', ['id' => $ad->getId()]);
//         $this->assertTrue($client->getResponse()->isRedirect($url));
    }
    
    public function testCandidateProfessionListAction()
    {
        $client = self::createClient();
        $container = $client->getContainer();
        $router = $container->get('router');
        
        $crawler = $client->request('GET', $router->generate('ad_candidates_profession_list'));
        $this->assertSame(200, $client->getResponse()->getStatusCode());
    }
    
    public function testRecentCandidatesAction()
    {
        $client = self::createClient();
        $container = $client->getContainer();
        $router = $container->get('router');
        
        $crawler = $client->request('GET', $router->generate('ad_candidates_recent'));
        $this->assertSame(200, $client->getResponse()->getStatusCode());
    }
    
    public function testCandidatesByProfessionAction()
    {
        $client = self::createClient();
        $container = $client->getContainer();
        $router = $container->get('router');
        $professionRepository = $this->doctrine->getRepository('WanaworkUserBundle:SectorJob');
        $profession = $professionRepository->findRandom();

        $crawler = $client->request('GET',
            $router->generate('ad_candidates_by_profession', ['profession_slug' => $profession->getSlug()]));
        $this->assertSame(200, $client->getResponse()->getStatusCode());
    }

    public function testIndexAction()
    {
        $client = self::createClient();
        $container = $client->getContainer();
        $router = $container->get('router');
        
        $client->connect($container->getParameter('admin_email'), $container->getParameter('admin_password'));
        
        $crawler = $client->request('GET', $router->generate('ad_list'));
        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $this->assertGreaterThan(0, $crawler->filter('div.ad-box')->count());
    }
    
    public function testIndexActionRequiresAdmin()
    {
        $client = self::createClient();
        $container = $client->getContainer();
        $router = $container->get('router');
        $client->connect($container->getParameter('employer_email'), $container->getParameter('employer_password'));
        $crawler = $client->request('GET', $router->generate('ad_list'));
        $this->assertSame(403, $client->getResponse()->getStatusCode());
    }
    
    public function testShowAction()
    {
        $client = self::createClient();
        $container = $client->getContainer();
        $router = $container->get('router');
        $doctrine = $this->doctrine;
        
        $adRepository = $doctrine->getRepository('WanaworkUserBundle:Ad');
        $ad = $adRepository->findRandom();
        $this->assertTrue($ad instanceof Ad);
        
        // Public version of the ad
        $client->request('GET', $router->generate('ad_show', ['slug' => $ad->getSlug()]));
        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $this->assertTrue($client->getResponse()->headers->hasCacheControlDirective('public'));
        $this->assertNotNull($client->getResponse()->getEtag());
        
        // View Permission given
        $client->connect($container->getParameter('admin_email'), $container->getParameter('admin_password'));
        $crawler = $client->request('GET', $router->generate('ad_show', ['slug' => $ad->getSlug()]));
        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $this->assertTrue($client->getResponse()->headers->hasCacheControlDirective('private'));
        $this->assertNotNull($client->getResponse()->getEtag());
        
        // Click on PDF
         $crawler = $client->click($crawler->selectLink('Download CV')->link());
         $this->assertSame(200, $client->getResponse()->getStatusCode());
         $this->assertSame('application/pdf', $client->getResponse()->headers->get('Content-Type'));
        
//         // Create File CV
//         copy(__DIR__ . '/../sample-cv.pdf', __DIR__ . '/../sample-cv1.pdf');
//         $cvFile = new CVFile();
//         $cvFile->setFile(new File(__DIR__ . '/../sample-cv1.pdf'));
//         $cvFile->setProfile($ad->getProfile());
//         $cvFile->setName('Test File CV');
//         $ad->setCv($cvFile);
//         $doctrine->persist($cvFile);
//         $doctrine->flush();
        
//         $crawler = $client->request('GET', $router->generate('ad_show', ['slug' => $ad->getSlug()]));
//         $crawler = $client->click($crawler->selectLink('Download PDF')->link());
//         $this->assertSame(200, $client->getResponse()->getStatusCode());
//         $this->assertTrue($client->getResponse()->headers->hasCacheControlDirective('private'));
//         $this->assertNotNull($client->getResponse()->getEtag());
//         $this->assertSame('application/pdf', $client->getResponse()->headers->get('Content-Type'));
//        
//        $cvFile->removeUpload();
    }
    
    public function testDownloadCVFromCVBuilder()
    {
        $client = self::createClient();
        $container = $client->getContainer();
        $router = $container->get('router');
        $doctrine = $this->doctrine;
        
        $adRepository = $doctrine->getRepository('WanaworkUserBundle:Ad');
        $ad = $adRepository->findRandom();
        $this->assertTrue($ad instanceof Ad);
        
        $client->connect($container->getParameter('admin_email'), $container->getParameter('admin_password'));
        
        $cv = $ad->getCv();
        $this->assertTrue($cv instanceof CVForm);

        $crawler = $client->request('GET', $router->generate('ad_download_cv', ['slug' => $ad->getSlug()]));
        
        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $this->assertSame('application/pdf', $client->getResponse()->headers->get('Content-Type'));
    }
    
    public function testDownloadCVFilePDF()
    {
        $client = self::createClient();
        $container = $client->getContainer();
        $router = $container->get('router');
        $doctrine = $this->doctrine;
        
        $adRepository = $doctrine->getRepository('WanaworkUserBundle:Ad');
        $ad = $adRepository->findRandom();
        $this->assertTrue($ad instanceof Ad);
        
        $client->connect($container->getParameter('admin_email'), $container->getParameter('admin_password'));
        
        copy(__DIR__ . '/../Entity/cv_file.pdf', __DIR__ . '/cv_file.pdf');
        $file = __DIR__ . '/cv_file.pdf';
        
        $cv = new CVFile();
        $cv->setFile(new File($file));
        $cv->setName('Test PDF');
        $cv->setProfile($ad->getProfile());
        
        $ad->setCv($cv);
        
        $doctrine->persist($cv);
        $doctrine->flush();
        
        $crawler = $client->request('GET', $router->generate('ad_download_cv', ['slug' => $ad->getSlug()]));
        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $this->assertSame('application/pdf', $client->getResponse()->headers->get('Content-Type'));
        
        $cv->removeUpload();
    }
    
    public function testDownloadCVFileDoc()
    {
        $client = self::createClient();
        $container = $client->getContainer();
        $router = $container->get('router');
        $doctrine = $this->doctrine;
    
        $adRepository = $doctrine->getRepository('WanaworkUserBundle:Ad');
        $ad = $adRepository->findRandom();
        $this->assertTrue($ad instanceof Ad);
    
        $client->connect($container->getParameter('admin_email'), $container->getParameter('admin_password'));
    
        copy(__DIR__ . '/../Entity/cv_file.doc', __DIR__ . '/cv_file.doc');
        $file = __DIR__ . '/cv_file.doc';
    
        $cv = new CVFile();
        $cv->setFile(new File($file));
        $cv->setName('Test PDF');
        $cv->setProfile($ad->getProfile());
    
        $ad->setCv($cv);
    
        $doctrine->persist($cv);
        $doctrine->flush();
    
        $crawler = $client->request('GET', $router->generate('ad_download_cv', ['slug' => $ad->getSlug()]));
        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $this->assertSame('application/msword', $client->getResponse()->headers->get('Content-Type'));
    
        $cv->removeUpload();
    }
    
    public function testDownloadCVFileDocX()
    {
        $client = self::createClient();
        $container = $client->getContainer();
        $router = $container->get('router');
        $doctrine = $this->doctrine;
    
        $adRepository = $doctrine->getRepository('WanaworkUserBundle:Ad');
        $ad = $adRepository->findRandom();
        $this->assertTrue($ad instanceof Ad);
    
        $client->connect($container->getParameter('admin_email'), $container->getParameter('admin_password'));
    
        copy(__DIR__ . '/../Entity/cv_file.docx', __DIR__ . '/cv_file.docx');
        $file = __DIR__ . '/cv_file.docx';
    
        $cv = new CVFile();
        $cv->setFile(new File($file));
        $cv->setName('Test PDF');
        $cv->setProfile($ad->getProfile());
    
        $ad->setCv($cv);
    
        $doctrine->persist($cv);
        $doctrine->flush();
    
        $crawler = $client->request('GET', $router->generate('ad_download_cv', ['slug' => $ad->getSlug()]));
        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $this->assertSame('application/vnd.openxmlformats-officedocument.wordprocessingml.document', $client->getResponse()->headers->get('Content-Type'));
    
        $cv->removeUpload();
    }
    
    public function testDownloadCVFileODT()
    {
        $client = self::createClient();
        $container = $client->getContainer();
        $router = $container->get('router');
        $doctrine = $this->doctrine;
    
        $adRepository = $doctrine->getRepository('WanaworkUserBundle:Ad');
        $ad = $adRepository->findRandom();
        $this->assertTrue($ad instanceof Ad);
    
        $client->connect($container->getParameter('admin_email'), $container->getParameter('admin_password'));
    
        copy(__DIR__ . '/../Entity/cv_file.odt', __DIR__ . '/cv_file.odt');
        $file = __DIR__ . '/cv_file.odt';
    
        $cv = new CVFile();
        $cv->setFile(new File($file));
        $cv->setName('Test PDF');
        $cv->setProfile($ad->getProfile());
    
        $ad->setCv($cv);
    
        $doctrine->persist($cv);
        $doctrine->flush();
    
        $crawler = $client->request('GET', $router->generate('ad_download_cv', ['slug' => $ad->getSlug()]));
        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $this->assertSame('application/vnd.oasis.opendocument.text', $client->getResponse()->headers->get('Content-Type'));
    
        $cv->removeUpload();
    }
    
    public function testDownloadCVRequiresViewAccessToAd()
    {
        $client = self::createClient();
        $container = $client->getContainer();
        $router = $container->get('router');
        $doctrine = $this->doctrine;
        
        $adRepository = $doctrine->getRepository('WanaworkUserBundle:Ad');
        $ad = $adRepository->findRandom();
        $this->assertTrue($ad instanceof Ad);
        
        $client->connect($container->getParameter('candidate_email'), $container->getParameter('candidate_password'));
        $crawler = $client->request('GET', $router->generate('ad_download_cv', ['slug' => $ad->getSlug()]));
        $this->assertSame(403, $client->getResponse()->getStatusCode());
    }
    
    public function testDownloadCV404IfNoCvPresent()
    {
        $client = self::createClient();
        $container = $client->getContainer();
        $router = $container->get('router');
        $doctrine = $this->doctrine;
        
        
        $userRepository = $doctrine->getRepository('WanaworkUserBundle:User');
        $user = $userRepository->findOneByEmail($container->getParameter('candidate_email'));
        
        $ad = new Ad();
        $ad->setHeadline('test');
        $ad->setPitch('test');
        $ad->setProfile($user->getEmployeeProfile());
        $doctrine->persist($ad);
        $doctrine->flush();
        
        $client->connect($container->getParameter('admin_email'), $container->getParameter('admin_password'));
        $crawler = $client->request('GET', $router->generate('ad_download_cv', ['slug' => $ad->getSlug()]));
        $this->assertSame(404, $client->getResponse()->getStatusCode());
    }
}
