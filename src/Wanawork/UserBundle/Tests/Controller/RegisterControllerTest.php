<?php
namespace Wanawork\UserBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Wanawork\UserBundle\Entity\User;
use Wanawork\UserBundle\Entity\Role;

class RegisterControllerTest extends WebTestCase
{
    /**
     * 
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    
    private $client;
    
    private $emailsToDelete = array();
    
    protected function setUp()
    {
        $this->client = $client = self::createClient();
        $container = $client->getContainer();
        $client->injectConnection();
        
        $this->em = $container->get('doctrine')->getManager();
    }
    
    protected function tearDown()
    {
        
    }

    public function testRegistrationCanidate()
    {
        $client = $this->client;
        $crawler = $client->request('GET', '/register');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        
        $this->assertEquals(1, $crawler->filter('#candidate_name')->count());
        $this->assertEquals(1, $crawler->filter('#candidate_email')->count());
        $this->assertEquals(1, $crawler->filter('#candidate_password')->count());
        
        $this->assertEquals(1, $crawler->filter('#employer_name')->count());
        $this->assertEquals(1, $crawler->filter('#employer_email')->count());
        $this->assertEquals(1, $crawler->filter('#employer_password')->count());
        
        $form = $crawler->selectButton('submit-candidate')->form();
        $form['candidate[name]'] = 'Ilya Biryukov';
        $form['candidate[email]'] = $email = 'candidate_test5@wanawork.ie';
        $form['candidate[password]'] = '123456';
        
        $client->submit($form);
        //$client->followRedirect();
        $this->assertTrue($client->getResponse()->isRedirect('/register/success/candidate'));
        
        $em = $this->em;
        $em->clear();
        
        $userRepository = $em->getRepository('WanaworkUserBundle:User');
        $user = $userRepository->findOneByEmail($email);
        
        $this->assertTrue($user instanceof User);
        $this->assertTrue($user->hasRoleName(Role::EMPLOYEE));
        
        // ensure top menu works
        $crawler = $client->followRedirect();
        $aNodes = $crawler->filter('#user-menu')->filter('a')->links();
        foreach ($aNodes as $node) {
            $client->click($node);
            $this->assertSame(200, $client->getResponse()->getStatusCode());
        }
        
    }
    
    
    public function testRegistrationEmployer()
    {
        $client = $this->client;
        $crawler = $client->request('GET', '/register');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    
        $this->assertEquals(1, $crawler->filter('#candidate_name')->count());
        $this->assertEquals(1, $crawler->filter('#candidate_email')->count());
        $this->assertEquals(1, $crawler->filter('#candidate_password')->count());
    
    
        $this->assertEquals(1, $crawler->filter('#employer_name')->count());
        $this->assertEquals(1, $crawler->filter('#employer_email')->count());
        $this->assertEquals(1, $crawler->filter('#employer_password')->count());
    
        $form = $crawler->selectButton('submit-employer')->form();
        $form['employer[name]'] = 'Ilya Biryukov';
        $form['employer[email]'] = $email = 'employer_test5@wanawork.ie';
        $form['employer[password]'] = '123456';
    
        $client->submit($form);
    
        $this->assertTrue($client->getResponse()->isRedirect('/register/success/employer'), $client->getResponse()->getStatusCode());
    
        $em = $this->em;
        $em->clear();
        $userRepository = $em->getRepository('WanaworkUserBundle:User');
        $user = $userRepository->findOneByEmail($email);
    
        $this->assertTrue($user instanceof User);
        $this->assertTrue($user->hasRoleName(Role::EMPLOYER));
        
        // ensure top menu works
        $crawler = $client->followRedirect();
        $aNodes = $crawler->filter('#user-menu')->filter('a')->links();
        foreach ($aNodes as $node) {
            $client->click($node);
            if ($node->getNode()->nodeValue === 'Profile') {
                $this->assertSame(302, $client->getResponse()->getStatusCode());
                $this->assertTrue($client->getResponse()->isRedirect('/company/new'));
            } else {
                $this->assertSame(200, $client->getResponse()->getStatusCode());
            }
            
        }
    }
}

