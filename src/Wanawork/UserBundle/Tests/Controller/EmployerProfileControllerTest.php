<?php

namespace Wanawork\UserBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Routing\RouterInterface;
use Wanawork\UserBundle\Entity\User;
use Wanawork\UserBundle\Entity\EmployerProfile;
use Wanawork\UserBundle\Entity\Billing\VerificationOrder;
use Wanawork\UserBundle\Entity\Billing\Order;

class EmployerProfileControllerTest extends WebTestCase
{
    /**
     * 
     * @var \Doctrine\ORM\EntityManager
     */
    private $doctrine;
    
    protected function setUp()
    {
        $client = self::createClient();
        $client->injectConnection();
        $this->doctrine = $client->getContainer()->get('doctrine')->getManager();
    }
    
    
    public function testIndexActionWorksForAdmin()
    {
        $client = static::createClient();
        $container = $client->getContainer();
        $router = $container->get('router');
        
        
        // not logged in -> 302
        $crawler = $client->request('GET', $router->generate('company'));
        $this->assertSame(302, $client->getResponse()->getStatusCode());
        $this->assertTrue(
            $client->getResponse()->isRedirect(
                $router->generate('login', array(), RouterInterface::ABSOLUTE_URL)
            )
        );
        
        // Login now
        $crawler = $client->followRedirect();
        $form = $crawler->selectButton('login-button')->form();
	    $form['_username'] = $container->getParameter('admin_email');
	    $form['_password'] = $container->getParameter('admin_password');
	    $client->submit($form);
	    
	    // Redirected to company url?
	    $this->assertTrue(
            $client->getResponse()->isRedirect(
                $router->generate('company', array(), RouterInterface::ABSOLUTE_URL)
            ),
	        $client->getResponse()->headers->get('Location')
        );
	    
	    $crawler = $client->followRedirect();
	    $this->assertSame(200, $client->getResponse()->getStatusCode());
	    
	    $this->assertSame(1, $crawler->filter('table.default-table')->count());
    }
    
	public function testIndexAction403ForNonAdmin()
	{
	    $client = static::createClient();
	    $router = $client->getContainer()->get('router');
	    $container = $client->getContainer();
	     
	    $crawler = $client->request('GET', $router->generate('login'));
	    $form = $crawler->selectButton('login-button')->form();
	    $form['_username'] = $container->getParameter('candidate_email');
	    $form['_password'] = $container->getParameter('candidate_password');
	     
	    $client->submit($form);
	     
	    $crawler = $client->request('GET', $router->generate('company'));
	    $this->assertSame(403, $client->getResponse()->getStatusCode());
	}
	
	public function testNewAndEditActions()
	{
	    $client = static::createClient();
	    $router = $client->getContainer()->get('router');
	    $container = $client->getContainer();
	    
	    // not logged in -> 302
	    $crawler = $client->request('GET', $router->generate('company_new'));
	    $this->assertSame(302, $client->getResponse()->getStatusCode());
	    $this->assertTrue(
	        $client->getResponse()->isRedirect(
	            $router->generate('login', array(), RouterInterface::ABSOLUTE_URL)
	        )
	    );
	    
	    // Login now
	    $crawler = $client->followRedirect();
	    $form = $crawler->selectButton('login-button')->form();
	    $form['_username'] = $container->getParameter('employer_email');
	    $form['_password'] = $container->getParameter('employer_password');
	    $client->submit($form);
	     
	    // Redirected back?
	    $this->assertTrue(
	        $client->getResponse()->isRedirect(
	            $router->generate('company_new', array(), RouterInterface::ABSOLUTE_URL)
	        )
	    );
	    
	    $crawler = $client->followRedirect();
	    
	    // Create new company
        $form = $crawler->selectButton('Save')->form();
        $companyName = "test company";
        $form->setValues(array(
        	'wanawork_userbundle_employerprofiletype[companyName]'          =>      $companyName,
            'wanawork_userbundle_employerprofiletype[address]'              =>      'Test Address',
            'wanawork_userbundle_employerprofiletype[city]'                 =>      'Test City',
            'wanawork_userbundle_employerprofiletype[county]'               =>      '63',  // Antrim
            'wanawork_userbundle_employerprofiletype[phoneNumber]'          =>      123456,
            'wanawork_userbundle_employerprofiletype[email]'                =>      'test@test.com',
            'wanawork_userbundle_employerprofiletype[contactName]'          =>      'Test Name',
            'wanawork_userbundle_employerprofiletype[industries]'           =>      '1200',
            'wanawork_userbundle_employerprofiletype[professions]'          =>      '3',
        ));
        $crawler = $client->submit($form);
        
        // Go to company page
        $this->assertTrue(
            $client->getResponse()->isRedirect(
                $router->generate('company_show', array('slug' => str_replace(' ', '-', $companyName)))
            )
        );
        $this->assertSame(302, $client->getResponse()->getStatusCode());
        
        $crawler = $client->followRedirect();
        $this->assertSame(200, $client->getResponse()->getStatusCode());    

        // Edit the company 
        $crawler = $client->click($crawler->selectLink('Edit Profile')->link());
        
        $form = $crawler->selectButton('Save')->form();
        $client->submit($form);
        
        // Redirected back?
        $this->assertTrue(
            $client->getResponse()->isRedirect(
                $router->generate('company_show', array('slug' => str_replace(' ', '-', $companyName)))
            )
        );
         
        $crawler = $client->followRedirect();
        $this->assertSame(200, $client->getResponse()->getStatusCode());
	}
	
	public function testEditAction403ForNonOwner()
	{
	    $client = static::createClient();
	    $router = $client->getContainer()->get('router');
	    $container = $client->getContainer();
	    
	    $crawler = $client->request('GET', '/register');
	    $form = $crawler->selectButton('submit-employer')->form();
	    $form['employer[name]'] = 'Ilya Biryukov';
	    $form['employer[email]'] = $email = "employer_test".time()."@wanawork.ie";
	    $form['employer[password]'] = $password = '123456';
	    $client->submit($form);

	    $employerRepository = $this->doctrine->getRepository('WanaworkUserBundle:EmployerProfile');
	    $employers = $employerRepository->findRandom();
	    if (sizeof($employers) !== 1) {
	        throw new \Exception("Cannot find an employer");
	    }
	    $employer = $employers[0];
	     
	    $url = $router->generate('company_edit', array(
	        'slug' => $employer->getSlug(),
	    ));
	    $crawler = $client->request('GET', $url);
	    
	    $this->assertSame(403, $client->getResponse()->getStatusCode());
	}
	
	public function testVerificationAction()
	{
	    $client = static::createClient();
	    $router = $client->getContainer()->get('router');
	    $container = $client->getContainer();
	    
	    $crawler = $client->request('GET', $router->generate('login'));
	    $form = $crawler->selectButton('login-button')->form();
	    $form['_username'] = $container->getParameter('employer_email');
	    $form['_password'] = $container->getParameter('employer_password');
	    $client->submit($form);
	    
	    $crawler = $client->followRedirect();
	    $crawler = $client->followRedirect();
	    
	    // Click on profile
	    $crawler = $client->click($crawler->selectLink('Profile')->link());
	    $this->assertSame(200, $client->getResponse()->getStatusCode());
	    
	    // Click on 'verification page'
	    $crawler = $client->click($crawler->selectLink('Company Not Verified')->link());
	    $this->assertSame(200, $client->getResponse()->getStatusCode());
	    
	    // Click on become verified
// 	    $crawler = $client->click($crawler->selectLink('Pay & Become Verified')->link());
// 	    $this->assertSame(302, $client->getResponse()->getStatusCode());
	}
	
	public function testVerificationActionIfAlreadyVerified()
	{
	    $client = static::createClient();
	    $router = $client->getContainer()->get('router');
	    $container = $client->getContainer();
	    
	    $email = $container->getParameter('employer_email');
	    $password = $container->getParameter('employer_password');
	    
	    $userRepository = $this->doctrine->getRepository('WanaworkUserBundle:User');
	    $user = $userRepository->findOneByEmail($email);
	    $this->assertTrue($user instanceof User);
	    $this->assertGreaterThan(0, sizeof($user->getEmployerProfiles()));
	    $employer = $user->getEmployerProfiles()->first();
	    $this->assertTrue($employer instanceof EmployerProfile);
	    
        // Login
        $crawler = $client->request('GET', $router->generate('login'));
        $form = $crawler->selectButton('login-button')->form();
        $form['_username'] = $email;
        $form['_password'] = $password;
        $crawler = $client->submit($form);
        $client->followRedirect();
        
        $employer->setIsVerified($isVerified = true);
        $this->doctrine->flush();
        
        // Try verification page
        $crawler = $client->request('GET', $router->generate('employer_verification_request', [
    	  'slug' => $employer->getSlug(),
        ]));
        
        // Already verified?
        $this->assertSame(302, $client->getResponse()->getStatusCode());
        
        $this->assertTrue(
            $client->getResponse()->isRedirect(
                $router->generate('my_employer_profile')
            )
        );
	        
	}
	
	public function testVerificationIfVerificationPending()
	{
	    $client = static::createClient();
	    $router = $client->getContainer()->get('router');
	    $container = $client->getContainer();
	     
	    $email = $container->getParameter('employer_email');
	    $password = $container->getParameter('employer_password');
	     
	    $userRepository = $this->doctrine->getRepository('WanaworkUserBundle:User');
	    $user = $userRepository->findOneByEmail($email);
	    $this->assertTrue($user instanceof User);
	    $this->assertGreaterThan(0, sizeof($user->getEmployerProfiles()));
	    $employer = $user->getEmployerProfiles()->first();
	    $this->assertTrue($employer instanceof EmployerProfile);
	     
	    // Login
	    $crawler = $client->request('GET', $router->generate('login'));
	    $form = $crawler->selectButton('login-button')->form();
	    $form['_username'] = $email;
	    $form['_password'] = $password;
	    $crawler = $client->submit($form);
	    $client->followRedirect();
	    
	    $amount = 200;
	    $vatRate = 23;
	    $order = new VerificationOrder($employer, $amount, $vatRate);
	    $order->setStatus(Order::STATUS_PAID);
	    $this->doctrine->persist($order);
	    
	    $this->doctrine->flush();
	    
	    // Try verification page
	    $crawler = $client->request('GET', $router->generate('employer_verification_request', [
	        'slug' => $employer->getSlug(),
	    ]));
	    
	    // Verification pending?
	    $this->assertSame(302, $client->getResponse()->getStatusCode());
	    
	    $this->assertTrue(
	        $client->getResponse()->isRedirect(
	            $router->generate('employer_verification_request_pending', ['slug' => $employer->getSlug()])
	        )
	    );
	}
	
	public function testVerificationActionRequiresAuthorisation()
	{
	    $client = static::createClient();
	    $router = $client->getContainer()->get('router');
	    $container = $client->getContainer();
	    
	    $employerRepository = $this->doctrine->getRepository('WanaworkUserBundle:EmployerProfile');
	    $employers = $employerRepository->findRandom();
	    if (sizeof($employers) !== 1) {
	        throw new \Exception("Cannot find an employer");
	    }
	    $employer = $employers[0];
	    
	    $crawler = $client->request('GET', '/register');
	    $form = $crawler->selectButton('submit-employer')->form();
	    $form['employer[name]'] = 'Ilya Biryukov';
	    $form['employer[email]'] = $email = "employer_test".time()."@wanawork.ie";
	    $form['employer[password]'] = $password = '123456';
	    $client->submit($form);
	    
	    $crawler = $client->request('GET', $router->generate('employer_verification_request', array(
	    	'slug' => $employer->getSlug(),
	    )));
	    $this->assertSame(403, $client->getResponse()->getStatusCode());
	}
	
	public function testMarkVerifiedAndRevokeVerificationAction()
	{
	    $client = static::createClient();
	    $container = $client->getContainer();
	    $router = $client->getContainer()->get('router');
	    $client->connect($container->getParameter('admin_email'), $container->getParameter('admin_password'));
	    
	    $employerRepository = $this->doctrine->getRepository('WanaworkUserBundle:EmployerProfile');
	    $employers = $employerRepository->findRandom();
	    if (sizeof($employers) !== 1) {
	        throw new \Exception("Cannot find an employer");
	    }
	    $employer = $employers[0];
	    $this->assertTrue($employer instanceof EmployerProfile);

	    // Verify the client
	    $client->request('POST', $router->generate('company_mark_verified', ['slug'=>$employer->getSlug()]));
	    $this->assertTrue(
	        $client->getResponse()->isRedirect($router->generate('company'))
	    );

	    $this->doctrine->refresh($employer);
	    $this->assertTrue($employer->isVerified());
	    $this->assertNotNull($employer->getVerifiedBy());
	    $this->assertSame($container->getParameter('admin_email'), $employer->getVerifiedBy()->getEmail());
	    
	    // Revoke verification
	    $client->request('POST', $router->generate('company_mark_unverified', ['slug'=>$employer->getSlug()]));
	    $this->assertTrue(
	        $client->getResponse()->isRedirect($router->generate('company'))
	    );
	    
	    $this->doctrine->refresh($employer);
	    $this->assertFalse($employer->isVerified());
	}
	
	public function testAdminVerificationActionRequiresValidUser()
	{
	    $client = self::createClient();
	    $container = $client->getContainer();
	    $router = $client->getContainer()->get('router');
	    
	    $employerRepository = $this->doctrine->getRepository('WanaworkUserBundle:EmployerProfile');
	    $employers = $employerRepository->findRandom();
	    if (sizeof($employers) !== 1) {
	        throw new \Exception("Cannot find an employer");
	    }
	    $employer = $employers[0];
	    $this->assertTrue($employer instanceof EmployerProfile);
	    
	    $client->request('POST', $router->generate('company_mark_verified', ['slug'=>$employer->getSlug()]));
	    $this->assertTrue(
	    	$client->getResponse()->isRedirect($router->generate('login',[], RouterInterface::ABSOLUTE_URL))
	    );
	    
	    $client->connect($container->getParameter('candidate_email'), $container->getParameter('candidate_password'));
	    $client->request('POST', $router->generate('company_mark_verified', ['slug'=>$employer->getSlug()]));
	    $this->assertSame(403, $client->getResponse()->getStatusCode());
	}
	
	public function testAdminRevokeActionRequiresValidUser()
	{
	    $client = self::createClient();
	    $container = $client->getContainer();
	    $router = $client->getContainer()->get('router');
	     
	    $employerRepository = $this->doctrine->getRepository('WanaworkUserBundle:EmployerProfile');
	    $employers = $employerRepository->findRandom();
	    if (sizeof($employers) !== 1) {
	        throw new \Exception("Cannot find an employer");
	    }
	    $employer = $employers[0];
	    $this->assertTrue($employer instanceof EmployerProfile);
	     
	    $client->request('POST', $router->generate('company_mark_unverified', ['slug'=>$employer->getSlug()]));
	    $this->assertTrue(
	        $client->getResponse()->isRedirect($router->generate('login',[], RouterInterface::ABSOLUTE_URL))
	    );
	     
	    $client->connect($container->getParameter('candidate_email'), $container->getParameter('candidate_password'));
	    $client->request('POST', $router->generate('company_mark_verified', ['slug'=>$employer->getSlug()]));
	    $this->assertSame(403, $client->getResponse()->getStatusCode());
	}
	
    /*
    public function testCompleteScenario()
    {
        // Create a new client to browse the application
        $client = static::createClient();

        // Create a new entry in the database
        $crawler = $client->request('GET', '/company/');
        $this->assertTrue(200 === $client->getResponse()->getStatusCode());
        $crawler = $client->click($crawler->selectLink('Create a new entry')->link());

        // Fill in the form and submit it
        $form = $crawler->selectButton('Create')->form(array(
            'wanawork_userbundle_employerprofiletype[field_name]'  => 'Test',
            // ... other fields to fill
        ));

        $client->submit($form);
        $crawler = $client->followRedirect();

        // Check data in the show view
        $this->assertTrue($crawler->filter('td:contains("Test")')->count() > 0);

        // Edit the entity
        $crawler = $client->click($crawler->selectLink('Edit')->link());

        $form = $crawler->selectButton('Edit')->form(array(
            'wanawork_userbundle_employerprofiletype[field_name]'  => 'Foo',
            // ... other fields to fill
        ));

        $client->submit($form);
        $crawler = $client->followRedirect();

        // Check the element contains an attribute with value equals "Foo"
        $this->assertTrue($crawler->filter('[value="Foo"]')->count() > 0);

        // Delete the entity
        $client->submit($crawler->selectButton('Delete')->form());
        $crawler = $client->followRedirect();

        // Check the entity has been delete on the list
        $this->assertNotRegExp('/Foo/', $client->getResponse()->getContent());
    }

    */
}
