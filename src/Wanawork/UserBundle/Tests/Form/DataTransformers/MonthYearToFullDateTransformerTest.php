<?php
namespace Wanawork\UserBundle\Tests\Form\DataTransformers;

use Wanawork\UserBundle\Tests\Entity\WebUnitTestBase;
use Wanawork\UserBundle\Form\DataTransformers\MonthYearToFullDateTransformer;

class MonthYearToFullDateTransformerTest extends WebUnitTestBase
{
    /**
     * From database to form
     */
    public function testTransform()
    {
        $transformer = new MonthYearToFullDateTransformer();
        
        $dateTime = new \DateTime();
        $dateTime->setDate($year = '2003', $month = '02', $day = '01');
        $result = $transformer->transform($dateTime);
        $this->assertEquals("{$month}/{$year}", $result);
        
        $result = $transformer->transform('test');
        $this->assertEquals('test', $result);
    }
    
    /**
     * from form to database
     */
    public function testTransformReverseWithInvalidDateReturnsNull()
    {
        $transformer = new MonthYearToFullDateTransformer();
        $result = $transformer->reverseTransform('00/2012');
        $this->assertNull($result);        
    }
    
    public function testTransformReverse()
    {
        $transformer = new MonthYearToFullDateTransformer();
        $result = $transformer->reverseTransform('03/2012');
        $this->assertTrue($result instanceof \DateTime);

        $date = new \DateTime();
        $date->setDate(2012, 3, 1);
        
        $diff = $result->getTimestamp() - $date->getTimestamp();
        $this->assertSame(0, $diff);
    }
    
    public function testValidDates()
    {
        $transformer = new MonthYearToFullDateTransformer();
        
        // test months 
        for ($i = 1; $i<=12; $i++) {
            $date = sprintf('%02d/2012', $i);
            $this->assertTrue($transformer->isValidDate($date));
        }
        
        // test years
        for ($i = 1900; $i<=2099; $i++) {
            $date = "01/{$i}";
            $this->assertTrue($transformer->isValidDate($date));
        }
    }

    public function testInvalidDates()
    {
        $transformer = new MonthYearToFullDateTransformer();
        // test months
        
        // 1 digit
        $date = '1/2012';
        $this->assertFalse($transformer->isValidDate($date));
        
        // one below 
        $date = '00/2012';
        $this->assertFalse($transformer->isValidDate($date));
        
        // one above
        $date = '13/2012';
        $this->assertFalse($transformer->isValidDate($date));
        
        // test years
        // one below
        $date = '01/1899';
        $this->assertFalse($transformer->isValidDate($date));
        
        // one above
        $date = '01/2100';
        $this->assertFalse($transformer->isValidDate($date));
        
        // invlid number of characters
        // one below
        $date = '01/200';
        $this->assertFalse($transformer->isValidDate($date));
    }
}