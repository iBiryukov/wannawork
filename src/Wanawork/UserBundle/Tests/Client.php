<?php
namespace Wanawork\UserBundle\Tests;

use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\BrowserKit\History;
use Symfony\Component\BrowserKit\CookieJar;

class Client extends \Symfony\Bundle\FrameworkBundle\Client
{

    protected static $connection;

    protected $requested;

    protected function doRequest($request)
    {
        if ($this->requested) {
            $this->kernel->shutdown();
            $this->kernel->boot();
        }
        
        $this->injectConnection();
        $this->requested = true;
        
        $_SERVER['SERVER_NAME'] = $request->getHost();
        $_SERVER['REQUEST_URI'] = $request->getUri();
        if ($request->getPort() != 80) {
            $_SERVER['SERVER_PORT'] = $request->getPort();
        }
        
        return $this->kernel->handle($request);
    }

    public function injectConnection()
    {
        if (null === self::$connection) {
            self::$connection = $this->getContainer()->get('doctrine.dbal.default_connection');
        } else {
            if (! $this->requested) {
                self::$connection->rollback();
            }
            $this->getContainer()->set('doctrine.dbal.default_connection', self::$connection);
        }
        
        if (! $this->requested) {
            self::$connection->beginTransaction();
        }
    }

    /**
     * Authenticate the user using email & password
     * @param string $email
     * @param string $password
     * @return  \Symfony\Component\DomCrawler\Crawler
     */
    public function connect($email, $password)
    {
        $crawler = $this->request('GET', '/login');
        if ($this->getResponse()->getStatusCode() === 200) {
            $form = $crawler->selectButton('login-button')->form();
            $form['_username'] = $email;
            $form['_password'] = $password;
            return $this->submit($form);
        }
    }
    
    public function logout()
    {
        return $this->request('GET', '/logout');
    }
}