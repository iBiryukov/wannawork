<?php
namespace Wanawork\UserBundle\Tests\Services;

use Wanawork\UserBundle\Tests\Entity\WebUnitTestBase;
use Wanawork\UserBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\HttpFoundation\Request;

class AccountServiceTest extends WebUnitTestBase
{
    private $service;
    
    private $container;
    
    private $client;
    
    /**
     *
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    
    protected function setUp()
    {
        $this->client = $client = self::createClient();
        $this->container = $container = $client->getContainer();
        
        $request = new Request();
        $this->container->enterScope('request');
        $this->container->set('request', $request, 'request');
        $context = $this->container->get('router')->getContext();
        $context->setHost($this->container->getParameter('router.request_context.host'));
        $context->setscheme($this->container->getParameter('router.request_context.scheme'));
    
        $this->em = $this->container->get('doctrine')->getManager();
        $this->em->getConnection()->beginTransaction();
    }
    
    protected function tearDown()
    {
        $this->em->getConnection()->rollBack();
        $this->em->getConnection()->close();
    }
    
    
    public function testRegistrationEmployee()
    {
        $client = $this->client;
        $container = $this->container;
        
        // Fill up the request for authentification purposes
        $client->request('GET', '/');
        
        $em = $this->em;
        $accountService = $container->get('wanawork.accounts_service');
        
        $user = new User();
        $user->setEmail('employee_test1@wanawork.ie');
        $user->setPassword('123456');
        $user->setName('Full Name');
        
        $roles = array($accountService->getRoleForAccountType('employee'));
        $accountService->register($user, $roles);
        
        $this->assertTrue(is_integer($user->getId()));
        $this->assertTrue($em->contains($user));
        $em->refresh($user);
        
        $userRepository = $em->getRepository('WanaworkUserBundle:User');
        $user = $userRepository->find($user->getId());
        $this->assertTrue($user instanceof User);
        $this->assertTrue($user->hasRoleName('ROLE_EMPLOYEE'));
        
        $this->assertNotSame('123456', $user->getPassword());
        
        $security = $container->get('security.context');
        
        // Test Login 
        $token = new UsernamePasswordToken($user, null, 'secured_area', $user->getRoles());
        $security->setToken($token);
        
        $event = new InteractiveLoginEvent($client->getRequest(), $token);
        $container->get("event_dispatcher")->dispatch("security.interactive_login", $event);
        
        $this->assertTrue($security->isGranted('OPERATOR', $user));
    }
    
    
    public function testRegistrationEmployer()
    {
        $client = $this->client;
        $container = $this->container;
        
        // Fill up the request for authentification purposes
        $client->request('GET', '/');
        $em = $this->em;
    
        $accountService = $container->get('wanawork.accounts_service');
    
        $email = 'employer_test1@wanawork.ie';
        $user = new User();
        $user->setEmail($email);
        $user->setPassword('123456');
        $user->setName('Full Name');
    
        $roles = array($accountService->getRoleForAccountType('employer'));
        $accountService->register($user, $roles);
    
        $this->assertTrue(is_integer($user->getId()));
        
        $em->clear();
        $user = null;
        
        $user = $em->getRepository('WanaworkUserBundle:User')->findOneByEmail($email);
        
        $this->assertTrue($user instanceof User);
        $this->assertTrue($user->hasRoleName('ROLE_EMPLOYER'));
        $this->assertNotSame('123456', $user->getPassword());
    
        $security = $container->get('security.context');
    
        // Test Login
        $token = new UsernamePasswordToken($user, null, 'secured_area', $user->getRoles());
        $security->setToken($token);
    
        $event = new InteractiveLoginEvent($client->getRequest(), $token);
        $container->get("event_dispatcher")->dispatch("security.interactive_login", $event);
    
        $this->assertTrue($security->isGranted('OPERATOR', $user));
    
    }
}