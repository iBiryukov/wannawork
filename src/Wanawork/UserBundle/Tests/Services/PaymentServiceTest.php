<?php
namespace Wanawork\UserBundle\Tests\Services;

use Wanawork\UserBundle\Tests\Entity\WebUnitTestBase;
use Wanawork\UserBundle\Entity\Billing\AdOrder;
use Wanawork\UserBundle\Entity\Billing\Payment;
use Wanawork\UserBundle\Entity\Ad;
use Wanawork\UserBundle\Entity\Billing\Order;

class PaymentServiceTest  extends WebUnitTestBase
{
    private $service;
    
    private $container;
    
    private $client;
    
    private $em;
    
    protected function setUp()
    {
        $this->client = $client = self::createClient();
        $this->container = $container = $client->getContainer();
        
        $this->service = $this->container->get('wanawork.billing.payment');
        $this->em = $this->container->get('doctrine');
    }
    
    protected function tearDown()
    {
        $this->em->getConnection()->close();
    }
    
    public function testPublishAd()
    {
        $bumps = 2;
        $amount = '5';
        $vatRate = 23;
        $plan = Ad::PLAN_PREMIUM;
        $duration = 60;
        $source = Payment::SOURCE_PAYPAL;
        
        $ad = new Ad();
        $ad->setHeadline($headline = 'Test Ad');
        $ad->setPitch($pitch = 'test');
        
        $order = new AdOrder($ad, $amount, $vatRate, $plan, $duration, $bumps);
        $order->setStatus(Order::STATUS_PAID);
        $payment = new Payment($order, $amount, $source);
        
        $this->assertNull($order->getRunFrom());
        $this->assertNull($order->getExpiry());
        $this->assertNull($ad->getExpiryDate());
        $this->assertFalse($ad->isPublished());
        
        $this->service->publishAd($order, $payment);
        
        $now = new \DateTime();
        $this->assertNotNull($order->getRunFrom());
        $this->assertNotNull($order->getExpiry());
        $this->assertNotNull($ad->getExpiryDate());
        $this->assertEquals($order->getExpiry(), $ad->getExpiryDate());
        $this->assertSame($now->format('d-m-y'), $order->getRunFrom()->format('d-m-y'));
        
        $expectedExpiry = clone $now;
        $expectedExpiry->add(new \DateInterval("P{$duration}D"));
        $this->assertSame($expectedExpiry->format('d-m-y'), $ad->getExpiryDate()->format('d-m-y'));
        $this->assertSame($expectedExpiry->format('d-m-y'), $order->getExpiry()->format('d-m-y'));
        
        $this->assertTrue($ad->isPublished());
        $this->assertTrue($ad->isPremium());
        
        // republish the ad (extends)
        $order = new AdOrder($ad, $amount, $vatRate, $plan, $duration, $bumps);
        $order->setStatus(Order::STATUS_PAID);
        $payment = new Payment($order, $amount, $source);
        $this->service->publishAd($order, $payment);
        
        $this->assertNotEquals($expectedExpiry, $ad->getExpiryDate());
        
        // account for 2 second difference in running here :) Close enough...
        $expiryDiff = abs($expectedExpiry->getTimestamp() - $order->getRunFrom()->getTimestamp());
        $this->assertTrue($expiryDiff <= 2);
        
//         $expectedExpiry->add(new \DateInterval("P{$duration}D"));
//         $this->assertSame($expectedExpiry->format('d-m-y'), $ad->getExpiryDate()->format('d-m-y'));
//         $this->assertSame($expectedExpiry->format('d-m-y'), $order->getExpiry())->format('d-m-y'));
        
    }
}