<?php
namespace Wanawork\UserBundle\Tests\Services;

use Wanawork\UserBundle\Entity\User;
use Wanawork\UserBundle\Tests\Entity\WebUnitTestBase;
use Wanawork\UserBundle\Entity\SystemEmail;
use Symfony\Component\Finder\Finder;
use Wanawork\UserBundle\Entity\Role;
use Wanawork\UserBundle\Entity\CvRequest;
use Wanawork\UserBundle\Entity\EmployerProfile;
use Wanawork\UserBundle\Entity\EmployeeProfile;
use Wanawork\UserBundle\Entity\Ad;
use Wanawork\UserBundle\Entity\Message;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\DomCrawler\Link;
use Wanawork\UserBundle\Entity\Billing\AdOrder;
use Wanawork\UserBundle\Entity\Billing\Payment;
use Wanawork\UserBundle\Entity\Billing\Transaction;
use Wanawork\UserBundle\Entity\Billing\VerificationOrder;
use Wanawork\UserBundle\Entity\UnsubscribedEmail;
use Wanawork\UserBundle\Entity\SystemEmailLink;

class MailServiceTest extends WebUnitTestBase
{
    /**
     * @var \Wanawork\UserBundle\Services\MailService
     */
    private $service;
    
    private $container;
    
    private $client;
    
    /**
     * @var \Wanawork\UserBundle\Services\EventSubscribers\MailListener
     */
    private $mailListener;
    
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    
    protected function setUp()
    {
        $this->client = $client = self::createClient();
        $this->container = $container = $client->getContainer();
        
        $request = new Request();
        $this->container->enterScope('request');
        $this->container->set('request', $request, 'request');
        $context = $this->container->get('router')->getContext();
        $context->setHost($this->container->getParameter('router.request_context.host'));
        $context->setscheme($this->container->getParameter('router.request_context.scheme'));
    
        $this->service = $this->container->get('wanawork.mail');
        $this->em = $this->container->get('doctrine')->getManager();
        
        $this->truncateSpool();
        $this->mailListener = $this->container->get('wanawork.mail.listener');
        
        $this->em->getConnection()->beginTransaction();
    }
    
    protected function tearDown()
    {
        $count = $this->truncateSpool();
        $this->em->getConnection()->rollBack();
        $this->em->clear();
        $this->em->getConnection()->close();

        if ($count === 0) {
            //$this->fail("Expected truncation to remove at least one file. Non removed");
        }
    }
    
    /**
     * Remove all pending emails
     * @return number
     */
    private function truncateSpool()
    {
        $swiftSpool = $this->container->getParameter('swift_spool_path');
        $files = $this->glob_recursive($swiftSpool . '/*.message*');
        $count = 0;
        foreach ($files as $file) {
            unlink($file) || $this->fail("cannot delete file:" . $file);
            ++$count;
        }
        return $count;
    }
    
    public function testSendRegistrationEmailWithCandidate()
    {
        $role = $this->em->getRepository('WanaworkUserBundle:Role')->find(Role::EMPLOYEE);
        
        $user = new User();
        $user->addRole($role);
        $user->setEmail($email = 'employee_test_mail0@wanawork.ie');
        $user->setName('test');
        $user->setPassword('test');
        $this->em->persist($user);
        $this->em->flush($user);
        
        $service = $this->service;
        $systemEmail = $service->emailUserRegistration($user);
        
        $this->assertTrue($systemEmail instanceof SystemEmail);
        $this->assertTrue($this->em->contains($systemEmail));
        $this->assertSame(SystemEmail::TYPE_REGISTRATION_CANDIDATE, $systemEmail->getType());
        $this->assertSame(SystemEmail::STATUS_QUEUED, $systemEmail->getStatus());
        $this->checkMessageFile($systemEmail);
    }
    
    public function testSendRegistrationEmailWithEmployer()
    {
        $role = $this->em->getRepository('WanaworkUserBundle:Role')->find(Role::EMPLOYER);
        
        $user = new User();
        $user->addRole($role);
        $user->setEmail($email = 'employee_test_mail1@wanawork.ie');
        $user->setName('test');
        $user->setPassword('test');
        $this->em->persist($user);
        $this->em->flush($user);
        
        $service = $this->service;
        $systemEmail = $service->emailUserRegistration($user);
        
        $this->assertTrue($systemEmail instanceof SystemEmail);
        $this->assertTrue($this->em->contains($systemEmail));
        $this->assertSame(SystemEmail::TYPE_REGISTRATION_EMPLOYER, $systemEmail->getType());
        $this->assertSame(SystemEmail::STATUS_QUEUED, $systemEmail->getStatus());
        $this->checkMessageFile($systemEmail);
    }
    
    public function testRestorePasswordEmail()
    {
        $user = new User();
        $user->setEmail($email = 'employee_test_mail1@wanawork.ie');
        $user->setName('test');
        $user->setPassword('test');
        $passwordReminder = $user->addPasswordReminder($ip = '0');
        $this->em->persist($user);
        $this->em->flush($user);
        
        $service = $this->service;
        $systemEmail = $service->emailPasswordReminder($passwordReminder);
        
        $this->assertTrue($systemEmail instanceof SystemEmail);
        $this->assertTrue($this->em->contains($systemEmail));
        $this->assertSame(SystemEmail::TYPE_RESTORE_PASSWORD, $systemEmail->getType());
        $this->assertSame(SystemEmail::STATUS_QUEUED, $systemEmail->getStatus());
        $this->checkMessageFile($systemEmail);
    }
    
    public function testEmailNewCVRequest()
    {
        $userEmployer = new User();
        $userEmployer->setEmail($email = 'employee_test_cvrequest_e@wanawork.ie');
        $userEmployer->setName('test');
        $userEmployer->setPassword('test');
        
        $employerProfile = new EmployerProfile();
        $employerProfile->setCompanyName($companyName = 'test');
        $employerProfile->setContactName($contactName = 'test');
        $employerProfile->setEmail($email = 'test@test.com');
        $employerProfile->setPhoneNumber($phoneNumber = '123');
        $employerProfile->setAddress($address = '123');
        $employerProfile->setCity('Dublin');
        $userEmployer->addEmployerProfile($employerProfile, true);
        
        $this->em->persist($userEmployer);
        $this->em->flush($userEmployer);
        
        $userEmployee = new User();
        $userEmployee->setEmail($email = 'employee_test_cvrequest_c@wanawork.ie');
        $userEmployee->setName('test');
        $userEmployee->setPassword('test');
        
        $profileEmployee = new EmployeeProfile();
        $profileEmployee->setAddress('test');
        $profileEmployee->setTitle($this->em->getRepository('WanaworkMainBundle:NameTitle')->findRandom());
        $profileEmployee->setCity('Dublin');
        $profileEmployee->setDob(new \DateTime());
        $profileEmployee->setName('test');
        $profileEmployee->setPhoneNumber(123456);
        $profileEmployee->setUser($userEmployee);
        
        $ad = new Ad();
        $ad->setHeadline('test');
        $ad->setPitch($pitch = 'test');
        $profileEmployee->addAd($ad);
        
        $cvRequest = new CvRequest();
        $cvRequest->setUser($userEmployer);
        $cvRequest->setAd($ad);
        $cvRequest->setEmployerProfile($employerProfile);
        $cvRequest->setMessage($message = 'hello');        
        
        $this->em->persist($userEmployee);
        $this->em->flush();
    
        $service = $this->service;
        $systemEmail = $service->emailNewCVRequest($cvRequest);
    
        $this->assertTrue($systemEmail instanceof SystemEmail);
        $this->assertTrue($this->em->contains($systemEmail));
        $this->assertSame(SystemEmail::TYPE_CV_REQUEST, $systemEmail->getType());
        $this->assertSame(SystemEmail::STATUS_QUEUED, $systemEmail->getStatus());
        $this->assertSame($userEmployee, $systemEmail->getUser());
        $this->checkMessageFile($systemEmail);
    }
    
    public function testEmailCVRequestResponseWithAcceptedResult()
    {
        $userEmployer = new User();
        $userEmployer->setEmail($email = 'employee_test_cvrequest_response_e1@wanawork.ie');
        $userEmployer->setName('test');
        $userEmployer->setPassword('test');
        
        $employerProfile = new EmployerProfile();
        $employerProfile->setCompanyName($companyName = 'test');
        $employerProfile->setContactName($contactName = 'test');
        $employerProfile->setEmail($email = 'test@test.com');
        $employerProfile->setPhoneNumber($phoneNumber = '123');
        $employerProfile->setAddress($address = '123');
        $employerProfile->setCity('Dublin');
        $userEmployer->addEmployerProfile($employerProfile, true);
        
        $userEmployee = new User();
        $userEmployee->setEmail($email = 'employee_test_cvrequest_response_c1@wanawork.ie');
        $userEmployee->setName('test');
        $userEmployee->setPassword('test');
        
        $profileEmployee = new EmployeeProfile();
        $profileEmployee->setAddress('test');
        $profileEmployee->setTitle($this->em->getRepository('WanaworkMainBundle:NameTitle')->findRandom());
        $profileEmployee->setCity('Dublin');
        $profileEmployee->setDob(new \DateTime());
        $profileEmployee->setName('test');
        $profileEmployee->setPhoneNumber(123456);
        $profileEmployee->setUser($userEmployee);
        
        $ad = new Ad();
        $ad->setHeadline('test');
        $ad->setPitch($pitch = 'test');
        $profileEmployee->addAd($ad);
        
        $cvRequest = new CvRequest();
        $cvRequest->setUser($userEmployer);
        $cvRequest->setAd($ad);
        $cvRequest->setEmployerProfile($employerProfile);
        $cvRequest->setMessage($message = 'hello');
        $cvRequest->approve();
        
        $this->em->persist($userEmployer);
        $this->em->persist($userEmployee);
        $this->em->persist($profileEmployee);
        $this->em->persist($employerProfile);
        $this->em->persist($ad);
        $this->em->persist($cvRequest);
        $this->em->flush();
        
        $service = $this->service;
        $systemEmail = $service->emailCVRequestResponse($cvRequest);
        
        $this->assertTrue($systemEmail instanceof SystemEmail);
        $this->assertTrue($this->em->contains($systemEmail));
        $this->assertSame(SystemEmail::TYPE_CV_REQUEST_RESPONSE, $systemEmail->getType());
        $this->assertSame(SystemEmail::STATUS_QUEUED, $systemEmail->getStatus());
        $this->assertSame($userEmployer, $systemEmail->getUser());
        $this->checkMessageFile($systemEmail);
        
    }
    
    public function testEmailCVRequestResponseWithDeniedResult()
    {
        $userEmployer = new User();
        $userEmployer->setEmail($email = 'employee_test_cvrequest_response_e2@wanawork.ie');
        $userEmployer->setName('test');
        $userEmployer->setPassword('test');
    
        $employerProfile = new EmployerProfile();
        $employerProfile->setCompanyName($companyName = 'test');
        $employerProfile->setContactName($contactName = 'test');
        $employerProfile->setEmail($email = 'test@test.com');
        $employerProfile->setPhoneNumber($phoneNumber = '123');
        $employerProfile->setAddress($address = '123');
        $employerProfile->setCity('Dublin');
        $userEmployer->addEmployerProfile($employerProfile, true);
    
        $userEmployee = new User();
        $userEmployee->setEmail($email = 'employee_test_cvrequest_response_c2@wanawork.ie');
        $userEmployee->setName('test');
        $userEmployee->setPassword('test');
    
        $profileEmployee = new EmployeeProfile();
        $profileEmployee->setAddress('test');
        $profileEmployee->setTitle($this->em->getRepository('WanaworkMainBundle:NameTitle')->findRandom());
        $profileEmployee->setCity('Dublin');
        $profileEmployee->setDob(new \DateTime());
        $profileEmployee->setName('test');
        $profileEmployee->setPhoneNumber(123456);
        $profileEmployee->setUser($userEmployee);
    
        $ad = new Ad();
        $ad->setHeadline('test');
        $ad->setPitch($pitch = 'test');
        $profileEmployee->addAd($ad);
    
        $cvRequest = new CvRequest();
        $cvRequest->setUser($userEmployer);
        $cvRequest->setAd($ad);
        $cvRequest->setEmployerProfile($employerProfile);
        $cvRequest->setMessage($message = 'hello');
        $cvRequest->deny();
    
        $this->em->persist($userEmployer);
        $this->em->persist($userEmployee);
        $this->em->persist($profileEmployee);
        $this->em->persist($employerProfile);
        $this->em->persist($ad);
        $this->em->persist($cvRequest);
        $this->em->flush();
    
        $service = $this->service;
        $systemEmail = $service->emailCVRequestResponse($cvRequest);
    
        $this->assertTrue($systemEmail instanceof SystemEmail);
        $this->assertTrue($this->em->contains($systemEmail));
        $this->assertSame(SystemEmail::TYPE_CV_REQUEST_RESPONSE, $systemEmail->getType());
        $this->assertSame(SystemEmail::STATUS_QUEUED, $systemEmail->getStatus());
        $this->assertSame($userEmployer, $systemEmail->getUser());
        $this->checkMessageFile($systemEmail);
    }
    
    public function testEmailCVRequestResponseWithPendingResult()
    {
        $userEmployer = new User();
        $userEmployer->setEmail($email = 'employee_test_cvrequest_response_e3@wanawork.ie');
        $userEmployer->setName('test');
        $userEmployer->setPassword('test');
    
        $employerProfile = new EmployerProfile();
        $employerProfile->setCompanyName($companyName = 'test');
        $employerProfile->setContactName($contactName = 'test');
        $employerProfile->setEmail($email = 'test@test.com');
        $employerProfile->setPhoneNumber($phoneNumber = '123');
        $employerProfile->setAddress($address = '123');
        $employerProfile->setCity('Dublin');
        $userEmployer->addEmployerProfile($employerProfile, true);
    
        $userEmployee = new User();
        $userEmployee->setEmail($email = 'employee_test_cvrequest_response_c3@wanawork.ie');
        $userEmployee->setName('test');
        $userEmployee->setPassword('test');
    
        $profileEmployee = new EmployeeProfile();
        $profileEmployee->setAddress('test');
        $profileEmployee->setTitle($this->em->getRepository('WanaworkMainBundle:NameTitle')->findRandom());
        $profileEmployee->setCity('Dublin');
        $profileEmployee->setDob(new \DateTime());
        $profileEmployee->setName('test');
        $profileEmployee->setPhoneNumber(123456);
        $profileEmployee->setUser($userEmployee);
    
        $ad = new Ad();
        $ad->setHeadline('test');
        $ad->setPitch($pitch = 'test');
        $profileEmployee->addAd($ad);
    
        $cvRequest = new CvRequest();
        $cvRequest->setUser($userEmployer);
        $cvRequest->setAd($ad);
        $cvRequest->setEmployerProfile($employerProfile);
        $cvRequest->setMessage($message = 'hello');
    
        $this->em->persist($userEmployer);
        $this->em->persist($userEmployee);
        $this->em->persist($profileEmployee);
        $this->em->persist($employerProfile);
        $this->em->persist($ad);
        $this->em->persist($cvRequest);
        $this->em->flush();
    
        $service = $this->service;
        $systemEmail = $service->emailCVRequestResponse($cvRequest);
        $this->assertNull($systemEmail);
    }
    
    public function testEmailMessageFromEmployer()
    {
        $userEmployer = new User();
        $userEmployer->setEmail($email = 'employee_test_message1@wanawork.ie');
        $userEmployer->setName('test');
        $userEmployer->setPassword('test');
    
        $employerProfile = new EmployerProfile();
        $employerProfile->setCompanyName($companyName = 'test');
        $employerProfile->setContactName($contactName = 'test');
        $employerProfile->setEmail($email = 'test@test.com');
        $employerProfile->setPhoneNumber($phoneNumber = '123');
        $employerProfile->setAddress($address = '123');
        $employerProfile->setCity('Dublin');
        $userEmployer->addEmployerProfile($employerProfile, true);
    
        $userEmployee = new User();
        $userEmployee->setEmail($email = 'employee_test_message2@wanawork.ie');
        $userEmployee->setName('test');
        $userEmployee->setPassword('test');
    
        $profileEmployee = new EmployeeProfile();
        $profileEmployee->setAddress('test');
        $profileEmployee->setTitle($this->em->getRepository('WanaworkMainBundle:NameTitle')->findRandom());
        $profileEmployee->setCity('Dublin');
        $profileEmployee->setDob(new \DateTime());
        $profileEmployee->setName('test');
        $profileEmployee->setPhoneNumber(123456);
        $profileEmployee->setUser($userEmployee);
    
        $ad = new Ad();
        $ad->setHeadline('test');
        $ad->setPitch($pitch = 'test');
        $profileEmployee->addAd($ad);
    
        $cvRequest = new CvRequest();
        $cvRequest->setUser($userEmployer);
        $cvRequest->setAd($ad);
        $cvRequest->setEmployerProfile($employerProfile);
        $cvRequest->setMessage($message = 'hello');
        $cvRequest->approve();
        $thread = $cvRequest->createMailThread();
        $message = new Message($text = 'test', $userEmployer, $employerProfile, $thread);
        $thread->addMessage($message);
    
        $this->em->persist($userEmployer);
        $this->em->persist($userEmployee);
        $this->em->persist($profileEmployee);
        $this->em->persist($employerProfile);
        $this->em->persist($ad);
        $this->em->persist($cvRequest);
        $this->em->persist($message);
        $this->em->flush();
    
        $service = $this->service;
        $systemEmail = $service->emailNewMessage($message);
    
        $this->assertTrue($systemEmail instanceof SystemEmail);
        $this->assertTrue($this->em->contains($systemEmail));
        $this->assertSame(SystemEmail::TYPE_NEW_MESSAGE, $systemEmail->getType());
        $this->assertSame(SystemEmail::STATUS_QUEUED, $systemEmail->getStatus());
        $this->assertSame($userEmployee, $systemEmail->getUser());
        $this->checkMessageFile($systemEmail);
    }
    
    public function testEmailMessageFromEmployee()
    {
        $userEmployer = new User();
        $userEmployer->setEmail($email = 'employee_test_message1@wanawork.ie');
        $userEmployer->setName('test');
        $userEmployer->setPassword('test');
    
        $employerProfile = new EmployerProfile();
        $employerProfile->setCompanyName($companyName = 'test');
        $employerProfile->setContactName($contactName = 'test');
        $employerProfile->setEmail($email = 'test@test.com');
        $employerProfile->setPhoneNumber($phoneNumber = '123');
        $employerProfile->setAddress($address = '123');
        $employerProfile->setCity('Dublin');
        $userEmployer->addEmployerProfile($employerProfile, true);
    
        $userEmployee = new User();
        $userEmployee->setEmail($email = 'employee_test_message2@wanawork.ie');
        $userEmployee->setName('test');
        $userEmployee->setPassword('test');
    
        $profileEmployee = new EmployeeProfile();
        $profileEmployee->setAddress('test');
        $profileEmployee->setTitle($this->em->getRepository('WanaworkMainBundle:NameTitle')->findRandom());
        $profileEmployee->setCity('Dublin');
        $profileEmployee->setDob(new \DateTime());
        $profileEmployee->setName('test');
        $profileEmployee->setPhoneNumber(123456);
        $profileEmployee->setUser($userEmployee);
    
        $ad = new Ad();
        $ad->setHeadline('test');
        $ad->setPitch($pitch = 'test');
        $profileEmployee->addAd($ad);
    
        $cvRequest = new CvRequest();
        $cvRequest->setUser($userEmployer);
        $cvRequest->setAd($ad);
        $cvRequest->setEmployerProfile($employerProfile);
        $cvRequest->setMessage($message = 'hello');
        $cvRequest->approve();
        $thread = $cvRequest->createMailThread();
        $message = new Message($text = 'test', $userEmployee, $profileEmployee, $thread);
        $thread->addMessage($message);
    
        $this->em->persist($userEmployer);
        $this->em->persist($userEmployee);
        $this->em->persist($profileEmployee);
        $this->em->persist($employerProfile);
        $this->em->persist($ad);
        $this->em->persist($cvRequest);
        $this->em->persist($message);
        $this->em->flush();
    
        $service = $this->service;
        $systemEmail = $service->emailNewMessage($message);
    
        $this->assertTrue($systemEmail instanceof SystemEmail);
        $this->assertTrue($this->em->contains($systemEmail));
        $this->assertSame(SystemEmail::TYPE_NEW_MESSAGE, $systemEmail->getType());
        $this->assertSame(SystemEmail::STATUS_QUEUED, $systemEmail->getStatus());
        $this->assertSame($userEmployer, $systemEmail->getUser());
        $this->checkMessageFile($systemEmail);
    }
    
    public function testEmailReceiptToCandidate()
    {
        
        $userEmployee = new User();
        $userEmployee->setEmail($email = 'employee_test_message2@wanawork.ie');
        $userEmployee->setName('test');
        $userEmployee->setPassword('test');
        
        $profileEmployee = new EmployeeProfile();
        $profileEmployee->setAddress('test');
        $profileEmployee->setTitle($this->em->getRepository('WanaworkMainBundle:NameTitle')->findRandom());
        $profileEmployee->setCity('Dublin');
        $profileEmployee->setDob(new \DateTime());
        $profileEmployee->setName('test');
        $profileEmployee->setPhoneNumber(123456);
        $profileEmployee->setUser($userEmployee);
        
        $ad = new Ad();
        $ad->setHeadline('test');
        $ad->setPitch($pitch = 'test');
        $profileEmployee->addAd($ad);
        
        $order = new AdOrder($ad, $amount = 241, $vatRate = 23, $plan = Ad::PLAN_STANDARD, $duration = 60, $bumps = 5);
        $order->setStatus(AdOrder::STATUS_PAID);
        
        $payment = new Payment($order, $order->getAmountIncludingVat(), Payment::SOURCE_PAYPAL);
        $payment->setState(Payment::STATE_DEPOSITED);
        
        $transaction = new Transaction($payment);
        $transaction->setType(Transaction::TRANSACTION_TYPE_DEPOSIT);
        $transaction->setState(Transaction::STATE_SUCCESS);
        
        $this->em->persist($userEmployee);
        $this->em->persist($profileEmployee);
        $this->em->persist($ad);
        $this->em->flush();
        
        $service = $this->service;
        
        $receiptService = $this->container->get('wanawork.receipt_service');
        $filePath = $receiptService->generateReceipt($order);
        
        $this->assertFalse($order->isReceiptEmailed());
        $systemEmail = $service->emailPaymentReceipt($order, $filePath);
        
        $this->assertTrue($systemEmail instanceof SystemEmail);
        $this->assertTrue($this->em->contains($systemEmail));
        $this->assertSame(SystemEmail::TYPE_RECEIPT, $systemEmail->getType());
        $this->assertSame(SystemEmail::STATUS_QUEUED, $systemEmail->getStatus());
        $this->assertSame($userEmployee, $systemEmail->getUser());
        $this->assertTrue($order->isReceiptEmailed());
        $this->checkMessageFile($systemEmail);
        
        unlink($filePath);
    }
    
    public function testEmailReceiptToEmployer()
    {
    
        $userEmployer = new User();
        $userEmployer->setEmail($email = 'employee_test_message1@wanawork.ie');
        $userEmployer->setName('test');
        $userEmployer->setPassword('test');
    
        $employerProfile = new EmployerProfile();
        $employerProfile->setCompanyName($companyName = 'test');
        $employerProfile->setContactName($contactName = 'test');
        $employerProfile->setEmail($email = 'test@test.com');
        $employerProfile->setPhoneNumber($phoneNumber = '123');
        $employerProfile->setAddress($address = '123');
        $employerProfile->setCity('Dublin');
        $userEmployer->addEmployerProfile($employerProfile, true);
    
    
        $order = new VerificationOrder($employerProfile, $amount = 24900, $vatRate = 23);
        $order->setStatus(AdOrder::STATUS_PAID);
    
        $payment = new Payment($order, $order->getAmountIncludingVat(), Payment::SOURCE_PAYPAL);
        $payment->setState(Payment::STATE_DEPOSITED);
    
        $transaction = new Transaction($payment);
        $transaction->setType(Transaction::TRANSACTION_TYPE_DEPOSIT);
        $transaction->setState(Transaction::STATE_SUCCESS);
    
        $this->em->persist($userEmployer);
        $this->em->persist($employerProfile);
        $this->em->persist($order);
        $this->em->flush();
    
        $service = $this->service;
    
        $receiptService = $this->container->get('wanawork.receipt_service');
        $filePath = $receiptService->generateReceipt($order);
    
        $this->assertFalse($order->isReceiptEmailed());
        $systemEmail = $service->emailPaymentReceipt($order, $filePath);
    
        $this->assertTrue($systemEmail instanceof SystemEmail);
        $this->assertTrue($this->em->contains($systemEmail));
        $this->assertSame(SystemEmail::TYPE_RECEIPT, $systemEmail->getType());
        $this->assertSame(SystemEmail::STATUS_QUEUED, $systemEmail->getStatus());
        $this->assertSame($userEmployer, $systemEmail->getUser());
        $this->assertTrue($order->isReceiptEmailed());
        $this->checkMessageFile($systemEmail);
    
        unlink($filePath);
    }
    
    public function testEmailCustomMessage()
    {
        $subject = SystemEmail::$subjects[SystemEmail::TYPE_PRESALES];
        $systemEmail = new SystemEmail($user = null, $subject, $type = SystemEmail::TYPE_PRESALES, $email = "test@wanawork.ie");
        
        $templating = $this->container->get('templating');
        $text = $templating->render('::/email/pre-sale.html.twig', array(
        	'systemEmail' => $systemEmail,
        ));
        
        $systemEmail->setText($text);
        
        $this->em->persist($systemEmail);
        
        $service = $this->service;
        $systemEmail = $service->emailCustomMessage($systemEmail);
        
        $this->assertTrue($systemEmail instanceof SystemEmail);
        $this->assertTrue($this->em->contains($systemEmail));
        $this->assertSame(SystemEmail::TYPE_PRESALES, $systemEmail->getType());
        $this->assertSame(SystemEmail::STATUS_QUEUED, $systemEmail->getStatus());
        $this->checkMessageFile($systemEmail);
    }
    
    private function glob_recursive($pattern, $flags = 0)
    {
        $files = glob($pattern, $flags);
    
        foreach (glob(dirname($pattern).'/*', GLOB_ONLYDIR|GLOB_NOSORT) as $dir)
        {
            $files = array_merge($files, $this->glob_recursive($dir.'/'.basename($pattern), $flags));
        }
    
        return $files;
    }
    
    private function checkMessageFile(SystemEmail $systemEmail)
    {
        static $checkedFiles;
        if ($checkedFiles === null) {
            $checkedFiles = array();
        }
        
        $swiftSpool = $this->container->getParameter('swift_spool_path');
        $files = $this->glob_recursive($swiftSpool . '/*.message*');
        
        $newFiles = array_filter($files, function($file) use($checkedFiles){
        	return !in_array($file, $checkedFiles, true);
        });
        
        $this->assertCount(1, $newFiles);
        
        $file = $newFiles[0];
        $checkedFiles[] = $file;
        
        $this->assertTrue(is_file($file));
        
        $messageContents = file_get_contents($file);
        $message = unserialize($messageContents);
        $this->assertTrue($message instanceof \Swift_Mime_Message);
        
        if (!$message->getTo()) {
            $this->fail("Message is missing a recepient");
        }
        
        if (!$message->getFrom()) {
            $this->fail("Message is missing a 'from' part");
        }
        
        $this->assertTrue(strlen($message->getSubject()) > 0);
        $this->assertTrue(strlen($message->getBody()) > 0);     
        $this->assertTrue($message->getHeaders()->has('wanawork-email-id'));
        $this->assertSame($message->getBody(), $systemEmail->getText(), "Number of links: " . $systemEmail->getLinks()->count());
        
        $baseUrl = $this->container->getParameter('site.url');
        
        $crawler = new Crawler($message->getBody());

        $urlRegex = '/^http(s?):\/\/.*\..*$/';
        foreach ($crawler->filter('a') as $link) {
            $href = $link->getAttribute('href');
            $this->assertRegExp($urlRegex, $href, "Invalid URL: '{$href}'");
            
            if (strpos($href, '.local') !== false) {
                $this->assertSame(0, strpos($href, $baseUrl), "'{$href}' not begins with '{$baseUrl}'");
            }

            if (preg_match('/wan(n)?awork/', $href)) {
                $this->assertRegExp('/redirect\/[a-z0-9]+$/', $href);
                
                preg_match('/redirect\/(.*)$/', $href, $match);
                if (sizeof($match) === 2) {
                    $link = $this->em->getRepository('WanaworkUserBundle:SystemEmailLink')->find($match[1]);
                    $this->assertTrue($link instanceof SystemEmailLink);
                } else {
                    $this->markTestSkipped("Skipping link '$href'");
                }
                
            } else {
                $this->markTestSkipped("Skipping link '$href'");
            }
        }
        
        foreach ($crawler->filter('img') as $link) {
            $href = $link->getAttribute('src');
            $this->assertRegExp($urlRegex, $href, "Invalid URL: '{$href}'");
            $this->assertSame(0, strpos($href, $baseUrl), "'{$href}' not begins with '{$baseUrl}'");
        }
        
        if ($systemEmail->getType() === SystemEmail::TYPE_RECEIPT) {
            $hasAttachment = false;
            foreach($message->getChildren() as $child) {
                if ($child instanceof \Swift_Attachment) {
                    $hasAttachment = true;
                    break;
                }
            }
            
            $this->assertTrue($hasAttachment);
        }
    }
    
    public function testUnsubscribe()
    {
        $service = $this->service;
        $unsub = $service->unsubscribe($email = 'test@wannawork.ie');
        
        $this->assertTrue($unsub instanceof UnsubscribedEmail);
        $this->assertTrue($this->em->contains($unsub));
        
        $unsub2 = $service->unsubscribe($email);
        $this->assertSame($unsub2, $unsub);
    }
    
    public function testUnsubscribeWithSystemEmail()
    {
        $userEmployer = new User();
        $userEmployer->setEmail($email = 'test2@wannawork.ie');
        $userEmployer->setName('test');
        $userEmployer->setPassword('test');
        $systemEmail = new SystemEmail($userEmployer, $subject = 'test', SystemEmail::TYPE_CV_REQUEST);
        
        $this->em->persist($userEmployer);
        $this->em->persist($systemEmail);
        $this->em->flush();
        
        $systemEmailId = $systemEmail->getId();
        
        $service = $this->service;
        $unsub = $service->unsubscribe($email, $systemEmail);
        
        $this->assertTrue($unsub instanceof UnsubscribedEmail);
        $this->assertTrue($this->em->contains($unsub));
        $this->assertSame($systemEmail, $unsub->getSystemEmail());
        $this->assertTrue($service->isUnsubscribed($email));
        
        $unsub2 = $service->unsubscribe($email, $systemEmail);
        $this->assertSame($unsub2, $unsub);
        $this->assertSame($systemEmail, $unsub2->getSystemEmail());
        $this->assertTrue($service->isUnsubscribed($email));
        
        // ensure system email was not not cascaded in delete operation
        $systemEmail = $this->em->getRepository('WanaworkUserBundle:SystemEmail')->find($systemEmailId);
        $this->assertNotNull($systemEmail);
    }
    
    public function testResubscribe()
    {
        $service = $this->service;
        $unsub = $service->unsubscribe($email = 'test3@wannawork.ie');
    
        $this->assertTrue($unsub instanceof UnsubscribedEmail);
        $this->assertTrue($this->em->contains($unsub));
        
        $result = $service->resubscribe($email);
        $this->assertTrue($result);
        
        $this->assertFalse($service->isUnsubscribed($email));
    }
}
