<?php
namespace Wanawork\UserBundle\Tests\Services;

use Wanawork\UserBundle\Tests\Entity\WebUnitTestBase;


class SitemapServiceTest extends WebUnitTestBase
{
    /**
     *
     * @var \Doctrine\ORM\EntityManager
     */
    private $doctrine;
    
    private $container;
    
    private $client;
    
    
    protected function setUp()
    {
        $this->client = $client = self::createClient();
        $this->container = $container = $client->getContainer();
        $this->doctrine = $doctrine = $container->get('doctrine')->getManager();
    
        $this->doctrine->getConnection()->beginTransaction();
    
    }
    
    protected function tearDown()
    {
        $doctrine = $this->doctrine;
        $doctrine->getConnection()->rollBack();
        $doctrine->clear();
        $doctrine->getConnection()->close();
    }
    
    public function testGenerateMap()
    {
        $sitemapService = $this->container->get('wanawork.sitemap');
        $map = $sitemapService->generateMap();
        
        $this->assertTrue(is_array($map));
        $this->assertTrue(sizeof($map) > 0);
        $this->assertContainsOnlyInstancesOf('Wanawork\UserBundle\Entity\Containers\SitemapUrl', $map);
    }
}