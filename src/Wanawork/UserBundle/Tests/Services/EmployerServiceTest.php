<?php
namespace Wanawork\UserBundle\Tests\Services;

use Wanawork\UserBundle\Tests\Entity\WebUnitTestBase;
use Wanawork\UserBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Wanawork\UserBundle\Entity\EmployerProfile;

class EmployerServiceTest extends WebUnitTestBase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    
    private $container;
    
    protected function setUp()
    {
        $client = self::createClient();
        $this->container = $container = $client->getContainer();
        $this->em = $container->get('doctrine')->getManager();
        $this->em->getConnection()->beginTransaction();
    }
    
    protected function tearDown()
    {
        $this->em->getConnection()->rollBack();
        $this->em->getConnection()->close();
    }
    
    public function testCreateProfile()
    {
        
        $container = $this->container;
        
        $em = $this->em;
        $service = $container->get('wanawork.employer_service');
        
        $user = new User();
        $user->setEmail('employee_test10@wanawork.ie');
        $user->setPassword('123456');
        $user->setName('Full Name');
        $em->persist($user);
        
        $profile = new EmployerProfile();
        $profile->setCompanyName($companyName = 'test');
        $profile->setContactName($contactName = 'test');
        $profile->setEmail($email = 'test@test.com');
        $profile->setPhoneNumber($phoneNumber = '123');
        $profile->setAddress($address = '123');
        $profile->setCity('Dublin');
        
        $service->createProfile($profile, $user);
        
        $this->assertTrue(is_integer($profile->getId()));
        $em->refresh($profile);
        
        $profile = $em->getRepository("WanaworkUserBundle:EmployerProfile")->find($profile->getId());
        $this->assertTrue($profile instanceof EmployerProfile);
        $user = $profile->getUsers()->first();
        $this->assertTrue($user instanceof User);
        $this->assertTrue($user->hasRole($profile->getAdminRole()));
        $this->assertTrue($user->hasRole($profile->getUserRole()));
    }
}
