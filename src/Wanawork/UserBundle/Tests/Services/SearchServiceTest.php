<?php
namespace Wanawork\UserBundle\Tests\Services;

use Wanawork\UserBundle\Tests\Entity\WebUnitTestBase;
use Wanawork\UserBundle\Entity\Ad;


use Wanawork\UserBundle\Entity\PositionType;
use Wanawork\UserBundle\Entity\CV;
use Wanawork\MainBundle\Entity\County;
use Wanawork\UserBundle\Entity\SectorJob;
use Wanawork\UserBundle\Entity\Sector;
use Wanawork\UserBundle\Entity\SectorExperience;
use Wanawork\UserBundle\Entity\User;
use Wanawork\UserBundle\Entity\EmployeeProfile;
use Wanawork\MainBundle\Entity\Language;
use Wanawork\UserBundle\Entity\Qualification;
use Wanawork\UserBundle\Entity\Billing\AdOrder;
use Wanawork\UserBundle\Entity\Billing\Order;
use Wanawork\UserBundle\Entity\Role;
use Wanawork\UserBundle\Entity\Search;
use Wanawork\UserBundle\Entity\Billing\Payment;

class SearchServiceTest extends WebUnitTestBase
{
/**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    
    private $container;
    
    /**
     * Valid advert for validation testing
     * @var \Wanawork\UserBundle\Entity\Ad
     */
    private $ad;
    
    protected function setUp()
    {
        $client = self::createClient();
        $this->container = $container = $client->getContainer();
        $this->em = $container->get('doctrine')->getManager();
        $this->em->getConnection()->beginTransaction();
        
        $this->ad = $ad = new Ad();
        $ad->setHeadline('test');
        $ad->setPitch('test');
        $ad->setPositions($this->em->getRepository('WanaworkUserBundle:PositionType')->findRandom($limit = 2));
        $ad->setLocations($this->em->getRepository('WanaworkMainBundle:County')->findRandom($limit = 2));
        
        $professions = $this->em->getRepository('WanaworkUserBundle:SectorJob')->findJobsWithoutAds($limit = 1);
        if (sizeof($professions) !== 1) {
            throw new \Exception("Unable to find professions without ads");
        }
        
        $ad->setProfession($professions[0]);
        $ad->setExperience($this->em->getRepository('WanaworkUserBundle:SectorExperience')->findRandom());
        $ad->setLanguages($this->em->getRepository('WanaworkMainBundle:Language')->findRandom($limit = 2));
        $ad->setEducationLevel($this->em->getRepository('WanaworkUserBundle:Qualification')->findRandom());
        $ad->setIndustries($this->em->getRepository('WanaworkUserBundle:Sector')->findRandom());
        
        $user = $this->em->getRepository('WanaworkUserBundle:User')->findRandomEmployeeUser();
        
        if (!$user instanceof User) {
            throw new \Exception("Unable to find the user");
        }
        
        if (!$user->getEmployeeProfile() instanceof EmployeeProfile) {
            throw new \Exception("User has not employee profile");
        }
        
        $ad->setProfile($user->getEmployeeProfile());
    }
    
    protected function tearDown()
    {
        $this->em->getConnection()->rollBack();
        $this->em->getConnection()->close();
    }
    
    
    public function testSearchWithStandardAd()
    {
        $em = $this->em;
        $paginator = $this->container->get('knp_paginator');
        $searchService = $this->container->get('wanawork.search_service');
        
        $ad1 = clone $this->ad;
        $search = new Search();
        $search->setProfession($ad1->getProfession());
        
        $em->persist($search);
        $em->persist($ad1);
        $em->flush();
        
        // Unpublished advert
        $ads = $searchService->findAds($search, $paginator);
        $this->assertTrue($ads instanceof \Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination);
        $this->assertCount(0, $ads);
        
        // Publish for free
        $ad1->publishStandard($duration = 30, $bumps = 5);
        $em->flush($ad1);
        
        $ads = $searchService->findAds($search, $paginator);
        $this->assertCount(1, $ads);
        $this->assertSame($ad1, $ads[0]);
        $this->assertEquals(100, $ad1->getMatch());
    }
    
    public function testSearchWithPremiumAd()
    {
        $em = $this->em;
        $paginator = $this->container->get('knp_paginator');
        $searchService = $this->container->get('wanawork.search_service');
        
        $ad1 = clone $this->ad;
        $order = new AdOrder($ad1, $amount = 500, $vatRate = 23, $plan = Ad::PLAN_PREMIUM, $duration = 30, $bumps = 5);
        $order->setStatus(AdOrder::STATUS_PAID);
        $ad1->publishPremium($order);
        
        $search = new Search();
        $search->setProfession($ad1->getProfession());
        
        $em->persist($search);
        $em->persist($ad1);
        $em->flush();
        
        $ads = $searchService->findAds($search, $paginator);
        $this->assertCount(1, $ads);
        $this->assertSame($ad1, $ads[0]);
        $this->assertEquals(100, $ad1->getMatch());
    }
    
    public function testPremiumAdComesAboveStandard()
    {
        $em = $this->em;
        $paginator = $this->container->get('knp_paginator');
        $searchService = $this->container->get('wanawork.search_service');
        
        $adStandard = clone $this->ad;
        $adStandard->publishStandard($duration = 60, $bumps = 5);
        
        $adPremium = clone $this->ad;
        $order = new AdOrder($adPremium, $amount = 500, $vatRate = 23, $plan = Ad::PLAN_PREMIUM, $duration = 30, $bumps = 5);
        $order->setStatus(AdOrder::STATUS_PAID);
        $adPremium->publishPremium($order);
        
        $search = new Search();
        $search->setProfession($adStandard->getProfession());
        
        $em->persist($search);
        $em->persist($adStandard);
        $em->persist($adPremium);
        $em->flush();
        
        $ads = $searchService->findAds($search, $paginator);
        $this->assertCount(2, $ads);
        $this->assertSame($adPremium, $ads[0]);
        $this->assertSame($adStandard, $ads[1]);
    }
    
    public function testPremiumAdUpgradedFromStandardComesHigherThanOlderPremiumAdvert()
    {
        $em = $this->em;
        $paginator = $this->container->get('knp_paginator');
        $searchService = $this->container->get('wanawork.search_service');
        
        $adStandard = clone $this->ad;
        $adStandard->publishStandard($duration = 60, $bumps = 5);
        
        $adPremium = clone $this->ad;
        $order = new AdOrder($adPremium, $amount = 500, $vatRate = 23, $plan = Ad::PLAN_PREMIUM, $duration = 30, $bumps = 5);
        $order->setStatus(AdOrder::STATUS_PAID);
        $payment = new Payment($order, $order->getAmount(), $source = Payment::SOURCE_PAYPAL);
        $payment->setState(Payment::STATE_DEPOSITED);
        $payment->setUpdatedAt(new \DateTime());
        $adPremium->publishPremium($order);
        
        
        $search = new Search();
        $search->setProfession($adStandard->getProfession());
        
        $em->persist($search);
        $em->persist($adStandard);
        $em->persist($adPremium);
        $em->flush();
        
        $ads = $searchService->findAds($search, $paginator);
        $this->assertCount(2, $ads);
        $this->assertSame($adPremium, $ads[0]);
        $this->assertSame($adStandard, $ads[1]);
        
        // Upgrade standard to premium
        $order = new AdOrder($adStandard, $amount = 500, $vatRate = 23, $plan = Ad::PLAN_PREMIUM, $duration = 30, $bumps = 5);
        $order->setStatus(AdOrder::STATUS_PAID);
        $payment = new Payment($order, $order->getAmount(), $source = Payment::SOURCE_PAYPAL);
        $payment->setState(Payment::STATE_DEPOSITED);
        $payment->setUpdatedAt(new \DateTime('tomorrow'));
       
        $adStandard->publishPremium($order);
        $em->flush();
        
        $ads = $searchService->findAds($search, $paginator);
        $this->assertCount(2, $ads);
        $this->assertSame($adStandard->getId(), $ads[0]->getId());
        $this->assertSame($adPremium->getId(), $ads[1]->getId());
    }
    
    public function testStandardOrderSortingAfterSecondPublish()
    {
        $em = $this->em;
        $paginator = $this->container->get('knp_paginator');
        $searchService = $this->container->get('wanawork.search_service');
        
        $adStandard1 = clone $this->ad;
        $adStandard1->publishStandard($duration = 60, $bumps = 5);
        
        $adStandard2 = clone $this->ad;
        $adStandard2->publishStandard($duration = 60, $bumps = 5, $runFrom = new \DateTime('+1 seconds'));
        
        $search = new Search();
        $search->setProfession($adStandard1->getProfession());
        
        $em->persist($search);
        $em->persist($adStandard1);
        $em->persist($adStandard2);
        $em->flush();
        
        $ads = $searchService->findAds($search, $paginator);
        $this->assertCount(2, $ads);
        $this->assertSame($adStandard2, $ads[0]);
        $this->assertSame($adStandard1, $ads[1]);
        
        // republish the first ad, it should be at the top
        $adStandard1->publishStandard($duration = 60, $bumps = 5, $runFrom = new \DateTime('+2 seconds'));
        $em->flush();
        
        $ads = $searchService->findAds($search, $paginator);
        $this->assertCount(2, $ads);
        $this->assertSame($adStandard1, $ads[0]);
        $this->assertSame($adStandard2, $ads[1]);
    }
    
    public function testSearchWithAllOptions()
    {
        $em = $this->em;
        $paginator = $this->container->get('knp_paginator');
        $searchService = $this->container->get('wanawork.search_service');
        
        $ad = clone $this->ad;
        $search = new Search();
        $search->setProfession($ad->getProfession());
        $search->setIndustries($ad->getIndustries()->toArray());
        $search->setEducationLevel($ad->getEducationLevel());
        $search->setExperience($ad->getExperience());
        $search->setLanguages($ad->getLanguages()->toArray());
        $search->setLocations($ad->getLocations()->toArray());
        $search->setPositions($ad->getPositions()->toArray());
        $ad->publishStandard($duration = 30, $bumps = 5);
        
        $em->persist($search);
        $em->persist($ad);
        $em->flush();
        
        $ads = $searchService->findAds($search, $paginator);
        $this->assertCount(1, $ads);
        $this->assertSame($ad, $ads[0]);
        $this->assertEquals(100, $ad->getMatch());
    }
}