<?php
namespace Wanawork\UserBundle\Tests\Services\eventSubscribers;

use Wanawork\UserBundle\Entity\User;
use Wanawork\UserBundle\Tests\Entity\WebUnitTestBase;
use Wanawork\UserBundle\Entity\SystemEmail;
use Symfony\Component\Finder\Finder;
use Wanawork\UserBundle\Entity\Role;
use Symfony\Component\HttpFoundation\Request;

class MailListenerTest extends WebUnitTestBase
{
    private $container;
    
    private $client;
    
    /**
     * @var \Wanawork\UserBundle\Services\EventSubscribers\MailListener
     */
    private $mailListener;
    
    /**
     *
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    
    protected function setUp()
    {
        $this->client = $client = self::createClient();
        $this->container = $container = $client->getContainer();
        
        $request = new Request();
        $this->container->enterScope('request');
        $this->container->set('request', $request, 'request');
        $context = $this->container->get('router')->getContext();
        $context->setHost($this->container->getParameter('router.request_context.host'));
        $context->setscheme($this->container->getParameter('router.request_context.scheme'));
    
        $this->em = $this->container->get('doctrine')->getManager();
    
        $this->truncateSpool();
        $this->mailListener = $this->container->get('wanawork.mail.listener');
    
        $this->em->getConnection()->beginTransaction();
    }
    
    protected function tearDown()
    {
        $this->truncateSpool();
        $this->em->getConnection()->rollBack();
        $this->em->getConnection()->close();
    }
    
    private function truncateSpool()
    {
        $swiftSpool = $this->container->getParameter('swift_spool_path');
        $files = $this->glob_recursive($swiftSpool . '/*.message*');
        foreach ($files as $file) {
            unlink($file) || $this->fail("cannot delete file:" . $file);
        }
    }
    
    public function testBeforeAndAfterSend()
    {
        $user = new User();
        $user->addRole($this->em->getRepository('WanaworkUserBundle:Role')->find(Role::EMPLOYEE));
        $user->setEmail($email = 'employee_test2@wanawork.ie');
        $user->setName($name = 'test');
        $user->setPassword('test');
        $this->em->persist($user);
        $this->em->flush();
        
        $service = $this->container->get('wanawork.mail');
        $systemEmail = $service->emailUserRegistration($user);
        
        $this->assertTrue($systemEmail instanceof SystemEmail);
        $this->assertSame(SystemEmail::STATUS_QUEUED, $systemEmail->getStatus());
        
        $spoolPath = $this->container->getParameter('swift_spool_path');
        $files = $this->glob_recursive($spoolPath . '/*.message');
        if (sizeof($files) !== 1) {
            $this->fail("Expected 1 file, got ".sizeof($files)." messages");
        }
        
        $transport = $this->getMock('\Swift_Transport');
        
        $message = unserialize(file_get_contents($files[0]));
        $event = new \Swift_Events_SendEvent($transport, $message);
        $event->setResult(\Swift_Events_SendEvent::RESULT_SUCCESS);
        
        $this->mailListener->beforeSendPerformed(clone $event);
        $this->assertSame(SystemEmail::STATUS_SENDING, $systemEmail->getStatus());
        $this->assertSame(1, $systemEmail->getAttempts());
        $this->assertFalse($systemEmail->isSent());
        
        $this->mailListener->sendPerformed(clone $event);
        $this->assertSame(SystemEmail::STATUS_SENT, $systemEmail->getStatus());
        $this->assertSame(1, $systemEmail->getAttempts());
        $this->assertTrue($systemEmail->isSent());
    }
    
    private function glob_recursive($pattern, $flags = 0)
    {
        $files = glob($pattern, $flags);
        foreach (glob(dirname($pattern).'/*', GLOB_ONLYDIR|GLOB_NOSORT) as $dir) {
            $files = array_merge($files, $this->glob_recursive($dir.'/'.basename($pattern), $flags));
        }

        return $files;
    }
}
