<?php
namespace Wanawork\UserBundle\Tests;

use Wanawork\UserBundle\Entity\User;
use Wanawork\UserBundle\Entity\Role;
use Wanawork\UserBundle\Tests\Entity\WebUnitTestBase;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class ACLTest extends WebUnitTestBase
{
    public function testThatIsGrantedAccountsForGroup()
    {
        $user = new User();
        $role = new Role($name = 'test_role', 'ROLE_1_EMPLOYER');        
        $user->addRole($role);
        
        $client = self::createClient();
        $client->request('GET', '/');
        
        $container = $client->getContainer();
        $security = $container->get('security.context');
        
        $token = new UsernamePasswordToken($user, null, 'secured_area', $user->getRoles());
        $security->setToken($token);
        
        $event = new InteractiveLoginEvent($client->getRequest(), $token);
        $container->get("event_dispatcher")->dispatch("security.interactive_login", $event);
        
        $this->assertTrue($security->isGranted('ROLE_1_EMPLOYER'));
        $this->assertFalse($security->isGranted('ROLE_2_EMPLOYER'));
    }
    
    
}