<?php
namespace Wanawork\UserBundle\Tests\Util;

use Wanawork\UserBundle\Tests\Entity\WebUnitTestBase;
use Wanawork\UserBundle\Util\Date;

class UtilTest extends WebUnitTestBase
{
    
    public function testStartDateGreaterThanFinishDateReturnEmptyString()
    {
        $startDate = new \DateTime('2012-12-01');
        $finishDate = new \DateTime('2012-11-01');
        $this->assertSame('', Date::diffInMonthsAndYears($finishDate, $startDate));
    }
    
    public function testSameDates()
    {
        $startDate = new \DateTime('2012-12-01');
        $finishDate = new \DateTime('2012-12-01');
        $this->assertSame('Less than a month', Date::diffInMonthsAndYears($finishDate, $startDate));
    }
    
    public function testMonthsOnly()
    {
        $startDate = new \DateTime('2012-11-01');
        $finishDate = new \DateTime('2012-12-01');
        $this->assertSame('1 month', Date::diffInMonthsAndYears($finishDate, $startDate));
        
        $startDate = new \DateTime('2012-10-01');
        $finishDate = new \DateTime('2012-12-01');
        $this->assertSame('2 months', Date::diffInMonthsAndYears($finishDate, $startDate));
        
        $startDate = new \DateTime('2012-12-01');
        $finishDate = new \DateTime('2013-01-01');
        $this->assertSame('1 month', Date::diffInMonthsAndYears($finishDate, $startDate));
    }
    
    public function testYearsOnly()
    {
        $startDate = new \DateTime('2012-12-01');
        $finishDate = new \DateTime('2013-12-01');
        $this->assertSame('1 year', Date::diffInMonthsAndYears($finishDate, $startDate));
        
        $startDate = new \DateTime('2012-12-01');
        $finishDate = new \DateTime('2014-12-01');
        $this->assertSame('2 years', Date::diffInMonthsAndYears($finishDate, $startDate));
    }
    
    public function testMonthsAndYears()
    {
        $startDate = new \DateTime('2012-11-01');
        $finishDate = new \DateTime('2013-12-01');
        $this->assertSame('1 month, 1 year', Date::diffInMonthsAndYears($finishDate, $startDate));
        
        $startDate = new \DateTime('2012-10-01');
        $finishDate = new \DateTime('2013-12-01');
        $this->assertSame('2 months, 1 year', Date::diffInMonthsAndYears($finishDate, $startDate));
        
        $startDate = new \DateTime('2012-11-01');
        $finishDate = new \DateTime('2014-12-01');
        $this->assertSame('1 month, 2 years', Date::diffInMonthsAndYears($finishDate, $startDate));
        
        $startDate = new \DateTime('2012-10-01');
        $finishDate = new \DateTime('2014-12-01');
        $this->assertSame('2 months, 2 years', Date::diffInMonthsAndYears($finishDate, $startDate));
    }
    
    public function testDaysAreDisregarded()
    {
        $startDate = new \DateTime('2012-10-01');
        $finishDate = new \DateTime('2014-12-01');
        $this->assertSame('2 months, 2 years', Date::diffInMonthsAndYears($finishDate, $startDate));
        
        $startDate = new \DateTime('2012-10-05');
        $finishDate = new \DateTime('2014-12-12');
        $this->assertSame('2 months, 2 years', Date::diffInMonthsAndYears($finishDate, $startDate));
        
    }
}