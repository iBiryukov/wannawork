<?php
namespace Wanawork\UserBundle\Util;

class Date
{
    
    private function __construct(){}
    
    public static function diffInMonthsAndYears(\DateTime $finishDate, \DateTime $startDate)
    {
        $finishYear = $finishDate->format('Y');
        $finishMonth = $finishDate->format('n');
        
        $startYear = $startDate->format('Y');
        $startMonth = $startDate->format('n');
        
        $totalDurationMonths = $finishMonth - $startMonth + (($finishYear - $startYear) * 12);
        $durationYears = floor($totalDurationMonths / 12);
        $durationMonths = $totalDurationMonths % 12;
        
        $text = array();
        if ($totalDurationMonths < 0) {
            $text[] = '';
        } elseif ($totalDurationMonths === 0) {
            $text[] = 'Less than a month';
        } else {
            if ($durationMonths > 0) {
                $text[] = ("{$durationMonths} month") . ($durationMonths > 1 ? 's' : '');
            }
        
            if ($durationYears > 0) {
                $text[] = ("{$durationYears} year") . ($durationYears > 1 ? 's' : '');
            }
        }
        
        return implode(', ', $text);
    }
    
}