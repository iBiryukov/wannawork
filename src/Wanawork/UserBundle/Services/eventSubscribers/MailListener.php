<?php
namespace Wanawork\UserBundle\Services\eventSubscribers;

use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use Wanawork\UserBundle\Entity\SystemEmail;

/**
 * Listens to new emails being sent and records the outcome
 * 
 * @author Ilya Biryukov <ilya@wannawork.ie>
 */
class MailListener implements \Swift_Events_SendListener
{

    /**
     * Entity Manager
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * Logger
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * Constructor
     * @param EntityManager $em
     * @param LoggerInterface $logger
     */
    public function __construct(EntityManager $em, LoggerInterface $logger)
    {
    	$this->em = $em;
    	$this->logger = $logger;
    }
    
    /**
     * (non-PHPdoc)
     * @see Swift_Events_SendListener::beforeSendPerformed()
     */
    public function beforeSendPerformed(\Swift_Events_SendEvent $event)
    {
        $message = $event->getMessage();
        $messageId = $message->getHeaders()->has('wanawork-email-id') ? 
            $message->getHeaders()->get('wanawork-email-id')->getValue() : null;
        
        if (!preg_match('/^\w+$/', $messageId)) {
            if ($messageId !== null) {
                $this->logger->error("Got invalid message ID in email pre-send", array(
                    'messageId' => $messageId,
                    'subject' => $message->getSubject(),
                    'recepient' => $message->getTo(),
                ));
            }
        } else {
            try {
                $this->em->getConnection()->beginTransaction();
                $systemEmail = $this->em->getRepository('WanaworkUserBundle:SystemEmail')->find($messageId);
                if ($systemEmail instanceof SystemEmail) {
                    if ($event->getTransport() instanceof \Swift_Transport_SpoolTransport) {
                        $systemEmail->setStatus(SystemEmail::STATUS_QUEUEING);
                    } else {
                        $systemEmail->setStatus(SystemEmail::STATUS_SENDING);
                        $systemEmail->increaseAttempts();
                    }
                    
                    $this->em->flush($systemEmail);
                    
                } else {
                    $this->logger->error("Unable to find email in pre-send email event", array(
                        'messageId' => $messageId,
                        'subject' => $message->getSubject(),
                        'recepient' => $message->getTo(),
                        'trace' => debug_backtrace(),
                    ));
                }
                $this->em->getConnection()->commit();
            } catch (\Exception $e) {
                $this->em->getConnection()->rollBack();
                $this->logger->alert("Exception in pre-send email event", array(
                    'exception' => $e->__toString(),
                    'messageId' => $messageId,
                    'subject' => $message->getSubject(),
                    'recepient' => $message->getTo(),
                ));
            }
        }
    }

    /**
     * (non-PHPdoc)
     * @see Swift_Events_SendListener::sendPerformed()
     */
    public function sendPerformed(\Swift_Events_SendEvent $event)
    {
        $message = $event->getMessage();
        $messageId = $message->getHeaders()->has('wanawork-email-id') ? 
            $message->getHeaders()->get('wanawork-email-id')->getValue() : null;
        
        if (!preg_match('/^\w+$/', $messageId)) {
            if ($messageId !== null) {
                $this->logger->error("Got invalid message ID in email post-send event", array(
                    'messageId' => $messageId,
                    'subject' => $message->getSubject(),
                    'recepient' => $message->getTo(),
                ));
            }
            return false;
        }
        
        try {
            $this->em->getConnection()->beginTransaction();
            
            $systemEmail = $this->em->getRepository('WanaworkUserBundle:SystemEmail')->find($messageId);
            if ($systemEmail instanceof SystemEmail) {
                switch ($event->getResult()) {
                	case \Swift_Events_SendEvent::RESULT_FAILED:
                	    $systemEmail->setStatus(SystemEmail::STATUS_FAILED);
                	    break;
                	case \Swift_Events_SendEvent::RESULT_PENDING:
                	    $systemEmail->setStatus(SystemEmail::STATUS_SENDING);
                	    break;
                	case \Swift_Events_SendEvent::RESULT_TENTATIVE:
                	    // @todo Not sure how to implement this
                	    // happens to messages with multiple recepients (when some fails). We don't do that currently
                	    $this->logger->error("Got tentatative result in post-send email event", array(
                	        'messageId' => $messageId,
                	        'subject' => $message->getSubject(),
                	        'recepient' => $message->getTo(),
                	    ));
                	    $systemEmail->setStatus(SystemEmail::STATUS_SENT);
                	    $this->postProcessSentEmail($systemEmail);
                	    break;
                	case \Swift_Events_SendEvent::RESULT_SUCCESS:
                	    if ($event->getTransport() instanceof \Swift_Transport_SpoolTransport) {
                	        $systemEmail->setStatus(SystemEmail::STATUS_QUEUED);
                	    } else {
                	        $systemEmail->markSent();
                	        $this->postProcessSentEmail($systemEmail);
                	    }
                	    break;
                	default:
                	    $this->logger->alert("Got uknown result in post-send email event", array(
                    	    'messageId' => $messageId,
                    	    'result' => $event->getResult(),
                	    ));
                	    break;
                }
                $this->em->flush($systemEmail);
            } else {
                $this->logger->error("Unable to find email in post-send email event", array(
                    'messageId' => $messageId,
                    'subject' => $message->getSubject(),
                    'recepient' => $message->getTo(),
                ));
            }
            
            $this->em->getConnection()->commit();
        } catch (\Exception $e) {
            $this->em->getConnection()->rollBack();
            $this->logger->alert("Exception in post-send email event", array(
                'exception' => $e->__toString(),
                'messageId' => $messageId,
                'subject' => $message->getSubject(),
                'recepient' => $message->getTo(),
            ));
        }
    }
    
    private function postProcessSentEmail(SystemEmail $systemEmail)
    {
        if ($systemEmail->getType() === SystemEmail::TYPE_ADM_EMPLOYER_REG) {
            $text = $systemEmail->getText();
            $text = preg_replace('/Password: ".*"/', 'Password: "Redacted from storage"', $text);
            $text = preg_replace('/<a.*id="login\-link".*>.*<\/a>/', 'click here (link removed)', $text);
            $systemEmail->setText($text);
        }
    }
    
}