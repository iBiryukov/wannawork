<?php
namespace Wanawork\UserBundle\Services\eventSubscribers;

use Doctrine\Common\EventSubscriber;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Wanawork\UserBundle\Entity\EmployeeProfile;
use Wanawork\UserBundle\Entity\CV;
use Wanawork\UserBundle\Entity\Ad;
use Doctrine\ORM\Event\PreFlushEventArgs;

/**
 * Listens to changes to ads and related data
 * and removes the PDF of the Ads when required
 * @author Ilya Biryukov <ilya@wanawork.ie>
 */
class AdPdfSubscriber implements EventSubscriber
{
    /**
     * Logger
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;
    
    private $pdfStoragePath;
    
    public function __construct(LoggerInterface $logger, $pdfStoragePath)
    {
        $this->logger = $logger; 
        $this->pdfStoragePath = $pdfStoragePath; 
    }
    
    public function preUpdate(PreUpdateEventArgs $eventArgs)
    {
        $changeset = $eventArgs->getEntityChangeSet();

        if(sizeof($changeset)) {
            $entity = $eventArgs->getEntity();
            
            $ads = array();
            if($entity instanceof EmployeeProfile) {
                $ads = $entity->getAds();
            } elseif($entity instanceof CV) {
                $ads = $entity->getAds();
            } elseif($entity instanceof Ad) {
                $ads[] = $entity;
            }
            
            foreach($ads as $ad) {
                if($ad instanceof Ad && !$ad->isPDFGenerated()) {
                    if($ad->getPdfPath() !== null && is_file($this->pdfStoragePath . $ad->getPdfPath())) {
                        unlink($this->pdfStoragePath . $ad->getPdfPath());
                    }
                }
            }
        }
    }
    
    public function getSubscribedEvents()
    {
        return array(
            'preUpdate',
            'preFlush'             
        );        
    }
}
