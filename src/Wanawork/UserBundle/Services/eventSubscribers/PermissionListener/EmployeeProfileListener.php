<?php
namespace Wanawork\UserBundle\Services\eventSubscribers\PermissionListener;

use Wanawork\UserBundle\Services\eventSubscribers\PermissionListener\PermissionListener;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Wanawork\UserBundle\Entity\EmployeeProfile;

class EmployeeProfileListener extends PermissionListener
{
    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if (!$entity instanceof EmployeeProfile) {
            return;
        }
    
        $this->getLogger()->info("Granting Operator Permissions to Employee Profile", array(
            'EmployeeProfile' => $entity->getId(),
            'user' => $entity->getUser()->getId(),
        ));
        
        $this->getAclManager()->grant($entity, MaskBuilder::MASK_OPERATOR, $entity->getUser());
    }
}