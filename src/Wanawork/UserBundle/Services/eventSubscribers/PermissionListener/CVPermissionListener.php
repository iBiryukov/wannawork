<?php
namespace Wanawork\UserBundle\Services\eventSubscribers\PermissionListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Wanawork\UserBundle\Entity\CV;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
class CVPermissionListener extends PermissionListener
{
    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if (!$entity instanceof CV) {
            return;
        }
        
        $this->getLogger()->info("Granting Operator Permissions to CV", array(
        	'cv' => $entity->getId(),
            'user' => $entity->getProfile()->getUser()->getId(),
        ));
        $this->getAclManager()->grant($entity, MaskBuilder::MASK_OPERATOR, $entity->getProfile()->getUser());
    }
}