<?php
namespace Wanawork\UserBundle\Services\eventSubscribers\PermissionListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Wanawork\UserBundle\Entity\Ad;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
class AdPermissionListener extends PermissionListener
{
    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if (!$entity instanceof Ad) {
            return;
        }
    
        $this->getLogger()->info("Granting Operator Permissions to Ad", array(
            'ad' => $entity->getId(),
            'user' => $entity->getProfile()->getUser()->getId(),
        ));
        
        $this->getAclManager()->grant($entity, MaskBuilder::MASK_OPERATOR, $entity->getProfile()->getUser());
    }
}