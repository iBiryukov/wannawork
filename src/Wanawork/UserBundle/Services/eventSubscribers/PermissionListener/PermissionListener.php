<?php
namespace Wanawork\UserBundle\Services\eventSubscribers\PermissionListener;
use Wanawork\MainBundle\Security\AclManager;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Listens to post persist event on doctrine and generates appropriate permissions
 * @author Ilya Biryukov <ilya@wannawork.ie>
 */
abstract class PermissionListener
{
    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    private $container;
    
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }
    
    /**
     * 
     * @return \Symfony\Component\DependencyInjection\ContainerInterface
     */
    public function getContainer()
    {
        return $this->container;
    }
    
    /**
     * @return \Wanawork\MainBundle\Security\AclManager
     */
    public function getAclManager()
    {
        $container = $this->getContainer();
        return new AclManager($container->get('security.acl.provider'), $container->get('security.context'));
    }
    
    /**
     * @return \Psr\Log\LoggerInterface
     */
    public function getLogger()
    {
        return $this->getContainer()->get('logger');
    }
    
    /**
     * @return Registry
     */
    public function getDoctrine()
    {
        return $this->getContainer()->get('doctrine');
    }
    
//     $this->aclProvider = $container->get('security.acl.provider');
//     $this->security = $container->get('security.context');
//     $this->logger = $container->get('logger');
//     $this->aclManager = new AclManager($aclProvider, $security);
    
}