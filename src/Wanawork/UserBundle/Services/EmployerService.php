<?php
namespace Wanawork\UserBundle\Services;

use Wanawork\MainBundle\Security\AclManager;
use Wanawork\UserBundle\Entity\EmployerProfile;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Wanawork\UserBundle\Entity\User;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Acl\Model\MutableAclProviderInterface;
use Wanawork\UserBundle\Entity\EmployeeProfile;
use Psr\Log\LoggerInterface;
use Wanawork\UserBundle\Entity\Role;

class EmployerService {
	
	private $aclProvider;
	
	private $em;
	
	private $security;
	
	private $logger;
	
	public function __construct(MutableAclProviderInterface $aclProvider, EntityManager $em, 
			SecurityContextInterface $security, LoggerInterface $logger) {
		$this->aclProvider = $aclProvider;
		$this->em = $em;
		$this->security = $security;
		$this->logger = $logger;
	}
	
	
	public function createProfile(EmployerProfile $profile, User $user)
	{
		
		try {
			$this->em->getConnection()->beginTransaction();
			
			$profile->addUser($user, $isAdmin = true);
			
			$employerRole = $this->em->getRepository('WanaworkUserBundle:Role')->find("ROLE_EMPLOYER");
			
			if(!$employerRole instanceof Role) {
			    throw new \Exception("Unable to find employer's role");
			}
			
			$this->em->persist($profile->getAdminRole());
			$this->em->persist($profile->getUserRole());
			
			$user->addRole($employerRole);
			$user->addRole($profile->getAdminRole());
			$user->addRole($profile->getUserRole());
			
			$this->em->persist($profile);
			$this->em->flush();

			$aclProvider = $this->aclProvider;
			$aclManager = new AclManager($aclProvider, $this->security);
			$aclManager->grant($profile, MaskBuilder::MASK_OPERATOR, $profile->getAdminRole());
			$aclManager->grant($profile, MaskBuilder::MASK_VIEW, $profile->getUserRole());
			
			$this->em->flush();
			$this->em->getConnection()->commit();
		} catch (\Exception $e) {
			$this->em->getConnection()->rollBack();
			$this->logger->alert("Error while creating employer profile", array(
				'ex' => $e->__toString(),
			));
			throw $e;
		}
		return $profile;
	} 	
}
