<?php
namespace Wanawork\UserBundle\Services\billing;

use Wanawork\UserBundle\Entity\Billing\Payment;
use Wanawork\UserBundle\Entity\Billing\Order;
use Wanawork\UserBundle\Entity\Billing\AdOrder;
use Wanawork\UserBundle\Entity\Billing\VerificationOrder;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManager;

class PaymentService
{
    private $logger;
    
    private $em;
    
    public function __construct(EntityManager $entityManager, LoggerInterface $logger)
    {
        $this->em = $entityManager;
        $this->logger = $logger;
    }
    
    public function processPayment(Order $order, Payment $payment)
    {
        if ($order instanceof AdOrder) {
            $this->publishAd($order, $payment);
        } elseif($order instanceof VerificationOrder) {
            $this->recordVerification($order, $payment);   
        }
    }
    
    public function recordVerification(VerificationOrder $order, Payment $payment)
    {
        // nothing to do really. 
        // Lets keep for this for future
    }
    
    public function publishAd(AdOrder $order, Payment $payment)
    {
       $order->getAd()->publishPremium($order);
       $this->em->flush();
    }
    
}