<?php
namespace Wanawork\UserBundle\Services\billing;

use Wanawork\UserBundle\Entity\Billing\Order;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use Wanawork\UserBundle\Services\MailService;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Wanawork\UserBundle\Entity\Billing\AdOrder;
use Wanawork\UserBundle\Entity\Billing\VerificationOrder;
use Knp\Snappy\GeneratorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ReceiptService
{
    /**
     * Entity Manager
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    
    /**
     * Logger
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;
    
    /**
     * Mail service
     * @var \Wanawork\UserBundle\Services\MailService
     */
    private $mail;
    
    /**
     * Twig
     * @var \Symfony\Bundle\FrameworkBundle\Templating\EngineInterface
     */
    private $twig;
    
    /**
     * PDF generator
     * @var \Knp\Snappy\GeneratorInterface
     */
    private $pdfService;
    
    /**
     * Where receipts are stored
     * @var string
     */
    private $receiptFolder;
    
    public function __construct(EntityManager $em, LoggerInterface $logger, EngineInterface $twig, MailService $mail, 
        GeneratorInterface $pdfService, $receiptFolder)
    {
        $this->em = $em;
        $this->logger = $logger;
        $this->mail = $mail;
        $this->twig = $twig;
        $this->pdfService = $pdfService;
        $this->receiptFolder = $receiptFolder;
    }
    
    /**
     * Generate a receipt for the order
     * @param Order     $order
     * @param boolean   $rewrite
     * 
     * @return string Path to receipt file
     */
    public function generateReceipt(Order $order, $rewrite = false)
    {
        $receiptPath = $this->receiptFolder . $order->getReceiptFileName() . '.pdf';
        
        if ($rewrite === true && is_file($receiptPath)) {
            unlink($receiptPath);
        }
        
        if (!is_file($receiptPath)) {
            $templateSuffix = null;
            
            if ($order instanceof AdOrder) {
                $templateSuffix = 'ad';
            } elseif ($order instanceof VerificationOrder) {
                $templateSuffix = 'verification';
            } else {
                throw new \Exception(
                    sprintf(
                        "Unknown order type specified: '%s'",
                        get_class($order)
                    )
                );
            }
            
            $template = $this->twig->render("WanaworkUserBundle:Payment:receipt_{$templateSuffix}.html.twig", array(
                'order' => $order,
            ));
            
            $this->pdfService->generateFromHtml($template, $receiptPath);
        }
    
        return $receiptPath;
    }
}