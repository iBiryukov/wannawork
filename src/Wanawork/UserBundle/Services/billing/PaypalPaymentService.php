<?php
namespace Wanawork\UserBundle\Services\billing;

use Wanawork\UserBundle\Entity\Billing\exception\PaymentAlreadyProcessedException;
use Wanawork\UserBundle\Entity\Billing\exception\ApprovalRequiredException;
use Wanawork\UserBundle\Entity\Billing\Order;
use Wanawork\UserBundle\Entity\Billing\Payment as WanaworkPayment;
use Wanawork\UserBundle\Entity\Billing\Transaction as WanaworkTransaction;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Address;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Payer;
use PayPal\Api\Payment as PaypalPayment;
use PayPal\Api\FundingInstrument;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Api\ItemList;
use PayPal\Api\Item;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Sale;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use Wanawork\UserBundle\Entity\Billing\Payment;



class PaypalPaymentService
{
    const PAYMENT_STATE_CREATED = 'created';
    const PAYMENT_STATE_APPROVED = 'approved';
    const PAYMENT_STATE_FAILED = 'failed';
    const PAYMENT_STATE_PENDING = 'pending';
    const PAYMENT_STATE_CANCELED = 'canceled';
    const PAYMENT_STATE_EXPIRED = 'expired';
    
    const SALE_STATE_PENDING = 'pending';
    const SALE_STATE_COMPLETED = 'completed';
    const SALE_STATE_REFUNDED = 'refunded';
    const SALE_STATE_PART_REFUNDED = 'partially_refunded';
    
    private $clientId;
    
    private $secret;
    
    private $paypalApiOptions;
    
    /**
     * URL To return to after successful transaction
     * @var string
     */
    private $successUrl;
    
    /**
     * URL to return after unsuccessful transaction
     * @var string
     */
    private $cancelUrl;
    
    /**
     * Entity Manager
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    
    /**
     * Logger
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;
    
    public function __construct(EntityManager $em, LoggerInterface $logger, 
        $successUrl, $cancelUrl, $clientId, $secret, array $paypalOptions)
    {
        $this->em = $em;
        $this->logger = $logger;
        $this->successUrl = $successUrl;
        $this->cancelUrl = $cancelUrl;
        $this->clientId = $clientId;
        $this->secret = $secret;
        $this->paypalApiOptions = $paypalOptions;
        
        if ($paypalOptions['log.log_enabled'] && isset($paypalOptions['log.file_name'])) {
            if(!file_exists($paypalOptions['log.file_name'])) {
                $fp = fopen($paypalOptions['log.file_name'], 'w');
                if($fp) {
                    fclose($fp);
                } else {
                    throw new \Exception(
                        sprintf('Unable to create paypal log file at %s', $paypalOptions['log.file_name'])
                    );
                }
            }
        }
    }

    public function getPaypalApiOptions()
    {
        return $this->paypalApiOptions;
    }
    
    /**
     * Create API Context getting paypal token
     * @return \PayPal\Rest\ApiContext
     */
    private function createContext()
    {
        static $context = null;
        
        if ($context === null) {
            $clientId = $this->clientId;
            $secret = $this->secret;
            $paypalApiOptions = $this->paypalApiOptions;
            
            $apiContext = new ApiContext(new OAuthTokenCredential($clientId, $secret));
            $apiContext->setConfig(array(
                'mode' => $paypalApiOptions['service.mode'],
                'http.ConnectionTimeOut' => $paypalApiOptions['http.connection_timeout'],
                'log.LogEnabled' => $paypalApiOptions['log.log_enabled'],
                'log.FileName' => $paypalApiOptions['log.file_name'],
                'log.LogLevel' => $paypalApiOptions['log.log_level']
            ));
            
            $context = $apiContext;
        }
        return $apiContext;
    }
    
    /**
     * Create a new payment with paypal
     * @param Order $order
     * @throws Exception
     * @return \Wanawork\UserBundle\Entity\Billing\Transaction
     */
    public function createPayment(Order $order)
    {
        $this->logger->info("Creating a new order", array(
        	'description' => $order->getInternalDescription(),
            'amount' => $order->getAmount(),
            'vat_rate' => $order->getVatRate(),
        ));
        
        $em = $this->em;
        try {
            $em->getConnection()->beginTransaction();
            $wanaworkPayment = new WanaworkPayment($order, $order->getAmountIncludingVat(), WanaworkPayment::SOURCE_PAYPAL);
            $em->persist($order);
            $em->persist($wanaworkPayment);
            $em->flush();
            $this->em->getConnection()->commit();
            
        } catch (Exception $e) {
            $em->getConnection()->rollBack();
            $this->logger->alert("Unable to create payment record", array(
            	'exception' => $e->getMessage(),
            ));
            throw $e;
        }
        
        
        $items = array();
        foreach ($order->getItems() as $item) {
            $paypalItem = new Item();
            $paypalItem->setName($item->getName())
            ->setCurrency('EUR')
            ->setQuantity($item->getQuantity())
            ->setPrice($item->getPrice() / 100);
            $items[] = $paypalItem;
        }
        
        $itemList = new ItemList();
        $itemList->setItems($items);
        
        $payer = new Payer();
        $payer->setPayment_method("paypal");
    
        $amount = new Amount();
        $amount->setCurrency('EUR');
        $amount->setTotal($order->getAmountIncludingVat() / 100);
        
        $paypalDetails = new Details();
        $paypalDetails->setSubtotal($order->getAmountExcludingVat() / 100);
        $paypalDetails->setTax($order->getVat() / 100);
        $amount->setDetails($paypalDetails);
    
        $paypalTran = new Transaction();
        $paypalTran->setAmount($amount)->setItemList($itemList);
    
        $successUrl = $this->successUrl;
        $successUrl .= (strpos($successUrl, '?') === false) ?
            "?payment={$wanaworkPayment->getId()}" : "&payment={$wanaworkPayment->getId()}";
        
        $cancelUrl = $this->cancelUrl;
        $cancelUrl .= (strpos($cancelUrl, '?') === false) ?
            "?payment={$wanaworkPayment->getId()}" : "&payment={$wanaworkPayment->getId()}";
        
        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturn_url($successUrl);
        $redirectUrls->setCancel_url($cancelUrl);
    
        $payment = new PaypalPayment();
        $payment->setIntent("sale");
        $payment->setPayer($payer);
        $payment->setRedirect_urls($redirectUrls);
        $payment->setTransactions(array($paypalTran));
    
        $wanaworkTransaction = new WanaworkTransaction($wanaworkPayment);
        $wanaworkTransaction->setType(WanaworkTransaction::TRANSACTION_TYPE_CREATE);
        
        try {
            $apiContext = $this->createContext();
            $payment->create($apiContext);
    
            $wanaworkTransaction->setData('raw', $payment->toJSON());
            $wanaworkTransaction->setData('state', $payment->getState());
            if ($payment->getState() !== self::PAYMENT_STATE_CREATED) {
                $wanaworkTransaction->setState(WanaworkTransaction::STATE_FAIL);
                $wanaworkPayment->setState(WanaworkPayment::STATE_FAILED);
                $order->setStatus(Order::STATUS_FAILED);
                $wanaworkTransaction->setFailReason("Unable to approve the transaction");
                $wanaworkTransaction->setData('fail-data', $payment->toJSON());
                $this->logger->alert("Unable to approve the trasaction", array(
                	'payment_data' => $payment->toArray(),
                    'order' => $order->getId(),
                    'payment' => $payment->getId(),
                ));
            } else {
                $wanaworkTransaction->setState(WanaworkTransaction::STATE_SUCCESS);
                $wanaworkTransaction->setTrackingId($payment->getId());
                $wanaworkTransaction->setData('create_time', $payment->getCreateTime());
                $wanaworkTransaction->setData('update_time', $payment->getUpdateTime());
                
                foreach($payment->getLinks() as $link) {
                    if($link->getRel() === 'approval_url') {
                        $wanaworkTransaction->setData('approval_url', $link->getHref());
                    }
                }
            }
            
        } catch (\PayPal\Exception\PPConnectionException $ex) {
            if ($payment->__isset('state')) {
                $wanaworkTransaction->setData('state', $payment->getState());
            }
            
            $this->logger->alert("Unable to approve the trasaction", array(
                'payment_data' => $payment->toArray(),
                'order' => $order->getId(),
                'payment' => $payment->getId(),
                'exception_data' => $ex->getData(),
                'exception' => $ex->__toString(),
                'exception-message' => $ex->getMessage(),
            ));
            
            $wanaworkTransaction->setData('ex', $ex->getData());
            $wanaworkTransaction->setState(WanaworkTransaction::STATE_FAIL);
            $wanaworkTransaction->setData('raw', $payment->toJSON());
            $wanaworkTransaction->setFailReason("Unable to approve the transaction");
            $wanaworkPayment->setState(WanaworkPayment::STATE_FAILED);
            $order->setStatus(Order::STATUS_FAILED);
        }
        $em->persist($wanaworkTransaction);
        $em->flush();
        return $wanaworkTransaction;
    }
    
    public function approvePayment(WanaworkPayment $wanaworkPayment, $payerId, $success)
    {
        $hasApproveTransaction = false;
        foreach ($wanaworkPayment->getTransactions() as $transaction) {
            if($transaction->getType() === WanaworkTransaction::TRANSACTION_TYPE_APPROVE) {
                $hasApproveTransaction = true;
                break;
            }
        }
        
        if (!$hasApproveTransaction) {
            $approveTransaction = new WanaworkTransaction($wanaworkPayment);
            $approveTransaction->setType(WanaworkTransaction::TRANSACTION_TYPE_APPROVE);
            $approveTransaction->setState(
            	$success ? WanaworkTransaction::STATE_SUCCESS : WanaworkTransaction::STATE_FAIL
            );
            $approveTransaction->setTrackingId($payerId);
            $this->em->persist($approveTransaction);
            $this->em->flush();
        }
    }
    
    public function depositPayment(WanaworkPayment $wanaworkPayment, $payerId, $success)
    {
        if ($wanaworkPayment->getState() === WanaworkPayment::STATE_CLEARING) {
            return $this->checkClearingSale($wanaworkPayment);    
        }
        
        if($wanaworkPayment->getState() !== WanaworkPayment::STATE_PENDING) {
            throw new PaymentAlreadyProcessedException('Payment was already processed');
        }
        
        $this->logger->info("Depositing payment", array(
        	'payment' => $wanaworkPayment->getId(),
        ));
        
        $createTransaction = $wanaworkPayment->getCreateTransaction();
        if ($createTransaction === null) {
            $this->logger->critical("Attempted to deposit payment without 'create' transaction", array(
                'payment' => $wanaworkPayment->getId(),
            ));
            throw new \Exception("The payment was not created. Cannot deposit");
        }
        
        $this->approvePayment($wanaworkPayment, $payerId, $success);

        $wanaworkOrder = $wanaworkPayment->getOrder();
        $depositTransaction = new WanaworkTransaction($wanaworkPayment);
        $depositTransaction->setType(WanaworkTransaction::TRANSACTION_TYPE_DEPOSIT);
        
        $this->em->getConnection()->beginTransaction();
        try {
        
            $completedPaymentState = null;
            $completedPaymentJson = null;
            
            // payment approved?
            if($success === true) {
                $apiContext = $this->createContext();
                $paypalPayment = PaypalPayment::get($createTransaction->getTrackingId(), $apiContext);
                
                $execution = new PaymentExecution();
                $execution->setPayer_id($payerId);
                $completedPayment = $paypalPayment->execute($execution, $apiContext);
                
                $completedPaymentState = $completedPayment->getState();
                $completedPaymentJson = $completedPayment->toJSON();
                
                $depositTransaction->setData('state', $completedPaymentState);
                $depositTransaction->setData('create_time', $completedPayment->getCreateTime());
                $depositTransaction->setData('update_time', $completedPayment->getUpdateTime());
                $depositTransaction->setData('raw', $completedPaymentJson);
            } else {
                $completedPaymentState = self::PAYMENT_STATE_CANCELED;
            }
            
            switch($completedPaymentState) {
            	case self::PAYMENT_STATE_APPROVED:
            	    
            	    $depositTransaction->setState(WanaworkTransaction::STATE_SUCCESS);
            	    
            	    $saleId = $this->extractSaleId($completedPaymentJson);
            	    if ($saleId !== null) {
            	        $depositTransaction->setTrackingId($saleId);
            	    } else {
            	        $this->logger->warning('Deposited payment missing sale information', array(
            	            'payment' => $wanaworkPayment->getId(),
            	        ));
            	    }
            	    
            	    $wanaworkPayment->setState(WanaworkPayment::STATE_DEPOSITED);
            	    $wanaworkOrder->setStatus(Order::STATUS_PAID);
            	    break;
            	    
            	case self::PAYMENT_STATE_CANCELED:
            	    $depositTransaction->setState(WanaworkTransaction::STATE_FAIL);
            	    $depositTransaction->setFailReason('Your payment was cancelled');
            	    $wanaworkPayment->setState(WanaworkPayment::STATE_CANCELED);
            	    
            	    $this->logger->info('Payment cancelled', array(
            	        'payment' => $wanaworkPayment->getId(),
            	    ));
            	    $wanaworkOrder->setStatus(Order::STATUS_CANCELLED);
            	    break;
            	    
            	case self::PAYMENT_STATE_CREATED:
            	    $this->logger->notice('Payment created and requires approval', array(
            	        'payment' => $wanaworkPayment->getId(),
            	    ));

            	    throw new ApprovalRequiredException($createTransaction->getDataByKey('approval_url'));
            	    break;
            	    
            	case self::PAYMENT_STATE_EXPIRED:
            	    $depositTransaction->setState(WanaworkTransaction::STATE_FAIL);
            	    $wanaworkPayment->setState(WanaworkPayment::STATE_EXPIRED);
            	    $depositTransaction->setFailReason('Your payment expired');
            	    $wanaworkOrder->setStatus(Order::STATUS_FAILED);
            	    $this->logger->info('Payment expired', array(
            	        'payment' => $wanaworkPayment->getId(),
            	    ));
            	    break;
            	    
            	case self::PAYMENT_STATE_FAILED:
            	    $depositTransaction->setState(WanaworkTransaction::STATE_FAIL);
            	    $depositTransaction->setFailReason('Your payment failed due to unknown reason');
            	    $wanaworkPayment->setState(WanaworkPayment::STATE_FAILED);
            	    $wanaworkOrder->setStatus(Order::STATUS_FAILED);
            	    $this->logger->warning('Payment failed', array(
            	        'payment' => $wanaworkPayment->getId(),
            	    ));
            	    break;
            	    
            	case self::PAYMENT_STATE_PENDING:
            	    $depositTransaction->setState(WanaworkTransaction::STATE_PENDING);
            	    $wanaworkPayment->setState(WanaworkPayment::STATE_CLEARING);
            	    
            	    $saleId = $this->extractSaleId($completedPaymentJson);
            	    if ($saleId !== null) {
            	        $depositTransaction->setTrackingId($saleId);
            	    } else {
            	        $this->logger->warning('Pending payment missing sale information', array(
            	            'payment' => $wanaworkPayment->getId(),
            	        ));
            	    }
            	    $depositTransaction->setTrackingId($saleId);
            	    
            	    $this->logger->notice('Payment pending', array(
            	        'payment' => $wanaworkPayment->getId(),
            	    ));
            	    break;
            	    
            	default:
            	    $this->logger->alert("Paypal returned unknown payment state", array(
            	    	'status' => $payment->getState(),
            	    	'payment_id' => $wanaworkPayment->getId(),
            	    ));   
            	    throw new \Exception("Unable to verify payment's status");
            	    break;
            }
            
            $this->em->flush();
            $this->em->getConnection()->commit();
             
        } catch (\PayPal\Exception\PPConnectionException $ex) {
            
            $depositTransaction->setState(WanaworkTransaction::STATE_FAIL);
            $depositTransaction->setFailReason('Your payment failed due to unknown reason');
            $wanaworkPayment->setState(WanaworkPayment::STATE_FAILED);
            $wanaworkOrder->setStatus(Order::STATUS_FAILED);
            
            $this->em->flush();
            $this->em->getConnection()->commit();
        } catch(\Exception $e) {
            $this->em->getConnection()->rollBack();
            $this->logger->alert('Failed to deposit transaction', array(
                'exception' => $e->__toString(),
                'payment' => $wanaworkPayment->getId(),
            ));
            throw $e;
        }
        
        return $depositTransaction;
    }
    
    
    public function checkClearingSale(WanaworkPayment $wanaworkPayment)
    {
        if ($wanaworkPayment->getState() !== WanaworkPayment::STATE_CLEARING) {
            throw new \Exception("The sale is not in clearing state");
        }
        
        $pendingDepositTransaction = null;
        foreach ($wanaworkPayment->getTransactions() as $transaction) {
            if ($transaction->getType() === WanaworkTransaction::TRANSACTION_TYPE_DEPOSIT && 
                $transaction->getState() === WanaworkTransaction::STATE_PENDING && 
                $transaction->getTrackingId()) {
                $pendingDepositTransaction = $transaction;
                break;
            }
        }
        
        if ($pendingDepositTransaction === null) {
            throw new \Exception("Cannot check the clearing sale. Pending deposit transaction was not found");
        }
        $saleId = $pendingDepositTransaction->getTrackingId();
        
        if (!$saleId) {
            throw new \Exception("Pending transaction is missing a sale id");
        }
        
        $em = $this->em;
        
        $checkSaleTransaction = new WanaworkTransaction($wanaworkPayment);
        $checkSaleTransaction->setType(WanaworkTransaction::TRANSACTION_TYPE_CHECK_SALE);
        $resultTransaction = $checkSaleTransaction;
        
        try {	
            $em->getConnection()->beginTransaction();
                        
            $apiContext = $this->createContext();
            $sale = Sale::get($saleId, $apiContext);
            $checkSaleTransaction->setState(WanaworkTransaction::STATE_SUCCESS);
            $checkSaleTransaction->setData('raw', $sale->toJSON());
            $checkSaleTransaction->setData('state', $sale->getState());
            
            switch($sale->getState()) {
            	case self::SALE_STATE_COMPLETED:
            	    $depositTransaction = new WanaworkTransaction($wanaworkPayment);
            	    $depositTransaction->setType(WanaworkTransaction::TRANSACTION_TYPE_DEPOSIT);
            	    $depositTransaction->setState(WanaworkTransaction::STATE_SUCCESS);
            	    $depositTransaction->setData('raw', $sale->toJSON());
            	    $depositTransaction->setTrackingId($saleId);
            	    $wanaworkPayment->setState(WanaworkPayment::STATE_DEPOSITED);
            	    $wanaworkPayment->getOrder()->setStatus(Order::STATUS_PAID);
            	    
            	    $resultTransaction = $depositTransaction;
            	    break;
            	    
            	case self::SALE_STATE_PENDING:
            	    
            	    break;
            	    
            	case self::SALE_STATE_PART_REFUNDED:
            	case self::SALE_STATE_REFUNDED:

            	    break;
            	    
            	default:
            	    $this->logger->alert('Got unknown status while checking sale status', array(
            	    	'status' => $sale->getState(),
            	    	'payment' => $wanaworkPayment->getId(),
            	    ));
            	    throw new \Exception('Unable to check sale status');
            	    break;
            	   
            }
            
            $em->flush();
            $em->getConnection()->commit();
            
        } catch (\PayPal\Exception\PPConnectionException $ex) {
            $checkSaleTransaction->setState(WanaworkTransaction::STATE_FAIL);
            $checkSaleTransaction->setFailReason('Unable to check the status of the payment');
            $checkSaleTransaction->setData('exception', $ex->getMessage());
            $checkSaleTransaction->setData('raw', $ex->getData());
            
            $em->flush(); 
            $em->getConnection()->commit();
            
            $this->logger->alert("Unable to check payment status", array(
            	'exception' => $ex->getMessage(),
                'payment' => $wanaworkPayment->getId(),
                'data' => $ex->getData(),
            ));
            
        } catch(\Exception $e) {
            $em->getConnection()->rollBack();
            
            $this->logger->alert("Exception occurred while checking payment status", array(
            	'exception' => $e->__toString(),
            ));
            throw $e;
        }
        
        return $resultTransaction;
    }
    
    private function extractSaleId($json)
    {
        $saleId = null;
        $array = json_decode($json, true);
        if(isset($array['transactions'][0]['related_resources'][0]['sale']['id'])) {
	        $saleId = $array['transactions'][0]['related_resources'][0]['sale']['id'];
        }
        return $saleId;
    }
}