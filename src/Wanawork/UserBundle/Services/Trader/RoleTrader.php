<?php
namespace Wanawork\UserBundle\Services\Trader;

use Wanawork\UserBundle\Interfaces\Trader;
use Doctrine\ORM\EntityManager;
use Wanawork\UserBundle\Entity\Role;

class RoleTrader implements Trader
{
    private $entityManager;
    
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }
    
    public function export()
    {
        $roles = $this->entityManager->getRepository('WanaworkUserBundle:Role')->findAll();
        $data = array();
        foreach ($roles as $role)
        {
            $data[] = $role->__toArray();
        }
        return $data;
    }

    public function import(\SplFileInfo $file, $callable = null)
    {
        $result = array(
            self::INSERTED => 0,
            self::DELETED  => 0,
            self::UPDATED  => 0,
            self::PROCESSED => 0,
        );
        
        $data = json_decode(file_get_contents($file->getRealPath()), $assoc = true);
        if (!is_array($data)) {
        
            throw new \Exception(
                sprintf("Unable to read the file: '%s'. Error: %s", $file->getRealPath(), json_last_error_msg())
            );
        }
        
        $roleRepository = $this->entityManager->getRepository('WanaworkUserBundle:Role');
        $ids = array();
        
        foreach ($data as $roleData) {
            $name = $roleData['name'];
            $roleName = $roleData['role'];
            $ids[] = $roleName;
        
            $role = $roleRepository->find($roleName);
            if (!$role instanceof Role) {
                $role = new Role($name, $roleName);
                $this->entityManager->persist($role);
                ++$result[self::INSERTED];
            } else {
        
                $changed = false;
                if ($name !== $role->getName()) {
                    $role->setName($name);
                    $changed = true;
                }
        
                if ($changed) {
                    ++$result[self::UPDATED];
                    $this->entityManager->persist($role);
                }
            }
        
            if ($callable !== null) {
                $callable($role);
            }
        
            ++$result[self::PROCESSED];
        }
        
        //$result[self::DELETED] = $roleRepository->deleteWhereNotInId($ids);
        $this->entityManager->flush();
        
        return $result;
    }
}