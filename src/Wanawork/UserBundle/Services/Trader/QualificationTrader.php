<?php
namespace Wanawork\UserBundle\Services\Trader;

use Wanawork\UserBundle\Interfaces\Trader;
use Doctrine\ORM\EntityManager;
use Wanawork\UserBundle\Entity\Qualification;

class QualificationTrader implements Trader
{
    private $entityManager;
    
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function export()
    {
        $qualifications = $this->entityManager->getRepository('WanaworkUserBundle:Qualification')->findAll();
        $data = array();
        foreach ($qualifications as $qualification)
        {
            $data[] = $qualification->__toArray();
        }
        
        return $data;
    }

    public function import(\SplFileInfo $file, $callable = null)
    {
        $result = array(
            self::INSERTED => 0,
            self::DELETED  => 0,
            self::UPDATED  => 0,
            self::PROCESSED => 0,
        );
        
        $data = json_decode(file_get_contents($file->getRealPath()), $assoc = true);
        if (!is_array($data)) {
        
            throw new \Exception(
                sprintf("Unable to read the file: '%s'. Error: %s", $file->getRealPath(), json_last_error_msg())
            );
        }
        
        $qualificationRepository = $this->entityManager->getRepository('WanaworkUserBundle:Qualification');
        $ids = array();
        
        foreach ($data as $qualificationData) {
            $id = $qualificationData['id'];
            $name = $qualificationData['name'];
            $isPopular = $qualificationData['isPopular'];
            $ids[] = $id;
        
            $qualification = $qualificationRepository->find($id);
            if (!$qualification instanceof Qualification) {
                $qualification = new Qualification($id, $name, $isPopular);
                $this->entityManager->persist($qualification);
                ++$result[self::INSERTED];
            } else {
        
                $changed = false;
                if ($name !== $qualification->getName()) {
                    $qualification->setName($name);
                    $changed = true;
                }
        
                if ($isPopular !== $qualification->getIsPopular()) {
                    $qualification->setOrder($isPopular);
                    $changed = true;
                }
        
                if ($changed) {
                    ++$result[self::UPDATED];
                    $this->entityManager->persist($qualification);
                }
            }
        
            if ($callable !== null) {
                $callable($qualification);
            }
        
            ++$result[self::PROCESSED];
        }
        
        $result[self::DELETED] = $qualificationRepository->deleteWhereNotInId($ids);
        $this->entityManager->flush();
        
        return $result;
    }
}