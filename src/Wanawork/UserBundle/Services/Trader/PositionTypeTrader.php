<?php
namespace Wanawork\UserBundle\Services\Trader;

use Wanawork\UserBundle\Interfaces\Trader;
use Doctrine\ORM\EntityManager;
use Wanawork\UserBundle\Entity\PositionType;

class PositionTypeTrader implements Trader
{
    private $entityManager;
    
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }
    
    public function export()
    {
        $positions = $this->entityManager->getRepository('WanaworkUserBundle:PositionType')->findAll();
        $data = array();
        foreach ($positions as $position)
        {
            $data[] = $position->__toArray();
        }
        return $data;
    }
    
    public function import(\SplFileInfo $file, $callable = null)
    {
        $result = array(
            self::INSERTED => 0,
            self::DELETED  => 0,
            self::UPDATED  => 0,
            self::PROCESSED => 0,
        );
    
        $data = json_decode(file_get_contents($file->getRealPath()), $assoc = true);
        if (!is_array($data)) {
    
            throw new \Exception(
            	   sprintf("Unable to read the file: '%s'. Error: %s", $file->getRealPath(), json_last_error_msg())
            );
        }
    
        $positionRepository = $this->entityManager->getRepository('WanaworkUserBundle:PositionType');
        $ids = array();
    
        foreach ($data as $positionTypeData) {
            $id = $positionTypeData['id'];
            $name = $positionTypeData['name'];
            $order = $positionTypeData['order'];
            $ids[] = $id;
    
            $positionType = $positionRepository->find($id);
            if (!$positionType instanceof PositionType) {
                $positionType = new PositionType($id, $name, $order);
                $this->entityManager->persist($positionType);
                ++$result[self::INSERTED];
            } else {
    
                $changed = false;
                if ($name !== $positionType->getName()) {
                    $positionType->setName($name);
                    $changed = true;
                }
    
                if ($order !== $positionType->getOrder()) {
                    $positionType->setOrder($order);
                    $changed = true;
                }
    
                if ($changed) {
                    ++$result[self::UPDATED];
                    $this->entityManager->persist($positionType);
                }
            }
    
            if ($callable !== null) {
                $callable($positionType);
            }
    
            ++$result[self::PROCESSED];
        }
    
        $result[self::DELETED] = $positionRepository->deleteWhereNotInId($ids);
        $this->entityManager->flush();
    
        return $result;
    }
}