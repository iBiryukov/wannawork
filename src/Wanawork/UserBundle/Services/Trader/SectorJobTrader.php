<?php
namespace Wanawork\UserBundle\Services\Trader;

use Wanawork\UserBundle\Interfaces\Trader;
use Doctrine\ORM\EntityManager;
use Wanawork\UserBundle\Entity\SectorJob;
use Wanawork\UserBundle\Entity\Sector;
use Gedmo\Sluggable\SluggableListener;

class SectorJobTrader implements Trader
{
    private $entityManager;
    
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }
    
    public function export()
    {
        $sectorJobs = $this->entityManager->getRepository('WanaworkUserBundle:SectorJob')->findAll();
        $data = array();
        foreach ($sectorJobs as $sectorJob)
        {
            foreach ($sectorJob->__toArray() as $k => $v) {
                $data[$k] = $v;
            }
        }
        return $data;
    }
    
    public function import(\SplFileInfo $file, $callable = null)
    {
        $result = array(
            self::INSERTED => 0,
            self::DELETED  => 0,
            self::UPDATED  => 0,
            self::PROCESSED => 0,
        );
    
        $data = json_decode(file_get_contents($file->getRealPath()), $assoc = true);
        if (!is_array($data)) {
    
            throw new \Exception(
                sprintf("Unable to read the file: '%s'. Error: %s", $file->getRealPath(), json_last_error_msg())
            );
        }
    
        $sectorJobRepository = $this->entityManager->getRepository('WanaworkUserBundle:SectorJob');
        $sectorRepository = $this->entityManager->getRepository('WanaworkUserBundle:Sector');
        
        $ids = array();
        foreach ($data as $id => $entry) {
            $ids[] = $id;
            $name = $entry['name'];
            $sectorIds = $entry['industries'];
            
            $sectors = $sectorRepository->findByIds($sectorIds);
            if(sizeof($sectors) !== sizeof($sectorIds)) {
                $foundSectorIds = array_map(function(Sector $sector){
                	return $sector->getId();
                }, $sectors);
                
                $notFoundSectorIds = array_diff($sectorIds, $foundSectorIds);
                
                throw new \Exception(
        	        sprintf(
        	            "Unable to find the following sectors '%s' in job: '%d'",
        	            implode(', ', $notFoundSectorIds),
        	            $id
                    )
                );
            }
            
            $sectorJob = $sectorJobRepository->find($id);
            if (!$sectorJob instanceof SectorJob) {
                $sectorJob = new SectorJob($id, $name, $sectors);
                $this->entityManager->persist($sectorJob);
                ++$result[self::INSERTED];
            } else {
    
                $changed = false;
                if ($name !== $sectorJob->getName()) {
                    $sectorJob->setName($name);
                    $changed = true;
                }

                // add sectors
                foreach ($sectors as $sector) {
                    if (!$sectorJob->getSectors()->contains($sector)) {
                        $sectorJob->addSector($sector);
                        $changed = true;
                    }
                }
                
                // remove sectors
                foreach ($sectorJob->getSectors() as $sector) {
                    if (!in_array($sector, $sectors)) {
                        $sectorJob->removeSector($sector);
                        $changed = true;
                    }
                }
                
                if ($sectorJob->getSlug() === null) {
                    $sectorJob->setSlug($sectorJob->getName());
                    $changed = true;
                }
                
                if ($changed) {
                    ++$result[self::UPDATED];
                    $this->entityManager->persist($sectorJob);
                }
            }
    
            if ($callable !== null) {
                $callable($sectorJob);
            }
    
            ++$result[self::PROCESSED];
        }
    
        $result[self::DELETED] = $sectorJobRepository->deleteWhereNotInId($ids);
        $this->entityManager->flush();
    
        return $result;
    }
}