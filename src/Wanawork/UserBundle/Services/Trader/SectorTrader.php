<?php
namespace Wanawork\UserBundle\Services\Trader;

use Wanawork\UserBundle\Interfaces\Trader;
use Doctrine\ORM\EntityManager;
use Wanawork\UserBundle\Entity\Sector;
use Wanawork\UserBundle\Entity\SectorJob;

class SectorTrader implements Trader
{
    private $entityManager;
    
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }
    
    public function export()
    {
        $sectors = $this->entityManager->getRepository('WanaworkUserBundle:Sector')->findAll();
        $data = array();
        foreach ($sectors as $sector)
        {
            foreach($sector->__toArray() as $k => $v) {
                $data[$k] = $v;
            }
        }
        return $data;
    }

    public function import(\SplFileInfo $file, $callable = null)
    {
        $result = array(
            self::INSERTED => 0,
            self::DELETED  => 0,
            self::UPDATED  => 0,
            self::PROCESSED => 0,
        );
        
        $data = json_decode(file_get_contents($file->getRealPath()), $assoc = true);
        if (!is_array($data)) {
        
            throw new \Exception(
                sprintf("Unable to read the file: '%s'. Error: %s", $file->getRealPath(), json_last_error_msg())
            );
        }
        
        $sectorRepository = $this->entityManager->getRepository('WanaworkUserBundle:Sector');
        $ids = array();
        
        foreach ($data as $id => $name) {
            $ids[] = $id;
        
            $sector = $sectorRepository->find($id);
            if (!$sector instanceof Sector) {
                $sector = new Sector($id, $name);
                $this->entityManager->persist($sector);
                ++$result[self::INSERTED];
            } else {
        
                $changed = false;
                if ($name !== $sector->getName()) {
                    $sector->setName($name);
                    $changed = true;
                }
        
                if ($changed) {
                    ++$result[self::UPDATED];
                    $this->entityManager->persist($sector);
                }
            }
        
            if ($callable !== null) {
                $callable($sector);
            }
        
            ++$result[self::PROCESSED];
        }
        
        $result[self::DELETED] = $sectorRepository->deleteWhereNotInId($ids);
        $this->entityManager->flush();
        
        return $result;
    }
}