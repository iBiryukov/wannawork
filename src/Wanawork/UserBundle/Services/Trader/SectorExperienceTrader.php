<?php
namespace Wanawork\UserBundle\Services\Trader;

use Wanawork\UserBundle\Interfaces\Trader;
use Doctrine\ORM\EntityManager;
use Wanawork\UserBundle\Entity\SectorExperience;

class SectorExperienceTrader implements Trader
{
    private $entityManager;
    
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }
    
    public function export()
    {
        $sectorExperiences = $this->entityManager->getRepository('WanaworkUserBundle:SectorExperience')->findAll();
        $data = array();
        foreach ($sectorExperiences as $sectorExperience)
        {
            foreach($sectorExperience->__toArray() as $k => $v) {
                $data[$k] = $v;
            }
        }
        return $data;
    }
    
    public function import(\SplFileInfo $file, $callable = null)
    {
        $result = array(
            self::INSERTED => 0,
            self::DELETED  => 0,
            self::UPDATED  => 0,
            self::PROCESSED => 0,
        );
    
        $data = json_decode(file_get_contents($file->getRealPath()), $assoc = true);
        if (!is_array($data)) {
    
            throw new \Exception(
                sprintf("Unable to read the file: '%s'. Error: %s", $file->getRealPath(), json_last_error_msg())
            );
        }
    
        $sectorExperienceRepostory = $this->entityManager->getRepository('WanaworkUserBundle:SectorExperience');
        $ids = array();
    
        foreach ($data as $id => $name) {
            $ids[] = $id;
    
            $sectorExperience = $sectorExperienceRepostory->find($id);
            if (!$sectorExperience instanceof SectorExperience) {
                $sectorExperience = new SectorExperience($id, $name);
                $this->entityManager->persist($sectorExperience);
                ++$result[self::INSERTED];
            } else {
    
                $changed = false;
                if ($name !== $sectorExperience->getName()) {
                    $sectorExperience->setName($name);
                    $changed = true;
                }
    
                if ($changed) {
                    ++$result[self::UPDATED];
                    $this->entityManager->persist($sectorExperience);
                }
            }
    
            if ($callable !== null) {
                $callable($sectorExperience);
            }
    
            ++$result[self::PROCESSED];
        }
    
        $result[self::DELETED] = $sectorExperienceRepostory->deleteWhereNotInId($ids);
        $this->entityManager->flush();
    
        return $result;
    }
}