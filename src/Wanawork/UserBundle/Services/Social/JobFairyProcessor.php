<?php
namespace Wanawork\UserBundle\Services\Social;

use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Wanawork\UserBundle\Entity\Social\Twitter\Tweet;
use Wanawork\UserBundle\Entity\Social\Twitter\User;
use Wanawork\UserBundle\Entity\Social\Twitter\JobsFairyTweet;
use Wanawork\UserBundle\Entity\SectorJob;

class JobFairyProcessor
{
    /**
     * Where the tweet files are located
     * @var string
     */
    private $path;
    
    /**
     * Entity Manager
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    
    /**
     * 
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;
    
    private $deleteProcessed = true;
    
    /**
     * @param string $path          Where the files are located
     * @param EntityManager $em
     * @param LoggerInterface $logger
     */
    public function __construct($path, EntityManager $em, LoggerInterface $logger, $deleteProcessed = true)
    {
        $this->path = $path;
        $this->em = $em;
        $this->logger = $logger;  
        $this->deleteProcessed = $deleteProcessed; 
    }
    
    public function processTweets(OutputInterface $output)
    {
        
        $output->writeln("<info>Processing Tweets</info>");
        
        if (!is_dir($this->path)) {
            $output->writeln("<error>'{$this->path}' is not a valid directory</error>");
        }
        
        $processed = 0;
        $matched = 0;
        $files = glob_recursive($this->path . '/*.json');
        foreach ($files as $file) {
            try {
                $tweet = $this->processTweet($file, $output);
                ++$processed;
                if ($tweet && sizeof($tweet->getMatchedProfessions()) > 0) {
                    ++$matched;
                }
            } catch (\Exception $e) {
                $this->logger->error($e->getMessage(), ['ex' => $e->__toString()]);
                $output->writeln('<error>' . $e->getMessage() . '</error>');
            }
        }
        
        $this->cleanupDirectories($this->path);        
        $output->writeln("Processed: {$processed}. Matched: {$matched}. Match Percentage: " . ($processed > 0 ? $matched * 100 / $processed: 0));
        
    }
    
    public function processTweet($file, OutputInterface $output)
    {
        $output->writeln("Processing file: {$file}");
        require_once 'Services/JSON.php';
        static $professions = null;
        if ($professions === null) {
            $sectorJobRepository = $this->em->getRepository('WanaworkUserBundle:SectorJob');
            $professions = $sectorJobRepository->findAll();
        }
        
        $minLengthProfession = PHP_INT_MAX;
        foreach ($professions as $profession) {
            if (strlen($profession->getName()) < $minLengthProfession) {
                $minLengthProfession = strlen($profession->getName());
            }
        }

        if (!is_readable($file)) {
            throw new \InvalidArgumentException(
	           sprintf("The file '%s' is not readable", $file)
            );
        }
        
        $fileContents = file_get_contents($file);
        $data = json_decode($fileContents, true);
        
        if (json_last_error()) {
            $jsonService = new \Services_JSON(SERVICES_JSON_LOOSE_TYPE);
            $data = $jsonService->decode($fileContents);
        }
        
        if (!$data && json_last_error()) {
            throw new \Exception(
	           sprintf("An error occured while parsing the file: '%s'. '%s'", $file, json_last_error_msg())
            );
        }
        
        if (array_key_exists('retweeted_status', $data)) {
            $output->writeln('<comment>Retweet. Skipping</comment>');
            unlink($file);
            return;
        }
        
        if (!array_key_exists('id_str', $data)) {
            throw new \Exception(sprintf("Missing attribute: '%s'", 'id_str'));
        }
        
        if (!array_key_exists('text', $data)) {
            throw new \Exception(sprintf("Missing attribute: '%s'", 'text'));
        }
        
        if (!array_key_exists('created_at', $data)) {
            throw new \Exception(sprintf("Missing attribute: '%s'", 'create_at'));
        }
        
        if (!array_key_exists('user', $data)) {
            throw new \Exception(sprintf("Missing attribute: '%s'", 'user'));
        }
        
        $userData = $data['user'];
        if (!array_key_exists('id_str', $userData)) {
            throw new \Exception(sprintf("Missing attribute: '%s'", 'user.id_str'));
        }
        
        if (!array_key_exists('name', $userData)) {
            throw new \Exception(sprintf("Missing attribute: '%s'", 'user.name'));
        }
        
        if (!array_key_exists('screen_name', $userData)) {
            throw new \Exception(sprintf("Missing attribute: '%s'", 'user.screen_name'));
        }
        
        if (!array_key_exists('profile_image_url_https', $userData)) {
            throw new \Exception(sprintf("Missing attribute: '%s'", 'user.profile_image_url_https'));
        }
        
        
        $userId = $userData['id_str'];
        $userName = $userData['name'];
        $userScreenName = $userData['screen_name'];
        $userAvatar = $userData['profile_image_url_https'];
        
        $userRepository = $this->em->getRepository('Wanawork\UserBundle\Entity\Social\Twitter\User');
        $user = $userRepository->find($userId);
        if (!$user instanceof User) {
            $user = new User($userId, $userName, $userScreenName, $userAvatar);
            $this->em->persist($user);
        }

        $id =  $data['id_str'];
        $text = $data['text'];
        $createdAt = new \DateTime($data['created_at']);
        
        $tweetRepository = $this->em->getRepository('Wanawork\UserBundle\Entity\Social\Twitter\Tweet');
        $tweet = $tweetRepository->find($id);
        
        if (!$tweet instanceof Tweet) {
            $tweet = new Tweet($id, $text, $user, $createdAt);
            $this->em->persist($tweet);
        }
        
        $jobsFairyTweetRepository = $this->em->getRepository('Wanawork\UserBundle\Entity\Social\Twitter\JobsFairyTweet');
        $jobsFairyTweet = $jobsFairyTweetRepository->find($id);
        
        if (!$jobsFairyTweet instanceof JobsFairyTweet) {
            $jobsFairyTweet = new JobsFairyTweet($tweet);
        }
        
        $matchedProfessions = [];
        
        $tweetWords = array_filter(preg_split('/[\s\/]/', $text), function($word) use($minLengthProfession){
        	return strlen($word) >= $minLengthProfession && strtolower($word) !== 'see'; 
        	// see === CEO under metaphone
        });
        
        $tweetWords = array_map(function($word){
        	return metaphone($word, 5);
        }, $tweetWords);
        $tweetWords = array_filter($tweetWords);
        
        foreach ($professions as $profession) {
            
            $variants = split('/', $profession->getName());
            
            foreach ($variants as $variant) {
                $professionWords = array_map(function($word){
                    return metaphone($word, 5);
                }, preg_split('/\s/', $variant));
                
                $intersection = array_intersect($tweetWords, $professionWords);
                if (sizeof($intersection) === sizeof($professionWords)) {
                    $matchedProfessions[] = $profession;
                    break;
                }
            }
        }
        
        $output->writeln("Found Matches: " . sizeof($matchedProfessions));
        
        if (sizeof($matchedProfessions)) {
            $this->logger->info("Matched", array(
            	'text' => $text,
                'professions' => array_map(function(SectorJob $profession){
                	return $profession->getName();
                }, $matchedProfessions),
            ));
        }
        
        $jobsFairyTweet->setProfessions($matchedProfessions);
        $this->em->persist($jobsFairyTweet);
        $this->em->flush();
        
        if ($this->deleteProcessed) {
            unlink($file);
        }
        
        return $jobsFairyTweet;
    }
    
    private function cleanupDirectories($path)
    {
        $iterator = new \DirectoryIterator($path);
        foreach ($iterator as $fsItem) {
            if ($fsItem instanceof \SplFileInfo && $fsItem->isDir() && !$fsItem->isDot()) {
                
                if(preg_match('/rop_(\d+)$/', $fsItem->getPathname(), $matches)) {
                    $rop = $matches[1];
                    if((($rop + 86400) < time()) && is_dir_empty($fsItem->getPathname())) {
                        rmdir($fsItem->getPathname());
                    }
                    
                } else {
                    $this->cleanupDirectories($fsItem->getPathname());
                }
            }
        }
        

    }
}