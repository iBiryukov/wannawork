<?php
namespace Wanawork\UserBundle\Services;

use Doctrine\Common\Persistence\ObjectManager;

class FooterService
{
    /**
     * Doctrine Registry
     * @var \Doctrine\Bundle\DoctrineBundle\Registry
     */
    private $doctrine;
    
    public function __construct(\Doctrine\Bundle\DoctrineBundle\Registry $doctrine)
    {
        $this->doctrine = $doctrine;  
    }
    
    public function getDoctrine()
    {
        return $this->doctrine;
    }
    
    /**
     * Get a list of blogs
     * @return array
     */
    public function getBlogs()
    {
        $latestBlogs = array();
        try {
            $connection = $this->getDoctrine()->getConnection('blog');
            if ($connection instanceof \Doctrine\DBAL\Connection) {
                $sql = "
                SELECT post_title, post_name
                FROM wp_posts
                WHERE
                    post_type='post'
                ORDER BY post_date DESC
                LIMIT 3";
            
                $query = $connection->query($sql);
                $latestBlogs = $query->fetchAll();
            }
        } catch (\Exception $e) {
            $this->get('logger')->error("Cannot fetch blog posts", array(
                'error' => $e->getTraceAsString(),
            ));
        }
        
        return $latestBlogs;
    }
    
    public function getProfessions()
    {
        $maxProfessions = 5;
        $professionAggregate = $this->getDoctrine()->getRepository('WanaworkUserBundle:SectorJob')->findCandidateCountByProfession();
        usort($professionAggregate, function(array $a1, array $a2){
            return $a2['ad_count'] - $a1['ad_count'];
        });
        $professions = array();
        foreach ($professionAggregate as $profession) {
            $professions[] = $profession[0];
            if (--$maxProfessions === 0) break;
        }
        
        return $professions;
    }
    
    public function getJobs()
    {
        $maxJobs = 5;
        $jobsAggregate = $this->getDoctrine()->getRepository('WanaworkUserBundle:SectorJob')->findJobsCountByProfession();
        usort($jobsAggregate, function(array $a1, array $a2){
            return $a2['job_count'] - $a1['job_count'];
        });
        $jobs = array();
        foreach ($jobsAggregate as $job) {
            if (--$maxJobs === 0) break;
            $jobs[] = $job[0];
        }
        return $jobs;
    }
}
