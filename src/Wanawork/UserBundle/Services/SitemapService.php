<?php
namespace Wanawork\UserBundle\Services;

use Symfony\Component\Routing\RouterInterface;
use Psr\Log\LoggerInterface;
use Wanawork\UserBundle\Entity\Containers\SitemapUrl;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Service that generates a sitemap
 * @author Ilya Biryukov <ilya@wannawork.ie>
 */
class SitemapService
{
    /**
     * Router
     * @var \Symfony\Component\Routing\RouterInterface
     */
    private $router;
    
    /**
     * Logger
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;
    
    /**
     * The date of the last release
     * @var \DateTime
     */
    private $releaseDate;
    
    /**
     * Doctrine Entity Manager
     * @var \Doctrine\Common\Persistence\ObjectManager
     */
    private $objectManager;
    
    public function __construct(RouterInterface $router, ObjectManager $objectManager, LoggerInterface $logger, $releaseDate)
    {
        $this->router = $router;
        $this->logger = $logger;
        $this->objectManager = $objectManager;
        
        $this->releaseDate = new \DateTime($releaseDate);
        if (!$this->releaseDate) {
            throw new \InvalidArgumentException(
                sprintf("Invalid date provided: '%s'", $releaseDate)
            );
        }
    }
    
    /**
     * @return \DateTime | null
     */
    public function getReleaseDate()
    {
        return $this->releaseDate;
    }
    
    
    public function generateMap()
    {
        $router = $this->router;
        $releaseDate = $this->getReleaseDate();
        $doctrine = $this->objectManager;
        
        $urls = [];
        
        // Main Page
        $location = $router->generate('wanawork_main_homepage', array(), RouterInterface::ABSOLUTE_URL);
        $urls[] = new SitemapUrl($location, $releaseDate, SitemapUrl::CHANGE_ALWAYS, '1.0');
        
        // Employers overview Page
        $location = $router->generate('wanawork_main_employers_overview', array(), RouterInterface::ABSOLUTE_URL);
        $urls[] = new SitemapUrl($location, $releaseDate, SitemapUrl::CHANGE_ALWAYS, '1.0');
        
        // Login Page
        $location = $router->generate('login', array(), RouterInterface::ABSOLUTE_URL);
        $urls[] = new SitemapUrl($location, $releaseDate, SitemapUrl::CHANGE_WEEKLY, '1.0');
        
        // Search
        $location = $router->generate('wanawork_main_search', array(), RouterInterface::ABSOLUTE_URL);
        $urls[] = new SitemapUrl($location, $releaseDate, SitemapUrl::CHANGE_WEEKLY, '1.0');
        
        // About us
        $location = $router->generate('wanawork_main_aboutus', array(), RouterInterface::ABSOLUTE_URL);
        $urls[] = new SitemapUrl($location, $releaseDate, SitemapUrl::CHANGE_MONTHLY, '0.3');

        // Privacy policy
        $location = $router->generate('wanawork_main_privacy_policy', array(), RouterInterface::ABSOLUTE_URL);
        $urls[] = new SitemapUrl($location, $releaseDate, SitemapUrl::CHANGE_MONTHLY, '0.6');

        // Cookie policy
        $location = $router->generate('wanawork_main_cookie_policy', array(), RouterInterface::ABSOLUTE_URL);
        $urls[] = new SitemapUrl($location, $releaseDate, SitemapUrl::CHANGE_MONTHLY, '0.6');
        
        // Contact us 
        $location = $router->generate('wanawork_main_contactus', array(), RouterInterface::ABSOLUTE_URL);
        $urls[] = new SitemapUrl($location, $releaseDate, SitemapUrl::CHANGE_MONTHLY, '1.0');
        
        // FAQ
        $location = $router->generate('wanawork_main_faq', array(), RouterInterface::ABSOLUTE_URL);
        $urls[] = new SitemapUrl($location, $releaseDate, SitemapUrl::CHANGE_WEEKLY, '0.8');
        
        // Registration
        $location = $router->generate('wanawork_register', array(), RouterInterface::ABSOLUTE_URL);
        $urls[] = new SitemapUrl($location, $releaseDate, SitemapUrl::CHANGE_WEEKLY, '1.0');
        
        // Registration Employer
        $location = $router->generate('wanawork_register', array('type' => 'employee'), RouterInterface::ABSOLUTE_URL);
        $urls[] = new SitemapUrl($location, $releaseDate, SitemapUrl::CHANGE_WEEKLY, '1.0');
        
        // Registration Employee
        $location = $router->generate('wanawork_register', array('type' => 'employer'), RouterInterface::ABSOLUTE_URL);
        $urls[] = new SitemapUrl($location, $releaseDate, SitemapUrl::CHANGE_WEEKLY, '1.0');
        
        // Restore Password
        $location = $router->generate('restore_password', array(), RouterInterface::ABSOLUTE_URL);
        $urls[] = new SitemapUrl($location, $releaseDate, SitemapUrl::CHANGE_MONTHLY, '0.3');
        
        // Product overview
        $location = $router->generate('product_overview', array(), RouterInterface::ABSOLUTE_URL);
        $urls[] = new SitemapUrl($location, $releaseDate, SitemapUrl::CHANGE_WEEKLY, '0.9');
        
        // Terms & Conditions
        $location = $router->generate('terms_and_conditions', array(), RouterInterface::ABSOLUTE_URL);
        $urls[] = new SitemapUrl($location, $releaseDate, SitemapUrl::CHANGE_WEEKLY, '0.7');
        
        // List of Professions (for jobs)
        $location = $router->generate('jobs_by_profession', array(), RouterInterface::ABSOLUTE_URL);
        $urls[] = new SitemapUrl($location, $releaseDate, SitemapUrl::CHANGE_WEEKLY, '0.8');
        
        // Recent Jobs
        $jobSpecRepository = $doctrine->getRepository('WanaworkUserBundle:JobSpec');
        $date = $jobSpecRepository->findLatestJobDate();
        $location = $router->generate('jobs_recent', array(), RouterInterface::ABSOLUTE_URL);
        $urls[] = new SitemapUrl($location, $date, SitemapUrl::CHANGE_HOURLY, '0.8');
        
        // Jobs by profession
        $professionsRepository = $doctrine->getRepository('WanaworkUserBundle:SectorJob');
        $professions = $professionsRepository->findAll();
        foreach ($professions as $profession) {
            $date = $jobSpecRepository->findLatestJobDate($profession);
            $location = $router->generate('jobs_for_profession', array('profession_slug' => $profession->getSlug()), RouterInterface::ABSOLUTE_URL);
            $urls[] = new SitemapUrl($location, $date, SitemapUrl::CHANGE_HOURLY, '0.8');
        }
        
        // Candidates List of professions
        $location = $router->generate('ad_candidates_profession_list', array(), RouterInterface::ABSOLUTE_URL);
        $urls[] = new SitemapUrl($location, $releaseDate, SitemapUrl::CHANGE_WEEKLY, '0.8');
        
        // Recent Candidates
        $adRepository = $doctrine->getRepository('WanaworkUserBundle:Ad');
        $date = $adRepository->findRecentPaidAdDate();
        $location = $router->generate('ad_candidates_recent', array(), RouterInterface::ABSOLUTE_URL);
        $urls[] = new SitemapUrl($location, $date, SitemapUrl::CHANGE_ALWAYS, '0.8');
        
        // Candidates by profession
        foreach ($professions as $profession) {
            $date = $adRepository->findRecentPaidAdDate($profession);
            $location = $router->generate('ad_candidates_by_profession', array('profession_slug' => $profession->getSlug()), RouterInterface::ABSOLUTE_URL);
            $urls[] = new SitemapUrl($location, $date, SitemapUrl::CHANGE_ALWAYS, '0.8');
        }
        
        $doctrine->clear();
        
        // List of ads
        $ads = $adRepository->findBy(array('isPublished' => true), array('updatedAt' => 'DESC'));
        foreach ($ads as $ad) {
            $location = $router->generate('ad_show', array('slug' => $ad->getSlug()), RouterInterface::ABSOLUTE_URL);
            $urls[] = new SitemapUrl($location, $ad->getUpdatedAt(), SitemapUrl::CHANGE_DAILY, '1.0');
        }
        
        $doctrine->clear();
        
        // List of employers
        $employerRepository = $doctrine->getRepository('WanaworkUserBundle:EmployerProfile');
        $employers = $employerRepository->findBy(array(), array('createdAt' => 'DESC'));
        
        foreach ($employers as $employer) {
            $location = $router->generate('company_show', array('slug' => $employer->getSlug()), RouterInterface::ABSOLUTE_URL);
            $urls[] = new SitemapUrl($location, $employer->getUpdatedAt(), SitemapUrl::CHANGE_WEEKLY, '1.0');
        }
        
        return $urls;
    }

    	
}