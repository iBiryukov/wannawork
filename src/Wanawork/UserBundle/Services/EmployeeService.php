<?php
namespace Wanawork\UserBundle\Services;

use Psr\Log\LoggerInterface;
use Wanawork\UserBundle\Entity\TemporaryAvatar;
use Wanawork\MainBundle\Security\AclManager;
use Wanawork\UserBundle\Entity\CvRequest;
use Wanawork\UserBundle\Entity\Ad;
use Wanawork\UserBundle\Entity\Reference;
use Wanawork\UserBundle\Entity\WorkExperience;
use Wanawork\UserBundle\Entity\CV;
use Wanawork\UserBundle\Entity\Education;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Wanawork\UserBundle\Entity\User;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Security\Acl\Model\MutableAclProviderInterface;
use Wanawork\UserBundle\Entity\EmployeeProfile;
use Wanawork\UserBundle\Entity\Role;

class EmployeeService 
{
	
	/**
	 * ACL Provider
	 * @var MutableAclProviderInterface
	 */
	private $aclProvider;
	
	/**
	 * Entity manager 
	 * @var \Doctrine\ORM\EntityManager
	 */
	private $em;
	
	/**
	 * Security
	 * @var \Symfony\Component\Security\Core\SecurityContextInterface
	 */
	private $security;
	
	/**
	 * Logger
	 * @var \Psr\Log\LoggerInterface
	 */
	private $logger;
	
	public function __construct(MutableAclProviderInterface $aclProvider, EntityManager $em, 
			SecurityContextInterface $security, LoggerInterface $logger) {
		$this->aclProvider = $aclProvider;
		$this->em = $em;
		$this->security = $security;
		$this->logger = $logger;
	}
	
	public function createProfile(EmployeeProfile $profile, $tempAvatarId = null)
	{
	    $this->logger->info('Creating Employee Profile', array(
	    	'user' => $profile->getId(),
	    ));
	    
		try {
			$this->em->getConnection()->beginTransaction();
			
			$role = $this->em->getRepository('WanaworkUserBundle:Role')->find('ROLE_EMPLOYEE');
			if (!$role instanceof Role) {
			    throw new \Exception('Unable to find candidate role');
			}
			
			$profile->getUser()->addRole($role);
			
			$this->em->flush();
			
			
			$aclProvider = $this->aclProvider;
			$objectIdentity = ObjectIdentity::fromDomainObject($profile);
			
			try {
				$acl = $aclProvider->findAcl($objectIdentity);
			} catch (\Symfony\Component\Security\Acl\Exception\AclNotFoundException $e) {
				$acl = $aclProvider->createAcl($objectIdentity);
			}
			
			$securityIdentity = UserSecurityIdentity::fromAccount($profile->getUser());
			
			$acl->insertObjectAce($securityIdentity, MaskBuilder::MASK_OPERATOR);
			$aclProvider->updateAcl($acl);
			$this->em->getConnection()->commit();
			
		} catch (\Exception $e) {
			$this->em->getConnection()->rollBack();
			$this->logger->alert('Unable to create new employee profile', array(
				'ex' => $e->__toString()
			));
			throw $e;
		}
		
		return $profile;
	}
}
