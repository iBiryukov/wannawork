<?php
namespace Wanawork\UserBundle\Services;

use Psr\Log\LoggerInterface;
use Wanawork\MainBundle\Security\AclManager;
use Wanawork\UserBundle\Entity\Ad;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Wanawork\UserBundle\Entity\User;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Security\Acl\Model\MutableAclProviderInterface;
use Wanawork\UserBundle\Entity\Role;
use Wanawork\UserBundle\Entity\JobSpec;

class JobService
{
    /**
     * ACL Provider
     * @var MutableAclProviderInterface
     */
    private $aclProvider;
    
    /**
     * Entity manager
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    
    /**
     * Security
     * @var \Symfony\Component\Security\Core\SecurityContextInterface
     */
    private $security;
    
    /**
     * Logger
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;
    
    public function __construct(MutableAclProviderInterface $aclProvider, EntityManager $em,
        SecurityContextInterface $security, LoggerInterface $logger) 
    {
        $this->aclProvider = $aclProvider;
        $this->em = $em;
        $this->security = $security;
        $this->logger = $logger;
    }
    
    public function createJob(JobSpec $job) 
    {
    
        try {
            $this->em->getConnection()->beginTransaction();
            $this->em->persist($job);
            $this->em->flush();
            	
            $aclProvider = $this->aclProvider;
            $objectIdentity = ObjectIdentity::fromDomainObject($job);
            $acl = $aclProvider->createAcl($objectIdentity);
            $securityIdentity = UserSecurityIdentity::fromAccount($job->getUser());
    
            $acl->insertObjectAce($securityIdentity, MaskBuilder::MASK_OPERATOR);
            $aclProvider->updateAcl($acl);
            	
            $this->em->getConnection()->commit();
        } catch (\Exception $e) {
            $this->em->getConnection()->rollBack();
            $this->logger->alert("Error while creating new job", array(
            	'ex' => $e->__toString(),
            ));
            throw $e;
        }
        return $education;
    }
}