<?php
namespace Wanawork\UserBundle\Services;

use Wanawork\UserBundle\Entity\SearchNotification;
use Symfony\Component\Console\Output\OutputInterface;
use Wanawork\UserBundle\Entity\Ad;
use Wanawork\UserBundle\Entity\Search;
use Doctrine\ORM\EntityManager;
use Wanawork\UserBundle\Entity\AdView;
use Psr\Log\LoggerInterface;
use Wanawork\UserBundle\Entity\Sector;
use Wanawork\UserBundle\WanaworkUserBundle;
use Wanawork\UserBundle\Entity\Billing\Order;
use Wanawork\UserBundle\Entity\Billing\Payment;
use Knp\Component\Pager\Paginator;

class SearchService 
{
    /**
     * Entity Manager instance
     * @var \Doctrine\ORM\EntityManager
     */	
	private $em;
	
	/**
	 * Logger
	 * @var \Psr\Log\LoggerInterface
	 */
	private $logger;
	
	/**
	 * Mail Service
	 * @var MailService
	 */
	private $mailer;
	
	public function __construct(EntityManager $manager, LoggerInterface $logger, MailService $mailer) 
	{
		$this->em = $manager;	
		$this->logger = $logger;
		$this->mailer = $mailer;
	}
	
	/**
	 * Find the ads
	 * @param Search               $search
	 * @param Paginator            $paginator
	 * @param number               $page
	 * @param number               $perPage
	 * @param SearchNotification   $notification
	 * 
	 * @return \Knp\Component\Pager\Pagination\PaginationInterface
	 */
	public function findAds(Search $search, Paginator $paginator, $page = 1, $perPage = 5, 
	    SearchNotification $notification = null)
	{
	    $qb = $this->getSearchQuery($search);
	    $pagination = $paginator->paginate($qb->getQuery(), $page, $perPage);
	    $paginationData = $pagination->getPaginationData();
	    
	    $ads = array();
	    foreach ($pagination as $row) {
	        $ad = $row[0];
	        $ad->setMatch($row['weight']);
	        $ads[] = $ad;
	    }
	    
	    $pagination->setItems($ads);
	    $search->setAds($ads, $page, $perPage, $paginationData['totalCount']);
 	    
 	    $this->em->flush();
 	    
 	    return $pagination;
	}
	
	private function getSearchQuery(Search $search)
	{
	    // within what percentage score, the ads are considered to be the same
	    $rangeStep = 5;  
	    $qb = $this->em->createQueryBuilder();
	    $qb->from('WanaworkUserBundle:Ad', 'ad');
	     
	    $weights = array(
	        'profession' => 40,
	        'industries' => 10,
	        'experience' => 10,
	        'educationLevel' => 10,
	        'positions' => 10,
	        'locations'  => 5,
	        'languages' => 5,
	    );
	     
	    $totalPossiblePoints = $weights['profession'];
	    $cases = array();
	     
	    // Profession
	    $qb->where('ad.profession=:profession');
	    $qb->setParameter('profession', $search->getProfession());
	    $cases[] = "(CASE WHEN ad.profession=:profession THEN {$weights['profession']} ELSE 0 END)";
	     
	    // Industries
	    if (sizeof($search->getIndustries())) {
	        $totalPossiblePoints += $weights['industries'];
	        $weightPerIndustry = $weights['industries'] / $search->getIndustries()->count();
	        $i = 0;
	        foreach ($search->getIndustries() as $industry) {
	            $prefix = 'prefix_industry_' . $industry->getId();
	            $cases[] =
	            "(CASE WHEN EXISTS(
	            SELECT {$prefix}_ad
	            FROM WanaworkUserBundle:Ad {$prefix}_ad
	            JOIN {$prefix}_ad.industries {$prefix}_industry
	            WHERE {$prefix}_ad=ad AND {$prefix}_industry IN (:industries{$i}))
	            THEN {$weightPerIndustry} ELSE 0 END)";
	            $qb->setParameter("industries{$i}", $industry);
	            ++$i;
	        }
	    }
	         
        // Experience
        if ($search->getExperience()) {
	        $totalPossiblePoints += $weights['experience'];
	        $cases[] = "(CASE WHEN ad.experience=:experience THEN {$weights['experience']} ELSE 0 END)";
	        $qb->setParameter('experience', $search->getExperience());
        }
	         
	    // Positions
	    if (sizeof($search->getPositions())) {
	        $totalPossiblePoints += $weights['positions'];
            $weightPerPosition = $weights['positions'] / $search->getPositions()->count();
            $i = 0;
            foreach ($search->getPositions() as $position) {
                $prefix = 'prefix_position_' . $position->getId();
                $cases[] =
                "(CASE WHEN EXISTS(
	            SELECT {$prefix}_ad
	            FROM WanaworkUserBundle:Ad {$prefix}_ad
	            JOIN {$prefix}_ad.positions {$prefix}_position
	            WHERE {$prefix}_ad=ad AND {$prefix}_position IN (:position{$i}))
	            THEN {$weightPerPosition} ELSE 0 END)";
	            $qb->setParameter("position{$i}", $position);
        	    ++$i;
            }
        }
	             
        // Education Level
        if ($search->getEducationLevel()) {
            $totalPossiblePoints += $weights['educationLevel'];
            $cases[] = "(CASE WHEN ad.educationLevel=:educationLevel THEN {$weights['educationLevel']} ELSE 0 END)";
            $qb->setParameter('educationLevel', $search->getEducationLevel());
        }
	             
        // Languages
        if (sizeof($search->getLanguages())) {
            $totalPossiblePoints += $weights['languages'];
            $weightPerLanguage = $weights['languages'] / $search->getLanguages()->count();
            $i = 0;
            foreach ($search->getLanguages() as $language) {
                $prefix = 'prefix_language_' . $language->getId();
                $cases[] =
                    "(CASE WHEN EXISTS(
        	            SELECT {$prefix}_ad
        	            FROM WanaworkUserBundle:Ad {$prefix}_ad
        	            JOIN {$prefix}_ad.languages {$prefix}_language
        	            WHERE {$prefix}_ad=ad AND {$prefix}_language IN (:language{$i}))
        	            THEN {$weightPerLanguage} ELSE 0 END)";
        	            $qb->setParameter("language{$i}", $language);
    	        ++$i;
            }
        }
	             
        // Locations
        if (sizeof($search->getLocations())) {
            $totalPossiblePoints += $weights['locations'];
            $weightPerLocations = $weights['locations'] / $search->getLocations()->count();
            $i = 0;
	        foreach ($search->getLocations() as $location) {
                $prefix = 'prefix_location_' . $location->getId();
                $cases[] =
                "(CASE WHEN EXISTS(
                SELECT {$prefix}_ad
                FROM WanaworkUserBundle:Ad {$prefix}_ad
                JOIN {$prefix}_ad.locations {$prefix}_location
                WHERE {$prefix}_ad=ad AND {$prefix}_location IN (:location{$i}))
                THEN {$weightPerLocations} ELSE 0 END)";
             
                $qb->setParameter("location{$i}", $location);
                ++$i;
            }
        }
	                 
        $casesStatement = "FLOOR((" . implode(' + ', $cases)  . ") * 100/{$totalPossiblePoints})";
        $casesStatementNormWeight = str_replace('prefix_', 'prefix2_',
        "FLOOR((" . implode(' + ', $cases)  . ") * 100/{$totalPossiblePoints})");
         
        $prefix = "last_payment";
        $lastPaymentDQL =
            "(SELECT MAX({$prefix}_payment.updatedAt)
            FROM WanaworkUserBundle:Ad {$prefix}_ad
            JOIN {$prefix}_ad.orders {$prefix}_order
            JOIN {$prefix}_order.payments {$prefix}_payment
            WHERE
            {$prefix}_ad = ad AND
            {$prefix}_order.status=:{$prefix}_orderStatus AND
            {$prefix}_payment.state=:{$prefix}_paymentState AND
            {$prefix}_payment.updatedAt IS NOT NULL)";
            
       $qb->setParameter("{$prefix}_orderStatus", Order::STATUS_PAID);
       $qb->setParameter("{$prefix}_paymentState", Payment::STATE_DEPOSITED);

       $prefix = "last_paid_order";
       $lastPublishedOrderDQL =
            "(SELECT MAX({$prefix}_order.runFrom)
            FROM WanaworkUserBundle:Ad {$prefix}_ad
            JOIN {$prefix}_ad.orders {$prefix}_order
            WHERE
            {$prefix}_ad = ad AND
            {$prefix}_order.status=:{$prefix}_orderStatus AND 
            {$prefix}_order.runFrom IS NOT NULL
            )";
         
        $qb->setParameter("{$prefix}_orderStatus", Order::STATUS_PAID);
         
        $prefix = 'ispremium';
        $isPremiumDQL =
            "(SELECT count({$prefix}_order)
            FROM WanaworkUserBundle:Ad {$prefix}_ad
            JOIN {$prefix}_ad.orders {$prefix}_order
            WHERE
            {$prefix}_ad = ad AND
            {$prefix}_order.status=:{$prefix}_orderStatus AND
            {$prefix}_order.plan=:{$prefix}_plan AND
            :{$prefix}_curdate >={$prefix}_order.runFrom AND
            :{$prefix}_curdate <= DATE_ADD({$prefix}_order.runFrom, {$prefix}_order.duration, 'DAY')
            )
            ";
            
        $qb->setParameter("{$prefix}_orderStatus", Order::STATUS_PAID);
        $qb->setParameter("{$prefix}_plan", Ad::PLAN_PREMIUM);
        $qb->setParameter("{$prefix}_curdate", date('Y-m-d G:i:s'));
         
        $qb->addSelect('ad');
        $qb->addSelect("$casesStatement as weight");
        $qb->addSelect("(FLOOR($casesStatementNormWeight / {$rangeStep}) * {$rangeStep}) as normalisedWeight");
	    $qb->addSelect("$lastPaymentDQL as lastPayment");
        $qb->addSelect("{$isPremiumDQL} as isPremium");
        $qb->addSelect("{$lastPublishedOrderDQL} as lastPublishedOrder");
	    	         
        $qb->andWhere('ad.isPublished=:published');
        $qb->setParameter('published', true);
	             
	    if (!$search->getIncludeExpired()) {
            $qb->andWhere('ad.expiryDate>=:expiryDate');
            $qb->setParameter('expiryDate', new \DateTime());
	    }
	    
	    $qb->orderBy('normalisedWeight', 'desc');
	    $qb->addOrderBy('isPremium', "DESC");
	    $qb->addOrderBy('lastPayment', 'DESC');
	    $qb->addOrderBy('lastPublishedOrder', 'DESC');
        $qb->addOrderBy('ad.lastBump', 'ASC');
        
	    return $qb;
	}
	
	public function updateNotifications(OutputInterface $output = null)
	{
		$logger = function($message) use ($output) {
			if($output) {
				$date = new \DateTime();
				$output->writeln($date->format('c') . ' ' . $message);
			}
		};
		
		$countQ = $this->em->createQuery("SELECT COUNT(n) AS sn_count FROM Wanawork\UserBundle\Entity\SearchNotification n");
		$notificationCount = $countQ->getSingleResult();
		$logger("Starting to check for new ads. Got {$notificationCount['sn_count']} notifications to check");
		
		$qb = $this->em->getRepository('WanaworkUserBundle:SearchNotification')->createQueryBuilder('n');
        $qb->where('n.active=:is_active');
        $qb->setParameter('is_active', true);
        $iterator = $qb->getQuery()->iterate();		
		
		$i = 1;
		foreach($iterator as $row) {
			$searchNotification = $row[0];
			
			if(!$searchNotification instanceof SearchNotification) {continue;}
			$logger("Processing search '{$searchNotification->getSearchName()}'. Id: '{$searchNotification->getId()}'");
			
			$qb = $this->getSearchQuery($searchNotification->getSearch());
			
			$prefix = "last_payment2";
			$lastPaymentDQL =
    			"(SELECT MAX({$prefix}_payment.updatedAt)
    			FROM WanaworkUserBundle:Ad {$prefix}_ad
    			JOIN {$prefix}_ad.orders {$prefix}_order
    			JOIN {$prefix}_order.payments {$prefix}_payment
    			WHERE
    			{$prefix}_ad = ad AND
    			{$prefix}_order.status=:{$prefix}_orderStatus AND
    			{$prefix}_payment.state=:{$prefix}_paymentState AND
    			{$prefix}_payment.updatedAt IS NOT NULL)";
			
			$qb->andWhere("$lastPaymentDQL > :last_check");
			$qb->setParameter('last_check', $searchNotification->getLastCheck());
			$qb->setParameter("{$prefix}_orderStatus", Order::STATUS_PAID);
			$qb->setParameter("{$prefix}_paymentState", Payment::STATE_DEPOSITED);

			$ads = array_map(function($row){
				return $row[0];
			}, $qb->getQuery()->getResult());
			
			if(sizeof($ads)) {
				$logger("Found " . sizeof($ads). " new ads. Updating");
			} else {
				$logger("No new ads found");
			}
			
			$searchCheck = $searchNotification->addSearchCheck($ads);
			$this->mailer->emailSearchNotificationUpdate($searchCheck);
			
			$this->em->flush();
			$this->em->clear();
			
			$logger("-------");
			++$i;
		}
		
		$logger(round(memory_get_peak_usage() / 1024 / 1024, 2) . 'MB used');
		
	}
}
