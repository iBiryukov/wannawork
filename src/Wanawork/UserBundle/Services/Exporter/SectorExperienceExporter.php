<?php
namespace Wanawork\UserBundle\Services\Exporter;

use Wanawork\UserBundle\Interfaces\Exporter;
use Doctrine\ORM\EntityManager;

class SectorExperienceExporter implements Exporter
{
    private $entityManager;
    
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }
    
    public function export()
    {
        $data = array();
        $sectors = $this->entityManager->getRepository('WanaworkUserBundle:SectorExperience')->findAll();
        
        foreach ($sectors as $sector) {
            $data[$sector->getId()] = $sector->getName();
        }
        
        return $data;
    }
}
