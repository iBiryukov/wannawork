<?php
namespace Wanawork\UserBundle\Services;

use Wanawork\UserBundle\Entity\PasswordReminder;
use Wanawork\UserBundle\Exception\EmailNotFound;
use Wanawork\UserBundle\Entity\Containers\EmailContainer;
use Symfony\Component\Security\Acl\Model\MutableAclInterface;
use Symfony\Component\Security\Acl\Model\AclInterface;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Wanawork\UserBundle\Entity\EmployeeProfile;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Wanawork\UserBundle\Entity\Role;
use Doctrine\Common\Persistence\ObjectManager;
use Wanawork\UserBundle\Entity\User;
use Symfony\Component\Security\Acl\Model\MutableAclProviderInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Psr\Log\LoggerInterface;

/**
 * Provides basic API for manipulating user accounts
 *
 * @author Ilya Biryukov <ilya@wanawork.ie>
 *        
 */
class AccountService
{

    /**
     * Password encoder to encode plain password
     * 
     * @var \Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface
     */
    private $encoder;

    /**
     * Access Control List service
     * 
     * @var \Symfony\Component\Security\Acl\Model\MutableAclProviderInterface
     */
    private $aclProvider;

    /**
     * Doctrine's Entity Manager
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * Service that checks access
     * @var \Symfony\Component\Security\Core\SecurityContextInterface
     */
    private $security;

    /**
     * Email service
     * @var \Wanawork\UserBundle\Services\MailService
     */
    private $mailer;
    
    /**
     * Logger
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * List of allowed account types
     * 
     * @var array
     */
    private $allowedRegistrationTypes = array(
        'employer',
        'employee'
    );

    /**
     * Constructor
     * @param EncoderFactoryInterface                     $encoder
     * @param MutableAclProviderInterface                 $aclProvider
     * @param EntityManager                               $em
     * @param SecurityContextInterface                    $security
     * @param \Wanawork\UserBundle\Services\MailService   $mailer
     * @param LoggerInterface                             $logger
     */
    public function __construct(EncoderFactoryInterface $encoder, MutableAclProviderInterface $aclProvider, 
        EntityManager $em, SecurityContextInterface $security, MailService $mailer, LoggerInterface $logger)
    {
        $this->encoder = $encoder;
        $this->aclProvider = $aclProvider;
        $this->em = $em;
        $this->security = $security;
        $this->mailer = $mailer;
        $this->logger = $logger;
    }

    /**
     * Give the registration account type,
     * get a role which the user will have
     *
     * @param string  $type   
     * 
     * @return string         
     */
    public function getRoleForAccountType($type)
    {
        $type = mb_strtolower($type);
        $role = null;
        $roleRepository = $this->em->getRepository('WanaworkUserBundle:Role');
        
        switch ($type) {
            case 'employer':
                $role = $roleRepository->find(Role::EMPLOYER);
                break;
            
            case 'employee':
                $role = $roleRepository->find(Role::EMPLOYEE);
                break;
            
            default:
                break;
        }
        
        if(!$role instanceof Role) {
            throw new \Exception('Unknown role requested: ' . $type);
        }
        
        return $role;
    }

    /**
     * Register a user
     * 
     * @param User  $user            
     * @param array $roles
     *             
     * @throws Exception
     * 
     * @return Wanawork\UserBundle\Entity\User
     */
    public function register(User $user, array $roles = array())
    {
        if(!sizeof($roles)) {
            throw new \Exception('No roles provided');
        }
        
        try {
            $this->em->getConnection()->beginTransaction();
            foreach($roles as $role) {
                $user->addRole($role);
            }
            $encoder = $this->encoder->getEncoder($user);
            $password = $encoder->encodePassword($user->getPassword(), $user->getSalt());
            $user->setPassword($password);
            
            $em = $this->em;
            $em->persist($user);
            
            $em->flush();
            
            $aclProvider = $this->aclProvider;
            
            // Grant permissions on User Object
            $objectIdentity = ObjectIdentity::fromDomainObject($user);
            $acl = $aclProvider->createAcl($objectIdentity);
            $securityIdentity = UserSecurityIdentity::fromAccount($user);
            
            $acl->insertObjectAce($securityIdentity, MaskBuilder::MASK_OPERATOR);
            $aclProvider->updateAcl($acl);
            $this->em->getConnection()->commit();
            
            $this->mailer->emailUserRegistration($user);
            
        } catch (\Exception $e) {
            $this->em->getConnection()->rollBack();
            $this->logger->alert("Failed to create a new account", array(
            	'exception' => $e->__toString(),
            ));
            throw $e;
        }
    }

    /**
     * Send out a password reminder
     * @param EmailContainer  $emailContainer
     * @param string          $ip
     * 
     * @throws EmailNotFound
     * @throws Exception
     * 
     * @return \Wanawork\UserBundle\Entity\PasswordReminder
     */
    public function restorePassword(EmailContainer $emailContainer, $ip)
    {
        $userRepository = $this->em->getRepository('WanaworkUserBundle:User');
        $user = $userRepository->findOneByEmail($emailContainer->getEmail());
        
        if (! $user instanceof User) {
            throw new EmailNotFound();
        }
        
        $passwordReminder = null;
        try {
            $this->em->getConnection()->beginTransaction();
            $passwordReminder = $user->addPasswordReminder($ip);
            $this->em->flush();
            
            $this->mailer->emailPasswordReminder($passwordReminder);
            $this->em->getConnection()->commit();
        } catch (\Exception $e) {
            $this->em->getConnection()->rollBack();
            $this->logger->alert("Error while doing a password restore", array(
            	'exception' => $e->getMessage(),
            ));
            throw $e;
        }
        
        return $passwordReminder;
    }

    /**
     * Set the new password for the user
     * 
     * @param PasswordReminder   $reminder
     * @param string             $newPassword
     * @param string             $ip
     * 
     * @throws \Exception
     * @throws Exception
     */
    public function resetPassword(PasswordReminder $reminder, $newPassword, $ip)
    {
        if (! $reminder->isValid()) {
            throw new \Exception("The password reset token is invalid");
        }
        try {
            $this->em->getConnection()->beginTransaction();
            
            $reminder->markUsed($ip);
            
            $encoder = $this->encoder->getEncoder($reminder->getUser());
            $password = $encoder->encodePassword($newPassword, $reminder->getUser()
                ->getSalt());
            $reminder->getUser()->setPassword($password);
            $this->em->flush();
            $this->em->getConnection()->commit();
        } catch (\Exception $e) {
            $this->em->getConnection()->rollBack();
            error_log("Error occurred while setting new password: {$e->getMessage()}");
            throw $e;
        }
    }
}

