<?php
namespace Wanawork\UserBundle\Services;

use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\Templating\EngineInterface;
use Wanawork\UserBundle\Entity\SystemEmail;
use Wanawork\UserBundle\Entity\User;
use Symfony\Component\Console\Event\ConsoleCommandEvent;
use Wanawork\UserBundle\Entity\UnsubscribedEmail;
use Wanawork\UserBundle\Entity\Role;
use Wanawork\UserBundle\Entity\PasswordReminder;
use Wanawork\UserBundle\Entity\CvRequest;
use Wanawork\UserBundle\Entity\Ad;
use Wanawork\UserBundle\Entity\Message;
use Wanawork\UserBundle\Entity\EmployerProfile;
use Wanawork\UserBundle\Entity\EmployeeProfile;
use Wanawork\UserBundle\Entity\Billing\Order;
use Wanawork\UserBundle\Entity\Billing\AdOrder;
use Wanawork\UserBundle\Entity\Billing\VerificationOrder;
use Wanawork\UserBundle\Entity\SearchNotificationCheck;

class MailService
{
    /**
     * Entity Manager
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    
    /**
     * Email service
     * @var \Swift_Mailer
     */
    private $mailer;
    
    /**
     * Templating service
     * @var \Symfony\Component\Templating\EngineInterface
     */
    private $twig;
    
    /**
     * Logger
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;
    
    /**
     * Swift's internal event dispatcher
     * @var \Swift_Events_EventDispatcher
     */
    private $mailerEventDispatcher;
    
    /**
     * List of listeners that are called when email message status changes
     * @var array
     */
    private $listeners;
    
    private $fromEmail = 'robot@wannawork.ie';
    private $fromName = "Wannawork.ie Team";
    
    /**
     * Constructor
     * @param EntityManager               $em
     * @param \Psr\Log\LoggerInterface    $logger
     * @param EngineInterface             $mailer
     * @param \Swift_Mailer               $templating
     */
    public function __construct(EntityManager $em, LoggerInterface $logger, 
        \Swift_Mailer $mailer, EngineInterface $twig, 
        \Swift_Events_EventDispatcher $mailerEventDispatcher, array $listeners, array $options)
    {
        $this->em = $em;
        $this->logger = $logger;
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->listeners = $listeners;
        $this->mailerEventDispatcher = $mailerEventDispatcher;
        $this->setOptions($options);
        $this->bindListeners();
    }
    
    public function setOptions(array $options)
    {
        if (isset($options['from_email'])) {
            $this->fromEmail = $options['from_email'];
        }
        
        if (isset($options['from_name'])) {
            $this->fromName = $options['from_name'];
        }
    }
    
    /**
     * Send registration email to the user
     * @param User $user
     * @return \Wanawork\UserBundle\Entity\SystemEmail
     */
    public function emailUserRegistration(User $user)
    {
        if ($this->isUnsubscribed($user->getEmail()) || defined('IS_CMD')) {
            return null;
        }
        
        $systemEmail = null;
        try {
            $this->em->getConnection()->beginTransaction();
            
            $template = null;
            $type = null;
            if ($user->hasRoleName(Role::EMPLOYEE)) {
                $template = '::/email/registration_employee.html.twig';
                $type = SystemEmail::TYPE_REGISTRATION_CANDIDATE;
            } elseif($user->hasRoleName(Role::EMPLOYER)) {
                $template = '::/email/registration_employer.html.twig';
                $type = SystemEmail::TYPE_REGISTRATION_EMPLOYER;
            } else {
                $this->logger->alert("Cannot send out user regitraion because the account type is not recognised", array(
                	'user' => $user->getEmail(),
                    'roles' => $user->getRoleNames(),
                ));
                return null;
            }
            
            $subject = 'Thank you for registering';
            $systemEmail = new SystemEmail($user, $subject, $type);
            $text = $this->twig->render($template, array(
            	'systemEmail' => $systemEmail,
            ));
            $systemEmail->setText($text);
            $this->em->persist($systemEmail);
            $this->em->flush($systemEmail);
            
            $message = \Swift_Message::newInstance()->setContentType('text/html')
                ->setSubject($subject)
                ->setFrom($this->fromEmail, $this->fromName)
                ->setTo($user->getEmail(), $user->getName())
                ->setBody($systemEmail->getText());
            $message->getHeaders()->addTextHeader('wanawork-email-id', $systemEmail->getId());
            $this->mailer->send($message);
            
            $this->em->getConnection()->commit();
            
        } catch (\Exception $e) {
            $this->em->rollback();
            $this->logger->alert("Unable to send email", array(
            	'exception' => $e->__toString(),
                'user' => $user->getId(),
            ));
        }
        return $systemEmail;
    }
    
    /**
     * Email password restore instruction to the user
     * @param PasswordReminder $passwordReminder
     * @return \Wanawork\UserBundle\Entity\SystemEmail
     */
    public function emailPasswordReminder(PasswordReminder $passwordReminder)
    {
        if ($this->isUnsubscribed($passwordReminder->getUser()->getEmail()) || defined('IS_CMD')) {
            return null;
        }
        
        $systemEmail = null;
        try {
            $this->em->getConnection()->beginTransaction();
            $user = $passwordReminder->getUser();
            $subject = 'Password Restore';
            
            $systemEmail = new SystemEmail($user, $subject, SystemEmail::TYPE_RESTORE_PASSWORD);
            
            $body = $this->twig->render('::/email/forgotten_password.html.twig', array(
                'passwordReminder' => $passwordReminder,
                'systemEmail' => $systemEmail,
            ));
            $systemEmail->setText($body);
            
            $this->em->persist($systemEmail);
            $this->em->flush($systemEmail);
            
            $message = \Swift_Message::newInstance()->setContentType('text/html')
              ->setSubject($subject)
              ->setFrom($this->fromEmail, $this->fromName)
              ->setTo($user->getEmail())
              ->setBody($systemEmail->getText());
            $message->getHeaders()->addTextHeader('wanawork-email-id', $systemEmail->getId());
            $this->mailer->send($message);
            
            $this->em->getConnection()->commit();
        } catch (\Exception $e) {
            $this->em->getConnection()->rollBack();
            $this->logger->alert("Unable to email password restore", array(
            	'exception' => $e->__toString(),
            ));
        }
        
        return $systemEmail;
    }
    
    /**
     * Notify candidate of the new cv request
     * @param CvRequest $request
     * @return \Wanawork\UserBundle\Entity\SystemEmail
     */
    public function emailNewCVRequest(CvRequest $request)
    {
        if ($this->isUnsubscribed($request->getAd()->getProfile()->getUser()->getEmail()) || defined('IS_CMD')) {
            return null;
        }
        
        $systemEmail = null;
        try {
            $this->em->getConnection()->beginTransaction();
            $user = $request->getAd()->getProfile()->getUser();
            $subject = 'New CV Request';
        
            $systemEmail = new SystemEmail($user, $subject, SystemEmail::TYPE_CV_REQUEST);
        
            $body = $this->twig->render('::/email/cv-request.html.twig', array(
                'request' => $request,
                'systemEmail' => $systemEmail,
            ));
            $systemEmail->setText($body);
        
            $this->em->persist($systemEmail);
            $this->em->flush($systemEmail);
        
            $message = \Swift_Message::newInstance()->setContentType('text/html')
            ->setSubject($subject)
            ->setFrom($this->fromEmail, $this->fromName)
            ->setTo($user->getEmail())
            ->setBody($systemEmail->getText());
            $message->getHeaders()->addTextHeader('wanawork-email-id', $systemEmail->getId());
            $this->mailer->send($message);
        
            $this->em->getConnection()->commit();
        } catch (\Exception $e) {
            $this->em->getConnection()->rollBack();
            $this->logger->alert("Unable to email new cv request", array(
                'exception' => $e->__toString(),
            ));
        }
        
        return $systemEmail;
    }
    
    public function emailCVRequestResponse(CvRequest $request)
    {
        if ($this->isUnsubscribed($request->getAd()->getProfile()->getUser()->getEmail()) || defined('IS_CMD')) {
            return null;
        }
        
        if ($request->isPending()) {
            return null;
        }
        
        $systemEmail = null;
        try {
            $this->em->getConnection()->beginTransaction();
            $user = $request->getUser();
            $subject = 'Response to CV Request';
        
            $systemEmail = new SystemEmail($user, $subject, SystemEmail::TYPE_CV_REQUEST_RESPONSE);
        
            $body = $this->twig->render('::/email/cv-request-response.html.twig', array(
                'request' => $request,
                'systemEmail' => $systemEmail,
            ));
            $systemEmail->setText($body);
        
            $this->em->persist($systemEmail);
            $this->em->flush($systemEmail);
        
            $message = \Swift_Message::newInstance()->setContentType('text/html')
            ->setSubject($subject)
            ->setFrom($this->fromEmail, $this->fromName)
            ->setTo($user->getEmail())
            ->setBody($systemEmail->getText());
            $message->getHeaders()->addTextHeader('wanawork-email-id', $systemEmail->getId());
            $this->mailer->send($message);
        
            $this->em->getConnection()->commit();
        } catch (\Exception $e) {
            $this->em->getConnection()->rollBack();
            $this->logger->alert("Unable to email cv request response", array(
                'exception' => $e->__toString(),
            ));
        }
        
        return $systemEmail;
    }
    
    public function emailNewMessage(Message $message)
    {
        $user = null;
        $recepientProfile = $message->getRecepient();
        if ($recepientProfile instanceof EmployerProfile) {
            // @todo fix for multi user here
            $user = $recepientProfile->getAdminUsers()->first();
        } elseif ($recepientProfile instanceof EmployeeProfile) {
            $user = $recepientProfile->getUser();
        }
        
        if ($user === null) {
            throw new \Exception("Unable to figure out the recepient");
        }
        
        if ($this->isUnsubscribed($user->getEmail()) || defined('IS_CMD')) {
            return null;
        }
        
        $systemEmail = null;
        try {
            $this->em->getConnection()->beginTransaction();
            
            $subject = 'New Message';
            $systemEmail = new SystemEmail($user, $subject, SystemEmail::TYPE_NEW_MESSAGE);
        
            $body = $this->twig->render('::/email/new-message.html.twig', array(
                'message' => $message,
                'systemEmail' => $systemEmail,
            ));
            $systemEmail->setText($body);
        
            $this->em->persist($systemEmail);
            $this->em->flush($systemEmail);
        
            $message = \Swift_Message::newInstance()->setContentType('text/html')
            ->setSubject($subject)
            ->setFrom($this->fromEmail, $this->fromName)
            ->setTo($user->getEmail())
            ->setBody($systemEmail->getText());
            $message->getHeaders()->addTextHeader('wanawork-email-id', $systemEmail->getId());
            $this->mailer->send($message);
        
            $this->em->getConnection()->commit();
        } catch (\Exception $e) {
            $this->em->getConnection()->rollBack();
            $this->logger->alert("Unable to email new message", array(
                'exception' => $e->__toString(),
            ));
        }
        
        return $systemEmail;
    }
    
    public function emailPaymentReceipt(Order $order, $receiptFile)
    {
        if (!is_file($receiptFile)) {
            throw new \Exception("Unable to find the receipt: '$receiptFile'");
        }
        
        $user = $order->getUser();
        if ($user === null) {
            if ($order instanceof AdOrder) {
                $user = $order->getAd()->getProfile()->getUser();
            } elseif($order instanceof VerificationOrder) {
                // @todo fix this for multi-admin
                $user = $order->getEmployer()->getAdminUsers()->first();
            } else {
                throw new \Exception("Unable to determine which user to send receipt to");
            }
        }
        
        if ($this->isUnsubscribed($user->getEmail()) || defined('IS_CMD')) {
            return null;
        }
        
        $systemEmail = null;
        try {
            $this->em->getConnection()->beginTransaction();
            $subject = 'Receipt';
        
            $systemEmail = new SystemEmail($user, $subject, SystemEmail::TYPE_RECEIPT);
        
            $body = $this->twig->render('::/email/payment-receipt.html.twig', array(
                'order' => $order,
                'systemEmail' => $systemEmail,
            ));
            $systemEmail->setText($body);
            
            $order->setReceiptEmailed(true);
            $this->em->persist($systemEmail);
            $this->em->flush();
        
            $message = \Swift_Message::newInstance()->setContentType('text/html')
            ->setSubject($subject)
            ->setFrom($this->fromEmail, $this->fromName)
            ->setTo($user->getEmail())
            ->setBody($systemEmail->getText());
            
            $message->getHeaders()->addTextHeader('wanawork-email-id', $systemEmail->getId());
            $message->attach(\Swift_Attachment::fromPath($receiptFile));
            
            $this->mailer->send($message);
        
            $this->em->getConnection()->commit();
        } catch (\Exception $e) {
            $this->em->getConnection()->rollBack();
            $this->logger->alert("Unable to email receipt", array(
                'exception' => $e->__toString(),
            ));
        }
        
        return $systemEmail;
        
    }
    
    public function emailAdminRegisteredEmployer(User $user, $rawPassword)
    {
        if ($this->isUnsubscribed($user->getEmail()) || defined('IS_CMD')) {
            return null;
        }
        
        $systemEmail = null;
        try {
            $this->em->getConnection()->beginTransaction();
        
            $template = '::/email/admin-email-registration.html.twig';
            $type = SystemEmail::TYPE_ADM_EMPLOYER_REG;
        
            $subject = 'Your Wannawork.ie Account';
            $systemEmail = new SystemEmail($user, $subject, $type);
            $text = $this->twig->render($template, array(
                'systemEmail' => $systemEmail,
                'rawPassword' => $rawPassword,
            ));
            $systemEmail->setText($text);
            $this->em->persist($systemEmail);
            $this->em->flush($systemEmail);
        
            $message = \Swift_Message::newInstance()->setContentType('text/html')
            ->setSubject($subject)
            ->setFrom($this->fromEmail, $this->fromName)
            ->setTo($user->getEmail(), $user->getName())
            ->setBody($systemEmail->getText());
            $message->getHeaders()->addTextHeader('wanawork-email-id', $systemEmail->getId());
            $this->mailer->send($message);
        
            $this->em->getConnection()->commit();
        
        } catch (\Exception $e) {
            $this->em->rollback();
            $this->logger->alert("Unable to send email", array(
                'exception' => $e->__toString(),
                'user' => $user->getId(),
            ));
        }
        return $systemEmail;
    }
    
    public function emailCustomMessage(SystemEmail $systemEmail)
    {
        if ($this->isUnsubscribed($systemEmail->getEmail()) || defined('IS_CMD')) {
            return null;
        }
        
        try {
            $this->em->getConnection()->beginTransaction();
            $this->em->persist($systemEmail);
            $this->em->flush();
        
            $message = \Swift_Message::newInstance()->setContentType('text/html')
            ->setSubject($systemEmail->getSubject())
            ->setFrom($this->fromEmail, $this->fromName)
            ->setTo($systemEmail->getEmail())
            ->setBody($systemEmail->getText());
        
            $message->getHeaders()->addTextHeader('wanawork-email-id', $systemEmail->getId());
        
            $this->mailer->send($message);
            $this->em->getConnection()->commit();
        } catch (\Exception $e) {
            $this->em->getConnection()->rollBack();
            $this->logger->alert("Unable to custom email", array(
                'exception' => $e->__toString(),
            ));
            throw $e;
        }
        
        return $systemEmail;
    }
    
    public function emailSearchNotificationUpdate(SearchNotificationCheck $notificationCheck)
    {
        if ($notificationCheck->getNewAdsCount() <= 0) {
            return;
        }
        
        $notification = $notificationCheck->getNotification();
        
        $subject = 'We have new candidates for you';
        $type = SystemEmail::TYPE_SEARCH_NOTIFICATION;
        $systemEmail = null;
        $email = null;
        $name = null;
        if ($notification->getUser()) {
            $email = $notification->getUser()->getEmail();
            $name = $notification->getUser()->getName();
            $systemEmail = new SystemEmail($notification->getUser(), $subject, $type);
        } else {
            $email = $notification->getEmail();
            $name = "There";
            $systemEmail = new SystemEmail($user = null, $subject, $type, $email);
        }
        
        if ($this->isUnsubscribed($email) || defined('IS_CMD')) {
            return;
        }
        
        try {
            $this->em->getConnection()->beginTransaction();
        
            $template = '::/email/search-notification.html.twig';
        
            $text = $this->twig->render($template, array(
                'name' => $name,
                'notificationCheck' => $notificationCheck,
                'systemEmail' => $systemEmail,
            ));
            $systemEmail->setText($text);
            $this->em->persist($systemEmail);
            $this->em->flush($systemEmail);
        
            $message = \Swift_Message::newInstance()->setContentType('text/html')
            ->setSubject($subject)
            ->setFrom($this->fromEmail, $this->fromName)
            ->setTo($email)
            ->setBody($systemEmail->getText());
            $message->getHeaders()->addTextHeader('wanawork-email-id', $systemEmail->getId());
            $this->mailer->send($message);
        
            $this->em->getConnection()->commit();
        
        } catch (\Exception $e) {
            $this->em->rollback();
            $this->logger->alert("Unable to send search notification notification", array(
                'exception' => $e->__toString(),
                'notification' => $notificationCheck->getNotification()->getId(),
            ));
        }
        return $systemEmail;
    }
    
    /**
     * 
     * @param ConsoleCommandEvent $command
     */
    public function onConsoleCommand(ConsoleCommandEvent $command)
    {
        if('swiftmailer:spool:send' === $command->getCommand()->getName()) {
            $this->bindListeners();
        }
    }
    
    private function bindListeners()
    {
        foreach ($this->listeners as $listener) {
            $this->mailerEventDispatcher->bindEventListener($listener);
        }
    }
    
    /**
     * Check if the given email unsubscribed from our correspondence
     * @param string $email
     * @return boolean
     */
    public function isUnsubscribed($email)
    {
        $unsub = $this->em->getRepository('WanaworkUserBundle:UnsubscribedEmail')->find($email);
        return $unsub instanceof UnsubscribedEmail;
    }
    
    public function unsubscribe($email, SystemEmail $systemEmail = null)
    {
        $unsub = $this->em->getRepository('WanaworkUserBundle:UnsubscribedEmail')->find($email);
        
        if (!$unsub) {
            try {
            	$this->em->getConnection()->beginTransaction();
            	$unsub = new UnsubscribedEmail($email, $systemEmail);
            	$this->em->persist($unsub);
            	$this->em->flush($unsub);
            	$this->em->getConnection()->commit();
            } catch (\Exception $e) {
                $this->em->getConnection()->rollBack();
                $this->logger->alert("Unable to unsubscribe user's email", array(
                	'email' => $email,
                    'systemEmail' => $systemEmail === null ? null : $systemEmail->getId(),
                ));
                throw $e;
            }
        }
        return $unsub;
    }
    
    public function resubscribe($email)
    {
        try {
            $this->em->getConnection()->beginTransaction();
            $this->em->getRepository('WanaworkUserBundle:UnsubscribedEmail')->removeByEmail($email);
            $this->em->getConnection()->commit();
        } catch (\Exception $e) {
            $this->em->getConnection()->rollBack();
            $this->logger->alert("Unable to re-subscribe user's email", array(
                'email' => $email,
            ));
            throw $e;
        }
        return true;
    }

}
   