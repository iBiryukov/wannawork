<?php
namespace Wanawork\UserBundle\Services;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Acl\Model\MutableAclProviderInterface;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Psr\Log\LoggerInterface;
use Wanawork\UserBundle\Entity\Ad;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Logic for Adverts
 * 
 * @author Ilya Biryukov <ilya@wanawork.ie>
 */
class AdService
{
    /**
     * Doctrine entity manager
     * 
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;
    
    /**
     * Access Control List provider
     * 
     * @var \Symfony\Component\Security\Acl\Model\MutableAclProviderInterface
     */
    private $aclProvider;
    
    /**
     * Security control
     * @var \Symfony\Component\Security\Core\SecurityContextInterface
     */
    private $security;
    
    /**
     * Logger
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;
    
    
    public function __construct(EntityManager $entityManager, MutableAclProviderInterface $aclProvider,
                                SecurityContextInterface $security, LoggerInterface $logger)
    {
        $this->entityManager = $entityManager;  
        $this->aclProvider = $aclProvider;  
        $this->security = $security;
        $this->logger = $logger;
    }
    
	
	public function deleteAd(Ad $ad)
	{
	    try {
	        $this->em->getConnection()->beginTransaction();
	
	        $aclProvider = $this->aclProvider;
	        $aclProvider->deleteAcl(ObjectIdentity::fromDomainObject($ad));
	
	        $this->em->remove($ad);
	        $this->em->flush();
	
	        $this->em->getConnection()->commit();
	    } catch (\Exception $e) {
	        $this->em->getConnection()->rollback();
	        $this->logger->critical("Error while deleting an ad: {$e->getMessage()}", array(
	        	'ad' => $ad->getId(),
	        ));
	        throw $e;
	    }
	}
}