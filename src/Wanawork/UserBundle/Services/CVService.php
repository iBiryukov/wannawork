<?php
namespace Wanawork\UserBundle\Services;

use Psr\Log\LoggerInterface;
use Wanawork\MainBundle\Security\AclManager;
use Wanawork\UserBundle\Entity\CvRequest;
use Wanawork\UserBundle\Entity\Ad;
use Wanawork\UserBundle\Entity\CV;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Wanawork\UserBundle\Entity\User;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Security\Acl\Model\MutableAclProviderInterface;
use Wanawork\UserBundle\Entity\EmployeeProfile;
use Symfony\Component\Security\Acl\Domain\RoleSecurityIdentity;
use Wanawork\UserBundle\Services\MailService;

class CVService
{
    /**
     * ACL Provider
     * @var MutableAclProviderInterface
     */
    private $aclProvider;
    
    /**
     * Entity manager
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    
    /**
     * Security
     * @var \Symfony\Component\Security\Core\SecurityContextInterface
     */
    private $security;
    
    /**
     * Logger
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;
    
    /**
     * Mail Service
     * @var \Wanawork\UserBundle\Services\MailService
     */
    private $mailer;
    
    public function __construct(MutableAclProviderInterface $aclProvider, EntityManager $em,
        SecurityContextInterface $security, LoggerInterface $logger, MailService $mailer) {
        $this->aclProvider = $aclProvider;
        $this->em = $em;
        $this->security = $security;
        $this->logger = $logger;
        $this->mailer = $mailer;
    }
    
    
    public function approveCvRequest(CvRequest $cvRequest) 
    {
        $this->logger->info("Approving CV Request", array(
        	'request' => $cvRequest->getId(),
        ));
        
        try {
            $this->em->getConnection()->beginTransaction();
            $cvRequest->approve();
            if(!$cvRequest->getMailThread()) {
                $cvRequest->createMailThread();
            }
            $this->em->flush();
            	
            $security = $this->security;
            $aclProvider = $this->aclProvider;
            $aclManager = new AclManager($aclProvider, $security);
            	
            // Ad. View Access
            $aclManager->grant($cvRequest->getAd(), MaskBuilder::MASK_VIEW, $cvRequest->getEmployerProfile()->getUserRole());
            	
            $mb = new MaskBuilder();
            $mb->add(MaskBuilder::MASK_CREATE);
            $mb->add(MaskBuilder::MASK_VIEW);
            	
            // Employer View/Create access to messages
            $aclManager->grant($cvRequest->getMailThread(), $mb->get(), $cvRequest->getEmployerProfile()->getUserRole());
            	
            // Employee View/Create access to messages
            $aclManager->grant($cvRequest->getMailThread(), $mb->get(), $cvRequest->getAd()->getCv()->getProfile()->getUser());
            
            // Disable cv request delete functionality
            $aclManager->revoke($cvRequest, MaskBuilder::MASK_DELETE, $cvRequest->getEmployerProfile()->getUserRole());

            $this->mailer->emailCVRequestResponse($cvRequest);
            
            $this->em->getConnection()->commit();
        } catch (\Exception $e) {
            $this->em->getConnection()->rollback();
            $this->logger->alert("Error while approving cv request", array(
            	'exception' => $e->__toString(),
            ));
            throw $e;
        }
    }
    
    public function denyCvRequest(CvRequest $cvRequest)
    {
        $this->logger->info('Denying CV Access Request', array(
        	'request' => $cvRequest->getId(),
        ));
        try {
            $this->em->getConnection()->beginTransaction();
            $cvRequest->deny();
            $this->em->flush();
            
            $security = $this->security;
            $aclProvider = $this->aclProvider;
            $aclManager = new AclManager($aclProvider, $security);
            	
            // Ad. View Access
            $aclManager->revoke($cvRequest->getAd(), MaskBuilder::MASK_VIEW, $cvRequest->getEmployerProfile()->getUserRole());
            
            // Disable cv request delete functionality
            $aclManager->revoke($cvRequest, MaskBuilder::MASK_DELETE, $cvRequest->getEmployerProfile()->getUserRole());
    
            if($cvRequest->getMailThread()) {
                // Employer Create access to messages
                $aclManager->revoke($cvRequest->getMailThread(), MaskBuilder::MASK_CREATE, $cvRequest->getEmployerProfile()->getUserRole());
    
                // Employee Create access to messages
                $aclManager->revoke($cvRequest->getMailThread(), MaskBuilder::MASK_CREATE, $cvRequest->getAd()->getCv()->getProfile()->getUser());
            }
            	
            $this->mailer->emailCVRequestResponse($cvRequest);
            $this->em->getConnection()->commit();
        } catch(\Exception $e) {
            $this->em->getConnection()->rollback();
            $this->logger->alert("Error while denying cv request", array(
            	'exception' => $e->__toString(),
            ));
            throw $e;
        }
    }
    
    public function createCvRequest(CvRequest $request)
    {
        $this->logger->info('Creating CV Request', array(
        	'ad' => $request->getAd() ? $request->getAd()->getId() : null,
            'employerProfile' => $request->getEmployerProfile() ? $request->getEmployerProfile()->getId() : null,
        ));
        
        try {
        	$this->em->getConnection()->beginTransaction();
        	$this->em->persist($request);
        	$this->em->flush();
        	
        	$security = $this->security;
        	$aclProvider = $this->aclProvider;
        	$aclManager = new AclManager($aclProvider, $security);
        	
        	$employerMask = MaskBuilder::MASK_VIEW | MaskBuilder::MASK_DELETE;
        	$candidateMask = MaskBuilder::MASK_VIEW | MaskBuilder::MASK_EDIT;
        	
        	$aclManager->grant($request, $employerMask, $request->getEmployerProfile()->getUserRole());
        	$aclManager->grant($request, $candidateMask, $request->getAd()->getProfile()->getUser());
        	$aclManager->grant($request->getEmployerProfile(), MaskBuilder::MASK_VIEW, $request->getAd()->getProfile()->getUser());
        	
        	$this->mailer->emailNewCVRequest($request);
        	
        	$this->em->getConnection()->commit();
        } catch (\Exception $e) {
            $this->em->getConnection()->rollBack();
            $this->logger->alert('Unable to save cv request', array(
            	'exception' => $e->__toString(),
            ));
            throw $e;
        }
    }
    
    public function deleteCvRequest(CvRequest $request)
    {
        $this->logger->info("Deleting CV Request", array(
        	'ad' => $request->getAd()->getId(),
            'employerProfile' => $request->getEmployerProfile()->getId(),
        ));
        
        try {
            $this->em->getConnection()->beginTransaction();
            
            $security = $this->security;
            $aclProvider = $this->aclProvider;
            $aclManager = new AclManager($aclProvider, $security);
            $aclManager->revoke($request->getEmployerProfile(), MaskBuilder::MASK_VIEW, $request->getAd()->getProfile()->getUser());
            	
            $aclProvider->deleteAcl(ObjectIdentity::fromDomainObject($request));
            	
            $this->em->remove($request);
            $this->em->flush();
            
            $this->em->getConnection()->commit();
        } catch (\Exception $e) {
            $this->em->getConnection()->rollback();
            $this->logger->alert("Unable to delete cv request", array(
            	'exception' => $e->__toString(),
            ));
            throw $e;
        }
    }
}