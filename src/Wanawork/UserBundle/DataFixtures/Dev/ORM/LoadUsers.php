<?php
namespace Wanawork\UserBundle\DataFixtures\Dev\ORM;
use Wanawork\MainBundle\Security\AclManager;

use Wanawork\UserBundle\Entity\Message;
use Wanawork\UserBundle\Entity\EmployerProfile;
use Wanawork\UserBundle\Entity\Ad;
use Wanawork\UserBundle\Entity\Reference;
use Wanawork\UserBundle\Entity\WorkExperience;
use Wanawork\UserBundle\Entity\Education;
use Wanawork\UserBundle\Entity\CVForm;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\SecurityContext;
use Wanawork\UserBundle\Entity\Role;
use Symfony\Component\Security\Acl\Domain\RoleSecurityIdentity;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Wanawork\UserBundle\Entity\EmployeeProfile;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Wanawork\UserBundle\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Wanawork\MainBundle\DataFixtures\DataLoader;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Wanawork\UserBundle\Entity\AdView;
use Wanawork\UserBundle\Entity\Billing\AdOrder;
use Wanawork\UserBundle\Entity\Billing\Payment;
use Wanawork\UserBundle\Entity\Billing\Transaction;

class LoadUsers extends DataLoader implements DependentFixtureInterface, ContainerAwareInterface
{

	private $container;
	
	private $em;
	
	public function load(ObjectManager $manager)
	{
	    $this->em = $manager;
		$file = __DIR__ . '/../data/users.json';
		$users = $this->loadFile($file);
		
		$aclProvider = $this->container->get('security.acl.provider');
		
		$objectIdentity = new ObjectIdentity('class', 'Wanawork\\UserBundle\\Entity\\User');
		try {
			$acl = $aclProvider->findAcl($objectIdentity);
		} catch (\Symfony\Component\Security\Acl\Exception\AclNotFoundException $e) {
			$acl = $aclProvider->createAcl($objectIdentity);
		}
		
		$acl->insertClassAce(new RoleSecurityIdentity(Role::ADMIN), MaskBuilder::MASK_OWNER);
		$aclProvider->updateAcl($acl);
		
		$accountsService = $this->container->get('wanawork.accounts_service');
		$employeeService = $this->container->get('wanawork.employee_service');
		$employerService = $this->container->get('wanawork.employer_service');
		$cvService       = $this->container->get('wanawork.cv_service');
		$adService       = $this->container->get('wanawork.ad_service');
		
		$this->container->get('security.context')->setToken(
			new UsernamePasswordToken('orm', null, 'secured_area', array('ROLE_ADMIN'))		
		);
		
		$aclManager = new AclManager($this->container->get('security.acl.provider'), $this->container->get('security.context'));
		$faker = \Faker\Factory::create('en_GB');
		
		$blankCandidate = new User();
		$blankCandidate->setEmail('blank_candidate@wanawork.ie');
		$blankCandidate->setPassword('123456');
		$blankCandidate->setName('Blank Candidate');
		$accountsService->register($blankCandidate, array($this->getReference('ROLE_EMPLOYEE')));
		
		// Employee With Profile
		$user = new User();
		$user->setEmail('test_employee@wanawork.ie');
		$user->setPassword('123456');
		$user->setName('Test Employee');
		$accountsService->register($user, array($this->getReference('ROLE_EMPLOYEE')));
		
		$profile = new EmployeeProfile();
		$user->setEmployeeProfile($profile);
		$profile->setName($user->getName());
		$profile->setDob(new \DateTime());
		$profile->setPhoneNumber(123456);
		$profile->setAddress('11111111');
		$profile->setCity('abc');
		$employeeService->createProfile($profile);
		
		$employer = null;
		foreach ($users as $user) {
			$userEntity = new User();
			$userEntity->setEmail($user['email']);
			$userEntity->setName($user['name']);
			
			$roles = array();
			foreach ($user['roles'] as $role) {
				$roles[] = $this->getReference($role);
			}
			$userEntity->setRoles($roles);
			$userEntity->setPassword($user['password']);
			
			$accountsService->register($userEntity, $roles);
			
			if(isset($user['profile'])) {
				$profile = $user['profile'];
				$title = $this->getReference('title-' . $profile['title']);
				$dob = new \DateTime($profile['dob']);
				$languages = array();
				foreach($profile['languages'] as $language) {
					$languages[] = $this->getReference("language-$language");
				}
				$address = $profile['address'];
				$city = $profile['city'];
				$county = $this->getReference("county-{$profile['county']}");
				$country = $this->getReference("country-{$profile['country']}");
				$name = $profile['name'];
				$phone = $profile['phoneNumber'];
				
				$manager->flush();
				
				$profileEntity = new EmployeeProfile();
				$userEntity->setEmployeeProfile($profileEntity);
				$profileEntity->setTitle($title);
				$profileEntity->setName($name);
				$profileEntity->setDob($dob);
				$profileEntity->setPhoneNumber($phone);
				$profileEntity->setAddress($address);
				$profileEntity->setCity($city);
				$profileEntity->setCounty($county);
				
				$employeeService->createProfile($profileEntity);
				$manager->flush();
			} 
			
			if($userEntity->hasRoleName('ROLE_EMPLOYER')) {
				$profile = new EmployerProfile();
				$profile->setCompanyName("Google");
				$profile->setContactName($faker->name);
				$profile->setEmail($faker->email);
				$profile->setPhoneNumber($faker->phoneNumber);
				$profile->setAddress($faker->address);
				$profile->setCity('Dublin');
				$employerService->createProfile($profile, $userEntity);
				$manager->flush();
				$employer = $userEntity;
				$this->setReference('employerProfile', $profile);
			}
		}
		
		$ads = array();
		// create some users with cvs and ads
		for($i = 0; $i < 5; $i++) {
			echo "Creating user $i\n";
			
			$user = $this->createUser();
			$profile = $this->createEmployeeProfile($user);
			
			
			echo "\tCreating ads ";
			for($a = 0, $m = rand(1,3); $a < $m; $a++) {
			    if($a + 1 < $m) {
			        echo "{$a}, ";
			    } else {
			        echo "\n";
			    }
			    
			    $ads[] = $ad1 = $this->createAd($profile, $publish = true);
			    $ads[] = $ad2 = $this->createAd($profile, $publish = false);
                $this->createCV($ad1);
                $this->createCV($ad2);
                			    
			}
			
			$manager->flush();
			//$manager->clear();
		}
		
		$cvRequests = array();
//		echo "Generating Ad Request\n";
// 		for($i = 0, $c = sizeof($ads); $i < $c; $i++) {
// 		    $ad = $ads[$i];
// 			$cvRequests[] = $ads[$i]->addAccessRequest($employer, $employer->getDefaultProfile()); 
// 			echo "$i, ";
// 		}
		$manager->flush();
		
// 		echo "\nAdding Mail Communication\n";
		
// 		$i = 0;
// 		foreach($cvRequests as $cvRequest) {
// 			if($i % 2 === 0) {
// 				$employeeService->approveCvRequest($cvRequest, $cvRequest->getAd()->getCv()->getProfile()->getUser());
				
// 				for($j = 0, $l = 6; $j < $l; $j++) {
					
// 					$thread = $cvRequest->getMailThread();
// 					if($j % 2 === 0) {
// 						$author = $cvRequest->getUser();
// 					} else {
// 						$author = $cvRequest->getAd()->getCv()->getProfile()->getUser();
// 					}
// 					$message = new Message($faker->text(), $author, $thread);
// 					$thread->getMessages()->add($message);
// 					$manager->flush();
// 					$aclManager->grant($message, MaskBuilder::MASK_OPERATOR, $author);
// 				}
// 			}
// 			++$i;
// 		}
// 		$manager->flush();
		
		// @todo Add views back
// 		echo "Adding Ad Views\n";
		
// 		$viewTimestamps = array();
// 		for($times = 0, $tm = 50; $times < $tm; $times++) {
// 			$dateTime = new \DateTime();
// 			$dateTime->setTimestamp(time() - 86400 * $times);
// 			$viewTimestamps[] = $dateTime;
// 		}
		
// 		foreach($ads as $ad) {
// 			foreach($viewTimestamps as $viewTimestamp) {
// 				for($i = 0, $m = rand(10,20); $i < $m; $i++) {
// 					$adView = $ad->addView(AdView::VIEW_IN_SEARCH, $employer);
// 					$adView->setTimestamp($viewTimestamp);
// 				}
// 			}
// 		}
		$manager->flush();
		
	}
	
	
	private function createUser()
	{
	    $accountsService = $this->container->get('wanawork.accounts_service');
	    
	    $faker = \Faker\Factory::create('en_GB');
	    $firstName = $faker->firstName;
	    $lastname = $faker->lastName;
	    $name = "$firstName $lastname";
	    	
	    $userEntity = new User();
	    $userEntity->setEmail($faker->userName . "@wannawork.ie");
	    $userEntity->setName($name);
	    $userEntity->setRoles(array($this->getReference("ROLE_EMPLOYEE")));
	    $userEntity->setPassword("123456");
	    	
	    $accountsService->register($userEntity, array($this->getReference("ROLE_EMPLOYEE")));
	    
	    return $userEntity;
	}
	
	private function createEmployeeProfile(User $user)
	{
	    $faker = \Faker\Factory::create('en_GB');
	    $employeeService = $this->container->get('wanawork.employee_service');
	    
	    $title = strtoupper(rtrim($faker->prefix, "."));
	    $title = $this->getReference('title-' . $title);
	    
	    $name =  $user->getName();
	    $dob = $faker->dateTime;
	    $phone = $faker->phoneNumber;
	    $languages = array($this->getReference("language-English"));
	    $address = $faker->address;
	    $city = "Dublin";
	    $county = $this->getReference("county-Dublin");
	    $country = $country = $this->getReference("country-Ireland");
	    	
	    $profile = new EmployeeProfile();
	    $user->setEmployeeProfile($profile);
	    $profile->setTitle($title);
	    $profile->setName($name);
	    $profile->setDob($dob);
	    $profile->setPhoneNumber($phone);
	    $profile->setAddress($address);
	    $profile->setCity($city);
	    $profile->setCounty($county);
	    $employeeService->createProfile($profile);
	    
	    return $profile;
	}
	
	private function createAd(EmployeeProfile $profile, $publish = true)
	{
	    $faker = \Faker\Factory::create('en_GB');
	    $manager = $this->em;
	    
	    $ad = new Ad();
	    $ad->setHeadline($faker->sentence);
	    $ad->setPitch($faker->text);
	    
	    $positionsRepository = $manager->getRepository('WanaworkUserBundle:PositionType');
	    $ad->setPositions($positionsRepository->findRandom(rand(1,4)));

	    $locationRepository = $manager->getRepository('WanaworkMainBundle:County');
	    $locations = $locationRepository->findRandom(rand(1,5));
	    $ad->setLocations($locations);
	    
	    $date = new \DateTime();
	    $ad->setCreatedAt($date);
	    
	    static $profession = null;
	    if($profession === null) {
	        $profession = $manager->getRepository('WanaworkUserBundle:SectorJob')->findRandom();
	    }
	    $ad->setProfession($profession);
	    
        $industries = $manager->getRepository('WanaworkUserBundle:Sector')->findRandom(rand(0,4));
        $ad->setIndustries($industries);
        
        $experience = $manager->getRepository('WanaworkUserBundle:SectorExperience')->findRandom();
        $ad->setExperience($experience);
	     
	    $languageRepository = $manager->getRepository('WanaworkMainBundle:Language');
	    $ad->setLanguages($languageRepository->findRandom(rand(1,5)));

	    $educationLevel = $manager->getRepository('WanaworkUserBundle:Qualification')->findRandom();
	    $ad->setEducationLevel($educationLevel);
	    $ad->setProfile($profile);
	    
	    $em = $this->em;
	    $em->getConnection()->beginTransaction();
	    
	    if ($publish)
	       $ad->publishStandard(60, 3);
	    
	    $em->persist($ad);
	    $em->flush();
	    $em->getConnection()->commit();
	    
	    return $ad;
	}
	
	private function createCV(Ad $ad)
	{
	    $faker = \Faker\Factory::create('en_GB');
	    $cvService = $this->container->get('wanawork.cv_service');
	    
	    $cv = new CVForm();
	    $cv->setName("Main CV");
	    for($j = 0; $j < rand(1, 3); $j++) {
	        $education = new Education();
	        $education->setStart(new \DateTime());
	        $education->setFinish(new \DateTime());
	        $education->setCollege("DCU");
	        $education->setCourse('Some Course');
	        $education->setAward("Some award");
	        $cv->addEducation($education);
	    }
	    
	    $workExperience = new WorkExperience();
	    $workExperience-> setStart(new \DateTime());
	    $workExperience->setFinish(new \DateTime());
	    $workExperience->setCompany($faker->company);
	    $workExperience->setPosition("Position here");
	    $workExperience->setDescription($faker->bs);
	    $cv->addJob($workExperience);
	    
	    $reference = new Reference();
	    $reference->setContact($faker->name);
	    $reference->setEmail($faker->email);
	    $reference->setEmployer($faker->company);
	    $reference->setTelephone($faker->phoneNumber);
	    $cv->addReference($reference);
	    
	    $cv->setAchievements('Test text here');
	    $cv->setCoverNote($faker->paragraph);
	    $cv->setProfile($ad->getProfile());
	    	
	    $ad->setCv($cv);
	    
	    $this->em->persist($cv);
	    
	    return $cv;
	}
	
	
	public function getDependencies()
	{
		return array(
		    'Wanawork\UserBundle\DataFixtures\Dev\ORM\LoadQualifications',
		    'Wanawork\UserBundle\DataFixtures\Dev\ORM\LoadPositionTypes',
		    'Wanawork\UserBundle\DataFixtures\Dev\ORM\LoadSectors',
		    'Wanawork\UserBundle\DataFixtures\Dev\ORM\LoadSectorJobs',
		    'Wanawork\UserBundle\DataFixtures\Dev\ORM\LoadSectorExperience',
			'Wanawork\UserBundle\DataFixtures\Dev\ORM\LoadRoles', 
			'Wanawork\MainBundle\DataFixtures\Dev\ORM\LoadCountryData',
			'Wanawork\MainBundle\DataFixtures\Dev\ORM\LoadCountyData',
			'Wanawork\MainBundle\DataFixtures\Dev\ORM\LoadTitleData',
			'Wanawork\MainBundle\DataFixtures\Dev\ORM\LoadLanguageData',
		);
	}
	
	public function setContainer(ContainerInterface $container = null) {
		$this->container = $container;		
	}
}

