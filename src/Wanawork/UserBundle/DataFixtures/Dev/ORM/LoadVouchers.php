<?php
namespace Wanawork\UserBundle\DataFixtures\Dev\ORM;

use Wanawork\MainBundle\DataFixtures\DataLoader;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Wanawork\UserBundle\Entity\Billing\Voucher\AdVoucher;

class LoadVouchers extends DataLoader implements ContainerAwareInterface
{
    private $container;
    
    public function load(ObjectManager $manager)
    {
        $insertCount = 0;
        $repository = $manager->getRepository('Wanawork\UserBundle\Entity\Billing\Voucher\Voucher');
        $voucher = $repository->findOneByCode("Wannawork1");
        if($voucher === null) {
            $voucher = new AdVoucher($code = 'Wannawork1');
            $voucher->setUserLimit($userLimit = 2);
            $manager->persist($voucher);
            $manager->flush();
            ++$insertCount;
        }
    
        echo sprintf(
            "Sectors result - Processed: %d, Inserted: %d, Updated: %d, Deleted: %d\n",
            1, $insertCount, 0, 0
        );
    }
    
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
}