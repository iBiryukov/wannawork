<?php
namespace Wanawork\UserBundle\DataFixtures\Dev\ORM;

use Wanawork\MainBundle\DataFixtures\DataLoader;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Wanawork\UserBundle\Entity\JobSpec;

class LoadJobSpecs extends DataLoader implements ContainerAwareInterface, DependentFixtureInterface
{
    private $container;
    
    public function load(ObjectManager $manager)
    {
        $professions = $manager->getRepository('WanaworkUserBundle:SectorJob')->findAll();
	    $industries = $manager->getRepository('WanaworkUserBundle:Sector')->findAll();
	    $locations = $manager->getRepository('WanaworkMainBundle:County')->findAll();
	    $positions = $manager->getRepository('WanaworkUserBundle:PositionType')->findAll();
        $educationLevels = $manager->getRepository('WanaworkUserBundle:Qualification')->findAll();
	    $languages = $manager->getRepository('WanaworkMainBundle:Language')->findAll();
        $experiences = $manager->getRepository('WanaworkUserBundle:SectorExperience')->findAll();
        $employerProfile = $this->getReference('employerProfile');
        
        $faker = \Faker\Factory::create('en_GB');
        foreach ($professions as $profession) {
            $maxJobs = rand(1, 5);
            for($i = 0; $i <= $maxJobs; $i++) {
                $jobSpec = new JobSpec();
                $jobSpec->setEmployer($employerProfile);
                $jobSpec->setUser($employerProfile->getUsers()->first());
                $jobSpec->setDescription(implode('<br />', $faker->paragraphs(6)));
                $jobSpec->setTitle($faker->userName);
                $jobSpec->setProfession($profession);
                $manager->persist($jobSpec);
            }
        }
        foreach ($industries as $industry) {
            $maxJobs = rand(1, 5);
            for($i = 0; $i <= $maxJobs; $i++) {
                $jobSpec = new JobSpec();
                $jobSpec->setEmployer($employerProfile);
                $jobSpec->setUser($employerProfile->getUsers()->first());
                $jobSpec->setDescription(implode('<br />', $faker->paragraphs(6)));
                $jobSpec->setTitle($faker->userName);
                $jobSpec->addIndustry($industry);
                $manager->persist($jobSpec);
            }
        }
        foreach ($locations as $location) {
            $maxJobs = rand(1, 5);
            for($i = 0; $i <= $maxJobs; $i++) {
                $jobSpec = new JobSpec();
                $jobSpec->setEmployer($employerProfile);
                $jobSpec->setUser($employerProfile->getUsers()->first());
                $jobSpec->setDescription(implode('<br />', $faker->paragraphs(6)));
                $jobSpec->setTitle($faker->userName);
                $jobSpec->addLocation($location);
                $manager->persist($jobSpec);
            }
        }
        foreach ($positions as $position) {
            $maxJobs = rand(1, 5);
            for($i = 0; $i <= $maxJobs; $i++) {
                $jobSpec = new JobSpec();
                $jobSpec->setEmployer($employerProfile);
                $jobSpec->setUser($employerProfile->getUsers()->first());
                $jobSpec->setDescription(implode('<br />', $faker->paragraphs(6)));
                $jobSpec->setTitle($faker->userName);
                $jobSpec->addPosition($position);
                $manager->persist($jobSpec);
            }
        }
        foreach ($educationLevels as $educationLevel) {
            $maxJobs = rand(1, 5);
            for($i = 0; $i <= $maxJobs; $i++) {
                $jobSpec = new JobSpec();
                $jobSpec->setEmployer($employerProfile);
                $jobSpec->setUser($employerProfile->getUsers()->first());
                $jobSpec->setDescription(implode('<br />', $faker->paragraphs(6)));
                $jobSpec->setTitle($faker->userName);
                $jobSpec->setEducationLevel($educationLevel);
                $manager->persist($jobSpec);
            }
        }
        foreach ($languages as $language) {
            $maxJobs = rand(1, 5);
            for($i = 0; $i <= $maxJobs; $i++) {
                $jobSpec = new JobSpec();
                $jobSpec->setEmployer($employerProfile);
                $jobSpec->setUser($employerProfile->getUsers()->first());
                $jobSpec->setDescription(implode('<br />', $faker->paragraphs(6)));
                $jobSpec->setTitle($faker->userName);
                $jobSpec->addLanguage($language);
                $manager->persist($jobSpec);
            }
        }
        foreach ($experiences as $experience) {
            $maxJobs = rand(1, 5);
            for($i = 0; $i <= $maxJobs; $i++) {
                $jobSpec = new JobSpec();
                $jobSpec->setEmployer($employerProfile);
                $jobSpec->setUser($employerProfile->getUsers()->first());
                $jobSpec->setDescription(implode('<br />', $faker->paragraphs(6)));
                $jobSpec->setTitle($faker->userName);
                $jobSpec->setExperience($experience);
                $manager->persist($jobSpec);
            }
        }
        
        for($i = 0; $i <= 100; $i++) {
            $jobSpec = new JobSpec();
            $jobSpec->setEmployer($employerProfile);
            $jobSpec->setUser($employerProfile->getUsers()->first());
            $jobSpec->setDescription(implode('<br />', $faker->paragraphs(6)));
            $jobSpec->setTitle($faker->userName);
            $jobSpec->setProfession($profession);
            $jobSpec->addIndustry($industry);
            $jobSpec->addLocation($location);
            $jobSpec->addPosition($position);
            $jobSpec->setEducationLevel($educationLevel);
            $jobSpec->addLanguage($language);
            $jobSpec->setExperience($experience);
            
            $manager->persist($jobSpec);
        }
        $manager->flush();
    }
    
	public function setContainer(ContainerInterface $container = null)
	{
        $this->container = $container;
	}

	public function getDependencies()
	{
	    return array(
	    	'Wanawork\UserBundle\DataFixtures\Dev\ORM\LoadSectorJobs',
	        'Wanawork\UserBundle\DataFixtures\Dev\ORM\LoadSectors',
	        'Wanawork\UserBundle\DataFixtures\Dev\ORM\LoadUsers',
	    );
	}

}