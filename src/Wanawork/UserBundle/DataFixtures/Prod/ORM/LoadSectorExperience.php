<?php
namespace Wanawork\UserBundle\DataFixtures\ORM\Prod;
use Wanawork\UserBundle\Entity\SectorExperience;

use Symfony\Component\Security\Acl\Model\AclInterface;

use Doctrine\Common\Persistence\ObjectManager;

use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Symfony\Component\Security\Acl\Domain\RoleSecurityIdentity;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Wanawork\UserBundle\Entity\PositionType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Wanawork\MainBundle\DataFixtures\DataLoader;
use Wanawork\UserBundle\Interfaces\Importer;

class LoadSectorExperience extends DataLoader implements ContainerAwareInterface {
	
	private $container;
	
	public function load(ObjectManager $manager)
	{
		$file = new \SplFileInfo(__DIR__ . '/../data/sector_experience.json');
		$trader = $this->container->get('wanawork.sector_experience_trader');
		$result = $trader->import($file);
			
		echo sprintf(
		    "Sector experience result - Processed: %d, Inserted: %d, Updated: %d, Deleted: %d\n",
		    $result[Importer::PROCESSED], $result[Importer::INSERTED], $result[Importer::UPDATED],
		    $result[Importer::DELETED]
		);
	}
	
	public function setContainer(ContainerInterface $container = null)
	{
		$this->container = $container;
	}
}

