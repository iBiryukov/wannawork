<?php
namespace Wanawork\UserBundle\DataFixtures\ORM\Prod;
use Symfony\Component\Security\Acl\Model\AclInterface;

use Doctrine\Common\Persistence\ObjectManager;

use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Symfony\Component\Security\Acl\Domain\RoleSecurityIdentity;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Wanawork\UserBundle\Entity\PositionType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Wanawork\MainBundle\DataFixtures\DataLoader;
use Wanawork\UserBundle\Interfaces\Importer;

class LoadPositionTypes extends DataLoader implements ContainerAwareInterface
{
	private $container;
	
	public function load(ObjectManager $manager)
	{
		$file = new \SplFileInfo(__DIR__ . '/../data/position_type.json');
		$trader = $this->container->get('wanawork.position_type_trader');
		$result = $trader->import($file);
		
	    echo sprintf(
	        "PositionType result - Processed: %d, Inserted: %d, Updated: %d, Deleted: %d\n",
	        $result[Importer::PROCESSED], $result[Importer::INSERTED], $result[Importer::UPDATED],
	        $result[Importer::DELETED]
	    );
	}
	
	public function setContainer(ContainerInterface $container = null)
	{
		$this->container = $container;
	}
}

