<?php
namespace Wanawork\UserBundle\DataFixtures\Prod\ORM;

use Wanawork\MainBundle\DataFixtures\DataLoader;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Wanawork\UserBundle\Entity\User;
use Wanawork\UserBundle\Entity\Role;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Domain\RoleSecurityIdentity;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;

class LoadUsers extends DataLoader implements DependentFixtureInterface, ContainerAwareInterface
{
    
	private $container;
	
	
	public function load(ObjectManager $manager)
	{
	    $aclProvider = $this->container->get('security.acl.provider');
	    
	    $objectIdentity = new ObjectIdentity('class', 'Wanawork\\UserBundle\\Entity\\User');
	    try {
	        $acl = $aclProvider->findAcl($objectIdentity);
	    } catch (\Symfony\Component\Security\Acl\Exception\AclNotFoundException $e) {
	        $acl = $aclProvider->createAcl($objectIdentity);
	    }
	    $acl->insertClassAce(new RoleSecurityIdentity(Role::ADMIN), MaskBuilder::MASK_OWNER);
	    $aclProvider->updateAcl($acl);

	    if (!$manager->getRepository('WanaworkUserBundle:User')->findOneByEmail('admin@wannawork.ie')) {
	        $user = new User();
	        $user->setEmail('admin@wannawork.ie');
	        $user->setPassword('wannaworkadmin14');
	        $user->setName('Wannawork Admin');
	        $user->addRole($this->getReference(Role::ADMIN));
	        
	        $encoderFactory = $this->container->get('security.encoder_factory');
	        $encoder = $encoderFactory->getEncoder($user);
	        $password = $encoder->encodePassword($user->getPassword(), $user->getSalt());
	        $user->setPassword($password);
	         
	        $manager->persist($user);
	        $manager->flush();
	    }
	    
	    //$accountsService->register($user, array($this->getReference(Role::ADMIN)));
	}
	
	public function getDependencies()
	{
		return array(
			'Wanawork\UserBundle\DataFixtures\Prod\ORM\LoadRoles', 
			'Wanawork\MainBundle\DataFixtures\Prod\ORM\LoadCountryData',
			'Wanawork\MainBundle\DataFixtures\Prod\ORM\LoadCountyData',
			'Wanawork\MainBundle\DataFixtures\Prod\ORM\LoadTitleData',
			'Wanawork\MainBundle\DataFixtures\Prod\ORM\LoadLanguageData',
		);
	}
	
	public function setContainer(ContainerInterface $container = null) {
		$this->container = $container;		
	}
}

