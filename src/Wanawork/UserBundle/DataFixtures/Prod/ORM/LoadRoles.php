<?php
namespace Wanawork\UserBundle\DataFixtures\Prod\ORM;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Symfony\Component\Security\Acl\Domain\RoleSecurityIdentity;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Wanawork\UserBundle\Entity\Role;
use Doctrine\Common\Persistence\ObjectManager;
use Wanawork\MainBundle\DataFixtures\DataLoader;
use Wanawork\UserBundle\Interfaces\Importer;

class LoadRoles extends DataLoader implements ContainerAwareInterface
{
	private $container; 
	
	public function load(ObjectManager $manager) 
	{
		$file = new \SplFileInfo(__DIR__ . '/../data/roles.json');
		
		$trader = $this->container->get('wanawork.role_trader');
		$result = $trader->import($file, function(Role $roleEntity){
			$this->addReference($roleEntity->getRole(), $roleEntity);
        });
		
        $aclProvider = $this->container->get('security.acl.provider');
	    // Employee Profile
	    $objectIdentity = new ObjectIdentity('class', 'Wanawork\\UserBundle\\Entity\\EmployeeProfile');
	    
	    try {
	        $acl = $aclProvider->findAcl($objectIdentity);
	    } catch (\Symfony\Component\Security\Acl\Exception\AclNotFoundException $e) {
	        $acl = $aclProvider->createAcl($objectIdentity);
	    }
	    
	    $acl->insertClassAce(new RoleSecurityIdentity(Role::EMPLOYEE), MaskBuilder::MASK_CREATE);
	    $aclProvider->updateAcl($acl);
	    
	    // Employee profile admin
	    $acl->insertClassAce(new RoleSecurityIdentity(Role::ADMIN), MaskBuilder::MASK_OWNER);
	    $aclProvider->updateAcl($acl);
	    
	    // CV Form
	    $objectIdentity = new ObjectIdentity('class', 'Wanawork\\UserBundle\\Entity\\CVForm');
	    try {
	        $acl = $aclProvider->findAcl($objectIdentity);
	    } catch (\Symfony\Component\Security\Acl\Exception\AclNotFoundException $e) {
	        $acl = $aclProvider->createAcl($objectIdentity);
	    }
	    
	    // cv admin
	    $acl->insertClassAce(new RoleSecurityIdentity(Role::ADMIN), MaskBuilder::MASK_OWNER);
	    $aclProvider->updateAcl($acl);
	    
	    // CV File
	    $objectIdentity = new ObjectIdentity('class', 'Wanawork\\UserBundle\\Entity\\CVFile');
	    try {
	        $acl = $aclProvider->findAcl($objectIdentity);
	    } catch (\Symfony\Component\Security\Acl\Exception\AclNotFoundException $e) {
	        $acl = $aclProvider->createAcl($objectIdentity);
	    }
	    $acl->insertClassAce(new RoleSecurityIdentity(Role::EMPLOYEE), MaskBuilder::MASK_CREATE);
	    $aclProvider->updateAcl($acl);
	    
	    // cv admin
	    $acl->insertClassAce(new RoleSecurityIdentity(Role::ADMIN), MaskBuilder::MASK_OWNER);
	    $aclProvider->updateAcl($acl);
	    
	    // Ad
	    $objectIdentity = new ObjectIdentity('class', 'Wanawork\\UserBundle\\Entity\\Ad');
	    try {
	        $acl = $aclProvider->findAcl($objectIdentity);
	    } catch (\Symfony\Component\Security\Acl\Exception\AclNotFoundException $e) {
	        $acl = $aclProvider->createAcl($objectIdentity);
	    }
	    $acl->insertClassAce(new RoleSecurityIdentity(Role::EMPLOYEE), MaskBuilder::MASK_CREATE);
	    $aclProvider->updateAcl($acl);
	    
	    // ad admin
	    $acl->insertClassAce(new RoleSecurityIdentity(Role::ADMIN), MaskBuilder::MASK_OWNER);
	    $aclProvider->updateAcl($acl);
	    
	    // Employer Profile
	    $objectIdentity = new ObjectIdentity('class', 'Wanawork\\UserBundle\\Entity\\EmployerProfile');
	    try {
	        $acl = $aclProvider->findAcl($objectIdentity);
	    } catch (\Symfony\Component\Security\Acl\Exception\AclNotFoundException $e) {
	        $acl = $aclProvider->createAcl($objectIdentity);
	    }
	    
	    $acl->insertClassAce(new RoleSecurityIdentity(Role::EMPLOYER), MaskBuilder::MASK_CREATE);
	    $aclProvider->updateAcl($acl);
	    
	    // Employer profile admin
	    $acl->insertClassAce(new RoleSecurityIdentity(Role::ADMIN), MaskBuilder::MASK_OWNER);
	    $aclProvider->updateAcl($acl);
	    
	    // Search. Employer
	    $objectIdentity = new ObjectIdentity('class', 'Wanawork\\UserBundle\\Entity\\Search');
	    try {
	        $acl = $aclProvider->findAcl($objectIdentity);
	    } catch (\Symfony\Component\Security\Acl\Exception\AclNotFoundException $e) {
	        $acl = $aclProvider->createAcl($objectIdentity);
	    }
	    $acl->insertClassAce(new RoleSecurityIdentity(Role::EMPLOYER), MaskBuilder::MASK_CREATE);
	    $aclProvider->updateAcl($acl);
	    
	    // Search. Admin
	    $acl->insertClassAce(new RoleSecurityIdentity(Role::ADMIN), MaskBuilder::MASK_OWNER);
	    $aclProvider->updateAcl($acl);
	    
	    // Mail Thread. Admin
	    $objectIdentity = new ObjectIdentity('class', 'Wanawork\\UserBundle\\Entity\\MailThread');
	    try {
	        $acl = $aclProvider->findAcl($objectIdentity);
	    } catch (\Symfony\Component\Security\Acl\Exception\AclNotFoundException $e) {
	        $acl = $aclProvider->createAcl($objectIdentity);
	    }
	    $acl->insertClassAce(new RoleSecurityIdentity(Role::ADMIN), MaskBuilder::MASK_OWNER);
	    $aclProvider->updateAcl($acl);
	    
	    // Message. Admin
	    $objectIdentity = new ObjectIdentity('class', 'Wanawork\\UserBundle\\Entity\\Message');
	    try {
	        $acl = $aclProvider->findAcl($objectIdentity);
	    } catch (\Symfony\Component\Security\Acl\Exception\AclNotFoundException $e) {
	        $acl = $aclProvider->createAcl($objectIdentity);
	    }
	    $acl->insertClassAce(new RoleSecurityIdentity(Role::ADMIN), MaskBuilder::MASK_OWNER);
	    $aclProvider->updateAcl($acl);
		
		echo sprintf(
		    "Role result - Processed: %d, Inserted: %d, Updated: %d, Deleted: %d\n",
		    $result[Importer::PROCESSED], $result[Importer::INSERTED], $result[Importer::UPDATED],
		    $result[Importer::DELETED]
		);
	}
	
	public function setContainer(ContainerInterface $container = null)
	{
		$this->container = $container;
	}
}
