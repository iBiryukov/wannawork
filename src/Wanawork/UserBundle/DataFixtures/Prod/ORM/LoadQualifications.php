<?php
namespace Wanawork\UserBundle\DataFixtures\ORM\Prod;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Wanawork\UserBundle\Entity\Qualification;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Wanawork\MainBundle\DataFixtures\DataLoader;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Symfony\Component\Security\Acl\Domain\RoleSecurityIdentity;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Wanawork\UserBundle\Entity\Role;
use Doctrine\Common\Persistence\ObjectManager;
use Wanawork\UserBundle\Interfaces\Importer;

class LoadQualifications extends DataLoader implements ContainerAwareInterface {
	
	private $container;
	
	public function load(ObjectManager $manager)
	{
		$file = new \SplFileInfo(__DIR__ . '/../data/qualifications.json');
        		
		$trader = $this->container->get('wanawork.qualifications_trader');
		$result = $trader->import($file);
		 
		echo sprintf(
		    "Sectors result - Processed: %d, Inserted: %d, Updated: %d, Deleted: %d\n",
		    $result[Importer::PROCESSED], $result[Importer::INSERTED], $result[Importer::UPDATED],
		    $result[Importer::DELETED]
		);
		
	}
	
	public function setContainer(ContainerInterface $container = null)
	{
		$this->container = $container;
	}
}
