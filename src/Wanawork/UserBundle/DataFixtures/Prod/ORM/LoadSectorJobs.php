<?php
namespace Wanawork\UserBundle\DataFixtures\Prod\ORM;

use Wanawork\MainBundle\DataFixtures\DataLoader;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Wanawork\UserBundle\Interfaces\Importer;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class LoadSectorJobs extends DataLoader implements ContainerAwareInterface, DependentFixtureInterface
{
    private $container;
    
    public function load(ObjectManager $manager)
    {
        $file = new \SplFileInfo(__DIR__ . '/../data/sector_jobs.json');

        $trader = $this->container->get('wanawork.sector_jobs_trader');
        $result = $trader->import($file);
         
        echo sprintf(
            "Sector Jobs result - Processed: %d, Inserted: %d, Updated: %d, Deleted: %d\n",
            $result[Importer::PROCESSED], $result[Importer::INSERTED], $result[Importer::UPDATED],
            $result[Importer::DELETED]
        );
        
    }
    
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    
    public function getDependencies()
    {
        return array(
            'Wanawork\UserBundle\DataFixtures\Prod\ORM\LoadSectors',
        );
    }
}