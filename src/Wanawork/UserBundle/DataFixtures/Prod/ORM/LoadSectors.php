<?php
namespace Wanawork\UserBundle\DataFixtures\Prod\ORM;

use Wanawork\UserBundle\Entity\Sector;
use Wanawork\UserBundle\Entity\SectorJob;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Symfony\Component\Security\Acl\Domain\RoleSecurityIdentity;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Wanawork\MainBundle\DataFixtures\DataLoader;
use Wanawork\UserBundle\Interfaces\Importer;

class LoadSectors extends DataLoader implements ContainerAwareInterface 
{
	private $container;
	
	public function load(ObjectManager $manager)
	{
		$file = new \SplFileInfo(__DIR__ . '/../data/sectors.json');
	    $trader = $this->container->get('wanawork.sectors_trader');
	    $result = $trader->import($file);
	    
	    echo sprintf(
	        "Sectors result - Processed: %d, Inserted: %d, Updated: %d, Deleted: %d\n",
	        $result[Importer::PROCESSED], $result[Importer::INSERTED], $result[Importer::UPDATED],
	        $result[Importer::DELETED]
	    );
	}
	
	
	public function setContainer(ContainerInterface $container = null) 
	{
		$this->container = $container;
	}
}

