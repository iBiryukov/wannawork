function getCookieVal(offset) {
	var endstr = document.cookie.indexOf(";", offset);
	if (endstr == -1) {
		endstr = document.cookie.length;
	}
	return unescape(document.cookie.substring(offset, endstr));
}
function GetCookie(name) {
	var arg = name + "=";
	var alen = arg.length;
	var clen = document.cookie.length;
	if ((document.cookie == null) || (document.cookie.length == null)) {
		return null;
	}
	var i = 0;
	while (i < clen) {
		var j = i + alen;
		if (document.cookie.substring(i, j) == arg) {
			return getCookieVal(j);
		}
		i = document.cookie.indexOf(" ", i) + 1;
		if (i == 0) {
			break;
		}
	}
	return null;
}
var groupName = null;
var hostname = document.location.hostname;
hostname = hostname.replace(/(\.au|\.nz)\.(.*)/, "$1");
switch (hostname) {
case "seek.co.nz":
case "www.seek.co.nz":
case "it.seek.co.nz":
case "www.it.seek.co.nz":
case "yahooxtra.seek.co.nz":
case "yahooxtra.it.seek.co.nz":
case "msn.seek.co.nz":
case "msn.it.seek.co.nz":
	groupName = "seeknz";
	break;
case "xtramsn.seek.co.nz":
case "www.xtramsn.seek.co.nz":
case "xtramsn.it.seek.co.nz":
case "www.xtramsn.it.seek.co.nz":
	groupName = "xtramsn";
	break;
case "jwren.com.au":
case "www.jwren.com.au":
case "jobs.jwren.com.au":
case "www.jobs.jwren.com.au":
	groupName = "jwren";
	break;
case "iconrec.com.au":
case "www.iconrec.com.au":
case "jobs.iconrec.com.au":
case "www.jobs.iconrec.com.au":
	groupName = "iconrec";
	break;
case "adecco.com.au":
case "www.adecco.com.au":
case "jobs.adecco.com.au":
case "www.jobs.adecco.com.au":
	groupName = "adecco";
	break;
case "seek.com.au":
case "www.seek.com.au":
case "executive.seek.com.au":
case "it.seek.com.au":
case "ninemsn.seek.com.au":
case "ninemsn.executive.seek.com.au":
case "ninemsn.it.seek.com.au":
default:
	groupName = "seek";
}
var loggedIn = GetCookie("SiteGroup." + groupName);
var username = GetCookie("JSUN." + groupName);
if (username == null) {
	username = "";
}
Array.insert = function(a, b, c) {
	a.splice(b, 0, c);
};
function GetObjectByName(name) {
	var obj;
	for (i = 0; i < document.forms.length; i++) {
		if (obj = eval("document.forms[i].elements['" + name + "']")) {
			return obj;
			break;
		}
	}
	return null;
}
function Find(TheItem) {
	var obj = GetObjectByName(TheItem);
	return obj;
}
function GetObjectByPartName(ctlName, matchIndex) {
	var obj;
	var elementnumber;
	var elementname;
	var numberOfMatches = 0;
	var match = $("#" + ctlName).length;
	if (match > 0) {
		if (matchIndex == undefined || match == matchIndex) {
			obj = $("#" + ctlName)[match - 1];
		}
	}
	return obj;
}
function KeywordHints() {
}
var IsDefaultEnterPressBehaviourOverridden = false;
var ButtonToClickWhenEnterPressed = null;
function ConnectEnterKeyToButton(buttonName) {
	if (buttonName == null) {
		SetIsDefaultEnterPressBehaviourOverridden(false);
	} else {
		SetIsDefaultEnterPressBehaviourOverridden(true);
		SetButtonToClickWhenEnterPressed(buttonName);
	}
}
function MapEnterPressToButtonClick() {
	if (GetIsDefaultEnterPressBehaviourOverridden()) {
		if (document.all) {
			DestroyEvent();
		}
		return FireOffAlternateButtonClick();
	} else {
		document.layout.onsubmit();
	}
}
function TrapAndHandleEnterKeyPress(e) {
	e = e ? e : window.event;
	var keyCodeEntered = e.keyCode ? e.keyCode : e.which ? e.which : null;
	if (keyCodeEntered == 13) {
		return MapEnterPressToButtonClick();
	}
	return true;
}
function DestroyEvent() {
	event.returnValue = false;
	event.cancel = true;
}
function FireOffAlternateButtonClick() {
	var ButtonToClick = null;
	if (GetButtonToClickWhenEnterPressed() != null) {
		if (document.all) {
			try {
				ButtonToClick = eval("document.all."
						+ GetButtonToClickWhenEnterPressed());
			} catch (e) {
				ButtonToClick = eval("document.all['"
						+ GetButtonToClickWhenEnterPressed() + "']");
			}
			return InvokeClick(ButtonToClick);
		} else {
			ButtonToClick = document
					.getElementById(GetButtonToClickWhenEnterPressed());
			if (ButtonToClick != null) {
				return InvokeClick(ButtonToClick);
			} else {
				ButtonToClick = document
						.getElementsByName(GetButtonToClickWhenEnterPressed());
				return InvokeClick(ButtonToClick[0]);
			}
		}
	}
}
function InvokeClick(obj) {
	if (obj != null) {
		if (obj.click) {
			return obj.click();
		} else {
			if (obj.onclick) {
				return obj.onclick();
			} else {
				$(obj).trigger("click");
			}
		}
	}
}
function GetIsDefaultEnterPressBehaviourOverridden() {
	return IsDefaultEnterPressBehaviourOverridden;
}
function SetIsDefaultEnterPressBehaviourOverridden(paramValue) {
	IsDefaultEnterPressBehaviourOverridden = paramValue;
}
function GetButtonToClickWhenEnterPressed() {
	return ButtonToClickWhenEnterPressed;
}
function SetButtonToClickWhenEnterPressed(paramValue) {
	ButtonToClickWhenEnterPressed = paramValue;
}
if (typeof aCatSelected == "undefined") {
	var aCatSelected = new Array(0);
}
if (typeof aDefaultOverrides == "undefined") {
	var aDefaultOverrides = new Array(0);
}
if (typeof isAdvancedKw == "undefined") {
	var isAdvancedKw = false;
}
var defaultKeywords = "Enter keyword(s)";
var isFirst = true;
var locationsTextCollection = [];
$(document)
		.ready(
				function() {
					$("#AdvSearchLink").click(function() {
						controlPrefix = "cat";
						updateSearchFrom(this);
						changeToAdvancedSearch();
						checkOnsites();
					});
					$("#bottomAdvSearchLink").click(function() {
						updateSearchFrom(this);
						controlPrefix = "bottom";
						changeToAdvancedSearch();
					});
					$("#FewerOptionsBottom")
							.click(
									function() {
										updateSearchFrom(this);
										controlPrefix = "cat";
										if (typeof (setTestSwitch) != "undefined"
												&& (setTestSwitch)) {
											_saved_jobsearchDisplayOrderType = "testingtarget";
											changeSearchOptions("quick",
													isCurrentSiteExecutive);
										} else {
											changeToQuickSearch();
										}
										checkOnsites();
									});
					$("#bottomFewerOptionsBottom").click(function() {
						updateSearchFrom(this);
						controlPrefix = "bottom";
						changeToQuickSearch();
					});
					$("#FewerOptionsTop")
							.click(
									function() {
										updateSearchFrom(this);
										controlPrefix = "cat";
										if (typeof (setTestSwitch) != "undefined"
												&& (setTestSwitch)) {
											_saved_jobsearchDisplayOrderType = "testingtarget";
											changeSearchOptions("quick",
													isCurrentSiteExecutive);
										} else {
											changeToQuickSearch();
										}
										checkOnsites();
									});
					$("#bottomFewerOptionsTop").click(function() {
						updateSearchFrom(this);
						controlPrefix = "bottom";
						changeToQuickSearch();
					});
					$("#DoSearch").click(function() {
						if (ValidateBeforePost("advanced", "upper")) {
							$("#layout").submit();
						}
						return false;
					});
					$("#bottomDoSearch").click(function() {
						if (ValidateBeforePost("advanced", "lower")) {
							$("#bottomlayout").submit();
						}
						return false;
					});
					$("#Keywords").focus(function() {
						controlPrefix = "cat";
						KeywordsFocus("#Keywords");
					});
					$("#Keywords").blur(function() {
						KeywordsBlur("#Keywords");
					});
					$("#bottomKeywords").focus(function() {
						controlPrefix = "bottom";
						KeywordsFocus("#bottomKeywords");
					});
					$("#bottomKeywords").blur(function() {
						KeywordsBlur("#bottomKeywords");
					});
					$("#catindustry, #bottomindustry").change(
							function() {
								ValidateCategoryList("industry", 0,
										"occupation", true);
							});
					$("#catindustry").focus(function() {
						controlPrefix = "cat";
						ConnectEnterKeyToButton("DoSearch");
					});
					$("#bottomindustry").focus(function() {
						controlPrefix = "bottom";
						ConnectEnterKeyToButton("bottomDoSearch");
					});
					$("#catoccupation, #bottomoccupation").change(
							function() {
								ValidateCategoryList("occupation", 0,
										"specialisation", true);
								disableSubFields();
							});
					$("#catoccupation").focus(function() {
						controlPrefix = "cat";
						ConnectEnterKeyToButton("DoSearch");
					});
					$("#bottomoccupation").focus(function() {
						controlPrefix = "bottom";
						ConnectEnterKeyToButton("bottomDoSearch");
					});
					$("#catparentlocation, #bottomparentlocation").change(
							function() {
								ValidateCategoryList("parentlocation", 0,
										"childlocation", true);
								disableSubFields();
								UpdateSubClassifications(this);
								populateSalary();
								populateLocationText(this);
							});
					$("#catparentlocation").focus(function() {
						controlPrefix = "cat";
						ConnectEnterKeyToButton("DoSearch");
					});
					$("#bottomparentlocation").focus(function() {
						controlPrefix = "bottom";
						ConnectEnterKeyToButton("bottomDoSearch");
					});
					$("#catchildlocation, #bottomchildlocation").change(
							function() {
								ValidateCategoryList("childlocation", 0, "",
										false);
								UpdateSubClassifications(this);
								populateSalary();
								UpdateParent(this, "parentlocation");
							});
					$("#catchildlocation").focus(function() {
						controlPrefix = "cat";
						ConnectEnterKeyToButton("DoSearch");
					});
					$("#bottomchildlocation").focus(function() {
						controlPrefix = "bottom";
						ConnectEnterKeyToButton("bottomDoSearch");
					});
					$("#salaryfrom, #salaryrefinefrom")
							.change(
									function() {
										controlPrefix = "cat";
										setSalaryType("#" + this.id,
												"#salaryType");
										updateSalary(
												this,
												(this.id.indexOf("refine") !== -1) ? "refine"
														: null);
									});
					$("#bottomsalaryfrom").change(
							function() {
								controlPrefix = "bottom";
								setSalaryType("#bottomsalaryfrom",
										"#bottomsalaryType");
								updateSalary(this);
							});
					$("#salaryfrom, #salaryrefinefrom").focus(function() {
						controlPrefix = "cat";
						ConnectEnterKeyToButton("DoSearch");
					});
					$("#bottomsalaryfrom").focus(function() {
						controlPrefix = "bottom";
						ConnectEnterKeyToButton("bottomDoSearch");
					});
					$("#salaryto, #salaryrefineto")
							.change(
									function() {
										controlPrefix = "cat";
										setSalaryType("#" + this.id,
												"#salaryType");
										updateSalary(
												this,
												(this.id.indexOf("refine") !== -1) ? "refine"
														: null);
									});
					$("#bottomsalaryto").change(function() {
						controlPrefix = "bottom";
						setSalaryType("#bottomsalaryto", "#bottomsalaryType");
						updateSalary(this);
					});
					$("#salaryto, #salaryrefineto").focus(function() {
						controlPrefix = "cat";
						ConnectEnterKeyToButton("DoSearch");
					});
					$("#bottomsalaryto").focus(function() {
						controlPrefix = "bottom";
						ConnectEnterKeyToButton("bottomDoSearch");
					});
					$("#salaryDisabled").change(function() {
						disabledSearchSalary(this);
					});
					$("#bottomsalaryDisabled").change(function() {
						bottomdisabledSearchSalary(this);
					});
					$("#ChkUnspecifiedchildlocation").focus(function() {
						ConnectEnterKeyToButton("DoSearch");
					});
					$("#bottomChkUnspecifiedchildlocation").focus(function() {
						ConnectEnterKeyToButton("bottomDoSearch");
					});
					$("#catworktype, #bottomworktype").change(function() {
						ValidateCategoryList("worktype", 0, "", false);
					});
					$("#catworktype").focus(function() {
						controlPrefix = "cat";
						ConnectEnterKeyToButton("DoSearch");
					});
					$("#bottomworktype").focus(function() {
						controlPrefix = "bottom";
						ConnectEnterKeyToButton("bottomDoSearch");
					});
					$("#DateRange").focus(function() {
						controlPrefix = "cat";
						ConnectEnterKeyToButton("DoSearch");
					});
					$("#bottomDateRange").focus(function() {
						controlPrefix = "bottom";
						ConnectEnterKeyToButton("bottomDoSearch");
					});
					$("#ClosePostcode").click(function(e) {
						e.preventDefault();
						hidePostCodeSearch();
					});
					$("#PostCodeButton").click(function(e) {
						e.preventDefault();
						$("#PostCodeSearch").show();
					});
					$("#bottomClosePostcode").click(function(e) {
						e.preventDefault();
						hidePostCodeSearch("bottom");
					});
					$("#bottomPostCodeButton").click(function(e) {
						e.preventDefault();
						$("#bottomPostCodeSearch").show();
					});
					setupPostCodeSearch("");
					setupPostCodeSearch("bottom");
					$("#layout .startStyle input,#layout .startStyle select")
							.focus(
									function() {
										$("#layout .startStyle").removeClass(
												"startStyle");
									});
					if (typeof (setTestSwitch) != "undefined"
							&& (setTestSwitch)) {
						setAnyCopy("industry", "All Classifications");
						setAnyCopy("occupation", "All Sub-Classifications");
					} else {
						setAnyCopy("industry", "Any Classification");
						setAnyCopy("occupation", "Any Sub-Classification");
					}
					setAnyCopy("worktype", "All Work Types");
					setAnyCopy("function", "Any Function");
					controlPrefix = "cat";
					populateClassiferList("catparentlocation", "parentlocation");
					populateClassiferList("catworktype", "worktype");
					populateClassiferList("catindustry", "industry");
					populateClassiferList("catoccupation", "occupation");
					ProcessChildList("parentlocation", "childlocation");
					ProcessChildList("industry", "occupation");
					SetInitialSalarySelections();
					SetInitialDateRangeSelection();
					controlPrefix = "bottom";
					populateClassiferList("bottomparentlocation",
							"parentlocation");
					populateClassiferList("bottomworktype", "worktype");
					populateClassiferList("bottomindustry", "industry");
					populateClassiferList("bottomoccupation", "occupation");
					ProcessChildList("parentlocation", "childlocation");
					ProcessChildList("industry", "occupation");
					SynchDuplicate("cat", "bottom", isCurrentSiteExecutive);
					controlPrefix = "cat";
					if (isCampus) {
						initializeCampus();
					}
					if (isHomePage) {
						DisplayLastQuickSearch();
						UnselectAnyInvalidLocationTexts();
						updateValue("#searchfrom", isHomePage ? "quick"
								: "quickupper");
						disableSubFields();
					} else {
						if ($("#FewerOptionsTop").length) {
							updateSearchFrom($("#FewerOptionsTop")[0]);
						}
						if ($("#bottomFewerOptionsTop").length) {
							updateSearchFrom($("#bottomFewerOptionsTop")[0]);
						}
						updateValue($("#searchtype"), "again");
						updateValue($("#bottomsearchtype"), "again");
					}
					if (typeof (setTestSwitch) != "undefined"
							&& (setTestSwitch)) {
						$("#catparentlocation").change(function() {
							updateLocationIndentation();
						});
						$("#FewerOptionsTop, #FewerOptionsBottom").click(
								function() {
									updateLocationIndentation();
								});
						updateLocationIndentation();
					}
					populateKeywords();
				});
function updateLocationIndentation() {
	if ($("#searchBoxContainer").hasClass("quickSearch")) {
		var selectedOption = $("#catparentlocation option:selected");
		if ($(document)[0].currentLocation) {
			if (selectedOption.text() !== $(document)[0].currentLocation.text()
					&& $(document)[0].currentLocation.val() !== "0") {
				var oldOptionText = $(document)[0].currentLocation.text();
				oldOptionText = indentation + "" + oldOptionText;
				$(document)[0].currentLocation.text(oldOptionText);
			}
		}
		var selectedOptionText = selectedOption.text();
		selectedOption.text(selectedOptionText.replace(indentation, ""));
		$(document)[0].currentLocation = $("#catparentlocation option:selected");
	}
}
function updateLocation(locId, areaId) {
	CustomWhichAreaSetValue("childlocation", "" + locId + ", " + areaId + "");
	if (areaId != "null") {
		$("#" + controlPrefix + "childlocation option[value='" + areaId + "']")
				.attr("selected", "selected");
	}
}
function updateValue(ctl, val) {
	if ($(ctl).length) {
		$(ctl).val(val);
	}
}
function populateLocationText(ctl) {
	var children = $(ctl).find("option");
	if (locationsTextCollection.length == 0) {
		populateLocationsTextCollection(children);
	}
	var selected = $(ctl).find("option:selected");
	$('input[name="locationText"]').val("");
	for (i = 0; i < selected.length; i++) {
		var value = $(selected[i]).val();
		if ($.inArray(value, locationsTextCollection) > -1) {
			$('input[name="locationText"]').val($(selected[i]).text());
			break;
		}
	}
}
function populateLocationsTextCollection(items) {
	var out = [];
	for (i = 0; i < items.length; i++) {
		var value = $(items[i]).val();
		var found = $.inArray(value, out);
		if (found > -1) {
			locationsTextCollection.push(value);
		} else {
			out.push(value);
		}
	}
}
function UnselectAnyInvalidLocationTexts() {
	if ($('input[name="locationText"]').val()) {
		var ctl = $('select[name="parentlocation"]');
		var selected = $(ctl).find("option");
		if (locationsTextCollection.length == 0) {
			populateLocationsTextCollection(ctl.find("option"));
		}
		for (i = 0; i < selected.length; i++) {
			var item = $(selected[i]);
			if ($.inArray(item.val(), locationsTextCollection) > -1) {
				if (item.text() != $('input[name="locationText"]').val()) {
					item[0].selected = false;
				} else {
					item[0].selected = true;
				}
			}
		}
	}
}
function KeywordsFocus(ctl) {
	$(ctl).removeClass("enter-text");
	if ($(ctl).val() == defaultKeywords) {
		$(ctl).val("");
	}
}
function KeywordsBlur(ctl) {
	if (!$(ctl).val() || $(ctl).val() == defaultKeywords) {
		$(ctl).addClass("enter-text");
		$(ctl).val(defaultKeywords);
	} else {
		$(ctl).removeClass("enter-text");
	}
}
function setSalaryType(ctl, typeControl) {
	var type = $(ctl).val();
	if (type == "ch:hourly") {
		updateValue($(typeControl), "hourly");
	} else {
		if (type == "ch:annual") {
			updateValue($(typeControl), "annual");
		}
	}
}
function setupPostCodeSearch(prefix) {
	$("#" + prefix + "PostCode").keypress(function(event) {
		if (event.keyCode == 13) {
			$("#" + prefix + "PostCodeSubmit").trigger("click");
			return false;
		}
	});
	$("#" + prefix + "PostCodeSubmit")
			.click(
					function() {
						var url = $("#" + prefix + "PostCodeUrl").val()
								+ "?searchString="
								+ $("#" + prefix + "PostCode").val();
						clearPostCodeResults(prefix);
						$
								.getJSON(
										url,
										function(data) {
											if (data) {
												var max = 10;
												var count = data.length;
												$("#" + prefix + "ResultCount")
														.html(
																count
																		+ " matches.");
												if (count == 0) {
													$(
															"#"
																	+ prefix
																	+ "ResultText")
															.html(
																	"No area matches your search.");
												} else {
													if (count >= max) {
														$(
																"#"
																		+ prefix
																		+ "ResultCount")
																.append(
																		" Please refine your search, only the first "
																				+ max
																				+ " are shown.");
													}
													$(
															"#"
																	+ prefix
																	+ "ResultText")
															.html(
																	"Choose an area from the list below.");
												}
												$
														.each(
																data,
																function(index,
																		result) {
																	var cats = aCategories
																			.get("childlocation");
																	var id = result.LocationId;
																	var locId = new String(
																			result.LocationId
																					.toString()
																					+ "|1");
																	var link = '<li><a href="#" onclick="updateLocation(\''
																			+ locId
																			+ "', '"
																			+ result.AreaId
																			+ "');hidePostCodeSearch('"
																			+ prefix
																			+ "')\">"
																			+ result.Area
																			+ ", "
																			+ result.Locality
																			+ ", "
																			+ result.PostCode
																			+ "</a></li>";
																	$(
																			"#"
																					+ prefix
																					+ "ResultDetails")
																			.append(
																					link);
																});
												if (count > 0) {
													$(
															"#"
																	+ prefix
																	+ "PostCodeResults")
															.show();
													$(
															"#"
																	+ prefix
																	+ "PostCodeNoResults")
															.hide();
												} else {
													$(
															"#"
																	+ prefix
																	+ "PostCodeResults")
															.hide();
													$(
															"#"
																	+ prefix
																	+ "PostCodeNoResults")
															.show();
												}
											}
										});
					});
}
function clearPostCodeResults(prefix) {
	$("#" + prefix + "ResultText").html("");
	$("#" + prefix + "ResultCount").html("");
	$("#" + prefix + "ResultDetails").html("");
}
function hidePostCodeSearch(prefix) {
	if (prefix == "bottom") {
		$("#bottomPostCodeSearch").hide();
		$("#bottomPostCodeResults").hide();
	} else {
		$("#PostCodeSearch").hide();
		$("#PostCodeResults").hide();
	}
}
function syncSalary() {
	var salarytype = aCatSelected.catsalarytype == "salaryhourly" ? "hourly"
			: "annual";
	if ($("#bottomsalaryfrom")[0]) {
		$("#bottomsalaryfrom").val($("#salaryfrom").val());
		controlPrefix = "bottom";
		updateSalary($("#bottomsalaryfrom")[0]);
		controlPrefix = "cat";
		$("#bottomsalaryto").val($("#salaryto").val());
	}
}
function changeToQuickSearch() {
	if (isHomePage) {
		changeSearchOptions("quick", isCurrentSiteExecutive);
	} else {
		changeSearchOptions("mini", isCurrentSiteExecutive, true);
	}
	$(".jobsearch-index").show();
	$(".joblisting-index").show();
	$(".footer-promo-panel").show();
	$("#bottomsearchBoxContainer").show();
}
function changeToAdvancedSearch() {
	changeSearchOptions("advanced", isCurrentSiteExecutive, true);
	$(".jobsearch-index").hide();
	$(".joblisting-index").hide();
	$(".footer-promo-panel").hide();
}
function changeToAdvancedSearchOnSearchProfile() {
	changeSearchOptionsWithoutPrepopulation("advanced", isCurrentSiteExecutive,
			true);
	$(".jobsearch-index").hide();
	$(".joblisting-index").hide();
	$(".footer-promo-panel").hide();
}
function checkOnsites() {
	if (isOnsite == "true") {
		$("#searchBoxContainer").addClass("onsite");
	}
}
function updateSalaryType(salaryType) {
	if (controlPrefix === "cat") {
		updateValue($("#salaryType"), salaryType);
	} else {
		updateValue($("#bottomsalaryType"), salaryType);
	}
}
function populateKeywords() {
	if (aCatSelected.keywords) {
		$("#Keywords").val(aCatSelected.keywords);
		$("#Keywords").removeClass("enter-text");
	} else {
		if ($("#Keywords").val() != defaultKeywords) {
			$("#Keywords").removeClass("enter-text");
		}
	}
}
function disabledSearchSalary(isDisabled) {
	if (isDisabled && !$("#searchBoxContainer").hasClass("advancedSearch")) {
		$("#searchBoxContainer").addClass("salaryDisabled");
		$("#salaryDisabled").val(0);
	} else {
		$("#searchBoxContainer").removeClass("salaryDisabled");
	}
}
function bottomdisabledSearchSalary(isDisabled) {
	if (isDisabled
			&& !$("#bottomsearchBoxContainer").hasClass("advancedSearch")) {
		$("#bottomsearchBoxContainer").addClass("salaryDisabled");
		$("#bottomsalaryDisabled").val(0);
	} else {
		$("#bottomsearchBoxContainer").removeClass("salaryDisabled");
	}
}
function updateSearchFrom(ctl, isAdvanced, isBottom) {
	if (ctl) {
		var id = ctl.id;
		if (id == "AdvSearchLink") {
			updateValue("#searchfrom", isHomePage ? "advanced"
					: "advancedupper");
		} else {
			if (id == "bottomAdvSearchLink") {
				updateValue("#bottomsearchfrom", "advancedlower");
			} else {
				if (id == "FewerOptionsBottom" || id == "FewerOptionsTop") {
					updateValue("#searchfrom", isHomePage ? "quick"
							: "quickupper");
				} else {
					updateValue("#bottomsearchfrom", "quicklower");
				}
			}
		}
	} else {
		if (isAdvanced != null && isBottom != null) {
			var searchFromVal = isAdvanced ? "advanced" : "quick";
			var searchFromPos = isBottom ? "lower" : "upper";
			var searchFromField = isBottom ? "#bottomsearchfrom"
					: "#searchfrom";
			searchFromVal += isHomePage ? "" : searchFromPos;
			updateValue(searchFromField, searchFromVal);
		}
	}
}
function initializeCampus() {
	$("#catoccupation").attr("disabled", "disabled");
	$("#bottomoccupation").attr("disabled", "disabled");
	$("#catchildlocation").attr("disabled", "disabled");
	$("#bottomchildlocation").attr("disabled", "disabled");
}
function disableSubFields() {
	if (typeof (setTestSwitch) != "undefined" && (setTestSwitch)) {
		if ($("#catindustry").val() == "0") {
			$("#catoccupation").addClass("disabled");
		} else {
			$("#catoccupation").removeClass("disabled");
		}
		var l = $("#catparentlocation").val();
		if (l == "0" || l == "3000|12" || l == "1005|1" || l == "1007|1"
				|| l == "1011|1" || l == "1013|1") {
			$("#catchildlocation").addClass("disabled");
			$("#catchildlocation").attr("disabled", "disabled");
		} else {
			$("#catchildlocation").removeClass("disabled");
			$("#catchildlocation").attr("disabled", "");
		}
	}
}
function parseQueryStringForCookie(criteriasValueQS) {
	var parsedQS = {
		KeywordQS : "",
		LocationQS : "",
		AreaQS : "",
		ClassificationQS : "",
		SubClassificationQS : "",
		FunctionQS : "",
		WorkTypeQS : "",
		SpecialisationQS : "",
		NotSpecifiedQS : "",
		SalaryTypeQS : "Salary",
		SalaryFromQS : "",
		SalaryToQS : "",
		NationQS : "",
		StateQS : "",
		ParentLocationTypeQS : "",
		Version : ""
	}, arrayCategoryQS = criteriasValueQS.split("~"), tempArray;
	for (mi = 0; mi < arrayCategoryQS.length; mi++) {
		tempArray = arrayCategoryQS[mi].split("|");
		if (tempArray[0] == "V") {
			parsedQS.Version = tempArray[1];
		} else {
			if (tempArray[0] == "K") {
				parsedQS.KeywordQS = unescape(tempArray[1]).replace(/\+/g, " ");
			} else {
				if (tempArray[0] == "PL") {
					parsedQS.ParentLocationTypeQS = tempArray[1];
				} else {
					if (tempArray[0] == "L") {
						parsedQS.LocationQS = tempArray[1];
					} else {
						if (tempArray[0] == "A") {
							if (tempArray[1].indexOf(":") == -1) {
								parsedQS.AreaQS = tempArray[1];
							} else {
								parsedQS.AreaQS = tempArray[1].substring(0,
										tempArray[1].indexOf(":"));
							}
						} else {
							if (tempArray[0] == "I") {
								parsedQS.ClassificationQS = tempArray[1];
							} else {
								if (tempArray[0] == "O") {
									parsedQS.SubClassificationQS = tempArray[1];
								} else {
									if (tempArray[0] == "F") {
										parsedQS.FunctionQS = tempArray[1];
									} else {
										if (tempArray[0] == "W") {
											parsedQS.WorkTypeQS = tempArray[1];
										} else {
											if (tempArray[0] == "S") {
												parsedQS.SpecialisationQS = tempArray[1];
											} else {
												if (tempArray[0] == "CB") {
													parsedQS.NotSpecifiedQS = tempArray[1];
												} else {
													if (tempArray[0] == "SFT") {
														parsedQS.SalaryTypeQS = tempArray[1];
													} else {
														if (tempArray[0] == "SF") {
															parsedQS.SalaryFromQS = tempArray[1];
														} else {
															if (tempArray[0] == "ST") {
																parsedQS.SalaryToQS = tempArray[1];
															} else {
																if (tempArray[0] == "NA") {
																	parsedQS.NationQS = tempArray[1];
																} else {
																	if (tempArray[0] == "SA") {
																		parsedQS.StateQS = tempArray[1];
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	return parsedQS;
}
var criteriasValueQS = GetCookie("QuickSearchCriteria" + marketSegment), parsedQS;
if (criteriasValueQS == null || criteriasValueQS == ";") {
	criteriasValueQS = "";
}
parsedQS = parseQueryStringForCookie(criteriasValueQS);
if (parsedQS.Version != version) {
	criteriasValueQS = null;
}
function DisplayLastQuickSearch() {
	if (parsedQS.KeywordQS != "") {
		Find("Keywords").value = parsedQS.KeywordQS;
	}
	if (parsedQS.LocationQS != "" || parsedQS.StateQS != ""
			|| parsedQS.NationQS != "") {
		if (marketSegment != "uk") {
			var selectedParentIndex = 0;
			var parentLocDD = Find("catparentlocation");
			var toSelect = parsedQS.LocationQS.replace(/#/g, "|");
			parentLocDD.value = toSelect;
			selectedParentIndex = parentLocDD.selectedIndex;
			if (selectedParentIndex == -1
					|| parentLocDD.options[selectedParentIndex].value != toSelect) {
				toSelect = parsedQS.StateQS.replace(/#/g, "|");
				parentLocDD.value = toSelect;
			}
			selectedParentIndex = parentLocDD.selectedIndex;
			if (selectedParentIndex == -1
					|| parentLocDD.options[selectedParentIndex].value != toSelect) {
				toSelect = parsedQS.NationQS.replace(/#/g, "|");
				parentLocDD.value = toSelect;
			}
			selectedParentIndex = parentLocDD.selectedIndex;
			if (selectedParentIndex == -1
					|| parentLocDD.options[selectedParentIndex].value != toSelect) {
				if (defaultIndexes.parentlocation != undefined) {
					parentLocDD.value = defaultIndexes.parentlocation;
				}
			}
			ValidateCategoryList("parentlocation", 0, "childlocation", true);
		} else {
			ValidateCategoryList("parentlocation", 0, "", true);
		}
	} else {
		if (criteriasValueQS) {
			Find("catparentlocation").value = "0";
			ValidateCategoryList("parentlocation", 0, "childlocation", true);
		}
	}
	if (parsedQS.AreaQS != "" || parsedQS.StateQS != ""
			|| parsedQS.LocationQS != "") {
		var selectedChildIndex = 0;
		var childLocDD = Find("catchildlocation");
		var toSelect = parsedQS.AreaQS.replace(/#/g, "|");
		childLocDD.value = toSelect;
		selectedChildIndex = childLocDD.selectedIndex;
		if (selectedChildIndex == -1
				|| childLocDD.options[selectedChildIndex].value != toSelect) {
			toSelect = parsedQS.LocationQS.replace(/#/g, "|");
			childLocDD.value = toSelect;
		}
		selectedChildIndex = childLocDD.selectedIndex;
		if (selectedChildIndex == -1
				|| childLocDD.options[selectedChildIndex].value != toSelect) {
			toSelect = parsedQS.StateQS.replace(/#/g, "|");
			childLocDD.value = toSelect;
		}
		selectedChildIndex = childLocDD.selectedIndex;
		if (selectedChildIndex == -1
				|| childLocDD.options[selectedChildIndex].value != toSelect) {
			childLocDD.value = "0";
		}
		ValidateCategoryList("childlocation", 0, "", false);
	}
	if (parsedQS.ClassificationQS != "" && marketSegment != "it") {
		Find("catindustry").value = parsedQS.ClassificationQS;
		ValidateCategoryList("industry", 0, "occupation", true);
	}
	if (parsedQS.SubClassificationQS != "") {
		Find("catoccupation").value = parsedQS.SubClassificationQS;
		ValidateCategoryList("occupation", 0, "specialisation", true);
	}
	if (parsedQS.NotSpecifiedQS != "") {
		if (Find("JobSearchBox_chkUnspecifiedspecialisation")) {
			Find("JobSearchBox_chkUnspecifiedspecialisation").checked = (parsedQS.NotSpecifiedQS == "true") ? true
					: false;
		}
		if (Find("ContentInclude_JobSearchBox_chkUnspecifiedspecialisation")) {
			Find("ContentInclude_JobSearchBox_chkUnspecifiedspecialisation").checked = (parsedQS.NotSpecifiedQS == "true") ? true
					: false;
		}
	}
	showClearQuickSearch();
	SetInitialSalarySelections();
	if (Find("salaryfrom") != null && Find("salaryfrom").disabled == false) {
		var targetType = "annual";
		if (parsedQS.SalaryTypeQS == "SalaryHourly") {
			targetType = "hourly";
		}
		switchSalaryTypeAndResync(targetType);
		if (parsedQS.SalaryFromQS != "") {
			Find("salaryfrom").value = parsedQS.SalaryFromQS;
			updateSalary(Find("salaryfrom"));
		}
		if (parsedQS.SalaryToQS != "") {
			Find("salaryto").value = parsedQS.SalaryToQS;
		}
	}
}
function showClearQuickSearch() {
	var showClear = "false";
	var CriteriaArray = new Array("catparentlocation", "catchildlocation",
			"catindustry", "catoccupation", "catworktype", "catspecialisation",
			"salaryfrom", "salaryto");
	if (Find("Keywords")) {
		if (document.domain.indexOf("xtramsn") != -1 && marketSegment != "uk") {
			if (Find("Keywords").value != "Keywords") {
				showClear = "true";
			}
		} else {
			if (Find("Keywords").value != "") {
				showClear = "true";
			}
		}
	}
	if (showClear == "false") {
		for (var i = 0; i < CriteriaArray.length; i++) {
			if (CriteriaArray[i] == "catparentlocation") {
				if (marketSegment != "uk"
						&& document.domain.indexOf("co.nz") != -1) {
					if (Find(CriteriaArray[i])
							&& Find(CriteriaArray[i]).value != 1017) {
						showClear = "true";
						break;
					}
				} else {
					if (marketSegment == "uk") {
						if (Find(CriteriaArray[i])
								&& Find(CriteriaArray[i]).value != 1027) {
							showClear = "true";
							break;
						}
					} else {
						if (Find(CriteriaArray[i])
								&& Find(CriteriaArray[i]).value != 0) {
							showClear = "true";
							break;
						}
					}
				}
			} else {
				if (Find(CriteriaArray[i]) && Find(CriteriaArray[i]).value != 0) {
					showClear = "true";
					break;
				}
			}
		}
	}
}
function ClearQuickCookie() {
	SEEK.Cookie.clearCookie("QuickSearchCriteria" + marketSegment);
	document.getElementById("clearsearch").style.visibility = "hidden";
	if (Find("Keywords")) {
		Find("Keywords").value = "";
	}
	if (Find("catparentlocation")) {
		if (document.domain.indexOf("co.nz") != -1 && marketSegment != "uk") {
			Find("catparentlocation").selectedIndex = 1;
		} else {
			Find("catparentlocation").selectedIndex = 0;
		}
		if (marketSegment == "exec") {
			ValidateCategoryList("parentlocation", 0, "", false);
		} else {
			ValidateCategoryList("parentlocation", 0, "childlocation", true);
		}
	}
	if (Find("catindustry")) {
		Find("catindustry").selectedIndex = 0;
		ValidateCategoryList("industry", 0, "occupation", true);
	}
	if (Find("catoccupation")) {
		if (marketSegment != "main") {
			Find("catoccupation").selectedIndex = 0;
		}
		if (marketSegment == "it") {
			ValidateCategoryList("occupation", 0, "specialisation", true);
		}
	}
	if (Find("catworktype")) {
		Find("catworktype").selectedIndex = 0;
	}
	if (Find("salaryfrom")) {
		Find("salaryfrom").selectedIndex = 0;
	}
	if (Find("salaryto")) {
		Find("salaryto").selectedIndex = 0;
	}
}
window.jobsearchModelLoaded = false;
var parentChildRelationships = {
	nation : "state",
	state : "location",
	location : "area",
	industry : "occupation",
	occupation : "specialisation"
};
var aCategories = {
	AUSalaryfrom : [ [ "$0", 0 ], [ "$30k", 30000 ], [ "$40k", 40000 ],
			[ "$50k", 50000 ], [ "$60k", 60000 ], [ "$70k", 70000 ],
			[ "$80k", 80000 ], [ "$100k", 100000 ], [ "$120k", 120000 ],
			[ "$150k", 150000 ], [ "$200k", 200000 ] ],
	AUSalaryto : [ [ "$30k", 30000 ], [ "$40k", 40000 ], [ "$50k", 50000 ],
			[ "$60k", 60000 ], [ "$70k", 70000 ], [ "$80k", 80000 ],
			[ "$100k", 100000 ], [ "$120k", 120000 ], [ "$150k", 150000 ],
			[ "$200k", 200000 ], [ "$200k+", 999999 ] ],
	NZSalaryfrom : [ [ "$0", 0 ], [ "$30k", 30000 ], [ "$40k", 40000 ],
			[ "$50k", 50000 ], [ "$60k", 60000 ], [ "$70k", 70000 ],
			[ "$80k", 80000 ], [ "$100k", 100000 ], [ "$120k", 120000 ],
			[ "$150k", 150000 ], [ "$200k", 200000 ] ],
	NZSalaryto : [ [ "$30k", 30000 ], [ "$40k", 40000 ], [ "$50k", 50000 ],
			[ "$60k", 60000 ], [ "$70k", 70000 ], [ "$80k", 80000 ],
			[ "$100k", 100000 ], [ "$120k", 120000 ], [ "$150k", 150000 ],
			[ "$200k", 200000 ], [ "$200k+", 999999 ] ],
	UKSalaryfrom : [ [ "�0", 0 ], [ "�20k", 20000 ], [ "�30k", 30000 ],
			[ "�40k", 40000 ], [ "�50k", 50000 ], [ "�70k", 70000 ],
			[ "�90k", 90000 ], [ "�110k", 110000 ], [ "�130k", 130000 ] ],
	UKSalaryto : [ [ "�20k", 20000 ], [ "�30k", 30000 ], [ "�40k", 40000 ],
			[ "�50k", 50000 ], [ "�70k", 70000 ], [ "�90k", 90000 ],
			[ "�110k", 110000 ], [ "�130k", 130000 ], [ "�130k+", 999999 ] ],
	AUSalaryExecfrom : [ [ "$80k", 80000 ], [ "$100k", 100000 ],
			[ "$120k", 120000 ], [ "$150k", 150000 ], [ "$200k", 200000 ] ],
	AUSalaryExecto : [ [ "$100k", 100000 ], [ "$120k", 120000 ],
			[ "$150k", 150000 ], [ "$200k", 200000 ], [ "$200k+", 999999 ] ],
	NZSalaryExecfrom : [ [ "$80k", 80000 ], [ "$100k", 100000 ],
			[ "$120k", 120000 ], [ "$150k", 150000 ], [ "$200k", 200000 ] ],
	NZSalaryExecto : [ [ "$100k", 100000 ], [ "$120k", 120000 ],
			[ "$150k", 150000 ], [ "$200k", 200000 ], [ "$200k+", 999999 ] ],
	AUSalaryHourlyfrom : [ [ "$0", 0 ], [ "$15", 15 ], [ "$20", 20 ],
			[ "$25", 25 ], [ "$30", 30 ], [ "$35", 35 ], [ "$40", 40 ],
			[ "$50", 50 ], [ "$60", 60 ], [ "$75", 75 ], [ "$100", 100 ] ],
	AUSalaryHourlyto : [ [ "$15", 15 ], [ "$20", 20 ], [ "$25", 25 ],
			[ "$30", 30 ], [ "$35", 35 ], [ "$40", 40 ], [ "$50", 50 ],
			[ "$60", 60 ], [ "$75", 75 ], [ "$100", 100 ], [ "$100+", 999999 ] ],
	NZSalaryHourlyfrom : [ [ "$0", 0 ], [ "$15", 15 ], [ "$20", 20 ],
			[ "$25", 25 ], [ "$30", 30 ], [ "$35", 35 ], [ "$40", 40 ],
			[ "$50", 50 ], [ "$60", 60 ], [ "$75", 75 ], [ "$100", 100 ] ],
	NZSalaryHourlyto : [ [ "$15", 15 ], [ "$20", 20 ], [ "$25", 25 ],
			[ "$30", 30 ], [ "$35", 35 ], [ "$40", 40 ], [ "$50", 50 ],
			[ "$60", 60 ], [ "$75", 75 ], [ "$100", 100 ], [ "$100+", 999999 ] ],
	UKSalaryHourlyfrom : [ [ "�0", 0 ], [ "�10", 10 ], [ "�15", 15 ],
			[ "�20", 20 ], [ "�25", 25 ], [ "�35", 35 ], [ "�45", 45 ],
			[ "�55", 55 ], [ "�65", 65 ] ],
	UKSalaryHourlyto : [ [ "�10", 10 ], [ "�15", 15 ], [ "�20", 20 ],
			[ "�25", 25 ], [ "�35", 35 ], [ "�45", 45 ], [ "�55", 55 ],
			[ "�65", 65 ], [ "�65+", 999999 ] ],
	AUSalaryHourlyExecfrom : [ [ "$40", 40 ], [ "$50", 50 ], [ "$60", 60 ],
			[ "$75", 75 ], [ "$100", 100 ] ],
	AUSalaryHourlyExecto : [ [ "$50", 50 ], [ "$60", 60 ], [ "$75", 75 ],
			[ "$100", 100 ], [ "$100+", 999999 ] ],
	NZSalaryHourlyExecfrom : [ [ "$40", 40 ], [ "$50", 50 ], [ "$60", 60 ],
			[ "$75", 75 ], [ "$100", 100 ] ],
	NZSalaryHourlyExecto : [ [ "$50", 50 ], [ "$60", 60 ], [ "$75", 75 ],
			[ "$100", 100 ], [ "$100+", 999999 ] ],
	parentlocation : [ [ "All Locations", "0" ],
			[ "All Australia", "3000|12" ], [ "Major Cities", "optgroup" ],
			[ "Sydney", "1000|1" ], [ "Melbourne", "1002|1" ],
			[ "Brisbane", "1004|1" ], [ "Gold Coast", "1005|1" ],
			[ "Perth", "1009|1" ], [ "Adelaide", "1007|1" ],
			[ "Hobart", "1011|1" ], [ "Darwin", "1013|1" ],
			[ "Canberra", "3100|13" ], [ "Regional", "optgroup" ],
			[ "NSW", "3101|13" ], [ "VIC", "3106|13" ], [ "QLD", "3103|13" ],
			[ "WA", "3107|13" ], [ "SA", "3104|13" ], [ "TAS", "3105|13" ],
			[ "ACT", "3100|13" ], [ "NT", "3102|13" ],
			[ "New Zealand", "optgroup" ], [ "All New Zealand", "3001|12" ],
			[ "Auckland", "1018|1" ], [ "Northland", "5101|1" ],
			[ "Waikato", "5126|1" ], [ "Bay of Plenty", "5089|1" ],
			[ "Gisborne", "5104|1" ], [ "Hawkes Bay", "5085|1" ],
			[ "Taranaki", "5082|1" ], [ "Manawatu", "5097|1" ],
			[ "Wellington", "1019|1" ], [ "Tasman", "5131|1" ],
			[ "Marlborough", "5079|1" ], [ "Canterbury", "5113|1" ],
			[ "Otago", "5093|1" ], [ "West Coast", "5123|1" ],
			[ "Southland", "5110|1" ], [ "UK & Ireland", "optgroup" ],
			[ "All UK & Ireland", "3002|12" ], [ "London", "5161|1" ],
			[ "Rest of UK", "5150|1" ], [ "Ireland", "1025|1" ],
			[ "Other", "optgroup" ], [ "All Other", "3003|12" ],
			[ "Asia Pacific", "5140|1" ], [ "Americas", "5137|1" ],
			[ "Europe & Russia", "5134|1" ],
			[ "Middle East & Africa", "5147|1" ] ],
	childlocation : {
		"0" : [ "Any", 0 ],
		"3101|13" : [ "New South Wales", "3101|13" ],
		"3106|13" : [ "Victoria", "3106|13" ],
		"3103|13" : [ "Queensland", "3103|13" ],
		"3107|13" : [ "Western Australia", "3107|13" ],
		"3104|13" : [ "South Australia", "3104|13" ],
		"3105|13" : [ "Tasmania", "3105|13" ],
		"3100|13" : [ "Australian Capital Territory", "3100|13" ],
		"3102|13" : [ "Northern Territories", "3102|13" ],
		"3108|13" : [ "New Zealand", "3108|13" ],
		"3109|13" : [ "UK & Ireland", "3109|13" ],
		"3110|13" : [ "Overseas", "3110|13" ],
		"5019|1" : [ "Adelaide Hills & Barossa", "5019|1" ],
		"5057|1" : [ "Albany & Great Southern", "5057|1" ],
		"5026|1" : [ "Albury Wodonga & Murray", "5026|1" ],
		"5060|1" : [ "Alice Springs & Central Australia", "5060|1" ],
		"5137|1" : [ "Americas", "5137|1" ],
		"5140|1" : [ "Asia Pacific", "5140|1" ],
		"5075|1" : [ "Bairnsdale & Gippsland", "5075|1" ],
		"5076|1" : [ "Ballarat & Central Highlands", "5076|1" ],
		"5067|1" : [ "Bendigo, Goldfields & Macedon Ranges", "5067|1" ],
		"5025|1" : [ "Blue Mountains & Central West", "5025|1" ],
		"5055|1" : [ "Broome & Kimberley", "5055|1" ],
		"5047|1" : [ "Bunbury & South West", "5047|1" ],
		"5001|1" : [ "Bundaberg & Wide Bay Burnett", "5001|1" ],
		"5003|1" : [ "Cairns & Far North", "5003|1" ],
		"5063|1" : [ "Central & South East", "5063|1" ],
		"5043|1" : [ "Coffs Harbour & North Coast", "5043|1" ],
		"5023|1" : [ "Coober Pedy & Outback SA", "5023|1" ],
		"5062|1" : [ "Devonport & North West", "5062|1" ],
		"5044|1" : [ "Dubbo & Central NSW", "5044|1" ],
		"5134|1" : [ "Europe & Russia", "5134|1" ],
		"5039|1" : [ "Far West & North Central NSW", "5039|1" ],
		"5017|1" : [ "Fleurieu Peninsula & Kangaroo Island", "5017|1" ],
		"5068|1" : [ "Geelong & Great Ocean Road", "5068|1" ],
		"5056|1" : [ "Geraldton, Gascoyne & Midwest", "5056|1" ],
		"5006|1" : [ "Gladstone & Central QLD", "5006|1" ],
		"5024|1" : [ "Gosford & Central Coast", "5024|1" ],
		"5035|1" : [ "Goulburn & Southern Tablelands", "5035|1" ],
		"5004|1" : [ "Hervey Bay & Fraser Coast", "5004|1" ],
		"5064|1" : [ "Horsham & Grampians", "5064|1" ],
		"1025|1" : [ "Ireland", "1025|1" ],
		"5058|1" : [ "Kalgoorlie, Goldfields & Esperance", "5058|1" ],
		"5059|1" : [ "Katherine & Northern Australia", "5059|1" ],
		"5061|1" : [ "Launceston & North East", "5061|1" ],
		"5036|1" : [ "Lismore & Far North Coast", "5036|1" ],
		"5161|1" : [ "London", "5161|1" ],
		"5015|1" : [ "Mackay & Coalfields", "5015|1" ],
		"5053|1" : [ "Mandurah & Peel", "5053|1" ],
		"5147|1" : [ "Middle East & Africa", "5147|1" ],
		"5074|1" : [ "Mildura & Murray", "5074|1" ],
		"5077|1" : [ "Mornington Peninsula & Bass Coast", "5077|1" ],
		"5022|1" : [ "Mt Gambier & Limestone Coast", "5022|1" ],
		"5008|1" : [ "Mt Isa & Western QLD", "5008|1" ],
		"5045|1" : [ "Newcastle, Maitland & Hunter", "5045|1" ],
		"5046|1" : [ "Northam & Wheatbelt", "5046|1" ],
		"5054|1" : [ "Port Hedland, Karratha & Pilbara", "5054|1" ],
		"5040|1" : [ "Port Macquarie & Mid North Coast", "5040|1" ],
		"5150|1" : [ "Rest of the UK", "5150|1" ],
		"5041|1" : [ "Richmond & Hawkesbury", "5041|1" ],
		"5018|1" : [ "Riverland & Murray Mallee", "5018|1" ],
		"5007|1" : [ "Rockhampton & Capricorn Coast", "5007|1" ],
		"5078|1" : [ "Shepparton & Goulburn Valley", "5078|1" ],
		"5009|1" : [ "Somerset & Lockyer", "5009|1" ],
		"5002|1" : [ "Sunshine Coast", "5002|1" ],
		"5042|1" : [ "Tamworth & North West NSW", "5042|1" ],
		"5016|1" : [ "Toowoomba & Darling Downs", "5016|1" ],
		"5005|1" : [ "Townsville & Northern QLD", "5005|1" ],
		"5066|1" : [ "Traralgon & La Trobe Valley", "5066|1" ],
		"5034|1" : [ "Tumut, Southern Highlands & Snowy", "5034|1" ],
		"5037|1" : [ "Wagga Wagga & Riverina", "5037|1" ],
		"5021|1" : [ "Whyalla & Eyre Peninsula", "5021|1" ],
		"5038|1" : [ "Wollongong, Illawarra & South Coast", "5038|1" ],
		"5065|1" : [ "Yarra Valley & High Country", "5065|1" ],
		"5020|1" : [ "Yorke Peninsula & Clare Valley", "5020|1" ],
		"1015|1" : [ "All ACT", "1015|1" ],
		"1018|1" : [ "Auckland", "1018|1" ],
		"5101|1" : [ "Northland", "5101|1" ],
		"5126|1" : [ "Waikato", "5126|1" ],
		"5089|1" : [ "Bay of Plenty", "5089|1" ],
		"5104|1" : [ "Gisborne", "5104|1" ],
		"5085|1" : [ "Hawkes Bay", "5085|1" ],
		"5082|1" : [ "Taranaki", "5082|1" ],
		"5097|1" : [ "Manawatu", "5097|1" ],
		"1019|1" : [ "Wellington", "1019|1" ],
		"5131|1" : [ "Tasman", "5131|1" ],
		"5079|1" : [ "Marlborough", "5079|1" ],
		"5113|1" : [ "Canterbury", "5113|1" ],
		"5093|1" : [ "Otago", "5093|1" ],
		"5123|1" : [ "West Coast", "5123|1" ],
		"5110|1" : [ "Southland", "5110|1" ],
		"1000|1" : [ "Sydney", "1000|1" ],
		"1002|1" : [ "Melbourne", "1002|1" ],
		"1004|1" : [ "Brisbane", "1004|1" ],
		"1005|1" : [ "Gold Coast", "1005|1" ],
		"1009|1" : [ "Perth", "1009|1" ],
		"1007|1" : [ "Adelaide", "1007|1" ],
		"1011|1" : [ "Hobart", "1011|1" ],
		"1013|1" : [ "Darwin", "1013|1" ],
		"5027" : [ "CBD, Inner West & Eastern Suburbs", "5027" ],
		"5028" : [ "North Shore & Northern Beaches", "5028" ],
		"5029" : [ "North West & Hills District", "5029" ],
		"5030" : [ "Parramatta & Western Suburbs", "5030" ],
		"5031" : [ "Ryde & Macquarie Park", "5031" ],
		"5032" : [ "Southern Suburbs & Sutherland Shire", "5032" ],
		"5033" : [ "South West & M5 Corridor", "5033" ],
		"5069" : [ "CBD & Inner Suburbs", "5069" ],
		"5070" : [ "Bayside & South Eastern Suburbs", "5070" ],
		"5071" : [ "Eastern Suburbs", "5071" ],
		"5072" : [ "Northern Suburbs", "5072" ],
		"5073" : [ "Western Suburbs", "5073" ],
		"5010" : [ "CBD & Inner Suburbs", "5010" ],
		"5011" : [ "Bayside & Eastern Suburbs", "5011" ],
		"5012" : [ "Northern Suburbs", "5012" ],
		"5013" : [ "Southern Suburbs & Logan", "5013" ],
		"5014" : [ "Western Suburbs & Ipswich", "5014" ],
		"5048" : [ "CBD, Inner & Western Suburbs", "5048" ],
		"5049" : [ "Eastern Suburbs", "5049" ],
		"5050" : [ "Fremantle & Southern Suburbs", "5050" ],
		"5051" : [ "Northern Suburbs & Joondalup", "5051" ],
		"5052" : [ "Rockingham & Kwinana", "5052" ],
		"5118" : [ "Auckland Central", "5118" ],
		"5119" : [ "Manukau & East Auckland", "5119" ],
		"5120" : [ "Papakura & Franklin", "5120" ],
		"5121" : [ "Rodney & North Shore", "5121" ],
		"5122" : [ "Waitakere & West Auckland", "5122" ],
		"2831" : [ "Wellington Central", "2831" ],
		"5107" : [ "Hutt Valley", "5107" ],
		"5108" : [ "Porirua & Kapiti Coast", "5108" ],
		"5109" : [ "Rest of Wellington Region", "5109" ],
		"5080" : [ "Blenheim", "5080" ],
		"5081" : [ "Rest of Marlborough", "5081" ],
		"5083" : [ "New Plymouth", "5083" ],
		"5084" : [ "Rest of Taranaki", "5084" ],
		"5086" : [ "Hastings", "5086" ],
		"5087" : [ "Napier", "5087" ],
		"5088" : [ "Rest of Hawkes Bay", "5088" ],
		"5090" : [ "Rotorua", "5090" ],
		"5091" : [ "Tauranga", "5091" ],
		"5092" : [ "Rest of Bay of Plenty", "5092" ],
		"5094" : [ "Dunedin", "5094" ],
		"5095" : [ "Queenstown & Wanaka", "5095" ],
		"5096" : [ "Rest of Otago", "5096" ],
		"5098" : [ "Palmerston North", "5098" ],
		"5099" : [ "Wanganui", "5099" ],
		"5100" : [ "Rest of Manawatu", "5100" ],
		"5102" : [ "Whangarei", "5102" ],
		"5103" : [ "Rest of Northland", "5103" ],
		"5105" : [ "Gisborne", "5105" ],
		"5106" : [ "Rest of Gisborne", "5106" ],
		"5111" : [ "Invercargill", "5111" ],
		"5112" : [ "Rest of Southland", "5112" ],
		"5114" : [ "Christchurch", "5114" ],
		"5115" : [ "North Canterbury", "5115" ],
		"5116" : [ "Timaru & South Canterbury", "5116" ],
		"5117" : [ "Rest of Canterbury", "5117" ],
		"5124" : [ "Greymouth", "5124" ],
		"5125" : [ "Rest of West Coast", "5125" ],
		"5127" : [ "Hamilton", "5127" ],
		"5128" : [ "Taupo", "5128" ],
		"5129" : [ "Thames & Coromandel", "5129" ],
		"5130" : [ "Rest of Waikato", "5130" ],
		"5132" : [ "Nelson", "5132" ],
		"5133" : [ "Rest of Tasman", "5133" ],
		"5135" : [ "Russia & Eastern Europe", "5135" ],
		"5136" : [ "Western Europe", "5136" ],
		"5138" : [ "Caribbean, Central & South America", "5138" ],
		"5139" : [ "USA & Canada", "5139" ],
		"5141" : [ "Central Asia", "5141" ],
		"5142" : [ "China, Hong Kong & Taiwan", "5142" ],
		"5143" : [ "India & South Asia", "5143" ],
		"5144" : [ "Japan & Korea", "5144" ],
		"5145" : [ "PNG & Pacific Islands", "5145" ],
		"5146" : [ "South-East Asia", "5146" ],
		"5148" : [ "Middle East & North Africa", "5148" ],
		"5149" : [ "Sub-Saharan Africa", "5149" ],
		"5151" : [ "Channel Islands", "5151" ],
		"5152" : [ "Midlands", "5152" ],
		"5153" : [ "Northern Ireland", "5153" ],
		"5154" : [ "Northern England", "5154" ],
		"5155" : [ "North West England", "5155" ],
		"5156" : [ "Scotland", "5156" ],
		"5157" : [ "South East & Home Counties", "5157" ],
		"5158" : [ "South West England", "5158" ],
		"5159" : [ "Thames Valley & Oxford", "5159" ],
		"5160" : [ "Wales", "5160" ]
	},
	worktype : [ [ "Any", 0 ], [ "Full Time", 242 ], [ "Part Time", 243 ],
			[ "Contract/Temp", 244 ], [ "Casual/Vacation", 245 ] ],
	industry : [ [ "Any", 0 ], [ "Accounting", 1200 ],
			[ "Administration & Office Support", 6251 ],
			[ "Advertising, Arts & Media", 6304 ],
			[ "Banking & Financial Services", 1203 ],
			[ "Call Centre & Customer Service", 1204 ],
			[ "CEO & General Management", 7019 ],
			[ "Community Services & Development", 6163 ],
			[ "Construction", 1206 ], [ "Consulting & Strategy", 6076 ],
			[ "Design & Architecture", 6263 ],
			[ "Education & Training", 6123 ], [ "Engineering", 1209 ],
			[ "Farming, Animals & Conservation", 6205 ],
			[ "Government & Defence", 1210 ], [ "Healthcare & Medical", 1211 ],
			[ "Hospitality & Tourism", 1212 ],
			[ "Human Resources & Recruitment", 6317 ],
			[ "Information & Communication Technology", 6281 ],
			[ "Insurance & Superannuation", 1214 ], [ "Legal", 1216 ],
			[ "Manufacturing, Transport & Logistics", 6092 ],
			[ "Marketing & Communications", 6008 ],
			[ "Mining, Resources & Energy", 6058 ],
			[ "Real Estate & Property", 1220 ],
			[ "Retail & Consumer Products", 6043 ], [ "Sales", 6362 ],
			[ "Science & Technology", 1223 ], [ "Self Employment", 6261 ],
			[ "Sport & Recreation", 6246 ], [ "Trades & Services", 1225 ] ],
	occupation : {
		"0" : [ "Any", 0 ],
		"6174" : [ "Account & Relationship Management", 6174 ],
		"6363" : [ "Account & Relationship Management", 6363 ],
		"6140" : [ "Accounts Officers/Clerks", 6140 ],
		"1303" : [ "Accounts Payable", 1303 ],
		"6141" : [ "Accounts Receivable/Credit Control", 6141 ],
		"6275" : [ "Actuarial", 6275 ],
		"6371" : [ "Administration", 6371 ],
		"6252" : [ "Administrative Assistants", 6252 ],
		"6022" : [ "Aerospace Engineering", 6022 ],
		"6164" : [ "Aged & Disability Support", 6164 ],
		"6305" : [ "Agency Account Management", 6305 ],
		"6206" : [ "Agronomy & Farm Services", 6206 ],
		"1313" : [ "Air Conditioning & Refrigeration", 1313 ],
		"1314" : [ "Air Force", 1314 ],
		"1315" : [ "Airlines", 1315 ],
		"6329" : [ "Ambulance/Paramedics", 6329 ],
		"6059" : [ "Analysis & Reporting", 6059 ],
		"6095" : [ "Analysis & Reporting", 6095 ],
		"6143" : [ "Analysis & Reporting", 6143 ],
		"6175" : [ "Analysis & Reporting", 6175 ],
		"6364" : [ "Analysis & Reporting", 6364 ],
		"6001" : [ "Analysts", 6001 ],
		"6077" : [ "Analysts", 6077 ],
		"6282" : [ "Architects", 6282 ],
		"6265" : [ "Architectural Drafting", 6265 ],
		"6264" : [ "Architecture", 6264 ],
		"1322" : [ "Army", 1322 ],
		"6306" : [ "Art Direction", 6306 ],
		"6093" : [ "Assembly & Process Work", 6093 ],
		"6276" : [ "Assessment", 6276 ],
		"6142" : [ "Assistant Accountants", 6142 ],
		"6144" : [ "Audit - External", 6144 ],
		"6145" : [ "Audit - Internal", 6145 ],
		"6023" : [ "Automotive Engineering", 6023 ],
		"1328" : [ "Automotive Trades", 1328 ],
		"6094" : [ "Aviation Services", 6094 ],
		"6226" : [ "Bakers & Pastry Chefs", 6226 ],
		"6177" : [ "Banking - Business", 6177 ],
		"6178" : [ "Banking - Corporate & Institutional", 6178 ],
		"6176" : [ "Banking - Retail/Branch", 6176 ],
		"6188" : [ "Banking & Finance Law", 6188 ],
		"1332" : [ "Bar & Beverage Staff", 1332 ],
		"6215" : [ "Biological & Biomedical Sciences", 6215 ],
		"6216" : [ "Biotechnology & Genetics", 6216 ],
		"7020" : [ "Board Appointments", 7020 ],
		"6002" : [ "Body Corporate & Facilities Management", 6002 ],
		"6146" : [ "Bookkeeping & Small Practice Accounting", 6146 ],
		"6009" : [ "Brand Management", 6009 ],
		"6277" : [ "Brokerage", 6277 ],
		"6024" : [ "Building Services Engineering", 6024 ],
		"6227" : [ "Building Trades", 6227 ],
		"6147" : [ "Business Services & Corporate Advisory", 6147 ],
		"6283" : [ "Business/Systems Analysts", 6283 ],
		"6228" : [ "Butchers", 6228 ],
		"6044" : [ "Buying", 6044 ],
		"1345" : [ "Carpentry & Cabinet Making", 1345 ],
		"7021" : [ "CEO", 7021 ],
		"6052" : [ "Chefs/Cooks", 6052 ],
		"6026" : [ "Chemical Engineering", 6026 ],
		"6217" : [ "Chemistry & Physics", 6217 ],
		"6165" : [ "Child Welfare, Youth & Family Services", 6165 ],
		"6124" : [ "Childcare & Outside School Hours Care", 6124 ],
		"6330" : [ "Chiropractic & Osteopathic", 6330 ],
		"6027" : [ "Civil/Structural Engineering", 6027 ],
		"1350" : [ "Claims", 1350 ],
		"6229" : [ "Cleaning Services", 6229 ],
		"6253" : [ "Client & Sales Administration", 6253 ],
		"1352" : [ "Client Services", 1352 ],
		"1353" : [ "Clinical/Medical Research", 1353 ],
		"6247" : [ "Coaching & Instruction", 6247 ],
		"6084" : [ "Collections", 6084 ],
		"6003" : [ "Commercial Sales, Leasing & Property Mgmt", 6003 ],
		"6166" : [ "Community Development", 6166 ],
		"6148" : [ "Company Secretaries", 6148 ],
		"6149" : [ "Compliance & Risk", 6149 ],
		"6179" : [ "Compliance & Risk", 6179 ],
		"6284" : [ "Computer Operators", 6284 ],
		"6207" : [ "Conservation, Parks & Wildlife", 6207 ],
		"6190" : [ "Construction Law", 6190 ],
		"6285" : [ "Consultants", 6285 ],
		"6318" : [ "Consulting & Generalist HR", 6318 ],
		"6254" : [ "Contracts Administration", 6254 ],
		"6113" : [ "Contracts Management", 6113 ],
		"7022" : [ "COO & MD", 7022 ],
		"6191" : [ "Corporate & Commercial Law", 6191 ],
		"6078" : [ "Corporate Development", 6078 ],
		"6180" : [ "Corporate Finance & Investment Banking", 6180 ],
		"6150" : [ "Cost Accounting", 6150 ],
		"6096" : [ "Couriers, Drivers & Postal Services", 6096 ],
		"6181" : [ "Credit", 6181 ],
		"6189" : [ "Criminal & Civil Law", 6189 ],
		"6085" : [ "Customer Service - Call Centre", 6085 ],
		"6086" : [ "Customer Service - Customer Facing", 6086 ],
		"6255" : [ "Data Entry & Word Processing", 6255 ],
		"6286" : [ "Database Development & Administration", 6286 ],
		"1372" : [ "Dental", 1372 ],
		"6287" : [ "Developers/Programmers", 6287 ],
		"6331" : [ "Dieticians", 6331 ],
		"6010" : [ "Digital & Search Marketing", 6010 ],
		"6011" : [ "Direct Marketing & CRM", 6011 ],
		"6307" : [ "Editing & Publishing", 6307 ],
		"6028" : [ "Electrical/Electronic Engineering", 6028 ],
		"6230" : [ "Electricians", 6230 ],
		"1378" : [ "Emergency Services", 1378 ],
		"6167" : [ "Employment Services", 6167 ],
		"6288" : [ "Engineering - Hardware", 6288 ],
		"6289" : [ "Engineering - Network", 6289 ],
		"6290" : [ "Engineering - Software", 6290 ],
		"6025" : [ "Engineering Drafting", 6025 ],
		"6192" : [ "Environment & Planning Law", 6192 ],
		"6079" : [ "Environment & Sustainability Consulting", 6079 ],
		"6029" : [ "Environmental Engineering", 6029 ],
		"1386" : [ "Environmental Services", 1386 ],
		"6218" : [ "Environmental, Earth & Geosciences", 6218 ],
		"1387" : [ "Estimating", 1387 ],
		"6012" : [ "Event Management", 6012 ],
		"6308" : [ "Event Management", 6308 ],
		"6193" : [ "Family Law", 6193 ],
		"6208" : [ "Farm Labour", 6208 ],
		"6209" : [ "Farm Management", 6209 ],
		"6267" : [ "Fashion & Textile Design", 6267 ],
		"6030" : [ "Field Engineering", 6030 ],
		"6151" : [ "Financial Accounting & Reporting", 6151 ],
		"6152" : [ "Financial Managers & Controllers", 6152 ],
		"1392" : [ "Financial Planning", 1392 ],
		"6210" : [ "Fishing & Aquaculture", 6210 ],
		"6248" : [ "Fitness & Personal Training", 6248 ],
		"6231" : [ "Fitters, Turners & Machinists", 6231 ],
		"6097" : [ "Fleet Management", 6097 ],
		"6232" : [ "Floristry", 6232 ],
		"6219" : [ "Food Technology & Safety", 6219 ],
		"6153" : [ "Forensic Accounting & Investigation", 6153 ],
		"6114" : [ "Foreperson/Supervisors", 6114 ],
		"6098" : [ "Freight/Cargo Forwarding", 6098 ],
		"6053" : [ "Front Office & Guest Services", 6053 ],
		"6278" : [ "Fund Administration", 6278 ],
		"6168" : [ "Fundraising", 6168 ],
		"1404" : [ "Funds Management", 1404 ],
		"1405" : [ "Gaming", 1405 ],
		"1406" : [ "Gardening & Landscaping", 1406 ],
		"6332" : [ "General Practitioners", 6332 ],
		"7023" : [ "General/Business Unit Manager", 7023 ],
		"6194" : [ "Generalists - In-house", 6194 ],
		"6195" : [ "Generalists - Law Firm", 6195 ],
		"6268" : [ "Graphic Design", 6268 ],
		"6233" : [ "Hair & Beauty Services", 6233 ],
		"6060" : [ "Health, Safety & Environment", 6060 ],
		"6115" : [ "Health, Safety & Environment", 6115 ],
		"6291" : [ "Help Desk & IT Support", 6291 ],
		"6211" : [ "Horticulture", 6211 ],
		"1415" : [ "Housekeeping", 1415 ],
		"6169" : [ "Housing & Homelessness Services", 6169 ],
		"6266" : [ "Illustration & Animation", 6266 ],
		"6099" : [ "Import/Export & Customs", 6099 ],
		"6170" : [ "Indigenous & Multicultural Services", 6170 ],
		"6319" : [ "Industrial & Employee Relations", 6319 ],
		"6269" : [ "Industrial Design", 6269 ],
		"6031" : [ "Industrial Engineering", 6031 ],
		"6196" : [ "Industrial Relations & Employment Law", 6196 ],
		"6154" : [ "Insolvency & Corporate Recovery", 6154 ],
		"6197" : [ "Insurance & Superannuation Law", 6197 ],
		"6198" : [ "Intellectual Property Law", 6198 ],
		"6270" : [ "Interior Design", 6270 ],
		"6013" : [ "Internal Communications", 6013 ],
		"6155" : [ "Inventory & Fixed Assets", 6155 ],
		"6309" : [ "Journalism & Writing", 6309 ],
		"6054" : [ "Kitchen & Sandwich Hands", 6054 ],
		"6220" : [ "Laboratory & Technical Services", 6220 ],
		"6234" : [ "Labourers", 6234 ],
		"6271" : [ "Landscape Architecture", 6271 ],
		"1429" : [ "Law Clerks & Paralegals", 1429 ],
		"6199" : [ "Legal Practice Management", 6199 ],
		"1431" : [ "Legal Secretaries", 1431 ],
		"6125" : [ "Library Services & Information Management", 6125 ],
		"6200" : [ "Litigation & Dispute Resolution", 6200 ],
		"6235" : [ "Locksmiths", 6235 ],
		"6101" : [ "Machine Operators", 6101 ],
		"6236" : [ "Maintenance & Handyperson Services", 6236 ],
		"6032" : [ "Maintenance", 6032 ],
		"6320" : [ "Management - Agency", 6320 ],
		"6045" : [ "Management - Area/Multi-site", 6045 ],
		"6046" : [ "Management - Department/Assistant", 6046 ],
		"6321" : [ "Management - Internal", 6321 ],
		"6126" : [ "Management - Schools", 6126 ],
		"6047" : [ "Management - Store", 6047 ],
		"6127" : [ "Management - Universities", 6127 ],
		"6128" : [ "Management - Vocational", 6128 ],
		"6080" : [ "Management & Change Consulting", 6080 ],
		"6087" : [ "Management & Support", 6087 ],
		"6157" : [ "Management Accounting & Budgeting", 6157 ],
		"6017" : [ "Management", 6017 ],
		"6033" : [ "Management", 6033 ],
		"6055" : [ "Management", 6055 ],
		"6061" : [ "Management", 6061 ],
		"6102" : [ "Management", 6102 ],
		"6116" : [ "Management", 6116 ],
		"6156" : [ "Management", 6156 ],
		"6171" : [ "Management", 6171 ],
		"6182" : [ "Management", 6182 ],
		"6249" : [ "Management", 6249 ],
		"6279" : [ "Management", 6279 ],
		"6292" : [ "Management", 6292 ],
		"6310" : [ "Management", 6310 ],
		"6333" : [ "Management", 6333 ],
		"6366" : [ "Management", 6366 ],
		"6014" : [ "Market Research & Analysis", 6014 ],
		"6015" : [ "Marketing Assistants/Coordinators", 6015 ],
		"6016" : [ "Marketing Communications", 6016 ],
		"6034" : [ "Materials Handling Engineering", 6034 ],
		"6221" : [ "Materials Sciences", 6221 ],
		"6222" : [ "Mathematics, Statistics & Information Sciences", 6222 ],
		"6035" : [ "Mechanical Engineering", 6035 ],
		"6311" : [ "Media Strategy, Planning & Buying", 6311 ],
		"6334" : [ "Medical Administration", 6334 ],
		"6335" : [ "Medical Imaging", 6335 ],
		"6336" : [ "Medical Specialists", 6336 ],
		"6048" : [ "Merchandisers", 6048 ],
		"6062" : [ "Mining - Drill & Blast", 6062 ],
		"6063" : [ "Mining - Engineering & Maintenance", 6063 ],
		"6064" : [ "Mining - Exploration & Geoscience", 6064 ],
		"6066" : [ "Mining - Operations", 6066 ],
		"6065" : [ "Mining - Processing", 6065 ],
		"6223" : [ "Modelling & Simulation", 6223 ],
		"6183" : [ "Mortgages", 6183 ],
		"6237" : [ "Nannies & Babysitters", 6237 ],
		"6073" : [ "Natural Resources & Water", 6073 ],
		"6337" : [ "Natural Therapies & Alternative Medicine", 6337 ],
		"1450" : [ "Navy", 1450 ],
		"6293" : [ "Networks & Systems Administration", 6293 ],
		"6365" : [ "New Business Development", 6365 ],
		"6338" : [ "Nursing - A&E, Critical Care & ICU", 6338 ],
		"6339" : [ "Nursing - Aged Care", 6339 ],
		"6340" : [ "Nursing - Community, Maternal & Child Health", 6340 ],
		"6341" : [ "Nursing - Educators & Facilitators", 6341 ],
		"6342" : [ "Nursing - General Medical & Surgical", 6342 ],
		"6343" : [ "Nursing - High Acuity", 6343 ],
		"6344" : [ "Nursing - Management", 6344 ],
		"6345" : [ "Nursing - Midwifery, Neo-Natal, SCN & NICU", 6345 ],
		"6346" : [ "Nursing - Paediatric & PICU", 6346 ],
		"6347" : [ "Nursing - Psych, Forensic & Correctional Health", 6347 ],
		"6348" : [ "Nursing - Theatre & Recovery", 6348 ],
		"6322" : [ "Occupational Health & Safety", 6322 ],
		"6256" : [ "Office Management", 6256 ],
		"6067" : [ "Oil & Gas - Drilling", 6067 ],
		"6068" : [ "Oil & Gas - Engineering & Maintenance", 6068 ],
		"6069" : [ "Oil & Gas - Exploration & Geoscience", 6069 ],
		"6071" : [ "Oil & Gas - Operations", 6071 ],
		"6070" : [ "Oil & Gas - Production & Refinement", 6070 ],
		"6349" : [ "Optical", 6349 ],
		"6323" : [ "Organisational Development", 6323 ],
		"7024" : [ "Other", 7024 ],
		"6257" : [ "PA, EA & Secretarial", 6257 ],
		"6238" : [ "Painters & Sign Writers", 6238 ],
		"1467" : [ "Pathology", 1467 ],
		"6104" : [ "Pattern Makers & Garment Technicians", 6104 ],
		"1468" : [ "Payroll", 1468 ],
		"6312" : [ "Performing Arts", 6312 ],
		"6201" : [ "Personal Injury Law", 6201 ],
		"6350" : [ "Pharmaceuticals & Medical Devices", 6350 ],
		"1470" : [ "Pharmacy", 1470 ],
		"6313" : [ "Photography", 6313 ],
		"6352" : [ "Physiotherapy, OT & Rehabilitation", 6352 ],
		"6105" : [ "Pickers & Packers", 6105 ],
		"6117" : [ "Planning & Scheduling", 6117 ],
		"6049" : [ "Planning", 6049 ],
		"6118" : [ "Plant & Machinery Operators", 6118 ],
		"6240" : [ "Plumbers", 6240 ],
		"6359" : [ "Police & Corrections", 6359 ],
		"6360" : [ "Policy, Planning & Regulation", 6360 ],
		"6081" : [ "Policy", 6081 ],
		"6072" : [ "Power Generation & Distribution", 6072 ],
		"6239" : [ "Printing & Publishing Services", 6239 ],
		"6036" : [ "Process Engineering", 6036 ],
		"6018" : [ "Product Management & Development", 6018 ],
		"6294" : [ "Product Management & Development", 6294 ],
		"6103" : [ "Production, Planning & Scheduling", 6103 ],
		"6295" : [ "Programme & Project Management", 6295 ],
		"6314" : [ "Programming & Production", 6314 ],
		"6038" : [ "Project Engineering", 6038 ],
		"6037" : [ "Project Management", 6037 ],
		"6119" : [ "Project Management", 6119 ],
		"6315" : [ "Promotions", 6315 ],
		"6202" : [ "Property Law", 6202 ],
		"6351" : [ "Psychology, Counselling & Social Work", 6351 ],
		"6019" : [ "Public Relations & Corporate Affairs", 6019 ],
		"6100" : [ "Public Transport & Taxi Services", 6100 ],
		"6109" : [ "Purchasing, Procurement & Inventory", 6109 ],
		"6106" : [ "Quality Assurance & Control", 6106 ],
		"6120" : [ "Quality Assurance & Control", 6120 ],
		"6224" : [ "Quality Assurance & Control", 6224 ],
		"6110" : [ "Rail & Maritime Transport", 6110 ],
		"6258" : [ "Receptionists", 6258 ],
		"6259" : [ "Records Management & Document Control", 6259 ],
		"6324" : [ "Recruitment - Agency", 6324 ],
		"6325" : [ "Recruitment - Internal", 6325 ],
		"6326" : [ "Remuneration & Benefits", 6326 ],
		"6129" : [ "Research & Fellowships", 6129 ],
		"6056" : [ "Reservations", 6056 ],
		"6004" : [ "Residential Leasing & Property Management", 6004 ],
		"6005" : [ "Residential Sales", 6005 ],
		"6370" : [ "Residents & Registrars", 6370 ],
		"6006" : [ "Retail & Property Development", 6006 ],
		"6050" : [ "Retail Assistants", 6050 ],
		"1499" : [ "Risk Consulting", 1499 ],
		"6111" : [ "Road Transport", 6111 ],
		"6088" : [ "Sales - Inbound", 6088 ],
		"6089" : [ "Sales - Outbound", 6089 ],
		"6296" : [ "Sales - Pre & Post", 6296 ],
		"6367" : [ "Sales Coordinators", 6367 ],
		"6368" : [ "Sales Representatives/Consultants", 6368 ],
		"6353" : [ "Sales", 6353 ],
		"6241" : [ "Security Services", 6241 ],
		"6297" : [ "Security", 6297 ],
		"6262" : [ "Self Employment", 6262 ],
		"6184" : [ "Settlements", 6184 ],
		"6354" : [ "Speech Therapy", 6354 ],
		"6185" : [ "Stockbroking & Trading", 6185 ],
		"6082" : [ "Strategy & Planning", 6082 ],
		"6158" : [ "Strategy & Planning", 6158 ],
		"6130" : [ "Student Services", 6130 ],
		"1524" : [ "Superannuation", 1524 ],
		"6090" : [ "Supervisors/Team Leaders", 6090 ],
		"6039" : [ "Supervisors", 6039 ],
		"6074" : [ "Surveying", 6074 ],
		"6121" : [ "Surveying", 6121 ],
		"6159" : [ "Systems Accounting & IT Audit", 6159 ],
		"6040" : [ "Systems Engineering", 6040 ],
		"6242" : [ "Tailors & Dressmakers", 6242 ],
		"6203" : [ "Tax Law", 6203 ],
		"6160" : [ "Taxation", 6160 ],
		"6132" : [ "Teaching - Early Childhood", 6132 ],
		"6133" : [ "Teaching - Primary", 6133 ],
		"6134" : [ "Teaching - Secondary", 6134 ],
		"6135" : [ "Teaching - Tertiary", 6135 ],
		"6136" : [ "Teaching - Vocational", 6136 ],
		"6131" : [ "Teaching Aides & Special Needs", 6131 ],
		"6107" : [ "Team Leaders/Supervisors", 6107 ],
		"6298" : [ "Team Leaders", 6298 ],
		"6299" : [ "Technical Writing", 6299 ],
		"6243" : [ "Technicians", 6243 ],
		"6300" : [ "Telecommunications", 6300 ],
		"6301" : [ "Testing & Quality Assurance", 6301 ],
		"1537" : [ "Tour Guides", 1537 ],
		"6020" : [ "Trade Marketing", 6020 ],
		"6327" : [ "Training & Development", 6327 ],
		"1542" : [ "Travel Agents/Consultants", 1542 ],
		"6161" : [ "Treasury", 6161 ],
		"6186" : [ "Treasury", 6186 ],
		"6137" : [ "Tutoring", 6137 ],
		"1544" : [ "Underwriting", 1544 ],
		"6272" : [ "Urban Design & Planning", 6272 ],
		"1546" : [ "Valuation", 1546 ],
		"6212" : [ "Veterinary Services & Animal Welfare", 6212 ],
		"6172" : [ "Volunteer Coordination & Support", 6172 ],
		"1549" : [ "Waiting Staff", 1549 ],
		"6108" : [ "Warehousing, Storage & Distribution", 6108 ],
		"6041" : [ "Water & Waste Engineering", 6041 ],
		"6273" : [ "Web & Interaction Design", 6273 ],
		"6302" : [ "Web Development & Production", 6302 ],
		"6244" : [ "Welders & Boilermakers", 6244 ],
		"6213" : [ "Winery & Viticulture", 6213 ],
		"1553" : [ "Workers' Compensation", 1553 ],
		"6138" : [ "Workplace Training & Assessment", 6138 ],
		"6356" : [ "Government", 6356 ],
		"6357" : [ "Local Government", 6357 ],
		"6358" : [ "Regional Council", 6358 ],
		"1409" : [ "Government - Federal", 1409 ],
		"1410" : [ "Government - Local", 1410 ],
		"1411" : [ "Government - State", 1411 ],
		"6051" : [ "Other", 6051 ],
		"6369" : [ "Other", 6369 ],
		"6162" : [ "Other", 6162 ],
		"6245" : [ "Other", 6245 ],
		"6122" : [ "Other", 6122 ],
		"6139" : [ "Other", 6139 ],
		"6021" : [ "Other", 6021 ],
		"6225" : [ "Other", 6225 ],
		"6274" : [ "Other", 6274 ],
		"6042" : [ "Other", 6042 ],
		"6328" : [ "Other", 6328 ],
		"6187" : [ "Other", 6187 ],
		"6007" : [ "Other", 6007 ],
		"6316" : [ "Other", 6316 ],
		"6173" : [ "Other", 6173 ],
		"6355" : [ "Other", 6355 ],
		"6204" : [ "Other", 6204 ],
		"6250" : [ "Other", 6250 ],
		"6361" : [ "Other", 6361 ],
		"6075" : [ "Other", 6075 ],
		"6083" : [ "Other", 6083 ],
		"6112" : [ "Other", 6112 ],
		"6057" : [ "Other", 6057 ],
		"6091" : [ "Other", 6091 ],
		"6280" : [ "Other", 6280 ],
		"6303" : [ "Other", 6303 ],
		"6214" : [ "Other", 6214 ],
		"6260" : [ "Other", 6260 ]
	}
};
var aRelationShips = {
	nation : {
		"3000|12" : [ "3101|13", "3106|13", "3103|13", "3107|13", "3104|13",
				"3105|13", "3100|13", "3102|13" ]
	},
	location : {
		"1000|1" : [ "5027", "5028", "5029", "5030", "5031", "5032", "5033" ],
		"1002|1" : [ "5069", "5070", "5071", "5072", "5073" ],
		"5147|1" : [ "5148", "5149" ],
		"5097|1" : [ "5098", "5099", "5100" ],
		"1004|1" : [ "5010", "5011", "5012", "5013", "5014" ],
		"1009|1" : [ "5048", "5049", "5050", "5051", "5052" ],
		"5123|1" : [ "5124", "5125" ],
		"5082|1" : [ "5083", "5084" ],
		"5140|1" : [ "5141", "5142", "5143", "5144", "5145", "5146" ],
		"5137|1" : [ "5138", "5139" ],
		"1018|1" : [ "5118", "5119", "5120", "5121", "5122" ],
		"5104|1" : [ "5105", "5106" ],
		"5131|1" : [ "5132", "5133" ],
		"5089|1" : [ "5090", "5091", "5092" ],
		"5093|1" : [ "5094", "5095", "5096" ],
		"5113|1" : [ "5114", "5115", "5116", "5117" ],
		"5110|1" : [ "5111", "5112" ],
		"5079|1" : [ "5080", "5081" ],
		"5085|1" : [ "5086", "5087", "5088" ],
		"5126|1" : [ "5127", "5128", "5129", "5130" ],
		"5101|1" : [ "5102", "5103" ],
		"1019|1" : [ "2831", "5107", "5108", "5109" ],
		"5134|1" : [ "5135", "5136" ],
		"5150|1" : [ "5151", "5152", "5153", "5154", "5155", "5156", "5157",
				"5158", "5159", "5160" ]
	},
	industry : {
		"6123" : [ "6124", "6125", "6126", "6127", "6128", "6129", "6130",
				"6132", "6133", "6134", "6135", "6136", "6131", "6137", "6138",
				"6139" ],
		"6362" : [ "6363", "6364", "6366", "6365", "6367", "6368", "6369" ],
		"7019" : [ "7020", "7021", "7022", "7023", "7024" ],
		"6246" : [ "6247", "6248", "6249", "6250" ],
		"1214" : [ "6275", "6276", "6277", "1350", "6278", "6279", "1499",
				"1524", "1544", "1553", "6280" ],
		"6058" : [ "6059", "6060", "6061", "6062", "6063", "6064", "6066",
				"6065", "6073", "6067", "6068", "6069", "6071", "6070", "6072",
				"6074", "6075" ],
		"1204" : [ "6084", "6085", "6086", "6087", "6088", "6089", "6090",
				"6091" ],
		"6008" : [ "6009", "6010", "6011", "6012", "6013", "6017", "6014",
				"6015", "6016", "6018", "6019", "6020", "6021" ],
		"1211" : [ "6329", "6330", "1353", "1372", "6331", "1386", "6332",
				"6333", "6334", "6335", "6336", "6337", "6338", "6339", "6340",
				"6341", "6342", "6343", "6344", "6345", "6346", "6347", "6348",
				"6349", "1467", "6350", "1470", "6352", "6351", "6370", "6353",
				"6354", "6355" ],
		"6092" : [ "6095", "6093", "6094", "6096", "6097", "6098", "6099",
				"6101", "6102", "6104", "6105", "6103", "6100", "6109", "6106",
				"6110", "6111", "6107", "6108", "6112" ],
		"6261" : [ "6262" ],
		"6304" : [ "6305", "6306", "6307", "6308", "6309", "6310", "6311",
				"6312", "6313", "6314", "6315", "6316" ],
		"6281" : [ "6282", "6283", "6284", "6285", "6286", "6287", "6288",
				"6289", "6290", "6291", "6292", "6293", "6294", "6295", "6296",
				"6297", "6298", "6299", "6300", "6301", "6302", "6303" ],
		"6251" : [ "6252", "6253", "6254", "6255", "6256", "6257", "6258",
				"6259", "6260" ],
		"1212" : [ "1315", "1332", "6052", "6053", "1405", "1415", "6054",
				"6055", "6056", "1537", "1542", "1549", "6057" ],
		"1210" : [ "1314", "1322", "1378", "1450", "6359", "6360", "6356",
				"6357", "6358", "1409", "1410", "1411", "6361" ],
		"6043" : [ "6044", "6045", "6046", "6047", "6048", "6049", "6050",
				"6051" ],
		"1216" : [ "6188", "6190", "6191", "6189", "6192", "6193", "6194",
				"6195", "6196", "6197", "6198", "1429", "6199", "1431", "6200",
				"6201", "6202", "6203", "6204" ],
		"1206" : [ "6113", "1387", "6114", "6115", "6116", "6117", "6118",
				"6119", "6120", "6121", "6122" ],
		"1225" : [ "1313", "1328", "6226", "6227", "6228", "1345", "6229",
				"6230", "6231", "6232", "1406", "6233", "6234", "6235", "6236",
				"6237", "6238", "6240", "6239", "6241", "6242", "6243", "6244",
				"6245" ],
		"6205" : [ "6206", "6207", "6208", "6209", "6210", "6211", "6212",
				"6213", "6214" ],
		"6317" : [ "6318", "6319", "6320", "6321", "6322", "6323", "6324",
				"6325", "6326", "6327", "6328" ],
		"1223" : [ "6215", "6216", "6217", "6218", "6219", "6220", "6221",
				"6222", "6223", "6224", "6225" ],
		"1209" : [ "6022", "6023", "6024", "6026", "6027", "6028", "6025",
				"6029", "6030", "6031", "6032", "6033", "6034", "6035", "6036",
				"6038", "6037", "6039", "6040", "6041", "6042" ],
		"1203" : [ "6174", "6175", "6177", "6178", "6176", "1352", "6179",
				"6180", "6181", "1392", "1404", "6182", "6183", "6184", "6185",
				"6186", "6187" ],
		"6263" : [ "6265", "6264", "6267", "6268", "6266", "6269", "6270",
				"6271", "6272", "6273", "6274" ],
		"1220" : [ "6371", "6001", "6002", "6003", "6004", "6005", "6006",
				"1546", "6007" ],
		"1210" : [ "1314", "1322", "1378", "1450", "6359", "6360", "6356",
				"6357", "6358", "1409", "1410", "1411", "6361" ],
		"1200" : [ "6140", "1303", "6141", "6143", "6142", "6144", "6145",
				"6146", "6147", "6148", "6149", "6150", "6151", "6152", "6153",
				"6154", "6155", "6157", "6156", "1468", "6158", "6159", "6160",
				"6161", "6162" ],
		"6163" : [ "6164", "6165", "6166", "6167", "6168", "6169", "6170",
				"6171", "6172", "6173" ],
		"6076" : [ "6077", "6078", "6079", "6080", "6081", "6082", "6083" ]
	},
	state : {
		"3100|13" : [ "1015|1" ],
		"3106|13" : [ "5075|1", "5076|1", "5067|1", "5068|1", "5064|1",
				"5074|1", "5077|1", "5078|1", "5066|1", "5065|1", "1002|1" ],
		"3103|13" : [ "5001|1", "5003|1", "5006|1", "5004|1", "5015|1",
				"5008|1", "5007|1", "5009|1", "5002|1", "5016|1", "5005|1",
				"1004|1", "1005|1" ],
		"3104|13" : [ "5019|1", "5023|1", "5017|1", "5022|1", "5018|1",
				"5021|1", "5020|1", "1007|1" ],
		"3102|13" : [ "5060|1", "5059|1", "1013|1" ],
		"3109|13" : [ "1025|1", "5161|1", "5150|1" ],
		"3110|13" : [ "5137|1", "5140|1", "5134|1", "5147|1" ],
		"3105|13" : [ "5063|1", "5062|1", "5061|1", "1011|1" ],
		"3101|13" : [ "5026|1", "5025|1", "5043|1", "5044|1", "5039|1",
				"5024|1", "5035|1", "5036|1", "5045|1", "5040|1", "5041|1",
				"5042|1", "5034|1", "5037|1", "5038|1", "1000|1" ],
		"3108|13" : [ "1018|1", "5101|1", "5126|1", "5089|1", "5104|1",
				"5085|1", "5082|1", "5097|1", "1019|1", "5131|1", "5079|1",
				"5113|1", "5093|1", "5123|1", "5110|1" ],
		"3107|13" : [ "5057|1", "5055|1", "5047|1", "5056|1", "5058|1",
				"5053|1", "5046|1", "5054|1", "1009|1" ]
	}
};
var categoryNames = [ "parentlocation", "childlocation", "nation", "state",
		"location", "area", "worktype", "industry", "occupation",
		"specialisation" ];
var defaultIndexes = {
	parentlocation : "3000|12",
	nation : "3000|12",
	state : "0",
	location : "0",
	area : "0",
	worktype : "0",
	industry : "0",
	occupation : "0",
	specialisation : "0"
};
var categorySelections = {
	parentlocation : new Array(),
	childlocation : new Array(),
	nation : new Array(),
	state : new Array(),
	location : new Array(),
	area : new Array(),
	worktype : new Array(),
	industry : new Array(),
	occupation : new Array(),
	specialisation : new Array()
};
var childBehaviours = {
	nation : [ "ShowDirectChildren", "ShowDirectChildren", "ShowDirectChildren" ],
	state : [ "ShowDirectChildren", "ShowDirectChildren", "ShowDirectChildren" ],
	location : [ "ShowDirectChildren", "ShowDirectChildren", "AggregateChild" ],
	area : [ "ShowDirectChildren", "ShowDirectChildren", "ShowDirectChildren" ],
	worktype : [ "ShowDirectChildren", "ShowDirectChildren",
			"ShowDirectChildren" ],
	industry : [ "ShowDirectChildren", "ShowDirectChildren", "AggregateChild" ],
	occupation : [ "ShowDirectChildren", "DisableChild", "DisableChild" ],
	specialisation : [ "ShowDirectChildren", "ShowDirectChildren",
			"ShowDirectChildren" ]
};
var isSorted = {
	nation : false,
	state : false,
	location : true,
	area : true,
	worktype : false,
	industry : true,
	occupation : true,
	specialisation : true,
	parentlocation : false,
	childlocation : false
};
var allTextGroups = {
	AllTextAU : {
		"3000|12" : "Everywhere in Australia",
		"1000|1" : "All Sydney",
		"1002|1" : "All Melbourne",
		"1004|1" : "All Brisbane",
		"1005|1" : "All Gold Coast",
		"1009|1" : "All Perth",
		"1007|1" : "All Adelaide",
		"1011|1" : "All Hobart",
		"1013|1" : "All Darwin",
		"3101|13" : "All Regional NSW",
		"3106|13" : "All Regional VIC",
		"3103|13" : "All Regional QLD",
		"3107|13" : "All Regional WA",
		"3104|13" : "All Regional SA",
		"3105|13" : "All Regional TAS",
		"3102|13" : "All Regional NT",
		"3001|12" : "All New Zealand",
		"3002|12" : "Everywhere in UK & Ireland",
		"3003|12" : "All Areas",
		"5140|1" : "All Asia Pacific",
		"5137|1" : "All Americas",
		"5134|1" : "All Europe and Russia",
		"5147|1" : "All Middle East and Africa",
		"0" : "Like... everywhere!",
		"5161|1" : "All London",
		"1025|1" : "All Ireland",
		"5150|1" : "All Rest of UK"
	},
	AllTextNZ : {
		"3001|12" : "Everywhere in New Zealand",
		"1018|1" : "All Auckland",
		"5089|1" : "All Bay of Plenty",
		"5113|1" : "All Canterbury",
		"5104|1" : "All Gisborne",
		"5085|1" : "All Hawkes Bay",
		"5097|1" : "All Manawatu",
		"5079|1" : "All Marlborough",
		"5101|1" : "All Northland",
		"5093|1" : "All Otago",
		"5110|1" : "All Southland",
		"5082|1" : "All Taranaki",
		"5131|1" : "All Tasman",
		"5126|1" : "All Waikato",
		"1019|1" : "All Wellington",
		"5123|1" : "All West Coast",
		"3000|12" : "Everywhere in Australia",
		"3002|12" : "All UK & Ireland",
		"3110|13" : "All Other"
	}
};
var allText = {
	nation : "AllTextAU,AllTextNZ"
};
var categoryTypes = [ "12|nation", "13|state", "1|location", "11|area",
		"4|worktype", "2|industry", "3|occupation", "10|specialisation" ];
var aGroupSeparatorMap = {
	nation : "GroupSeparatorItemAU|GroupSeparator|after",
	state : "GroupSeparatorItemAU|GroupSeparator|after",
	location : "GroupSeparatorItemALL|GroupSeparator|after",
	area : "",
	worktype : "",
	industry : "",
	occupation : "",
	specialisation : ""
};
var aGroupSeparators = {
	GroupSeparatorItemAU : [ "0", "1013|1", "3102|13", "3000|12", "3110|13",
			"5038|1", "5065|1", "5005|1", "5054|1", "5020|1", "5061|1",
			"5059|1" ],
	GroupSeparatorItemALL : [ "0" ]
};
var aGroupSeparatorTypes = {
	GroupSeparator : "------|optgroup"
};
var aReplicateInChildList = {
	state : [ "3101|13", "3106|13", "3103|13", "3107|13", "3104|13", "3105|13",
			"3100|13", "3102|13" ]
};
var aSpecialSelect = {};
aSuppressionConditions = {
	IsParent : {
		location : "~5027~5028~5029~5030~5031~5032~5033:1000|1~5069~5070~5071~5072~5073:1002|1~5148~5149:5147|1~5098~5099~5100:5097|1~5010~5011~5012~5013~5014:1004|1~5048~5049~5050~5051~5052:1009|1~5124~5125:5123|1~5083~5084:5082|1~5141~5142~5143~5144~5145~5146:5140|1~5138~5139:5137|1~5118~5119~5120~5121~5122:1018|1~5105~5106:5104|1~5132~5133:5131|1~5090~5091~5092:5089|1~5094~5095~5096:5093|1~5114~5115~5116~5117:5113|1~5111~5112:5110|1~5080~5081:5079|1~5086~5087~5088:5085|1~5127~5128~5129~5130:5126|1~5102~5103:5101|1~2831~5107~5108~5109:1019|1~5135~5136:5134|1~5151~5152~5153~5154~5155~5156~5157~5158~5159~5160:5150|1~1015|1:3100|13~5075|1~5076|1~5067|1~5068|1~5064|1~5074|1~5077|1~5078|1~5066|1~5065|1~1002|1:3106|13~5001|1~5003|1~5006|1~5004|1~5015|1~5008|1~5007|1~5009|1~5002|1~5016|1~5005|1~1004|1~1005|1:3103|13~5019|1~5023|1~5017|1~5022|1~5018|1~5021|1~5020|1~1007|1:3104|13~5060|1~5059|1~1013|1:3102|13~1025|1~5161|1~5150|1:3109|13~5137|1~5140|1~5134|1~5147|1:3110|13~5063|1~5062|1~5061|1~1011|1:3105|13~5026|1~5025|1~5043|1~5044|1~5039|1~5024|1~5035|1~5036|1~5045|1~5040|1~5041|1~5042|1~5034|1~5037|1~5038|1~1000|1:3101|13~1018|1~5101|1~5126|1~5089|1~5104|1~5085|1~5082|1~5097|1~1019|1~5131|1~5079|1~5113|1~5093|1~5123|1~5110|1:3108|13~5057|1~5055|1~5047|1~5056|1~5058|1~5053|1~5046|1~5054|1~1009|1:3107|13~3101|13~3106|13~3103|13~3107|13~3104|13~3105|13~3100|13~3102|13:3000|12"
	}
};
aConditionalSuppressions = {
	cs : {
		"1000|1" : "IsParent(location,3101|13)",
		"1002|1" : "IsParent(location,3106|13)",
		"1004|1" : "IsParent(location,3103|13)",
		"1005|1" : "IsParent(location,3103|13)",
		"1009|1" : "IsParent(location,3107|13)",
		"1007|1" : "IsParent(location,3104|13)",
		"1011|1" : "IsParent(location,3105|13)",
		"1013|1" : "IsParent(location,3102|13)",
		"6356" : "IsNotParent(location,3108|13)",
		"6357" : "IsNotParent(location,3108|13)",
		"6358" : "IsNotParent(location,3108|13)",
		"1409" : "IsParent(location,3108|13)",
		"1410" : "IsParent(location,3108|13)",
		"1411" : "IsParent(location,3108|13)"
	}
};
getFn = function(hashKey, _subType) {
	var subType = _subType || "";
	if (subType != "" && this[hashKey + "_" + subType] != undefined) {
		return this[hashKey + "_" + subType];
	} else {
		return this[hashKey];
	}
};
aCategories.get = getFn;
aRelationShips.get = getFn;
allText.get = getFn;
function getGroupSeparator(type) {
	return aGroupSeparatorTypes[type];
}
function isNZSite() {
	return false;
}
function isExecSite() {
	return false;
}
function getMinimumSelection() {
	return 1;
}
function getMinSelectOverride() {
	return 0;
}
function getListArray(dateRangeList) {
	return [ dateRangeList, document.getElementById("catnation"),
			document.getElementById("catstate"),
			document.getElementById("catlocation"),
			document.getElementById("catarea"),
			document.getElementById("catworktype"),
			document.getElementById("catindustry"),
			document.getElementById("catoccupation"),
			document.getElementById("catspecialisation"),
			document.getElementById("catparentlocation"),
			document.getElementById("catchildlocation") ];
}
function getForceDefault() {
	return [ "3000", "0", "0", "0", "0", "0", "0", "0" ];
}
function getUnspecifiedCheckBoxes() {
	return {
		nation : GetObjectByPartName("chkUnspecifiednation"),
		state : GetObjectByPartName("chkUnspecifiedstate"),
		location : GetObjectByPartName("chkUnspecifiedlocation"),
		area : GetObjectByPartName("chkUnspecifiedarea"),
		worktype : GetObjectByPartName("chkUnspecifiedworktype"),
		industry : GetObjectByPartName("chkUnspecifiedindustry"),
		occupation : GetObjectByPartName("chkUnspecifiedoccupation"),
		specialisation : GetObjectByPartName("chkUnspecifiedspecialisation"),
		childlocation : GetObjectByPartName("chkUnspecifiedchildlocation")
	};
}
function getMinSelectMsg() {
	return "";
}
function getPersistentValues() {
	return "";
}
window.jobsearchModelLoaded = true;

var controlPrefix = "cat", OPT_SEARCHBOX_CLASS_NAME = "searchbox-group", OPTGROUP_VALUE = "optgroup", jobsearchCommonLoaded = false;
var IE_revert_to_previosSelection = {};
if (typeof (jobsearchDisplayOrderType) == "undefined") {
	jobsearchDisplayOrderType = "";
}
_saved_salaryDropDownCountry = {
	cat : "",
	bottom : ""
};
var indentation = "\xa0\xa0\xa0\xa0";
function Querystring(qs) {
	var args, i = 0;
	this.params = {};
	this.get = function(key, defValue_) {
		var defValue = defValue_ || null;
		return this.params[key] || defValue;
	};
	if (qs === undefined) {
		qs = location.search.substring(1, location.search.length);
	}
	if (qs.length == 0) {
		return;
	}
	qs = qs.replace(/\+/g, " ");
	args = qs.split("&");
	for (; i < args.length; i++) {
		var value;
		var pair = args[i].split("=");
		var name = unescape(pair[0]);
		if (pair.length == 2) {
			value = unescape(pair[1]);
		} else {
			value = name;
		}
		this.params[name] = value;
	}
}
var ITIndustryID = "1215";
var unspecifiedSpecialisationID = "2148";
var allSpecialisationID = "2259";
var populateFromQueryString = true;
var hiddenIsSelfEmployClass = document.layout.IsSelfEmployClass;
var hiddenIsSelfEmploySubClass = document.layout.IsSelfEmploySubClass;
var SelfEmployWarningPlaceHolder = document
		.getElementById("SelfEmployWarningPlaceHolder");
var SelfEmployWarningHTML = document.layout.SelfEmployWarningHTML;
var hiddenIsITClass = document.layout.IsITClass;
var hiddenIsITSubClass = document.layout.IsITSubClass;
var isCampus = false;
var qsJobResults = "";
function SelectItem(catId, catIdx, selected) {
	var cat;
	if (typeof catId === "string") {
		cat = $("#" + controlPrefix + catId)[0];
	} else {
		cat = catId;
	}
	if (cat == null) {
		return;
	}
	cat[catIdx].selected = selected;
	if (selected) {
		IE_revert_to_previosSelection[catId] = cat[catIdx].value;
	}
}
function getItemValue(catId, catIdx) {
	var cat = $("#" + controlPrefix + catId)[0];
	if (cat != null) {
		return cat[catIdx].value;
	} else {
		return null;
	}
}
function hasNoDate() {
	return typeof (searchMode) != "undefined"
			&& (searchMode == "jobmail" || searchMode == "rss");
}
function setAnyCopy(catid, copy) {
	if (aCategories.get(catid) != null
			&& aCategories.get(catid)["0"] != undefined) {
		aCategories.get(catid)["0"][0] = copy;
	}
}
function getAnyCopy(catid) {
	if (aCategories.get(catid) != null) {
		if (aCategories.get(catid)["0"] != undefined) {
			return aCategories.get(catid)["0"][0];
		}
	}
	return "Any";
}
function getAnyOption(catid) {
	var copy = getAnyCopy(catid), r = new Option(copy, 0, false, false);
	r.innerText = copy;
	return r;
}
function scrollToItem(evt) {
	for (var catIdx = 0; catIdx < categoryNames.length; catIdx++) {
		if (categoryNames[catIdx] != "suppress") {
			var ddl = document.getElementById(controlPrefix
					+ categoryNames[catIdx]);
			var selections = categorySelections[categoryNames[catIdx]];
			if (ddl != null) {
				for (var i = 0; i < ddl.options.length; i++) {
					var selected = false;
					if (selections.length > 0) {
						selected = selections["OPT_" + ddl.options[i].value];
					} else {
						selected = ddl.options[i].selected;
					}
					if (selected) {
						if (ddl.options.type == "select-multiple") {
							SelectItem(categoryNames[catIdx], i, false);
							SelectItem(categoryNames[catIdx], i, true);
							break;
						} else {
							SelectItem(categoryNames[catIdx], i, true);
						}
					} else {
						if (ddl.options.type == "select-multiple") {
							SelectItem(categoryNames[catIdx], i, true);
							SelectItem(categoryNames[catIdx], i, false);
						}
					}
				}
			}
		}
	}
}
function findParentID(ID) {
	var type = getClassifierType(ID);
	if (type == "area") {
		parents = aRelationShips.get("location");
	} else {
		if (type == "location") {
			parents = aRelationShips.get("state");
		} else {
			if (type == "state") {
				parents = aRelationShips.get("nation");
			} else {
				return "";
			}
		}
	}
	for (key in parents) {
		related = parents[key];
		for (var index = 0; index < related.length; index++) {
			if (related[index] == ID) {
				return key;
			}
		}
	}
	return "";
}
function getParentCategory(category) {
	if (category == "area") {
		return "location";
	} else {
		if (category == "location") {
			return "state";
		} else {
			if (category == "state") {
				return "nation";
			} else {
				return "";
			}
		}
	}
}
function getTwoLowestLevelLocations(categorisedSelections) {
	var type = "area";
	var ret = [ "0", "0" ];
	var lastTestedIndex = 0;
	var tryParentSecondTime = false;
	var firstItemFound = true;
	if (categorisedSelections == undefined) {
		categorisedSelections = aCatSelected;
	}
	while (type != "") {
		if (categorisedSelections["cat" + type] != undefined) {
			ret[1] = categorisedSelections["cat" + type][lastTestedIndex];
			if (type != "area" && firstItemFound) {
				theParent = ret[1];
				ret[1] = "0";
			} else {
				var theParent = findParentID(ret[1]);
				if (tryParentSecondTime) {
					theParent = findParentID(theParent);
				}
			}
			if (theParent != "" || type == "nation" || type == "state") {
				var parentDropDownList = aCategories.get("parentlocation");
				for (var parentSearchIndex = 0; parentSearchIndex < parentDropDownList.length; parentSearchIndex++) {
					if (parentDropDownList[parentSearchIndex][1] == theParent) {
						ret[0] = theParent;
						var childSelectable = false;
						if (ret[1] == 0) {
							childSelectable = true;
						} else {
							var innerParentType = getClassifierType(ret[0]);
							var innerRelations = aRelationShips
									.get(innerParentType);
							if (innerRelations[ret[0]] != undefined) {
								var children = innerRelations[ret[0]];
								if (isNZSite()
										&& capitalCities[ret[0]] != undefined) {
									children = children
											.concat(capitalCities[ret[0]]);
								}
								for (var scanIndex = 0; scanIndex < children.length; scanIndex++) {
									if (children[scanIndex] == ret[1]) {
										childSelectable = true;
										break;
									}
								}
							}
						}
						if (childSelectable) {
							if (ret[1] != 0) {
								for (var parentSearchInner = 0; parentSearchInner < parentDropDownList.length; parentSearchInner++) {
									if (parentDropDownList[parentSearchInner][1] == ret[1]) {
										ret[0] = ret[1];
										ret[1] = "0";
										break;
									}
								}
							}
							return ret;
						}
					}
				}
				if (firstItemFound) {
					firstItemFound = false;
					ret[1] = theParent;
					ret[0] = -1;
					theParent = ret[1];
				} else {
					if (type == "location" && tryParentSecondTime == false) {
						tryParentSecondTime = true;
					} else {
						tryParentSecondTime = false;
						if (lastTestedIndex < categorisedSelections["cat"
								+ type].length - 1) {
							lastTestedIndex++;
						} else {
							lastTestedIndex = 0;
							type = getParentCategory(type);
						}
					}
				}
				if (type == "") {
					ret[0] = ret[1];
					ret[1] = "0";
					return ret;
				}
			} else {
				lastTestedIndex = 0;
				type = getParentCategory(type);
			}
		} else {
			type = getParentCategory(type);
			lastTestedIndex = 0;
		}
	}
	return ret;
}
function RetrieveSelectedLocations(selectionsHash) {
	if (selectionsHash == null) {
		selectionsHash = aCatSelected;
	}
	var theSelectedLocations = [];
	var goThrough = [ "nation", "state", "location", "area" ];
	for (var goThroughIndex = 0; goThroughIndex < goThrough.length; goThroughIndex++) {
		var toSelect = selectionsHash["cat" + goThrough[goThroughIndex]];
		if (toSelect) {
			theSelectedLocations = theSelectedLocations.concat(toSelect);
		}
	}
	theSelectedLocations = addRequiredParents(theSelectedLocations);
	if (theSelectedLocations.length == 0) {
		theSelectedLocations[0] = "0";
	}
	return theSelectedLocations;
}
function CollectLocationSelectionsFromDropDowns() {
	var allSelected = [];
	lists = [ controlPrefix + "parentlocation", controlPrefix + "childlocation" ];
	for (var index = 0; index < lists.length; index++) {
		var dd = document.getElementById(lists[index]);
		if (dd) {
			for (var oIndex = 0; oIndex < dd.options.length; oIndex++) {
				if (dd.options[oIndex].selected
						&& dd.options[oIndex].value != OPTGROUP_VALUE
						&& dd.options[oIndex].value != "0") {
					allSelected[allSelected.length] = dd.options[oIndex].value;
				}
			}
		}
	}
	return allSelected;
}
function addRequiredParents(selections) {
	nonSelectable = [];
	selectable = [];
	allRelated = [];
	var parentItems = aCategories.get("parentlocation");
	for (var index = 0; index < selections.length; index++) {
		for (var pIndex = 0; pIndex < parentItems.length; pIndex++) {
			if (parentItems[pIndex][1] == selections[index]) {
				selectable[selectable.length] = index;
				var theType = getClassifierType(selections[index]);
				if (theType) {
					var aRels = aRelationShips.get(theType);
					if (aRels) {
						var aRelItems = aRels[selections[index]];
						if (aRelItems != undefined) {
							allRelated = allRelated.concat(aRelItems);
						}
					}
				}
				break;
			}
		}
	}
	selectable = selectable.sort();
	var expectedIndex = 0;
	var currentSelectableIndex = 0;
	var toAdd = [];
	for (var index = 0; index < selections.length; index++) {
		var found = false;
		if (currentSelectableIndex < selectable.length) {
			if (selectable[currentSelectableIndex] == index) {
				found = true;
				currentSelectableIndex++;
			}
		}
		if (!found) {
			var notInParent = selections[index];
			found = false;
			for (var scanIndex = 0; scanIndex < allRelated.length; scanIndex++) {
				if (allRelated[scanIndex] == notInParent) {
					found = true;
					break;
				}
			}
			if (!found) {
				var theParent = findParentID(notInParent);
				if (theParent) {
					toAdd[toAdd.length] = theParent;
				}
			}
		}
	}
	return selections.concat(toAdd);
}
function getNewOption(label, val) {
	var opt = new Option(label, val, false, false);
	opt.innerText = label;
	return opt;
}
function getNewOptionGroup(label, val) {
	var re = /i(?=Phone|Pod|Pad)/, platform = navigator.platform, ua = navigator.userAgent, iOS6 = re
			.test(platform)
			&& (ua.indexOf("Version/6") !== -1);
	if (iOS6) {
		opt = getNewOption(label, val);
		opt.disabled = true;
	} else {
		var opt = document.createElement("optgroup");
		opt.label = label;
		opt.value = val;
	}
	return opt;
}
function moveChildrenToOptionGroups(list, checkForOptionParent) {
	var i, length = list.length, lastOptGroup = null, res = [];
	for (i = 0; i < length; i++) {
		if (list[i].tagName.toLowerCase() === OPTGROUP_VALUE) {
			lastOptGroup = list[i];
			res.push(lastOptGroup);
		} else {
			if (lastOptGroup !== null) {
				if (checkForOptionParent) {
					if (list[i].catParent === undefined) {
						res.push(list[i]);
						continue;
					} else {
						if (list[i].catParent.text !== lastOptGroup.label) {
							continue;
						}
					}
				}
				lastOptGroup.appendChild(list[i]);
			} else {
				res.push(list[i]);
			}
		}
	}
	return res.length == 0 ? list : res;
}
function ClearDropDown(ddl) {
	while (ddl.hasChildNodes()) {
		ddl.removeChild(ddl.lastChild);
	}
}
function populateClassiferList(ddListId, catID, includeItemCallback,
		existingSelectionsHash) {
	var ddl = document.getElementById(ddListId), classifers = aCategories
			.get(catID), defKey = defaultIndexes[catID], forceDefaultZero = (aDefaultOverrides[catID] != null), defaults = [], ItemIndex = 0, selections = [], defSelections = [], qs2 = new Querystring(), sortArray, sortIndex, theSelectedLocations, optionGroupFlag = false, opt, currentOptGroup = null, f, i, isSelected, finalList = [];
	if (typeof aCategories.get(catID) === "undefined") {
		return;
	}
	if (typeof existingSelectionsHash === "undefined"
			|| existingSelectionsHash == null) {
		existingSelectionsHash = aCatSelected;
	}
	if (forceDefaultZero || areCategoriesSelected(null, existingSelectionsHash)) {
		defKey = 0;
	}
	if (!ddl) {
		SelfEmployWarningDisplay(catID);
		return;
	}
	ClearDropDown(ddl);
	sortArray = [];
	sortIndex = 0;
	theSelectedLocations = RetrieveSelectedLocations(existingSelectionsHash);
	for ( var key in classifers) {
		isSelected = false;
		var classifier = classifers[key];
		if (classifier == undefined || isSuppressed(classifier[1])) {
			continue;
		}
		function getOptionItem() {
			var option = null, isOptGroup = false;
			if (includeItemCallback === undefined) {
				if (ddListId.indexOf("location") != -1) {
					isOptGroup = (classifier[1] === OPTGROUP_VALUE);
				}
				if (isOptGroup) {
					option = getNewOptionGroup(classifier[0], classifier[1]);
					option.className = OPT_SEARCHBOX_CLASS_NAME;
				} else {
					option = getNewOption(classifier[0], classifier[1]);
				}
			} else {
				if (includeItemCallback(classifier[1]) == true) {
					option = getNewOption(classifier[0], classifier[1]);
				}
			}
			return option;
		}
		opt = getOptionItem();
		if (opt === null) {
			continue;
		}
		if (qs2.get("Preselect") == "true") {
			var qs = location.search.substring(1, location.search.length), args = qs
					.split("&");
			for (var i = 0; i < args.length; i++) {
				var qsvalue = unescape(args[i].split("=")[1]);
				isSelected = (qsvalue == opt.value);
			}
		} else {
			if (ddl.id.indexOf("locationarea") == -1
					&& ddl.id.indexOf("parentlocation") == -1 && opt.value == 0) {
				if (defKey == 0
						&& !areCategoriesSelected(catID, existingSelectionsHash)) {
					defSelections["OPT_" + opt.value] = true;
				} else {
					defSelections["OPT_" + opt.value] = false;
				}
				defaults[0] = opt;
			} else {
				var selectedItems = existingSelectionsHash["cat" + catID];
				if (ddl.multiple == false) {
					if (catID == "parentlocation") {
						selectedItems = [
								getTwoLowestLevelLocations(existingSelectionsHash)[0],
								-1 ];
					} else {
						if (catID == "childlocation") {
							selectedItems = [
									getTwoLowestLevelLocations(existingSelectionsHash)[1],
									-1 ];
						}
					}
				} else {
					if (catID == "parentlocation" || catID == "childlocation") {
						selectedItems = theSelectedLocations;
					}
				}
				if (selectedItems != null && selectedItems.length > 0
						&& selectedItems[0] != 0) {
					for (sIdx = 0; sIdx < selectedItems.length; sIdx++) {
						if (selectedItems[sIdx] == classifier[1]
								&& qs2.get("Preselect") != "true") {
							isSelected = true;
							break;
						}
					}
				} else {
					if (opt.value == defKey && qs2.get("Preselect") != "true") {
						selections["OPT_" + opt.value] = true;
						isSelected = true;
					}
				}
				if (ddl.id.indexOf("parentlocation") == -1 && key != defKey
						&& qs2.get("Preselect") != "true") {
					sortArray[ItemIndex++] = opt;
					selections["OPT_" + opt.value] = isSelected;
				} else {
					defaults.push(opt);
					defSelections["OPT_" + opt.value] = isSelected;
				}
			}
		}
	}
	for (var d = 0; d < defaults.length; d++) {
		selections["OPT_" + defaults[d].value] = defSelections["OPT_"
				+ defaults[d].value];
		finalList.push(defaults[d]);
	}
	for (i = 0; i < sortArray.length; i++) {
		finalList.push(sortArray[i]);
	}
	for (i = 0; i < finalList.length; i++) {
		finalList[i].selected = selections["OPT_" + finalList[i].value];
	}
	if (catID == "parentlocation"
			&& existingSelectionsHash.catlocationtext != undefined) {
		var locations = existingSelectionsHash.catlocation;
		var matched = undefined;
		for (i = 0; i < finalList.length; i++) {
			if (finalList[i].text != undefined
					&& finalList[i].text == existingSelectionsHash.catlocationtext) {
				matched = finalList[i];
				break;
			}
		}
		for (i = 0; i < finalList.length; i++) {
			if (finalList[i].text != existingSelectionsHash.catlocationtext
					&& $(finalList[i]).val() == $(matched).val()) {
				finalList[i].selected = false;
			}
		}
	}
	finalList = moveChildrenToOptionGroups(finalList, false);
	for (var f = 0; f < finalList.length; f++) {
		var selIdx = ddl.options.length;
		ddl.appendChild(finalList[f]);
	}
	categorySelections[catID] = selections;
}
function areCategoriesSelected(categoryName, existingSelectionsHash) {
	if (existingSelectionsHash == null) {
		existingSelectionsHash = aCatSelected;
	}
	for (var idx = 0; idx < categoryNames.length; idx++) {
		if (categoryName === null || categoryName === undefined
				|| categoryName == categoryNames[idx]) {
			if (existingSelectionsHash["cat" + categoryNames[idx]] != null
					&& existingSelectionsHash["cat" + categoryNames[idx]].length > 0) {
				return true;
			}
		}
	}
	return false;
}
function insertOptGroup(arr, category) {
	var finalArray = [], mapPieces, i = 0;
	for (; i < arr.length; i++) {
		finalArray.push(arr[i]);
		var foundMap = IsOptGroupItem(arr[i].value, category);
		if (foundMap != "") {
			mapPieces = foundMap.split("|");
			if (mapPieces[2] == "after" && i != arr.length - 1) {
				if (arr.length - 1 <= i || arr[i + 1].value != OPTGROUP_VALUE) {
					finalArray.push(CreateGroupSeparatorOption(mapPieces[1]));
				}
			} else {
				if (mapPieces[2] == "before" && i != 0) {
					if (finalArray.length <= 1
							|| finalArray[finalArray.length - 2].value != OPTGROUP_VALUE) {
						Array.insert(finalArray, finalArray.length - 1,
								CreateGroupSeparatorOption(mapPieces[1]));
					}
				}
			}
		}
	}
	return finalArray;
}
function removeDuplicates(arr) {
	var optText = "", optValue = 0, resultArr = [];
	i = 0;
	for (; i < arr.length; i++) {
		if (arr[i].text != optText) {
			optText = arr[i].text;
			optValue = arr[i].value;
			resultArr.push(arr[i]);
		} else {
			if (optValue != arr[i].value) {
				resultArr[resultArr.length - 1].value += "," + arr[i].value;
			}
		}
	}
	return resultArr;
}
function UpdateParent(childObj, parentID) {
	var isChildLocation = childObj.id.indexOf("childlocation") >= 0;
	if (isChildLocation && childObj.size == 1) {
		var parent = document.getElementById(controlPrefix + parentID);
		if (childObj.value != 0 && parent != null) {
			for (var idx = 0; idx < parent.options.length; idx++) {
				if (parent.options[idx].value == childObj.value) {
					parent.options[idx].selected = true;
					var childID = childObj.id.replace(controlPrefix, "");
					ValidateCategoryList(parentID, 0, childID, true);
					break;
				}
			}
		}
	}
}
function UpdateSubClassifications(ddl) {
	var industry = document.getElementById(controlPrefix + "industry");
	var occupation = document.getElementById(controlPrefix + "occupation");
	var selections = [];
	if (industry && industry.options.length > 0 && occupation) {
		if (occupation && occupation.options.length > 0) {
			for (var index = 0; index < occupation.options.length; index++) {
				if (occupation.options[index].selected) {
					selections[selections.length] = occupation.options[index].value;
				}
			}
		}
		ValidateCategoryList("industry", 0, "occupation", true);
		for (var index = 0; index < occupation.options.length; index++) {
			if (occupation.options[index].selected) {
				occupation.options[index].selected = false;
			}
		}
		var foundSome = false;
		if (occupation && occupation.options.length > 0) {
			for (var selIndex = 0; selIndex < selections.length; selIndex++) {
				for (var index = 0; index < occupation.options.length; index++) {
					if (occupation.options[index].value == selections[selIndex]) {
						occupation.options[index].selected = true;
						foundSome = true;
					}
				}
			}
		}
		if (!foundSome && occupation && occupation.options.length > 0) {
			occupation.value = "0";
		}
	}
}
function ValidateCategoryList(categoryId, maxItems, childCategoryId,
		populateChildren) {
	var defIdx = defaultIndexes[categoryId];
	var ddl = document.getElementById(controlPrefix + categoryId);
	var ddlChild = document.getElementById(controlPrefix + childCategoryId);
	if (ddl.multiple == false && ddl.value == OPTGROUP_VALUE) {
		if (IE_revert_to_previosSelection[categoryId] != undefined) {
			ddl.value = IE_revert_to_previosSelection[categoryId];
		}
		var e = window.event;
		if (e.preventDefault) {
			e.preventDefault();
		} else {
			e.returnValue = false;
		}
		return;
	}
	if (ddl.value != OPTGROUP_VALUE) {
		IE_revert_to_previosSelection[categoryId] = ddl.value;
	}
	populateFromQueryString = false;
	showedMainAdvanceSel = true;
	showedBottomAdvanceSel = true;
	if (ddl.multiple) {
		var allAnyIdx = 0;
		for (var idx = 0; idx < ddl.options.length; idx++) {
			if (ddl.options[idx].value == "0") {
				allAnyIdx = idx;
				break;
			}
		}
		for (var idx = 0; idx < ddl.options.length; idx++) {
			if (ddl.options[idx].selected && idx != allAnyIdx) {
				ddl.options[allAnyIdx].selected = false;
				break;
			}
		}
	}
	enableDisableUnspecifiedCheckBox(categoryId);
	if (maxItems > 0) {
		var selectedIdx = ddl.selectedIndex;
		if (ddl != null) {
			var removeIdx = 0;
			var removeOptions = [];
			var selected = 0;
			for (idx = 0; idx < ddl.options.length; idx++) {
				if (ddl.options[idx].selected) {
					if (selected > maxItems) {
						removeOptions[removeIdx++] = idx;
					}
					selected++;
				}
			}
			if (selected > maxItems) {
				alert("Only " + maxItems
						+ " items may be selected for category " + categoryId);
				for (rIdx = 0; rIdx < removeOptions.length; rIdx++) {
					ddl.options[removeOptions[rIdx]].selected = false;
				}
				populateChildren = false;
			}
		}
	}
	SelfEmployWarningDisplay(categoryId);
	if (populateChildren) {
		var selectedParent = 0;
		for (var i = 0; i < ddl.options.length; i++) {
			if (ddl.options[i].selected) {
				selectedParent++;
			}
		}
		if (selectedParent <= 1 && ddlChild != null) {
			ddlChild.selectedIndex = 0;
		}
		aCatSelected[controlPrefix + childCategoryId] = [];
		if (!isCampus) {
			ProcessChildList(categoryId, childCategoryId);
		}
	}
	var cat = GetObjectByPartName(controlPrefix + categoryId);
	if (categoryId == "worktype") {
		worktypeChanged(cat);
	}
}
function enableDisableUnspecifiedCheckBox(childListId) {
	var ddl = document.getElementById(controlPrefix + childListId), idx = 0, searchIndex = 0, anyOption = null, areaSelected = false;
	if (ddl == null) {
		return;
	}
	for (; searchIndex < ddl.options.length; searchIndex++) {
		if (ddl.options[searchIndex].value == 0) {
			anyOption = ddl.options[searchIndex];
			break;
		}
	}
	for (; idx < ddl.options.length; idx++) {
		if (ddl.options[idx].selected) {
			if (ddl.options[idx].value != 0) {
				if (anyOption != null) {
					anyOption.selected = false;
				}
				if (ddl.options[idx].value != OPTGROUP_VALUE
						&& ddl.options[idx].value.indexOf("|") < 0) {
					areaSelected = true;
					break;
				}
			}
		}
	}
	var chk = GetObjectByPartName("chkUnspecified" + childListId,
			controlPrefix == "cat" ? 1 : 2);
	if (chk != null) {
		if (anyOption != null && (anyOption.selected || areaSelected == false)) {
			chk.checked = false;
			chk.disabled = true;
		} else {
			if (chk.disabled) {
				chk.checked = false;
				chk.disabled = false;
			}
		}
	}
}
function SelfEmployWarningDisplay(parentCategoryId) {
	var ddl = document.getElementById(controlPrefix + parentCategoryId);
	var selValues = "";
	if (ddl != null) {
		for (var idx = 1; idx < ddl.options.length; idx++) {
			if (ddl.options[idx].selected) {
				selValues = selValues + ":" + ddl.options[idx].value;
			}
		}
		if (parentCategoryId == "industry" && hiddenIsSelfEmployClass != null) {
			if (selValues.indexOf("1224") != -1) {
				hiddenIsSelfEmployClass.value = "true";
				SelfEmployWarningPlaceHolder.innerHTML = SelfEmployWarningHTML.value;
			} else {
				hiddenIsSelfEmployClass.value = "false";
				SelfEmployWarningPlaceHolder.innerHTML = "";
			}
		}
	}
}
function ProcessChildList(parentCategoryId, childCategoryId) {
	var ddl = document.getElementById(controlPrefix + parentCategoryId);
	var mode = 0;
	var anyOption = false;
	var classifiers = aCategories.get(parentCategoryId);
	if (typeof (classifiers) == "undefined") {
		return;
	}
	if (!ddl) {
		return;
	}
	if (ddl.id.indexOf("parentlocation") == -1) {
		if (ddl.options[0] != undefined) {
			anyOption = ddl.options[0].selected;
		}
	}
	if (anyOption) {
		mode = 2;
	} else {
		var selCount = 0;
		mode = 0;
		for (var idx = 1; idx < ddl.options.length; idx++) {
			if (ddl.options[idx].selected) {
				selCount++;
				if (selCount > 1) {
					mode = 1;
					break;
				}
			}
		}
		if (ddl.options.selectedIndex != -1) {
			SetHiddenFields(selCount, parentCategoryId,
					ddl.options[ddl.options.selectedIndex].value);
		}
		SetSelfEmployHiddenFields(parentCategoryId);
	}
	SelfEmployWarningDisplay(parentCategoryId);
	if (parentCategoryId == "parentlocation") {
		processListSelect(childBehaviours.location[mode], parentCategoryId,
				childCategoryId);
	} else {
		processListSelect(childBehaviours[parentCategoryId][mode],
				parentCategoryId, childCategoryId);
	}
	if (parentChildRelationships[childCategoryId] != null) {
		ProcessChildList(childCategoryId,
				parentChildRelationships[childCategoryId]);
	}
}
function SetSelfEmployHiddenFields(parentCategoryId) {
	if (hiddenIsSelfEmployClass != null && hiddenIsSelfEmploySubClass != null) {
		if (parentCategoryId == "occupation"
				&& hiddenIsSelfEmployClass.value != "true") {
			var ddl = document.getElementById(controlPrefix + parentCategoryId);
			var selValues = "";
			for (var idx = 1; idx < ddl.options.length; idx++) {
				if (ddl.options[idx].selected) {
					selValues = selValues + ":" + ddl.options[idx].value;
				}
			}
			var SelfEmployOccupationIDArray = new Array("1341", "1354", "1399",
					"1400", "1451", "1552");
			hiddenIsSelfEmploySubClass.value = "false";
			SelfEmployWarningPlaceHolder.innerHTML = "";
			for (var i = 0; i < SelfEmployOccupationIDArray.length; i++) {
				if (selValues.indexOf(SelfEmployOccupationIDArray[i]) != -1) {
					hiddenIsSelfEmploySubClass.value = "true";
					SelfEmployWarningPlaceHolder.innerHTML = SelfEmployWarningHTML.value;
				}
			}
		}
	}
}
function SetHiddenFields(selCount, parentCategoryId, selectedValue) {
	if (selCount == 1) {
		if (parentCategoryId == "industry" && hiddenIsITClass != null) {
			if (selectedValue == ITIndustryID) {
				hiddenIsITClass.value = "true";
			} else {
				hiddenIsITClass.value = "false";
			}
			hiddenIsITSubClass.value = "false";
		} else {
			if (parentCategoryId == "occupation" && hiddenIsITSubClass != null) {
				if (hiddenIsITClass.value == "true") {
					hiddenIsITSubClass.value = "true";
				} else {
					hiddenIsITSubClass.value = "false";
				}
			}
		}
	} else {
		if (parentCategoryId == "industry" && hiddenIsITClass != null) {
			hiddenIsITClass.value = "false";
		} else {
			if (parentCategoryId == "occupation" && hiddenIsITSubClass != null) {
				hiddenIsITSubClass.value = "false";
			}
		}
	}
}
function processListSelect(action, parentListId, childListId) {
	switch (action) {
	case "AggregateChild":
		aggregateChildList(parentListId, childListId);
		break;
	case "DisableChild":
		clearCategoryList(parentListId, childListId, true,
				"No Selection Required");
		break;
	case "EmptyChild":
		break;
	case "ShowDirectChildren":
	case "Default":
	default:
		populateChildList(parentListId, childListId);
		break;
	}
}
function clearCategoryList(parentListId, childListId, showAny, anyCopy) {
	var pList = document.getElementById(controlPrefix + parentListId);
	var cList = document.getElementById(controlPrefix + childListId);
	var defIdx = defaultIndexes[childListId];
	if (pList == null || cList == null) {
		return;
	}
	var anyOption = pList.options[0];
	for (var cIdx = cList.options.length - 1; cIdx >= 0; cIdx--) {
		cList.remove(cIdx);
	}
	disableCheckBox(childListId);
}
function aggregateChildList(parentListId, childListId) {
	clearCategoryList(parentListId, childListId, false, getAnyCopy(childListId));
	var cList = document.getElementById(controlPrefix + childListId);
	var defIdx = defaultIndexes[childListId];
	var classifers = aCategories.get(childListId);
	var anyOption = getAnyOption(childListId);
	var nonDefaults = 0;
	var sortIdx = 0;
	var selectedItems = aCatSelected[controlPrefix + childListId];
	var sortedArray = [];
	var defaults = [];
	var selections = [];
	var qs2 = new Querystring();
	for ( var key in classifers) {
		var cItem = classifers[key];
		if (cItem == null) {
			continue;
		}
		var newOpt = getNewOption(cItem[0], cItem[1]);
		var isSelected = false;
		if (selectedItems != null) {
			for (sIdx = 0; sIdx < selectedItems.length; sIdx++) {
				if (cItem[1] == selectedItems[sIdx]
						&& qs2.get("Preselect") != "true"
						&& populateFromQueryString == true) {
					newOpt.selected = true;
					isSelected = true;
					nonDefaults++;
					break;
				}
			}
		}
		selections["OPT_" + newOpt.value] = isSelected;
		if (newOpt.value == defIdx) {
			defaults[defaults.length] = newOpt;
		} else {
			sortedArray[sortIdx++] = newOpt;
		}
	}
	var sorted = isSorted[childListId], tmpElem;
	if (sorted == null || sorted) {
		sortedArray.sort(sortCategory);
		if (childListId === "occupation" || childListId === "function") {
			var otherItems = [];
			var otherIndices = [];
			for (var scanIndex = 0; scanIndex < sortedArray.length; scanIndex++) {
				tmpElem = sortedArray[scanIndex];
				if (tmpElem.text == "Other") {
					otherItems.push(tmpElem);
					otherIndices.push(scanIndex);
				}
			}
			if (otherItems.length > 0) {
				for (var index = 0; index < otherItems.length; index++) {
					sortedArray.push(otherItems[index]);
				}
				for (var index = otherIndices.length - 1; index >= 0; index--) {
					sortedArray.splice(otherIndices[index], 1);
				}
			}
		}
	}
	var finalArray = [];
	finalArray[0] = anyOption;
	selections["OPT_" + anyOption.value] = (nonDefaults == 0 && defIdx == 0);
	for (var dIdx = 0; dIdx < defaults.length; dIdx++) {
		var defOpt = defaults[dIdx];
		selections["OPT_" + defOpt.value] = (nonDefaults == 0);
		finalArray[finalArray.length] = defOpt;
	}
	for (var s = 0; s < sortedArray.length; s++) {
		finalArray[finalArray.length] = sortedArray[s];
	}
	finalArray = removeDuplicates(finalArray);
	var insertAt = 0;
	for (var i = 0; i < finalArray.length; i++) {
		if (cList) {
			insertAt = cList.options.length;
			cList.appendChild(finalArray[i]);
		}
		var sel = false;
		var arrIds = finalArray[i].value.split(",");
		for (var j = 0; j < arrIds.length; j++) {
			sel = selections["OPT_" + arrIds[j]];
			if (sel) {
				break;
			}
		}
		SelectItem(cList, insertAt, sel);
	}
	aCatSelected[controlPrefix + childListId] = [];
	enableDisableUnspecifiedCheckBox(childListId);
	if (populateFromQueryString) {
		populateCheckBox(childListId);
	}
}
function setSelectedItems(childListId) {
	var selectedID = childListId;
	var selectedItems = aCatSelected[controlPrefix + selectedID];
	if (selectedItems == undefined) {
		var prefix = controlPrefix;
		if (controlPrefix == "bottom") {
			prefix = "cat";
		}
		var areaSelected = aCatSelected[prefix + "area"];
		var locationSelected = aCatSelected[prefix + "location"];
		var stateSelected = aCatSelected[prefix + "state"];
		if (childListId == "childlocation") {
			var setArea = (locationSelected != undefined
					&& itemFound(locationSelected, "parentlocation") && stateSelected == undefined);
			if (areaSelected != undefined && areaSelected != "" && setArea) {
				selectedID = "area";
			} else {
				if (locationSelected != undefined && stateSelected != undefined) {
					selectedID = "location";
				}
			}
			selectedItems = aCatSelected[prefix + selectedID];
		}
	}
	return selectedItems;
}
function RetrieveCategoryItemIntID(categoryItemValue) {
	categoryItemValue = categoryItemValue.toString();
	if (categoryItemValue != -1 && categoryItemValue.indexOf("|") > 0) {
		categoryItemValue = categoryItemValue.substring(0, categoryItemValue
				.indexOf("|"));
	}
	return categoryItemValue;
}
function setOptionSelected(opt, anyOpt, val, selections) {
	opt.selected = val;
	anyOpt.selected = !val;
	selections["OPT_" + opt.value] = val;
	selections["OPT_" + anyOpt.value] = !val;
}
function sortLocationOptions(allSelections) {
	var oInd = 0, order = [ 12, 13, 1 ], orderLength = order.length, selLength = allSelections.length, pIdx, splitValue, res = [];
	for (; oInd < orderLength; oInd++) {
		for (pIdx = 0; pIdx < selLength; pIdx++) {
			splitValue = allSelections[pIdx].value.split("|");
			if (splitValue.length > 1 && splitValue[1] == order[oInd]) {
				res.push(allSelections[pIdx]);
			}
		}
	}
	return res;
}
function handleFirstFakeOptGroup(resultOptions, childList) {
	var optNode = null, parentNodeText, i = 0;
	if (resultOptions.length > 2 && resultOptions[1].value === OPTGROUP_VALUE
			&& resultOptions[2].catParent) {
		resultOptions[1] = getNewOptionGroup(resultOptions[2].catParent.text,
				OPTGROUP_VALUE);
	}
}
function handleAllCaption(options) {
	if (options && options.length > 1) {
		options[0].text = "All Areas";
	}
}
function populateChildList(parentListId, childListId, existingSelections) {
	var cList = document.getElementById(controlPrefix + childListId), pList = document
			.getElementById(controlPrefix + parentListId), i, defIdx, pIdx, optGroup, parentSelections = [], parentClassifiers = aCategories
			.get(parentListId), childClassifiers = aCategories.get(childListId), childSelectedOptions = [], parentSelectedOptions = [], parentsWithChildrenAddedCount = 0, isResultGroupingRequired = false, isSingleOptionHandlingRequired = true, selections = [], selectedItems, needsSeparatorOnParentChange = false, currentParentID = "", previousParentID = "", resultOptions = [];
	existingSelections = existingSelections || aCatSelected;
	if (pList === null || cList === null) {
		return;
	}
	if (childListId == "childlocation") {
		defIdx = defaultIndexes.location;
	} else {
		defIdx = defaultIndexes[childListId];
	}
	for (var cIdx = 0; cIdx < cList.options.length; cIdx++) {
		if (cList.options[cIdx].selected) {
			var arrIds = cList.options[cIdx].value.split(",");
			for (i = 0; i < arrIds.length; i++) {
				childSelectedOptions.push(arrIds[i]);
			}
		}
	}
	if (pList.options.selectedIndex >= 0) {
		if (pList.options[pList.options.selectedIndex].value.split(",").length > 1) {
			clearCategoryList(parentListId, childListId, true,
					"No Selection is Required");
			return;
		}
	}
	clearCategoryList(parentListId, childListId, false, getAnyCopy(childListId));
	var aRels = aRelationShips.get(parentListId);
	if (parentListId == "parentlocation") {
		aRels = aRelationShips.get("location");
		if (aRels == null) {
			aRels = aRelationShips.get("state");
		}
	}
	if (populateFromQueryString) {
		if (cList.multiple == false) {
			if (childListId == "parentlocation") {
				selectedItems = [
						getTwoLowestLevelLocations(existingSelections)[0], -1 ];
			} else {
				if (childListId == "childlocation") {
					selectedItems = [
							getTwoLowestLevelLocations(existingSelections)[1],
							-1 ];
				}
			}
		} else {
			if (childListId == "childlocation") {
				selectedItems = RetrieveSelectedLocations(existingSelections);
			}
		}
	} else {
		selectedItems = [];
	}
	var optIdx = 0;
	var anyOption = getAnyOption(childListId);
	if (defIdx == 0) {
		anyOption.selected = true;
		selections["OPT_" + anyOption.value] = true;
	}
	if (aRels != null) {
		var parentSelectedOptions = getSelectedOptions(pList);
		if (parentListId == "parentlocation") {
			parentSelectedOptions = sortLocationOptions(parentSelectedOptions);
		}
		for (pIdx = 0; pIdx < parentSelectedOptions.length; pIdx++) {
			var parentOption = parentSelectedOptions[pIdx];
			var qs2 = new Querystring();
			if (qs2.get("Preselect") == "true"
					&& populateFromQueryString == true) {
				qs = location.search.substring(1, location.search.length);
				var args = qs.split("&");
				selectedItems = new Array(args.length);
				for (i = 0; i < args.length; i++) {
					var qsvalue = unescape(args[i].split("=")[1]);
					if (qsvalue == parentOption.value
							&& parentOption.value != 0) {
						parentOption.selected = true;
					}
					if (qsvalue != 0) {
						selectedItems[i] = qsvalue;
					} else {
						selectedItems[i] = -999;
					}
				}
			}
			parentClassifierType = getClassifierType(parentOption.value);
			if (parentClassifierType == "location") {
				for (stateKey in aRelationShips.get("state")) {
					stateArray = aRelationShips.get("state")[stateKey];
					for (stateIndex = 0; stateIndex < stateArray.length; stateIndex++) {
						if (stateArray[stateIndex] == parentOption.value) {
							if (currentParentID != stateKey) {
								if (currentParentID != "") {
									needsSeparatorOnParentChange = true;
								}
								currentParentID = stateKey;
								parentsWithChildrenAddedCount += 1;
							}
						}
					}
				}
			} else {
				if (parentClassifierType == "state") {
					if (currentParentID != parentOption.value) {
						if (currentParentID != "") {
							needsSeparatorOnParentChange = true;
						}
						currentParentID = parentOption.value;
						parentsWithChildrenAddedCount += 1;
					}
				}
			}
			var itemRels = aRels[parentOption.value];
			if (itemRels == null) {
				var defaultValue = defaultIndexes.parentlocation;
				if (defaultValue != parentOption.value) {
					itemRels = aRelationShips.get("state")[parentOption.value];
				}
			}
			if (isNZSite() && itemRels == null) {
				if (capitalCities[parentOption.value] != null) {
					itemRels = capitalCities[parentOption.value];
					itemRels = itemRels
							.concat(aRelationShips.get("nation")[parentOption.value]);
				}
			}
			if (itemRels) {
				for (idx = 0; idx < itemRels.length; idx++) {
					var rIdx = itemRels[idx], cat = childClassifiers[rIdx], opt = null;
					if (isSuppressed(cat[1])) {
						continue;
					}
					opt = getNewOption(cat[0], cat[1]);
					opt.catParent = parentOption;
					if (selectedItems != null) {
						for (sIdx = 0; sIdx < selectedItems.length; sIdx++) {
							var optionItem = RetrieveCategoryItemIntID(opt.value);
							var selectedItem = RetrieveCategoryItemIntID(selectedItems[sIdx]);
							if (optionItem == selectedItem) {
								setOptionSelected(opt, anyOption, true,
										selections);
							}
						}
					}
					for (var oldSelIdx = 0; oldSelIdx < childSelectedOptions.length; oldSelIdx++) {
						if (opt.value == childSelectedOptions[oldSelIdx]) {
							setOptionSelected(opt, anyOption, true, selections);
						}
					}
					var add = true;
					var optValueDup = 0;
					var optIndexDup = 0;
					for (var dupIdx = 0; dupIdx < resultOptions.length; dupIdx++) {
						if (opt.text == resultOptions[dupIdx].text) {
							add = false;
							if (opt.value != resultOptions[dupIdx].value) {
								optValueDup = opt.value;
								optIndexDup = dupIdx;
							}
							break;
						}
					}
					if (optValueDup != 0) {
						if (parentListId == "parentlocation"
								&& childListId == "childlocation") {
							if (needsSeparatorOnParentChange) {
								needsSeparatorOnParentChange = false;
								resultOptions[optIdx++] = getNewOptionGroup(
										parentOption.text, currentParentID);
							}
							resultOptions[optIdx++] = opt;
						} else {
							resultOptions[optIndexDup].value += "," + opt.value;
						}
					} else {
						if (add) {
							if (needsSeparatorOnParentChange) {
								needsSeparatorOnParentChange = false;
								resultOptions[optIdx++] = getNewOptionGroup(
										parentOption.text, currentParentID);
							}
							resultOptions[optIdx++] = opt;
						}
					}
					if (removeOptionByValue(opt.value, parentSelectedOptions)
							&& add) {
						if (typeof thisItemRels !== "undefined") {
							for (areaIndex = 0; areaIndex < thisItemRels.length; areaIndex++) {
								var childRelNew = aCategories
										.get("childlocation")[thisItemRels[areaIndex]];
								var newOption = getNewOption(childRelNew[0],
										childRelNew[1]);
								resultOptions[optIdx++] = newOption;
								if (selectedItems != null) {
									for (sIdx = 0; sIdx < selectedItems.length; sIdx++) {
										var optionItem = RetrieveCategoryItemIntID(newOption.value);
										var selectedItem = RetrieveCategoryItemIntID(selectedItems[sIdx]);
										if (optionItem == selectedItem) {
											newOption.selected = true;
											selections["OPT_" + newOption.value] = true;
										}
									}
								}
							}
						}
					}
				}
			}
		}
		if (resultOptions.length == 1) {
			isSingleOptionHandlingRequired = false;
		} else {
			resultOptions.splice(0, 0, anyOption);
		}
		if (parentListId != "parentlocation" || childListId != "childlocation") {
			resultOptions = removeDuplicates(resultOptions);
		}
		if (childListId == "childlocation") {
			resultOptions = insertOptGroup(resultOptions, "location");
		} else {
			resultOptions = insertOptGroup(resultOptions, childListId);
		}
		isResultGroupingRequired = parentsWithChildrenAddedCount > 1;
		if (isResultGroupingRequired && childListId === "childlocation") {
			handleFirstFakeOptGroup(resultOptions, cList);
			handleAllCaption(resultOptions);
			resultOptions = moveChildrenToOptionGroups(resultOptions, true);
		}
		if (parentsWithChildrenAddedCount > 1) {
			resultOptions[0].text = "All Areas";
			resultOptions[0].value = 0;
			isSingleOptionHandlingRequired = false;
		}
		ClearDropDown(cList);
		for (i = 0; i < resultOptions.length; i++) {
			cList.appendChild(resultOptions[i]);
		}
	}
	if (isSingleOptionHandlingRequired) {
		SetChildAnyText(cList, pList);
	}
	enableDisableUnspecifiedCheckBox(childListId);
	if (populateFromQueryString) {
		populateCheckBox(childListId);
	}
	if (cList.childNodes.length == 1) {
		cList.disabled = true;
	} else {
		cList.disabled = false;
	}
}
function getSelectedIDs(selectList) {
	ret = [];
	for (index = 0; index < selectList.options.length; index++) {
		if (selectList.options[index].selected
				&& selectList.options[index].value != "0"
				&& selectList.options[index].value != OPTGROUP_VALUE) {
			ret[ret.length] = selectList.options[index].value;
		}
	}
	return ret;
}
function isSuppressed(item) {
	var hashKey = "cs";
	if (aConditionalSuppressions[hashKey] == undefined) {
		return false;
	}
	var theCS = aConditionalSuppressions[hashKey];
	if (theCS[item] != undefined) {
		var condition = theCS[item];
		var regex = /IsParent\((\w+),\s*([A-Z0-9\|]+)\)/i;
		var m = regex.exec(condition);
		if (m != null) {
			if (m[1] == "location") {
				var selectedLocations = [];
				selectedLocations = selectedLocations
						.concat(getSelectedIDs(document
								.getElementById(controlPrefix
										+ "parentlocation")));
				if (selectedLocations.length == 0) {
					return false;
				}
				cp = Suppression_CountParent(m[1], selectedLocations, m[2]);
				return (cp[0] == selectedLocations.length);
			}
		}
		regex = /IsNotParent\((\w+),\s*([A-Z0-9\|]+)\)/i;
		var m = regex.exec(condition);
		if (m != null) {
			if (m[1] == "location") {
				var selectedLocations = [];
				selectedLocations = selectedLocations
						.concat(getSelectedIDs(document
								.getElementById(controlPrefix
										+ "parentlocation")));
				selectedLocations = selectedLocations
						.concat(getSelectedIDs(document
								.getElementById(controlPrefix + "childlocation")));
				if (selectedLocations.length == 0) {
					return false;
				}
				cp = Suppression_CountParent(m[1], selectedLocations, m[2]);
				return (cp[1] == selectedLocations.length);
			}
		}
	}
	return false;
}
function Suppression_CountParent(categoryType, categorySelections, parentID) {
	var parentHash = aSuppressionConditions.IsParent;
	var list = parentHash[categoryType];
	var parentMatch = 0;
	var parentNoMatch = 0;
	for (var currentSelectionIndex = 0; currentSelectionIndex < categorySelections.length; currentSelectionIndex++) {
		var currentClassifier = categorySelections[currentSelectionIndex];
		if (currentClassifier == "0") {
			continue;
		}
		var couldNotFindEntry = false;
		currentClassifierParentMatches = false;
		while (true) {
			if (currentClassifier == parentID) {
				currentClassifierParentMatches = true;
				break;
			}
			var substringIndex = list.indexOf("~" + currentClassifier + "~");
			if (substringIndex < 0) {
				substringIndex = list.indexOf("~" + currentClassifier + ":");
			}
			if (substringIndex >= 0) {
				var nextColonIndex = list.indexOf(":", substringIndex
						+ currentClassifier.length);
				var endIndex = list.indexOf("~", nextColonIndex);
				if (endIndex < 0) {
					endIndex = list.length;
				}
				var currentParentID = list.substring(nextColonIndex + 1,
						endIndex);
				if (currentParentID == parentID) {
					currentClassifierParentMatches = true;
					break;
				} else {
					currentClassifierParentMatches = false;
					currentClassifier = currentParentID;
				}
			} else {
				couldNotFindEntry = true;
				break;
			}
		}
		if (currentClassifierParentMatches == false || couldNotFindEntry) {
			parentNoMatch++;
		} else {
			parentMatch++;
		}
	}
	return new Array(parentMatch, parentNoMatch);
}
function getSelectedOptions(pList) {
	var ssKey = "ss", theSSMap = aSpecialSelect[ssKey], allSelectionsRaw = [], items, alreadyAdded;
	for (pIdx = 0; pIdx < pList.options.length; pIdx++) {
		if (pList.options[pIdx].value != OPTGROUP_VALUE
				&& pList.options[pIdx].selected == true) {
			if (theSSMap != undefined
					&& theSSMap[pList.options[pIdx].value] != undefined) {
				items = theSSMap[pList.options[pIdx].value];
				for (index = 0; index < items.length; index++) {
					alreadyAdded = false;
					for (searchIndex = 0; searchIndex < allSelectionsRaw.length; searchIndex++) {
						if (allSelectionsRaw[searchIndex].value == items[index]) {
							alreadyAdded = true;
							break;
						}
					}
					if (!alreadyAdded) {
						for (searchIndex = 0; searchIndex < pList.options.length; searchIndex++) {
							if (pList.options[searchIndex].value == items[index]) {
								allSelectionsRaw[allSelectionsRaw.length] = pList.options[searchIndex];
								break;
							}
						}
					}
				}
			} else {
				allSelectionsRaw.push(pList.options[pIdx]);
			}
		}
	}
	return allSelectionsRaw;
}
function getClassifierType(classifierId) {
	var parts = classifierId.toString().split("|");
	if (parts.length > 1) {
		for (index = 0; index < categoryTypes.length; index++) {
			var split = categoryTypes[index].split("|");
			if (split[0] == parts[1]) {
				return split[1];
			}
		}
		return "";
	} else {
		if (aCategories.get("childlocation")[classifierId] != undefined) {
			return "area";
		}
	}
	return "";
}
function removeOptionByValue(val, allSelectionsList) {
	var index = 0;
	for (index = 0; index < allSelectionsList.length; index++) {
		if (allSelectionsList[index].value == val) {
			allSelectionsList.splice(index, 1);
			return true;
		}
	}
	return false;
}
function SetChildAnyText(cList, pList) {
	var selectedValue, checkThese, hashKey;
	if (cList != null && pList != null) {
		if (pList.id.indexOf("parentlocation") == -1) {
			if (cList.options.length == 1 && cList.options[0].value == 0) {
				cList.options[0].text = "No Selection Required";
			}
		} else {
			if (pList.selectedIndex < 0) {
				return;
			}
			selectedValue = pList.options[pList.selectedIndex].value;
			if (typeof allText !== "undefined" && allText != null) {
				checkThese = Array("nation", "state", "location");
				for (index = 0; index < checkThese.length; index++) {
					var currentCheckKey = checkThese[index];
					if (allText.get(currentCheckKey) != undefined) {
						var groupsForKey = allText.get(currentCheckKey).split(
								",");
						for (searchGroupIndex = 0; searchGroupIndex < groupsForKey.length; searchGroupIndex++) {
							if (allTextGroups[groupsForKey[searchGroupIndex]][selectedValue] != undefined) {
								$(cList.options[0])
										.text(
												allTextGroups[groupsForKey[searchGroupIndex]][selectedValue]);
								return;
							}
						}
					}
				}
				for (index = 0; index < checkThese.length; index++) {
					hashKey = checkThese[index] + "_defaulttext";
					if (allText[hashKey] != undefined) {
						cList.options[0].text = allText[hashKey];
						return;
					}
				}
			}
		}
	}
}
function CreateGroupSeparatorOption(type) {
	var optSeparator = getGroupSeparator(type).split("|");
	var opt = getNewOption(optSeparator[0], optSeparator[1]);
	opt.disabled = "disabled";
	opt.className = OPT_SEARCHBOX_CLASS_NAME;
	return opt;
}
function IsOptGroupItem(itemValue, category) {
	var optGroupItemFound = false, map = aGroupSeparatorMap[category], mapMembers;
	if (map == undefined) {
		return false;
	}
	mapMembers = map.split(",");
	for (index = 0; index < mapMembers.length; index++) {
		memberPieces = mapMembers[index].split("|");
		var groupSeparatorItems = aGroupSeparators[memberPieces[0]];
		if (groupSeparatorItems != undefined) {
			for (var itemIdx = 0; itemIdx < groupSeparatorItems.length; itemIdx++) {
				if (itemValue == groupSeparatorItems[itemIdx]) {
					return mapMembers[index];
				}
			}
		}
	}
	return "";
}
function populateCheckBox(childListId) {
	var chkBox = GetObjectByPartName("chkUnspecified" + childListId,
			controlPrefix == "cat" ? 1 : 2);
	if (chkBox != null) {
		if (chkBox.disabled == false) {
			var currentSels = RetrieveSelectedLocations();
			for (var index = 0; index < currentSels.length; index++) {
				if (currentSels[index] == "2835"
						|| currentSels[index] == "2836") {
					chkBox.checked = true;
					return;
				}
			}
			if (typeof AreaUnspecifiedValue !== "undefined") {
				chkBox.checked = AreaUnspecifiedValue;
				var bottomCheckBox = GetObjectByPartName("chkUnspecified"
						+ childListId, 2);
				if (bottomCheckBox != null) {
					bottomCheckBox.checked = AreaUnspecifiedValue;
				}
			}
		}
	}
	return;
}
function disableCheckBox(childListId) {
	var chkBox = GetObjectByPartName("chkUnspecified" + childListId);
	if (chkBox != null) {
		chkBox.checked = false;
		chkBox.disabled = true;
	}
}
function sortCategory(opt1, opt2) {
	if (opt1.text.toLowerCase() == opt2.text.toLowerCase()) {
		return 0;
	} else {
		if (opt1.text.toLowerCase() > opt2.text.toLowerCase()) {
			return 1;
		} else {
			return -1;
		}
	}
}
function changeSearchOptions(searchType, isExecSearch, isJobSearch) {
	doChangeSearchOptions(searchType, isExecSearch, isJobSearch, true);
}
function changeSearchOptionsWithoutPrepopulation(searchType, isExecSearch,
		isJobSearch) {
	doChangeSearchOptions(searchType, isExecSearch, isJobSearch, false);
}
function doChangeSearchOptions(searchType, isExecSearch, isJobSearch,
		isPopulateValues) {
	var locationList = document
			.getElementById(controlPrefix + "parentlocation");
	var childList = document.getElementById(controlPrefix + "childlocation");
	var selections = CollectLocationSelectionsFromDropDowns();
	selections = addRequiredParents(selections);
	if (selections.length == 0) {
		selections = [ "0" ];
	}
	if (searchType) {
		changeSearchInput(searchType, isPopulateValues, isExecSearch);
		setClassName(searchType, isExecSearch, isJobSearch);
	}
	if (locationList.multiple == false) {
		var categorised = {};
		for (var index = 0; index < selections.length; index++) {
			var type = getClassifierType(selections[index]);
			if (type) {
				var arr = categorised["cat" + type];
				if (arr == undefined) {
					arr = [ selections[index] ];
					categorised["cat" + type] = arr;
				} else {
					arr[arr.length] = selections[index];
				}
			}
		}
		selections = getTwoLowestLevelLocations(categorised);
		if (selections[1] == "0") {
			selections = [ selections[0] ];
		}
	}
	if (isPopulateValues) {
		$(locationList).empty();
		populateClassiferList(controlPrefix + "parentlocation",
				"parentlocation");
		if (!populateFromQueryString) {
			var alreadyDone = {};
			for (var cIdx = locationList.options.length - 1; cIdx >= 0; cIdx--) {
				locationList.options[cIdx].selected = false;
			}
			var parentFound = false;
			for (var index = 0; index < selections.length; index++) {
				var locationValue = selections[index];
				for (var cIdx = locationList.options.length - 1; cIdx >= 0; cIdx--) {
					if (locationList.options[cIdx].value == locationValue) {
						parentFound = true;
						alreadyDone[locationValue] = true;
						if (!locationList.options[cIdx].selected) {
							locationList.options[cIdx].selected = true;
						}
						if (locationList.multiple == false) {
							break;
						}
					}
				}
			}
			if (!parentFound && populateFromQueryString) {
				locationList.value = defaultIndexes.parentlocation;
			}
		}
		ProcessChildList("parentlocation", "childlocation");
		if (!populateFromQueryString) {
			for (var index = 0; index < childList.options.length; index++) {
				childList.options[index].selected = false;
			}
			var childFound = false;
			for (var index = 0; index < selections.length; index++) {
				var childValue = selections[index];
				if (alreadyDone[childValue] != undefined) {
					continue;
				}
				for (var cIdx = childList.options.length - 1; cIdx >= 0; cIdx--) {
					if (childList.options[cIdx].value == childValue) {
						childFound = true;
						if (!childList.options[cIdx].selected) {
							childList.options[cIdx].selected = true;
						}
					}
				}
			}
			if (!childFound) {
				childList.options[0].selected = true;
			}
			UpdateParent(childList, "parentlocation");
		}
		populateSalary(true);
	}
}
function changeSearchToQuick(isExecSearch) {
	var urlString = "";
	if (controlPrefix != "cat") {
		urlString = document.getElementById(controlPrefix
				+ "searchBoxContainer").className;
	} else {
		urlString = document.getElementById("searchBoxContainer").className;
	}
	if (urlString.indexOf("advancedSearch") == -1) {
		setClassName("mini", isExecSearch, true);
	}
	populateSalary(true);
}
function setClassName(searchType, isExecSearch, isJobSearch) {
	if (searchType) {
		var searchClassName = "";
		var displayContent = "block";
		if (searchType == "advanced") {
			searchClassName = "advancedSearch";
			displayContent = "none";
		} else {
			if (searchType == "quick") {
				searchClassName = "quickSearch";
			} else {
				if (searchType == "mini") {
					searchClassName = "miniSearch";
				}
			}
		}
		if (isExecSearch) {
			searchClassName += " execSearch";
		}
		if (isJobSearch) {
			if (controlPrefix != "cat") {
				document.getElementById("searchBoxContainer").style.display = displayContent;
				if (document.getElementById("bottomsearchBoxContainer")) {
					document.getElementById("bottomsearchBoxContainer").className = searchClassName;
				}
			} else {
				if (document.getElementById("bottomsearchBoxContainer")) {
					document.getElementById("bottomsearchBoxContainer").style.display = displayContent;
				}
				document.getElementById("searchBoxContainer").className = searchClassName;
			}
			if (document.getElementById("Content")) {
				document.getElementById("Content").style.display = displayContent;
			}
		} else {
			document.getElementById("searchBoxContainer").className = searchClassName;
		}
	}
}
function changeSearchInput(searchType, isPopulateValues, isExecSearch) {
	if (searchType) {
		var tag = "";
		if (controlPrefix != "cat") {
			tag = controlPrefix;
		}
		var tagElement = null;
		if (searchType == "advanced") {
			tagElement = document.getElementById(tag + "DoSearch1");
			if (tagElement) {
				tagElement.name = "QuickSearch";
			}
			tagElement = document.getElementById(tag + "AdvSearch");
			if (tagElement) {
				tagElement.name = "DoSearch";
			}
			setSearchInputLayout(true, 6);
			setTimeout("scrollToItem()", 50);
		} else {
			if (searchType == "quick" || searchType == "mini") {
				tagElement = document.getElementById(tag + "AdvSearch");
				if (tagElement) {
					tagElement.name = "AdvSearch";
				}
				tagElement = document.getElementById(tag + "DoSearch1");
				if (tagElement) {
					tagElement.name = "DoSearch";
				}
				setSearchInputLayout(false, 1);
				displaySalWarning(false);
			}
		}
	}
}
function setSearchInputLayout(isMultiSelect, multiSelectRowSize) {
	var controls = new Array(document
			.getElementById(controlPrefix + "industry"), document
			.getElementById(controlPrefix + "function"), document
			.getElementById(controlPrefix + "parentlocation"), document
			.getElementById(controlPrefix + "occupation"), document
			.getElementById(controlPrefix + "area"), document
			.getElementById(controlPrefix + "worktype"));
	for (var i = 0; i < controls.length; i++) {
		if (controls[i]) {
			controls[i].size = multiSelectRowSize;
			controls[i].multiple = isMultiSelect;
		}
	}
	var childloc = document.getElementById(controlPrefix + "childlocation");
	if (childloc) {
		childloc.size = (isMultiSelect) ? 5 : multiSelectRowSize;
		childloc.multiple = isMultiSelect;
	}
}
var showedMainAdvanceSel = false;
var showedBottomAdvanceSel = false;
function selectCtrlValue(obj, strValue) {
	if (obj) {
		for (var i = 0; i < obj.length; i++) {
			if (obj.options[i].value == strValue) {
				obj.options[i].selected = true;
				break;
			}
		}
	}
}
function getSearchFrom(searchType, searchBox) {
	var searchString = "";
	if (searchType) {
		searchString += ("&searchfrom=" + searchType);
		if (searchBox) {
			if (searchBox == "upper") {
				searchString += "upper";
			} else {
				if (searchBox == "lower") {
					searchString += "lower";
				}
			}
		}
	}
	return (searchString);
}
function getSearchType() {
	var searchString = "";
	if (!(location.pathname.toLowerCase() == "/"
			|| location.pathname.toLowerCase() == "/uk/"
			|| location.pathname.toLowerCase().indexOf("alliance_homepage") != -1
			|| location.pathname.toLowerCase() == "/index.ascx" || location.pathname
			.toLowerCase() == "/uk/index.ascx")
			&& location.href.toLowerCase().indexOf("?") != -1) {
		searchString += "&searchtype=again";
	}
	return (searchString);
}
function ValidateBeforePost(searchType, searchBox) {
	var listArray = [], numSelectedParentLocation = 0, numSelectedWorkType = 0, numSelectedClassification = 0, numSelectedSubClassification = 0, locationArea, dateRangeList, idx, ddList, k, sItems, opt;
	if (controlPrefix != "cat") {
		dateRangeList = document.getElementById(controlPrefix + "DateRange");
	} else {
		dateRangeList = document.getElementById("DateRange");
	}
	locationArea = Find(controlPrefix + "locationarea");
	if (locationArea && locationArea.value == "-please select-") {
		alert("Please select a location.");
		return false;
	}
	listArray = getListArray(dateRangeList);
	for (idx = 0; idx < listArray.length; idx++) {
		ddList = listArray[idx];
		if (ddList == null) {
			continue;
		}
		if (ddList != null) {
			if ((ddList.name.substr(0, 3) == "cat") && (controlPrefix != "cat")) {
				ddList = document.getElementById(ddList.name.replace("cat",
						controlPrefix));
			}
			for (k = 0; k < ddList.options.length; k++) {
				sItems = 0;
				opt = ddList.options[k];
				if (opt.value == OPTGROUP_VALUE) {
					continue;
				}
				if (opt.selected) {
					if (ddList.name == "parentlocation") {
						numSelectedParentLocation++;
					} else {
						if (ddList.name == "worktype") {
							numSelectedWorkType++;
						} else {
							if (ddList.name == "industry") {
								numSelectedClassification++;
							} else {
								if (ddList.name == "occupation") {
									numSelectedSubClassification++;
								}
							}
						}
					}
				}
			}
		}
	}
	if (numSelectedParentLocation > 4) {
		alert("Too many locations have been selected: please limit selections to a maximum of 4.");
		return false;
	}
	if (numSelectedWorkType > 4) {
		alert("Too many work types have been selected: please limit selections to a maximum of 4.");
		return false;
	}
	if (numSelectedClassification > 4) {
		alert("Too many classifications have been selected: please limit selections to a maximum of 4.");
		return false;
	}
	if (numSelectedSubClassification > 10) {
		alert("Too many sub-classifications have been selected: please limit selections to a maximum of 10.");
		return false;
	}
	return true;
}
function ConvertCategoryName(queryPara, optionValue) {
	if (categoryTypes != null) {
		var categoryNameFound = false;
		for (var idx = 0; idx < categoryTypes.length; idx++) {
			var category = categoryTypes[idx].split("|");
			if (!categoryNameFound
					&& ("cat" + category[1].toLowerCase()) == queryPara
							.toLowerCase()) {
				categoryNameFound = true;
			}
		}
		if (!categoryNameFound) {
			var categoryIDFound = false;
			for (var idx = 0; idx < categoryTypes.length; idx++) {
				var category = categoryTypes[idx].split("|");
				if (optionValue.indexOf("|") != -1) {
					var optionCategory = optionValue.split("|")[1];
					if (!categoryIDFound && optionCategory == category[0]) {
						queryPara = "cat" + category[1];
						categoryIDFound = true;
					}
				}
			}
		}
	}
	return queryPara;
}
function ConvertCategoryValue(optionValue) {
	var categoryIDFound = false;
	if (optionValue.indexOf("|") != -1 && categoryTypes != null) {
		for (var idx = 0; idx < categoryTypes.length; idx++) {
			var category = categoryTypes[idx].split("|");
			var optionCategory = optionValue.split("|")[1];
			if (!categoryIDFound && category[0] == optionCategory) {
				optionValue = optionValue.split("|")[0];
				categoryIDFound = true;
			}
		}
	}
	return optionValue;
}
function doUriEncode(uri) {
	var hasEncodeURIComponent = (typeof encodeURIComponent !== "undefined" && encodeURIComponent != null);
	var bBrowser = navigator.appName;
	var bVersion = navigator.appVersion;
	var encoded = uri;
	if (!hasEncodeURIComponent) {
		encoded = escape(uri);
	} else {
		encoded = encodeURIComponent(uri);
	}
	while (encoded.search("'") != -1) {
		encoded = encoded.replace("'", "%27");
	}
	while (encoded.search("[+]") != -1) {
		encoded = encoded.replace("+", "%2B");
	}
	return encoded;
}
function SynchDuplicate(sourcePrefix, targetPrefix, isExec) {
	if (document.getElementById(sourcePrefix + "parentlocation")
			&& document.getElementById(targetPrefix + "parentlocation")) {
		document.getElementById(targetPrefix + "parentlocation").selectedIndex = document
				.getElementById(sourcePrefix + "parentlocation").selectedIndex;
		controlPrefix = targetPrefix;
		if (aCategories.get("childlocation") != null) {
			ProcessChildList("parentlocation", "childlocation");
		}
		if (!isExec) {
			document.getElementById(targetPrefix + "industry").selectedIndex = document
					.getElementById(sourcePrefix + "industry").selectedIndex;
			ProcessChildList("industry", "occupation");
			document.getElementById(targetPrefix + "occupation").selectedIndex = document
					.getElementById(sourcePrefix + "occupation").selectedIndex;
		} else {
			document.getElementById(targetPrefix + "function").selectedIndex = document
					.getElementById(sourcePrefix + "function").selectedIndex;
		}
		document.getElementById(targetPrefix + "childlocation").selectedIndex = document
				.getElementById(sourcePrefix + "childlocation").selectedIndex;
		document.getElementById(targetPrefix + "worktype").selectedIndex = document
				.getElementById(sourcePrefix + "worktype").selectedIndex;
		if (sourcePrefix != "cat" && targetPrefix == "cat") {
			document.forms.layout.elements.Keywords.value = document
					.getElementById(sourcePrefix + "Keywords").value;
			document.getElementById("DateRange").selectedIndex = document
					.getElementById(sourcePrefix + "DateRange").selectedIndex;
			if (document.getElementById("salaryfrom")) {
				populateSalary(true);
				document.getElementById("salaryfrom").selectedIndex = document
						.getElementById(sourcePrefix + "salaryfrom").selectedIndex;
				document.getElementById("salaryto").selectedIndex = document
						.getElementById(sourcePrefix + "salaryto").selectedIndex;
			}
		} else {
			if (sourcePrefix == "cat" && targetPrefix != "cat") {
				$("#" + targetPrefix + "Keywords").val($("#Keywords").val());
				document.getElementById(targetPrefix + "DateRange").selectedIndex = document
						.getElementById("DateRange").selectedIndex;
				if (document.getElementById(targetPrefix + "salaryfrom")) {
					populateSalary(true);
					document.getElementById(targetPrefix + "salaryfrom").selectedIndex = document
							.getElementById("salaryfrom").selectedIndex;
					document.getElementById(targetPrefix + "salaryto").selectedIndex = document
							.getElementById("salaryto").selectedIndex;
				}
			} else {
				document.getElementById(targetPrefix + "Keywords").value = document
						.getElementById(sourcePrefix + "Keywords").value;
				document.getElementById(targetPrefix + "DateRange").selectedIndex = document
						.getElementById(sourcePrefix + "DateRange").selectedIndex;
				if (document.getElementById(targetPrefix + "salaryfrom")) {
					populateSalary(true);
					document.getElementById(targetPrefix + "salaryfrom").selectedIndex = document
							.getElementById(sourcePrefix + "salaryfrom").selectedIndex;
					document.getElementById(targetPrefix + "salaryto").selectedIndex = document
							.getElementById(sourcePrefix + "salaryto").selectedIndex;
				}
			}
		}
	}
}
function GetLocationID(classifierID) {
	var classifiers = CollectLocationSelectionsFromDropDowns();
	return classifiers.length == 0 ? "0" : classifiers[0];
}
function disabledSalary(isDisabled) {
	if (isDisabled
			&& document.getElementById("searchBoxContainer").className
					.indexOf("advancedSearch") == -1) {
		document.getElementById("salaryto").style.display = "none";
		document.getElementById("salarytolabel").style.display = "none";
		document.getElementById("salaryfrom").style.display = "none";
		document.getElementById("salaryDisabled").style.display = "block";
		document.getElementById("salaryDisabled").selectedIndex = 0;
	} else {
		if (document.getElementById("searchBoxContainer").className
				.indexOf("startStyle") == -1) {
			document.getElementById("salaryto").style.display = "block";
			document.getElementById("salarytolabel").style.display = "block";
			document.getElementById("salaryfrom").style.display = "block";
		}
		if (document.getElementById("salaryDisabled") != null) {
			document.getElementById("salaryDisabled").style.display = "none";
		}
	}
}
function bottomdisabledSalary(isDisabled) {
	if (isDisabled
			&& document.getElementById("bottomsearchBoxContainer").className
					.indexOf("advancedSearch") == -1) {
		document.getElementById("bottomsalaryto").style.display = "none";
		document.getElementById("bottomsalarytolabel").style.display = "none";
		document.getElementById("bottomsalaryfrom").style.display = "none";
		document.getElementById("bottomsalaryDisabled").style.display = "block";
		document.getElementById("bottomsalaryDisabled").selectedIndex = 0;
	} else {
		document.getElementById("bottomsalaryto").style.display = "block";
		document.getElementById("bottomsalarytolabel").style.display = "block";
		document.getElementById("bottomsalaryfrom").style.display = "block";
		if (document.getElementById("bottomsalaryDisabled") != null) {
			document.getElementById("bottomsalaryDisabled").style.display = "none";
		}
	}
}
function populateSalary(forceRefresh, ctrlPostfix) {
	var selections = null;
	selections = GetLocationsSelectedForSalary();
	var multiCountry;
	var ctrlPrefix = "";
	if (forceRefresh == undefined) {
		forceRefresh = false;
	}
	if (controlPrefix != "cat") {
		ctrlPrefix = controlPrefix;
	}
	var salaryfrom = document.getElementById(ctrlPrefix + "salary"
			+ (ctrlPostfix || "") + "from");
	var salaryto = document.getElementById(ctrlPrefix + "salary"
			+ (ctrlPostfix || "") + "to");
	if ((salaryfrom) && (salaryto)) {
		multiCountry = checkIsMultiLocation(selections);
		if (multiCountry === "") {
			if (forceRefresh
					|| _saved_salaryDropDownCountry[controlPrefix] != multiCountry) {
				displaySalWarning(true);
			}
			_saved_salaryDropDownCountry[controlPrefix] = multiCountry;
		} else {
			var lastOptionValueInFrom = "";
			if (salaryfrom.options.length > 0) {
				lastOptionValueInFrom = salaryfrom.options[salaryfrom.options.length - 1].value;
			}
			var populateType = "hourly";
			if (populateFromQueryString
					&& aCatSelected.catsalarytype != undefined) {
				if (aCatSelected.catsalarytype.toLowerCase() == "salaryhourly") {
					populateType = "hourly";
				} else {
					if (aCatSelected.catsalarytype.toLowerCase() == "salary") {
						populateType = "annual";
					}
				}
			} else {
				if (lastOptionValueInFrom.indexOf(":") > 0) {
					if (lastOptionValueInFrom.split(":")[1] == "hourly") {
						populateType = "annual";
					}
				}
			}
			displaySalWarning(false);
			if (forceRefresh
					|| _saved_salaryDropDownCountry[controlPrefix] != multiCountry) {
				if (forceRefresh) {
					populateSalaryDropDown((controlPrefix == "cat" ? ""
							: controlPrefix), "from", multiCountry, null,
							populateType, ctrlPostfix);
					populateSalaryDropDown((controlPrefix == "cat" ? ""
							: controlPrefix), "to", multiCountry, null,
							populateType, ctrlPostfix);
				} else {
					populateSalaryDropDown((controlPrefix == "cat" ? ""
							: controlPrefix), "from", multiCountry, -1,
							populateType, ctrlPostfix);
					populateSalaryDropDown((controlPrefix == "cat" ? ""
							: controlPrefix), "to", multiCountry, -1,
							populateType, ctrlPostfix);
				}
			}
			_saved_salaryDropDownCountry[controlPrefix] = multiCountry;
		}
	}
}
function GetLocationsSelectedForSalary() {
	var pList = document.getElementById(controlPrefix + "parentlocation");
	var cList = document.getElementById(controlPrefix + "childlocation");
	var ret = [];
	var allSelections = getSelectedOptions(pList);
	for (var index = 0; index < allSelections.length; index++) {
		if (allSelections[index].value != "0") {
			ret[ret.length] = allSelections[index].value;
		}
	}
	var childLocationSelectedIds = getSelectedIDs(cList);
	return ret.concat(childLocationSelectedIds);
}
function displaySalWarning(isWarning) {
	var ctrlPrefix = "";
	if (controlPrefix != "cat") {
		ctrlPrefix = controlPrefix;
	}
	var salaryfrom = document.getElementById(ctrlPrefix + "salaryfrom");
	var salaryto = document.getElementById(ctrlPrefix + "salaryto");
	if ((salaryfrom) && (salaryto)) {
		if (isWarning) {
			if (salaryfrom.options.length == 0 && salaryto.options.length == 0) {
				var allParentLocs = aCategories.get("parentlocation");
				var tempSels = [ "0" ];
				for ( var plkey in allParentLocs) {
					if (allParentLocs[plkey] != "0"
							&& allParentLocs[plkey] != OPTGROUP_VALUE) {
						tempSels = [ allParentLocs[plkey] ];
						break;
					}
				}
				var defCountry = checkIsMultiLocation(tempSels);
				populateSalaryDropDown(ctrlPrefix, "from", defCountry, -1,
						"annual");
				populateSalaryDropDown(ctrlPrefix, "to", defCountry, -1,
						"annual");
			}
			if (salaryto.options.length > 1) {
				salaryto.selectedIndex = salaryto.length - 2;
			} else {
				salaryto.selectedindex = -1;
			}
			salaryfrom.selectedIndex = 0;
			salaryfrom.disabled = true;
			salaryto.disabled = true;
			changeSalaryText("Salary");
			if (document.getElementById("bodyContainer").className
					.indexOf("jobmail-profile-pages") == -1) {
				if (controlPrefix == "cat") {
					disabledSearchSalary(true);
				}
			}
			if (document.getElementById("bodyContainer").className
					.indexOf("SearchResults") != -1
					|| document.getElementById("bodyContainer").className
							.indexOf("JobAppProcess")) {
				if (controlPrefix == "bottom") {
					bottomdisabledSearchSalary(true);
				}
			}
		} else {
			salaryfrom.disabled = false;
			salaryto.disabled = false;
			changeSalaryText("");
			if (document.getElementById("bodyContainer").className
					.indexOf("jobmail-profile-pages") == -1) {
				if (controlPrefix == "cat") {
					disabledSearchSalary(false);
				}
			}
			if (document.getElementById("bodyContainer").className
					.indexOf("SearchResults") != -1
					|| document.getElementById("bodyContainer").className
							.indexOf("JobAppProcess")) {
				if (controlPrefix == "bottom") {
					bottomdisabledSearchSalary(false);
				}
			}
		}
	}
}
function changeSalaryText(helpText) {
	var browserType = navigator.appName;
	var ctrlPrefix = "";
	if (controlPrefix != "cat") {
		ctrlPrefix = controlPrefix;
	}
	var salarylabel = document
			.getElementById(ctrlPrefix + "salaryMessageLabel");
	if (salarylabel) {
		if (browserType.indexOf("Internet Explorer") > 0) {
			salarylabel.innerText = helpText;
		} else {
			salarylabel.textContent = helpText;
		}
	}
}
function SetInitialSalarySelections(postfix) {
	var selections = GetLocationsSelectedForSalary();
	multiCountry = checkIsMultiLocation(selections);
	populateSalary(true, postfix);
	if (multiCountry != "") {
		prePopulateSalary(multiCountry, undefined, postfix);
	}
	if (typeof ($) != "undefined" && typeof ($.fn) != "undefined"
			&& typeof ($.fn.seekRefineSlider) != "undefined") {
		if (multiCountry != "") {
			var startupType = "Salary";
			if (aCatSelected.catsalarytype != undefined) {
				startupType = aCatSelected.catsalarytype;
			}
			$.fn.seekRefineSlider(multiCountry, startupType,
					aCatSelected.catsalaryfrom, aCatSelected.catsalaryto);
			var incUnspec = document
					.getElementById("salaryIncludeUnspecifiedCheck");
			if (incUnspec != null) {
				if (aCatSelected.catsalaryunspecified != undefined) {
					incUnspec.checked = aCatSelected.catsalaryunspecified;
				} else {
					incUnspec.checked = false;
				}
			}
		}
	}
}
function SetInitialDateRangeSelection() {
	var dr = (aCatSelected.DateRange != undefined) ? aCatSelected.DateRange
			: "31";
	$("#DateRange").val(dr);
}
function checkIsMultiLocation(selections) {
	var topLevelParents = [];
	var multiCountry = false;
	if (selections.length == 0) {
		multiCountry = true;
	}
	for (var index = 0; index < selections.length; index++) {
		var currentParent = selections[index];
		if (currentParent == "0") {
			multiCountry = true;
			break;
		}
		while (true) {
			var nextParent = findParentID(currentParent);
			if (nextParent != "") {
				currentParent = nextParent;
			} else {
				break;
			}
		}
		if (currentParent != null) {
			if (currentParent == "3110|13") {
				currentParent = "3000|12";
			}
			for (var scanIndex = 0; scanIndex < topLevelParents.length; scanIndex++) {
				if (topLevelParents[scanIndex] != currentParent) {
					multiCountry = true;
					break;
				}
			}
			topLevelParents[topLevelParents.length] = currentParent;
		}
	}
	if (!multiCountry) {
		var countryCode = "AU";
		switch (topLevelParents[0]) {
		case "3000|12":
			countryCode = "AU";
			break;
		case "3109|13":
		case "3002|12":
			countryCode = "UK";
			break;
		case "3110|13":
			countryCode = "AU";
			break;
		case "3108|13":
			countryCode = "NZ";
			break;
		}
		return countryCode;
	} else {
		return "";
	}
}
function populateSalaryDropDown(prefix, fromOrTo, country, setValue,
		salaryType, postfix) {
	if (salaryType == null) {
		salaryType = "annual";
	}
	var ddList = document.getElementById(prefix + "salary" + (postfix || "")
			+ fromOrTo);
	var catid = country + "Salary" + (salaryType == "annual" ? "" : "Hourly")
			+ fromOrTo;
	var hiddenSalaryType = document.getElementById(prefix + "salarytype");
	var startFrom = -1;
	if (fromOrTo == "to") {
		var fromList = document.getElementById(prefix + "salaryfrom");
		if (fromList) {
			startFrom = fromList.value;
		}
	}
	if (ddList) {
		var originalValue = ddList.value;
		if (setValue != null) {
			originalValue = setValue;
		}
		var goDefault = true;
		for (var index = ddList.options.length - 1; index >= 0; index--) {
			ddList.remove(index);
		}
		var allItems = aCategories.get(catid);
		for ( var i in allItems) {
			if (allItems[i] != null) {
				if (parseFloat(allItems[i][1]) > startFrom) {
					var opt = new Option(allItems[i][0], allItems[i][1]);
					ddList.options[ddList.options.length] = opt;
					if (originalValue) {
						if (originalValue == opt.value) {
							opt.selected = true;
							goDefault = false;
						}
					}
				}
			}
		}
		var switcherOption = null;
		if (salaryType == "hourly") {
			switcherOption = new Option("Annual rates", "ch:annual");
		} else {
			switcherOption = new Option("Hourly rates", "ch:hourly");
		}
		ddList.options[ddList.options.length] = switcherOption;
		if (goDefault && ddList.options.length > 0) {
			if (fromOrTo == "from") {
				ddList.options[0].selected = true;
			} else {
				ddList.options[ddList.options.length - 2].selected = true;
			}
		}
		if (hiddenSalaryType) {
			hiddenSalaryType.value = salaryType;
		}
	}
}
function updateSalary(obj, postfix) {
	if (!obj) {
		return;
	}
	populateFromQueryString = false;
	var ctrlPrefix = "";
	if (obj.id.indexOf("bottom") != -1) {
		ctrlPrefix = "bottom";
	}
	if (obj.value.match(/ch:/)) {
		var targetType = obj.value.split(":")[1];
		var salaryCountry = _saved_salaryDropDownCountry[controlPrefix];
		populateSalaryDropDown(ctrlPrefix, "from", salaryCountry,
				parsedQS.SalaryFromQS, targetType, postfix);
		populateSalaryDropDown(ctrlPrefix, "to", salaryCountry,
				parsedQS.SalaryToQS, targetType, postfix);
		return;
	}
	if (obj.id.indexOf("salary" + (postfix || "") + "to") >= 0) {
		return;
	}
	var lastOptionValueInFrom = obj.options[obj.options.length - 1].value;
	var appendType = "hourly";
	if (lastOptionValueInFrom.indexOf(":") > 0) {
		appendType = lastOptionValueInFrom.split(":")[1];
	}
	var lowSalary = parseInt(obj.value);
	var salary = "";
	var newHighSalary = "";
	var highSalary = 0;
	var salaryto = document.getElementById(ctrlPrefix + "salary"
			+ (postfix || "") + "to");
	var catid = _saved_salaryDropDownCountry[controlPrefix] + "Salary"
			+ (appendType == "hourly" ? "" : "Hourly") + "to";
	if (salaryto) {
		highSalary = salaryto.value;
		salaryto.length = 0;
		var allItems = aCategories.get(catid);
		for ( var i in allItems) {
			if (allItems[i] != null) {
				if (parseFloat(allItems[i][1]) > lowSalary) {
					var opt = new Option(allItems[i][0], allItems[i][1]);
					salaryto.options[salaryto.options.length] = opt;
				}
			}
		}
		var switcherOption = null;
		if (appendType == "hourly") {
			switcherOption = new Option("Hourly rates", "ch:hourly");
		} else {
			switcherOption = new Option("Annual rates", "ch:annual");
		}
		salaryto.options[salaryto.options.length] = switcherOption;
		for (var i = 0; i < salaryto.length; i++) {
			if (parseFloat(salaryto[i].value) == highSalary) {
				salaryto.selectedIndex = i;
				break;
			}
		}
	}
}
function populateSalaryDefault(country) {
	var ctrlPrefix = "";
	if (controlPrefix != "cat") {
		ctrlPrefix = controlPrefix;
	}
	var salaryto = document.getElementById(ctrlPrefix + "salaryto");
	var salaryfrom = document.getElementById(ctrlPrefix + "salaryfrom");
	if (salaryto) {
		salaryto.selectedIndex = salaryto.length - 1;
	}
	if (salaryfrom) {
		salaryfrom.selectedIndex = 0;
	}
	prePopulateSalary(country, controlPrefix);
}
function retrieveSalaryFromQueryString() {
	var qs = location.search.substring(1, location.search.length);
	if (qsJobResults != "") {
		if (qsJobResults.indexOf("?") != -1) {
			qs = qsJobResults.split("?")[1];
		} else {
			qs = qsJobResults;
		}
	}
	var args = qs.split("&");
	for (var i = 0; i < args.length; i++) {
		var qsvalue, qsctrl;
		var pair = args[i].split("=");
		qsctrl = unescape(pair[0]);
		qsvalue = unescape(pair[1]);
		if ((qsctrl.toLowerCase() == "salary") && (qsvalue != "")) {
			var splitArray = qsvalue.split("-");
			if (splitArray[0] == "") {
				splitArray[0] = 0;
			}
			if (splitArray[1] == "") {
				splitArray[1] = 999999;
			}
			return Array(splitArray[0], splitArray[1]);
		}
	}
	return null;
}
function prePopulateSalary(country, prefix, postfix) {
	var salarySels = null;
	var populateType = "annual";
	if (populateFromQueryString) {
		if (aCatSelected.catsalaryfrom != undefined
				&& aCatSelected.catsalaryto != undefined) {
			salarySels = [ aCatSelected.catsalaryfrom, aCatSelected.catsalaryto ];
		} else {
			salarySels = retrieveSalaryFromQueryString();
		}
		if (salarySels == null) {
			salarySels = [ -1, -1 ];
		}
		if (aCatSelected.catsalarytype != undefined) {
			if (aCatSelected.catsalarytype.toLowerCase() == "salaryhourly") {
				populateType = "hourly";
			} else {
				if (aCatSelected.catsalarytype.toLowerCase() == "salary") {
					populateType = "annual";
				}
			}
		}
	} else {
		salarySels = [ -1, -1 ];
	}
	if (prefix != undefined) {
		if (prefix == "cat") {
			_saved_salaryDropDownCountry.cat = country;
			populateSalaryDropDown("", "from", country, salarySels[0],
					populateType, postfix);
			populateSalaryDropDown("", "to", country, salarySels[1],
					populateType, postfix);
		} else {
			_saved_salaryDropDownCountry[prefix] = country;
			populateSalaryDropDown(prefix, "from", country, salarySels[0],
					populateType, postfix);
			populateSalaryDropDown(prefix, "to", country, salarySels[1],
					populateType, postfix);
		}
	} else {
		_saved_salaryDropDownCountry.cat = country;
		_saved_salaryDropDownCountry.bottom = country;
		populateSalaryDropDown("", "from", country, salarySels[0],
				populateType, postfix);
		populateSalaryDropDown("", "to", country, salarySels[1], populateType,
				postfix);
		populateSalaryDropDown("bottom", "from", country, salarySels[0],
				populateType, postfix);
		populateSalaryDropDown("bottom", "to", country, salarySels[1],
				populateType, postfix);
	}
}
function worktypeChanged(worktype) {
	var selection = worktype.value;
	switch (selection) {
	case "0":
	case "242":
		switchSalaryTypeAndResync("annual");
		break;
	case "243":
	case "244":
	case "245":
		switchSalaryTypeAndResync("hourly");
		break;
	}
}
function switchSalaryTypeAndResync(salarytype) {
	updateSalaryType(salarytype);
	var ctrlPrefix = "";
	if (controlPrefix != "cat") {
		ctrlPrefix = controlPrefix;
	}
	var salaryfrom = document.getElementById(ctrlPrefix + "salaryfrom");
	var salaryto = document.getElementById(ctrlPrefix + "salaryto");
	if ((salaryfrom) && (salaryto)) {
		var fromVal = salaryfrom.value;
		var toVal = salaryto.value;
		var lastOptionValueInFrom = salaryfrom.options[salaryfrom.options.length - 1].value;
		var currentType = "hourly";
		if (lastOptionValueInFrom.indexOf(":") > 0) {
			currentType = lastOptionValueInFrom.split(":")[1];
		}
		if (currentType == "annual") {
			currentType = "hourly";
			fromVal = fromVal * 2000;
			toVal = toVal * 2000;
		} else {
			currentType = "annual";
			fromVal = fromVal / 2000;
			toVal = toVal / 2000;
		}
		if (currentType == salarytype) {
			return;
		}
		salaryfrom.value = "ch:" + salarytype;
		updateSalary(salaryfrom);
		for (var index = 0; index < salaryfrom.options.length; index++) {
			if (salaryfrom.options[index].value == fromVal) {
				salaryfrom.options[index].selected = true;
				break;
			}
		}
		for (var index = 0; index < salaryto.options.length; index++) {
			if (salaryto.options[index].value == toVal) {
				salaryto.options[index].selected = true;
				break;
			}
		}
	}
}
window.jobsearchCommonLoaded = true;

var CriteriasValue = GetCookie(marketSegment);
if (CriteriasValue == null || CriteriasValue == ";") {
	CriteriasValue = "";
}
if (document.getElementById("LastLoggedInSearch") != undefined) {
	CriteriasValue = document.getElementById("LastLoggedInSearch").value;
}
var URL = host;
var oldClassifierDetected = false;
var Keyword = "-";
var Location = "-";
var Area = "-";
var Classification = "-";
var SubClassification = "-";
var ExecFunction = "-";
var IsCampus = false;
var Salary = "-";
var SalaryType = "salary";
var Nation = "-";
var State = "-";
$(document).ready(function() {
	$("#SaveLink").click(function() {
		$("#SaveLastSearchForm").submit();
	});
	$("#ClearCookieLink").click(function() {
		ClearCookie();
	});
});
function checkIfOldClassifier(value) {
	if (value == "-") {
		oldClassifierDetected = true;
	}
}
var arrayCategory = new Array;
arrayCategory = CriteriasValue.split("~");
for (mi = 0; mi < arrayCategory.length; mi++) {
	var tempArray = new Array;
	tempArray = arrayCategory[mi].split("|");
	if (tempArray[0] == "V") {
		if (tempArray[1] != version) {
			CriteriasValue = null;
		}
	} else {
		if (tempArray[0] == "K") {
			var orgKeyword = unescape(tempArray[1]).replace(/\+/g, " ");
			URL += "&Keywords=" + orgKeyword;
			Keyword = Truncate(orgKeyword);
		} else {
			if (tempArray[0] == "P") {
				URL += tempArray[1] + "?";
			} else {
				if (tempArray[0] == "D") {
					URL += "DateRange=" + tempArray[1];
				} else {
					if (tempArray[0] == "L") {
						Location = Truncate(IDMappingLocation(tempArray[1]));
						checkIfOldClassifier(Location);
					} else {
						if (tempArray[0] == "A") {
							Area = Truncate(IDMappingLocation(tempArray[1]));
							checkIfOldClassifier(Area);
						} else {
							if (tempArray[0] == "NA") {
								Nation = Truncate(IDMappingLocation(tempArray[1]));
								checkIfOldClassifier(Nation);
							} else {
								if (tempArray[0] == "SA") {
									State = Truncate(IDMappingLocation(tempArray[1]));
									checkIfOldClassifier(State);
								} else {
									if (tempArray[0] == "I") {
										Classification = Truncate(IDMapping(
												"industry", tempArray[1]));
										checkIfOldClassifier(Classification);
									} else {
										if (tempArray[0] == "O") {
											SubClassification = Truncate(IDMapping(
													"occupation", tempArray[1]));
											checkIfOldClassifier(SubClassification);
										} else {
											if (tempArray[0] == "F") {
												ExecFunction = Truncate(IDMapping(
														"function",
														tempArray[1]));
											} else {
												if (tempArray[0] == "S") {
													IDMapping("specialisation",
															tempArray[1]);
												} else {
													if (tempArray[0] == "W") {
														IDMapping("worktype",
																tempArray[1]);
													} else {
														if (tempArray[0] == "C") {
															IsCampus = true;
														} else {
															if (tempArray[0] == "SF") {
																Salary += tempArray[1]
																		+ "-";
															} else {
																if (tempArray[0] == "ST") {
																	Salary += "-"
																			+ tempArray[1];
																} else {
																	if (tempArray[0] == "SFT") {
																		SalaryType = tempArray[1]
																				.toLowerCase();
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
}
if (Salary != "" && Salary != "-") {
	URL += "&" + SalaryType + "=" + Salary.replace(/--/, "-");
}
if (IsCampus) {
	SubClassification = " ";
}
function SubmitMyLastSearchUrl() {
	window.document.location = URL + "&MLS=true";
}
function SaveSearchURL() {
	window.document.location = URL + "&MLS=true&savesearch=true";
}
function DisplayLastSearch() {
	if (CriteriasValue == null || CriteriasValue.length == 0
			|| oldClassifierDetected) {
		if ($("#MySavedSearches, #MySavedJobs").length == 0) {
			$("#MLSCriterias, .saved-base").hide();
		}
	} else {
		$("#MLSCriterias, .saved-base").show();
		if (IsCampus) {
			if (document.getElementById("SubClass") != null) {
				document.getElementById("SubClass").style.display = "none";
			}
			if (document.getElementById("Area") != null) {
				document.getElementById("Area").style.display = "none";
			}
			$("#Campus").html("Graduate/Entry Level");
		} else {
			if (document.getElementById("Campus") != null) {
				document.getElementById("Campus").style.display = "none";
			}
		}
	}
}
function IDMappingLocation(valuesArray) {
	var mappingArray;
	var mapping = "";
	var mj = 0;
	valuesArray = valuesArray.toString().replace(/#/g, "|");
	var typesToScan = [ "parentlocation", "childlocation" ];
	var arrayLoc = new Array;
	arrayLoc = valuesArray.split(":");
	for (mk = 0; mk < arrayLoc.length; mk++) {
		var found = false;
		for ( var typeKey in typesToScan) {
			mappingArray = aCategories.get(typesToScan[typeKey]);
			if (typeof (mappingArray) != "undefined") {
				for ( var mj in mappingArray) {
					var locationItem = mappingArray[mj][1];
					if (locationItem == arrayLoc[mk]) {
						mapping += mappingArray[mj][0] + ", ";
						var theCategory = getClassifierType(arrayLoc[mk]);
						URL += "&cat" + theCategory + "="
								+ arrayLoc[mk].toString().split("|")[0];
						found = true;
						break;
					}
				}
			}
			if (found) {
				break;
			}
		}
	}
	if (mapping.length == 0) {
		return "-";
	} else {
		return mapping.substring(0, mapping.length - 2);
	}
}
function DisplayInstruction() {
	if ((CriteriasValue == null) || (CriteriasValue.length == 0)) {
		$("#MLSInstruction").show();
	} else {
		$("#MLSInstruction").hide();
	}
}
function Truncate(value) {
	if (value.length < 20) {
		return value;
	}
	return value.substring(0, 17) + "...";
}
function IDMapping(type, valuesArray) {
	var mappingArray = aCategories.get(type);
	var mapping = "";
	var mj = 0;
	if (typeof (mappingArray) != "undefined") {
		var arrayLoc = new Array;
		arrayLoc = valuesArray.split(":");
		for (mk = 0; mk < arrayLoc.length; mk++) {
			for (mappingItem in mappingArray) {
				if (mappingArray[mappingItem][1] != null
						&& mappingArray[mappingItem][1] == arrayLoc[mk]) {
					mapping += mappingArray[mappingItem][0] + ", ";
				}
				mj++;
			}
			URL += "&cat" + type + "=" + arrayLoc[mk];
		}
	}
	if (mapping.length == 0) {
		return "-";
	} else {
		return mapping.substring(0, mapping.length - 2);
	}
}
function ClearCookie() {
	SEEK.Cookie.clearCookie(marketSegment);
	SEEK.Cookie.clearCookie(marketSegment, host);
	if ($("#MySavedSearches, #MySavedJobs").length == 0) {
		$("#MLSCriterias, .saved-base").hide();
	} else {
		$("#MyLastSearch").hide();
	}
}
function CustomWhichAreaSetValue(postbackControlName, value) {
	var parentLocationControl = $("#" + controlPrefix + "parentlocation")[0];
	var valueArray = value.split(";");
	var childArray = [];
	if ((parentLocationControl.type == "select-one")
			|| (parentLocationControl.type == "select-multiple")) {
		for (j = 0; j < parentLocationControl.options.length; j++) {
			parentLocationControl.options[j].selected = false;
		}
		for (i = 0; i < valueArray.length; i++) {
			var thisLocationAreaPair = retrieveTwoLowestLevelLocations(
					parentLocationControl, valueArray[i]);
			for (j = 0; j < parentLocationControl.options.length; j++) {
				if (thisLocationAreaPair[0] == parentLocationControl.options[j].value) {
					parentLocationControl.options[j].selected = true;
					childArray[childArray.length] = thisLocationAreaPair[1];
				}
			}
		}
	} else {
		oPairArray = valueArray[0].split(",");
		LocationControl.value = oPairArray[0];
	}
	ValidateCategoryList("parentlocation", 0, "childlocation", true);
	WhichAreaSetValue(postbackControlName, childArray);
	ValidateCategoryList("childlocation", 0, "", false);
	SetInitialSalarySelections();
}
function retrieveTwoLowestLevelLocations(parentLocationControl, valueArray) {
	var thisLocationAreaPair = valueArray.split(",");
	var locationFound = false;
	if (parentLocationControl.options.length > 0) {
		while (!locationFound) {
			for (j = 0; j < parentLocationControl.options.length; j++) {
				if (thisLocationAreaPair[0] == parentLocationControl.options[j].value) {
					locationFound = true;
				}
			}
			if (!locationFound) {
				var theParent = findParentID(thisLocationAreaPair[0]);
				if (theParent != "") {
					thisLocationAreaPair[1] = thisLocationAreaPair[0];
					thisLocationAreaPair[0] = theParent;
				}
			}
		}
	}
	return thisLocationAreaPair;
}
function WhichAreaCallPopup(popupPage, postbackControlName,
		postbackFunctionName, popupWidth, popupHeight, selectionMode,
		popupOptions) {
	var locationOptions = $("#catparentlocation option");
	var areaOptions = $("#catchildlocation option");
	if (locationOptions.length == 0 && areaOptions.length == 0) {
		alert("Please refresh this page to use the SEEK Area Finder.");
	} else {
		var options = "status=1,width=" + popupWidth + ",height=" + popupHeight
				+ "," + popupOptions;
		var win = window.open(popupPage + "?PostbackControlName="
				+ postbackControlName + "&PostbackFunctionName="
				+ postbackFunctionName + "&SelectionMode=" + selectionMode,
				"whichAreaPopup", options);
		win.focus();
	}
}
function WhichAreaCallPopupWithLocation(popupPage, postbackControlName,
		postbackFunctionName, popupWidth, popupHeight, selectionMode,
		popupOptions, fromLoc) {
	var options = "status=1,width=" + popupWidth + ",height=" + popupHeight
			+ "," + popupOptions;
	var win = window.open(popupPage + "?PostbackControlName="
			+ postbackControlName + "&PostbackFunctionName="
			+ postbackFunctionName + "&SelectionMode=" + selectionMode
			+ "&fromLoc" + fromLoc, "whichAreaPopup", options);
	win.focus();
}
function WhichAreaSetValue(postbackControlName, valueList) {
	var postbackControl = document.getElementById(controlPrefix
			+ postbackControlName);
	if ((postbackControl.type == "select-one")
			|| (postbackControl.type == "select-multiple")) {
		for (j = 0; j < postbackControl.options.length; j++) {
			postbackControl.options[j].selected = false;
		}
		for (i = 0; i < valueList.length; i++) {
			for (j = 0; j < postbackControl.options.length; j++) {
				if (valueList[i] == postbackControl.options[j].value) {
					postbackControl.options[j].selected = true;
				}
			}
		}
	} else {
		oPairArray = valueArray[0].split(",");
		postbackControl.value = oPairArray[0];
	}
}
function WhichAreaSetValueCAJA(postbackControlName, value) {
	var valueArray = value.split(";");
	var firstValue = valueArray[0].split(",");
	var childArray = [];
	var parentLocationControl = document.getElementById("catparentlocation");
	var thisLocationAreaPair = retrieveTwoLowestLevelLocations(
			parentLocationControl, valueArray[0]);
	for (j = 0; j < parentLocationControl.options.length; j++) {
		if (thisLocationAreaPair[0] == parentLocationControl.options[j].value) {
			parentLocationControl.options[j].selected = true;
			childArray[childArray.length] = thisLocationAreaPair[1];
		}
	}
	ValidateCategoryList("parentlocation", 0, "childlocation", true);
	var postbackControl = document.getElementById(postbackControlName);
	for (i = 0; i < childArray.length; i++) {
		for (j = 0; j < postbackControl.options.length; j++) {
			if (childArray[i] == postbackControl.options[j].value) {
				postbackControl.options[j].selected = true;
				$("#catchildlocation").removeAttr("disabled");
				break;
			}
		}
	}
	ResetSalary();
}
$(function() {
	var refine = $("#noSalaryCheck a");
	refine
			.click(function() {
				if (SEEK.mq.isTablet) {
					var salaryFrom = $("#salaryrefinefrom"), salaryTo = $("#salaryrefineto"), typeVal = salaryFrom[0].options[salaryFrom
							.children().length - 1].value.split(":")[1], type = (typeVal == "hourly") ? "salary"
							: "salaryhourly";
					RefineBy([ type, salaryFrom.val() + "-" + salaryTo.val() ]);
				}
			});
	if (SEEK.mq.isTablet) {
		SetInitialSalarySelections("refine");
	}
});

var $industrySelect = $('#catindustry');
var $professionSelect = $('#catoccupation');
var wWada = [];
for(var industryId in aRelationShips.industry) {
	
	var industry = {
		id: industryId,
		name: $industrySelect.find("option[value='"+industryId+"']").text(),
		occupations:[]
	};
	
	for(var i in aRelationShips.industry[industryId]) {
		var occupationId = aRelationShips.industry[industryId][i];
		industry.occupations.push({
			id: occupationId,
			name: (function(){
				var value = null;
				$professionSelect.find('option').each(function(){
					var vals = $(this).val().split(',');
					for(var i = 0; i<vals.length; i++) {
						if(occupationId == vals[i]) {
							value = $(this).text();
							return false;
						}
					}
				});
				return value;
				
			})()
		});
	}
	
	wWada.push(industry);
}

console.log(JSON.stringify(wWada));
