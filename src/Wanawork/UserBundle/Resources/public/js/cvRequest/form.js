$(function(){
	
	$('#new-job-spec').on('click', function(e){
		e.preventDefault();
		var link = $(this).prop('href') + '.json';
		
		$.get(link, function(data, status, xhr){
			setupForm(data);
		});
		
	});
	
	$('#new-job-dialog').dialog({
		autoOpen: false,
		modal: true,
		width: 800,
		height: 600
	});
	
	function setupForm(data)
	{
		var dialog = $('#new-job-dialog');
		dialog.html(data.form);
		var editor = CKEDITOR.replace( 'wanawork_userbundle_jobspec_description' );
		$('#wanawork_userbundle_jobspec_description').show();
		dialog.dialog('open');
		
		$('#job-form').on('submit', function(e){
			e.preventDefault();
			$('#wanawork_userbundle_jobspec_description').val(editor.getData());
			$.post($(this).prop('action'), $(this).serialize(), function(data, status, xhr){
				if(xhr.status === 201) {
					var entityId = xhr.getResponseHeader('ww_entity_id');
					var jobTitle = $('#wanawork_userbundle_jobspec_title').val();
					$('<option />', {
						selected:'selected',
						value: entityId
					}).html(jobTitle).appendTo('#wanawork_userbundle_cvrequest_job');
					
					dialog.empty().dialog('close');
				} else {
					setupForm(data);
				}
			});
			return false;
		});
	}
});

