$(function(){
	
	$('.filter-buttons i.icon').popover({
		html: true,
		placement: 'bottom',
		'trigger': 'hover',
		content: function(){
			return $('.status-toopltip').html();
		}
	});
	
	$('.update-status-form .btn').on('click', function(e){
		var $form = $(this).parents('form').first();
		$form.find('input.status-id').val($(this).data('status'));
	});
	
	$('.dropdown-menu .delete-link').on('click', function(e){
		e.preventDefault();
		var $form = $(this).parent().find('form'),
			$row = $(this).parents('tr').first();
		$.ajax({
			url: $form.attr('action') + '.json',
			type: 'DELETE',
			data: $form.serialize(),
			success: function(){
				if($row.siblings().length === 0) {
					location.reload(false);
				}
				$row.remove();
			}
		});
	});
	
});


/*
var requestsTable = $('#requests-table');
var requestTableTrs = requestsTable.find('tbody').find('tr');
$('div.filter-buttons').delegate('button.btn', 'click', function(e){
	var filter = $(this).data("filter");
	if(filter === 'all') {
		requestTableTrs.show();	
	} else {
		requestTableTrs.each(function(){
			$(this).hasClass(filter) ? $(this).show() : $(this).hide();
		});
	}
});*/