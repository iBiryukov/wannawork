$(function(){
	
	var requestsTable = $('#cv-list');
	var requestTableTrs = requestsTable.find('tbody').find('tr');
	$('div.filter-buttons').on('button.btn', 'click', function(e){
		
		var $this = $(this),
			filter = $this.data("filter");
		if($this.hasClass('disabled')) {
			return;
		}
		
		if(filter === 'all') {
			requestTableTrs.show();	
		} else {
			requestTableTrs.each(function(){
				$(this).hasClass(filter) ? $(this).show() : $(this).hide();
			});
		}
	});
});