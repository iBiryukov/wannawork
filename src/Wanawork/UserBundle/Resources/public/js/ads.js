$(function(){
	
	$('div.ad-row').each(function(){
		
		var heights = [];
		$(this).find('div.ad-box').each(function(){
			heights.push($(this).find('div.boxSquare').height());
		});
		
		var max = _.max(heights);
		$(this).find('div.ad-box').each(function(){
			$(this).find('div.boxSquare').height(max);
		});
	});
	
	$('#ad-list').delegate('a.delete', 'click', function(e){
		e.preventDefault();
		
		var parent = $(this).parents('div.ad-box');
		var headline = parent.find('h4.headline').text();
		
		if(confirm('Are you sure you want to delete this ad "'+headline+'"')) {
			parent.find('form.delete-form').submit();
		}
	});
});