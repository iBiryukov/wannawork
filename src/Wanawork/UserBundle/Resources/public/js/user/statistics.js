$(function(){
	
	var $frequenceFilter = $('#frequency-filter'),
		$monthSelector = $('#month'),
		$yearSelector = $('#year'),
		monthNames = {
			'01': "January", 
			'02': "February", 
			'03': "March", 
			'04': "April", 
			'05': "May", 
			'06': "June",
		    '07': "July", 
		    '08': "August", 
		    '09': "September", 
		    '10': "October", 
		    '11': "November", 
		    '12': "December" 
	    }, 
	    plot = null;
	
	$frequenceFilter.on('click', 'button', function(){
		$(this).siblings().removeClass('active');
		$(this).addClass('active');
		drawGraph();
	});
	
	$monthSelector.change(function(){
		drawGraph();
	});
	
	$yearSelector.change(function(){
		drawGraph();
	});
	
	function drawGraph() {
		var employeeData = groupDataSet(registrationsEmployee),
			employerData = groupDataSet(registrationsEmployer);
		
		if(plot !== null) {
			plot.destroy();
		}
		
		if(employeeData.length === 0) {
			$('#registrationsGraph').html('No data for the selected time period');
			return;
		}
		
		plot = $.jqplot('registrationsGraph', [employeeData, employerData], jqplotOptions("Date when your ad was viewed", "View Count"));
	}
	
	function groupDataSet(dataset) {
		var newData = [];
		
		switch($frequenceFilter.find('button.active').data('filter')) {
			case 'day':
				var key = $yearSelector.val() + '-' + $monthSelector.val();
				for(var date in dataset) {
					if(date.substr(0,7) === key) {
						newData.push([date.substr(8), dataset[date]]);
					}
				}
				break;
			
			case 'month':
				var key = $yearSelector.val();
				var monthsGrouped = {};
				for(var date in dataset) {
					var dateParts = date.split('-');
					if(dateParts[0] === key) {
						var monthName = monthNames[dateParts[1]];
						if(!(monthName in monthsGrouped)) {
							monthsGrouped[monthName] = 0;
						}
						monthsGrouped[monthName] += dataset[date];
					}
				}
				newData = _.pairs(monthsGrouped);
				break;
				
			case 'year':
				var yearGrouped = {};
				for(var date in dataset) {
					var dateParts = date.split('-');
					if(!(dateParts[0] in yearGrouped)) {
						yearGrouped[dateParts[0]] = 0;
					}
					yearGrouped[dateParts[0]] += dataset[date];
				}
				newData = _.pairs(yearGrouped);
				break;
		}
		return newData;
	}
	
	function jqplotOptions(xLabel, yLabel) {
		return {
			series:[{
				label: 'Employee Data'
			},{
				label: 'Employer Data'
			}],
			legend: {
				show: true
			},
			seriesDefaults: {
				renderer: $.jqplot.BarRenderer,
				rendererOptions: {
	               // barPadding: 1,
	                barDirection: 'vertical'
	               // barWidth: 20
	            },
	            pointLabels: {
	            	show: true
	            }
			},
			axes: {
				yaxis: {
					showMinorTicks:true,
					label: yLabel,
					labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
					labelOptions: {
						angle: -90
					},
					tickOptions: {
						formatString:'%.0f'
					},
					rendererOptions: {
						forceTickAt0: true
					}
				},
				xaxis: {
					renderer: $.jqplot.CategoryAxisRenderer,
					label: xLabel,
					tickRenderer: $.jqplot.CanvasAxisTickRenderer,
					tickOptions: {
						angle: 0,
						fontSize: '8pt'
					}
				}
			}
		};
	}
	
	
	drawGraph();
	
});