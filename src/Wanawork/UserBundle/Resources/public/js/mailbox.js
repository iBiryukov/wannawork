$(function(){
	
	$('.mailbox').on('click', '.send-message-button', function(e){
		e.preventDefault();
		
		var $form = $(this).parents('form'),
		    url = $form.prop('action');
		
		if($form.find('textarea').val().trim().length === 0) {
			return;
		}
		
		$.post(url, $form.serialize(), function(data, status, xhr){
			if (xhr.status === 201) {
				var $messagesContainer = $('.mailbox .messages');
				if($messagesContainer.find('.mailbox-empty-message').length > 0) {
					$messagesContainer.find('.mailbox-empty-message').remove();
				}
				$(data.message).appendTo($messagesContainer);
				$messagesContainer.scrollTop($messagesContainer[0].scrollHeight);
				$form.find('textarea').val('');
			}
		});
		return false;
	});
	
	$('.mailbox').on('mailbox-loaded', function(e){
		var $thread = $('.thread.selected');
		if($thread.length > 0) {
			$.getJSON($thread.data('markreadurl'));
			$thread.find('.new-message-count-badge').html('&nbsp;');
		}
		
	});
	
	$('.thread').on('click', function(){
		var url = $(this).data('messagesurl');
		History.pushState(null, null, $(this).data('threadurl'));
		loadMessages.apply(this, [url]);
	});
	
	function loadMessages(url) {
		$.get(url, function(data){
			$('.right-column').html(data);
			resize();
			$('.timeago').timeago();
			$('.new-message-form').find('textarea').autosize({
				callback: function(){
					var $formDiv = $('.new-message-form');
					    $textArea = $formDiv.find('textarea');
					    $submitDiv = $formDiv.find('.submit'),
					    $messagesContainer = $('.mailbox .messages');
					    
					$formDiv.height($textArea.outerHeight() + $submitDiv.outerHeight() + 20);
					resize();
					$messagesContainer.scrollTop($messagesContainer[0].scrollHeight);
				}
			}).placeholder();
			$('.mailbox').trigger('mailbox-loaded');
		});
		
		$('.threads .thread.selected').removeClass('selected');
		$(this).addClass('selected');
	}
	
	$('.thread-search').on('keyup', function(){
		var search = $(this).val().toLocaleLowerCase();
		
		if(search && search.length > 0) {
			$('.thread .respondent').each(function(){
				var $thread = $(this).parents('.thread');
				var text = $(this).text().trim().toLocaleLowerCase().split(' ');
				for(var i = 0; i < text.length; i++) {
					if(text[i].search(search) === 0) {
						$thread.show();
						return;
					}
				}
				$thread.hide();
			});
		} else {
			$('.thread').show();
		}
		
	});
	
//	$('#mailbox-messages div.message i.icon-remove').click(function(e){
//		if(confirm('Are you sure you want to delete this message?')) {
//			$(this).parent().find('form.delete-form').submit();
//		}
//	});
	
	function resize() {
		var windowHeight = $(window).height();
		var headerHeight = $('#header-wrapper').outerHeight();
		var pageHeadingHeight = $('.heading').outerHeight();
		var footerHeight = 20;
		var availableHeight = windowHeight - headerHeight - footerHeight - pageHeadingHeight;
		
		var minHeight = $('.new-message-form').outerHeight() || 100;
		if(availableHeight < minHeight) {
			availableHeight = minHeight;
		}
		$('.mailbox').height(availableHeight);
		
		if($('.right-column').length > 0) {
			var formHeight = $('.new-message-form').outerHeight();
			$('.messages').height(availableHeight - formHeight - 10);
		}
		
		var searchHeight = $('.search').outerHeight();
		$('.threads').height(availableHeight - searchHeight);
	}
	
	resize();
	$(window).resize(function(){
		resize();
	});
	
	History.Adapter.bind(window,'statechange',function(){ // Note: We are using statechange instead of popstate
        var returnLocation = History.getState(); // Note: We are using History.getState() instead of event.state
		
		if(/mailbox(\/\d+)?$/.test(returnLocation.url)) {
			var urlBits = returnLocation.url.split('/');
			if(/^\d+$/.test(urlBits[urlBits.length - 1])) {
				var $thread = $('#thread-' + urlBits[urlBits.length - 1]);
				if($thread.length === 1) {
					loadMessages.apply($thread.get(0), [$thread.data('messagesurl')]);
				}
			}
		} else {
			location.href = returnLocation.url;
		}
    });
	
	if(/#mailbox\/\d+$/.test(location.href)) {
		var urlBits = location.href.split('/');
		if(/^\d+$/.test(urlBits[urlBits.length - 1])) {
			var $thread = $('#thread-' + urlBits[urlBits.length - 1]);
			if($thread.length === 1) {
				loadMessages.apply($thread.get(0), [$thread.data('messagesurl')]);
			}
		}
	}
	
	
	if($('.thread.selected').length > 0) {
		$('.mailbox').trigger('mailbox-loaded');
	}
	
});