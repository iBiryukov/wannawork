$(function(){
	
	$('#avatar-upload').fileupload({
		autoUpload: true,
		maxFileSize: 10 * 1024 * 1024
	}).on('fileuploadstart', function(){
		$('#progress-bar-inner').html('Uploading...');
		$('#progress-bar-outer').show();
	}).on('fileuploadprogress', function(e, data){
		var progress = parseInt(data.loaded / data.total * 100, 10);
		$('#progress-bar-inner').width(progress + '%');
	}).on('fileuploaddone', function(e, data){
		$('#progress-bar-inner').html('Finished');

		if('path' in data.result) {
			$('#form-avatar').attr('src', data.result.path);
		}
		
		if('id' in data.result) {
			$('#employee_profile_temp_avatar').val(data.result.id);
		}
		
	}).on('fileuploadfail', function(){
		$('#progress-bar-inner').html('Failed. Try again');
	});	
	
	$("#employee_profile_languages").chosen();
	$("#employee_profile_county").chosen();
});