$(function(){
	$('.switch').on('click', function(e){
		e.preventDefault();
		var $this = $(this),
		    $otherLink = $('.switch').not($this);
		
		if ($this.hasClass("active")) {
			return;
		}
		
		var $toShow = $($this.attr('href')),
		    $toHide = $($otherLink.attr('href'));
		
		$toHide.slideUp(400, function(){
			$toShow.slideDown(300);
		});
		
		$this.addClass('active');
		$otherLink.removeClass('active');
	});
});