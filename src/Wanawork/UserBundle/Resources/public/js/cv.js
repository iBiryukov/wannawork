//$(function() {
//	function attachDateTimePickers () {
//		$("form.inline-education-form").find('.date').each(function(){
//			$(this).monthpicker({
//				pattern: 'mmm-yyyy',
//				startYear: wanawork.dateStartYear(),
//				finalYear: wanawork.dateEndYear()
//			});
//		});
//	}
//	attachDateTimePickers();
//
//	function attachEducationEditHandler() {
//		$("form.inline-education-form").each(function(){
//			
//			$(this).attachAjaxHandler({
//				processData: function(data) {
//					
//					for(var i in data) {
//						var entry = data[i];
//						if(entry['value'] && /start]$/.test(entry['name']) || /finish]$/.test(entry['name'])) {
//							
//							var splitValue = entry['value'].split('-');
//							if(splitValue.length === 2) {
//								var newValue = '01-' + splitValue[0] + '-' + splitValue[1];
//								entry['value'] = newValue;
//							}
//						}
//					}
//					return data;
//				}
//				
//			}, function(data){
//				if(data['row']) {
//					this.resetForm();
//					if($(this).hasClass('inline-education-form-edit')) {
//						$(data['row']).insertBefore($(this).prev());
//						$(this).prev().remove();
//						$(this).remove();
//					} else {
//						$(data['row']).insertBefore(this);
//					}
//					
//					attachEducationDeleteHandler();
//					attachEducationEditHandler();
//					attachDateTimePickers();
//				}
//			});
//		});
//	}
//	attachEducationEditHandler();
//	
//	function attachEducationDeleteHandler() {
//		$("form.education-delete-form").each(function(){
//			$(this).attachAjaxHandler({}, function(data){
//				var field1 = $(this).parents("div.field").first(), 
//					field2 = $(this).parents("div.field").first().next();
//				field1.remove(); 
//				field2.remove();
//			});
//		});
//	}
//	attachEducationDeleteHandler();
//	
//	// Show edit form on 'edit-button' click
//	$("div.education").delegate('a.iconEdit', 'click', function(e){
//		e.preventDefault();
//		$(this).parents('div.field').first().hide().next().show();
//	});
//	
//	// Hide edit form on cancel button click
//	$("div.education").delegate('input.butCancel', 'click', function(e) {
//		$(this).parents('div.field').first().parent().hide().prev().show();
//	});
//	
//});