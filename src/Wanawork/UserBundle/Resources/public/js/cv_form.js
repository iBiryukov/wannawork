$(function(){
	var $cvForm = $('#cv_form'),
		dateRegex = /^(0[1-9]|1[1-2])\/((19\d{2})|(20\d{2}))$/;
	
	$cvForm.on('submit', function(e){
		
		var result = true;
		if(!validateCvNameCovernote()) {
			result = false;
		}
		
		var educationsValid = true;
		$('#education-section').find('.education_row').each(function(){
			if(!validateEducation($(this))) {
				result = false;
				educationsValid = false;
			}
		});
		
		if(educationsValid) {
			$('a.accordion-toggle[href="#education-section"]').removeClass('error').addClass('ok');
		} else {
			$('a.accordion-toggle[href="#education-section"]').removeClass('ok').addClass('error');
		}
		
		var jobsValid = true;
		$('#jobs-section').find('.work-experience-row').each(function(){
			if(!validateWorkExperience($(this))) {
				result = false;
				jobsValid = false;
			}
		});
		
		if(jobsValid) {
			$('a.accordion-toggle[href="#jobs-section"]').removeClass('error').addClass('ok');
		} else {
			$('a.accordion-toggle[href="#jobs-section"]').removeClass('ok').addClass('error');
		}
			
		var refsValid = true;
		$('#references-section').find('.reference-row').each(function(){
			if(!validateReference($(this))) {
				result = false;
				refsValid = false;
			}
		});
		
		if(refsValid) {
			$('a.accordion-toggle[href="#references-section"]').removeClass('error').addClass('ok');
		} else {
			$('a.accordion-toggle[href="#references-section"]').removeClass('ok').addClass('error');
		}
		
		if(!validateAchievements()) {
			result = false;
		}
		return result;
	});
	
	var $errorSpan = $('<span />').addClass('help-inline');
	var processResult = function(result, $element, message, showErrors) {
		
		if(showErrors) {
			var $errorSpan = $('<span />').css('display', 'block').addClass('help-inline input-medium');
			if(result !== true && !$element.hasClass('error')) {
				$element.addClass('error');
				$errorSpan.clone().text(message).appendTo($element.find('div.controls'));
			} else if(result === true && $element.hasClass('error')) {
				$element.removeClass('error');
				$element.find('span.help-inline').remove();
			}
		}
		
		if(!result) {
			outcome = false;
		}
	};
	
	var validateAchievements = function(showErrors) {
		var outcome = true;
		if(showErrors === undefined) {
			showErrors = true;
		}
		
		$achievementField = $('#wanawork_userbundle_cvtype_achievements');
		var validate = function() {
			var result = $achievementField.val().length > 0;
			var message = 'Please enter some of your achievements';
			processResult(result, $achievementField.parents('.control-group').first(), message, showErrors);
			
			if(result) {
				$achievementField.parents('.accordion-group').find('a.accordion-toggle').removeClass('error').addClass('ok');
			} else {
				$achievementField.parents('.accordion-group').find('a.accordion-toggle').removeClass('ok').addClass('error');
			}
			return result;
		};
		
		$achievementField.off('focusout keyup').on({
			'focusout': validate,
			'keyup': validate
		});
		
		if(!validate()) {
			outcome = false;
		}
		return outcome;
	};
	
	var validateCvNameCovernote = function(showErrors) {
		var outcome = true;
		
		if(showErrors === undefined) {
			showErrors = true;
		}
		
		// 1. cv name
		var $cvNameField = $('#wanawork_userbundle_cvtype_name');
		var validateCvName = function() {
			var result = $cvNameField.val().length > 0;
			var message = 'Please name your CV';
			processResult(result, $cvNameField.parents('.control-group').first(), message, showErrors);
			return result;
		};
		
		$cvNameField.off('focusout keyup').on({
			'focusout': validateCvName,
			'keyup': validateCvName
		});
		
		if(!validateCvName()) {
			outcome = false;
		}
		
		$covernoteField = $('#wanawork_userbundle_cvtype_coverNote');
		var validateCovernote = function() {
			var result = $covernoteField.val().length > 0;
			var message = 'Please enter your cover note';
			processResult(result, $covernoteField.parents('.control-group').first(), message, showErrors);
			$covernoteField.parent().find('span.help-inline').removeClass('input-medium');
			return result;
		};
		
		$covernoteField.off('focusout keyup').on({
			'focusout': validateCovernote,
			'keyup': validateCovernote
		});
		
		if(!validateCovernote()) {
			outcome = false;
		}
		
		if(outcome) {
			$cvNameField.parents('.accordion-group').find('a.accordion-toggle').removeClass('error').addClass('ok');
		} else {
			$cvNameField.parents('.accordion-group').find('a.accordion-toggle').removeClass('ok').addClass('error');
		}
		
		return outcome;
	};
	
	var validateReference = function($dataRow, showErrors) {
		if(showErrors === undefined) {
			showErrors = true;
		}
		
		var outcome = true;
		var $minifiedRow = $dataRow.prev();
		
		// 1. Employer (Company)
		var $employerField = $dataRow.find('input.company');
		var validateEmployerField = function() {
			var result = $employerField.val().length > 0;
			var message = 'Please enter the company name where you worked';
			processResult(result, $employerField.parents('div.company'), message, showErrors);
			if(result) {
				$minifiedRow.find('.company').text($employerField.val());
			}
			return result;
		};
		
		if(!validateEmployerField()) {
			outcome = false;
		}
			
		$employerField.off('focusout keyup').on({
			'focusout': validateEmployerField,
			'keyup': validateEmployerField
		});
		
		// 2. Contact Name
		$contactField = $dataRow.find('input.contact');
		var validateContact = function(){
			var result = $contactField.val().length > 0;
			var message = 'Please enter the contact name in the company';
			processResult(result, $contactField.parents('div.contact'), message, showErrors);
			if(result) {
				$minifiedRow.find('.contact').text($contactField.val());
			}
			return result;
		};
		
		if(!validateContact()) {
			outcome = false;
		}
		
		$contactField.off('focusout keyup').on({
			'focusout': validateContact,
			'keyup': validateContact
		});
		
		// 3. Phone Number
		$phoneField = $dataRow.find('input.phone');
		var validatePhone = function(){
			var result = $phoneField.val().length > 0;
			var message = 'Please enter the phone number for your referee';
			processResult(result, $phoneField.parents('div.phone'), message, showErrors);
			if(result) {
				$minifiedRow.find('.phone').text($phoneField.val());
			}
			return result;
		};
		
		if(!validatePhone()) {
			outcome = false;
		}
		
		$phoneField.off('focusout keyup').on({
			'focusout': validatePhone,
			'keyup': validatePhone
		});
		
		// 4. Email
		$emailField = $dataRow.find('input.email');
		var validateEmail = function(){
			var result = $emailField.val().length > 0;
			var message = 'Please enter the email for your referee';
			processResult(result, $emailField.parents('div.email'), message, showErrors);
			if(result) {
				$minifiedRow.find('.email').text($emailField.val());
			}
			return result;
		};
		
		if(!validateEmail()) {
			outcome = false;
		}
		
		$emailField.off('focusout keyup').on({
			'focusout': validateEmail,
			'keyup': validateEmail
		});
		
		if(outcome) {
			if(showErrors) {
				$dataRow.slideUp(400, function(){
					$minifiedRow.slideDown();
				});
			} else {
				$dataRow.hide();
				$minifiedRow.show();
			}
		} else {
			if(showErrors) {
				$minifiedRow.slideUp(400, function(){
					$dataRow.slideDown();
				});
			} else {
				$dataRow.show();
				$minifiedRow.hide();
			}
		}
		return outcome;
	};
	
	var validateWorkExperience = function ($dataRow, showErrors) {
		if(showErrors === undefined) {
			showErrors = true;
		}
		
		var outcome = true;
		var $minifiedRow = $dataRow.prev();
		
		// 1. Start Date
		var $startDateField =  $dataRow.find('input.start-date');
		var validateStartDateField = function(){
			var result = dateRegex.test($startDateField.val());
			var message = 'Please specify when you started the job';
			processResult(result, $startDateField.parents('div.start_date'), message, showErrors, showErrors);
			if(result) {
				$minifiedRow.find('.start-date').text($startDateField.val());
			}
			return result;
		};
		
		if(!validateStartDateField()) {
			outcomce = false;
		}
		
		$startDateField.change(function(){
			validateStartDateField();
		});
		
		// 2. End Date
		var $endDateField =  $dataRow.find('input.finish-date');
		var validateEndDateField = function(){
			var result = $endDateField.val().length === 0 ? true : dateRegex.test($endDateField.val());
			var message = 'Please specify when you left the job or leave it blank';
			processResult(result, $endDateField.parents('div.finish_date'), message, showErrors);
			if(result) {
				if($endDateField.val().length > 0) {
					$minifiedRow.find('.finish-date').text($endDateField.val());
				} else {
					$minifiedRow.find('.finish-date').text("Current Job");
				}
				
			}
			return result;
		};
		
		if(!validateEndDateField()) {
			outcome = false;
		}
		
		$endDateField.change(function(){
			validateEndDateField();
		});
		
		// 3. Employer
		var $employerField = $dataRow.find('input.company');
		var validateEmployerField = function() {
			var result = $employerField.val().length > 0;
			var message = 'Please enter the company name where you worked';
			processResult(result, $employerField.parents('div.company'), message, showErrors);
			if(result) {
				$minifiedRow.find('.company').text($employerField.val());
			}
			return result;
		};
		
		if(!validateEmployerField()) {
			outcome = false;
		}
		
		$employerField.off('focusout keyup').on({
			'focusout': validateEmployerField,
			'keyup': validateEmployerField
		});
		
		// 4. Position
		var $positionField = $dataRow.find('input.position');
		var validatePositionField = function() {
			var result = $positionField.val().length > 0;
			var message = 'Please enter the position you held in the company';
			processResult(result, $positionField.parents('div.position'), message, showErrors);
			if(result) {
				$minifiedRow.find('.position').text($positionField.val());
			}
			return result;
		};
		validatePositionField();
		
		$positionField.off('focusout keyup').on({
			'focusout': validatePositionField,
			'keyup': validatePositionField
		});
		
		// 5. Description
		var $descriptionField = $dataRow.find('textarea.description');
		var validateDescriptionField = function() {
			var result = $descriptionField.val().length > 0;
			var message = 'Please provide small description of the position you held';
			processResult(result, $descriptionField.parents('div.description'), message, showErrors);
			if(result) {
				$minifiedRow.find('.description').text($descriptionField.val());
			}
			$descriptionField.parents('div.description').find('span.help-inline').removeClass('input-medium');
			return result;
		};
		
		if(!validateDescriptionField()) {
			outcome = false;
		}
		
		$descriptionField.off('focusout keyup').on({
			'focusout': validateDescriptionField,
			'keyup': validateDescriptionField
		});
		
		if(outcome) {
			if(showErrors) {
				$dataRow.slideUp(400, function(){
					$minifiedRow.slideDown();
				});
			} else {
				$dataRow.hide();
				$minifiedRow.show();
			}
		} else {
			if(showErrors) {
				$minifiedRow.slideUp(400, function(){
					$dataRow.slideDown();
				});
			} else {
				$dataRow.show();
				$minifiedRow.hide();
			}
		}
		
		return outcome;
	};
	
	var validateEducation = function($educationRow, showErrors) {
		if(showErrors === undefined) {
			showErrors = true;
		}
		
		$minifiedRow = $educationRow.prev();
		var outcome = true;
		
		// 1. Start Date
		var $startDateField =  $educationRow.find('input.start-date');
		var validateStartDateField = function(){
			var result = dateRegex.test($startDateField.val());
			var message = 'Please specify when you started the course';
			processResult(result, $startDateField.parents('div.start_date'), message, showErrors, showErrors);
			if(result) {
				$minifiedRow.find('.start-date').text($startDateField.val());
			}
			return result;
		};
		
		if(!validateStartDateField()) {
			outcome = false;
		}
		
		$startDateField.change(function(){
			validateStartDateField();
		});
		
		// 2. End Date
		var $endDateField =  $educationRow.find('input.end-date');
		var validateEndDateField = function(){
			var result = dateRegex.test($endDateField.val());
			var message = 'Please specify when you finished the course';
			processResult(result, $endDateField.parents('div.finish_date'), message, showErrors);
			if(result) {
				$minifiedRow.find('.finish-date').text($endDateField.val());
			}
			return result;
		};
		
		if(!validateEndDateField()) {
			outcome = false;
		}
		
		$endDateField.change(function(){
			validateEndDateField();
		});
		
		// 3. Institute
		var $collegeField =  $educationRow.find('input.college');
		var validateCollegeField = function(){
			var result = $collegeField.val().length > 0;
			var message = 'Please enter the name of the institute you attended';
			processResult(result, $collegeField.parents('div.college'), message, showErrors);
			if(result) {
				$minifiedRow.find('.college').text($collegeField.val());
			}
			return result;
		};
		
		if(!validateCollegeField()) {
			outcome = false;
		}
		
		$collegeField.off('focusout keyup').on({
			'focusout': validateCollegeField,
			'keyup': validateCollegeField
		});
		
		// 4. College Award
		var $awardField =  $educationRow.find('input.award');
		var validateAwardField = function(){
			var result = $awardField.val().length > 0;
			var message = 'Please enter the course you attended and award you received';
			processResult(result, $awardField.parents('div.award'), message, showErrors);
			return result;
		};
		
		if(!validateAwardField()) {
			outcome = false;
		}
		
		$awardField.off('focusout keyup').on({
			'focusout': validateAwardField,
			'keyup': validateAwardField
		});
		
		// 5. Received award
		var $qualificaitonField =  $educationRow.find('select.qualification');
		var validateQualificationField = function(){
			var result = $qualificaitonField.val() > 0;
			var message = 'Please enter the course you attended and award you received';
			processResult(result, $qualificaitonField.parents('div.qualification'), message, showErrors);
			$qualificaitonField.parents('div.qualification').find('span.help-inline').removeClass('input-medium');
			
			if(result) {
				$minifiedRow.find('.award').text($qualificaitonField.find('option:selected').text());
			}
			return result;
		};
		
		if(!validateQualificationField()) {
			outcome = false;
		}
		
		$qualificaitonField.change(function(){
			validateQualificationField();
		});
		
		// 6. Obtained qualifications
		var $coursesField =  $educationRow.find('select.courses');
		var validateCoursesField = function(){
			var result = $.isArray($coursesField.val()) && $coursesField.val().length > 0;
			var message = 'Please specify at least one qualification';
			processResult(result, $coursesField.parents('div.course'), message, showErrors);
			$coursesField.parents('div.course').find('span.help-inline').removeClass('input-medium');
			return result;
		};
		
		if(!validateCoursesField()) {
			outcome = false;
		}
		
		$coursesField.change(function(){
			validateCoursesField();
		});
		
		if(outcome) {
			if(showErrors) {
				$educationRow.slideUp(400, function(){
					$minifiedRow.slideDown();
				});
			} else {
				$educationRow.hide();
				$minifiedRow.show();
			}
		} else {
			if(showErrors) {
				$minifiedRow.slideUp(400, function(){
					$educationRow.slideDown();
				});
			} else {
				$educationRow.show();
				$minifiedRow.hide();
			}
		}
		return outcome;
	};
	
	$('#education-section').on('click', '.education-save-button', function(e){
		e.preventDefault();
		validateEducation($(this).parents('.education_row'));
		
	}).on('click', '.iconEdit', function(e){
		e.preventDefault();
		$(this).parents('.education-row-mini').slideUp(400, function(){
			$(this).next().slideDown();
		});
	});
	
	$('#jobs-section').on('click', '.work-exprience-save-button', function(e){
		e.preventDefault();
		validateWorkExperience($(this).parents('.work-experience-row'));
	}).on('click', '.iconEdit', function(e){
		e.preventDefault();
		$(this).parents('.work-experience-row-mini').slideUp(400, function(){
			$(this).next().slideDown();
		});
	});
	$('#references-section').on('click', '.reference-save-button', function(e){
		e.preventDefault();
		validateReference($(this).parents('.reference-row'));
	}).on('click', '.iconEdit', function(e){
		e.preventDefault();
		$(this).parents('.reference-row-mini').slideUp(400, function(){
			$(this).next().slideDown();
		});
	});
	
	$('a.next-section').click(function(e){
		e.preventDefault();
		var result = false;
		switch(this.id) {
			case 'cv-covernote-next':
				result = validateCvNameCovernote();
			break;
			
			case 'education-next':
				result = true;
				$(this).parents('#education-section').find('.education_row').each(function(){
					if(!validateEducation($(this))) {
						result = false;
					}
				});
				break;
				
			case 'work-experience-next':
				result = true;
				$(this).parents('#jobs-section').find('.work-experience-row').each(function(){
					if(!validateWorkExperience($(this))) {
						result = false;
					}
				});
				break;
				
			case 'references-next':
				result = true;
				$(this).parents('#references-section').find('.reference-row').each(function(){
					if(!validateReference($(this))) {
						result = false;
					}
				});
				break;
		}
		
		if(result) {
			$(this).parents('.accordion-group').next().find('a.accordion-toggle').click();
			$(this).parents('.accordion-group').find('a.accordion-toggle').removeClass('error').addClass('ok');
		} else {
			$(this).parents('.accordion-group').find('a.accordion-toggle').removeClass('ok').addClass('error');
		}
	});
	
	// Remove a selected course on item click
	$cvForm.on('click', 'a.remove-course', function(e){
		e.preventDefault();
		
		var courseId = $(this).parents('li.search-choice').data('course').toString();
		var $actualCourseSelector = $(this).parents('div.course-block').find('select.courses');
		var currentValue = $actualCourseSelector.val();
		if(currentValue === null) {
			currentValue = [];
		}
		currentValue = _.without(currentValue, courseId);
		$actualCourseSelector.val(currentValue);
		$actualCourseSelector.trigger('change');
		
		if($(this).parent().siblings().length === 0) {
			$(this).parents('li.sector').remove();
		} else {
			$(this).parent().remove();
		}
	});
	
	
	$('div.addable-section').find('a.add-row').click(function(e){
		e.preventDefault();
		var nextCounter = $(this).siblings('div.form_row').length;
		var newRow = $(this).parents('div.addable-section').find('div.template').first().clone(false, false);
		var newHtml = newRow.html().replace(/_\d+_/g, "_" + nextCounter + "_").replace(/\[\d+\]/g, "[" + nextCounter + "]");
		newRow.html(newHtml);
		clearNewRow(newRow);
		if($(this).siblings('.form_row').length > 0) {
			newRow.insertAfter($(this).siblings('.form_row').last());
		} else {
			newRow.insertBefore(this);
		}
		
		newRow.find('div.field').hide();
		newRow.find('div.form-expanded').show();
		
		newRow.find("select.chzn-done").removeAttr("style", "").removeClass("chzn-done").data("chosen", null).next().remove();
		setupChosen();
		attachMonthPickers(newRow);
	});
	
	function clearNewRow(newRow) {
		newRow.find('div.error').removeClass('error');
		newRow.find('span.help-inline').remove();
		newRow.find('input, select, textarea').val('');
		if(newRow.hasClass('education')) {
			newRow.find('ul.selected-sectors').empty();
		}
	}
	
	$('div.removable-section').on('click', 'a.delete', function(e){
		e.preventDefault();
		var parentHolder = $(this).parents('div.form_row').first().parent();
		var rowToRemove = $(this).parents('div.form_row').first();
		if(rowToRemove.siblings('div.form_row').length === 0) {
			$(this).parents('div.removable-section').find('a.add-row').click();
		}
		rowToRemove.remove();
		
		var i = 0;
		parentHolder.find('div.form_row').each(function(){
			$(this).find('input, select, textarea').each(function(){
				var id = $(this).attr('id').replace(/_\d+_/g, "_" + i + "_").replace(/\[\d+\]/g, "[" + i + "]"),
					name = $(this).attr('name').replace(/_\d+_/g, "_" + i + "_").replace(/\[\d+\]/g, "[" + i + "]");
				$(this).attr('id', id);
				$(this).attr('name', name);
			});
			++i;
		});
	});
	
	function setupChosen() {
		$("select.chosen").chosen();
	}
	setupChosen();
	
	function attachMonthPickers(parent) {
		var elements = null;
		if(parent) {
			elements = $(parent).find('div.start_date, div.finish_date');
		} else {
			elements = $('div.start_date, div.finish_date');
		}
		
		elements.each(function(){
			var $that = $(this).find('input');
			$that.mask('99/9999');
		});
		
//		elements.each(function(){
//			var that = $(this).find('input');
//			that.removeClass('hasMonthpicker');
//			if(!$(that).data('monthpicker')) {
//				$(that).monthpicker({
//					dateFormat: 'M yy',
//					yearRange: $(that).data('start') + ":" + $(that).data('end')
//				});
//			}
//		});
	}
	attachMonthPickers();
	
});


//Sector selection
function onSectorChange(that) {
	var $that = $(that),
		selectedValue = $that.val(); // sector id
	
	// got the selected sector id? Fill up the course list 
	if(selectedValue in wanawork.educationSectors) {
		var $courseSelect = $that.parent().find('select.course-selector');
		
		$courseSelect.find('option').each(function(){
			$this = $(this);
			if(!$this.hasClass('default')) {
				$this.remove();
			}
		});
		
		for(var courseId in wanawork.educationSectors[selectedValue]['courses']) {
			var $option = $('<option />')
				.text(wanawork.educationSectors[selectedValue]['courses'][courseId]['title'])
				.attr('value', wanawork.educationSectors[selectedValue]['courses'][courseId]['id']);
			$courseSelect.append($option);
		}
	}
};

function onCourseChange(that){
	
	var $that = $(that),
		$sectorSelect = $that.parent().find('select.sector-selector'),
		selectedSectorId = $sectorSelect.val(),
		selectedCourseId = $that.val(),
		$selectedSectors = $that.parent().find('.selected-sectors'),
		$sectorLi = $selectedSectors.find('[data-sector="'+selectedSectorId+'"]'),
		$coursesDiv = null,
		$courseLi = null,
		$courseListTemplate = $('div.course-list-template').first();
	
	// Create the sector entry
	if($sectorLi.length === 0) {
		
		$sectorLi = $('<li />', {
			'data-sector': selectedSectorId
		}).addClass('sector');
		$sectorLi.append(
			$('<span />', {
				text: wanawork.educationSectors[selectedSectorId]['title']
			})
		);
		$sectorLi.appendTo($selectedSectors);
		
		// Empty list for course entries
		$coursesDiv = $courseListTemplate.last().clone();
		$coursesDiv.find('li').remove();
		$coursesDiv.appendTo($sectorLi);
		$coursesDiv.show();
	} else {
		$coursesDiv = $sectorLi.find('div.chzn-container');
	}
	
	$courseLi = $coursesDiv.find('[data-course="'+selectedCourseId+'"]');
	if($courseLi.length === 0) {
		$courseLi = $courseListTemplate.find('li').last().clone();
		$courseLi.find('span').text($that.find('option:selected').text());
		$courseLi.attr('data-course', selectedCourseId);
		$courseLi.appendTo($coursesDiv.find('ul'));
	} else {
		$courseLi.effect("highlight", {}, 1000);
	}
	var $actualCourseSelector = $that.parents('div.course-block').find('select.courses');
	var newValue = [];
	if($.isArray($actualCourseSelector.val())) {
		newValue = $actualCourseSelector.val();
	}
	newValue.push($that.val());
	$actualCourseSelector.val(newValue);
	$actualCourseSelector.trigger('change');
	$that.val(-1);
};
