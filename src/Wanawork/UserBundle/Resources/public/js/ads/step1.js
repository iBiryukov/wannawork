$(function(){
	$('#wanawork_userbundle_adtype_industries').chosen();
	$('#wanawork_userbundle_adtype_profession').chosen({
		search_placeholder: 'Search for your profession'
	});
	$('#wanawork_userbundle_adtype_positions').chosen();
	$('#wanawork_userbundle_adtype_locations').chosen();
	$('#wanawork_userbundle_adtype_languages').chosen();
	$('#wanawork_userbundle_adtype_experience').chosen();
	$('#wanawork_userbundle_adtype_educationLevel').chosen();
	
	
	var $cvUploadWrapper = $('.upload-wrapper'),
		$cvViewWapper = $('.cv-view-wrapper'),
		$cvInput = $('#wanawork_userbundle_adtype_cv');
	
	$('#cv-file-upload').fileupload({
		autoUpload: true,
		maxFileSize: 10 * 1024
	}).on('fileuploadstart', function(){
		$('#progress-bar-inner').html('Uploading...');
		$('#progress-bar-outer').show();
	}).on('fileuploadprogress', function(e, data){
		var progress = parseInt(data.loaded / data.total * 100, 10);
		$('#progress-bar-inner').width(progress + '%');
	}).on('fileuploaddone', function(e, data){
		$('#progress-bar-inner').html('Finished');
		setTimeout(function(){
			$('#progress-bar-outer').slideUp();
			$cvUploadWrapper.slideUp(300, function(){
				$cvViewWapper.slideDown(200, function(){
					$('.keep-existing-cv').show();
					$cvViewWapper.find('.cv-name').text(data.result.name);
					$cvInput.val(data.result.cv);
				});
			});
		}, 1000);
		
		$('.view-cv').data('url', data.result.download_url);
		
	}).on('fileuploadfail', function(e, data){
		var response = $.parseJSON(data.jqXHR.responseText);
		if ('errors' in response) {
			$('#progress-bar-inner').html(response.errors[0]);
		} else {
			$('#progress-bar-inner').html('Upload Failed. Try again');
		}
		
	});
	
	var $headlineTextbox = $('#wanawork_userbundle_adtype_headline');
	$("#headline-remaining-text").html(($headlineTextbox.attr('maxLength') - $headlineTextbox.val().length) + " characters remaining");
	$headlineTextbox.on('input propertychange keyup change', function(){
	    $("#headline-remaining-text").html(($headlineTextbox.attr('maxLength') - $headlineTextbox.val().length) + " characters remaining");
	});
	
	var $pitchTextbox = $('#wanawork_userbundle_adtype_pitch');
	$("#pitch-remaining-text").html(($pitchTextbox.attr('maxLength') - $pitchTextbox.val().length) + " characters remaining");
	$pitchTextbox.on('input propertychange keyup change', function(){
	    $("#pitch-remaining-text").html(($pitchTextbox.attr('maxLength') - $pitchTextbox.val().length) + " characters remaining");
	});
	
	// Setup preview functionality
	var $adBox = $('.ad-box');
	var $headline = $('#wanawork_userbundle_adtype_headline');
	var $pitch = $('#wanawork_userbundle_adtype_pitch');
	var $profession = $('#wanawork_userbundle_adtype_profession');
	var $industries = $('#wanawork_userbundle_adtype_industries');
	var $experience = $('#wanawork_userbundle_adtype_experience');
	var $positions = $('#wanawork_userbundle_adtype_positions');
	var $educationLevel = $('#wanawork_userbundle_adtype_educationLevel');
	var $locations = $('#wanawork_userbundle_adtype_locations');
	var $languages = $('#wanawork_userbundle_adtype_languages');
	
	var headlineFiller = function(){
		var val = $headline.val();
		if (!val) {
			val = 'Your headline';
		}
		$adBox.find('.headline').text(val);
	};
	$headline.on('keyup', headlineFiller);
	
	pitchFiller = function(){
		var val = $pitch.val();
		if (!val) {
			val = 'You Pitch';
		}
		$adBox.find('.pitch').text(val);
	};
	$pitch.on('keyup', pitchFiller);
	
	var professionFiller = function(){
		var val = $profession.find('option:selected').text();
		if ($profession.find('option:selected').val() == 0) {
			val = 'Your profession';
		}
		$adBox.find('.profession').text(val);
	};
	$profession.on('change', professionFiller);
	
	var industryFiller = function(){
		var $selectedOptions = $industries.find('option:selected');
		if ($selectedOptions.length > 0) {
			$adBox.find('.industry-separator').show();
			$adBox.find('.industries').show();
			var selectedText = [];
			$selectedOptions.each(function(){
				selectedText.push($(this).text());
			});
			$adBox.find('.industries').text(selectedText.join(', '));
		} else {
			$adBox.find('.industry-separator').hide();
			$adBox.find('.industries').hide();
		}
	};
	$industries.on('change', industryFiller);
	
	var experienceFiller = function(){
		var val = $experience.find('option:selected').text();
		if ($experience.find('option:selected').val() == 0) {
			val = 'Your';
		}
		$adBox.find('.experience-value').text(val);
	};
	$experience.on('change', experienceFiller);
	
	var positionsFiller = function(){
		$adBox.find('.position-list').empty();
		var optionCount = $positions.find('option:selected').length;
		
		if (optionCount > 0) {
			$positions.find('option:selected').each(function(index){
				$('<span />')
				  .addClass('position-item')
				  .addClass('item')
				  .text($(this).text())
				  .appendTo($adBox.find('.position-list'));
				if (index + 1 < optionCount) {
					$adBox.find('.position-list').append(', ');
				}
			});
		} else {
			$('<span />')
			  .addClass('position-item')
			  .addClass('item')
			  .text('Positions you can take')
			  .appendTo($adBox.find('.position-list'));
		}
	};
	$positions.on('change', positionsFiller);
	
	var educationFiller = function(){
		var val = $(this).find('option:selected').text();
		if (!val) {
			val = 'Your Education Level';
		}
		$adBox.find('.education-level').find('span').text(val);
	};
	$educationLevel.on('change', educationFiller);
	
	var locationFiller = function(){
		$adBox.find('.locations-list').empty();
		var optionCount = $locations.find('option:selected').length;
		
		if (optionCount > 0) {
			$locations.find('option:selected').each(function(index){
				$('<span />')
				  .addClass('location-item')
				  .addClass('item')
				  .text($(this).text())
				  .appendTo($adBox.find('.locations-list'));
				if (index + 1 < optionCount) {
					$adBox.find('.locations-list').append(', ');
				}
			});
		} else {
			$('<span />')
			  .addClass('location-item')
			  .addClass('item')
			  .text('Counties where you can work')
			  .appendTo($adBox.find('.locations-list'));
		}
		
	};
	$locations.on('change', locationFiller);
	
	var languageFiller = function(){
		$adBox.find('.languages-list').empty();
		var optionCount = $languages.find('option:selected').length;
		
		if (optionCount > 0) {
			$languages.find('option:selected').each(function(index){
				$('<span />')
				  .addClass('language-item')
				  .addClass('item')
				  .text($(this).text())
				  .appendTo($adBox.find('.languages-list'));
				if (index + 1 < optionCount) {
					$adBox.find('.languages-list').append(', ');
				}
			});
		} else {
			$('<span />')
			  .addClass('language-item')
			  .addClass('item')
			  .text('Languages you speak')
			  .appendTo($adBox.find('.languages-list'));
		}
		
	};
	$languages.on('change', languageFiller);
	
	$('.btn-preview').on('click', function(e){
		e.preventDefault();
		headlineFiller.call($headline.get(0));
		pitchFiller.call($pitch.get(0));
		professionFiller.call($profession.get(0));
		industryFiller.call($industries.get(0));
		positionsFiller.call($positions.get(0));
		experienceFiller.call($experience.get(0));
		positionsFiller.call($positions.get(0));
		educationFiller.call($educationLevel.get(0));
		locationFiller.call($locations.get(0));
		languageFiller.call($languages.get(0));
		
		$('.preview').fadeIn();
	});
	
	$('#hide-preview').on('click', function(e){
		e.preventDefault();
		$('.preview').fadeOut();
	});
	
	$('.change-cv').on('click', function(e){
		e.preventDefault();
		
		$('.cv-view-wrapper').slideUp(400, function(){
			$('.upload-wrapper').slideDown(200);
		});
	});
	
	$('.keep-existing-cv').on('click', function(e){
		e.preventDefault();
		$('.upload-wrapper').slideUp(400, function(){
			$('.cv-view-wrapper').slideDown(200);
		});
	});
	
	$('.view-cv').on('click', function(e){
		e.preventDefault();
		location.href = $(this).data('url');
	});
	
});
