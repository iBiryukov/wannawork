$(function(){
	$.jqplot.config.enablePlugins = true;
	
	var plots = {},
		$frequencySelector = $('#frequency-filter'),
		$monthsFilter = $('#months-filter'),
		$tooltipDiv = $('<div />').attr('id', 'chartpseudotooltip').css({
			'position': 'absolute',
			'z-index': 100
		}),
		monthNames = ["January", "February", "March", "April", "May", "June",
		              "July", "August", "September", "October", "November", "December"];
	
	$tooltipDiv.appendTo(document.body);
	
	$frequencySelector.on('click', 'button', function(){
		$(this).siblings().removeClass('active');
		$(this).addClass('active');
		drawGraphs();
	});
	
	$monthsFilter.on('click', 'button', function(){
		$(this).siblings().removeClass('active');
		$(this).addClass('active');
		drawGraphs();
	});
	
	function drawGraphs() {
		
		$('.views-graph').each(function(){
			var month = $monthsFilter.find('.active').data('month').split('-')[0],
				year = $monthsFilter.find('.active').data('month').split('-')[1],
				urlAddon = ('&month=' + month + '&year=' + year);
			
			var $this = $(this),
				url = $this.data('url') + urlAddon;
			$.get(url, function(data){
				if(data.length === 0) return;
				
				if($this.prop('id') in plots) {
					plots[$this.prop('id')].destroy();
					delete plots[$this.prop('id')];
				}
				var plot = $.jqplot($this.prop('id'), [data], jqplotOptions($this.data('xlabel'), $this.data('ylabel'), data));
				plots[$this.prop('id')] = plot;
			});
		});
		
	}
	
	function jqplotOptions(xLabel, yLabel, data) {
		var labels = [],
			yValues = [],
			minY = 0,
			maxY = 0,
			maxTicks = 10,
			y = null;
			
		for(var i = 0; i < data.length; i++) {
			y = data[i][1];
			labels.push(data[i][1]);
			
			if(yValues.indexOf(y) === -1) {
				yValues.push(y);
			}
		}
		
		return {
			seriesDefaults: {
				renderer: $.jqplot.BarRenderer,
				rendererOptions: {
	               // barPadding: 1,
	                barDirection: 'vertical'
	               // barWidth: 20
	            },
	            pointLabels: {
	            	show: true,
	            	labels: labels
	            }
			},
			axes: {
				yaxis: {
					showMinorTicks:true,
					label: yLabel,
					labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
					labelOptions: {
						angle: -90
					},
					tickOptions: {
						formatString:'%.0f'
					},
					rendererOptions: {
						forceTickAt0: true
					},
					numberTicks: yValues.length > maxTicks ? maxTicks : yValues.length
				},
				xaxis: {
					renderer: $.jqplot.CategoryAxisRenderer,
					label: xLabel,
					tickRenderer: $.jqplot.CanvasAxisTickRenderer,
					tickOptions: {
						angle: data.length > 15 ? -45 : 0,
						fontSize: '8pt'
					}
				}
			}
		};
	}
	
	function groupDataByDate(views, type) {
		return type;
//		var month = new String($monthsFilter.find("button.active").data("month"));
//		if(month.length === 1) {
//			month = "0" + month;
//		}
//		
//		var monthsGroupedViews = {};
//		for(var date in views) {
//			if(date.split("-")[1] === month) {
//				monthsGroupedViews[date] = views[date];
//			}
//		}
//		
//		views = monthsGroupedViews;
//		if(type !== undefined) {
//			for(var date in views) {
//				views[date] = _.filter(views[date], function(view){
//					return view.type === viewTypes.PDF;
//				});
//			}
//		}
//		
//		var data = {};
//		switch($frequencySelector.find('button.active').data('filter')) {
//		
//			case 'day':
//			default:
//				for(var date in views) {
//					var day = date.split('-')[2],
//						month = monthNames[parseInt(date.split('-')[1]) - 1];
//					data[day + ' ' + month] = views[date].length;
//				}
//				data = _.pairs(data);
//				break;
//				
//			case 'week':
//				
//				for(var date in views) {
//					var dateObj = new Date(date),
//						weekNumber = dateObj.getWeek(),
//						weekStartDate = firstDayOfWeek(weekNumber, dateObj.getFullYear()),
//						weekEndDate = new Date(weekStartDate);
//					
//					weekEndDate.setDate(weekStartDate.getDate() + 6);
//					var key = weekStartDate.getDate() + ' ' + monthNames[weekStartDate.getMonth()] + 
//								' - ' + weekEndDate.getDate() + ' ' + monthNames[weekEndDate.getMonth()];
//					
//					data[key] = views[date].length;
//				}
//				
//				data = _.pairs(data);
//				break;
//		}
//		return data;
	}
	
	drawGraphs();
	
});

