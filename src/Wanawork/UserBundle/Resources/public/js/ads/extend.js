$(function(){
	$('.plan-selection').click(function(e){
		e.preventDefault();
		if ($(this).hasClass('disabled')) {
			return;
		}
		
		var planType = $(this).data('type');
		$('#form_plan').val(planType);
		//$('#form_plan').parents('form').submit();
		
		$('.plansTable').addClass('non-selected');
		$(this).parents('.plansTable').removeClass('non-selected');
		
		//$('.plan-selection').removeClass('disabled');
		//$(this).addClass('disabled');
	});
	
	$('#pay-button').on('click', function(e){
		e.preventDefault();
		
		if(!$('#form_plan').val() || $('.non-selected').length >= 2) {
			alert("Please select your plan first");
		} else {
			$('#form_payment-method').val($(this).attr("id").replace('pay-', ''));
			$('#form_plan').parents('form').submit();
		}
	});
});