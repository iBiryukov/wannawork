$(function(){
	$('.bumps').on('click', '.bump-link', function(e){
		e.preventDefault();
		var link = $(this).prop('href'),
		    $bumpBlock = $(this).parents('.bumps');
		
		$.post(link, function(data, status, xhr){
			if(xhr.status === 201) {
				var bumpsLeft = xhr.getResponseHeader('ww_bumps_left');
				
				if(bumpsLeft > 0) {
					var text = bumpsLeft + ' bump' + (bumpsLeft === 1 ? '' : 's');
					$bumpBlock.find('.count').text(text);
				} else {
					$bumpBlock.slideUp();
				}
				
				$.jGrowl('Your ad is now at the top');
			} else {
				alert("Something went wrong. Please try again");
			}
		});
	});
});