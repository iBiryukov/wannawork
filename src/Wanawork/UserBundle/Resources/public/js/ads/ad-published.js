$(function(){
	
	$('#ad-holder').css("height", $('.ad-box').outerHeight());
	
	$('#upgrade-button').on('click', function(e){
		e.preventDefault();
		
		var url = $(this).attr('href');
		$badgeInput = $('.badge-input');
		$.post($badgeInput.data('url'), {badge: $badgeInput.val()}, function(){
	        location.href = url;
		});
	});
	
	$('.preview').on('click', function(e){
		e.preventDefault();
		
		var $this = $(this),
			$adBox = $('.ad-box');
		
		if (!$this.hasClass("active")) {
			$('.preview').removeClass("active");
			$this.addClass("active");
			
			
			if ($this.hasClass("standard")) {
				$adBox.removeClass('premium');
				$adBox.find('.badges').hide();
			} else {
				$adBox.find('.badges').show();
				$adBox.addClass('premium');
			}
			
		}
	});
});