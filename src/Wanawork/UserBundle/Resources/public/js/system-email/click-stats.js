$(function(){
	$('.view-details-link').click(function(e){
		e.preventDefault();
		
		var $tr = $(this).parents('tr'),
			$detailsTr = $tr.next();
		
		if($detailsTr.is(':hidden')) {
			$detailsTr.slideDown();
			$(this).text('Hide Details');
		} else {
			$detailsTr.slideUp();
			$(this).text('View Details');
		}
	});
});