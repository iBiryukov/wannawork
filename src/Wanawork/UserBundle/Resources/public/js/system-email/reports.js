$(function(){
	$('#date').change(function(e){
        var $this = $(this);
        var date = $this.val();
        var url = $(this).data('url');
        if(date) {
        	url += '/' + date;
        }
        location.href = url;
    });
});