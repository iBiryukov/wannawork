$(function(){
	CKEDITOR.replace( 'wanawork_userbundle_systememail_text', {
		fullPage: true,
		allowedContent: true
	} );
	
	$('#wanawork_userbundle_systememail_type').change(function(){
		if(confirm("Are you sure to change the email type. You current email will be lost")) {
			var urlBits = location.href.split('/');
			urlBits[urlBits.length - 1] = $(this).attr('value');
			var newUrl = urlBits.join('/');
			location.href = newUrl;
		}
	});
});