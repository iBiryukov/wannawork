$(function(){
	
	$('.cv-builder-button').on('click', function(){
		
	});
	
	// CV Builder
	
	$('.rows-wrapper').each(function(){
		if($(this).find('.empty-message').length === 0) {
			$(this).find('.new-row').show();
		}
		
		$(this).sortable({
			items: '> .cv-row',
			handle: '.move-handler',
			containment: this
		});
	});
	
	$('.masked-date').mask('99/9999');
	
	var $cvForm = $('#cv_form'),
		$educationWrapper = $('#education-wrapper'),
		$workExpWrapper = $('#work-experience-wrapper'),
		$referenceWrapper = $('#reference-wrapper'),
		dateRegex = /^(0[1-9]|1[0-2])\/((19\d{2})|(20\d{2}))$/,
		slideUpSpeed = 250,
		slideDownSpeed = 400;
	
	$cvForm.find('textarea').autosize();
	
	// Reset input ids on submit
	$cvForm.on('submit', function(){
		$(this).find('.rows-wrapper').each(function(){
			var i = 0;
			$(this).find('.cv-row').each(function(){
				$(this).find('input, select, textarea').each(function(){
					var name = $(this).attr('name');
					var result = /^.*(\[\d+\]).*/.exec(name);
					if(result.length === 2) {
						var search = result[1];
						var replace = '[' + i + ']';
						name = name.replace(search, replace);
						$(this).attr('name', name);
					}
				});
				++i;
			});
		});
	});
	
	// Validate & Save reference form
	$referenceWrapper.on('click', '.save-row', function(e){
		e.preventDefault();
		var $row = $(this).parents('.cv-row');
		validateReference($row);
	});
	
	function validateReference($row)
	{
		var $infoView = $row.find('.info-view'),
			$formView = $row.find('.form-view'),
			$companyInput = $formView.find('.company'),
			$contactInput = $formView.find('.contact-name'),
			$telephoneInput = $formView.find('.phone-number'),
			$emailInput = $formView.find('.email'),
			ok = true;
		
		$companyInput.next().remove();
		if(!$companyInput.val() || $companyInput.val().length === 0) {
			var $msg = createErrorMessage('Please enter the company name');
			$msg.insertAfter($companyInput);
			ok = false;
		}
		
		$contactInput.next().remove();
		if(!$contactInput.val() || $contactInput.val().length === 0) {
			var $msg = createErrorMessage('Please enter the contact name in the company');
			$msg.insertAfter($contactInput);
			ok = false;
		}
		
		$telephoneInput.next().remove();
		if(!$telephoneInput.val() || $telephoneInput.val().length === 0) {
			var $msg = createErrorMessage('Please enter the phone number for your referee');
			$msg.insertAfter($telephoneInput);
			ok = false;
		}
		
		$emailInput.next().remove();
		if(!$emailInput.val() || $emailInput.val().length === 0) {
			var $msg = createErrorMessage('Please enter the email for your referee');
			$msg.insertAfter($emailInput);
			ok = false;
		}
		
		if(ok === true) {
			$infoView.find('.contact-name').text($contactInput.val());
			$infoView.find('.company').text($companyInput.val());
			$infoView.find('.phone-number').text($telephoneInput.val());
			$infoView.find('.email').text($emailInput.val());
			
			$formView.slideUp(slideUpSpeed, function(){
				$infoView.slideDown(slideDownSpeed);
			});
		}
		
	}
	
	// Validate & Save education form
	$educationWrapper.on('click', '.save-row', function(e){
		e.preventDefault();
		var $row = $(this).parents('.cv-row');
		validateEducation($row);
	});
	
	function validateEducation($row)
	{
		var $infoView = $row.find('.info-view'),
			$formView = $row.find('.form-view'),
			$startDateInput = $formView.find('.education-start-date'),
			$finishDateInput = $formView.find('.education-finish-date'),
			$collegeInput = $formView.find('.education-college'),
			$courseInput = $formView.find('.education-course'),
			$awardInput = $formView.find('.education-award'),
			ok = true;
		
		$startDateInput.next().remove();
		if(!dateRegex.test($startDateInput.val())) {
			var $msg = createErrorMessage('Please specify when you started the course.');
			$msg.insertAfter($startDateInput);
			ok = false;
		}
		
		$finishDateInput.next().remove();
		if(!dateRegex.test($finishDateInput.val())) {
			var $msg = createErrorMessage('Please specify when you finished the course');
			$msg.insertAfter($finishDateInput);
			ok = false;
		}
		
		// Start & Finish Months ok?
		if(ok === true) {
			if(monthDiff($startDateInput.val(), $finishDateInput.val()) <= 0) {
				var $msg = createErrorMessage('Graduation date must be greater than commencement date');
				$msg.insertAfter($finishDateInput);
				ok = false;
			}
			
		}
		
		$collegeInput.next().remove();
		if(!$collegeInput.val() || $collegeInput.val().length === 0) {
			var $msg = createErrorMessage('Please enter the name of the institute you attended');
			$msg.insertAfter($collegeInput);
			ok = false;
		}
		
		$courseInput.next().remove();
		if(!$courseInput.val() || $courseInput.val().length === 0) {
			var $msg = createErrorMessage('Please enter the name of your course');
			$msg.insertAfter($courseInput);
			ok = false;
		}
		
		$awardInput.next().remove();
		if(!$awardInput.val() || $awardInput.val().length === 0) {
			var $msg = createErrorMessage('Please enter the award you received');
			$msg.insertAfter($awardInput);
			ok = false;
		}
		
		if(ok === true) {
			$infoView.find('.start').text($startDateInput.val());
			$infoView.find('.finish').text($finishDateInput.val());
			$infoView.find('.college').text($collegeInput.val());
			$infoView.find('.course').text($courseInput.val());
			$infoView.find('.award').text($awardInput.val());
			
			var diff = monthDiff($startDateInput.val(), $finishDateInput.val());
			var durationText = '';
			if(0 >= diff) {
				durationText = 'Less than a month';
			} else {
				var durationYears = parseInt(diff / 12),
					durationMonths = parseInt(diff % 12),
					durationTextBits = [];
				
				
				if(durationYears > 0) {
					durationTextBits.push(durationYears + ' year' + (durationYears === 1 ? '' : 's'));
				}
				
				if(durationMonths > 0) {
					durationTextBits.push(durationMonths + ' month' + (durationMonths != 1 ? 's':''));
				}
				durationText = durationTextBits.join(', ');
				
			}
			$infoView.find('.duration').text(durationText);
			
			$formView.slideUp(slideUpSpeed, function(){
				$infoView.slideDown(slideDownSpeed);
			});
		}
	}
	
	// Validate & save work experience row
	$workExpWrapper.on('click', '.save-row', function(e){
		e.preventDefault();
		var $row = $(this).parents('.cv-row');
		validateWorkExperience($row);
	});
	
	function validateWorkExperience($row)
	{
		var $infoView = $row.find('.info-view'),
			$formView = $row.find('.form-view'),
			$startDateInput = $formView.find('.work-experience-start'),
			$finishDateInput = $formView.find('.work-experience-finish'),
			$companyInput = $formView.find('.work-experience-company'),
			$positionInput = $formView.find('.work-experience-position'),
			$descriptionInput = $formView.find('.work-experience-description'),
			ok = true;
		
		$startDateInput.next().remove();
		if(!dateRegex.test($startDateInput.val())) {
			var $msg = createErrorMessage('Please specify when you started the job');
			$msg.insertAfter($startDateInput);
			ok = false;
		}
		
		$finishDateInput.parent().find('.error').remove();
		if($finishDateInput.val() !== undefined && $finishDateInput.val().length > 0 && 
				!dateRegex.test($finishDateInput.val())) {
			var $msg = createErrorMessage('Please specify when you left the job');
			$finishDateInput.parent().append($msg);
			ok = false;
		}
		
		// Start & Finish Months ok?
		if(ok === true && $finishDateInput.val() !== undefined) {
			if(monthDiff($startDateInput.val(), $finishDateInput.val()) <= 0) {
				var $msg = createErrorMessage('Finish date must be greater than commencement date');
				$msg.insertAfter($finishDateInput);
				ok = false;
			}
		}
		
		if($companyInput.val() === undefined || $companyInput.val().length === 0) {
			var $msg = createErrorMessage('Please enter the company name where you worked');
			$msg.insertAfter($companyInput);
			ok = false;
		}
		
		if($positionInput.val() === undefined || $positionInput.val().length === 0) {
			var $msg = createErrorMessage('Please enter the position you held in the company');
			$msg.insertAfter($positionInput);
			ok = false;
		}
		
		if($descriptionInput.val() === undefined || $descriptionInput.val().length === 0) {
			var $msg = createErrorMessage('Please provide small description of the position you held');
			$msg.insertAfter($descriptionInput);
			ok = false;
		}
		
		if(ok === true) {
			var finishDate = 'Current here';
			if($finishDateInput.val() !== undefined && $finishDateInput.val().length > 0) {
				finishDate = $finishDateInput.val();
				
				var diff = monthDiff($startDateInput.val(), $finishDateInput.val());
				var durationText = '';
				if(0 >= diff) {
					durationText = 'Less than a month';
				} else {
					var durationYears = parseInt(diff / 12),
						durationMonths = parseInt(diff % 12),
						durationTextBits = [];
					
					if(durationYears > 0) {
						durationTextBits.push(durationYears + ' year' + (durationYears === 1 ? '' : 's'));
					}
					
					if(durationMonths > 0) {
						durationTextBits.push(durationMonths + ' month' + (durationMonths != 1 ? 's':''));
					}
					durationText = durationTextBits.join(', ');
					
				}
				$infoView.find('.duration').text(durationText);
				
			} else {
				
			}
			$infoView.find('.start').text($startDateInput.val());
			$infoView.find('.finish').text(finishDate);
			$infoView.find('.position').text($positionInput.val());
			$infoView.find('.company').text($companyInput.val());
			$infoView.find('.description').text($descriptionInput.val());
			
			$formView.slideUp(slideUpSpeed, function(){
				$infoView.slideDown(slideDownSpeed);
			});
		}
	}
	
	function createErrorMessage(text)
	{
		return $('<div class="control-group error"><span class="help-inline">'+text+'</span></div>');
	}
	
	// global edit row button handler
	$cvForm.on('click', '.edit-row', function(e){
		e.preventDefault();
		var $row = $(this).parents('.cv-row'),
			$infoView = $row.find('.info-view'),
			$formView = $row.find('.form-view');
		
		$infoView.slideUp(slideUpSpeed, function(){
			$formView.slideDown(slideDownSpeed);
		})
	});
	
	// global delete row button handler
	$cvForm.on('click', '.delete-row', function(e){
		e.preventDefault();
		
		var $rowsWrapper = $(this).parents('.rows-wrapper'),
			minCount = parseInt($rowsWrapper.data('mincount'));
		
		if(minCount > 0 && $(this).parents('.cv-row').siblings('.cv-row').length === 0) {
			$(this).parents('.cv-row').find('input, textarea').val('');
			$(this).parents('.cv-row').find('.error').remove();
			
			if($(this).parents('.cv-row').find('.form-view').is(':hidden')) {
				var $that = $(this);
				$(this).parents('.cv-row').find('.info-view').slideUp(slideUpSpeed, function(){
					$that.parents('.cv-row').find('.form-view').slideDown(slideDownSpeed);
				});
			}
			
		} else {
			$(this).parents('.cv-row').slideUp(slideUpSpeed, function(){
				$(this).remove();
			});
		}
	});
	
	// global create new row handler
	$cvForm.on('click', '.new-row', function(e){
		e.preventDefault();
		var $rowsWrapper = $(this).parents('.rows-wrapper');
		var prototype = $rowsWrapper.data('prototype');
		
		if($rowsWrapper.data('index') === undefined) {
			var newIndex = $rowsWrapper.find('.new-row').length;
			$rowsWrapper.data('index', newIndex);
		}
		
		var index = parseInt($rowsWrapper.data('index'));
		$rowsWrapper.data('index', index + 1);
		
		var $newRow = $(prototype.replace(/__name__/g, index)).hide();
		$newRow.find('.masked-date').mask('99/9999');
		$newRow.find('textarea').autosize();
		$newRow.insertBefore($rowsWrapper.find('.new-row').last());
		
		if($rowsWrapper.find('.empty-message').length > 0) {
			$rowsWrapper.find('.empty-message').slideUp(slideDownSpeed, function(){
				$newRow.slideDown(slideUpSpeed);
			});
		} else {
			$newRow.slideDown(slideUpSpeed);
		}
		
		$rowsWrapper.find('.new-row').show();
	});
	
	function monthDiff(str1, str2)
	{
		var month1 = parseInt(str1.split('/')[0]),
			year1 = parseInt(str1.split('/')[1]),
			month2 = parseInt(str2.split('/')[0]),
			year2 = parseInt(str2.split('/')[1]);
		
		return month2 - month1 + ((year2 - year1) * 12);
	}
});