$(function(){
	var $dialog = $('#dialog').dialog({
			autoOpen: false,
			modal: true,
			width: 600,
			height: 500
		}),
	    $connectionsForm = $('#connections-form');
	
	// Edit entity
	$('.entity').on('click', 'a', function(e){
		e.preventDefault();
		var $this = $(this),
		    $entity = $this.parents('.entity');
		
		if ($entity.find('.input').css('visibility') === 'visible') {
			$entity.find('input').click();
			return;
		}
		
		$('.entity.selected').removeClass('selected');
		$entity.addClass('selected');
		
		$connectionsForm.data('entity', $entity.attr('id'));
		
		$.get($this.prop('href'), function(data){
			$connectionsForm.html(data).find('.chosen').chosen();
			postProcessEditForm.apply($('#connections-form').get(0));
			
			var selectedValues = $connectionsForm.find('.chosen').val() || [];
			var $thisEntityList = $this.parents('.entity-list');
			var $relatedEntityList = $('.entity-list').not($thisEntityList);
			$thisEntityList.find('.input').css('visibility', 'hidden');
			$relatedEntityList.find('.input').css('visibility', 'visible').each(function(){
				var $entity = $(this).parents('.entity'),
					$input = $(this).find('input'),
				    id = $entity.attr('id').split('-')[1];
				
				if(selectedValues.indexOf(id) >= 0) {
					$input.prop('checked', true);
				} else {
					$input.prop('checked', false);
				}
				
			});
			$relatedEntityList.parent().find('.entity-filter').show().find('.all').click();
		});
	});
	
	function postProcessEditForm()
	{
		$(this).find('#label_wanawork_userbundle_sector_jobs').parents('.control-group').hide();
		$(this).find('#wanawork_userbundle_sector_id').parents('.control-group').hide();
		
		$(this).find('#label_wanawork_userbundle_sectorjob_sectors').parents('.control-group').hide();
		$(this).find('#wanawork_userbundle_sectorjob_id').parents('.control-group').hide();
	}
	
	$connectionsForm.on('click', '.cancel', function(){
		$connectionsForm.empty();
		$('.input').css('visibility', 'hidden');
		$('.entity.selected').removeClass('selected');
		$('.entity-filter').find('.all').click();
		$('.entity-filter').hide();
	}).on('click', '.delete', function(e){
		e.preventDefault();
		e.stopPropagation();
		
		if(!confirm('Are you sure to delete this entity?')) {
			return;
		}
		
		var $form = $(this).parents('form');
		$.ajax({
			url: $form.prop('action'),
			data: $form.serialize(),
			type: $form.find('input[name="_method"]').val() || 'POST',
			success: function(data, status, xhr){
				if(xhr.status === 204) {
					$connectionsForm.find('.cancel').click();
					$.jGrowl('Entity deleted');
					var $entity = $('#' + $connectionsForm.data('entity'));
					$entity.remove();
				}
			}
		});
		
	}).on('submit', function(e){
		e.preventDefault();
		var $form = $(this).find('form');
		
		var $entityList = $('.entity-list').filter(function(){
			return $(this).find('.input').find('input').first().css('visibility') === 'visible';
		});
		
		var selectedIds = [];
		$entityList.find('.entity').filter(function(){
			return $(this).find('.input').find('input:checked').length > 0;
		}).each(function(){
			var id = parseInt($(this).attr('id').split('-')[1]);
			selectedIds.push(id);
		});
		
		$connectionsForm.find('.chosen').find('option').each(function(){
			var id = parseInt($(this).attr('value'));
			
			if(selectedIds.indexOf(id) >= 0) {
				$(this).attr('selected', 'selected');
			} else {
				$(this).removeAttr('selected');
			}
		});
		
		$.ajax({
			url: $form.prop('action'),
			data: $form.serialize(),
			type: $form.find('input[name="_method"]').val() || 'POST',
			success: function(data, status, xhr){
				if(xhr.status === 204) {
					var resourceUrl = xhr.getResponseHeader('ww_resource');
					onEntityChange(resourceUrl, $connectionsForm.data('entity'));
					$connectionsForm.find('.cancel').click();
					$.jGrowl('Entity updated');
				}
			}
		});
		
		return false;
	});
	
	
	function onEntityChange(resourceUrl, entityId)
	{
		var $entity = $('#' + entityId);

		$.get(resourceUrl, function(data){
			$entity.data('dialogtitle', data.name);
			$entity.find('.name').html(data.name);
			for(var key in data) {
				if($.isArray(data[key])) {
					$entity.find('.count').text(data[key].length);
					break;
				}
			}
		});
	}
	
	
	$('.entity-filter').on('click', 'button', function(e){
		var $this = $(this),
			$entityList = $this.parent().parent().find('.entity-list');
		
		if($this.hasClass('all')) {
			$entityList.find('.entity').each(function(){
				$(this).show();
			});
		} else if($(this).hasClass('selected')) {
			$entityList.find('.entity').each(function(){
				if($(this).find('input').is(':checked')) {
					$(this).show();
				} else {
					$(this).hide();
				}
			});
		} else if($(this).hasClass('not-selected')) {
			$entityList.find('.entity').each(function(){
				if($(this).find('input').is(':checked')) {
					$(this).hide();
				} else {
					$(this).show();
				}
			});
		}
	});
	
	$('.new-entity-link').on('click', function(e){
		e.preventDefault();
		$.get($(this).attr('href'), function(data){
			$dialog.html(data).dialog('open').find('.chosen').chosen();
		});
	});
	
	// Create/update entities
	$dialog.on('submit', function(e){
		var $this = $(this),
			$form = $this.find('form'), 
			method = ($form.find('input[name="_method"]').val() || 'POST').toUpperCase();
		
		$.ajax({
			type: method, 
			url: $form.prop('action'),
			success: function(data, status, xhr){
				if(xhr.status === 201) {
					$dialog.dialog('close');
					//onEntityChange(resourceUrl, $dialog.data('entity'));
					$.jGrowl('Entity created');
					location.reload();
				} else if(xhr.status === 200 && 'form' in data) {
					$dialog.html(data.form).find('.chosen').chosen();
					postProcessEditForm();
				}	
			},
			data: $form.serialize()
		});
		    
		return false;
	});
	
	function resize() {
		var windowHeight = $(window).height(),
			headerHeight = $('#header-wrapper').outerHeight(),
			otherHeight = $('.entity-list').first().parents('.row').first().siblings('.row').outerHeight(),
			entityListHeight = windowHeight -  headerHeight - otherHeight;
			
		$('.entity-list').height(entityListHeight * 0.8);
	}
	resize();
});