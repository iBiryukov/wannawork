$(function(){
	
	var $textarea = $('#form_tweet'),
		$charCounter = $('.char-counter'),
		maxChars = 140,
		computeRemainingChars = function() {
			var left = maxChars - $textarea.val().length;
			var text = null;
			
			if (left === 1) {
				text = left + " character left";
			} else {
				text = left + " characters left";
			}
			
			$charCounter.text(text);
		};
	
	
	computeRemainingChars();
	
	$textarea.keyup(function(){
		computeRemainingChars();
	});
	
	
});