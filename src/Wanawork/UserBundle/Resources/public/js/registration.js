$(function(){
	var $employerDescription = $('.employer-description'),
		$employerForm = $('.employer-form'),
		$employeeDescription = $('.employee-description'),
		$employeeForm = $('.employee-form');
	
	$("#link-employer").on('click', function(e){
		e.preventDefault();
		
		$employerDescription.hide();
		$employerForm.show();
		
		$employeeForm.hide();
		$employeeDescription.show();
	});
	
	$("#link-employee").on('click', function(e){
		e.preventDefault();
		
		$employerForm.hide();
		$employerDescription.show();
		
		$employeeDescription.hide();
		$employeeForm.show();
	});
});