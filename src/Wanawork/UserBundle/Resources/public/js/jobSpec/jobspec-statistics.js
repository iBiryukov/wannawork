$(function(){
	var dateFormat = 'Y-m-d H:i';

	$('#start-date').datetimepicker({
		format: dateFormat
	});

	$('#finish-date').datetimepicker({
		format: dateFormat
	});
	
	$('#profession-table').dynatable();
	$('#industry-table').dynatable();
	$('#location-table').dynatable();
	$('#position-table').dynatable();
	$('#education-table').dynatable();
	$('#language-table').dynatable();
	$('#experience-table').dynatable();
});


