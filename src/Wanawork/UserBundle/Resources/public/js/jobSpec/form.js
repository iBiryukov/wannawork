$(function(){
	CKEDITOR.replace( 'wanawork_userbundle_jobspec_description', {
		removePlugins: "elementspath",
		toolbarGroups: [
		        		{ name: 'clipboard',   groups: [ 'clipboard' ] },
		        		{ name: 'basicstyles', groups: [ 'basicstyles' ] },
		        		{ name: 'paragraph',   groups: [ 'list' ] }
		    		]
	} );
	$('#wanawork_userbundle_jobspec_description').show();
	
	$('#wanawork_userbundle_jobspec_profession').chosen();
	$('#wanawork_userbundle_jobspec_industries').chosen();
	$('#wanawork_userbundle_jobspec_positions').chosen();
	$('#wanawork_userbundle_jobspec_languages').chosen();
	$('#wanawork_userbundle_jobspec_locations').chosen();
	$('#wanawork_userbundle_jobspec_educationLevel').chosen();
	$('#wanawork_userbundle_jobspec_experience').chosen();
});