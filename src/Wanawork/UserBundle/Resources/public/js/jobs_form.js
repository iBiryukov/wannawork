$(function(){
	
	var stillHereCheckbox = $('#wanawork_userbundle_workexperiencetype_currently_here');
	stillHereCheckbox.change(function(){
		setupDateFields();
	});
	
	function setupDateFields() {
		if($(stillHereCheckbox).is(":checked")) {
			$('#wanawork_userbundle_workexperiencetype_finish').hide();
			$('#to_date_present').show();
		} else {
			$('#wanawork_userbundle_workexperiencetype_finish').show();
			$('#to_date_present').hide();
		}
	}
	setupDateFields();
	
	$('#job-form').submit(function(){
		if(stillHereCheckbox.is(":checked")) {
			$("#wanawork_userbundle_workexperiencetype_finish_day").val('');
			$("#wanawork_userbundle_workexperiencetype_finish_month").val('');
			$("#wanawork_userbundle_workexperiencetype_finish_year").val('');
		}
	});
	
});