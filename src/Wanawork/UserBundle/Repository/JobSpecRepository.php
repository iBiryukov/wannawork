<?php
namespace Wanawork\UserBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Wanawork\UserBundle\Entity\SectorJob;
use Wanawork\UserBundle\Entity\JobSpec;

class JobSpecRepository extends EntityRepository
{
    /**
     * Latest job date
     * @return \DateTime | null
     */
    public function findLatestJobDate(SectorJob $profession = null)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('MAX(job.createdOn) as date');
        $qb->from($this->getEntityName(), 'job');
        $qb->where('job.public=:public');
        
        if ($profession instanceof SectorJob) {
            $qb->andWhere('job.profession=:profession');
            $qb->setParameter('profession', $profession);
        }

        $qb->setParameter('public', true);
        
        $result = $qb->getQuery()->getOneOrNullResult();
        
        $date = null;
        if (is_array($result) && isset($result['date']) && strtotime($result['date']) !== false) {
            $date = new \DateTime($result['date']);
        }
        
        return $date;
    }
    
    public function findNumberofJobs($startDate, $finishDate)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
		$qb->select('count(jobSpec) as c');
		$qb->from($this->getEntityName(), 'jobSpec');
		if($startDate !== null) {
		    $qb->andWhere('jobSpec.createdOn >= :startDate');
            $qb->setParameter('startDate',$startDate);
		}
		if($endDate !== null) {
		    $qb->andWhere('jobSpec.createdOn < :endDate');
            $qb->setParameter('endDate',$endDate);
		}
		return $qb->getQuery()->getSingleScalarResult();
    }
    
    public function findMostUsedProfession($startDate, $finishDate)
    {
		$dql = "
		SELECT profession, (SELECT count(jobSpec) FROM WanaworkUserBundle:JobSpec jobSpec
	    WHERE jobSpec.profession=profession AND jobSpec.createdOn >= :start_date AND jobSpec.createdOn < :finish_date ) as c
		    
		FROM WanaworkUserBundle:SectorJob profession
		WHERE (SELECT count(jobSpec2) FROM WanaworkUserBundle:JobSpec jobSpec2
	    WHERE jobSpec2.profession=profession and jobSpec2.createdOn >= :start_date and jobSpec2.createdOn < :finish_date) > 0
		ORDER BY c DESC, profession.name ASC";

		$query = $this->getEntityManager()->createQuery($dql);
        $query->setParameters(array(
            'start_date'    =>   $startDate,
            'finish_date'   =>   $finishDate,
        ));
		return $query->getResult();
    }
    
    public function findMostUsedIndustry($startDate, $finishDate)
    {
        $dql = "
		SELECT industry, (SELECT count(jobSpec) FROM WanaworkUserBundle:JobSpec jobSpec
        JOIN jobSpec.industries sub_indt
	    WHERE sub_indt.id=industry.id AND jobSpec.createdOn >= :start_date AND jobSpec.createdOn < :finish_date) as c
            
		FROM WanaworkUserBundle:Sector industry
	    WHERE (SELECT count(jobSpec2) FROM WanaworkUserBundle:JobSpec jobSpec2 JOIN jobSpec2.industries sub_indt2 
	    WHERE sub_indt2.id=industry.id and jobSpec2.createdOn >= :start_date and jobSpec2.createdOn < :finish_date) > 0
		ORDER BY c DESC, industry.name ASC";

        $query = $this->getEntityManager()->createQuery($dql);
        $query->setParameters(array(
            'start_date'    =>   $startDate,
            'finish_date'   =>   $finishDate,
        ));
		return $query->getResult();
    }
    
    public function findMostUsedLocation($startDate, $finishDate)
    {

        $dql = "
		SELECT location, (SELECT count(jobSpec) FROM WanaworkUserBundle:JobSpec jobSpec
        JOIN jobSpec.locations sub_loc
	    WHERE sub_loc.id=location.id AND jobSpec.createdOn >= :start_date AND jobSpec.createdOn < :finish_date) as c
            
		FROM WanaworkMainBundle:County location
	    WHERE (SELECT count(jobSpec2) FROM WanaworkUserBundle:JobSpec jobSpec2 JOIN jobSpec2.locations sub_loc2 
	    WHERE sub_loc2.id=location.id and jobSpec2.createdOn >= :start_date and jobSpec2.createdOn < :finish_date) > 0
		ORDER BY c DESC, location.name ASC";

        $query = $this->getEntityManager()->createQuery($dql);
        $query->setParameters(array(
            'start_date'    =>   $startDate,
            'finish_date'   =>   $finishDate,
        ));
		return $query->getResult();
    }
    
    public function findMostUsedPosition($startDate, $finishDate)
    {
        $dql = "
		SELECT position, (SELECT count(jobSpec) FROM WanaworkUserBundle:JobSpec jobSpec
        JOIN jobSpec.positions sub_pos
	    WHERE sub_pos.id=position.id AND jobSpec.createdOn >= :start_date AND jobSpec.createdOn < :finish_date) as c
            
		FROM WanaworkUserBundle:PositionType position
	    WHERE (SELECT count(jobSpec2) FROM WanaworkUserBundle:JobSpec jobSpec2 JOIN jobSpec2.positions sub_pos2 
	    WHERE sub_pos2.id=position.id and jobSpec2.createdOn >= :start_date and jobSpec2.createdOn < :finish_date) > 0
		ORDER BY c DESC, position.name ASC";

		$query = $this->getEntityManager()->createQuery($dql);
        $query->setParameters(array(
            'start_date'    =>   $startDate,
            'finish_date'   =>   $finishDate,
        ));
		return $query->getResult();
    }
    
    public function findMostUsedEducationLevel($startDate, $finishDate)
    {
        $dql = "
		SELECT educationLevel, (SELECT count(jobSpec) FROM WanaworkUserBundle:JobSpec jobSpec
	    WHERE jobSpec.educationLevel=educationLevel AND jobSpec.createdOn >= :start_date AND jobSpec.createdOn < :finish_date ) as c
            
		FROM WanaworkUserBundle:Qualification educationLevel
		WHERE (SELECT count(jobSpec2) FROM WanaworkUserBundle:JobSpec jobSpec2
	    WHERE jobSpec2.educationLevel=educationLevel and jobSpec2.createdOn >= :start_date and jobSpec2.createdOn < :finish_date) > 0
		ORDER BY c DESC, educationLevel.name ASC";

		$query = $this->getEntityManager()->createQuery($dql);
        $query->setParameters(array(
            'start_date'    =>   $startDate,
            'finish_date'   =>   $finishDate,
        ));
		return $query->getResult();
    }
    
    public function findMostUsedLanguage($startDate, $finishDate)
    {
        $dql = "
		SELECT language, (SELECT count(jobSpec) FROM WanaworkUserBundle:JobSpec jobSpec
        JOIN jobSpec.languages sub_lang
	    WHERE sub_lang.id=language.id AND jobSpec.createdOn >= :start_date AND jobSpec.createdOn < :finish_date) as c
            
		FROM WanaworkMainBundle:Language language
	    WHERE (SELECT count(jobSpec2) FROM WanaworkUserBundle:JobSpec jobSpec2 JOIN jobSpec2.languages sub_lang2 
	    WHERE sub_lang2.id=language.id and jobSpec2.createdOn >= :start_date and jobSpec2.createdOn < :finish_date) > 0
		ORDER BY c DESC, language.name ASC";

		$query = $this->getEntityManager()->createQuery($dql);
        $query->setParameters(array(
            'start_date'    =>   $startDate,
            'finish_date'   =>   $finishDate,
        ));
		return $query->getResult();
    }
    
    public function findMostUsedExperience($startDate, $finishDate)
    {
        $dql = "
		SELECT experience, (SELECT count(jobSpec) FROM WanaworkUserBundle:JobSpec jobSpec
	    WHERE jobSpec.experience=experience AND jobSpec.createdOn >= :start_date AND jobSpec.createdOn < :finish_date ) as c
            
		FROM WanaworkUserBundle:SectorExperience experience
		WHERE (SELECT count(jobSpec2) FROM WanaworkUserBundle:JobSpec jobSpec2
	    WHERE jobSpec2.experience=experience and jobSpec2.createdOn >= :start_date and jobSpec2.createdOn < :finish_date) > 0
		ORDER BY c DESC, experience.name ASC";

		$query = $this->getEntityManager()->createQuery($dql);
        $query->setParameters(array(
            'start_date'    =>   $startDate,
            'finish_date'   =>   $finishDate,
        ));
		return $query->getResult();
    }
}