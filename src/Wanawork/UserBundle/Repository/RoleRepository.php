<?php
namespace Wanawork\UserBundle\Repository;
use Doctrine\ORM\EntityRepository;

class RoleRepository extends EntityRepository
{
    public function deleteWhereNotInId(array $ids)
    {
        $dql = "DELETE {$this->_entityName} e WHERE e.id NOT IN(:ids)";
        $query = $this->getEntityManager()->createQuery($dql);
        $query->setParameter('ids', $ids);
        return $query->execute();
    }
    
    /**
     * Get the next id
     * @return number
     */
    public function findNextId()
    {
        $dql = "SELECT MAX(s) FROM {$this->getEntityName()} s";
        $query = $this->getEntityManager()->createQuery($dql);
        $id = $query->getSingleScalarResult();
        if(!is_int($id)) {
            $id = 0;
        }
        return ($id + 1);
    }
	
}

