<?php
namespace Wanawork\UserBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Wanawork\UserBundle\Entity\UnsubscribedEmail;

class UnsubscribedEmailRepository extends EntityRepository
{
    /**
     * Delete the given email from unsubribers list
     * @param string $email
     * 
     * @return int  Count of affected rows
     */
    public function removeByEmail($email)
    {
        $unsub = $this->find($email);
        if ($unsub instanceof UnsubscribedEmail) {
            $this->getEntityManager()->remove($unsub);
            $this->getEntityManager()->flush();
            return 1;
        }
        return 0;
    }
}