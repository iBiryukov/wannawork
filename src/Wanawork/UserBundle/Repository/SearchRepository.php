<?php
namespace Wanawork\UserBundle\Repository;
use Wanawork\UserBundle\Entity\Search;
use Wanawork\MainBundle\Entity\NameTitle;

use Doctrine\ORM\EntityRepository;

class SearchRepository extends EntityRepository
{
    public function findMostUsedProfessions($startDate, $finishDate)
    {
        $dql = "
		SELECT profession, (SELECT count(search) FROM WanaworkUserBundle:Search search 
        WHERE search.profession=profession and search.date >= :start_date and search.date < :finish_date) as c
		FROM WanaworkUserBundle:SectorJob profession
		WHERE (SELECT count(search1) FROM WanaworkUserBundle:Search search1 
        WHERE search1.profession=profession and search1.date >= :start_date and search1.date < :finish_date) > 0
		ORDER BY c DESC, profession.name ASC";
    
        $query = $this->getEntityManager()->createQuery($dql);
        $query->setParameters(array(
            'start_date'    =>   $startDate,
            'finish_date'   =>   $finishDate,
        ));
        return $query->getResult();
    }
    
    public function findMostUsedIndustires($startDate, $finishDate)
    {
        $dql = "
		SELECT industry,
	    (SELECT count(search) FROM WanaworkUserBundle:Search search JOIN search.industries sub_indt1 
        WHERE sub_indt1.id=industry.id and search.date >= :start_date and search.date < :finish_date) as c
		FROM WanaworkUserBundle:Sector industry
	    WHERE (SELECT count(search1) FROM WanaworkUserBundle:Search search1 JOIN search1.industries sub_indt2 
        WHERE sub_indt2.id=industry.id and search1.date >= :start_date and search1.date < :finish_date) > 0
		ORDER BY c DESC, industry.name ASC";
         
        $query = $this->getEntityManager()->createQuery($dql);
        $query->setParameters(array(
            'start_date'    =>   $startDate,
            'finish_date'   =>   $finishDate,
        ));
        return $query->getResult();
    }

    public function findMostUsedLocation($startDate, $finishDate)
    {
        $dql = "
		SELECT location,
	    (SELECT count(search) FROM WanaworkUserBundle:Search search JOIN search.locations sub_loc1 
        WHERE sub_loc1.id=location.id and search.date >= :start_date and search.date < :finish_date) as c
		FROM WanaworkMainBundle:County location
	    WHERE (SELECT count(search1) FROM WanaworkUserBundle:Search search1 JOIN search1.locations sub_loc2 
        WHERE sub_loc2.id=location.id and search1.date >= :start_date and search1.date < :finish_date) > 0
		ORDER BY c DESC, location.name ASC";
         
        $query = $this->getEntityManager()->createQuery($dql);
        $query->setParameters(array(
            'start_date'    =>   $startDate,
            'finish_date'   =>   $finishDate,
        ));
        return $query->getResult();
    }
    
    public function findMostUsedLanguage($startDate, $finishDate)
    {
        $dql = "
		SELECT language,
	    (SELECT count(search) FROM WanaworkUserBundle:Search search JOIN search.languages sub_lang1 
        WHERE sub_lang1.id=language.id and search.date >= :start_date and search.date < :finish_date) as c
		FROM WanaworkMainBundle:Language language
	    WHERE (SELECT count(search1) FROM WanaworkUserBundle:Search search1 JOIN search1.languages sub_lang2 
        WHERE sub_lang2.id=language.id and search1.date >= :start_date and search1.date < :finish_date) > 0
		ORDER BY c DESC, language.name ASC";
         
        $query = $this->getEntityManager()->createQuery($dql);
        $query->setParameters(array(
            'start_date'    =>   $startDate,
            'finish_date'   =>   $finishDate,
        ));
        return $query->getResult();
    }
    
    public function findMostUsedEducationLevel($startDate, $finishDate)
    {
        $dql = "
		SELECT educationLevel, (SELECT count(search) FROM WanaworkUserBundle:Search search 
        WHERE search.educationLevel=educationLevel and search.date >= :start_date and search.date < :finish_date) as c
		FROM WanaworkUserBundle:Qualification educationLevel
		WHERE (SELECT count(search1) FROM WanaworkUserBundle:Search search1 
        WHERE search1.educationLevel=educationLevel and search1.date >= :start_date and search1.date < :finish_date) > 0
		ORDER BY c DESC, educationLevel.name ASC";
    
        $query = $this->getEntityManager()->createQuery($dql);
        $query->setParameters(array(
            'start_date'    =>   $startDate,
            'finish_date'   =>   $finishDate,
        ));
        return $query->getResult();
    }
    
    public function findMostUsedExperience($startDate, $finishDate)
    {
        $dql = "
		SELECT experience, (SELECT count(search) FROM WanaworkUserBundle:Search search 
        WHERE search.experience=experience and search.date >= :start_date and search.date < :finish_date) as c
		FROM WanaworkUserBundle:SectorExperience experience
		WHERE (SELECT count(search1) FROM WanaworkUserBundle:Search search1 
        WHERE search1.experience=experience and search1.date >= :start_date and search1.date < :finish_date) > 0
		ORDER BY c DESC, experience.name ASC";
    
        $query = $this->getEntityManager()->createQuery($dql);
        $query->setParameters(array(
            'start_date'    =>   $startDate,
            'finish_date'   =>   $finishDate,
        ));
        return $query->getResult();
    }
    
    public function findMostUsedRole($startDate, $finishDate)
    {
        $dql = "
		SELECT role,
	    (SELECT count(search) FROM WanaworkUserBundle:Search search JOIN search.positions sub_role1 
        WHERE sub_role1.id=role.id and search.date >= :start_date and search.date < :finish_date) as c
		FROM WanaworkUserBundle:PositionType role
	    WHERE (SELECT count(search1) FROM WanaworkUserBundle:Search search1 JOIN search1.positions sub_role2 
        WHERE sub_role2.id=role.id and search1.date >= :start_date and search1.date < :finish_date) > 0
		ORDER BY c DESC, role.name ASC";
         
        $query = $this->getEntityManager()->createQuery($dql);
        $query->setParameters(array(
            'start_date'    =>   $startDate,
            'finish_date'   =>   $finishDate,
        ));
        return $query->getResult();
    }
	
	public function getSearchesMade($startDate = null, $endDate = null)
	{

	    $qb = $this->getEntityManager()->createQueryBuilder();
	    $qb->select('count(search) as c');
	    $qb->from($this->getEntityName(), 'search');
	    if($startDate !== null) {
	        $qb->andWhere('search.date >= :startDate');
	        $qb->setParameter('startDate',$startDate);
	    }
	    if($endDate !== null) {
	        $qb->andWhere('search.date < :endDate');
	        $qb->setParameter('endDate',$endDate);
	    }
	    
		$qb2 = $this->getEntityManager()->createQueryBuilder();
		$qb2->select('count(search) as c');
		$qb2->from($this->getEntityName(), 'search');
		$qb2->join('search.user', 'user');
		$qb2->join('user.employeeProfile', 'profile');
		$qb2->andWhere('profile.title = :gender');
        $qb2->setParameter('gender', NameTitle::TITLE_MR);
		if($startDate !== null) {
		    $qb2->andWhere('search.date >= :startDate');
            $qb2->setParameter('startDate',$startDate);
		}
		if($endDate !== null) {
		    $qb2->andWhere('search.date < :endDate');
            $qb2->setParameter('endDate',$endDate);
		}
		
		$qb3 = $this->getEntityManager()->createQueryBuilder();
		$qb3->select('count(search) as c');
		$qb3->from($this->getEntityName(), 'search');
		$qb3->join('search.user', 'user');
		$qb3->join('user.employeeProfile', 'profile');
		$qb3->andWhere('profile.title in (:genders)');
		$qb3->setParameter('genders',array(NameTitle::TITLE_MS,NameTitle::TITLE_MISS,NameTitle::TITLE_MRS));
		if($startDate !== null) {
		    $qb3->andWhere('search.date >= :startDate');
            $qb3->setParameter('startDate',$startDate);
		}
		if($endDate !== null) {
		    $qb3->andWhere('search.date < :endDate');
            $qb3->setParameter('endDate',$endDate);
		}
		
		$qb4 = $this->getEntityManager()->createQueryBuilder();
		$qb4->select('count(search) as c');
		$qb4->from($this->getEntityName(), 'search');
		$qb4->join('search.user', 'user');
		$qb4->join('user.employeeProfile', 'profile');
		$qb4->andWhere('profile.title = :gender');
        $qb4->setParameter('gender', NameTitle::TITLE_DOCTOR);
		if($startDate !== null) {
		    $qb4->andWhere('search.date >= :startDate');
            $qb4->setParameter('startDate',$startDate);
		}
		if($endDate !== null) {
		    $qb4->andWhere('search.date < :endDate');
            $qb4->setParameter('endDate',$endDate);
		}
        
        return array(
            'totalSearches'     => $qb->getQuery()->getSingleScalarResult(),
            'searchesByMales'   => $qb2->getQuery()->getSingleScalarResult(),
            'searchesByFemales' => $qb3->getQuery()->getSingleScalarResult(),
            'searchesByDoctors' => $qb4->getQuery()->getSingleScalarResult(),
        );
	}
}
