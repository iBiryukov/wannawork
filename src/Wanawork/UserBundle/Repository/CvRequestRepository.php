<?php
namespace Wanawork\UserBundle\Repository;
use Wanawork\UserBundle\Entity\User;
use Wanawork\MainBundle\Entity\NameTitle;

use Doctrine\ORM\EntityRepository;

class CvRequestRepository extends EntityRepository
{
	
	public function getRequestsForUser(User $user, $status = null, $limit = null)
	{
		$qb = $this->createQueryBuilder('cvRequest');
		
		$qb->addSelect('userEr');
		$qb->addSelect('ad');
		$qb->addSelect('employerProfile');
		
		$qb->join('cvRequest.user', 'userEr');
		$qb->join('userEr.employerProfile', 'employerProfile');
		
		$qb->join('cvRequest.ad', 'ad');
		$qb->join('ad.cv', 'cv');
		$qb->join('cv.profile', 'employeeProfile');
		$qb->where('employeeProfile.user = ?0');
		$qb->setParameter(0, $user->getEmployeeProfile());
		
		if($status !== null) {
			$qb->andWhere('cvRequest.status = ?1');
			$qb->setParameter(1, $status);
		}
		
		if($limit !== null) {
			$qb->setMaxResults($limit);
		}
		
		$qb->orderBy('cvRequest.createDate', 'DESC');
		return $qb->getQuery()->getResult();
	}
	
	public function getCVsRequested($startDate = null, $endDate = null)
	{
		$qb = $this->getEntityManager()->createQueryBuilder();
		$qb->select('count(cvRequest) as c');
		$qb->from($this->getEntityName(), 'cvRequest');
		if($startDate !== null) {
		    $qb->andWhere('cvRequest.createDate >= :startDate');
            $qb->setParameter('startDate',$startDate);
		}
		if($endDate !== null) {
		    $qb->andWhere('cvRequest.createDate < :endDate');
            $qb->setParameter('endDate',$endDate);
		}
		
		$qb2 = $this->getEntityManager()->createQueryBuilder();
		$qb2->select('count(cvRequest) as c');
		$qb2->from($this->getEntityName(), 'cvRequest');
		$qb2->join('cvRequest.user', 'user');
		$qb2->join('user.employeeProfile', 'profile');
		$qb2->andWhere('profile.title = :gender');
        $qb2->setParameter('gender', NameTitle::TITLE_MR);
		if($startDate !== null) {
		    $qb2->andWhere('cvRequest.createDate >= :startDate');
            $qb2->setParameter('startDate',$startDate);
		}
		if($endDate !== null) {
		    $qb2->andWhere('cvRequest.createDate < :endDate');
            $qb2->setParameter('endDate',$endDate);
		}
		
		$qb3 = $this->getEntityManager()->createQueryBuilder();
		$qb3->select('count(cvRequest) as c');
		$qb3->from($this->getEntityName(), 'cvRequest');
		$qb3->join('cvRequest.user', 'user');
		$qb3->join('user.employeeProfile', 'profile');
		$qb3->andWhere('profile.title in (:genders)');
		$qb3->setParameter('genders',array(NameTitle::TITLE_MS,NameTitle::TITLE_MISS,NameTitle::TITLE_MRS));
		if($startDate !== null) {
		    $qb3->andWhere('cvRequest.createDate >= :startDate');
            $qb3->setParameter('startDate',$startDate);
		}
		if($endDate !== null) {
		    $qb3->andWhere('cvRequest.createDate < :endDate');
            $qb3->setParameter('endDate',$endDate);
		}
		
		$qb4 = $this->getEntityManager()->createQueryBuilder();
		$qb4->select('count(cvRequest) as c');
		$qb4->from($this->getEntityName(), 'cvRequest');
		$qb4->join('cvRequest.user', 'user');
		$qb4->join('user.employeeProfile', 'profile');
		$qb4->andWhere('profile.title = :gender');
        $qb4->setParameter('gender', NameTitle::TITLE_DOCTOR);
		if($startDate !== null) {
		    $qb4->andWhere('cvRequest.createDate >= :startDate');
            $qb4->setParameter('startDate',$startDate);
		}
		if($endDate !== null) {
		    $qb4->andWhere('cvRequest.createDate < :endDate');
            $qb4->setParameter('endDate',$endDate);
		}
		
		return array(
            'totalCvs'     => $qb->getQuery()->getSingleScalarResult(),
            'cvsByMales'   => $qb2->getQuery()->getSingleScalarResult(),
            'cvsByFemales' => $qb3->getQuery()->getSingleScalarResult(),
            'cvsByDoctors' => $qb4->getQuery()->getSingleScalarResult(),
        );
	}

}
