<?php
namespace Wanawork\UserBundle\Repository;
use Doctrine\ORM\EntityRepository;

class SectorJobRepository extends EntityRepository 
{
	public function findRandom()
	{
		$dql = "SELECT COUNT(c) FROM {$this->getEntityName()} c";
		$count = $this->getEntityManager()->createQuery($dql)
				->getSingleScalarResult();

		return $this->createQueryBuilder("c")->setMaxResults(1)
				->setFirstResult(rand(0, $count - 1))->getQuery()
				->getSingleResult();
	}
	
	/**
	 * Get the next id
	 * @return number
	 */
	public function findNextId()
	{
	    $dql = "SELECT MAX(s) FROM {$this->getEntityName()} s";
	    $query = $this->getEntityManager()->createQuery($dql);
	    $id = $query->getSingleScalarResult();
	    if(!is_int($id)) {
	        $id = 0;
	    }
	    return ($id + 1);
	}
	
	public function deleteWhereNotInId(array $ids)
	{
	    $dql = "DELETE {$this->_entityName} e WHERE e.id NOT IN(:ids)";
	    $query = $this->getEntityManager()->createQuery($dql);
	    $query->setParameter('ids', $ids);
	    return $query->execute();
	}
	
	public function findCandidateCountByProfession()
	{
	    $qb = $this->createQueryBuilder('profession');
	    
	    $qb->addSelect('COUNT(ad) as ad_count');
	    $qb->leftJoin('WanaworkUserBundle:Ad', 'ad', 'WITH', 
	        'profession.id=ad.profession AND ad.expiryDate >= :date AND ad.isPublished=:is_published');
	    $qb->orderBy('profession.name', 'ASC');
	    $qb->groupBy('profession');
	    
	    $qb->setParameters(array(
	    	'is_published' => true,
	        'date' => new \DateTime(),
	    ));
	    return $qb->getQuery()->getResult();
	}
	
	public function findJobsCountByProfession()
	{
	    $qb = $this->createQueryBuilder('profession');
	    $qb->addSelect('COUNT(job) as job_count');
	    $qb->leftJoin('WanaworkUserBundle:JobSpec', 'job', 'WITH', 'job.profession=profession.id and job.public=:public');
	    $qb->orderBy('profession.name', 'ASC');
	    $qb->groupBy('profession');
	    
	    $qb->setParameters(array(
	    	'public' => true,
	    ));
	    
	    return $qb->getQuery()->getResult();
	}
	
	public function findJobsWithoutAds($limit = null)
	{
	    $query = $this->getEntityManager()->createQuery(
	    	"SELECT DISTINCT(ad.profession) FROM WanaworkUserBundle:Ad ad"
	    );
	    
	    $professionIdsWithAds = $query->getResult();
	    
	    $qb = $this->createQueryBuilder('profession');
	    $qb->where('profession.id NOT IN(:ids)');
	    $qb->setParameter('ids', $professionIdsWithAds);
	    
	    if ($limit !== null && preg_match('/\d+/', $limit)) {
	        $qb->setMaxResults($limit);
	    }
	    
	    return $qb->getQuery()->getResult();
	}
	
	
}
