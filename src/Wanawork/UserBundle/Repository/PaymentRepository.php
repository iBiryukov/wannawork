<?php
namespace Wanawork\UserBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Wanawork\UserBundle\Entity\Billing\Payment;
use Wanawork\MainBundle\Entity\NameTitle;

class PaymentRepository extends EntityRepository
{
    /**
     * Get payments for dates
     * @return array
     */
    public function findDepositedPaymentsByMonth()
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        
        $qb->addSelect('SUM(ordr.amount) as amount');
        $qb->addSelect('SUM(ordr.vat) as vat');
        $qb->addSelect('SUM(payment.amount) as total');
        $qb->addSelect('YEAR(payment.updatedAt) as year');
        $qb->addSelect('MONTH(payment.updatedAt) as month');
        $qb->from($this->getEntityName(), 'payment');
        $qb->join('payment.order', 'ordr');
        $qb->where('payment.state=:state');
        $qb->groupBy('year, month');
        $qb->orderBy('year', 'DESC');
        $qb->addOrderBy('month', 'DESC');
        
        $qb->setParameters(array(
        	'state' => Payment::STATE_DEPOSITED,
        ));
        
        $data = $qb->getQuery()->getResult();
        
        foreach($data as &$row) {
            $row['month_name'] = date("F", mktime(0, 0, 0, $row['month'], 10));
        }
        return $data;
    }
    
    public function findPaymentsGroupedByAmount($startDate, $finishDate, $grouping = 'day')
    {
        $allowedGroupings = array('day', 'month');
        $qb = $this->getEntityManager()->createQueryBuilder();
        
        $qb->addSelect('payment.amount as amount');
        $qb->addSelect('(CASE WHEN payment.state=:state_paid THEN 1 ELSE 0 END) as is_paid');
        $qb->addSelect('count(payment) as payment_count');
        $qb->addSelect('MONTH(payment.createdAt) as month');
        $qb->addSelect('YEAR(payment.createdAt) as year');
        $qb->from($this->getEntityName(), 'payment');
        $qb->where(
            $qb->expr()->andX(
                'payment.createdAt >= :start_date',
                'payment.createdAt < :finish_date'
            )
        );
        
        $qb->andWhere('payment.source=:source');
        
        switch ($grouping) {
        	case 'day':
        	    $qb->addSelect('DAY(payment.createdAt) as day');
        	    $qb->groupBy('year, month, day, is_paid, amount');
        	    break;
        
        	case 'month':
        	    $qb->groupBy('year, month, is_paid, amount');
        	    break;
        
        	default:
        	    throw new \InvalidArgumentException(
        	       sprintf("Invalid grouping provided: '%s'. Valid values are: ", $grouping, implode(',', $allowedGroupings))
        	    );
        }
        
        
        $qb->orderBy('year', 'DESC');
        $qb->addOrderBy('month', 'DESC');
        if ($grouping === 'day') {
            $qb->addOrderBy('day', 'DESC');
        }
        $qb->addOrderBy('amount', 'ASC');
        
        $qb->setParameters(array(
            'start_date'    =>   $startDate,
            'finish_date'   =>   $finishDate,
            'state_paid'    =>   Payment::STATE_DEPOSITED,
            'source'        =>   Payment::SOURCE_PAYPAL,
        ));
        
        $data = $qb->getQuery()->getResult();
        
        $amounts = [];
        foreach ($data as $row) {
            if (!in_array($row['amount'], $amounts)) {
                $amounts[] = $row['amount'];
            }
        }
        sort($amounts);
        
        $groupedData = [];
        
        foreach ($data as $row) {
            $date = null;
            switch ($grouping) {
            	case 'day':
            	    $date = sprintf("%d %s %d", $row['day'], date("F", mktime(0, 0, 0, $row['month'], 10)), $row['year']);
            	    break;
            
            	case 'month':
            	    $date = sprintf("%s %d", date("F", mktime(0, 0, 0, $row['month'], 10)), $row['year']);
            	    break;
            
            	default:
            	    throw new \InvalidArgumentException(
            	       sprintf(
            	       "Invalid grouping provided: '%s'. Valid values are: ", $grouping, implode(',', $allowedGroupings)
            	       )
            	    );
            }
            
            if (!array_key_exists($date, $groupedData)) {
                $groupedData[$date] = array();
                foreach ($amounts as $amount) {
                    $groupedData[$date][$amount] = array(
                    	'paid' => 0,
                        'unpaid' => 0,
                    );
                }
            }
            
            $key = $row['is_paid'] === 1 ? 'paid' : 'unpaid';
            $groupedData[$date][$row['amount']][$key] += $row['payment_count'];
        }
        
        return $groupedData;
    }
    
    public function findPaymentsGroupByStatus($startDate, $finishDate, $grouping = 'day')
    {
        $allowedGroupings = array('day', 'month');
        $qb = $this->getEntityManager()->createQueryBuilder();
        
        $qb->addSelect('payment.state as state');
        $qb->addSelect('count(payment) as payment_count');
        $qb->addSelect('MONTH(payment.createdAt) as month');
        $qb->addSelect('YEAR(payment.createdAt) as year');
        $qb->addSelect('payment.source as source');
        
        $qb->from($this->getEntityName(), 'payment');
        $qb->where(
            $qb->expr()->andX(
                'payment.createdAt >= :start_date',
                'payment.createdAt < :finish_date'
            )
        );
        
        switch ($grouping) {
        	case 'day':
        	    $qb->addSelect('DAY(payment.createdAt) as day');
        	    $qb->groupBy('year, month, day, source, state');
        	    break;
        	     
        	case 'month':
        	    $qb->groupBy('year, month, source, state');
        	    break;
        	     
        	default:
        	    throw new \InvalidArgumentException(
        	    sprintf("Invalid grouping provided: '%s'. Valid values are: ", $grouping, implode(',', $allowedGroupings))
        	    );
        }
        
        $qb->orderBy('year', 'DESC');
        $qb->addOrderBy('month', 'DESC');
        if ($grouping === 'day') {
            $qb->addOrderBy('day', 'DESC');
        }
        
        $qb->addOrderBy('source', 'ASC');
        $qb->addOrderBy('state', 'ASC');
        
        $qb->setParameters(array(
            'start_date'    =>   $startDate,
            'finish_date'   =>   $finishDate,
        ));
        
        $stateTemplate = array();
        $states = array_values(Payment::$validStates);
        sort($states);
        foreach ($states as $state) {
            $stateTemplate[$state] = 0;        
        }
        
        $allSources = array_values(Payment::$validSources);
        sort($allSources);

        $data = $qb->getQuery()->getResult();
        $groupedData = array();
        foreach ($data as $row) {
            $date = null;
            switch ($grouping) {
            	case 'day':
            	    $date = sprintf("%d %s %d", $row['day'], date("F", mktime(0, 0, 0, $row['month'], 10)), $row['year']);
            	    break;
            
            	case 'month':
            	    $date = sprintf("%s %d", date("F", mktime(0, 0, 0, $row['month'], 10)), $row['year']);
            	    break;
            
            	default:
            	    throw new \InvalidArgumentException(
                	    sprintf(
                	    "Invalid grouping provided: '%s'. Valid values are: ", $grouping, implode(',', $allowedGroupings)
                	    )
            	    );
            }
            
            if (!array_key_exists($date, $groupedData)) {
                $groupedData[$date] = array();
                foreach ($allSources as $source) {
                    $groupedData[$date][$source] = $stateTemplate;
                }
            }
            
            $groupedData[$date][$row['source']][$row['state']] = $row['payment_count'];
        }

        foreach ($groupedData as $date => &$row) {
            $totalCount = [];
            foreach ($row as $source => &$rowData) {
                
                foreach ($rowData as $state => $count) {
                    if (!array_key_exists($state, $totalCount)) {
                        $totalCount[$state] = 0;
                    }
                    $totalCount[$state] += $count;
                }
            }
            $row['Total'] = $totalCount;
        }
        
        return $groupedData;
    }
    
    /**
     * Find all payments in the given date range
     * and group them by type (payment source)
     * 
     * @param string $startDate
     * @param string $endDate
     * @param string $grouping [day|month]
     * 
     * @return array
     */
    public function findPaymentsGroupByType($startDate, $finishDate, $grouping = 'day')
    {
        $allowedGroupings = array('day', 'month');
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        
        $qb->addSelect('(CASE WHEN payment.state=:state_paid THEN 1 ELSE 0 END) as is_paid');
        $qb->addSelect('payment.source as source');
        $qb->addSelect('count(payment) as payment_count');
        $qb->addSelect('MONTH(payment.createdAt) as month');
        $qb->addSelect('YEAR(payment.createdAt) as year');
        
        $qb->from($this->getEntityName(), 'payment');
        $qb->where(
        	$qb->expr()->andX(
        	   'payment.createdAt >= :start_date',
        	   'payment.createdAt < :finish_date'
            )
        );
        
        switch ($grouping) {
        	case 'day':
        	    $qb->addSelect('DAY(payment.createdAt) as day');
        	    $qb->groupBy('year, month, day, is_paid, source');
        	    break;
        	    
        	case 'month':
        	    $qb->groupBy('year, month, is_paid, source');
        	    break;
        	    
        	default:
        	    throw new \InvalidArgumentException(
        	       sprintf("Invalid grouping provided: '%s'. Valid values are: ", $grouping, implode(',', $allowedGroupings))
                );
        }
        
        $qb->orderBy('year', 'DESC');
        $qb->addOrderBy('month', 'DESC');
        if ($grouping === 'day') {
            $qb->addOrderBy('day', 'DESC');
        }
        
        $qb->addOrderBy('source', 'ASC');
        
        $qb->setParameters(array(
        	'start_date'    =>   $startDate,
            'finish_date'   =>   $finishDate,
            'state_paid'    =>   Payment::STATE_DEPOSITED,
        ));
        
        $data = $qb->getQuery()->getResult();
        $groupedData = array();
        $allSources = array_values(Payment::$validSources);
        sort($allSources);
        
        foreach ($data as $row) {
            $date = null;
            switch ($grouping) {
            	case 'day':
            	    $date = sprintf("%d %s %d", $row['day'], date("F", mktime(0, 0, 0, $row['month'], 10)), $row['year']);
            	    break;
            	     
            	case 'month':
            	    $date = sprintf("%s %d", date("F", mktime(0, 0, 0, $row['month'], 10)), $row['year']);
            	    break;
            	     
            	default:
            	    throw new \InvalidArgumentException(
            	       sprintf(
            	           "Invalid grouping provided: '%s'. Valid values are: ", $grouping, implode(',', $allowedGroupings)
                       )
            	    );
            }
            
            if (!array_key_exists($date, $groupedData)) {
                $groupedData[$date] = array();
                foreach ($allSources as $source) {
                    $groupedData[$date][$source] = array(
                    	'paid' => 0,
                        'unpaid' => 0,
                    );
                }
            }
            
            if ($row['is_paid']) {
                $groupedData[$date][$row['source']]['paid'] = $row['payment_count'];
            } else {
                $groupedData[$date][$row['source']]['unpaid'] = $row['payment_count'];
            }
            
            
        }

        return $groupedData;
    }
    

    public function getPaymentsMade($startDate = null, $endDate = null)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('count(payment) as c');
        $qb->from($this->getEntityName(), 'payment');
        $qb->where('payment.state=:state');
        $qb->setParameter('state', Payment::STATE_DEPOSITED);
        if($startDate !== null) {
            $qb->andWhere('payment.createdAt >= :startDate');
            $qb->setParameter('startDate',$startDate);
        }
        if($endDate !== null) {
            $qb->andWhere('payment.createdAt < :endDate');
            $qb->setParameter('endDate',$endDate);
        }
        
        return $qb->getQuery()->getSingleScalarResult();
    }
    
    public function getPayPalPayments($startDate = null, $endDate = null)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('count(payment) as c');
        $qb->from($this->getEntityName(), 'payment');
        $qb->where('payment.source = :source');
        $qb->setParameter('source', Payment::SOURCE_PAYPAL);
        $qb->andWhere('payment.state=:state');
        $qb->setParameter('state', Payment::STATE_DEPOSITED);
        if($startDate !== null) {
            $qb->andWhere('payment.createdAt >= :startDate');
            $qb->setParameter('startDate',$startDate);
        }
        if($endDate !== null) {
            $qb->andWhere('payment.createdAt < :endDate');
            $qb->setParameter('endDate',$endDate);
        }
        
        return $qb->getQuery()->getSingleScalarResult();
    }
    

    public function getVoucherPayments($startDate = null, $endDate = null)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('count(payment) as c');
        $qb->from($this->getEntityName(), 'payment');
        $qb->where('payment.source=:source');
        $qb->setParameter('source', Payment::SOURCE_VOUCHER);
        $qb->andWhere('payment.state=:state');
        $qb->setParameter('state', Payment::STATE_DEPOSITED);
        if($startDate !== null) {
            $qb->andWhere('payment.createdAt >= :startDate');
            $qb->setParameter('startDate',$startDate);
        }
        if($endDate !== null) {
            $qb->andWhere('payment.createdAt < :endDate');
            $qb->setParameter('endDate',$endDate);
        }
        
        return $qb->getQuery()->getSingleScalarResult();
    }
    

    public function getTwitterPayments($startDate = null, $endDate = null)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('count(payment) as c');
        $qb->from($this->getEntityName(), 'payment');
        $qb->where('payment.source = :source');
        $qb->setParameter('source', Payment::SOURCE_TWITTER);
        
        $qb->andWhere('payment.state=:state');
        $qb->setParameter('state', Payment::STATE_DEPOSITED);
        
        if($startDate !== null) {
            $qb->andWhere('payment.createdAt >= :startDate');
            $qb->setParameter('startDate',$startDate);
        }
    
        if($endDate !== null) {
            $qb->andWhere('payment.createdAt < :endDate');
            $qb->setParameter('endDate',$endDate);
        }
        
        return $qb->getQuery()->getSingleScalarResult();
    }
}
