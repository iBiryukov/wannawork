<?php
namespace Wanawork\UserBundle\Repository;
use Wanawork\UserBundle\Entity\Search;
use Wanawork\UserBundle\Entity\CV;
use JMS\DiExtraBundle\Annotation as DI;
use Wanawork\UserBundle\Entity\EmployeeProfile;
use Wanawork\UserBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Psr\Log\LoggerInterface;
use Wanawork\UserBundle\Entity\SectorJob;
use Wanawork\MainBundle\Entity\NameTitle;

class AdRepository extends EntityRepository
{
	
    /**
     * Logger
     * @var \Psr\Log\LoggerInterfac
     */
	private $logger;
	
	/**
	 * Find 
	 * @param array $CVs
	 */
	public function findAdsByUser(User $user)
	{
		$profile = $user->getEmployeeProfile();
		if(!$profile instanceof EmployeeProfile) {
			$this->logger->warn("User {$user->getId()} tried to find ads, but doesn't have an employee profile");
			return array();
		}
		
		$qb = $this->createQueryBuilder('ad');
		
		$qb->addSelect('profession');
		$qb->leftJoin('ad.profession', 'profession');
		
		$qb->addSelect('experience');
		$qb->leftJoin('ad.experience', 'experience');
		
		$qb->addSelect('industries');
		$qb->leftJoin('ad.industries', 'industries');
		
		$qb->addSelect('positions');
		$qb->leftJoin('ad.positions', 'positions');		
		
		$qb->addSelect('locations');
		$qb->leftJoin('ad.locations', 'locations');
		
		$qb->addSelect('languages');
		$qb->leftJoin('ad.languages', 'languages');
		
		$qb->addSelect('accessRequests');
		$qb->leftJoin('ad.accessRequests', 'accessRequests');
		
		$qb->addSelect('educationLevel');
		$qb->leftJoin('ad.educationLevel', 'educationLevel');
		
		$qb->addSelect('cv');
		$qb->leftJoin('ad.cv', 'cv');
		
		$qb->where('ad.profile = :profile');
		$qb->setParameter('profile', $profile);
		
		$qb->orderBy('ad.createdAt', 'desc');
		return $qb->getQuery()->getResult();
	}
	
	/**
	 * Inject the logger
	 * @param LoggerInterface $logger
	 * @DI\InjectParams({
	 * 	"logger" = @DI\Inject("logger")
	 * })
	 */
	public function setLogger(LoggerInterface $logger)
	{
		$this->logger = $logger;
	}
	
	public function findMostUsedProfessions($startDate, $finishDate)
	{
	   $dql = "
		SELECT profession, (SELECT count(ad) FROM WanaworkUserBundle:Ad ad 
	    WHERE ad.profession=profession and ad.createdAt >= :start_date and ad.createdAt < :finish_date ) as c,
	       
	    profession, (SELECT count(maleAds) FROM WanaworkUserBundle:Ad maleAds JOIN maleAds.profile pM
	    WHERE maleAds.profession=profession and maleAds.createdAt >= :start_date and maleAds.createdAt < :finish_date 
		and pM.title = 1) as m,
	       
	    profession, (SELECT count(femaleAds) FROM WanaworkUserBundle:Ad femaleAds JOIN femaleAds.profile pF
	    WHERE femaleAds.profession=profession and femaleAds.createdAt >= :start_date and femaleAds.createdAt < :finish_date 
		and pF.title >= 2 and pF.title < 5) as f,
	       
	    profession, (SELECT count(doctorAds) FROM WanaworkUserBundle:Ad doctorAds JOIN doctorAds.profile pD
	    WHERE doctorAds.profession=profession and doctorAds.createdAt >= :start_date and doctorAds.createdAt < :finish_date 
		and pD.title = 5) as d
	       
		FROM WanaworkUserBundle:SectorJob profession
		WHERE (SELECT count(ad1) FROM WanaworkUserBundle:Ad ad1
	    WHERE ad1.profession=profession and ad1.createdAt >= :start_date and ad1.createdAt < :finish_date) > 0
		ORDER BY c DESC, profession.name ASC";
	   
		$query = $this->getEntityManager()->createQuery($dql);
        $query->setParameters(array(
            'start_date'    =>   $startDate,
            'finish_date'   =>   $finishDate,
        ));
		return $query->getResult();
	}
	
	public function findMostUsedIndustires($startDate, $finishDate)
	{
	    $dql = "
		SELECT industry, (SELECT count(ad) FROM WanaworkUserBundle:Ad ad JOIN ad.industries sub_indt1 
	    WHERE sub_indt1.id=industry.id and ad.createdAt >= :start_date and ad.createdAt < :finish_date ) as c,
	       
	    industry, (SELECT count(maleAds) FROM WanaworkUserBundle:Ad maleAds JOIN maleAds.profile pM JOIN maleAds.industries sub_indt2 
	    WHERE sub_indt2.id=industry.id and maleAds.createdAt >= :start_date and maleAds.createdAt < :finish_date 
		and pM.title = 1) as m,
	       
	    industry, (SELECT count(femaleAds) FROM WanaworkUserBundle:Ad femaleAds JOIN femaleAds.profile pF JOIN femaleAds.industries sub_indt3 
	    WHERE sub_indt3.id=industry.id and femaleAds.createdAt >= :start_date and femaleAds.createdAt < :finish_date 
		and pF.title >= 2 and pF.title < 5) as f,
	       
	    industry, (SELECT count(doctorAds) FROM WanaworkUserBundle:Ad doctorAds JOIN doctorAds.profile pD JOIN doctorAds.industries sub_indt4 
	    WHERE sub_indt4.id=industry.id and doctorAds.createdAt >= :start_date and doctorAds.createdAt < :finish_date 
		and pD.title = 5) as d
	       
		FROM WanaworkUserBundle:Sector industry
	    WHERE (SELECT count(ad1) FROM WanaworkUserBundle:Ad ad1 JOIN ad1.industries sub_indt5 
	    WHERE sub_indt5.id=industry.id and ad1.createdAt >= :start_date and ad1.createdAt < :finish_date) > 0
		ORDER BY c DESC, industry.name ASC";
	    
	    $query = $this->getEntityManager()->createQuery($dql);
        $query->setParameters(array(
            'start_date'    =>   $startDate,
            'finish_date'   =>   $finishDate,
        ));
	    return $query->getResult();
	}
	
	public function findMostUsedLocation($startDate, $finishDate)
	{
	    $dql = "
		SELECT location, (SELECT count(ad) FROM WanaworkUserBundle:Ad ad JOIN ad.locations sub_loc1 
	    WHERE sub_loc1.id=location.id and ad.createdAt >= :start_date and ad.createdAt < :finish_date ) as c,
	       
	    location, (SELECT count(maleAds) FROM WanaworkUserBundle:Ad maleAds JOIN maleAds.profile pM JOIN maleAds.locations sub_loc2 
	    WHERE sub_loc2.id=location.id and maleAds.createdAt >= :start_date and maleAds.createdAt < :finish_date 
		and pM.title = 1) as m,
	       
	    location, (SELECT count(femaleAds) FROM WanaworkUserBundle:Ad femaleAds JOIN femaleAds.profile pF JOIN femaleAds.locations sub_loc3
	    WHERE sub_loc3.id=location.id and femaleAds.createdAt >= :start_date and femaleAds.createdAt < :finish_date 
		and pF.title >= 2 and pF.title < 5) as f,
	       
	    location, (SELECT count(doctorAds) FROM WanaworkUserBundle:Ad doctorAds JOIN doctorAds.profile pD JOIN doctorAds.locations sub_loc4 
	    WHERE sub_loc4.id=location.id and doctorAds.createdAt >= :start_date and doctorAds.createdAt < :finish_date 
		and pD.title = 5) as d
	       
		FROM WanaworkMainBundle:County location
	    WHERE (SELECT count(ad1) FROM WanaworkUserBundle:Ad ad1 JOIN ad1.locations sub_loc5 
	    WHERE sub_loc5.id=location.id and ad1.createdAt >= :start_date and ad1.createdAt < :finish_date) > 0
		ORDER BY c DESC, location.name ASC";
	    
	    $query = $this->getEntityManager()->createQuery($dql);
        $query->setParameters(array(
            'start_date'    =>   $startDate,
            'finish_date'   =>   $finishDate,
        ));
	    return $query->getResult();
	}
	
	public function findMostUsedLanguage($startDate, $finishDate)
	{
	    $dql = "
		SELECT language, (SELECT count(ad) FROM WanaworkUserBundle:Ad ad JOIN ad.languages sub_lang1 
	    Where sub_lang1.id=language.id and ad.createdAt >= :start_date and ad.createdAt < :finish_date ) as c,
	       
	    language, (SELECT count(maleAds) FROM WanaworkUserBundle:Ad maleAds JOIN maleAds.profile pM JOIN maleAds.languages sub_lang2 
	    Where sub_lang2.id=language.id and maleAds.createdAt >= :start_date and maleAds.createdAt < :finish_date 
		and pM.title = 1) as m,
	       
	    language, (SELECT count(femaleAds) FROM WanaworkUserBundle:Ad femaleAds JOIN femaleAds.profile pF JOIN femaleAds.languages sub_lang3 
	    Where sub_lang3.id=language.id and femaleAds.createdAt >= :start_date and femaleAds.createdAt < :finish_date 
		and pF.title >= 2 and pF.title < 5) as f,
	       
	    language, (SELECT count(doctorAds) FROM WanaworkUserBundle:Ad doctorAds JOIN doctorAds.profile pD JOIN doctorAds.languages sub_lang4 
	    WHERE sub_lang4.id=language.id and doctorAds.createdAt >= :start_date and doctorAds.createdAt < :finish_date 
		and pD.title = 5) as d
	       
		FROM WanaworkMainBundle:Language language
	    WHERE (SELECT count(ad1) FROM WanaworkUserBundle:Ad ad1 JOIN ad1.languages sub_lang5 
	    WHERE sub_lang5.id=language.id and ad1.createdAt >= :start_date and ad1.createdAt < :finish_date) > 0
		ORDER BY c DESC, language.name ASC";
	    
	    $query = $this->getEntityManager()->createQuery($dql);
        $query->setParameters(array(
            'start_date'    =>   $startDate,
            'finish_date'   =>   $finishDate,
        ));
	    return $query->getResult();
	}
	
	public function findMostUsedEducationLevel($startDate, $finishDate)
	{
	   $dql = "
		SELECT educationLevel, (SELECT count(ad) FROM WanaworkUserBundle:Ad ad 
	    WHERE ad.educationLevel=educationLevel and ad.createdAt >= :start_date and ad.createdAt < :finish_date) as c,
	       
	    educationLevel, (SELECT count(maleAds) FROM WanaworkUserBundle:Ad maleAds JOIN maleAds.profile pM
	    WHERE maleAds.educationLevel=educationLevel and maleAds.createdAt >= :start_date and maleAds.createdAt < :finish_date 
		and pM.title = 1) as m,
	       
	    educationLevel, (SELECT count(femaleAds) FROM WanaworkUserBundle:Ad femaleAds JOIN femaleAds.profile pF
	    WHERE femaleAds.educationLevel=educationLevel and femaleAds.createdAt >= :start_date and femaleAds.createdAt < :finish_date 
		and pF.title >= 2 and pF.title < 5) as f,
	       
	    educationLevel, (SELECT count(doctorAds) FROM WanaworkUserBundle:Ad doctorAds JOIN doctorAds.profile pD
	    WHERE doctorAds.educationLevel=educationLevel and doctorAds.createdAt >= :start_date and doctorAds.createdAt < :finish_date 
		and pD.title = 5) as d
	       
		FROM WanaworkUserBundle:Qualification educationLevel
		WHERE (SELECT count(ad1) FROM WanaworkUserBundle:Ad ad1 
	    WHERE ad1.educationLevel=educationLevel and ad1.createdAt >= :start_date and ad1.createdAt < :finish_date) > 0
		ORDER BY c DESC, educationLevel.name ASC";
		
		$query = $this->getEntityManager()->createQuery($dql);
        $query->setParameters(array(
            'start_date'    =>   $startDate,
            'finish_date'   =>   $finishDate,
        ));
		return $query->getResult();
	}
	
	public function findMostUsedExperience($startDate, $finishDate)
	{
	   $dql = "
		SELECT experience, (SELECT count(ad) FROM WanaworkUserBundle:Ad ad 
	    WHERE ad.experience=experience and ad.createdAt >= :start_date and ad.createdAt < :finish_date) as c,
	       
	    experience, (SELECT count(maleAds) FROM WanaworkUserBundle:Ad maleAds JOIN maleAds.profile pM
	    WHERE maleAds.experience=experience and maleAds.createdAt >= :start_date and maleAds.createdAt < :finish_date 
		and pM.title = 1) as m,
	       
	    experience, (SELECT count(femaleAds) FROM WanaworkUserBundle:Ad femaleAds JOIN femaleAds.profile pF
	    WHERE femaleAds.experience=experience and femaleAds.createdAt >= :start_date and femaleAds.createdAt < :finish_date 
		and pF.title >= 2 and pF.title < 5) as f,
	       
	    experience, (SELECT count(doctorAds) FROM WanaworkUserBundle:Ad doctorAds JOIN doctorAds.profile pD
	    WHERE doctorAds.experience=experience and doctorAds.createdAt >= :start_date and doctorAds.createdAt < :finish_date 
		and pD.title = 5) as d
	       
		FROM WanaworkUserBundle:SectorExperience experience
		WHERE (SELECT count(ad1) FROM WanaworkUserBundle:Ad ad1 
	    WHERE ad1.experience=experience and ad1.createdAt >= :start_date and ad1.createdAt < :finish_date) > 0
		ORDER BY c DESC, experience.name ASC";
		
		$query = $this->getEntityManager()->createQuery($dql);
        $query->setParameters(array(
            'start_date'    =>   $startDate,
            'finish_date'   =>   $finishDate,
        ));
		return $query->getResult();
	}
	
	public function findMostUsedRole($startDate, $finishDate)
	{
	    $dql = "
		SELECT role, (SELECT count(ad) FROM WanaworkUserBundle:Ad ad JOIN ad.positions sub_role1 
	    WHERE sub_role1.id=role.id and ad.createdAt >= :start_date and ad.createdAt < :finish_date) as c,
	       
	    role, (SELECT count(maleAds) FROM WanaworkUserBundle:Ad maleAds JOIN maleAds.profile pM JOIN maleAds.positions sub_role2 
	    WHERE sub_role2.id=role.id and maleAds.createdAt >= :start_date and maleAds.createdAt < :finish_date 
		and pM.title = 1) as m,
	       
	    role, (SELECT count(femaleAds) FROM WanaworkUserBundle:Ad femaleAds JOIN femaleAds.profile pF JOIN femaleAds.positions sub_role3 
	    WHERE sub_role3.id=role.id and femaleAds.createdAt >= :start_date and femaleAds.createdAt < :finish_date 
		and pF.title >= 2 and pF.title < 5) as f,
	       
	    role, (SELECT count(doctorAds) FROM WanaworkUserBundle:Ad doctorAds JOIN doctorAds.profile pD JOIN doctorAds.positions sub_role4 
	    WHERE sub_role4.id=role.id and doctorAds.createdAt >= :start_date and doctorAds.createdAt < :finish_date 
		and pD.title = 5) as d
	       
		FROM WanaworkUserBundle:PositionType role
	    WHERE (SELECT count(ad1) FROM WanaworkUserBundle:Ad ad1 JOIN ad1.positions sub_role5 
	    WHERE sub_role5.id=role.id and ad1.createdAt >= :start_date and ad1.createdAt < :finish_date ) > 0
		ORDER BY c DESC, role.name ASC";
	    
	    $query = $this->getEntityManager()->createQuery($dql);
        $query->setParameters(array(
            'start_date'    =>   $startDate,
            'finish_date'   =>   $finishDate,
        ));
	    return $query->getResult();
	}
	
	/**
	 * Find the date of the most recent paid ad
	 * @return \DateTime | NULL
	 */
	public function findRecentPaidAdDate(SectorJob $profession = null)
	{
	    $qb = $this->getEntityManager()->createQueryBuilder();
	    $qb->select('MAX(ad.createdAt) as date');
	    $qb->from($this->getEntityName(), 'ad');
	    $qb->where('ad.isPublished=:published');
	    $qb->setParameter('published', true);
	    
	    if ($profession instanceof SectorJob) {
	        $qb->andWhere('ad.profession=:profession');
	        $qb->setParameter('profession', $profession);
	    }
	    
	    $data = $qb->getQuery()->getSingleResult();

	    $date = null;
	    if (sizeof($data) && isset($data['date']) && strtotime($data['date']) !== false) {
	       $date = new \DateTime($data['date']);    
	    }
	    
	    return $date;
    }
	
	public function getAdsPosted($startDate = null, $endDate = null)
	{
		$qb = $this->getEntityManager()->createQueryBuilder();
		$qb->select('count(ad) as c');
		$qb->from($this->getEntityName(), 'ad');
		if($startDate !== null) {
		    $qb->andWhere('ad.createdAt >= :startDate');
            $qb->setParameter('startDate',$startDate);
		}
		if($endDate !== null) {
		    $qb->andWhere('ad.createdAt < :endDate');
            $qb->setParameter('endDate',$endDate);
		}
		
		$qb2 = $this->getEntityManager()->createQueryBuilder();
		$qb2->select('count(ad) as m');
		$qb2->from($this->getEntityName(), 'ad');
		$qb2->join('ad.profile', 'profile');
		$qb2->andWhere('profile.title = :gender');
        $qb2->setParameter('gender', NameTitle::TITLE_MR);
		if($startDate !== null) {
		    $qb2->andWhere('ad.createdAt >= :startDate');
            $qb2->setParameter('startDate',$startDate);
		}
		if($endDate !== null) {
		    $qb2->andWhere('ad.createdAt < :endDate');
            $qb2->setParameter('endDate',$endDate);
		}
		
		$qb3 = $this->getEntityManager()->createQueryBuilder();
		$qb3->select('count(ad) as m');
		$qb3->from($this->getEntityName(), 'ad');
		$qb3->join('ad.profile', 'profile');
		$qb3->andWhere('profile.title in (:genders)');
		$qb3->setParameter('genders',array(NameTitle::TITLE_MS,NameTitle::TITLE_MISS,NameTitle::TITLE_MRS));
		if($startDate !== null) {
		    $qb3->andWhere('ad.createdAt >= :startDate');
            $qb3->setParameter('startDate',$startDate);
		}
		if($endDate !== null) {
		    $qb3->andWhere('ad.createdAt < :endDate');
            $qb3->setParameter('endDate',$endDate);
		}
		
		$qb4 = $this->getEntityManager()->createQueryBuilder();
		$qb4->select('count(ad) as m');
		$qb4->from($this->getEntityName(), 'ad');
		$qb4->join('ad.profile', 'profile');
		$qb4->andWhere('profile.title = :gender');
        $qb4->setParameter('gender', NameTitle::TITLE_DOCTOR);
		if($startDate !== null) {
		    $qb4->andWhere('ad.createdAt >= :startDate');
            $qb4->setParameter('startDate',$startDate);
		}
		if($endDate !== null) {
		    $qb4->andWhere('ad.createdAt < :endDate');
            $qb4->setParameter('endDate',$endDate);
		}

		return array(
		    'totalAds'    =>   $qb->getQuery()->getSingleScalarResult(),
		    'adsByMales'  =>   $qb2->getQuery()->getSingleScalarResult(),
		    'adsByFemales'  =>   $qb3->getQuery()->getSingleScalarResult(),
		    'adsByDoctors'  =>   $qb4->getQuery()->getSingleScalarResult(),
		);
	}
	
	/**
	 * Get a random ad
	 * @return \Wanawork\UserBundle\Entity\Ad
	 */
	public function findRandom()
	{
	    $dql = "SELECT COUNT(c) FROM {$this->getEntityName()} c";
	    $count = $this->getEntityManager()->createQuery($dql)
	    ->getSingleScalarResult();
	
	    return $this->createQueryBuilder("c")->setMaxResults(1)
	    ->setFirstResult(rand(0, $count - 1))->getQuery()
	    ->getSingleResult();
	}
}

