<?php
namespace Wanawork\UserBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Wanawork\UserBundle\Entity\Billing\AdOrder;
use Wanawork\MainBundle\Entity\NameTitle;
use Wanawork\UserBundle\Entity\Ad;
use Wanawork\UserBundle\Entity\Billing\Order;
use Wanawork\UserBundle\Entity\Billing\Payment;
use Wanawork\UserBundle\Entity\User;

class AdOrderRepository extends EntityRepository 
{
    public function getStandardOrders($startDate = null, $endDate = null)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('count(adOrder) as c');
        $qb->from($this->getEntityName(), 'adOrder');
        $qb->where('adOrder.plan = :plan');
        $qb->setParameter('plan', Ad::PLAN_STANDARD);
        if($startDate !== null) {
            $qb->andWhere("adOrder.runFrom >= :startDate");
            $qb->setParameter('startDate',$startDate);
        }
        if($endDate !== null) {
            $qb->andWhere("adOrder.runFrom < :endDate");
            $qb->setParameter('endDate',$endDate);
        }
        
        $qb2 = $this->getEntityManager()->createQueryBuilder();
        $qb2->select('count(adOrder) as c');
        $qb2->from($this->getEntityName(), 'adOrder');
        $qb2->where('adOrder.plan = :plan');
        $qb2->setParameter('plan', Ad::PLAN_STANDARD);
		$qb2->join('adOrder.ad', 'ad');
		$qb2->join('ad.profile', 'profile');
		$qb2->andWhere('profile.title = :gender');
        $qb2->setParameter('gender', NameTitle::TITLE_MR);
        if($startDate !== null) {
            $qb2->andWhere("adOrder.runFrom >= :startDate");
            $qb2->setParameter('startDate',$startDate);
        }
        if($endDate !== null) {
            $qb2->andWhere("adOrder.runFrom < :endDate");
            $qb2->setParameter('endDate',$endDate);
        }
        
        $qb3 = $this->getEntityManager()->createQueryBuilder();
        $qb3->select('count(adOrder) as c');
        $qb3->from($this->getEntityName(), 'adOrder');
        $qb3->where('adOrder.plan = :plan');
        $qb3->setParameter('plan', Ad::PLAN_STANDARD);
		$qb3->join('adOrder.ad', 'ad');
		$qb3->join('ad.profile', 'profile');
		$qb3->andWhere('profile.title in (:genders)');
		$qb3->setParameter('genders',array(NameTitle::TITLE_MS,NameTitle::TITLE_MISS,NameTitle::TITLE_MRS));
        if($startDate !== null) {
            $qb3->andWhere("adOrder.runFrom >= :startDate");
            $qb3->setParameter('startDate',$startDate);
        }
        if($endDate !== null) {
            $qb3->andWhere("adOrder.runFrom < :endDate");
            $qb3->setParameter('endDate',$endDate);
        }
        
        $qb4 = $this->getEntityManager()->createQueryBuilder();
        $qb4->select('count(adOrder) as c');
        $qb4->from($this->getEntityName(), 'adOrder');
        $qb4->where('adOrder.plan = :plan');
        $qb4->setParameter('plan', Ad::PLAN_STANDARD);
		$qb4->join('adOrder.ad', 'ad');
		$qb4->join('ad.profile', 'profile');
		$qb4->andWhere('profile.title = :gender');
        $qb4->setParameter('gender', NameTitle::TITLE_DOCTOR);
        if($startDate !== null) {
            $qb4->andWhere("adOrder.runFrom >= :startDate");
            $qb4->setParameter('startDate',$startDate);
        }
        if($endDate !== null) {
            $qb4->andWhere("adOrder.runFrom < :endDate");
            $qb4->setParameter('endDate',$endDate);
        }
    
        return array(
            'totalStandardOrders'     => $qb->getQuery()->getSingleScalarResult(),
            'standardOrdersByMales'   => $qb2->getQuery()->getSingleScalarResult(),
            'standardOrdersByFemales' => $qb3->getQuery()->getSingleScalarResult(),
            'standardOrdersByDoctors' => $qb4->getQuery()->getSingleScalarResult(),
        );
    }
    
    public function getPremiumOrders($startDate = null, $endDate = null)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('count(adOrder) as c');
        $qb->from($this->getEntityName(), 'adOrder');
        $qb->where('adOrder.plan = :plan');
        $qb->setParameter('plan', Ad::PLAN_PREMIUM);
        if($startDate !== null) {
            $qb->andWhere("adOrder.runFrom >= :startDate");
            $qb->setParameter('startDate',$startDate);
        }
        if($endDate !== null) {
            $qb->andWhere("adOrder.runFrom < :endDate");
            $qb->setParameter('endDate',$endDate);
        }
        
        $qb2 = $this->getEntityManager()->createQueryBuilder();
        $qb2->select('count(adOrder) as c');
        $qb2->from($this->getEntityName(), 'adOrder');
        $qb2->where('adOrder.plan = :plan');
        $qb2->setParameter('plan', Ad::PLAN_PREMIUM);
        $qb2->join('adOrder.ad', 'ad');
        $qb2->join('ad.profile', 'profile');
		$qb2->andWhere('profile.title = :gender');
        $qb2->setParameter('gender', NameTitle::TITLE_MR);
        if($startDate !== null) {
            $qb2->andWhere("adOrder.runFrom >= :startDate");
            $qb2->setParameter('startDate',$startDate);
        }
        if($endDate !== null) {
            $qb2->andWhere("adOrder.runFrom < :endDate");
            $qb2->setParameter('endDate',$endDate);
        }
        
        $qb3 = $this->getEntityManager()->createQueryBuilder();
        $qb3->select('count(adOrder) as c');
        $qb3->from($this->getEntityName(), 'adOrder');
        $qb3->where('adOrder.plan = :plan');
        $qb3->setParameter('plan', Ad::PLAN_PREMIUM);
        $qb3->join('adOrder.ad', 'ad');
        $qb3->join('ad.profile', 'profile');
		$qb3->andWhere('profile.title in (:genders)');
		$qb3->setParameter('genders',array(NameTitle::TITLE_MS,NameTitle::TITLE_MISS,NameTitle::TITLE_MRS));
        if($startDate !== null) {
            $qb3->andWhere("adOrder.runFrom >= :startDate");
            $qb3->setParameter('startDate',$startDate);
        }
        if($endDate !== null) {
            $qb3->andWhere("adOrder.runFrom < :endDate");
            $qb3->setParameter('endDate',$endDate);
        }
        
        $qb4 = $this->getEntityManager()->createQueryBuilder();
        $qb4->select('count(adOrder) as c');
        $qb4->from($this->getEntityName(), 'adOrder');
        $qb4->where('adOrder.plan = :plan');
        $qb4->setParameter('plan', Ad::PLAN_PREMIUM);
        $qb4->join('adOrder.ad', 'ad');
        $qb4->join('ad.profile', 'profile');
		$qb4->andWhere('profile.title = :gender');
        $qb4->setParameter('gender', NameTitle::TITLE_DOCTOR);
        if($startDate !== null) {
            $qb4->andWhere("adOrder.runFrom >= :startDate");
            $qb4->setParameter('startDate',$startDate);
        }
        if($endDate !== null) {
            $qb4->andWhere("adOrder.runFrom < :endDate");
            $qb4->setParameter('endDate',$endDate);
        }
        
        return array(
            'totalPremiumOrders'     => $qb->getQuery()->getSingleScalarResult(),
            'premiumOrdersByMales'   => $qb2->getQuery()->getSingleScalarResult(),
            'premiumOrdersByFemales' => $qb3->getQuery()->getSingleScalarResult(),
            'premiumOrdersByDoctors' => $qb4->getQuery()->getSingleScalarResult(),
        );
    }
    


public function getPaymentsByGender($startDate = null, $endDate = null)
    {
        $qb1 = $this->getEntityManager()->createQueryBuilder();
        $qb1->select('count(adOrder) as c');
        $qb1->from($this->getEntityName(), 'adOrder');
        $qb1->join('adOrder.ad', 'ad');
        $qb1->join('ad.profile', 'profile');
        $qb1->where('adOrder.status = :orderStatus');
        $qb1->setParameter('orderStatus', Order::STATUS_PAID);
		$qb1->andWhere('profile.title = :gender');
        $qb1->setParameter('gender', NameTitle::TITLE_MR);
        if($startDate !== null) {
            $qb1->andWhere('adOrder.createdAt >= :startDate');
            $qb1->setParameter('startDate',$startDate);
        }
        if($endDate !== null) {
            $qb1->andWhere('adOrder.createdAt < :endDate');
            $qb1->setParameter('endDate',$endDate);
        }
    
        $qb2 = $this->getEntityManager()->createQueryBuilder();
        $qb2->select('count(adOrder) as c');
        $qb2->from($this->getEntityName(), 'adOrder');
        $qb2->join('adOrder.ad', 'ad');
        $qb2->join('ad.profile', 'profile');
        $qb2->where('adOrder.status = :orderStatus');
        $qb2->setParameter('orderStatus', Order::STATUS_PAID);
		$qb2->andWhere('profile.title in (:genders)');
		$qb2->setParameter('genders',array(NameTitle::TITLE_MS,NameTitle::TITLE_MISS,NameTitle::TITLE_MRS));
        if($startDate !== null) {
            $qb2->andWhere('adOrder.createdAt >= :startDate');
            $qb2->setParameter('startDate',$startDate);
        }
        if($endDate !== null) {
            $qb2->andWhere('adOrder.createdAt < :endDate');
            $qb2->setParameter('endDate',$endDate);
        }
    
        $qb3 = $this->getEntityManager()->createQueryBuilder();
        $qb3->select('count(adOrder) as c');
        $qb3->from($this->getEntityName(), 'adOrder');
        $qb3->join('adOrder.ad', 'ad');
        $qb3->join('ad.profile', 'profile');
        $qb3->where('adOrder.status = :orderStatus');
        $qb3->setParameter('orderStatus', Order::STATUS_PAID);
		$qb3->andWhere('profile.title = :gender');
        $qb3->setParameter('gender', NameTitle::TITLE_DOCTOR);
        if($startDate !== null) {
            $qb3->andWhere('adOrder.createdAt >= :startDate');
            $qb3->setParameter('startDate',$startDate);
        }
        if($endDate !== null) {
            $qb3->andWhere('adOrder.createdAt < :endDate');
            $qb3->setParameter('endDate',$endDate);
        }
    
        return array(
            'paymentsByMales'   => $qb1->getQuery()->getSingleScalarResult(),
            'paymentsByFemales' => $qb2->getQuery()->getSingleScalarResult(),
            'paymentsByDoctors' => $qb3->getQuery()->getSingleScalarResult(),
        );
    }
    
    public function getPaymentAmount($startDate = null, $endDate = null)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('sum(adOrder.amount) as c');
        $qb->from($this->getEntityName(), 'adOrder');
        $qb->join('adOrder.ad', 'ad');
        $qb->join('ad.profile', 'profile');
        $qb->where('adOrder.status = :orderStatus');
        $qb->setParameter('orderStatus', Order::STATUS_PAID);
        if($startDate !== null) {
            $qb->andWhere('adOrder.createdAt >= :startDate');
            $qb->setParameter('startDate',$startDate);
        }
        if($endDate !== null) {
            $qb->andWhere('adOrder.createdAt < :endDate');
            $qb->setParameter('endDate',$endDate);
        }
        
        $qb1 = $this->getEntityManager()->createQueryBuilder();
        $qb1->select('sum(adOrder.amount) as c');
        $qb1->from($this->getEntityName(), 'adOrder');
        $qb1->join('adOrder.ad', 'ad');
        $qb1->join('ad.profile', 'profile');
        $qb1->where('adOrder.status = :orderStatus');
        $qb1->setParameter('orderStatus', Order::STATUS_PAID);
		$qb1->andWhere('profile.title = :gender');
        $qb1->setParameter('gender', NameTitle::TITLE_MR);
        if($startDate !== null) {
            $qb1->andWhere('adOrder.createdAt >= :startDate');
            $qb1->setParameter('startDate',$startDate);
        }
        if($endDate !== null) {
            $qb1->andWhere('adOrder.createdAt < :endDate');
            $qb1->setParameter('endDate',$endDate);
        }
    
        $qb2 = $this->getEntityManager()->createQueryBuilder();
        $qb2->select('sum(adOrder.amount) as c');
        $qb2->from($this->getEntityName(), 'adOrder');
        $qb2->join('adOrder.ad', 'ad');
        $qb2->join('ad.profile', 'profile');
        $qb2->where('adOrder.status = :orderStatus');
        $qb2->setParameter('orderStatus', Order::STATUS_PAID);
		$qb2->andWhere('profile.title in (:genders)');
		$qb2->setParameter('genders',array(NameTitle::TITLE_MS,NameTitle::TITLE_MISS,NameTitle::TITLE_MRS));
        if($startDate !== null) {
            $qb2->andWhere('adOrder.createdAt >= :startDate');
            $qb2->setParameter('startDate',$startDate);
        }
        if($endDate !== null) {
            $qb2->andWhere('adOrder.createdAt < :endDate');
            $qb2->setParameter('endDate',$endDate);
        }
    
        $qb3 = $this->getEntityManager()->createQueryBuilder();
        $qb3->select('sum(adOrder.amount) as c');
        $qb3->from($this->getEntityName(), 'adOrder');
        $qb3->join('adOrder.ad', 'ad');
        $qb3->join('ad.profile', 'profile');
        $qb3->where('adOrder.status = :orderStatus');
        $qb3->setParameter('orderStatus', Order::STATUS_PAID);
		$qb3->andWhere('profile.title = :gender');
        $qb3->setParameter('gender', NameTitle::TITLE_DOCTOR);
        if($startDate !== null) {
            $qb3->andWhere('adOrder.createdAt >= :startDate');
            $qb3->setParameter('startDate',$startDate);
        }
        if($endDate !== null) {
            $qb3->andWhere('adOrder.createdAt < :endDate');
            $qb3->setParameter('endDate',$endDate);
        }
    
        return array(
            'totalPaymentsMade' => $qb->getQuery()->getSingleScalarResult(),
            'paymentsByMales'   => $qb1->getQuery()->getSingleScalarResult(),
            'paymentsByFemales' => $qb2->getQuery()->getSingleScalarResult(),
            'paymentsByDoctors' => $qb3->getQuery()->getSingleScalarResult(),
        );
    }
    
    public function getPayPalPaymentsByGender($startDate = null, $endDate = null)
    {
        $qb1 = $this->getEntityManager()->createQueryBuilder();
        $qb1->select('count(adOrder) as c');
        $qb1->from($this->getEntityName(), 'adOrder');
        $qb1->join('adOrder.ad', 'ad');
        $qb1->join('ad.profile', 'profile');
        $qb1->join('adOrder.payments', 'payments');
        $qb1->where('adOrder.status = :orderStatus');
        $qb1->setParameter('orderStatus', Order::STATUS_PAID);
		$qb1->andWhere('profile.title = :gender');
        $qb1->setParameter('gender', NameTitle::TITLE_MR);
        $qb1->andwhere('payments.source=:source');
        $qb1->setParameter('source', Payment::SOURCE_PAYPAL);
        if($startDate !== null) {
            $qb1->andWhere('adOrder.createdAt >= :startDate');
            $qb1->setParameter('startDate',$startDate);
        }
        if($endDate !== null) {
            $qb1->andWhere('adOrder.createdAt < :endDate');
            $qb1->setParameter('endDate',$endDate);
        }
    
        $qb2 = $this->getEntityManager()->createQueryBuilder();
        $qb2->select('count(adOrder) as c');
        $qb2->from($this->getEntityName(), 'adOrder');
        $qb2->join('adOrder.ad', 'ad');
        $qb2->join('ad.profile', 'profile');
        $qb2->join('adOrder.payments', 'payments');
        $qb2->where('adOrder.status = :orderStatus');
        $qb2->setParameter('orderStatus', Order::STATUS_PAID);
		$qb2->andWhere('profile.title in (:genders)');
		$qb2->setParameter('genders',array(NameTitle::TITLE_MS,NameTitle::TITLE_MISS,NameTitle::TITLE_MRS));
        $qb2->andwhere('payments.source=:source');
        $qb2->setParameter('source', Payment::SOURCE_PAYPAL);
        if($startDate !== null) {
            $qb2->andWhere('adOrder.createdAt >= :startDate');
            $qb2->setParameter('startDate',$startDate);
        }
        if($endDate !== null) {
            $qb2->andWhere('adOrder.createdAt < :endDate');
            $qb2->setParameter('endDate',$endDate);
        }
    
        $qb3 = $this->getEntityManager()->createQueryBuilder();
        $qb3->select('count(adOrder) as c');
        $qb3->from($this->getEntityName(), 'adOrder');
        $qb3->join('adOrder.ad', 'ad');
        $qb3->join('ad.profile', 'profile');
        $qb3->join('adOrder.payments', 'payments');
        $qb3->where('adOrder.status = :orderStatus');
        $qb3->setParameter('orderStatus', Order::STATUS_PAID);
		$qb3->andWhere('profile.title = :gender');
        $qb3->setParameter('gender', NameTitle::TITLE_DOCTOR);
        $qb3->andwhere('payments.source=:source');
        $qb3->setParameter('source', Payment::SOURCE_PAYPAL);
        if($startDate !== null) {
            $qb3->andWhere('adOrder.createdAt >= :startDate');
            $qb3->setParameter('startDate',$startDate);
        }
        if($endDate !== null) {
            $qb3->andWhere('adOrder.createdAt < :endDate');
            $qb3->setParameter('endDate',$endDate);
        }
    
        return array(
            'paymentsByMales'   => $qb1->getQuery()->getSingleScalarResult(),
            'paymentsByFemales' => $qb2->getQuery()->getSingleScalarResult(),
            'paymentsByDoctors' => $qb3->getQuery()->getSingleScalarResult(),
        );
    }
    
    public function getVoucherPaymentsByGender($startDate = null, $endDate = null)
    {
        
        $qb1 = $this->getEntityManager()->createQueryBuilder();
        $qb1->select('count(adOrder) as c');
        $qb1->from($this->getEntityName(), 'adOrder');
        $qb1->join('adOrder.ad', 'ad');
        $qb1->join('ad.profile', 'profile');
        $qb1->join('adOrder.payments', 'payments');
        $qb1->where('adOrder.status = :orderStatus');
        $qb1->setParameter('orderStatus', Order::STATUS_PAID);
		$qb1->andWhere('profile.title = :gender');
        $qb1->setParameter('gender', NameTitle::TITLE_MR);
        $qb1->andwhere('payments.source=:source');
        $qb1->setParameter('source', Payment::SOURCE_VOUCHER);
        if($startDate !== null) {
            $qb1->andWhere('adOrder.createdAt >= :startDate');
            $qb1->setParameter('startDate',$startDate);
        }
        if($endDate !== null) {
            $qb1->andWhere('adOrder.createdAt < :endDate');
            $qb1->setParameter('endDate',$endDate);
        }
    
        $qb2 = $this->getEntityManager()->createQueryBuilder();
        $qb2->select('count(adOrder) as c');
        $qb2->from($this->getEntityName(), 'adOrder');
        $qb2->join('adOrder.ad', 'ad');
        $qb2->join('ad.profile', 'profile');
        $qb2->join('adOrder.payments', 'payments');
        $qb2->where('adOrder.status = :orderStatus');
        $qb2->setParameter('orderStatus', Order::STATUS_PAID);
		$qb2->andWhere('profile.title in (:genders)');
		$qb2->setParameter('genders',array(NameTitle::TITLE_MS,NameTitle::TITLE_MISS,NameTitle::TITLE_MRS));
        $qb2->andwhere('payments.source=:source');
        $qb2->setParameter('source', Payment::SOURCE_VOUCHER);
        if($startDate !== null) {
            $qb2->andWhere('adOrder.createdAt >= :startDate');
            $qb2->setParameter('startDate',$startDate);
        }
        if($endDate !== null) {
            $qb2->andWhere('adOrder.createdAt < :endDate');
            $qb2->setParameter('endDate',$endDate);
        }
    
        $qb3 = $this->getEntityManager()->createQueryBuilder();
        $qb3->select('count(adOrder) as c');
        $qb3->from($this->getEntityName(), 'adOrder');
        $qb3->join('adOrder.ad', 'ad');
        $qb3->join('ad.profile', 'profile');
        $qb3->join('adOrder.payments', 'payments');
        $qb3->where('adOrder.status = :orderStatus');
        $qb3->setParameter('orderStatus', Order::STATUS_PAID);
		$qb3->andWhere('profile.title = :gender');
        $qb3->setParameter('gender', NameTitle::TITLE_DOCTOR);
        $qb3->andwhere('payments.source=:source');
        $qb3->setParameter('source', Payment::SOURCE_VOUCHER);
        if($startDate !== null) {
            $qb3->andWhere('adOrder.createdAt >= :startDate');
            $qb3->setParameter('startDate',$startDate);
        }
        if($endDate !== null) {
            $qb3->andWhere('adOrder.createdAt < :endDate');
            $qb3->setParameter('endDate',$endDate);
        }
    
        return array(
            'paymentsByMales'   => $qb1->getQuery()->getSingleScalarResult(),
            'paymentsByFemales' => $qb2->getQuery()->getSingleScalarResult(),
            'paymentsByDoctors' => $qb3->getQuery()->getSingleScalarResult(),
        );
    }
    
    public function getTwitterPaymentsByGender($startDate = null, $endDate = null)
    {
       
        $qb1 = $this->getEntityManager()->createQueryBuilder();
        $qb1->select('count(adOrder) as c');
        $qb1->from($this->getEntityName(), 'adOrder');
        $qb1->join('adOrder.ad', 'ad');
        $qb1->join('ad.profile', 'profile');
        $qb1->join('adOrder.payments', 'payments');
        $qb1->where('adOrder.status = :orderStatus');
        $qb1->setParameter('orderStatus', Order::STATUS_PAID);
		$qb1->andWhere('profile.title = :gender');
        $qb1->setParameter('gender', NameTitle::TITLE_MR);
        $qb1->andwhere('payments.source=:source');
        $qb1->setParameter('source', Payment::SOURCE_TWITTER);
        
        if($startDate !== null) {
            $qb1->andWhere('adOrder.createdAt >= :startDate');
            $qb1->setParameter('startDate',$startDate);
        }
        if($endDate !== null) {
            $qb1->andWhere('adOrder.createdAt < :endDate');
            $qb1->setParameter('endDate',$endDate);
        }
    
        $qb2 = $this->getEntityManager()->createQueryBuilder();
        $qb2->select('count(adOrder) as c');
        $qb2->from($this->getEntityName(), 'adOrder');
        $qb2->join('adOrder.ad', 'ad');
        $qb2->join('ad.profile', 'profile');
        $qb2->join('adOrder.payments', 'payments');
        $qb2->where('adOrder.status = :orderStatus');
        $qb2->setParameter('orderStatus', Order::STATUS_PAID);
		$qb2->andWhere('profile.title in (:genders)');
		$qb2->setParameter('genders',array(NameTitle::TITLE_MS,NameTitle::TITLE_MISS,NameTitle::TITLE_MRS));
        $qb2->andwhere('payments.source=:source');
        $qb2->setParameter('source', Payment::SOURCE_TWITTER);
        if($startDate !== null) {
            $qb2->andWhere('adOrder.createdAt >= :startDate');
            $qb2->setParameter('startDate',$startDate);
        }
        if($endDate !== null) {
            $qb2->andWhere('adOrder.createdAt < :endDate');
            $qb2->setParameter('endDate',$endDate);
        }
    
        $qb3 = $this->getEntityManager()->createQueryBuilder();
        $qb3->select('count(adOrder) as c');
        $qb3->from($this->getEntityName(), 'adOrder');
        $qb3->join('adOrder.ad', 'ad');
        $qb3->join('ad.profile', 'profile');
        $qb3->join('adOrder.payments', 'payments');
        $qb3->where('adOrder.status = :orderStatus');
        $qb3->setParameter('orderStatus', Order::STATUS_PAID);
		$qb3->andWhere('profile.title = :gender');
        $qb3->setParameter('gender', NameTitle::TITLE_DOCTOR);
        $qb3->andwhere('payments.source=:source');
        $qb3->setParameter('source', Payment::SOURCE_TWITTER);
        if($startDate !== null) {
            $qb3->andWhere('adOrder.createdAt >= :startDate');
            $qb3->setParameter('startDate',$startDate);
        }
        if($endDate !== null) {
            $qb3->andWhere('adOrder.createdAt < :endDate');
            $qb3->setParameter('endDate',$endDate);
        }
    
        return array(
            'paymentsByMales'   => $qb1->getQuery()->getSingleScalarResult(),
            'paymentsByFemales' => $qb2->getQuery()->getSingleScalarResult(),
            'paymentsByDoctors' => $qb3->getQuery()->getSingleScalarResult(),
        );
    }
}
