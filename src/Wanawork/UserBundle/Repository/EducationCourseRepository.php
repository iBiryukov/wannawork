<?php
namespace Wanawork\UserBundle\Repository;
use Doctrine\ORM\EntityRepository;

class EducationCourseRepository extends EntityRepository {

	public function findRandom() {
		$dql = "SELECT COUNT(c) FROM {$this->getEntityName()} c";
		$count = $this->getEntityManager()->createQuery($dql)->getSingleScalarResult();
		
		return
		$this->createQueryBuilder("c")
			->setMaxResults(1)
			->setFirstResult(rand(0, $count - 1))
			->getQuery()
			->getSingleResult();
	}
}
