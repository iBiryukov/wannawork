<?php
namespace Wanawork\UserBundle\Repository;

use Doctrine\ORM\EntityRepository;

class VoucherRepository extends EntityRepository
{

    /**
     * Get voucher by code, case insensitive
     * @param string $code
     * 
     * @return \Wanawork\UserBundle\Entity\Billing\Voucher\Voucher | null
     */
    public function findOneByCode($code)
    {
        $qb = $this->createQueryBuilder('voucher');
        $qb->where('LOWER(voucher.id) = :code');
        $qb->setParameter('code', strtolower($code));
        
        return $qb->getQuery()->getOneOrNullResult();
    }
}