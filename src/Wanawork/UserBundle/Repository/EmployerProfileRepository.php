<?php
namespace Wanawork\UserBundle\Repository;
use Doctrine\ORM\EntityRepository;
use Wanawork\UserBundle\Entity\Billing\Order;

class EmployerProfileRepository extends EntityRepository
{
    /**
     * Get a list of employers that need to be reviewed for verification
     * Verification paid already but not yet done
     * @return array
     */
    public function findPendingVerification()
    {
        $qb = $this->createQueryBuilder('emp');
        $qb->join('emp.verificationOrders', 'orders');
        $qb->where('emp.isVerified=:isVerified');
        $qb->andWhere('orders.status=:orderStatus');
        
        $qb->setParameter('isVerified', false);
        $qb->setParameter('orderStatus', Order::STATUS_PAID);
        
        return $qb->getQuery()->getResult();
    }
    
    
    public function findRandom($limit = 1) 
	{
		$dql = "SELECT COUNT(c) FROM {$this->getEntityName()} c";
		$count = $this->getEntityManager()->createQuery($dql)
				->getSingleScalarResult();

		return $this->createQueryBuilder("c")->setMaxResults($limit)
				->setFirstResult(rand(0, $count - 1))->getQuery()->getResult();
	}
    
}