<?php
namespace Wanawork\UserBundle\Repository;
use Doctrine\ORM\EntityRepository;
use Wanawork\MainBundle\Entity\NameTitle;

class UserRepository extends EntityRepository {

	/**
	 * Get the number of registrations per day
	 * @param string | null $accountType		
	 * @param \DateTime | null $startDate
	 * @return array
	 */
	public function findRegistrationByDate($accountType = null, \DateTime $startDate = null) {
		$qb = $this->getEntityManager()->createQueryBuilder();
		
		$qb->select(array(
			'COUNT(user) as c',
			'CONCAT_WS(\'-\', YEAR(user.registrationDate), MONTH(user.registrationDate), DAY(user.registrationDate)) as date'
		));
		
		$qb->from($this->getEntityName(), 'user');
		$qb->groupBy('date');
		
		if($accountType !== null) {
			$qb->join('user.roles', 'role');
			$qb->where('role.id IN (:role)');
			$qb->setParameter('role', $accountType);
		}

		$query = $qb->getQuery();
		
		$registrations = array();
		foreach($query->getResult() as $registration) {
			$dateParts = explode('-', $registration['date']);
			if(strlen($dateParts[1]) === 1) {
				$dateParts[1] = '0' . $dateParts[1];
			}
			
			if(strlen($dateParts[2]) === 1) {
				$dateParts[2] = '0' . $dateParts[2];
			}
			$registrations[implode('-', $dateParts)] = $registration['c'];
		}
		
		if($startDate === null) {
			$startDate = new \DateTime('2013-06-01');
		}
		
		$nowDate = new \DateTime();
		$interval = new \DateInterval('P1D');
		
		$dateGroupedRegistrations = array();
		for($date = $startDate; $date <= $nowDate; $date->add($interval)) {
			$dateString = $date->format('Y-m-d');
			if(isset($registrations[$dateString])) {
				$dateGroupedRegistrations[$dateString] = $registrations[$dateString];
			} else {
				$dateGroupedRegistrations[$dateString] = 0;
			}
		}
		return $dateGroupedRegistrations;
	}
	
	/**
	 * Get the count of users of a particular type
	 * @param string $accountType
	 * @throws \Exception
	 * @return integer
	 */
	public function getUserCount($accountType = null, $startDate = null, $endDate = null)
	{
		$qb = $this->getEntityManager()->createQueryBuilder();
		$qb->select('count(user) as c');
		$qb->from($this->getEntityName(), 'user');
		
		if($accountType !== null)
		{
    		$qb->join('user.roles', 'role');
    		$qb->where('role.id IN (:role)');
    		$qb->setParameter('role', $accountType);
		}
		
		if($startDate !== null) {
		    $qb->andWhere('user.registrationDate >= :startDate');
            $qb->setParameter('startDate',$startDate);
		}
		
		if($endDate !== null) {
		    $qb->andWhere('user.registrationDate < :endDate');
            $qb->setParameter('endDate',$endDate);
		}
        
        return $qb->getQuery()->getSingleScalarResult();
	}
	
	/**
	 * Get the count of users by gender
	 * @param string $accountType
	 * @throws \Exception
	 * @return integer
	 */
	public function getUsersCountByGender($startDate = null, $endDate = null)
	{
		$qb = $this->getEntityManager()->createQueryBuilder();
		$qb->select('count(user) as c');
		$qb->from($this->getEntityName(), 'user');
		$qb->join('user.employeeProfile', 'profile');
		$qb->andWhere('profile.title = :gender');
        $qb->setParameter('gender', NameTitle::TITLE_MR);
		if($startDate !== null) {
		    $qb->andWhere('user.registrationDate >= :startDate');
            $qb->setParameter('startDate',$startDate);
		}
		if($endDate !== null) {
		    $qb->andWhere('user.registrationDate < :endDate');
            $qb->setParameter('endDate',$endDate);
		}
		
        $qb2 = $this->getEntityManager()->createQueryBuilder();
		$qb2->select('count(user) as c');
		$qb2->from($this->getEntityName(), 'user');
		$qb2->join('user.employeeProfile', 'profile');
		$qb2->andWhere('profile.title in (:genders)');
		$qb2->setParameter('genders',array(NameTitle::TITLE_MS,NameTitle::TITLE_MISS,NameTitle::TITLE_MRS));
		if($startDate !== null) {
		    $qb2->andWhere('user.registrationDate >= :startDate');
            $qb2->setParameter('startDate',$startDate);
		}
		
		if($endDate !== null) {
		    $qb2->andWhere('user.registrationDate < :endDate');
            $qb2->setParameter('endDate',$endDate);
		}
		
		$qb3 = $this->getEntityManager()->createQueryBuilder();
		$qb3->select('count(user) as c');
		$qb3->from($this->getEntityName(), 'user');
		$qb3->join('user.employeeProfile', 'profile');
		$qb3->andWhere('profile.title = :gender');
        $qb3->setParameter('gender', NameTitle::TITLE_DOCTOR);
		if($startDate !== null) {
		    $qb3->andWhere('user.registrationDate >= :startDate');
            $qb3->setParameter('startDate',$startDate);
		}
		
		if($endDate !== null) {
		    $qb3->andWhere('user.registrationDate < :endDate');
            $qb3->setParameter('endDate',$endDate);
		}
        

		return array(
		    'numberOfMales'  =>   $qb->getQuery()->getSingleScalarResult(),
		    'numberOfFemales'  =>   $qb2->getQuery()->getSingleScalarResult(),
		    'numberOfDoctors'  =>   $qb3->getQuery()->getSingleScalarResult(),
		);
	}
	
	/**
	 * Find one random user
	 * @return \Wanawork\UserBundle\Entity\User
	 */
	public function findRandomEmployeeUser()
	{
	    $qb = $this->createQueryBuilder('user');
	    $qb->join('user.employeeProfile', 'profile');
	    $qb->setMaxResults($maxResults = 1);
	    return $qb->getQuery()->getSingleResult();
	}
}
