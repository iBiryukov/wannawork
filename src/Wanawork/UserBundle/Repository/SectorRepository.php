<?php
namespace Wanawork\UserBundle\Repository;
use Doctrine\ORM\EntityRepository;

class SectorRepository extends EntityRepository
{

	public function findAllWithJobs()
	{
		$dql = "SELECT sector, jobs FROM {$this->_entityName} sector JOIN sector.jobs jobs order by sector.name, jobs.name";
		return $this->getEntityManager()->createQuery($dql)->getResult();
	}
	
    public function findRandom($limit = 1)
    {
        $dql = "SELECT COUNT(c) FROM {$this->getEntityName()} c";
        $count = $this->getEntityManager()->createQuery($dql)
        ->getSingleScalarResult();
    
        return $this->createQueryBuilder("c")->setMaxResults($limit)
        ->setFirstResult(rand(0, $count - 1))->getQuery()->getResult();
    }
    
    /**
     * Get the next id
     * @return number
     */
    public function findNextId()
    {
        $dql = "SELECT MAX(s) FROM {$this->getEntityName()} s";
        $query = $this->getEntityManager()->createQuery($dql);
        $id = $query->getSingleScalarResult();
        if(!is_int($id)) {
            $id = 0;
        }
        return ($id + 1);
    }
    
    public function deleteWhereNotInId(array $ids)
    {
        $dql = "DELETE {$this->_entityName} e WHERE e.id NOT IN(:ids)";
        $query = $this->getEntityManager()->createQuery($dql);
        $query->setParameter('ids', $ids);
        return $query->execute();
    }
    
    /**
     * Find sectors by the list of ids
     * @param array $ids
     * @return array
     */
    public function findByIds(array $ids)
    {
        $dql = "SELECT s FROM {$this->_entityName} s WHERE s.id IN(:ids)";
        $query = $this->getEntityManager()->createQuery($dql);
        $query->setParameter('ids', $ids);
        
        return $query->getResult();
    }
    
}
