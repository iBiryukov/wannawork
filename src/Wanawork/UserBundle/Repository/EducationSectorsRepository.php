<?php
namespace Wanawork\UserBundle\Repository;
use Doctrine\ORM\EntityRepository;

class EducationSectorsRepository extends EntityRepository {

	public function findAllWithCourses() {
		$dql = "SELECT sector, courses FROM {$this->_entityName} sector JOIN sector.courses courses ORDER BY sector.title, courses.title";
		return $this->getEntityManager()->createQuery($dql)->getResult();
	}
}
