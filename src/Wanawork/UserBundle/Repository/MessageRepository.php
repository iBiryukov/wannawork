<?php
namespace Wanawork\UserBundle\Repository;
use Wanawork\UserBundle\Entity\Message;
use Wanawork\MainBundle\Entity\NameTitle;

use Doctrine\ORM\EntityRepository;

class MessageRepository extends EntityRepository 
{
	public function getMessagesSent($startDate = null, $endDate = null)
	{
	    $qb = $this->getEntityManager()->createQueryBuilder();
		$qb->select('count(message) as c');
		$qb->from($this->getEntityName(), 'message');
		if($startDate !== null) {
		    $qb->andWhere('message.postedOn >= :startDate');
            $qb->setParameter('startDate',$startDate);
		}
		if($endDate !== null) {
		    $qb->andWhere('message.postedOn < :endDate');
            $qb->setParameter('endDate',$endDate);
		}
		
	    $qb2 = $this->getEntityManager()->createQueryBuilder();
		$qb2->select('count(message) as c');
		$qb2->from($this->getEntityName(), 'message');
		$qb2->join('message.author', 'author');
		$qb2->join('author.employeeProfile', 'profile');
		$qb2->andWhere('profile.title = :gender');
        $qb2->setParameter('gender', NameTitle::TITLE_MR);
		if($startDate !== null) {
		    $qb2->andWhere('message.postedOn >= :startDate');
            $qb2->setParameter('startDate',$startDate);
		}
		if($endDate !== null) {
		    $qb2->andWhere('message.postedOn < :endDate');
            $qb2->setParameter('endDate',$endDate);
		}
		
	    $qb3 = $this->getEntityManager()->createQueryBuilder();
		$qb3->select('count(message) as c');
		$qb3->from($this->getEntityName(), 'message');
		$qb3->join('message.author', 'author');
		$qb3->join('author.employeeProfile', 'profile');
		$qb3->andWhere('profile.title in (:genders)');
		$qb3->setParameter('genders',array(NameTitle::TITLE_MS,NameTitle::TITLE_MISS,NameTitle::TITLE_MRS));
		if($startDate !== null) {
		    $qb3->andWhere('message.postedOn >= :startDate');
            $qb3->setParameter('startDate',$startDate);
		}
		if($endDate !== null) {
		    $qb3->andWhere('message.postedOn < :endDate');
            $qb3->setParameter('endDate',$endDate);
		}
		
	    $qb4 = $this->getEntityManager()->createQueryBuilder();
		$qb4->select('count(message) as c');
		$qb4->from($this->getEntityName(), 'message');
		$qb4->join('message.author', 'author');
		$qb4->join('author.employeeProfile', 'profile');
		$qb4->andWhere('profile.title = :gender');
        $qb4->setParameter('gender', NameTitle::TITLE_DOCTOR);
		if($startDate !== null) {
		    $qb4->andWhere('message.postedOn >= :startDate');
            $qb4->setParameter('startDate',$startDate);
		}
		if($endDate !== null) {
		    $qb4->andWhere('message.postedOn < :endDate');
            $qb4->setParameter('endDate',$endDate);
		}
        
        return array(
            'totalMessages'     => $qb->getQuery()->getSingleScalarResult(),
            'messagesByMales'   => $qb2->getQuery()->getSingleScalarResult(),
            'messagesByFemales' => $qb3->getQuery()->getSingleScalarResult(),
            'messagesByDoctors' => $qb4->getQuery()->getSingleScalarResult(),
        );
	}

}
