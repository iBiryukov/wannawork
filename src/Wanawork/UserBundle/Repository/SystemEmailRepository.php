<?php
namespace Wanawork\UserBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Wanawork\UserBundle\Entity\SystemEmail;

class SystemEmailRepository extends EntityRepository
{
    
    public function findReportDates()
    {
        $dql = "
            SELECT CONCAT_WS('-', MONTH(email.dateSent), YEAR(email.dateSent)) as dateSent
            FROM {$this->getEntityName()} email
            WHERE email.status=:status
            GROUP BY dateSent
        ";
        
        $query = $this->getEntityManager()->createQuery($dql);
        $query->setParameter('status', SystemEmail::STATUS_SENT);
        
        return array_map(function($data){
        	return $data['dateSent'];
        }, $query->getArrayResult());
    }
    
    public function findStats($date = null)
    {
        if ($date !== null && !preg_match('/^\d{1,2}\-\d{4}$/', $date)) {
            throw new \InvalidArgumentException("Invalid date given: " . $date);
        }
        
        $dateDql = $date == null ? '' : "AND CONCAT_WS('-', MONTH(email.dateSent), YEAR(email.dateSent))=:dateSent";
        
        $sentDql = "
            SELECT 
                COUNT(email) as email_count, 
                email.type as type
            FROM {$this->getEntityName()} email
            INDEX BY email.type
            WHERE 
                email.status=:status
                $dateDql
            GROUP BY email.type
        ";
        
        $openedDql = "
            SELECT 
                COUNT(email) as open_count,
                email.type as type
            FROM {$this->getEntityName()} email
            INDEX BY email.type
            WHERE email.status=:status AND
                SIZE(email.trackers) > 0
                $dateDql
            GROUP BY email.type
        ";
        
        $parameters = array(
            'status' => SystemEmail::STATUS_SENT,
        );
        if ($date !== null) {
            $parameters['dateSent'] = $date;
        }
        
        $sentQuery = $this->getEntityManager()->createQuery($sentDql);
        $sentQuery->setParameters($parameters);
        $openedQuery = $this->getEntityManager()->createQuery($openedDql);
        $openedQuery->setParameters($parameters);    

        $sentResult = $sentQuery->getResult();
        $openedResult = $openedQuery->getResult();
        
        foreach ($sentResult as $type => &$data) {
            if (array_key_exists($type, $openedResult)) {
                $data['open_count'] = $openedResult[$type]['open_count'];   
            } else {
                $data['open_count'] = 0;
            }
            $data['type_name'] = SystemEmail::_getTypeName($type);
        }
        
        return $sentResult;
    }
}