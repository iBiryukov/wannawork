<?php
namespace Wanawork\UserBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Wanawork\UserBundle\Entity\Billing\Order;

class OrderRepository extends EntityRepository
{
    
    /**
     * Get all orders for which receipts have not been yet generated
     * @param number $limit
     * @return array
     */
    public function findOrdersPendingReceipts($limit = null)
    {
        $qb = $this->createQueryBuilder('o');
        $qb->where('o.status = :status');
        $qb->andWhere('o.receiptEmailed = :emailStatus');
        $qb->orderBy('o.createdAt', 'ASC');
        
        if ($limit !== null) {
            $qb->setMaxResults($limit);
        }
        
        $qb->setParameters(array(
        	'status' => Order::STATUS_PAID,
            'emailStatus' => false,
        ));
        
        return $qb->getQuery()->getResult();
    }
}