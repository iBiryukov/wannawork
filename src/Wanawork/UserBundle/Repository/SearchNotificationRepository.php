<?php
namespace Wanawork\UserBundle\Repository;
use Wanawork\UserBundle\Entity\User;

use Doctrine\ORM\EntityRepository;

class SearchNotificationRepository extends EntityRepository {

	public function findByUser(User $user) {
		$qb = $this->createQueryBuilder('sn');
		$qb->addSelect('search');
		$qb->join('sn.search', 'search');
		$qb->where('search.user = :user');
		$qb->setParameter('user', $user);
		
		return $qb->getQuery()->getResult();
	}
}
