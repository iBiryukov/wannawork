<?php
namespace Wanawork\UserBundle\Entity;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * 
 * @author Ilya Biryukov <ilya@wanawork.ie>
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class TemporaryAvatar {
	
	/**
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;
	
	/**
	 * 
	 * @var User
	 * @ORM\ManyToOne(targetEntity="User")
	 */
	protected $owner;
	
	/**
	 *
	 * @var Symfony\Component\HttpFoundation\File\File | null
	 * @Assert\Image(maxSize="2M")
	 */
	protected $avatarFile;
	
	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	protected $avatarPath;
	
	/**
	 * Upload timestamp
	 * @var \DateTime
	 * @ORM\Column(type="datetime")
	 */
	protected $uploadDate;
	
	public function __construct(File $file, User $owner) {
		$this->setAvatarFile($file);
		$this->owner = $owner;
		$this->uploadDate = new \DateTime();
	}
	
	/**
	 * @return \DateTime
	 */
	public function getUploadDate() 
	{
		return $this->uploadDate;
	}
	
	public function getId() {
		return $this->id;
	}
	
	public function getOwner() {
		return $this->owner;
	}
	
	/**
	 *
	 * @return File $avatar
	 */
	public function getAvatarFile() {
		if(!$this->avatarFile && file_exists($this->getAbsolutePath())) {
			$this->avatarFile = new File($this->getAbsolutePath());
		}
		return $this->avatarFile;
	}
	
	public function setAvatarFile(File $avatar) {
		$this->avatarFile = $avatar;
		$this->preUpload();
	}
	
	/**
	 * @ORM\PrePersist()
	 * @ORM\PreUpdate()
	 */
	public function preUpload()
	{
		if (null !== $this->avatarFile) {
			// do whatever you want to generate a unique name
			$this->avatarPath = sha1(uniqid(mt_rand(), true)).'_temporary.'.$this->avatarFile->guessExtension();
		}
	}
	
	/**
	 * @ORM\PostPersist()
	 * @ORM\PostUpdate()
	 */
	public function upload()
	{
		if (null === $this->avatarFile) {
			return;
		}
	
		// if there is an error when moving the file, an exception will
		// be automatically thrown by move(). This will properly prevent
		// the entity from being persisted to the database on error
		$this->avatarFile->move($this->getUploadRootDir(), $this->avatarPath);
		unset($this->avatarFile);
	}
	
	/**
	 * @ORM\PostRemove()
	 */
	public function removeUpload()
	{
		$file = $this->getAbsolutePath();
		if ($file && file_exists($file)) {
			unlink($file);
		}
	}
	
	public function getAbsolutePath() {
		return null === $this->avatarPath ? null : $this->getUploadRootDir () . '/' . $this->avatarPath;
	}
	
	public function getWebPath() {
		return null === $this->avatarPath ? null : $this->getUploadDir () . '/' . $this->avatarPath;
	}
	
	protected function getUploadRootDir() {
		// the absolute directory path where uploaded documents should be saved
		return (__DIR__ . '/../../../../web/' . $this->getUploadDir ());
	}
	
	protected function getUploadDir() {
		// get rid of the __DIR__ so it doesn't screw when displaying uploaded
		// doc/image in the view.
		return 'uploads/avatars';
	}
}
