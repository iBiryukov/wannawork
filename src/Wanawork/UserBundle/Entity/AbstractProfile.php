<?php
namespace Wanawork\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Wanawork\MainBundle\Entity\County;
use Wanawork\UserBundle\Interfaces\EntityWithFileInterface;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Abstract Profile
 * @author Ilya Biryukov <ilya@goparty.ie>
 * @ORM\Entity
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({"employee" = "EmployeeProfile", "employer" = "EmployerProfile"})
 * @ORM\HasLifecycleCallbacks
 * 
 * @Assert\GroupSequence({"AbstractProfile", "step1"})
 */
abstract class AbstractProfile implements EntityWithFileInterface
{
    use \Gedmo\Timestampable\Traits\TimestampableEntity;
    
    /**
	 * Profile ID
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;
    
	/**
	 * Skype name
	 * @var string
	 * @ORM\Column(type="string", nullable=true)
	 * @Assert\Length(max=255, maxMessage="Skype name cannot be longer than {{ limit }} characters", groups={"Default", "step1"})
	 */
	protected $skype;

	/**
	 * Phone Number
	 * @var string
	 * @ORM\Column(type="string", length=25, nullable=true)
	 * @Assert\NotBlank(message="Phone number cannot be blank", groups={"Default"})
	 * @Assert\Length(max=25, maxMessage="Phone number is too long. Max {{ limit }} characters.", groups={"Default", "step1"})
	 */
	protected $phoneNumber;

	/**
	 * Address
	 *
	 * @var string
	 * @ORM\Column(type="text", nullable=true)
	 * @Assert\NotBlank(message="Please enter your address", groups={"Default"})
	 * @Assert\Length(max=500, maxMessage="Address cannot be longer than {{ limit }} characters", groups={"Default", "step1"})
	 */
	protected $address;

	/**
	 * City
	 *
	 * @var string
	 * @ORM\Column(type="string", length=150, nullable=true)
	 * @Assert\NotBlank(message="Please enter the city", groups={"Default"})
	 * @Assert\Length(max=150, maxMessage="City cannot be longer than {{ limit }} characters", groups={"Default", "step1"})
	 */
	protected $city;

	/**
	 * County
	 * @var Wanawork\MainBundle\Entity\County
	 * @ORM\ManyToOne(targetEntity="Wanawork\MainBundle\Entity\County")
	 * 
	 * @Assert\NotNull(message="Please select your county", groups={"Default"})
	 * @Assert\Type(type="\Wanawork\MainBundle\Entity\County", message="Please select your county", groups={"Default", "step1"})
	 * @Assert\Valid
	 */
	protected $county;

	/**
	 *
	 * @var \Symfony\Component\HttpFoundation\File\File | null
	 * @Assert\Image(maxSize="1M", groups={"step1", "Default"})
	 */
	protected $avatarFile;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	protected $avatarPath;

	public function setPhoneNumber($phoneNumber) 
	{
		$this->phoneNumber = $phoneNumber;
	}

	public function getPhoneNumber() 
	{
		return $this->phoneNumber;
	}

	/**
	 *
	 * @return string $address
	 */
	public function getAddress()
	{
		return $this->address;
	}

	/**
	 *
	 * @return string $city
	 */
	public function getCity()
	{
		return $this->city;
	}

	/**
	 *
	 * @return \Wanawork\MainBundle\Entity\County $county
	 */
	public function getCounty() 
	{
		return $this->county;
	}

	/**
	 *
	 * @param string $address
	 */
	public function setAddress($address) 
	{
		$this->address = $address;
	}

	/**
	 *
	 * @param string $city
	 */
	public function setCity($city) 
	{
		$this->city = $city;
	}

	/**
	 *
	 * @param \Wanawork\MainBundle\Entity\County $county
	 */
	public function setCounty($county)
	{
		$this->county = $county;
	}

	/**
	 *
	 * @return File $avatar
	 */
	public function getAvatarFile() 
	{
		return $this->avatarFile;
	}

	public function setAvatarFile(File $avatar) 
	{
		$this->avatarFile = $avatar;
		$this->preUpload();
	}

	/**
	 * @ORM\PrePersist()
	 * @ORM\PreUpdate()
	 */
	public function preUpload() 
	{
		if (null !== $this->avatarFile) {
			// do whatever you want to generate a unique name
			$this->avatarPath = sha1(uniqid(mt_rand(), true)) . '.'
					. $this->avatarFile->guessExtension();
		}
	}

	/**
	 * @ORM\PostPersist()
	 * @ORM\PostUpdate()
	 */
	public function upload() 
	{
		if (null === $this->avatarFile) {
			return;
		}

		// if there is an error when moving the file, an exception will
		// be automatically thrown by move(). This will properly prevent
		// the entity from being persisted to the database on error
		$this->avatarFile->move($this->getUploadRootDir(), $this->avatarPath);
		unset($this->avatarFile);
	}

	/**
	 * @ORM\PostRemove()
	 */
	public function removeUpload() 
	{
		$file = $this->getAbsolutePath();
		if ($file) {
			unlink($file);
		}
	}

	public function getAbsolutePath()
	{
		return null === $this->avatarPath ? null
				: $this->getUploadRootDir() . '/' . $this->avatarPath;
	}

	public function getWebPath()
	{
		return null === $this->avatarPath ? null
				: $this->getUploadDir() . '/' . $this->avatarPath;
	}

	protected function getUploadRootDir()
	{
		// the absolute directory path where uploaded documents should be saved
		return (__DIR__ . '/../../../../web/' . $this->getUploadDir());
	}

	protected function getUploadDir() 
	{
		// get rid of the __DIR__ so it doesn't screw when displaying uploaded
		// doc/image in the view.
		return 'uploads/avatars';
	}

	public function getSkype()
	{
		return $this->skype;
	}

	public function setSkype($skype) 
	{
		$this->skype = $skype;
	}
	
	public function getId()
	{
	    return $this->id;
	}
	
	abstract public function getCvRequests($status = null);
	
	/**
	 * Get a list of orders this user paid for on his profiles
	 * 
	 * @return array
	 */
	abstract public function getOrders($status = null);
	
	/**
	 * Get the list of mailboxes that can be used for communication
	 * sorted by last updated 
	 * @return array
	 */
    public function getMailboxes()
    {
        $threads = array();
        foreach ($this->getCvRequests() as $cvRequest) {
            $thread = $cvRequest->getMailThread();
            if ($thread instanceof MailThread) {
                $threads[] = $thread;
            }
        }
        
        usort($threads, function(MailThread $t1, MailThread $t2){
        	$u1 = $t1->getUpdatedOn();
        	$u2 = $t2->getUpdatedOn();
        
        	if ($u1 == $u2) return 0;
        	return $u1 < $u2 ? 1 : -1;
        });
        
        return $threads;
    }
	
}
