<?php
namespace Wanawork\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Record created when a search notification is checked for new ads
 * @author Ilya Biryukov <ilya@wannawork.ie>
 * @ORM\Entity
 */
class SearchNotificationCheck
{
    /**
     * @var integer
     * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * Notification for which this check is
     * @var \Wanawork\UserBundle\Entity\SearchNotification
     * 
     * @ORM\ManyToOne(targetEntity="\Wanawork\UserBundle\Entity\SearchNotification", inversedBy="searchChecks")
     * @ORM\JoinColumn(name="search_id", referencedColumnName="search_id")
     */
    protected $notification;
    
    /**
     * When the check was done
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    protected $date;
    
    /**
     * How many new ads were found
     * @var integer
     * @ORM\Column(type="integer")
     */
    protected $newAdsCount;
    
    /**
     * List of new ads that were found
     * @var \Doctrine\Common\Collections\ArrayCollection
     * @ORM\ManyToMany(targetEntity="Wanawork\UserBundle\Entity\Ad")
     */
    protected $newAds;
    
    public function __construct(SearchNotification $notification, array $newAds)
    {
        $this->notification = $notification;
        $this->date = new \DateTime();
        
        $this->newAds = new ArrayCollection($newAds);
        $this->newAdsCount = sizeof($newAds);
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \Wanawork\UserBundle\Entity\SearchNotification
     */
    public function getNotification()
    {
        return $this->notification;
    }

    public function getDate()
    {
        return clone $this->date;
    }

    public function getNewAdsCount()
    {
        return $this->newAdsCount;
    }
    
    public function setNewAdsCount($count)
    {
        $this->newAdsCount = $count;
    }

    public function getNewAds()
    {
        return $this->newAds;
    }
}