<?php
namespace Wanawork\UserBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Mail Thead 
 * @author Ilya Biryukov <ilya@wanawork.ie>
 * @ORM\Entity(repositoryClass="Wanawork\UserBundle\Repository\MailThreadRepository")
 */
class MailThread 
{
	/**
	 * Mail Thread ID
	 * @var integer
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;
	
	/**
	 * List of messages in this thread
	 * @var Doctrine\Common\Collections\ArrayCollection
	 * @ORM\OneToMany(targetEntity="Wanawork\UserBundle\Entity\Message", mappedBy="thread", cascade={"all"}, indexBy="id")
	 * @ORM\OrderBy({"id" = "ASC"})
	 */
	protected $messages;
	
	/**
	 * When the thread was created
	 * @var \DateTime 
	 * @ORM\Column(type="datetime")
	 * @Assert\NotNull()
	 * @Assert\DateTime()
	 */
	protected $createdOn;
	
	/**
	 * When was the last message posted?
	 * @var \DateTime
	 * @ORM\Column(type="datetime")
	 * @Assert\NotNull
	 * @Assert\DateTime
	 */
	protected $updatedOn;
	
	/**
	 * Employee Account that participates in the conversation
	 * @var Wanawork\UserBundle\Entity\User
	 * @ORM\ManyToOne(targetEntity="Wanawork\UserBundle\Entity\EmployeeProfile")
	 * @Assert\NotNull()
	 */
	protected $employee;
	
	/**
	 * Employer account that participates in the conversation
	 * @var Wanawork\UserBundle\Entity\User
	 * @ORM\ManyToOne(targetEntity="Wanawork\UserBundle\Entity\EmployerProfile")
	 * @Assert\NotNull()
	 */
	protected $employer;
	
	/**
	 * CV request to which this mail thread is realted
	 * @var Wanawork\UserBundle\Entity\CvRequest
	 * @ORM\OneToOne(targetEntity="Wanawork\UserBundle\Entity\CvRequest", inversedBy="mailThread")
	 * @Assert\NotNull()
	 */
	protected $cvRequest;
	
	public function __construct(CvRequest $cvRequest) 
	{
		$date =  new \DateTime();
		$this->messages = new ArrayCollection();
		$this->createdOn = $date;	
		$this->setUpdatedOn($date);
		$this->setCvRequest($cvRequest);
	}
	
	/**
	 * 
	 * @return number
	 */
	public function getId()
	{
		return $this->id;
	}
	
	/**
	 * 
	 * @return \Doctrine\Common\Collections\ArrayCollection
	 */
	public function getMessages()
	{
		return $this->messages;
	}
	
	/**
	 * Get unread messages. 
	 * If the user is specified, 
	 * then unread messages will be given for this specific user profile
	 * 
	 * @param AbstractProfile $profile 
	 * 
	 * @return \Doctrine\Common\Collections\ArrayCollection
	 */
	public function getUnreadMessages(AbstractProfile $profile = null)
	{
		$messages = $this->getMessages()->filter(function(Message $m){
			return !$m->isRead();
		});
		if($profile !== null) {
			$messages = $messages->filter(function(Message $m) use($profile){
			    return $m->getProfile() !== $profile;
			});
		}
		return $messages;
	}
	
	public function addMessage(Message $message)
	{
	    $this->setUpdatedOn(new \DateTime());
	    $this->getMessages()->add($message);
	}
	
	public function removeMessage(Message $message)
	{
	    if($this->getMessages()->contains($message)) {
	        $this->getMessages()->removeElement($message);
	        
	        // Set the last update timestamp of the thread
	        if($this->getMessages()->count() > 0) {
	            $this->setUpdatedOn($this->getMessages()->last()->getPostedOn());
	        } else {
	            $this->setUpdatedOn($this->getCreatedOn());
	        }
	    }
	}
	
	/**
	 * 
	 * @return \DateTime
	 */
	public function getCreatedOn() 
	{
		return clone $this->createdOn;
	}
	
	public function setUpdatedOn(\DateTime $date = null)
	{
		if($date === null) {
			$date = new \DateTime();
		}
		$this->updatedOn = $date;
	}
	
	/**
	 * 
	 * @return \DateTime
	 */
	public function getUpdatedOn()
	{
		return clone $this->updatedOn;
	}
	
	
	public function setCvRequest(CvRequest $cvRequest)
	{
		$this->cvRequest = $cvRequest;
		$this->employee = $cvRequest->getAd()->getProfile();
		$this->employer = $cvRequest->getEmployerProfile();
	}
	
	/**
	 * 
	 * @return \Wanawork\UserBundle\Entity\CvRequest
	 */
	public function getCvRequest()
	{
		return $this->cvRequest;
	}
	
	/**
	 * 
	 * @return \Wanawork\UserBundle\Entity\EmployeeProfile
	 */
	public function getEmployee()
	{
		return $this->employee;
	}
	
	/**
	 * 
	 * @return \Wanawork\UserBundle\Entity\EmployerProfile
	 */
	public function getEmployer()
	{
		return $this->employer;
	}
	
	/**
	 * Mark all messages as read for the given profile
	 * 
	 * @param AbstractProfile $profile
	 * 
	 * @return integer How many messages were marked as read
	 */
	public function markReadForUser(AbstractProfile $profile)
	{
		$messages = $this->getUnreadMessages($profile);
		foreach($messages as $message) {
		    $message->markRead();
		}
		
		return sizeof($messages);
	}
	
	/**
	 * Facebook style message date
	 * @return string | null
	 */
	public function getLastMessageDateInWords()
	{
	    $date = null;
	    if (sizeof($this->getMessages())) {
	        $date = $this->getMessages()->last()->getPostDateInWords();
	    }
	    return $date;
	}
}
