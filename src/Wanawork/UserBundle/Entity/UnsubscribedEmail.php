<?php
namespace Wanawork\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * A record of an email address and user that unsubscribed
 * @author Ilya Biryukov <ilya@wanawork.ie>
 * @ORM\Entity(repositoryClass="Wanawork\UserBundle\Repository\UnsubscribedEmailRepository")
 */
class UnsubscribedEmail 
{

	/**
	 * Email from which the link was clicked
	 * @var Wanawork\UserBundle\Entity\SystemEmail
	 * @ORM\OneToOne(targetEntity="Wanawork\UserBundle\Entity\SystemEmail")
	 */
	protected $systemEmail;
	
	/**
	 * Email to unsubscribe
	 * @var string
	 * @ORM\Column(type="string")
	 * @ORM\Id
	 * @Assert\NotBlank(message="Please enter your email")
     * @Assert\Email(message="Please enter a valid email address")
     * @Assert\Length(max=60, maxMessage="Your email is too long. Max {{ limit }} characters")
	 */
	protected $email;

	/**
	 * Date when the email
	 * @var \DateTime
	 * @ORM\Column(type="datetime")
	 * @Assert\DateTime()
	 */
	protected $dateUnsubscribed;

	public function __construct($email, SystemEmail $systemEmail = null, \DateTime $dateUnsubscribed = null) {
		if ($dateUnsubscribed === null) {
			$dateUnsubscribed = new \DateTime();
		}
        $this->email = $email;
		$this->systemEmail = $systemEmail;
		$this->dateUnsubscribed = $dateUnsubscribed;
	}

	/**
	 * @return Wanawork\UserBundle\Entity\SystemEmail SystemEmail
	 */
	public function getSystemEmail() {
		return $this->systemEmail;
	}

	/**
	 * @return \DateTime DateTime
	 */
	public function getDateUnsubscribed() {
		return $this->dateUnsubscribed;
	}
	
	public function getEmail()
	{
	    return $this->email;
	}
	
	public function getId()
	{
	    return $this->getEmail();
	}
	
	public function setEmail($email)
	{
	    $this->email = $email;
	}
}
