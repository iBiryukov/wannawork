<?php
namespace Wanawork\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Represents an Industry
 * 
 * @author Ilya Biryukov <ilya@goparty.ie>
 * @ORM\Entity(repositoryClass="Wanawork\UserBundle\Repository\SectorRepository")
 * @ORM\ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @UniqueEntity("id")
 */
class Sector
{

    /**
     * Sector ID
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @Assert\NotNull(message="Industry ID cannot be null")
     * @Assert\Type(type="integer")
     */
    protected $id;

    /**
     * Sector name
     * 
     * @var string 
     * @ORM\Column(type="string")
     * @Assert\Type(type="string", message="Industry name must be a string")
     * @Assert\NotBlank(message="Industry name cannot be blank")
     * @Assert\Length(min=1, max=255, minMessage="Industry name cannot be empty", 
     *  maxMessage="Industry name cannot be longer than {{ limit }} characters")
     * 
     */
    protected $name;

    /**
     * Type of jobs that exist in this sector
     * 
     * @var Doctrine\Common\Collections\ArrayCollection 
     * @ORM\ManyToMany(targetEntity="Wanawork\UserBundle\Entity\SectorJob", mappedBy="sectors", cascade={"all"})
     * @ORM\OrderBy({"name" = "ASC"})
     * 
     * @Assert\Count(min=1, minMessage="Industry must have at least one job")
     * @Assert\Valid()
     * @ORM\OrderBy({"name"="ASC"})
     */
    protected $jobs;
    
    /**
     * Slug for url
     * @Gedmo\Mapping\Annotation\Slug(fields={"name"})
     * @Doctrine\ORM\Mapping\Column(length=255, unique=true)
     */
    private $slug;

    /**
     * Constructor
     * @param integer   $id     Sector ID
     * @param string    $name   Sector name
     * @param array     $jobs   Sector jobs
     */
    public function __construct($id, $name, array $jobs = array())
    {
        $this->setId($id);
        $this->setName($name);
        $this->jobs = new ArrayCollection();
        foreach ($jobs as $job) {
            $this->addJob($job);
        }
    }
    
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Sector ID
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * @param string $name            
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection $jobs
     */
    public function getJobs()
    {
        return $this->jobs;
    }

    /**
     * Add new job
     * @param SectorJob $job
     */
    public function addJob(SectorJob $job)
    {
        if (!$this->getJobs()->contains($job)) {
            $this->getJobs()->add($job);
            $job->addSector($this);
        }
    }
    
    /**
     * Remove existing job
     * @param SectorJob $job
     */
    public function removeJob(SectorJob $job)
    {
        if ($this->getJobs()->contains($job)) {
            $this->getJobs()->removeElement($job);
            $job->removeSector($this);
        }
    }

    /**
     * Transform sector to string
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }
    
    public function __toArray()
    {
        return array($this->getId() => $this->getName());
    }
    
    public function getSlug()
    {
        return $this->slug;
    }
    
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }
}
