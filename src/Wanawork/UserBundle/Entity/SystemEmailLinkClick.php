<?php
namespace Wanawork\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Records individual clicks on links
 * 
 * @author Ilya Biryukov <ilya@wannawork.ie>
 * @ORM\Entity
 */
class SystemEmailLinkClick
{
    /**
     * Click ID
     * @ORM\Column(type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * Link that was clicked
     * @var \Wanawork\UserBundle\Entity\SystemEmailLink
     * @ORM\ManyToOne(targetEntity="SystemEmailLink", inversedBy="clicks")
     */
    protected $link;
    
    /**
     * IP address that clicked the link
     * @var string
     * @ORM\Column(type="string")
     */
    protected $ip;
    
    /**
     * When the link was clicked
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    protected $date;
    
    public function __construct(SystemEmailLink $link, $ip)
    {
        $this->link = $link;
        $this->ip = $ip;
        $this->date = new \DateTime();
    }

    /**
     * @return number
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \Wanawork\UserBundle\Entity\SystemEmailLink
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return clone $this->date;
    }
        
}