<?php
namespace Wanawork\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\Common\Collections\ArrayCollection;
use Wanawork\MainBundle\Entity\Language;
use Wanawork\MainBundle\Entity\County;

/**
 * Represents a job created to employer
 * @author Ilya Biryukov <ilya@wanawork.ie>
 * @ORM\Entity(repositoryClass="Wanawork\UserBundle\Repository\JobSpecRepository")
 * 
 * @ORM\HasLifecycleCallbacks
 */
class JobSpec
{
    /**
     * Job ID
     * @var integer
     * 
     * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * Job Title
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Please specify the title")
     * @Assert\Length(max=255, maxMessage="Title cannot be longer that {{ limit }} characters")
     */
    protected $title;
    
    /**
     *
     * Job description
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     * @Assert\NotBlank(message="Please enter the job description")
     * @Assert\Length(max=50000, maxMessage="Description cannot be longer that {{ limit }} characters")
     */
    protected $description;
    
    /**
     * Use that created this job spec
     * @var \Wanawork\UserBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="\Wanawork\UserBundle\Entity\User")
     * @Assert\NotNull(message="Job spec needs a user")
     * @Assert\Type(type="\Wanawork\UserBundle\Entity\User", message="Job spec needs a user")
     */
    protected $user;
    
    /**
     * Employer that owns this spec
     * @var \Wanawork\UserBundle\Entity\EmployerProfile
     *  
     * @ORM\ManyToOne(targetEntity="\Wanawork\UserBundle\Entity\EmployerProfile", inversedBy="jobSpecs")
     * @Assert\NotNull(message="Job spec needs an employer profile")
     * @Assert\Type(type="\Wanawork\UserBundle\Entity\EmployerProfile", message="Job spec needs an employer profile")
     */
    protected $employer;
    
    /**
     * Is this spec public
     * @var boolean
     * @ORM\Column(type="boolean")
     * @Assert\NotBlank
     */
    protected $public;
    
    /**
     * When the job was created
     * @var \DateTime
     * @ORM\Column(type="datetime")
     * @Assert\NotNull
     * @Assert\DateTime
     */
    protected $createdOn;

    /**
     * Profession in which the user wishes to work
     * @var \Wanawork\UserBundle\Entity\SectorJob
     * @ORM\ManyToOne(targetEntity="Wanawork\UserBundle\Entity\SectorJob")
     * @Assert\NotNull(message="Please pick required profession")
     * @Assert\Type(type="\Wanawork\UserBundle\Entity\SectorJob", message="Please pick required profession")
     * @Assert\Valid()
     */
    protected $profession;
    
    /**
     * List of industries where the candidate wants to work
     * @var \Doctrine\Common\Collections\ArrayCollection
     * @ORM\ManyToMany(targetEntity="Wanawork\UserBundle\Entity\Sector")
     * @Assert\Valid()
     */
    protected $industries;
    
    /**
     * Positions, that were searched for
     * @var Doctrine\Common\Collections\ArrayCollection
     * @ORM\ManyToMany(targetEntity="PositionType", cascade={"persist"})
     * @Assert\Valid()
     */
    protected $positions;
    
    /**
     * Languages, that were searched for
     * @var Doctrine\Common\Collections\ArrayCollection
     * @ORM\ManyToMany(targetEntity="Wanawork\MainBundle\Entity\Language", cascade={"persist"})
     * @Assert\Valid()
     */
    protected $languages;
    
    /**
     * Locations, that were searched for
     * @var Doctrine\Common\Collections\ArrayCollection
     * @ORM\ManyToMany(targetEntity="Wanawork\MainBundle\Entity\County", cascade={"persist"})
     * @Assert\Valid()
     */
    protected $locations;
    
    /**
     * Experience that the candidate has in the above profession
     * @var \Wanawork\UserBundle\Entity\SectorExperience
     * @ORM\ManyToOne(targetEntity="Wanawork\UserBundle\Entity\SectorExperience")
     * @Assert\Type(type="\Wanawork\UserBundle\Entity\SectorExperience", message="Please choose required experience")
     * @Assert\Valid()
     */
    protected $experience;
    
    /**
     * Level of education
     * @var \Wanawork\UserBundle\Entity\Qualification
     * @ORM\ManyToOne(targetEntity="Wanawork\UserBundle\Entity\Qualification")
     *
     * @Assert\Valid()
     */
    protected $educationLevel;
    
    /**
     * Information about salary
     * @var string
     * @ORM\Column(type="string", length=250, nullable=true)
     */
    protected $salary;
    
    
    public function __construct()
    {
        $this->public = true;
        $this->createdOn = new \DateTime();
        $this->industries = new ArrayCollection();
        $this->languages = new ArrayCollection();
        $this->locations = new ArrayCollection();
        $this->positions = new ArrayCollection();
    }
    
    /**
     *
     * @return the integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     *
     * @return \Wanawork\UserBundle\Entity\EmployerProfile
     */
    public function getEmployer()
    {
        return $this->employer;
    }

    /**
     *
     * @param \Wanawork\UserBundle\Entity\EmployerProfile  $employer
     */
    public function setEmployer($employer)
    {
        $this->employer = $employer;
    }

    /**
     *
     * @return \Wanawork\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     *
     * @param \Wanawork\UserBundle\Entity\User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     *
     * @param string $title            
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     *
     * @param string $description            
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }
    
    public function isPublic()
    {
        return $this->public;
    }
    
    public function setPublic($public)
    {
        $this->public = $public;
    }
    
    /**
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getLanguages()
    {
        return $this->languages;
    }
    
    public function setLanguages(array $languages)
    {
        foreach ($this->getLanguages() as $language) {
            $this->removeLanguage($language);
        }
    
        foreach ($languages as $language) {
            $this->addLanguage($language);
        }
    }
    
    public function addLanguage(Language $language)
    {
        if (!$this->getLanguages()->contains($language)) {
            $this->getLanguages()->add($language);
        }
    }
    
    public function removeLanguage(Language $language)
    {
        if ($this->getLanguages()->contains($language)) {
            $this->getLanguages()->removeElement($language);
        }
    }
    
    /**
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getLocations() {
        return $this->locations;
    }
    
    
    public function setLocations(array $locations)
    {
        foreach ($this->getLocations() as $location) {
            $this->removeLocation($location);
        }
    
        foreach ($locations as $location) {
            $this->addLocation($location);
        }
    }
    
    public function addLocation(County $location)
    {
        if (!$this->getLocations()->contains($location)) {
            $this->getLocations()->add($location);
        }
    }
    
    public function removeLocation(County $location)
    {
        if ($this->getLocations()->contains($location)) {
            $this->getLocations()->removeElement($location);
        }
    }
    
    /**
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getPositions() {
        return $this->positions;
    }
    
    public function setPositions(array $positions)
    {
        foreach ($this->getPositions() as $position) {
            $this->removePosition($position);
        }
    
        foreach ($positions as $position) {
            $this->addPosition($position);
        }
    }
    
    public function addPosition(PositionType $position)
    {
        if (!$this->getPositions()->contains($position)) {
            $this->getPositions()->add($position);
        }
    }
    
    public function removePosition(PositionType $position)
    {
        if ($this->getPositions()->contains($position)) {
            $this->getPositions()->removeElement($position);
        }
    }
    
    /**
     *
     * @return \Wanawork\UserBundle\Entity\SectorJob
     */
    public function getProfession()
    {
        return $this->profession;
    }
    
    /**
     *
     * @param $profession
     */
    public function setProfession($profession)
    {
        $this->profession = $profession;
    }
    
    /**
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getIndustries()
    {
        return $this->industries;
    }
    
    /**
     *
     * @param $industries
     */
    public function setIndustries(array $industries)
    {
        foreach ($this->getIndustries() as $industry) {
            $this->removeIndustry($industry);
        }
    
        foreach ($industries as $industry) {
            $this->addIndustry($industry);
        }
    }
    
    public function addIndustry(Sector $industry)
    {
        if (!$this->getIndustries()->contains($industry)) {
            $this->getIndustries()->add($industry);
        }
    }
    
    public function removeIndustry(Sector $industry)
    {
        if ($this->getIndustries()->contains($industry)) {
            $this->getIndustries()->removeElement($industry);
        }
    }
    
    /**
     *
     * @return the SectorExperience
     */
    public function getExperience()
    {
        return $this->experience;
    }
    
    /**
     *
     * @param $experience
     */
    public function setExperience($experience)
    {
        $this->experience = $experience;
    }
    
    /**
     *
     * @return the Qualification
     */
    public function getEducationLevel()
    {
        return $this->educationLevel;
    }
    
    /**
     *
     * @param $educationLevel
     */
    public function setEducationLevel($educationLevel)
    {
        $this->educationLevel = $educationLevel;
    }

    /**
     *
     * @return the string
     */
    public function getSalary()
    {
        return $this->salary;
    }

    /**
     *
     * @param string $salary            
     */
    public function setSalary($salary)
    {
        $this->salary = $salary;
    }
    
    public function getCreatedOn()
    {
        return clone $this->createdOn;
    }
    
}