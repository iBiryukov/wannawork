<?php
namespace Wanawork\UserBundle\Entity\Billing;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Wanawork\UserBundle\Entity\Ad;
use Wanawork\UserBundle\Entity\EmployerProfile;

/**
 * Order made by employers to be verified
 * @author Ilya Biryukov <ilya@wanawork.ie>
 * @ORM\Entity
 */
class VerificationOrder extends Order
{
    /**
     * Employer for whome the verification order is
     * @var \Wanawork\UserBundle\Entity\EmployerProfile
     * @ORM\ManyToOne(targetEntity="Wanawork\UserBundle\Entity\EmployerProfile", inversedBy="verificationOrders")
     */
    private $employer;
    
    public function __construct(EmployerProfile $employer, $amount, $vatRate)
    {
        parent::__construct($amount, $vatRate);
        $this->employer = $employer;
        $employer->addVerificationOrder($this);
    }
    
    public function getInternalDescription()
    {
        return sprintf(
            'Order for Employer: %d',
            $this->getEmployer()->getId()
        );
    }
    
    public function getOrderType()
    {
        return 'Verification Order';
    }

    /**
     * @return \Wanawork\UserBundle\Entity\EmployerProfile
     */
    public function getEmployer()
    {
        return $this->employer;
    }
    
    public function getPublicDescription()
    {
        return sprintf(
	       "Company Verification for \"%s\" ",
            $this->getEmployer()->getCompanyName()
        );
    }
    
    public function getPayer()
    {
        return $this->getEmployer();
    }
    
    public function getReceiptFileName()
    {
        return sprintf(
            "Employer_Verfication_%d_%s.pdf",
            $this->getId(),
            $this->getPayDate()->format('Y_m_d')
        );
    }
}