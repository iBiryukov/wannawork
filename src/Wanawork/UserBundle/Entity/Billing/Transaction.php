<?php
namespace Wanawork\UserBundle\Entity\Billing;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Wanawork\UserBundle\Entity\User;

/**
 * 
 * @author Ilya Biryukov <ilya@wanawork.ie>
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Transaction
{
    const STATE_FAIL     = 2;
    const STATE_PENDING  = 3;
    const STATE_SUCCESS  = 5;
    
    static $validStates = array(
    	'fail'     =>   self::STATE_FAIL,
        'success'  =>   self::STATE_SUCCESS,
        'pending'  =>   self::STATE_PENDING,
    );
    
    const TRANSACTION_TYPE_CREATE  = 1;
    const TRANSACTION_TYPE_APPROVE = 2;
    const TRANSACTION_TYPE_REFUND  = 3;
    const TRANSACTION_TYPE_DEPOSIT = 4;
    const TRANSACTION_TYPE_CHECK_SALE = 5;
    
    static $validTypes = array(
    	'approve' => self::TRANSACTION_TYPE_APPROVE,
        'create' => self::TRANSACTION_TYPE_CREATE,
        'deposit' => self::TRANSACTION_TYPE_DEPOSIT,
        'refund'  => self::TRANSACTION_TYPE_REFUND,
        'check_sale' => self::TRANSACTION_TYPE_CHECK_SALE,
    );
    
    /**
     * ID
     * @var integer
     * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * Payment
     * @var \Wanawork\UserBundle\Entity\Billing\Payment
     * @ORM\ManyToOne(targetEntity="\Wanawork\UserBundle\Entity\Billing\Payment", inversedBy="transactions")
     */
    private $payment;
    
    /**
     * Status
     * @var integer
     * @ORM\Column(type="smallint")
     */
    private $state;
    
    /**
     * When the transaction was created
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $createdAt;
    
    /**
     * When the transaction was updated
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;
    
    /**
     * A remoted/alternative id to track this transaction by
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $trackingId;
    
    /**
     * Array of random data
     * @var array
     * @ORM\Column(type="array", nullable=true)
     */
    private $data;
    
    /**
     * Transaction type
     * @var integer
     * @ORM\Column(type="smallint")
     */
    private $type;
    
    /**
     * Explanation why the transaction failed
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $failReason;
    
    /**
     * User that carried out the transaction
     * @var \Wanawork\UserBundle\Entity\User | null
     * @ORM\ManyToOne(targetEntity="Wanawork\UserBundle\Entity\User")
     */
    private $user;
    
    public function __construct(Payment $payment)
    {
        $this->data = array();
        $this->createdAt = new \DateTime();
        $this->setPayment($payment);
    }

    /**
     *
     * @return the integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     *
     * @return \Wanawork\UserBundle\Entity\Billing\Payment
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     *
     * @param $payment
     */
    public function setPayment(Payment $payment)
    {
        $this->payment = $payment;
        $payment->addTransaction($this);
    }

    /**
     *
     * @return the integer
     */
    public function getState()
    {
        return $this->state;
    }
    
    public function getStateName()
    {
        return array_search($this->getState(), self::$validStates);
    }

    /**
     *
     * @param $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     *
     * @return the DateTime
     */
    public function getCreatedAt()
    {
        return clone $this->createdAt;
    }

    /**
     *
     * @param \DateTime $createdAt            
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     *
     * @return the DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     *
     * @param \DateTime $updatedAt            
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     *
     * @return the string
     */
    public function getTrackingId()
    {
        return $this->trackingId;
    }

    /**
     *
     * @param string $trackingId            
     */
    public function setTrackingId($trackingId)
    {
        $this->trackingId = $trackingId;
    }

    /**
     *
     * @return the array
     */
    public function getData()
    {
        return $this->data;
    }

    public function setData($key, $value)
    {
        $this->data[$key] = $value;
    }
    
    public function hasKey($key)
    {
        return array_key_exists($key, $this->data);
    }
    
    public function getDataByKey($key)
    {
        $data = null;
        if ($this->hasKey($key)) {
            $data = $this->data[$key];
        }
        return $data;
    }

    /**
     * @return the integer
     */
    public function getType()
    {
        return $this->type;
    }
    
    public function getTypeName()
    {
        return array_search($this->getType(), self::$validTypes);
    }

    /**
     * @param $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     *
     * @return the string
     */
    public function getFailReason()
    {
        return $this->failReason;
    }

    /**
     *
     * @param string $failReason            
     */
    public function setFailReason($failReason)
    {
        $this->failReason = $failReason;
    }
    
    /** @ORM\PreUpdate */
    public function preUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser(User $user)
    {
        $this->user = $user;
    }
    
}