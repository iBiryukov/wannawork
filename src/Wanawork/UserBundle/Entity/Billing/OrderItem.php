<?php
namespace Wanawork\UserBundle\Entity\Billing;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * 
 * @author Ilya Biryukov <ilya@wanawork.ie>
 * @ORM\Entity
 */
class OrderItem
{
    /**
     * Order Item ID
     * @var string
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * Item name
     * @var string
     * @ORM\Column(type="string")
     */
    private $name;
    
    /**
     * Item Price in cents, without any taxes
     * @var string
     * @ORM\Column(type="integer")
     */
    private $price;
    
    /**
     * Item quantity
     * @var integer
     * @ORM\Column(type="smallint")
     */
    private $quantity;
    
    /**
     * Order
     * @var \Wanawork\UserBundle\Entity\Billing\Order
     * @ORM\ManyToOne(targetEntity="Wanawork\UserBundle\Entity\Billing\Order", inversedBy="items")
     */
    private $order;
    
    
    public function __construct($name, $price, $quantity, Order $order)
    {
        $this->name = $name;
        $this->price = $price;
        $this->quantity = $quantity;
        $this->order = $order;
        $order->addItem($this);        
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getQuantity()
    {
        return $this->quantity;
    }

    public function getOrder()
    {
        return $this->order;
    }
    
}