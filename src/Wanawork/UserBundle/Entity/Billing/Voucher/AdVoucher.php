<?php
namespace Wanawork\UserBundle\Entity\Billing\Voucher;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Wanawork\UserBundle\Entity\Billing\Order;
use Wanawork\UserBundle\Entity\Billing\AdOrder;
use Wanawork\UserBundle\Entity\EmployeeProfile;
use Wanawork\UserBundle\Entity\Ad;
use Wanawork\UserBundle\Entity\Billing\Payment;
use Wanawork\UserBundle\Entity\Billing\Transaction;

/**
 * Voucher used to pay for ads
 * @author Ilya Biryukov <ilya@wannawork.ie>
 *
 * @ORM\Entity
 */
class AdVoucher extends Voucher
{
    /**
     * How many times can a voucher be used in total
     * @var integer
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\GreaterThan(value=0)
     */
    private $globalLimit;
    
    /**
     * How many times can this voucher be used by the same user
     * @var integer
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\GreaterThan(value=0)
     */
    private $userLimit;
    
	public function supports(Order $order)
	{
        return 
            $order instanceof AdOrder && 
            !$this->globalLimitReached() && 
            !$this->userLimitReached($order->getAd()->getProfile());	        
	}
	
	/**
	 * Have we used up the voucher?
	 * 
	 * @return boolean
	 */
	public function globalLimitReached()
	{
	    $limit = $this->getGlobalLimit();
	    return $limit !== null && sizeof($this->getRedemptions()) >= $limit;
	}
	
	/**
	 * Has the given user used up the voucher?
	 * @param  EmployeeProfile $profile
	 * 
	 * @return boolean
	 */
	public function userLimitReached(EmployeeProfile $profile)
	{
	    $limitReached = false;
	    $limit = $this->getUserLimit();
	    if ($limit !== null) {
	        $voucherUseCount = 0;
	        foreach ($profile->getAds() as $ad) {
	            foreach($ad->getOrders(Order::STATUS_PAID) as $order) {
	                $payments = $order->getPayments(Payment::STATE_DEPOSITED, Payment::SOURCE_VOUCHER);
	                foreach ($payments as $payment) {
	                    foreach($payment->getTransactions() as $transaction) {
	                        if ($transaction->getType() === Transaction::TRANSACTION_TYPE_DEPOSIT && 
	                            $transaction->getState() === Transaction::STATE_SUCCESS &&
	                            $transaction->getTrackingId() === $this->getCode()) {
	                            ++$voucherUseCount;        
	                        }
	                    }
	                }   
	            }
	        }
	        
	        $limitReached = ($voucherUseCount >= $limit);
	    }
	    
	    return $limitReached;
	}

    /**
     * @return integer
     */
    public function getGlobalLimit()
    {
        return $this->globalLimit;
    }

    /**
     * @param $globalLimit
     */
    public function setGlobalLimit($globalLimit)
    {
        $this->globalLimit = $globalLimit;
    }

    /**
     * @return integer
     */
    public function getUserLimit()
    {
        return $this->userLimit;
    }

    /**
     * @param $userLimit
     */
    public function setUserLimit($userLimit)
    {
        $this->userLimit = $userLimit;
    }
	
}