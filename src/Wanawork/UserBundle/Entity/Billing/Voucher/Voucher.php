<?php
namespace Wanawork\UserBundle\Entity\Billing\Voucher;

use Wanawork\UserBundle\Entity\Billing\Order;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Wanawork\UserBundle\Entity\Billing\Payment;

/**
 * Abstract voucher
 * @author Ilya Biryukov <ilya@wannawork.ie>
 * 
 * @ORM\Entity(repositoryClass="Wanawork\UserBundle\Repository\VoucherRepository")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({"AdVoucher" = "AdVoucher"})
 */
abstract class Voucher
{
    /**
     * Code to redeem the voucher
     * @var string
     * @ORM\Column(type="string", length=100)
     * @ORM\Id
     * @Assert\NotNull
     * @Assert\Length(max=100)
     */
    private $id;
    
    /**
     * List of payments where this voucher was used
     * @var \Doctrine\Common\Collections\ArrayCollection
     * @ORM\ManyToMany(targetEntity="Wanawork\UserBundle\Entity\Billing\Payment", fetch="EXTRA_LAZY")
     */
    private $redemptions;
    
    final public function __construct($code)
    {
        $this->setCode($code);
        $this->redemptions = new ArrayCollection();
    }
    
    public function setCode($code)
    {
        $this->id = $code;
    }
    
    public function getCode()
    {
        return $this->id;
    }
    
    public function addRedemptions(Payment $payment)
    {
        if ($payment->getSource() !== Payment::SOURCE_VOUCHER) {
            throw new \InvalidArgumentException("Vouchers can only accept payments with 'voucher' source");
        }
        $this->getRedemptions()->add($payment);
    }
    
    public function getRedemptions()
    {
        return $this->redemptions;
    }
    
    /**
     * Can this voucher pay for the given order
     * @param Order $order
     * @return boolean
     */
    abstract public function supports(Order $order);
}