<?php
namespace Wanawork\UserBundle\Entity\Billing;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Payment Entity
 * @author Ilya Biryukov <ilya@wanawork.ie>
 * @ORM\Entity(repositoryClass="Wanawork\UserBundle\Repository\PaymentRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Payment
{
    const STATE_CANCELED   =  3;
    const STATE_EXPIRED    =  4;
    const STATE_FAILED     =  5;
    const STATE_PENDING    =  6;
    const STATE_CLEARING   =  7;
    const STATE_DEPOSITED  =  8;
    
    static $validStates = array(
    	'cancelled'  =>  self::STATE_CANCELED,
        'expired'    =>  self::STATE_EXPIRED,
        'failed'     =>  self::STATE_FAILED,
        'pending'    =>  self::STATE_PENDING,
        'clearing'   =>  self::STATE_CLEARING,
        'deposited'  =>  self::STATE_DEPOSITED,
    );
    
    const SOURCE_PAYPAL  = 1;
    //const SOURCE_CHEQUE  = 2;
    const SOURCE_VOUCHER = 3;
    const SOURCE_TWITTER = 4;
    
    static $validSources = array(
    	'paypal'  =>  self::SOURCE_PAYPAL,
        //'cheque'  =>  self::SOURCE_CHEQUE,
        'voucher' =>  self::SOURCE_VOUCHER,
        'twitter' =>  self::SOURCE_TWITTER,
    );
    
    /**
     * 
     * @var integer
     * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * Order State
     * @var integer
     * @ORM\Column(type="smallint")
     */
    private $state;
    
    /**
     * Amount to pay
     * @var string
     * @ORM\Column(type="integer")
     */
    private $amount;
    
    /**
     * When the payment was created
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $createdAt;
    
    /**
     * When the order was updated
     * @var \DateTime | null
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;
    
    /**
     * 
     * Order for which this payment is
     * @var \Wanawork\UserBundle\Entity\Billing\Order
     * @ORM\ManyToOne(targetEntity="\Wanawork\UserBundle\Entity\Billing\Order", inversedBy="payments")
     */
    private $order;
    
    /**
     * List of transactions
     * @var \Doctrine\Common\Collections\ArrayCollection
     * @ORM\OneToMany(targetEntity="Wanawork\UserBundle\Entity\Billing\Transaction", mappedBy="payment", 
     * cascade={"persist"})
     * @ORM\OrderBy({"id"="ASC"})
     */
    private $transactions;
    
    /**
     * Source of payment
     * @var integer
     * @ORM\Column(type="smallint")
     */
    private $source;
    
    public function __construct(Order $order, $amount, $source)
    {
        $this->setState(self::STATE_PENDING);
        $this->transactions = new ArrayCollection();
        $this->createdAt = new \DateTime();
        $this->setOrder($order);
        $this->setAmount($amount);
        $this->setSource($source);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getState()
    {
        return $this->state;
    }
    
    public function getStateName()
    {
        return array_search($this->getState(), self::$validStates);
    }

    public function setState($state)
    {
        if (!in_array($state, self::$validStates, true)) {
            throw new \InvalidArgumentException(
    	       sprintf('Invalid state provided: "%s". Valid values: "%s"', 
    	           $state, implode(', ', self::$validStates))
            );
        }
        $this->state = $state;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    public function getCreatedAt()
    {
        return clone $this->createdAt;
    }

    public function getUpdatedAt()
    {
        return clone $this->updatedAt;
    }

    public function getOrder()
    {
        return $this->order;
    }

    public function setOrder(Order $order)
    {
        $this->order = $order;
        $order->addPayment($this);
    }

    public function getTransactions()
    {
        return $this->transactions;
    }

    public function addTransaction(Transaction $transaction)
    {
        if (!$this->getTransactions()->contains($transaction)) {
            
            if ($transaction->getType() === Transaction::TRANSACTION_TYPE_CREATE && $this->hasCreateTransaction()) {
                throw new \Exception("Payment can only have one create transaction");
            }
            $this->getTransactions()->add($transaction);
            $transaction->setPayment($this);
        }
    }

    /**
     * Get the transaction that created the payment in the gateway system
     * @return \Wanawork\UserBundle\Entity\Billing\Transaction | null§
     */
    public function getCreateTransaction()
    {
        $transaction = null;
        foreach ($this->getTransactions() as $t) {
            if($t->getType() === Transaction::TRANSACTION_TYPE_CREATE) {
                $transaction = $t;
            }
        }
        return $transaction;
    }
    
    /**
     * Check if we have a transaction that created a payment in the gateway system
     * @return boolean
     */
    public function hasCreateTransaction()
    {
        return 
        $this->getTransactions()->exists(function($key, Transaction $transaction){
        	return $transaction->getType() === Transaction::TRANSACTION_TYPE_CREATE;
        });
    }
    
    public function getSource()
    {
        return $this->source;
    }
    
    public function getSourceName()
    {
        return array_search($this->getSource(), self::$validSources);
    }

    public function setSource($source)
    {
        if (!in_array($source, self::$validSources, true)) {
            throw new \InvalidArgumentException(
                sprintf('Invalid source provided: "%s". Valid values: "%s"', 
                    $source, implode(', ', self::$validSources))
            );
        }
        $this->source = $source;
    }
    
    /** @ORM\PreUpdate */
    public function preUpdate()
    {
        $this->setUpdatedAt(new \DateTime());
    }
    
    public function setUpdatedAt(\DateTime $date)
    {
        $this->updatedAt = $date;
    }
    
    /**
     * 
     * @param integer $sourceId
     * @return string | null
     */
    public static function getSourceById($sourceId)
    {
        return array_search($sourceId, self::$validSources);
    }
        
}