<?php
namespace Wanawork\UserBundle\Entity\Billing;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Wanawork\UserBundle\Entity\Ad;

/**
 * AdOrder Entity
 * @author Ilya Biryukov <ilya@wanawork.ie>
 * @ORM\Entity(repositoryClass="Wanawork\UserBundle\Repository\AdOrderRepository")
 */
class AdOrder extends Order
{
    /**
     * Ad to which this order belongs
     * @var \Wanawork\UserBundle\Entity\Ad
     * @ORM\ManyToOne(targetEntity="Wanawork\UserBundle\Entity\Ad", inversedBy="orders")
     * @Assert\NotNull
     */
    private $ad;
    
    /**
     * Plan which is being ordered
     * @var integer
     * @ORM\Column(type="smallint")
     */
    private $plan;
    
    /**
     * How many days will the ad be active?
     * @var integer
     * @ORM\Column(type="smallint")
     */
    private $duration;
    
    /**
     * Once the order is paid, when will it start running?
     * This date will be calculated based on the current expiry date of the ad
     * @var \DateTime | null
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $runFrom;
    
    /**
     * How many bumps allowed
     * @var integer
     * @ORM\Column(type="integer", nullable=true)
     */
    private $bumps;
    
    /**
     * Is this order placed to extend the ad?
     * Keep for statistical purposes
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isExtension;
    
    public function __construct(Ad $ad, $amount, $vatRate, $plan, $duration, $bumps, $isExtension = false)
    {
        parent::__construct($amount, $vatRate);
        $this->setAd($ad);
        $this->setPlan($plan);
        $this->setDuration($duration);
        $this->bumps = $bumps;
        $this->isExtension = $isExtension;
    }
    
    public function setAd(Ad $ad)
    {
        $this->ad = $ad;
        $ad->addOrder($this);
    }
    
    /**
     * 
     * @return \Wanawork\UserBundle\Entity\Ad
     */
    public function getAd()
    {
        return $this->ad;
    }
    
    public function getPlan()
    {
        return $this->plan;
    }
    
    public function getPlanName()
    {
        return array_search($this->getPlan(), Ad::getPricePlans());
    }
    
    private function setPlan($plan)
    {
        if (!in_array($plan, Ad::getPricePlans())) {
            throw new \InvalidArgumentException(
	           sprintf('Invalid plan id provided: "%s". Valid valus: "%s"', $plan, implode(', ', Ad::getPricePlans()))
            );
        }
        $this->plan = $plan;
    }
    
    private function setDuration($duration)
    {
        if (!is_int($duration)) {
            throw new \InvalidArgumentException(sprintf('Duration type must be integer. Got: "%s"', $duration));
        }
        
        if ($duration <= 0) {
            throw new \InvalidArgumentException(
	           sprintf('Duration must be greater than 0. Got: "%s"', $duration)
            );
        }
        
        $this->duration = $duration;
    }
    
    public function getInternalDescription()
    {
        return sprintf(
	       'Order for AD: %d',
            $this->getAd()->getId()
        );
    }
    
    public function getOrderType()
    {
        return 'Ad Order';
    }

    public function getDuration()
    {
        return $this->duration;
    }

    public function getRunFrom()
    {
        if ($this->runFrom) {
            return clone $this->runFrom;
        }
        return null;
    }

    public function setRunFrom(\DateTime $runFrom)
    {
        $this->runFrom = $runFrom;
    }
    
    /**
     * When the ad based on this order will expire
     * @return \DateTime | NULL
     */
    public function getExpiry()
    {
        $expiry = null;
        if ($this->getRunFrom()) {
            $expiry = clone $this->getRunFrom(); 
            $expiry->add(new \DateInterval("P{$this->getDuration()}D"));
        }
        return $expiry;
    }
    
    /**
     * How many bumps were included in the order
     * @return number
     */
    public function getBumps()
    {
        $bumps = $this->bumps;
        if (!is_int($bumps)) {
            $bumps = 0;
        }
        return $bumps;
    }
    
    public function isExtension()
    {
        return $this->isExtension;
    }
    
    public function getPublicDescription()
    {
        return sprintf(
	       "%s Subscription for %d days",
            ucfirst($this->getPlanName()),
            $this->getDuration()
        );
    }
    
    /**
     * (non-PHPdoc)
     * @see \Wanawork\UserBundle\Entity\Billing\Order::getPayer()
     */
    public function getPayer()
    {
        return $this->getAd()->getProfile();
    }
    
    public function getReceiptFileName()
    {
        return sprintf(
	       "Ad_%d_%s.pdf",
            $this->getId(),
            $this->getPayDate()->format('Y_m_d')
        );
    }
}
