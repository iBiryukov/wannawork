<?php
namespace Wanawork\UserBundle\Entity\Billing;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Order Entity
 * !---!
 *  All amounts are in cents
 * !---!
 * @author Ilya Biryukov <ilya@wanawork.ie>
 * @ORM\Entity(repositoryClass="Wanawork\UserBundle\Repository\OrderRepository")
 * @ORM\Table(name="order_generic")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({
 *   "AdOrder" = "AdOrder",
 *   "VerificationOrder" = "VerificationOrder"
 * })
 * @Assert\Callback(methods={"isAuthorValid"})
 */
abstract class Order
{
    const STATUS_PENDING    =   1;
    const STATUS_PAID       =   2;
    const STATUS_CANCELLED  =   3;
    const STATUS_FAILED     =   4;
    
    static $validStatuses = array(
    	'paid'      => self::STATUS_PAID,
        'pending'   => self::STATUS_PENDING,
        'cancelled' => self::STATUS_CANCELLED,
        'failed'    => self::STATUS_FAILED,
    );
    
    /**
     * Order ID
     * @var string
     * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * Total amount to pay, excluding taxes
     * @var string
     * @ORM\Column(type="integer")
     * @Assert\Range(min=1)
     */
    private $amount;
    
    /**
     * Vat percentage
     * e.g 23
     * @var float
     * @ORM\Column(type="smallint")
     * @Assert\NotNull
     * @Assert\Range(min=0)
     */
    private $vatRate;
    
    /**
     * VAT amount in this payment
     * @var integer
     * @ORM\Column(type="integer")
     */
    private $vat;
    
    /**
     * Payments log
     * @var \Doctrine\Common\Collections\ArrayCollection
     * @ORM\OneToMany(targetEntity="Wanawork\UserBundle\Entity\Billing\Payment", mappedBy="order", cascade={"persist"})
     */
    private $payments;
    
    /**
     * When the order was created
     * @var \DateTime
     * @ORM\Column(type="datetime")
     * @Assert\NotNull
     */
    private $createdAt;
    
    /**
     * Order status
     * @var integer
     * @ORM\Column(type="smallint")
     */
    private $status;
    
    /**
     * Order items
     * @var \Doctrine\Common\Collections\ArrayCollection
     * @ORM\OneToMany(targetEntity="Wanawork\UserBundle\Entity\Billing\OrderItem", mappedBy="order", cascade={"persist"})
     */
    private $items;
    
    /**
     * User that created the order
     * @var \Wanawork\UserBundle\Entity\User | null
     * @ORM\ManyToOne(targetEntity="Wanawork\UserBundle\Entity\User")
     */
    private $user;
    
    /**
     * Was a receipt generated for this order?
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $receiptEmailed;
    
    public function __construct($amount, $vatRate)
    {
        $this->amount = $amount;
        $this->vatRate = $vatRate; 
        $this->status = self::STATUS_PENDING;
        $this->createdAt = new \DateTime();   
        $this->vat = (int)floor($amount * $vatRate / 100);
        $this->payments = new ArrayCollection();
        $this->items = new ArrayCollection();
        $this->receiptGenerated = false;
    }

    /**
     *
     * @return the string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get amount excluding VAT
     * @return integer
     */
    public function getAmount()
    {
        return $this->amount;
    }
    
    /**
     * Get how much money we got after the government's thirst was quenched 
     * @return number
     */
    public function getAmountExcludingVat()
    {
        return $this->amount;
    }
    
    public function getAmountIncludingVat()
    {
        return $this->getAmountExcludingVat() + $this->getVat();
    }

    /**
     *
     * @return the float
     */
    public function getVatRate()
    {
        return $this->vatRate;
    }

    /**
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return clone $this->createdAt;
    }

    /**
     *
     * @return the integer
     */
    public function getStatus()
    {
        return $this->status;
    }
    
    public function getStatusName()
    {
        return array_search($this->getStatus(), self::$validStatuses);
    }
    
    /**
     * How much vat was paid
     * @return integer
     */
    public function getVat()
    {
        return $this->vat;
    }
    

    /**
     *
     * @param $status
     */
    public function setStatus($status)
    {
        if (!in_array($status, self::$validStatuses, true)) {
            throw new \InvalidArgumentException(sprintf(
                "Invalid order status provided: '%s'. Valid values: '%s'" 
            , $status, implode(', ', self::$validStatuses)));
        }
        $this->status = $status;
    }
    
    public function isValidStatus(ExecutionContextInterface $context)
    {
        if (!in_array($this->getStatus(), self::$validStatuses, true)) {
            $context->addViolationAt('status', "Invalid status provided: '{$this->getStatus()}'");
        }
    }
    
    /**
     * 
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getPayments($state = null, $source = null)
    {
        $payments = $this->payments;
        if ($state !== null) {
            $payments = 
            $payments->filter(function(Payment $payment) use ($state){
            	return $payment->getState() === $state;
            });
        }
        
        if ($source !== null) {
            $payments =
            $payments->filter(function(Payment $payment) use ($source){
                return $payment->getSource() === $source;
            });
        }
        return $payments;
    }
    
    public function addPayment(Payment $payment)
    {
        if (!$this->getPayments()->contains($payment)) {
            $this->getPayments()->add($payment);
            $payment->setOrder($this);
        }
    }
    
    /**
     * 
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getTransactions()
    {
        $transactions = array();
        foreach ($this->getPayments() as $payment) {
            foreach ($payment->getTransactions() as $t) {
                $transactions[] = $t;
            }
        }
        return $transactions;
    }
    
//    /**
//     * Calculate how much money was deposited for this order
//     * @return string
//     */
//     public function getDepositedAmount()
//     {
//         $amount = 0;
//         foreach ($this->getPayments() as $payment) {
//             if ($payment->getState() === Payment::STATE_DEPOSITED) {
//                 $amount += $payment->getAmount();
//             }
//         }
//         return $amount;
//     }
    
//     /**
//      * Calculate how much money is left to pay on this order
//      * @return string
//      */
//     public function getLeftAmount()
//     {
//         return $this->getAmount() - $this->getDepositedAmount();
//     }
    
    /**
     * 
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getItems()
    {
        return $this->items;
    }
    
    public function addItem(OrderItem $item)
    {
        if (!$this->getItems()->contains($item)) {
            $this->getItems()->add($item);
        }
    }
    
    /**
     * Check when the order was payed
     * @return \DateTime | null
     */
    public function getPayDate()
    {
        if($this->getStatus() !== self::STATUS_PAID) {
            return null;
        } 
               
        $payDates = array();
        foreach ($this->getTransactions() as $transaction) {
            if($transaction instanceof Transaction && 
               $transaction->getPayment()->getState() === Payment::STATE_DEPOSITED &&
               $transaction->getType() === Transaction::TRANSACTION_TYPE_DEPOSIT &&
               $transaction->getState() === Transaction::STATE_SUCCESS) {
               
                $payDates[] = $transaction->getCreatedAt();
                break;
            }
        }
        
        if (sizeof($payDates) === 0 && $this->getAmount() === 0) {
            $payDates[] = $this->getCreatedAt();
        }
        
        if (!sizeof($payDates)) {
            throw new \Exception("Unable to determine pay date");
        }
        
        // return the latest date
        return max($payDates);
    }
    
    /**
     * Get generic description of the order for internal purposes
     * @return string
     */
    abstract public function getInternalDescription();
    
    /**
     * Get description used to display it the public
     * @return string
     */
    abstract public function getPublicDescription();
    
    abstract public function getOrderType();
    
    /**
     * @return \Wanawork\UserBundle\Entity\AbstractProfile
     */
    abstract public function getPayer();
    
    /**
     * Get the file name for the receipt
     * @return string
     */
    abstract public function getReceiptFileName();

    public function getUser()
    {
        return $this->user;
    }

    public function setUser($user)
    {
        $this->user = $user;
    }
    
    /**
     * @return boolean
     */
    public function isReceiptEmailed()
    {
        return $this->receiptEmailed === true;
    }
    
    public function setReceiptEmailed($flag)
    {
        $this->receiptEmailed = $flag;
    }
}