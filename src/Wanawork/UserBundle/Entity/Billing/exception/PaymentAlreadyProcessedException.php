<?php
namespace Wanawork\UserBundle\Entity\Billing\exception;

class PaymentAlreadyProcessedException extends \Exception
{
}