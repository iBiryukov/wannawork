<?php
namespace Wanawork\UserBundle\Entity\Billing\exception;

class ApprovalRequiredException extends \Exception
{
    private $url;
    
    public function __construct ($url, $message = null, $code = null, $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->url = $url;
    }
    
    public function getUrl()
    {
        return $this->url;
    }
}