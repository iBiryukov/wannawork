<?php
namespace Wanawork\UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;

/**
 * Education Entity
 * @author Ilya Biryukov <ilya@goparty.ie>
 * @ORM\Entity
 * 
 * @Assert\Callback(methods={"isFinishDateGreaterThanStart"})
 */
class Education 
{

	/**
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;
	
	/**
	 * Course start date
	 * @var \DateTime
	 * @ORM\Column(type="date")
	 * @Assert\NotNull(message="Please specify when you started the course")
	 * @Assert\Date(message="Please specify when you started the course")
	 */
	protected $start;
	
	/**
	 * Course end date
	 * @var \DateTime
	 * @ORM\Column(type="date")
	 * @Assert\NotNull(message="Please specify when you finished the course")
	 * @Assert\Date(message="Please specify when you finished the course")
	 */
	protected $finish;
	
	/**
	 * 
	 * @var string
	 * @ORM\Column(type="string", length=200)
	 * @Assert\NotBlank(message="Please enter the name of the institute you attended")
	 * @Assert\Length(max=200, maxMessage="College name cannot be longer than {{ limit }} characters")
	 */
	protected $college;
	
	/**
	 * List of courses in which the candidate is qualified in
	 * @var string
	 * @ORM\Column(type="string", length=200)
	 * @Assert\NotBlank(message="Please enter the name of your course")
	 * @Assert\Length(max=200, maxMessage="Course name cannot be longer than {{ limit }} characters")
	 */
	protected $course;

	/** 
	 * The received award
	 * @var string 
	 * @ORM\column(type="string", length=200)
	 * @Assert\NotBlank(message="Please enter the award you received")
	 * @Assert\Length(max=200, maxMessage="Award name cannot be longer than {{ limit }} characters")
	 */
	protected $award;
	
	/**
	 * CV To which this education belongs
	 * @var Wanawork\UserBundle\Entity\CV
	 * @ORM\ManyToOne(targetEntity="Wanawork\UserBundle\Entity\CVForm", inversedBy="educations")
	 * @Assert\NotNull(message="CV is required for the education")
	 */
	protected $cv;
	
	public $dateUtility = '\Wanawork\UserBundle\Util\Date';
	
	
	/**
	 * @return the $id
	 */
	public function getId() 
	{
		return $this->id;
	}

	/**
	 * @return \DateTime $start
	 */
	public function getStart() 
	{
	    if($this->start) {
	        return clone $this->start;
	    }
		return null;
	}

	/**
	 * @return \DateTime $finish
	 */
	public function getFinish()
	{
	    if ($this->finish) {
	        return clone $this->finish;
	    }
		return null;
	}


	/**
	 * @return string $award
	 */
	public function getAward()
	{
		return $this->award;
	}

	/**
	 * @param DateTime $start
	 */
	public function setStart(\DateTime $start = null)
	{
		$this->start = $start;
	}

	/**
	 * @param \DateTime $finish
	 */
	public function setFinish(\DateTime $finish = null)
	{
		$this->finish = $finish;
	}


	/**
	 * @param string $award
	 */
	public function setAward($award)
	{
		$this->award = $award;
	}
	
	/**
	 * @return \Wanawork\UserBundle\Entity\CV $cv
	 */
	public function getCv()
	{
		return $this->cv;
	}

	/**
	 * @param \Wanawork\UserBundle\Entity\CV $cv
	 */
	public function setCv(CV $cv = null)
	{
		$this->cv = $cv;
	}
	
	/**
	 * @return string $college
	 */
	public function getCollege()
	{
		return $this->college;
	}

	/**
	 * @param string $college
	 */
	public function setCollege($college)
	{
		$this->college = $college;
	}
	
	public function isFinishDateGreaterThanStart(ExecutionContextInterface $context)
	{
		if($this->getStart() && $this->getFinish() && $this->getStart() >= $this->getFinish()) {
		    $context->addViolationAt('finish', 'Graduation date must be greater than commencement date');
		}
	}

    public function getCourse()
    {
        return $this->course;
    }

    public function setCourse($course)
    {
        $this->course = $course;
    }
    
    public function getDuration()
    {
        $dateUtility = $this->dateUtility;
        return $dateUtility::diffInMonthsAndYears($this->getFinish(), $this->getStart());
    }
	
}

