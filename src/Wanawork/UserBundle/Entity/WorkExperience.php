<?php
namespace Wanawork\UserBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\ExecutionContextInterface;

/**
 * Work experience entity
 * @author Ilya Biryukov <ilya@goparty.ie>
 * @ORM\Entity
 * 
 * @Assert\Callback(methods={"isFinishDateGreaterThanStart"})
 */
class WorkExperience 
{

	/**
	 * Work Experience ID
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;
	
	/**
	 * Word start date
	 * @var \DateTime
	 * @ORM\column(type="date")
	 * @Assert\NotNull(message="Please specify when you started the job")
	 * @Assert\Date(message="Please specify when you started the job")
	 */
	protected $start;
	
	/**
	 * Work end date
	 * @var \DateTime
	 * @ORM\column(type="date", nullable=true)
	 * @Assert\Date(message="Please specify when you left the job")
	 */
	protected $finish;
	
	/**
	 * Company where the experience whas acquired
	 * @var string 
	 * @ORM\Column(type="string", length=200)
	 * @Assert\NotBlank(message="Please enter the company name where you worked")
	 * @Assert\Length(max=200, maxMessage="Company name cannot be longer than {{ limit }} characters")
	 */
	protected $company;
	
	/** 
	 * Position held in the company
	 * @var string
	 * @ORM\Column(type="string", length=200)
	 * @Assert\NotBlank(message="Please enter the position you held in the company")
	 * @Assert\Length(max=200, maxMessage="Position cannot be longer than {{ limit }} characters")
	 */
	protected $position;
	
	/**
	 * Description of the work done/achievements 
	 * @var string
	 * @ORM\Column(type="text", nullable=true)
	 * @Assert\NotBlank(message="Please provide small description of the position you held")
	 * @Assert\Length(max=1000, maxMessage="Description cannot be longer than {{ limit }} characters")
	 */
	protected $description;
	
	/**
	 * CV to which this work experience belongs
	 * @var Wanawork\UserBundle\Entity\CV
	 * @ORM\ManyToOne(targetEntity="Wanawork\UserBundle\Entity\CVForm", inversedBy="jobs")
	 * @Assert\NotNull(message="You need to select a CV")
	 */
	protected $cv;
	
	public $dateUtility = '\Wanawork\UserBundle\Util\Date';
	
	
// 	public function __construct($company = null, \DateTime $start = null, \DateTime $finish = null, $description = null, CV $cv = null) {
// 		$this->setCompany($company);
// 		$this->setStart($start);
// 		$this->setFinish($finish);
// 		$this->setDescription($description);
// 		$this->setCv($cv);
// 	}
	
	/**
	 * @return integer $id
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @return \DateTime $start
	 */
	public function getStart()
	{
	    if ($this->start) {
	        return clone $this->start;
	    }
		return null;
	}

	/**
	 * @return \DateTime $finish
	 */
	public function getFinish()
	{
		if ($this->finish) {
		    return clone $this->finish;
		}
		return null;
	}

	/**
	 * @return string $company
	 */
	public function getCompany()
	{
		return $this->company;
	}

	/**
	 * @return string $description
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * @return Wanawork\UserBundle\Entity\CV $cv
	 */
	public function getCv()
	{
		return $this->cv;
	}

	/**
	 * @param \DateTime $start
	 */
	public function setStart(\DateTime $start = null)
	{
		$this->start = $start;
	}

	/**
	 * @param \DateTime $finish
	 */
	public function setFinish(\DateTime $finish = null)
	{
		$this->finish = $finish;
	}

	/**
	 * @param string $company
	 */
	public function setCompany($company)
	{
		$this->company = $company;
	}

	/**
	 * @param string $description
	 */
	public function setDescription($description) 
	{
		$this->description = $description;
	}

	/**
	 * @param \Wanawork\UserBundle\Entity\CV $cv
	 */
	public function setCv(CV $cv = null) 
	{
		$this->cv = $cv;
	}
	/**
	 * @return the $position
	 */
	public function getPosition()
	{
		return $this->position;
	}

	/**
	 * @param string $position
	 */
	public function setPosition($position)
	{
		$this->position = $position;
	}
	
	public function isFinishDateGreaterThanStart(ExecutionContextInterface $context)
	{
	    if($this->getStart() && $this->getFinish() && $this->getStart() >= $this->getFinish()) {
	        $context->addViolationAt('finish', 'Finish date must be greater than commencement date');
	    }
	}
	
	public function getDuration()
	{
	    $dateUtility = $this->dateUtility;
	    $startDate = $this->getStart();
	    $finishDate = $this->getFinish();
	    if (!$finishDate instanceof \DateTime) {
	        $finishDate = new \DateTime();
	    }
	    return $dateUtility::diffInMonthsAndYears($finishDate, $startDate);
	}
}
