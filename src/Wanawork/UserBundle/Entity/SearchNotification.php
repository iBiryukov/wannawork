<?php
namespace Wanawork\UserBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;
use Symfony\Component\Validator\Constraints\EmailValidator;

/**
 * An entity that stores requests to be notified about new ads in the given sector
 * @author Ilya Biryukov <ilya@wanawork.ie>
 * 
 * @ORM\Entity(repositoryClass="Wanawork\UserBundle\Repository\SearchNotificationRepository")
 * @Assert\Callback(methods={"validate"})
 */
class SearchNotification
{
	/**
	 * Search that is being tracked
	 * @var \Wanawork\UserBundle\Entity\Search
	 * @ORM\OneToOne(targetEntity="Wanawork\UserBundle\Entity\Search", inversedBy="notification")
	 * @ORM\Id
	 * @Assert\NotNull(message="Please select a search to be notified about")
	 * @Assert\Valid()
	 */
	protected $search;

	/**
	 * Name of the search
	 * @var string
	 * @ORM\Column(type="string", length=255)
	 * @Assert\NotBlank(message="Please enter the name")
	 * @Assert\Length(max=255, maxMessage="Search name cannot be longer than {{ limit }} characters")
	 */
	protected $searchName;
	
	/**
	 * When the notification was created
	 * @var \DateTime
	 * @ORM\Column(type="datetime", nullable=true)
	 * @Assert\DateTime(message="Notification requires creation date")
	 */
	protected $createdOn;

	/**
	 * Employer that created this search notification
	 * @var \Wanawork\UserBundle\Entity\EmployerProfile  | null
	 * @ORM\ManyToOne(targetEntity="EmployerProfile", inversedBy="savedSearches")
	 * 
	 * @ignore Reserved for future use.
	 */
	protected $employer;
	
	/**
	 * User that created this notification
	 * @var \Wanawork\UserBundle\Entity\User | null
	 * @ORM\ManyToOne(targetEntity="User", inversedBy="savedSearches")
	 */
	protected $user;
	
	/**
	 * Email to send the notification to, if the email profile is missing
	 * @var string
	 * @ORM\Column(type="string", length=250, nullable=true)
	 * @Assert\Email(message="Please enter a valid email")
	 */
	protected $email;
	
	/**
	 * List of checks done for the this notification
	 * @var \Doctrine\Common\Collections\ArrayCollection
	 * @ORM\OneToMany(targetEntity="Wanawork\UserBundle\Entity\SearchNotificationCheck", mappedBy="notification", 
	 *   cascade={"all"})
	 * @ORM\OrderBy({"date"="DESC"})
	 */
	protected $searchChecks;
	
	/**
	 * Is this notification active?
	 * @var boolean
	 * @ORM\Column(type="boolean")
	 */
	protected $active;
	
	public function __construct(Search $search, $name, User $user = null, $email = null, EmployerProfile $employer = null)
	{
		$this->search = $search;
		$this->setSearchName($name);
		$this->createdOn = new \DateTime();
		$this->employer = $employer;
		$this->email = $email;
		$this->searchChecks = new ArrayCollection();
		$this->active = true;
		
		$this->user = $user;
		if ($user) {
		    $user->addSavedSearch($this);
		}
	}

	/**
	 * @return \Wanawork\UserBundle\Entity\Search $search
	 */
	public function getSearch()
	{
		return $this->search;
	}

	/**
	 * @return the string
	 */
	public function getSearchName()
	{
		return $this->searchName;
	}
	
	public function setSearchName($name)
	{
		$this->searchName = $name;
	}

	public function getId()
	{
		return ($this->getSearch()) ? $this->getSearch()->getId() : null;
	}
	
	public function validate(ExecutionContextInterface $context)
	{
	    $search = $this->getSearch();
	    if (!$search) {
	        $context->addViolation("The search you're adding notification to, seems to be missing");
	    } else {
	        if (!$this->getUser() && !$this->getEmail()) {
	            $context->addViolation("You need to specify an email or be registered as an employer");
	        }
	    }
	}

    /**
     * @return \Wanawork\UserBundle\Entity\EmployerProfile
     */
    public function getEmployer()
    {
        return $this->employer;
    }

    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedOn()
    {
        return clone $this->createdOn;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getSearchChecks()
    {
        return $this->searchChecks;
    }
	
    public function addSearchCheck(array $newAds)
    {
        $searchCheck = new SearchNotificationCheck($this, $newAds);
        $this->getSearchChecks()->add($searchCheck);
        return $searchCheck;
    }
    
    public function markActive($flag)
    {
        $this->active = $flag;
    }
    
    public function isActive()
    {
        return $this->active;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }
    
    /**
     * Get the last date when the new searches were checked
     * @return \DateTime
     */
    public function getLastCheck()
    {
        $date = $this->getCreatedOn();
        if (sizeof($this->getSearchChecks()) > 0) {
            $date = $this->getSearchChecks()->first()->getDate();
        }
        
        return clone $date;
    }
}
