<?php
namespace Wanawork\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Wanawork\MainBundle\Services\UUID;
use Doctrine\Common\Collections\ArrayCollection;
use Wanawork\UserBundle\Tests\Entity\SystemEmailLinkTest;

/**
 * Record of email sent to a user
 * @author Ilya Biryukov <ilya@wanawork.ie>
 * @ORM\Entity(repositoryClass="Wanawork\UserBundle\Repository\SystemEmailRepository")
 */
class SystemEmail
{
    CONST STATUS_NEW        =   1;
    CONST STATUS_SENDING    =   2;
    CONST STATUS_SENT       =   3;
    CONST STATUS_FAILED     =   4;
    CONST STATUS_QUEUEING   =   5;
    CONST STATUS_QUEUED     =   6;
    
    static $validStatuses = array(
    	self::STATUS_NEW        =>  'new',
        self::STATUS_SENDING    =>  'sending',
        self::STATUS_SENT       =>  'sent',
        self::STATUS_FAILED     =>  'failed',
        self::STATUS_QUEUEING   =>  'queueing',
        self::STATUS_QUEUED     =>  'queued',
    );
    
    const TYPE_REGISTRATION_CANDIDATE   =   1;
    const TYPE_REGISTRATION_EMPLOYER    =   2;
    const TYPE_RESTORE_PASSWORD         =   3;
    const TYPE_CV_REQUEST               =   4;
    const TYPE_CV_REQUEST_RESPONSE      =   5;
    const TYPE_NEW_MESSAGE              =   6;
    const TYPE_RECEIPT                  =   7;
    const TYPE_ADM_EMPLOYER_REG         =   8;
    const TYPE_PRESALES                 =   9;
    const TYPE_BLANK                    =   10;
    const TYPE_SEARCH_NOTIFICATION      =   11;
    
    /**
     * Email Types.
     * Dictionary of id => description
     */
    static $validTypes = array(
    	self::TYPE_REGISTRATION_CANDIDATE   =>     'registration - candidate',
        self::TYPE_REGISTRATION_EMPLOYER    =>     'registration - employer',
        self::TYPE_RESTORE_PASSWORD         =>     'restore password',
        self::TYPE_CV_REQUEST               =>     'cv request',
        self::TYPE_CV_REQUEST_RESPONSE      =>     'cv request response',
        self::TYPE_NEW_MESSAGE              =>     'new mailbox message',
        self::TYPE_RECEIPT                  =>     'receipt',
        self::TYPE_ADM_EMPLOYER_REG         =>     'admin registration - employer',
        self::TYPE_PRESALES                 =>     'pre-sales',
        self::TYPE_BLANK                    =>     'blank',
        self::TYPE_SEARCH_NOTIFICATION      =>     'saved search notification',
    );
    
    /**
     * List of emails that can be edited and sent
     * Dictionary of id => template name
     */
    static $templates = array(
    	self::TYPE_PRESALES => 'pre-sale',
        self::TYPE_BLANK    => 'blank',
    );
    
    static $subjects = array(
        self::TYPE_PRESALES => 'Cut Your Recruitment Costs by 100% with Wannawork.ie',
        self::TYPE_BLANK    => '',
    );
    
    /**
     * Email type
     * @var integer
     * @ORM\Column(type="smallint")
     */
    private $type;

	/**
	 * The user who received the email
	 * @var \Wanawork\UserBundle\Entity\User
	 * @ORM\ManyToOne(targetEntity="Wanawork\UserBundle\Entity\User", inversedBy="systemEmails")
	 */
	protected $user;

	/**
	 * Email address that the message was sent to
	 * Recorded in case the user changes email address.
	 * @var string
	 * @ORM\Column(type="string")
	 * @Assert\NotBlank(message="System Email requires a valid email")
	 * @Assert\Email()
	 */
	protected $email;

	/**
	 * Email ID
	 * @var string
	 * @ORM\Column(type="string")
	 * @ORM\Id
	 * @Assert\NotBlank()
	 */
	protected $id;
	
	/**
	 * When the email was created
	 * @var \DateTime
	 * @ORM\Column(type="datetime")
	 */
	protected $dateCreated;

	/**
	 * Date the email was sent
	 * @var \DateTime
	 * @ORM\Column(type="datetime", nullable=true)
	 * @Assert\DateTime()
	 */
	protected $dateSent;
	
	/**
	 * Status of the email
	 * @var integer
	 * @ORM\Column(type="smallint")
	 */
	private $status;

	/**
	 * Text of the sent email
	 * @var string
	 * @ORM\Column(type="text", nullable=true)
	 * @Assert\NotBlank
	 */
	protected $text;
	
	/**
	 * Subject of the sent email
	 * @var string
	 * @ORM\Column(type="string")
	 * @Assert\NotBlank
	 */
	protected $subject;
	
	/**
	 * How many attempts to send this email were done
	 * @var integer
	 * @ORM\Column(type="integer")
	 */
	protected $attempts;
	
	/**
	 * Tracks individual email "open" events
	 * @var \Doctrine\Common\Collections\ArrayCollection
	 * @ORM\OneToMany(targetEntity="SystemEmailTracker", mappedBy="systemEmail", cascade={"all"})
	 * @ORM\OrderBy({"date"="DESC"})
	 */
	protected $trackers;
	
	/**
	 * List of links
	 * @var \Doctrine\Common\Collections\ArrayCollection
	 * @ORM\OneToMany(targetEntity="SystemEmailLink", mappedBy="systemEmail", cascade={"all"})
	 */
	protected $links;
	
	public function __construct($user = null, $subject = null, $type = null, $email = null)
	{
		$this->user = $user;

		if ($email === null && $user instanceof User) {
		    $email = $user->getEmail();
		}
		$this->email = $email;
		$this->subject = $subject;
		
		$this->id = self::createId($email);
		$this->status = self::STATUS_NEW;
		$this->attempts = 0;
		$this->dateCreated = new \DateTime();
		$this->setType($type);
		
		if ($user instanceof User) {
		    $user->getSystemEmails()->add($this);
		}
		$this->trackers = new ArrayCollection();
		$this->links = new ArrayCollection();
	}
	
	public static function createId($email)
	{
	    return sha1(uniqid(UUID::v4(), true));
	}

	/**
	 * @return Wanawork\UserBundle\Entity\User
	 */
	public function getUser()
	{
		return $this->user;
	}

	/**
	 * @return string
	 */
	public function getEmail()
	{
		return $this->email;
	}

	/**
	 * @return string
	 */
	public function getId()
	{
		return $this->id;
	}
	
	public function setId($id)
	{
	    $this->id = $id;
	}

	/**
	 * @return \DateTime
	 */
	public function getDateSent()
	{
		return $this->dateSent;
	}

	/**
	 * @return string
	 */
	public function getText()
	{
		return $this->text;
	}
	
	public function setText($text, $trackLinks = true)
	{
	    if ($trackLinks === true) {
	        $replacements = array();
	         
	        libxml_use_internal_errors(true);
	        $dom = new \DOMDocument();
	        $dom->loadHTML($text);
	         
	        // make redirect links
	        foreach ($dom->getElementsByTagName('a') as $aTag) {
	            $href = $aTag->getAttribute('href');
	            $linkText = $aTag->nodeValue;
	        
	            // skip already redirected links
	            if (!preg_match('/redirect\/.+$/', $href)) {
	                $link = $this->addLink($href, $linkText);
	                
	                $host = 'http://wannawork.ie';
	                $urlData = @parse_url($href);
	                if (isset($urlData['host']) && preg_match('/wan(n)?awork/', $urlData['host'])) {
	                    $host = 'http://' . $urlData['host'];
	                }
	                $host .= '/redirect/' . $link->getId();
	                
 	                $pattern = '/href=["\']' . preg_quote($href, $delimeter = '/') . '["\']/';
 	                $text = preg_replace($pattern, 'href="'.$host.'"', $text, $max = 1);
	            }
	        }
	        
	    }
	    
	    $this->text = $text;
	}
	
	/**
	 * @return string
	 */
	public function getSubject()
	{
		return $this->subject;
	}
	
	/**
	 * @return number
	 */
	public function getStatus()
	{
	    return $this->status;
	}
	
	public function setStatus($status)
	{
	    if (!array_key_exists($status, self::$validStatuses)) {
	        throw new \InvalidArgumentException(
    	        sprintf(
    	            "Invalid status provided '%s'. Valid values: %s",
                    $status, 
    	            implode(', ', array_keys(self::$validStatuses))
                )
	        );
	    }
	    
	    $this->status = $status;
	}
	
	/**
	 * @return string
	 */
	public function getStatusName()
	{
	    return self::$validStatuses[$this->getStatus()];
	}

	/**
	 * @return \DateTime
	 */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @return number
     */
    public function getAttempts()
    {
        return $this->attempts;
    }

    public function increaseAttempts()
    {
        $this->attempts += 1;
    }
    
    public function markSent()
    {
        $this->setStatus(self::STATUS_SENT);
        $this->dateSent = new \DateTime();
    }
    
    /**
     * Was the email sent?
     * @return boolean
     */
    public function isSent()
    {
        return $this->getDateSent() instanceof \DateTime && $this->getStatus() === self::STATUS_SENT;
    }
    
    public function setType($type)
    {
        if (!array_key_exists($type, self::$validTypes)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid type provided '%s'. Valid values: %s",
                    $type,
                    implode(', ', array_keys(self::$validTypes))
                )
            );
        }
        $this->type = $type;
    }
    
    /**
     * @return number
     */
    public function getType()
    {
        return $this->type;
    }
    
    /**
     * @return string
     */
    public function getTypeName()
    {
        return self::$validTypes[$this->getType()];
    }
    
    /**
     * Add new tracker
     * @param string $ip
     * @return \Wanawork\UserBundle\Entity\SystemEmailTracker
     */
    public function addTracker($ip)
    {
        $tracker = new SystemEmailTracker($this, $ip);
        $this->getTrackers()->add($tracker);
        return $tracker;
    }
    
    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getTrackers()
    {
        return $this->trackers;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function setSubject($subject)
    {
        $this->subject = $subject;
    }
    
    public static function _getTypeName($type)
    {
        return self::$validTypes[$type];
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getLinks()
    {
        return $this->links;
    }
	
    /**
     * Create new link
     * @param string $href
     * @param string $text
     * @return \Wanawork\UserBundle\Entity\SystemEmailLink
     */
	public function addLink($href, $text)
	{
	    $link = new SystemEmailLink($this, $href, $text);
	    $this->getLinks()->add($link);
	    return $link;
	}
	
	/**
	 * How many times all the links were clicked
	 * @return number
	 */
	public function getTotalLinkClicks()
	{
	    $clicks = 0;
	    foreach($this->getLinks() as $link) {
	        $clicks += sizeof($link->getClicks());
	    }
	    
	    return $clicks;
	}
    
}
