<?php
namespace Wanawork\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * An entity that manages password reminders
 * 
 * @author Ilya Biryukov <ilya@wanawork.ie>
 * @ORM\Entity
 */
class PasswordReminder
{

    const LIFETIME = 'PT4H';

    /**
     * Unique id
     * 
     * @var string @ORM\Column(type="string", length=32)
     * @ORM\Id
     */
    protected $id;

    /**
     *
     * @var Wanawork\UserBundle\Entity\User @ORM\ManyToOne(targetEntity="Wanawork\UserBundle\Entity\User", inversedBy="passwordReminders")
     *      @Assert\Type(type="Wanawork\UserBundle\Entity\User", message="Please provide a valid user entity")
     */
    protected $user;

    /**
     * When was this toke created
     * 
     * @var \DateTime @ORM\Column(type="datetime")
     */
    protected $createdOn;

    /**
     *
     * @var \DateTime | null
     *      @ORM\Column(type="datetime", nullable=true)
     */
    protected $usedOn;

    /**
     * String for DateInterval
     * 
     * @var string @ORM\Column(type="string")
     */
    protected $lifetime;

    /**
     * Ip Address of the person who created this request
     * 
     * @var string @ORM\Column(type="string", length=20)
     */
    protected $creatorsIp;

    /**
     * Ip Address of the person who completed the request
     * 
     * @var string @ORM\Column(type="string", length=20, nullable=true)
     */
    protected $usedByIp;

    public function __construct(User $user, $ip, $lifetime = self::LIFETIME)
    {
        $this->user = $user;
        $this->id = md5(uniqid($this->user->getId(), $entropy = true));
        $this->createdOn = new \DateTime();
        $this->usedOn = null;
        $this->lifetime = $lifetime;
        $this->creatorsIp = $ip;
    }

    /**
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     *
     * @return \Wanawork\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Check if this token is still valid and can be used to restore a password
     * 
     * @return boolean
     */
    public function isValid()
    {
        return $this->getUsedOn() === null && $this->getExpiryDate() > new \DateTime();
    }

    public function getExpiryDate()
    {
        $createdOn = clone $this->createdOn;
        $createdOn->add(new \DateInterval($this->lifetime));
        return $createdOn;
    }

    /**
     */
    public function markUsed($ip)
    {
        if (! $this->isValid()) {
            throw new \Exception("The password reset token is invalid");
        }
        $this->usedOn = new \DateTime();
        $this->usedByIp = $ip;
    }

    /**
     *
     * @return \DateTime
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }

    /**
     *
     * @return \DateTime | null
     */
    public function getUsedOn()
    {
        return $this->usedOn;
    }

    /**
     *
     * @return string
     */
    public function getLifetime()
    {
        return $this->lifetime;
    }

    public function getCreatorsIp()
    {
        return $this->creatorsIp;
    }

    public function getUsedByIp()
    {
        return $this->usedByIp;
    }
}
