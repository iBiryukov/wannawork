<?php
namespace Wanawork\UserBundle\Entity\Containers;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Email;

/**
 * Container for email 
 * 
 * 
 * @author Ilya Biryukov <ilya@wanawork.ie>
 * @see Password Restore Form
 */
class EmailContainer {

	/**
	 * 
	 * @var string 
	 * @NotBlank(message="Please enter a valid email address")
	 * @Email(message="Please enter a valid email address")
	 */
	protected $email;

	public function getEmail() {
		return $this->email;
	}

	public function setEmail($email) {
		$this->email = $email;
	}
	
}
