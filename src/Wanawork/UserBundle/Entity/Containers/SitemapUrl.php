<?php
namespace Wanawork\UserBundle\Entity\Containers;

class SitemapUrl
{
    const URL_MAX_LENGTH = 2048;
    
    const CHANGE_ALWAYS     =    'always';
    const CHANGE_HOURLY     =    'hourly';
    const CHANGE_DAILY      =    'daily';
    const CHANGE_WEEKLY     =    'weekly';
    const CHANGE_MONTHLY    =    'monthly';
    const CHANGE_YEARLY     =    'yearly';
    const CHANGE_NEVER      =    'never';
        
    static $validChangeFrequencies = array(
    	self::CHANGE_ALWAYS,
        self::CHANGE_HOURLY,
        self::CHANGE_DAILY,
        self::CHANGE_WEEKLY,
        self::CHANGE_MONTHLY,
        self::CHANGE_YEARLY,
        self::CHANGE_NEVER,
    );
        
    
    /**
     * Location - the URL
     * @var string
     */
    private $location;
    
    /**
     * When the page was last modified
     * ISO8601 Format
     * 
     * @var \DateTime | null
     * @see http://www.w3.org/TR/NOTE-datetime
     */
    private $lastModified;
    
    /**
     * How often does the page change
     * @var string | null
     */
    private $changeFrequency;
    
    /**
     * Priority from 0.0 to 1.0
     * @var double | null
     */
    private $priority;
    
    public function __construct($location, $lastModified = null, $changeFrequency = null, $priority = null)
    {
        $this->setLocation($location);
        $this->setLastModified($lastModified);
        $this->setChangeFrequency($changeFrequency);
        $this->setPriority($priority);
    }
    
    public function setLocation($location)
    {
        $regex = '/^http(s)?:\/\/wan(n)?awork\.(ie|local)(\/.*)?$/';
        if (!preg_match($regex, $location)) {
            throw new \InvalidArgumentException(sprintf("URL: '%s' doesn't match the regex", $location));
        }
        
        if (strlen($location) > self::URL_MAX_LENGTH) {
            throw new \InvalidArgumentException(
                sprintf("URL: '%s' cannot be longer than '%d' characters", $location, self::URL_MAX_LENGTH)
            ); 
        }
        $this->location = $location;   
    }
    
    public function setLastModified(\DateTime $lastModified = null)
    {
        $this->lastModified = $lastModified;
    }
    
    public function setChangeFrequency($frequency = null)
    {
        if ($frequency !== null && !in_array($frequency, self::$validChangeFrequencies, true)) {
            throw new \InvalidArgumentException(
    	       sprintf("Invalid frequency provided '%s'. Valid values: '%s'", $frequency, implode(', ', self::$validChangeFrequencies))
            );
        }
        
        $this->changeFrequency = $frequency;
    }
    
    public function setPriority($priority = null)
    {
        $regex = '/^((0\.[0-9])|(1\.0))$/';
        if ($priority !== null && !preg_match($regex, $priority)) {
            throw new \InvalidArgumentException(
    	       sprintf("Invalid priority provided: '%s'. It must be a valid double number with one decimal digit", $priority)
            );
        }
        
        $this->priority = $priority;
    }
    
    /**
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @return \DateTime | null
     */
    public function getLastModified()
    {
        return $this->lastModified;
    }

    /**
     * @return string | null
     */
    public function getChangeFrequency()
    {
        return $this->changeFrequency;
    }

    /**
     * @return double | null
     */
    public function getPriority()
    {
        return $this->priority;
    }
    
}