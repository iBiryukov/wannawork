<?php
namespace Wanawork\UserBundle\Entity\Containers;
use Symfony\Component\Validator\Constraints as Assert;

class PasswordContainer {

	/**
	 * 
	 * @var string 
	 * @Assert\NotBlank(message="Please enter the new password")
	 */
	protected $password;

	public function getPassword() {
		return $this->password;
	}

	public function setPassword($password) {
		$this->password = $password;
	}
}
