<?php
namespace Wanawork\UserBundle\Entity\Containers;

use Wanawork\UserBundle\Entity\User;
use Symfony\Component\Validator\ExecutionContextInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;

/**
 * Container that holds password change
 * @author Ilya Biryukov <ilya@wannawork.ie>
 * @Assert\Callback(methods={"validateCurrentPassword"}, groups={"user"})
 */
class ChangePasswordContainer
{
    /**
     * Current user's password
     * @var string
     * @Assert\NotBlank(message="Please enter your current password", groups={"user"})
     */
    private $currentPassword;
    
    /**
     * New user password
     * @var string
     * @Assert\NotBlank(message="Please enter your new password", groups={"user", "admin"})
     */
    private $newPassword;
    
    /**
     * User for whom the password is being changed
     * @var \Wanawork\UserBundle\Entity\User
     */
    private $user;
    
    /**
     * Encoder factory
     * @var \Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface
     */
    private $encoderFactor;
    
    public function __construct(User $user, EncoderFactoryInterface $encoder)
    {
        $this->user = $user;
        $this->encoderFactor = $encoder;
    }
    
    public function validateCurrentPassword(ExecutionContextInterface $context)
    {
        if (strlen($this->getCurrentPassword()) > 0) {
            $encoder = $this->encoderFactor->getEncoder($this->user);
            
            if (!$encoder->isPasswordValid($this->user->getPassword(), $this->getCurrentPassword(), $this->user->getSalt())) {
                $context->addViolationAt('currentPassword', 'Your current password is invalid');
            }
        }
    }

    public function getCurrentPassword()
    {
        return $this->currentPassword;
    }

    public function setCurrentPassword($currentPassword)
    {
        $this->currentPassword = $currentPassword;
    }

    public function getNewPassword()
    {
        return $this->newPassword;
    }

    public function setNewPassword($newPassword)
    {
        $this->newPassword = $newPassword;
    }
	
}