<?php
namespace Wanawork\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Position Type 
 * @author Ilya Biryukov <ilya@goparty.ie>
 * @ORM\Entity(repositoryClass="Wanawork\UserBundle\Repository\PositionTypeRepository")
 * @ORM\ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 */
class PositionType
{
    /**
     * Position ID
     * @ORM\Column(type="integer")
     * @ORM\Id
     */
    protected $id;

    /**
     * Position name
     * 
     * @var string 
     * @ORM\Column(type="string", unique=true)
     * @Assert\NotBlank(message="Please enter the position type")
     * @Assert\Length(max="255", maxMessage="Position name cannot be longer than {{ limit }} characters")
     *     
     */
    protected $name;

    /**
     * Order the list of positions by this field
     * 
     * @var integer 
     * @ORM\Column(type="integer", name="ordering")
     * @Assert\NotNull()
     * @Assert\Type(type="integer")
     */
    protected $order;

    /**
     * Constructor
     * 
     * @param string $name            
     * @param integer $order            
     */
    public function __construct($id, $name, $order)
    {
        $this->setId($id);
        $this->setName($name);
        $this->setOrder($order);
    }
    
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     *
     * @param string $name            
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     *
     * @return the $order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     *
     * @param number $order            
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }
    
    public function __toString()
    {
        return $this->getName();
    }
    
    public function __toArray()
    {
        return array(
        	'id' => $this->getId(),
            'name' => $this->getName(),
            'order' => $this->getOrder(),
        );
    }
}

