<?php
namespace Wanawork\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Represents a profession within industry
 * @author Ilya Biryukov <ilya@goparty.ie>
 * @ORM\Entity(repositoryClass="Wanawork\UserBundle\Repository\SectorJobRepository")
 * @ORM\ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @UniqueEntity(fields={"id"})
 */
class SectorJob
{

    /**
     * Sector ID
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @Assert\NotNull(message="Occupation ID cannot be null")
     * @Assert\Type(type="integer")
     */
    protected $id;

    /**
     * Job name
     * 
     * @var string 
     * 
     * @ORM\Column(type="string")
     * 
     * @Assert\Type(type="string", message="Occupation name must be a string")
     * @Assert\NotBlank(message="Occupation name cannot be blank")
     * @Assert\Length(min=1, max=255, minMessage="Occupation name cannot be empty", 
     *  maxMessage="Occupation name cannot be longer than {{ limit }} characters")
     */
    protected $name;

    /**
     * Sector to which this job belongs
     * 
     * @var \Doctrine\Common\Collections\ArrayCollection 
     * @ORM\ManyToMany(targetEntity="Wanawork\UserBundle\Entity\Sector", inversedBy="jobs")
     * @Assert\Count(min=1, minMessage="Occupation needs to have at least one industry")
     * @Assert\Valid()
     */
    protected $sectors;
    
    /**
     * Slug for url
     * @Gedmo\Mapping\Annotation\Slug(fields={"name"})
     * @Doctrine\ORM\Mapping\Column(length=255, unique=true)
     */
    private $slug;

    /**
     * Get string name
     * @param integer  $id
     * @param string   $name
     * @param array    $sectors
     */
    public function __construct($id, $name, array $sectors = array())
    {
        $this->setId($id);
        $this->setName($name);
        $this->sectors = new ArrayCollection();
        foreach ($sectors as $sector) {
            $this->addSector($sector);
        }
    }
    
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * 
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getSectors()
    {
        return $this->sectors;
    }
    
    /**
     * Add new sector to this occupation
     * @param Sector $sector
     */
    public function addSector(Sector $sector)
    {
        if (!$this->getSectors()->contains($sector)) {
           $this->getSectors()->add($sector);
           $sector->addJob($this); 
        }
    }
    
    /**
     * Remove sector from this occupation
     * @param Sector $sector
     */
    public function removeSector(Sector $sector)
    {
        if($this->getSectors()->contains($sector)) {
            $this->getSectors()->removeElement($sector);
            $sector->removeJob($this);
        }
        
    }

    /**
     * set sector name
     * @param string $name            
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    public function __toString()
    {
        return $this->getName();
    }
    
    public function __toArray()
    {
        $data[$this->getId()] = array(
            'name' => $this->getName(),
            'industries' => array(),
        );
        
        foreach ($this->getSectors() as $sector) {
            $data[$this->getId()]['industries'][] = $sector->getId();
        }
        
        return $data;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }
    
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }
}
