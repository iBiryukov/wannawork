<?php
namespace Wanawork\UserBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Records different type of views the ad receives
 * @author Ilya Biryukov <ilya@wanawork.ie>
 * @ORM\Entity
 */
class AdView 
{

    const VIEW_IN_SEARCH = 1;
	const VIEW_PDF = 2;
	const VIEW_FULL = 3;

	static public $viewTypes = array(
		'SEARCH' => self::VIEW_IN_SEARCH,
		'PDF' => self::VIEW_PDF,
		'FULL' => self::VIEW_FULL,
	);

	/**
	 * Ad View ID
	 * @var integer
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO") 
	 */
	protected $id;

	/**
	 * The add that was viewed
	 * @var Wanawork\UserBundle\Entity\Ad
	 * @ORM\ManyToOne(targetEntity="Wanawork\UserBundle\Entity\Ad", inversedBy="views")
	 * @Assert\NotNull()
	 */
	protected $ad;

	/**
	 * The user that viewed the ad
	 * @var \Wanawork\UserBundle\Entity\User
	 * @ORM\ManyToOne(targetEntity="Wanawork\UserBundle\Entity\User")
	 */
	protected $user;
	
	/**
	 * Employer profile that viewed the ad
	 * @var \Wanawork\UserBundle\Entity\EmployerProfile
	 * @ORM\ManyToOne(targetEntity="EmployerProfile")
	 */
	protected $employerProfile;

	/**
	 * Date/Time when the ad was viewed
	 * @var \DateTime
	 * @ORM\Column(type="datetime")
	 * @Assert\DateTime()
	 */
	protected $timestamp;

	/**
	 * Type of the view
	 * @var integer 
	 * @ORM\Column(type="smallint")
	 */
	protected $type;
	
	/**
	 * Position in search, in case of In search view
	 * @var integer | null
	 * @ORM\Column(type="integer", nullable=true)
	 */
	protected $position;
	
	/**
	 * Search in which the ad was viewed
	 * @var \Wanawork\UserBundle\Entity\Search
	 * @ORM\ManyToOne(targetEntity="Wanawork\UserBundle\Entity\Search", cascade={"persist"})
	 */
	protected $search;

	public function __construct(Ad $ad, $type, User $user = null, EmployerProfile $profile = null, $position = null, Search $search = null) 
	{
	    if (!in_array($type, self::$viewTypes)) {
	        throw new \InvalidArgumentException("Invalid ad view type provided: $type");
	    }
	    
		$this->timestamp = new \DateTime();
		$this->ad = $ad;
		$this->user = $user;
		$this->type = $type;
		$this->employerProfile = $profile;
		$this->position = $position;
		$this->search = $search;
	}
	
	/**
	 * @return number
	 */
	public function getId() 
	{
		return $this->id;
	}

	/**
	 * @return \Wanawork\UserBundle\Entity\Wanawork\UserBundle\Entity\Ad
	 */
	public function getAd() 
	{
		return $this->ad;
	}

	/**
	 * @return \DateTime
	 */
	public function getTimestamp() 
	{
		return clone $this->timestamp;
	}
	
	public function setTimestamp($t)
	{
	    $this->timestamp = $t;
	}

	/**
	 * @return number
	 */
	public function getType() 
	{
		return $this->type;
	}

	/**
	 * @return the User
	 */
	public function getUser() 
	{
		return $this->user;
	}

	/**
	 * @return \Wanawork\UserBundle\Entity\EmployerProfile | null
	 */
    public function getEmployerProfile()
    {
        return $this->employerProfile;
    }

    /**
     *
     * @param $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     *
     * @return the Search
     */
    public function getSearch()
    {
        return $this->search;
    }
	
}
