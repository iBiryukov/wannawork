<?php
namespace Wanawork\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Represents a quantifable experience in a particular sector
 * A time range really
 * 
 * @author Ilya Biryukov <ilya@goparty.ie>
 * @ORM\Entity(repositoryClass="Wanawork\UserBundle\Repository\SectorExperienceRepository")
 * @UniqueEntity("id")
 * @ORM\ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 */
class SectorExperience
{

    /**
     * Experience ID
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @Assert\NotNull()
     * @Assert\Type(type="integer")
     */
    protected $id;

    /**
     * Name (label) of the experience
     * 
     * @var string 
     * @ORM\column(type="string")
     * @Assert\NotBlank(message="Please enter the experience label")
     * @Assert\Length(max=255, maxMessage="Name cannot be longer than {{ limit }} characters")
     */
    protected $name;

    public function __construct($id, $name)
    {
        $this->setId($id);
        $this->setName($name);
    }

    /**
     *
     * @return integer $id
     */
    public function getId()
    {
        return $this->id;
    }
    
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     *
     * @param string $name            
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    public function __toString()
    {
        return $this->getName();
    }
    
    public function __toArray()
    {
        return array($this->getId() => $this->getName());
    }
}
