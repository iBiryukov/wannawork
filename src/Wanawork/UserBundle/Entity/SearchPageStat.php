<?php
namespace Wanawork\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Records statistics about a page of ads
 * @author Ilya Biryukov <ilya@wanawork.ie>
 * @ORM\Entity
 */
class SearchPageStat
{
    /**
     * ID
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * Search entity
     * @var \Wanawork\UserBundle\Entity\Search
     * @ORM\ManyToOne(targetEntity="Search", inversedBy="pageStats")
     */
    protected $search;
    
    /**
     * Page number
     * @var integer
     * @ORM\Column(type="integer")
     */
    protected $page;
    
    /**
     * The average score of all found ads
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    protected $averageAdScore;
    
    /**
     * A list of scores each ad received
     * @var array
     * @ORM\Column(type="array", nullable=true)
     */
    protected $adScores;
    
    /**
     * When the page was viewed
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    protected $date;
    
    /**
     * How many ads found in total on all pages
     * @var integer
     * @ORM\Column(type="integer", nullable=true)
     */
    private $totalFound;
    
    public function __construct(Search $search, $page, array $adScores, $totalFound)
    {
        $this->setSearch($search);
        $this->setPage($page);
        $this->setAdScores($adScores);    
        $this->setDate(new \DateTime());
        $this->totalFound = $totalFound;
    }
    
    public function setAdScores(array $scores)
    {
        $this->adScores = $scores;
        $this->setAverageAdScore($scores);
    }
    
    /**
     * Get the list of all scores
     * @return array
     */
    public function getAdScores()
    {
        return $this->adScores;
    }
    
    /**
     * Get the average score of ads
     * @return number
     */
    public function getAverageAdScore()
    {
        return $this->averageAdScore;
    }
    
    /**
     * Calculate the average score for all ads
     * @param array $scores
     */
    protected function setAverageAdScore($scores)
    {
        $avg = null;
        if($scores) {
            $avg = array_sum($scores) / sizeof($scores);
        }
        $this->averageAdScore = $avg;
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return the Search
     */
    public function getSearch()
    {
        return $this->search;
    }

    /**
     * @param $search
     */
    public function setSearch($search)
    {
        $this->search = $search;
    }

    /**
     * @return the integer
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param $page
     */
    public function setPage($page)
    {
        $this->page = $page;
    }

    /**
     * @return the \DateTime
     */
    public function getDate()
    {
        return clone $this->date;
    }

    /**
     * @param \DateTime $date            
     */
    public function setDate(\DateTime $date)
    {
        $this->date = $date;
    }

    /**
     * @return number
     */
    public function getTotalFound()
    {
        return $this->totalFound;
    }
}