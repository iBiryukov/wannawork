<?php
namespace Wanawork\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Wanawork\UserBundle\Exception\employerProfile\MissingEmployerAdminException;
use Symfony\Component\Validator\Constraints\Length;
use Doctrine\Common\Collections\Criteria;
use Wanawork\UserBundle\Entity\Billing\VerificationOrder;
use Wanawork\UserBundle\Entity\Billing\Order;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Representation of employer profile
 * @author Ilya Biryukov <ilya@goparty.ie>
 * @ORM\Entity(repositoryClass="Wanawork\UserBundle\Repository\EmployerProfileRepository")
 */
class EmployerProfile extends AbstractProfile
{
    const VERIFIED_DAILY_REQUESTS = 500;
    
    const UNVERIFIED_DAILY_REQUESTS = 5;

    /**
     * Name of the company
     * 
     * @var string 
     * 
     * @Orm\Column(type="string", length=255)
     * @Assert\NotBlank(message="Please enter the company name")
     * @Assert\Length(max=255, maxMessage="Company name cannot be longer than {{ limit }} characters")
     */
    protected $companyName;

    /**
     * Who is the contact for the company
     * @var string 
     * 
     * @Orm\Column(type="string")
     * @Assert\NotBlank(message="Please enter the name")
     * @Assert\Length(max=250, maxMessage="Contact name is too long. Max {{ limit }} characters")
     */
    protected $contactName;
    
    /**
     * Name Title
     *
     * @var \Wanawork\MainBundle\Entity\NameTitle
     * @ORM\ManyToOne(targetEntity="Wanawork\MainBundle\Entity\NameTitle")
     * @deprecated
     */
    protected $contactTitle;

    /**
     * Contact email
     * 
     * @var string 
     * 
     * @ORM\Column(type="string")
     * @Assert\NotBlank(message="Please enter the email address")
     * @Assert\Email(message="Please enter the email address")
     */
    protected $email;

    /**
     * Company website
     * @var string 
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Url(message="Please enter a valid web address")
     */
    protected $website;

    /**
     * Company Industries
     * 
     * @var Doctrine\Common\Collections\ArrayCollection
     * @ORM\ManyToMany(targetEntity="Wanawork\UserBundle\Entity\Sector")
     * @Assert\NotNull(message="Please select your industries")
     * @Assert\Count(min = "1", minMessage = "Please select at least one industry")
     */
    protected $industries;

    /**
     * Company Professions
     *
     * @var Doctrine\Common\Collections\ArrayCollection
     * @ORM\ManyToMany(targetEntity="Wanawork\UserBundle\Entity\SectorJob")
     * @Assert\NotNull(message="Please select the professions you wish to hire")
     * @Assert\Count(min = "1", minMessage = "Please select the professions you wish to hire")
     */
    protected $professions;
    
    /**
     * Company facebook
     * 
     * @var string 
     * 
     * @ORM\Column(type="string", nullable=true, length=200)
     * @Assert\Length(max=200, maxMessage="Facebook username cannot be longer than {{ limit }} characters")
     * @Assert\Regex(pattern="/^[0-9a-zA-Z\-_\.]+$/", message="Invalid facebook username")
     */
    protected $facebook;

    /**
     * Company twitter (username)
     * 
     * @var string 
     * 
     * @ORM\Column(type="string", nullable=true, length=200)
     * @Assert\Length(max="200", maxMessage="Twitter name cannot be longer then {{ limit }} characters")
     * @Assert\Regex(pattern="/^[a-zA-Z0-9_]+$/", message="Invalid username. Only letters, numbers and underscore are accepted")
     */
    protected $twitter;
    
    /**
     * Company linked in
     * 
     * @var string 
     * 
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Url(message="Please enter a full address to your linked page")
     */
    protected $linkedin;

    /**
     * Description of the company
     * 
     * @var string 
     * @ORM\Column(type="text", nullable=true)
     * @Assert\Length(max=2000, maxMessage="Your company description cannot be longer than {{ limit }} characters")
     */
    protected $about;

    /**
     * Is this company verified
     * 
     * @var boolean 
     * @ORM\Column(type="boolean")
     */
    protected $isVerified;

    /**
     * If this is a verified profile, store who verified it
     * 
     * @var \Wanawork\UserBundle\Entity\User | null
     * 
     * @ORM\ManyToOne(targetEntity="Wanawork\UserBundle\Entity\User")
     */
    protected $verifiedBy;
    
    /**
     * Order details that initiated verification process
     * @var \Doctrine\Common\Collections\ArrayCollection
     * @ORM\OneToMany(targetEntity="Wanawork\UserBundle\Entity\Billing\VerificationOrder", mappedBy="employer")
     */
    protected $verificationOrders;

    /**
     * CRO (or alternative) company number
     * @var integer
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Length(max=30, maxMessage="Company number cannot be longer than {{ limit }} characters")
     */
    protected $companyNumber;
    
    /**
     * List of SENT cv requests by this employer
     * @var \Doctrine\Common\Collections\ArrayCollection
     * @ORM\OneToMany(targetEntity="CvRequest", mappedBy="employerProfile", indexBy="ad", fetch="EXTRA_LAZY")
     * @ORM\OrderBy({"id"="desc"})
     */
    protected $cvRequests;
    
    /**
     * List of jobs created by this employer
     * @var \Doctrine\Common\Collections\ArrayCollection
     * @ORM\OneToMany(targetEntity="JobSpec", mappedBy="employer", cascade={"persist", "remove"})
     * @ORM\OrderBy({"createdOn"="DESC"})
     */
    protected $jobSpecs;
    
    /**
     * List of users that can use account
     * @var \Doctrine\Common\Collections\ArrayCollection
     * @ORM\ManyToMany(targetEntity="Wanawork\UserBundle\Entity\User", inversedBy="employerProfiles")
     * @Assert\Count(min=1, minMessage="Profiles requires at least {{ limit }} user")
     */
    protected $users;
    
    /**
     * Admin role to use for this employer
     * @var \Wanawork\UserBundle\Entity\Role
     * @ORM\ManyToOne(targetEntity="Role", cascade={"persist"})
     * @Assert\NotNull()
     */
    protected $adminRole;
    
    /**
     * User role to use for this employer
     * @var \Wanawork\UserBundle\Entity\Role
     * @ORM\ManyToOne(targetEntity="Role", cascade={"persist"})
     * @Assert\NotNull()
     */
    protected $userRole;
    
    /**
     * List of searches this profile saved
     * @var \Doctrine\Common\Collections\ArrayCollection
     * @ORM\OneToMany(targetEntity="SearchNotification", mappedBy="employer", cascade={"persist"})
     */
    protected $savedSearches;
    
    /**
     * Slug for url
     * @Gedmo\Slug(fields={"companyName"})
     * @Doctrine\ORM\Mapping\Column(length=275, unique=true)
     */
    private $slug;

    public function __construct()
    {
        $this->isVerified = false;
        $this->companyNumber = null;
        $this->cvRequests = new ArrayCollection();
        $this->jobSpecs = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->adminRole = new Role('ADMIN', uniqid('ADMIN', true));
        $this->userRole = new Role('USER', uniqid('USER', true));
        $this->savedSearches = new ArrayCollection();
        $this->verificationOrders = new ArrayCollection();
        $this->industries = new ArrayCollection();
        $this->professions = new ArrayCollection();
    }

    /**
     *
     * @return the $companyName
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     *
     * @return the $contactName
     */
    public function getContactName()
    {
        return $this->contactName;
    }

    /**
     *
     * @return string $email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     *
     * @return the $website
     */
    public function getWebsite()
    {
        return $this->website;
    }
    
    /**
     *
     * @return the $facebook
     */
    public function getFacebook()
    {
        return $this->facebook;
    }

    /**
     *
     * @return the $twitter
     */
    public function getTwitter()
    {
        return $this->twitter;
    }

    /**
     *
     * @return the $linkedin
     */
    public function getLinkedin()
    {
        return $this->linkedin;
    }

    /**
     *
     * @return the $about
     */
    public function getAbout()
    {
        return $this->about;
    }

    public function isVerified()
    {
        return $this->isVerified;
    }

    /**
     *
     * @return \Wanawork\UserBundle\Entity\User | null
     */
    public function getVerifiedBy()
    {
        return $this->verifiedBy;
    }

    /**
     *
     * @return the $companyNumber
     */
    public function getCompanyNumber()
    {
        return $this->companyNumber;
    }

    /**
     *
     * @param string $companyName            
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;
    }

    /**
     *
     * @param string $contactName            
     */
    public function setContactName($contactName)
    {
        $this->contactName = $contactName;
    }

    /**
     *
     * @param string $email            
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     *
     * @param string $website            
     */
    public function setWebsite($website)
    {
        $this->website = $website;
    }

    /**
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getIndustries()
    {
        return $this->industries;
    }
	
	/**
	 *
	 * @param array $Industries
	 */
	public function setIndustries(array $industries)
	{
		foreach ($this->getIndustries() as $industry) {
			$this->removeIndustry($industry);
		}
	
		foreach ($industries as $industry) {
			$this->addIndustry($industry);
		}
	
	}
	
	public function addIndustry(Sector $industry)
	{
		if (!$this->getIndustries()->contains($industry)) {
			$this->getIndustries()->add($industry);
		}
	}
	
	public function removeIndustry(Sector $industry)
	{
		if ($this->getIndustries()->contains($industry)) {
			$this->getIndustries()->removeElement($industry);
		}
	}
	
	/**
	 *
	 * @return \Doctrine\Common\Collections\ArrayCollection
	 */
	public function getProfessions()
	{
		return $this->professions;
	}
	
	/**
	 *
	 * @param array $Professions
	 */
	public function setProfessions(array $professions)
	{
		foreach ($this->getProfessions() as $profession) {
			$this->removeProfession($profession);
		}
	
		foreach ($professions as $profession) {
			$this->addProfession($profession);
		}
	}
	
	public function addProfession(SectorJob $profession)
	{
		if (!$this->getProfessions()->contains($profession)) {
			$this->getProfessions()->add($profession);
		}
	}
	
	public function removeProfession(SectorJob $profession)
	{
		if ($this->getProfessions()->contains($profession)) {
			$this->getProfessions()->removeElement($profession);
		}
	}
	
	
    /**
     *
     * @param string $facebook            
     */
    public function setFacebook($facebook)
    {
        $this->facebook = $facebook;
    }

    /**
     *
     * @param string $twitter            
     */
    public function setTwitter($twitter)
    {
        $this->twitter = $twitter;
    }
    
    
    /**
     *
     * @param string $linkedin            
     */
    public function setLinkedin($linkedin)
    {
        $this->linkedin = $linkedin;
    }

    /**
     *
     * @param string $about            
     */
    public function setAbout($about)
    {
        $this->about = $about;
    }

    /**
     *
     * @param boolean $isVerified            
     */
    public function setIsVerified($isVerified)
    {
        $this->isVerified = $isVerified;
    }

    /**
     *
     * @param \Wanawork\UserBundle\Entity\User $verifiedBy            
     */
    public function setVerifiedBy($verifiedBy)
    {
        $this->verifiedBy = $verifiedBy;
    }

    /**
     *
     * @param number $companyNumber            
     */
    public function setCompanyNumber($companyNumber)
    {
        $this->companyNumber = $companyNumber;
    }

    /**
     * 
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getJobSpecs()
    {
        return $this->jobSpecs;
    }

    public function setJobSpecs(array $jobSpecs)
    {
        foreach ($this->getJobSpecs() as $jobSpec) {
            $this->removeJobSpec($jobSpec);
        }
        
        foreach ($jobSpecs as $jobSpec) {
            $this->addJobSpec($jobSpec);
        }
    }
    
    public function addJobSpec(JobSpec $jobSpec)
    {
        if (!$this->getJobSpecs()->contains($jobSpec)) {
            $this->getJobSpecs()->add($jobSpec);
            $jobSpec->setEmployer($this);
        }
    }
    
    public function removeJobSpec(JobSpec $jobSpec)
    {
        if ($this->getJobSpecs()->contains($jobSpec)) {
            $this->getJobSpecs()->removeElement($jobSpec);
            $jobSpec->setEmployer(null);
        }
    }

    /**
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getCvRequests($status = null)
    {
        $requests = $this->cvRequests;
        
        if ($status !== null) {
            $requests = 
            $requests->filter(function(CvRequest $request) use($status){
            	return $request->getStatus() === $status;
            });    
        }
        
        return $requests;
    }
    
    /**
     *
     * @param array $cvRequests
     */
    public function setCvRequests(array $cvRequests)
    {
        foreach ($this->getCvRequests() as $cvRequest) {
            $this->removeCvRequest($cvRequest);
        }
        
        foreach ($cvRequests as $cvRequest) {
            $this->addCvRequest($cvRequest);
        }
        
    }

    public function addCvRequest(CvRequest $cvRequest)
    {
        if (!$this->getCvRequests()->contains($cvRequest)) {
            $this->getCvRequests()->add($cvRequest);
            $cvRequest->setEmployerProfile($this);
        }
    }
    
    public function removeCvRequest(CvRequest $cvRequest)
    {
        if ($this->getCvRequests()->contains($cvRequest)) {
            $this->getCvRequests()->removeElement($cvRequest); 
            $cvRequest->setEmployerProfile(null);           
        }
    }
    
    /**
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getUsers()
    {
        return $this->users;
    }

    public function addUser(User $user, $isAdmin = false)
    {
        if (!$this->getUsers()->contains($user)) {
            
            if ($this->getAdminUsers()->count() === 0 && $isAdmin === false) {
                throw new MissingEmployerAdminException('The first user in employerProfile must be admin');
            }
            
            $this->getUsers()->add($user);
            $user->addEmployerProfile($this, $isAdmin);
        }
    }
    
    public function removeUser(User $user)
    {
        if ($this->getUsers()->contains($user)) {
            if ($user->hasRole($this->getAdminRole()) && $this->getAdminUsers()->count() === 1) {
                throw new MissingEmployerAdminException('Cannot remove the user, as they are the only admin');
            }
            
            $this->getUsers()->removeElement($user);
            $user->removeEmployerProfile($this);
        }
    }
    
    /**
     * 
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getAdminUsers()
    {
        $adminRole = $this->getAdminRole();
        return $this->getUsers()->filter(function(User $user) use($adminRole){
        	return $user->hasRole($adminRole);
        });
    }

    /**
     *
     * @return the Role
     */
    public function getAdminRole()
    {
        return $this->adminRole;
    }

    /**
     *
     * @return the Role
     */
    public function getUserRole()
    {
        return $this->userRole;
    }
    
    /**
     * Get the number of requests this profile can make today
     * @return number
     */
    public function getRemainingCVRequests()
    {
        $dateStart = new \DateTime();
        $dateStart->setTime($hour = 0, $minute = 0, $second = 0);
        $dateEnd = new \DateTime();
        $dateEnd->setTimestamp($dateStart->getTimestamp() + 86399);
        
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->gte('createDate', $dateStart));
        $criteria->andWhere(Criteria::expr()->lte('createDate', $dateEnd));
        
        $todaysRequests = $this->getCvRequests()->matching($criteria);
        
        $leftRequests = 0;
        if ($this->isVerified()) {
            $leftRequests = self::VERIFIED_DAILY_REQUESTS - $todaysRequests->count();
        } else {
            $leftRequests = self::UNVERIFIED_DAILY_REQUESTS - $todaysRequests->count();
        }
        
        if ($leftRequests < 0) {
            $leftRequests = 0;   
        }
        
        return $leftRequests;
    }
    
    public function addVerificationOrder(VerificationOrder $order)
    {
        if (!$this->getVerificationOrders()->contains($order)) {
            $this->getVerificationOrders()->add($order);
        }
    }

    public function getVerificationOrders()
    {
        return $this->verificationOrders;
    }
    
    /**
     * Was the verification paid for?
     * @return boolean
     */
    public function isVerificationPaid()
    {
        foreach ($this->getVerificationOrders() as $order) {
            if ($order->getStatus() === VerificationOrder::STATUS_PAID) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Get the order that initiated verification
     * @return \Wanawork\UserBundle\Entity\Billing\VerificationOrder | null
     */
    public function getVerificationOrder()
    {
        $verificationOrder = null;
        foreach ($this->getVerificationOrders() as $order) {
            if ($order->getStatus() === VerificationOrder::STATUS_PAID) {
                $verificationOrder = $order;
                break;
            }
        }
        
        return $verificationOrder;
    }
    
    public function getOrders($status = null)
    {
        return
        $this->getVerificationOrders()->filter(function(Order $order) use ($status){
        	return $status === null || $order->getStatus() === $status;
        });    
    }
	
    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }
    
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }
}
