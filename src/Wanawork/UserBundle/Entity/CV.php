<?php
namespace Wanawork\UserBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CV Entity
 * @author Ilya Biryukov <ilya@goparty.ie>
 * @ORM\Entity
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({"cvfile" = "CVFile", "cvform" = "CVForm"})
 * @ORM\HasLifecycleCallbacks
 * 
 * @Assert\GroupSequence({"CV", "step1"})
 */
abstract class CV
{
    const TYPE_FILE = 'file';
    const TYPE_FORM = 'form';
    
	/**
	 * CV ID
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;
	
	/** 
	 * A nick name for the cv
	 * @var string 
	 * @ORM\Column(type="string", length=200)
	 * @Assert\NotBlank(message="Please name your CV", groups={"Default", "step1"})
	 * @Assert\Length(max=200, maxMessage="CV Name cannot be longer than {{ limit }} characters", groups={"Default", "step1"})
	 */
	protected $name;
	
	/**
	 * Profile that owns this CV
	 * @var EmployeeProfile
	 * @ORM\ManyToOne(targetEntity="EmployeeProfile", inversedBy="cvs")
	 * @Assert\NotNull(message="Your profile is missing", groups={"Default", "step1"})
	 * @Assert\Valid()
	 */
	protected $profile;
	
	/** 
	 * List of ads posted with this cv
	 * @var Doctrine\Common\Collections\ArrayCollection
	 * @ORM\OneToMany(targetEntity="Wanawork\UserBundle\Entity\Ad", mappedBy="cv", cascade={"all"})
	 */
	protected $ads;
	
	/** 
	 * When the cv was created
	 * @var \DateTime
	 * @ORM\Column(type="datetime")
	 * @Assert\NotNull()
	 */
	protected $created;
	
	/**
	 * When the cv was updated
	 * @var \DateTime
	 * @ORM\Column(type="datetime")
	 * @Assert\NotNull()
	 */
	protected $updated;
	
	public function __construct()
	{
		$this->ads = new ArrayCollection();
		$this->created = new \DateTime();
		$this->updated = new \DateTime();
	}
	
	/**
	 * @return integer $id
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @return the $name
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}
	
	/**
	 * 
	 * @return \Doctrine\Common\Collections\ArrayCollection
	 */
	public function getAds()
	{
		return $this->ads;
	}

	public function setAds(array $ads)
	{
	    foreach ($this->getAds() as $ad) {
	        $this->removeAd($ad);
	    }
	    
	    foreach ($ads as $ad) {
	        $this->addAd($ad);
	    }
	    
	}
	
	public function addAd(Ad $ad)
	{
	   if (!$this->getAds()->contains($ad)) {
	       $this->getAds()->add($ad);
	       $ad->setCv($this);
	   }    
	}
	
	public function removeAd(Ad $ad)
	{
	    if ($this->getAds()->contains($ad)) {
	        $this->getAds()->removeElement($ad);
	        $ad->setCv(null);
        }
	}

    public function getCreated()
    {
        return $this->created;
    }

    public function getUpdated()
    {
        return $this->updated;
    }
    
    /**
     * @param \DateTime $date
     * @ORM\PreUpdate
     */
    public function setUpdated($date = null)
    {
        if (!$date instanceof \DateTime) {
           $date = new \DateTime(); 
        }
        $this->updated = $date;
    }
    
    /**
     * @return \Wanawork\UserBundle\Entity\EmployeeProfile $profile
     */
    public function getProfile()
    {
        return $this->profile;
    }
    
    /**
     * @param \Wanawork\UserBundle\Entity\EmployeeProfile $profile
     */
    public function setProfile($profile)
    {
        if($this->getProfile() instanceof EmployeeProfile) {
            $this->getProfile()->removeCV($this);
        }
         
        if ($profile instanceof EmployeeProfile) {
            $profile->addCv($this);
        }
         
        $this->profile = $profile;
    }
    
    abstract function getType();
}

