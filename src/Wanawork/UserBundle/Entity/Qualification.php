<?php
namespace Wanawork\UserBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Qualification - Level of education
 * @author Ilya Biryukov <ilya@goparty.ie>
 * @ORM\Entity(repositoryClass="Wanawork\UserBundle\Repository\QualificationRepository")
 * @ORM\ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @UniqueEntity("id")
 */
class Qualification
{

	/**
	 * Qualitification ID
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 */
	protected $id;
	
	/**
	 * Qualification name
	 * @var string 
	 * @ORM\Column(type="string", unique=true)
	 * @Assert\NotBlank(message="Please enter qualification name")
	 */
	protected $name;
	
	/** 
	 * Is this a popular qualification
	 * @var boolean 
	 * @ORM\Column(type="boolean")
	 * @Assert\NotNull()
	 * @Assert\Type(type="bool")
	 */
	protected $isPopular;
	
	public function __construct($id, $name, $isPopular)
	{
	    $this->setId($id);
		$this->setName($name);
		$this->setIsPopular($isPopular);
	}
	
	public function setId($id)
	{
	    $this->id = $id;
	}
	
	/**
	 * @return the $id
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @return the $name
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @return the $isPopular
	 */
	public function getIsPopular()
	{
		return $this->isPopular;
	}

	/**
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * @param boolean $isPopular
	 */
	public function setIsPopular($isPopular)
	{
		$this->isPopular = $isPopular;
	}
	
	public function __toString()
	{
		return $this->getName();
	}
	
	public function __toArray()
	{
	    return array(
	    	'id' => $this->getId(),
	        'name' => $this->getName(),
	        'isPopular' => $this->getIsPopular(),
	    );
	}
}

