<?php
namespace Wanawork\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Wanawork\UserBundle\Interfaces\EntityWithFileInterface;
use Symfony\Component\Validator\ExecutionContextInterface;

/**
 * CV In form a of file
 * @author Ilya Biryukov <ilya@wanawork.ie>
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @Assert\GroupSequence({"CVFile", "step1"})
 * @Assert\Callback(methods={"validateFile"}, groups={"Default", "step1"})
 */
class CVFile extends CV implements EntityWithFileInterface
{
    /**
     * The actual cv document
     * @var \Symfony\Component\HttpFoundation\File\File | null
     * 
     * @Assert\File(maxSize="1M", mimeTypes = 
     *  {"application/pdf", "application/x-pdf", "application/msword", 
     *      "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/vnd.oasis.opendocument.text"},
     *  mimeTypesMessage="CV File format is invalid. Please upload PDF, open office or a word document",
     *  groups={"Default", "step1"}
     * )
     * 
     */
    protected $file;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $filePath;
    
    public function validateFile(ExecutionContextInterface $context)
    {
        // this is done for cases when the form is resubmitted and the file is empty
        if ($this->file === null && $this->filePath === null) {
            $context->addViolationAt('file', 'Please upload a CV');
        }
    }
    
    /**
     * 
     * @return \Symfony\Component\HttpFoundation\File
     */
    public function getFile()
    {
        if(!$this->file instanceof \SplFileInfo && file_exists($this->getAbsolutePath())) {
            $this->file = new File($this->getAbsolutePath(), $checkPath = false);
        }
        return $this->file;
    }
    
    public function setFile(File $file)
    {
        $this->file = $file;
        $this->preUpload();
    }
    
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->file) {
            // do whatever you want to generate a unique name
            $this->filePath = sha1(uniqid(mt_rand(), true)) . '.'
                . $this->file->guessExtension();
        }
    }
    
    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->file) {
            return;
        }
    
        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->file->move($this->getUploadRootDir(), $this->filePath);
        unset($this->file);
    }
    
    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        $file = $this->getAbsolutePath();
        if ($file) {
            unlink($file);
        }
    }
    
    public function getAbsolutePath()
    {
        return null === $this->filePath ? null
        : $this->getUploadRootDir() . '/' . $this->filePath;
    }
    
    public function getWebPath()
    {
        return null === $this->filePath ? null
        : $this->getUploadDir() . '/' . $this->filePath;
    }
    
    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return (__DIR__ . '/../../../../web/' . $this->getUploadDir());
    }
    
    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded
        // doc/image in the view.
        return 'uploads/cvs';
    }
    
    public function getType()
    {
        return self::TYPE_FILE;
    }
    
    /**
     * Generate a name for this pdf
     * @return string
     */
    public function getFileName()
    {
        $name = trim($this->getProfile()->getName());
        $name = preg_replace('/\s+/', '_', $name);
        $name = preg_replace('/[^a-zA-Z0-9_]/', '', $name);
        if (!strlen($name)) {
            $name = $this->getProfile()->getId();
        }
        
        $name .= '_' . $this->getId();
        $name .= '.' . $this->getFile()->guessExtension();
        
        return $name;
    }
}