<?php
namespace Wanawork\UserBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Wanawork\UserBundle\Exception\MessageProfileMismatch;

/**
 * 
 * @author Ilya Biryukov <ilya@wanawork.ie>
 * @ORM\Entity(repositoryClass="Wanawork\UserBundle\Repository\MessageRepository")
 */
class Message
{
	
	/**
	 * Message ID
	 * @var integer 
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;
	
	/**
	 * The thread to which this message belongs
	 * @var Wanawork\UserBundle\Entity\MailThread
	 * @ORM\ManyToOne(targetEntity="Wanawork\UserBundle\Entity\MailThread", inversedBy="messages")
	 * @Assert\NotNull
	 */
	protected $thread;
	
	/**
	 * Message Author
	 * @var Wanawork\UserBundle\Entity\User
	 * @ORM\ManyToOne(targetEntity="Wanawork\UserBundle\Entity\User")
	 * @Assert\NotNull
	 */
	protected $author;
	
	/**
	 * Profile that posted this message
	 * @var \Wanawork\UserBundle\Entity\AbstractProfile
	 * 
	 * @ORM\ManyToOne(targetEntity="Wanawork\UserBundle\Entity\AbstractProfile")
	 * ORM\JoinColumn(name="profile_id", referencedColumnName="id")
	 * @Assert\NotNull
	 */
	protected $profile;
	
	/**
	 * Is the message read?
	 * @var bool
	 * @ORM\Column(type="boolean")
	 */
	protected $isRead;
	
	/**
	 * Message body
	 * @var string 
	 * @ORM\Column(type="text")
	 * @Assert\NotBlank(message="Please enter the message")
	 */
	protected $text;
	
	/**
	 * When the message was posted
	 * @var \DateTime 
	 * @ORM\Column(type="datetime")
	 * @Assert\NotNull
	 * @Assert\DateTime
	 */
	protected $postedOn;
	
	public function __construct($text, User $author, AbstractProfile $profile, MailThread $thread) {
		$date = new \DateTime();
		$this->setText($text);
		$this->setAuthor($author);
		$this->setThread($thread);
		$this->postedOn = $date;
		$this->isRead = false;
		$this->getThread()->setUpdatedOn($date);
		$this->setProfile($profile);
		
// 		if(!$this->getThread()->getCvRequest()->isGranted()) {
// 			throw new \Exception("The candidate must first approve your request before you can start communicating");
// 		}
	}
	
	public function getId()
	{
		return $this->id;
	}
	
	public function setText($text)
	{
		$this->text = $text;
	}
	
	/**
	 * 
	 * @return string
	 */
	public function getText()
	{
		return $this->text;
	}
	
	public function getShortText($chars = 20, $terminate = '...')
	{
		$text = $this->getText();
		$shortText = '';
		$i = 0;
		$textSize = mb_strlen($text);
		$maxLength = $chars - mb_strlen($terminate);
		while($i < $chars && $i < $textSize) {
			$shortText .= $text{$i};
			++$i;
		}
		
		if ($i === $chars) {
		    $shortText .= $terminate;
		}
		return $shortText;
	}
	
	public function setAuthor(User $author)
	{
		$this->author = $author;
	}
	
	/**
	 * 
	 * @return \Wanawork\UserBundle\Entity\User
	 */
	public function getAuthor()
	{
		return $this->author;
	}
	
	/**
	 * @return \Wanawork\UserBundle\Entity\AbstractProfile | null
	 */
	public function getRecepient()
	{
	    $profile = null;
	    if ($this->isAuthorEmployee()) {
	        $profile = $this->getThread()->getEmployer();
	    } elseif ($this->isAuthorEmployer()) {
	        $profile = $this->getThread()->getEmployee();
	    }
	    
	    return $profile;
	}
	
	public function setThread(MailThread $thread)
	{
		$this->thread = $thread;
	}
	
	/**
	 * 
	 * @return \Wanawork\UserBundle\Entity\MailThread
	 */
	public function getThread()
	{
		return $this->thread;
	}
	
	/**
	 * 
	 * @return boolean
	 */
	public function isRead() 
	{
		return $this->isRead;
	}
	
	/**
	 * mark the message as read
	 */
	public function markRead() 
	{
		$this->isRead = true;
	}
	
	/**
	 * When the message was posted
	 * @return \DateTime
	 */
	public function getPostedOn() 
	{
		return clone $this->postedOn;
	}
	
	/**
	 * @return boolean
	 */
	public function isAuthorEmployer() 
	{
		return $this->getThread()->getEmployer() === $this->getProfile();
	}
	
	/**
	 * @return boolean
	 */
	public function isAuthorEmployee() 
	{
		return $this->getThread()->getEmployee() === $this->getProfile();
	}

	/**
	 * @return \Wanawork\UserBundle\Entity\AbstractProfile
	 */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * Set the profile that from which this message was posted
     * 
     * @param AbstractProfile $profile
     * @throws MessageProfileMismatch
     * @throws \InvalidArgumentException
     */
    public function setProfile(AbstractProfile $profile)
    {
        if ($profile instanceof EmployerProfile) {
            if($this->getThread()->getEmployer() !== $profile) {
                $msg = 'Employer profile in the message does not match the employer profile in the thread';
                throw new MessageProfileMismatch($msg);
            }
            
        } elseif ($profile instanceof EmployeeProfile) {
            if($this->getThread()->getEmployee() !== $profile) {
                $msg = 'Employee profile in the message does not match the employee profile in the thread';
                throw new MessageProfileMismatch($msg);
            }
        } else {
            throw new \InvalidArgumentException('Provided profile is does not match employer or employee profile');
        }
        
        $this->profile = $profile;
    }
    
    const DAY_LENGTH = 86400;
    const YEAR_LENGTH = 31557600;
    
    /**
     * Facebook style message date
     * @return string
     */
    public function getPostDateInWords()
    {
        $lastDate = $this->getPostedOn();
        $now = new \DateTime();
        $date = null;
        
        if ($now->format('y-m-d') === $lastDate->format('y-m-d')) { // Same day
            $date = $lastDate->format('G:i');
        } elseif(($now->getTimestamp() - $lastDate->getTimestamp()) <= (86400 * 7)) { // Less than a week
            $date = $lastDate->format('l');
        } elseif (($now->getTimestamp() - $lastDate->getTimestamp()) < 31557600) { // less than 1 year
            $date = $lastDate->format('j F');
        } else { // everything else
            $date = $lastDate->format('d/m/Y');
        }
        
        return $date;
    }
}
