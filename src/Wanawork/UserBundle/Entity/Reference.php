<?php
namespace Wanawork\UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Reference entry on CV
 * @author Ilya Biryukov <ilya@goparty.ie>
 * @ORM\Entity
 */
class Reference
{

	/**
	 * Reference ID
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;
	
	/**
	 * Company Name
	 * @var string
	 * 
	 * @ORM\column(type="string", length=200)
	 * @Assert\NotBlank(message="Please enter the company name")
	 * @Assert\Length(max=200, maxMessage="Company name cannot be longer than {{ limit }} characters")
	 */
	protected $employer;
	
	/**
	 * Contact name in the company
	 * @var string 
	 * @ORM\Column(type="string", length=200)
	 * @Assert\NotBlank(message="Please enter the contact name in the company")
	 * @Assert\Length(max=200, maxMessage="Contact name cannot be longer than {{ limit }} characters")
	 */
	protected $contact;
	
	/** 
	 * Contact telephone
	 * @var string 
	 * @ORM\Column(type="string", length=20)
	 * @Assert\NotBlank(message="Please enter the phone number for your referee")
	 * @Assert\Length(max=20, maxMessage="Phone number cannot be longer than {{ limit }} characters")
	 */
	protected $telephone;
	
	/**
	 * Contact email
	 * @var string 
	 * 
	 * @ORM\Column(type="string", length=200)
	 * @Assert\NotBlank(message="Please enter the email for your referee")
	 * @Assert\Email(message="Please enter the email for your referee")
	 * @Assert\Length(max=200, maxMessage="Email cannot be longer than {{ limit }} characters")
	 */
	protected $email;
	
	/**
	 * CV to which the reference belongs
	 * @var Wanawork\UserBundle\Entity\CV
	 * @ORM\ManyToOne(targetEntity="Wanawork\UserBundle\Entity\CVForm", inversedBy="references")
	 * @Assert\NotNull(message="Please select a CV")
	 */
	protected $cv;
	
// 	public function __construct($employer = null, $contact = null, $telephone = null, $email = null, CV $cv = null) 
// 	{
// 		$this->setEmail($email);
// 		$this->setEmployer($employer);
// 		$this->setContact($contact);
// 		$this->setTelephone($telephone);
// 		$this->setCv($cv);
// 	}
	
	/**
	 * @return integer $id
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @return string $employer
	 */
	public function getEmployer()
	{
		return $this->employer;
	}

	/**
	 * @return string $contact
	 */
	public function getContact()
	{
		return $this->contact;
	}

	/**
	 * @return string $telephone
	 */
	public function getTelephone()
	{
		return $this->telephone;
	}

	/**
	 * @return string $email
	 */
	public function getEmail()
	{
		return $this->email;
	}

	/**
	 * @return \Wanawork\UserBundle\Entity\CV $cv
	 */
	public function getCv()
	{
		return $this->cv;
	}

	/**
	 * @param string $employer
	 */
	public function setEmployer($employer)
	{
		$this->employer = $employer;
	}

	/**
	 * @param string $contact
	 */
	public function setContact($contact)
	{
		$this->contact = $contact;
	}

	/**
	 * @param string $telephone
	 */
	public function setTelephone($telephone)
	{
		$this->telephone = $telephone;
	}

	/**
	 * @param string $email
	 */
	public function setEmail($email)
	{
		$this->email = $email;
	}

	/**
	 * @param \Wanawork\UserBundle\Entity\CV $cv
	 */
	public function setCv(CV $cv = null)
	{
		$this->cv = $cv;
	}
}
?>