<?php
namespace Wanawork\UserBundle\Entity;

use Wanawork\MainBundle\Entity\Language;
use Wanawork\MainBundle\Entity\County;
use Wanawork\MainBundle\Entity\NameTitle;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Wanawork\UserBundle\Entity\Billing\Order;
use Wanawork\UserBundle\Entity\Billing\Payment;

/** 
 * Employee Profile 
 * 
 * @author Ilya Biryukov <ilya@goparty.ie>
 * 
 * @ORM\Entity
 */
class EmployeeProfile extends AbstractProfile
{
    /**
     * Employee name
     *
     * @var string 
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Please enter your name", groups={"step1"})
     * @Assert\Length(max=255, maxMessage="Name cannot be longer than {{ limit }} characters", groups={"step1"})
     */
    protected $name;

    /**
     * Name Title
     *
     * @var \Wanawork\MainBundle\Entity\NameTitle 
     * @ORM\ManyToOne(targetEntity="Wanawork\MainBundle\Entity\NameTitle")
     * @Assert\Type(type="\Wanawork\MainBundle\Entity\NameTitle", message="Please select your title", groups={"step1"})
     */
    protected $title;

    /**
     * Date of birth
     * 
     * @var \DateTime 
     * 
     * @ORM\Column(type="date", nullable=true)
     * @Assert\Date(message="Please enter your date of birth", groups={"step1"})
     */
    protected $dob;

    /**
     * List Of CVS
     * @var \Doctrine\Common\Collections\ArrayCollection 
     * @ORM\OneToMany(targetEntity="CV", mappedBy="profile", cascade={"all"})
     * @ORM\OrderBy({"updated"="DESC"})
     */
    protected $cvs;
    
    /**
     * List of ads this profile posted
     * @var \Doctrine\Common\Collections\ArrayCollection
     * @ORM\OneToMany(targetEntity="Wanawork\UserBundle\Entity\Ad", mappedBy="profile", cascade={"all"})
     */
    protected $ads;
    
    /**
     * User that owns this profile
     * @var \Wanawork\UserBundle\Entity\User
     * @ORM\OneToOne(targetEntity="Wanawork\UserBundle\Entity\User", inversedBy="employeeProfile")
     * @Assert\NotNull(message="Employee profile requires a user", groups={"step1"})
     * @Assert\Type(type="\Wanawork\UserBundle\Entity\User", message="Employee profile requires a user", groups={"step1"})
     */
    protected $user;

    public function __construct(NameTitle $title = null, $name = null, \DateTime $dob = null, $phoneNumber = null, $address = null, $city = null, County $county = null)
    {
        $this->title = $title;
        $this->dob = $dob;
        $this->city = $city;
        $this->county = $county;
        $this->name = $name;
        $this->address = $address;
        $this->phoneNumber = $phoneNumber;
        $this->cvs = new ArrayCollection();
        $this->ads = new ArrayCollection();
    }
    
    /**
     *
     * @return \Wanawork\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
    
    public function setUser($user)
    {
        $this->user = $user;   
    }

    /**
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     *
     * @return Wanawork\MainBundle\Entity\NameTitle $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     *
     * @return \DateTime $dob
     */
    public function getDob()
    {
        return $this->dob;
    }


    /**
     *
     * @param string $name            
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     *
     * @param \Wanawork\MainBundle\Entity\NameTitle $title            
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     *
     * @param \DateTime $dob            
     */
    public function setDob($dob)
    {
        $this->dob = $dob;
    }

    /**
     *
     * @param \Doctrine\Common\Collections\ArrayCollection $cvs            
     */
    public function setCvs(array $cvs)
    {
        foreach ($this->getCvs() as $cv) {
            $this->getCvs()->removeElement($cv);
        }
        
        foreach ($cvs as $cv) {
            $this->addCv($cv);
        }
    }

    /**
     *
     * @return ArrayCollection $cvs
     */
    public function getCvs()
    {
        return $this->cvs;
    }

    public function addCv(CV $cv)
    {
        if (!$this->getCvs()->contains($cv)) {
            $this->getCvs()->add($cv);
            $cv->setProfile($this);
        } 
    }
    
    public function removeCV(CV $cv)
    {
        if ($this->getCvs()->contains($cv)) {
            $this->getCvs()->removeElement($cv);
            $cv->setProfile(null);
        }
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getAds()
    {
        return $this->ads;
    }
    
    public function addAd(Ad $ad)
    {
        if(!$this->getAds()->contains($ad)) {
            $this->getAds()->add($ad);
            $ad->setProfile($this);
        }
    }
    
    public function removeAd(Ad $ad)
    {
        $this->getAds()->removeElement($ad);
    }

    /**
     * Check if this profile has ads
     * 
     * @return boolean
     */
    public function hasAds()
    {
        return sizeof($this->getAds()) > 0;
    }
    
    /**
     * Get requests for this profile
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getCvRequests($status = null)
    {
        $requests = array();
        foreach ($this->getAds() as $ad) {
            foreach ($ad->getAccessRequests() as $request) {
                if($status === null || $request->getStatus() === $status) {
                    $requests[] = $request;
                }
            }
        }
        
        usort($requests, function(CvRequest $r1, CvRequest $r2){
        	if ($r1->getCreateDate() == $r2->getCreateDate()) {
        	    return 0;
        	}
        	return $r1->getCreateDate() > $r2->getCreateDate() ? -1 : 1;
        });
        
        return new ArrayCollection($requests);
    }
    
    public function getPendingCvRequests()
    {
        return $this->getCvRequests(CvRequest::AWAITING);
    }
    
    /**
     * Get person's age in years
     * @return integer
     */
    public function getAge()
    {
        $dob = $this->getDob();
        $now = new \DateTime();
        $interval = $now->diff($dob);
        return $interval->y;
    }
    
    public function getOrders($status = null)
    {
        $orders = array();
        foreach ($this->getAds() as $ad) {
            $adOrders = $ad->getOrders($status);
            foreach ($adOrders as $order) {
                $orders[] = $order;
            }
        }

        return $orders;
    }
    
    
    /**
     * Check if the user can pay for ads for a tweet
     * @return boolean
     */
    public function canPayWithTwitter()
    {
        $canPay = true;
        $paidOrders = $this->getOrders(Order::STATUS_PAID);
        
        foreach ($paidOrders as $paidOrder) {
            $payments = $paidOrder->getPayments(Payment::STATE_DEPOSITED, Payment::SOURCE_TWITTER);
            if (sizeof($payments) > 0) {
                $canPay = false;
                break;
            }
        }
        
        return $canPay;
    }
}

