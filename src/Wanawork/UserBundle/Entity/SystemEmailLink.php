<?php
namespace Wanawork\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Wanawork\MainBundle\Services\UUID;

/**
 * Represent each individual link in the email
 * @author Ilya Biryukov <ilya@wannawork.ie>
 * @ORM\Entity
 */
class SystemEmailLink
{
    /**
     * Email ID
     * @var string
     * @ORM\Column(type="string")
     * @ORM\Id
     */
    protected $id;
    
    /**
     * Email to which the link belongs
     * @var \Wanawork\UserBundle\Entity\SystemEmail
     * @ORM\ManyToOne(targetEntity="SystemEmail", inversedBy="links")
     */
    protected $systemEmail;
    
    /**
     * Link to go to
     * @var string
     * @ORM\Column(type="string", length=250)
     */
    protected $href;
    
    /**
     * Link text
     * @var string
     * @ORM\Column(type="string", length=250)
     */
    protected $text;
    
    /**
     * list of clicks
     * @var \Doctrine\Common\Collections\ArrayCollection
     * @ORM\OneToMany(targetEntity="SystemEmailLinkClick", mappedBy="link", cascade={"all"}, fetch="EXTRA_LAZY")
     * @ORM\OrderBy({"date"="DESC"})
     */
    protected $clicks;
    
    public function __construct(SystemEmail $systemEmail, $href, $text)
    {
        $this->id = sha1(uniqid(UUID::v4(), true));
        $this->systemEmail = $systemEmail;
        $this->href = $href;
        $this->text = $text;
        $this->clicks = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \Wanawork\UserBundle\Entity\SystemEmail
     */
    public function getSystemEmail()
    {
        return $this->systemEmail;
    }

    /**
     * @return string
     */
    public function getHref()
    {
        return $this->href;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getClicks()
    {
        return $this->clicks;
    }
	
    /**
     * Record a new link click
     * @param string $ip
     * @return \Wanawork\UserBundle\Entity\SystemEmailLinkClick
     */
    public function addClick($ip)
    {
        $link = $this;
        $click = new SystemEmailLinkClick($link, $ip);
        $this->getClicks()->add($click);
        return $click;
    }
    
}