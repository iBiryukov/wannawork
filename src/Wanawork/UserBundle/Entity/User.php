<?php
namespace Wanawork\UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Wanawork\UserBundle\Entity\Billing\Order;
use Wanawork\MainBundle\Services\UUID;
use Wanawork\UserBundle\Entity\Social\Twitter;

/**
 * User Entity
 * @author Ilya Biryukov <ilya@cookfuture.com>
 *         @Orm\Entity(repositoryClass="Wanawork\UserBundle\Repository\UserRepository")
 *         @UniqueEntity(fields={"email"}, message="This email is already in use")
 *         @ORM\Table(name="users")
 */
class User implements AdvancedUserInterface
{

    /**
     * User ID
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Password's salt
     * @ORM\Column(type="string", length=255)
     * 
     */
    protected $salt;

    /**
     * User password
     * @ORM\Column(type="string", length=512)
     * @Assert\NotBlank(message="Please enter a password")
     */
    protected $password;

    /**
     * User's email address
     * @ORM\Column(type="string", length=60, unique=true)
     * @Assert\NotBlank(message="Please enter your email")
     * @Assert\Email(message="Please enter a valid email address")
     * @Assert\Length(max=60, maxMessage="Your email is too long. Max {{ limit }} characters")
     */
    protected $email;

    /**
     * Full name
     *
     * @var string 
     * @ORM\Column(type="string")
     * @Assert\NotBlank(message="Please enter your name")
     * @Assert\Length(max=250, maxMessage="Your name is too long. Max {{ limit }} characters")
     */
    protected $name;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     * @Assert\Type(type="bool")
     */
    protected $isActive;

    /**
     * @ORM\ManyToMany(targetEntity="Wanawork\UserBundle\Entity\Role", inversedBy="users", indexBy="id")
     *
     * @var \Doctrine\Common\Collections\ArrayCollection 
     * @Assert\Count(min=1, minMessage="The user is missing a role")
     */
    protected $roles;

    /**
     *
     * @var boolean @ORM\Column(type="boolean")
     *      @Assert\Type(type="bool")
     */
    protected $isEnabled;

    /**
     * employer profile, if has one
     *
     * @var \Doctrine\Common\Collections\ArrayCollection 
     * @ORM\ManyToMany(targetEntity="EmployerProfile", mappedBy="users", cascade={"all"})
     *  @ORM\OrderBy({"id"="ASC"})
     */
    protected $employerProfiles;

    /**
     * Employee profile, if one exists
     *
     * @var Wanawork\UserBundle\Entity\EmployeeProfile 
     * @ORM\OneToOne(targetEntity="EmployeeProfile", mappedBy="user", cascade={"all"})
     */
    protected $employeeProfile;

    /** 
     * Default profile to use for this user
     *
     * @var \Wanawork\UserBundle\Entity\AbstractProfile 
     * @ORM\OneToOne(targetEntity="Wanawork\UserBundle\Entity\AbstractProfile")
     * @ORM\JoinColumn(name="defaultprofile_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $defaultProfile;

    /**
     * Timestamp the user signed up
     *
     * @var \DateTime 
     * @ORM\Column(type="datetime")
     */
    protected $registrationDate;

    /**
     * List of password reminders
     *
     * @var Doctrine\Common\Collections\ArrayCollection 
     * @ORM\OneToMany(targetEntity="Wanawork\UserBundle\Entity\PasswordReminder", mappedBy="user", cascade={"all"})
     */
    protected $passwordReminders;

    /**
     * Did the user verify the email?
     *
     * @var boolean @ORM\Column(type="boolean", nullable=true)
     */
    protected $isEmailVerified;

    /**
     * Token used to verify the email address
     *
     * @var string 
     * @ORM\Column(nullable=true)
     */
    protected $emailVerificationToken;

    /**
     * When the email will be verified
     *
     * @var \DateTime 
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $emailVerificationDate;

    /**
     * A list of emails received by this user from the website
     *
     * @var Doctrine\Common\Collections\ArrayCollection 
     * @ORM\OneToMany(targetEntity="Wanawork\UserBundle\Entity\SystemEmail", 
     * cascade={"all"}, mappedBy="user", fetch="EXTRA_LAZY", indexBy="id")
     */
    protected $systemEmails;
    
    /**
     * Key Value list of settings
     * @var \Doctrine\Common\Collections\ArrayCollection
     * @ORM\OneToMany(targetEntity="KeyValue", mappedBy="owner", cascade={"persist", "remove"}, indexBy="key")
     * @Assert\Valid
     */
    protected $settings;
    
    /**
     * Token to use for auto login
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $autoLoginToken;
    
    /**
     * When the token is supposed to expire
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $autoLoginTokenExpiry;
    
    /**
     * List of searches this user saved
     * @var \Doctrine\Common\Collections\ArrayCollection
     * @ORM\OneToMany(targetEntity="SearchNotification", mappedBy="user", cascade={"persist"})
     */
    protected $savedSearches;
    
    /**
     * List of twitter accounts associated with this user
     * @var \Doctrine\Common\Collections\ArrayCollection
     * @ORM\OneToMany(targetEntity="Wanawork\UserBundle\Entity\Social\Twitter", mappedBy="user", cascade={"persist"}, indexBy="id")
     * @ORM\OrderBy({"updatedAt"="DESC"})
     */
    private $twitterAccounts;
    
    public function __construct()
    {
        $this->isActive = true;
        $this->salt = md5(uniqid(null, true));
        $this->roles = new ArrayCollection();
        $this->isEnabled = true;
        $this->registrationDate = new \DateTime();
        $this->passwordReminders = new ArrayCollection();
        $this->isEmailVerified = false;
        $this->emailVerificationDate = null;
        $this->emailVerificationToken = uniqid(null, true);
        $this->systemEmails = new ArrayCollection();
        $this->employerProfiles = new ArrayCollection();
        $this->settings = new ArrayCollection();
        $this->savedSearches = new ArrayCollection();
        $this->twitterAccounts = new ArrayCollection();
    }

    public function isAccountNonExpired()
    {
        return true;
    }

    public function isAccountNonLocked()
    {
        return true;
    }

    public function isCredentialsNonExpired()
    {
        return true;
    }

    public function isEnabled()
    {
        return $this->isEnabled;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getSalt()
    {
        return $this->salt;
    }

    public function getUsername()
    {
        return $this->email;
    }

    public function eraseCredentials()
    {
        // TODO: Auto-generated method stub
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getIsActive()
    {
        return $this->isActive;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Never use this to check if this user has access to anything!
     *
     * Use the SecurityContext, or an implementation of AccessDecisionManager
     * instead, e.g.
     *
     * $securityContext->isGranted('ROLE_USER');
     *
     * @param string $role            
     *
     * @return Boolean
     */
    public function hasRole(Role $role)
    {
        return $this->roles->contains($role);
    }

    /**
     * Check if the user has a particular role name in their profile
     *
     * @param string $name            
     * @return boolean
     */
    public function hasRoleName($name)
    {
        foreach ($this->getRoles() as $role) {
            if ($name === $role->getRole()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Sets the roles of the user.
     *
     * This overwrites any previous roles.
     *
     * @param array $roles            
     */
    public function setRoles(array $roles)
    {
        foreach($this->roles as $role) {
            $this->removeRole($role);
        }
        
        foreach($roles as $role) {
            $this->addRole($role);
        }
    }

    /**
     * Adds a role to the user.
     *
     * @param string $role            
     */
    public function addRole(Role $role)
    {
        $this->roles->set($role->getRole(), $role);
    }

    /**
     * Removes a role to the user.
     *
     * @param string $role            
     */
    public function removeRole(Role $role)
    {
        $this->roles->removeElement($role);
    }

    /**
     *
     * @return Doctrine\Common\Collections\ArrayCollection (non-PHPdoc)
     * @see Symfony\Component\Security\Core\User.UserInterface::getRoles()
     */
    public function getRoles()
    {
        return $this->roles->toArray();
    }
    
    /**
     * Get the list of names
     * @return array
     */
    public function getRoleNames()
    {
        return array_map(function(Role $role){
        	return $role->getName();
        }, $this->getRoles());
    }

    /**
     *
     * @return \Wanawork\UserBundle\Entity\EmployeeProfile $employeeProfile
     */
    public function getEmployeeProfile()
    {
        return $this->employeeProfile;
    }

    public function setEmployeeProfile(EmployeeProfile $profile)
    {
        $this->setDefaultProfile($profile);
        $this->employeeProfile = $profile;
        $profile->setUser($this);
    }

    /**
     *
     * @return \DateTime $registrationDate
     */
    public function getRegistrationDate()
    {
        return $this->registrationDate;
    }

    /**
     *
     * @return the $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     *
     * @param string $name            
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getPasswordReminders()
    {
        return $this->passwordReminders;
    }

    /**
     * Add a new password reminder
     *
     * @param string $ip            
     * @return \Wanawork\UserBundle\Entity\PasswordReminder
     */
    public function addPasswordReminder($ip)
    {
        $reminder = new PasswordReminder($this, $ip);
        $this->getPasswordReminders()->add($reminder);
        return $reminder;
    }
    
    public function removePasswordReminder(PasswordReminder $pr)
    {
        $this->getPasswordReminders()->removeElement($pr);
    }


    public function disable()
    {
        $this->isEnabled = false;
    }

    public function enable()
    {
        $this->isEnabled = true;
    }


    public function __sleep()
    {
        return array(
            "id"
        );
    }

    public function isEmailVerified()
    {
        return $this->isEmailVerified === true;
    }

    public function markEmailAsVerified($flag = true)
    {
        $this->isEmailVerified = (bool) $flag;
        if ($flag) {
            $this->emailVerificationDate = new \DateTime();
        }
    }

    /**
     *
     * @return string
     */
    public function getEmailVerificationToken()
    {
        return $this->emailVerificationToken;
    }

    /**
     *
     * @return \Date
     */
    public function getEmailVerificationDate()
    {
        return $this->emailVerificationDate;
    }

    /**
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getSystemEmails()
    {
        return $this->systemEmails;
    }

    /**
     *
     * @return \Wanawork\UserBundle\Entity\AbstractProfile | null
     */
    public function getDefaultProfile()
    {
        $defaultProfile = $this->defaultProfile;
        if (! $defaultProfile) {
            if ($this->getEmployeeProfile()) {
                $defaultProfile = $this->getEmployeeProfile();
                $this->setDefaultProfile($defaultProfile);
            } elseif ($this->getEmployerProfiles()->count() > 0) {
                $defaultProfile = $this->getEmployerProfiles()->first();
                $this->setDefaultProfile($defaultProfile);
            }
        }
        
        return $defaultProfile;
    }

    public function setDefaultProfile(AbstractProfile $profile = null)
    {
        $this->defaultProfile = $profile;
    }

    /**
     * Get all employer profiles
     * @param bool $adminOnly Get only profiles where the user is admin
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getEmployerProfiles($adminOnly = false)
    {
        $profiles = $this->employerProfiles;
        
        if ($adminOnly === true) {
            $user = $this;
            $profiles = $profiles->filter(function(EmployerProfile $profile) use($user){
                return $user->hasRole($profile->getAdminRole());
            });
        } 

        return $profiles;
    }

    /**
     * Add a new employer profile
     * 
     * @param EmployerProfile $profile            
     */
    public function addEmployerProfile(EmployerProfile $profile, $isAdmin = false)
    {
        if (!$this->getEmployerProfiles()->contains($profile)) {
            $this->setDefaultProfile($profile);
            
            $this->getEmployerProfiles()->add($profile);
            $profile->addUser($this, $isAdmin);
            
            $this->addRole($profile->getUserRole());
            if($isAdmin === true) {
                $this->addRole($profile->getAdminRole());
            }
        }
    }

    public function removeEmployerProfile(EmployerProfile $profile)
    {
        if ($this->getEmployerProfiles()->contains($profile)) {
            $this->getEmployerProfiles()->removeElement($profile);
            $profile->removeUser($this);

            if($this->getDefaultProfile() === $profile) {
                if($this->getEmployerProfiles()->count() > 0) {
                    $this->setDefaultProfile($this->getEmployerProfiles()->first());
                } elseif($this->getEmployeeProfile()) {
                    $this->setDefaultProfile($this->getEmployeeProfile());
                } else {
                    $this->setDefaultProfile(null);
                }
            }
            
            $this->removeRole($profile->getAdminRole());
            $this->removeRole($profile->getUserRole());
        }
    }
    
    /**
     * Get all profiles belonging to this user
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getAllProfiles($adminOnly = false)
    {
        $profiles = clone $this->getEmployerProfiles($adminOnly);
        
        if ($this->getEmployeeProfile()) {
            $profiles[] = $this->getEmployeeProfile();
        }
        return $profiles;
    }
    
    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getSettings()
    {
        return $this->settings;
    }
    
    public function addSetting($key, $value)
    {
        if ($this->getSettings()->containsKey($key)) {
           $kv = $this->getSettings()->get($key); 
           $kv->setValue($value);
        } else {
            $kv = new KeyValue($key, $value, $this);
            $this->getSettings()->set($key, $kv);
        }
    }
    
    public function removeSetting($key)
    {
        if ($this->getSettings()->containsKey($key)) {
            $this->getSettings()->remove($key);
        }
    }
    
    public function getMenuType()
    {
        if($this->getDefaultProfile() instanceof EmployerProfile) {
            return 'employer';
        } elseif($this->getDefaultProfile() instanceof EmployeeProfile) {
            return 'candidate';
        } else {
            if ($this->hasRoleName(\Wanawork\UserBundle\Entity\Role::EMPLOYEE)) {
                return 'candidate';
            }
            
            if ($this->hasRoleName(\Wanawork\UserBundle\Entity\Role::EMPLOYER)) {
                return 'employer';
            }
        }
        return null;
    }
    
    /**
     * Get the list of orders this user placed
     * @param integer $status
     * @return array
     */
    public function getOrders($status = null)
    {
        $orders = array();
        
        $profiles = $this->getEmployerProfiles($adminOnly = true);
        if ($this->getEmployeeProfile()) {
            $profiles[] = $this->getEmployeeProfile();
        }        
        
        foreach ($profiles as $profile) {
            foreach ($profile->getOrders($status) as $order) {
                $orders[] = $order;
            }
        }
        
        usort($orders, function(Order $order1, Order $order2){
        	$d1 = $order1->getCreatedAt();
        	$d2 = $order2->getCreatedAt();
        	
        	if ($d1 == $d2) {
        	    return 0;
        	}
        	
        	return $d1 < $d2 ? 1 : -1;
        });
        
        return $orders;
    }
    
    public function createAutoLoginToken($expiry = null)
    {
        if (!$expiry instanceof \DateTime) {
            $expiry = new \DateTime('+60 days');
        }
        $token = sha1(uniqid(UUID::v4(), true));
        $this->autoLoginToken = $token;
        $this->autoLoginTokenExpiry = $expiry;
        
        return $token;
    }

    public function getAutoLoginToken()
    {
        return $this->autoLoginToken;
    }

    public function getAutoLoginTokenExpiry()
    {
        return $this->autoLoginTokenExpiry;
    }
    
    public function addSavedSearch(SearchNotification $searchNotification)
    {
        if (!$this->getSavedSearches()->contains($searchNotification)) {
            $this->getSavedSearches()->add($searchNotification);
        }
    }
    
    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getSavedSearches()
    {
        return $this->savedSearches;
    }
    
    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getTwitterAccounts()
    {
        return $this->twitterAccounts;
    }
    
    /**
     * Get accounts that have valid access tokens
     * 
     * @param array $oauthOptions
     * @return array
     */
    public function getTwitterAccountsWithValidTokens(array $oauthOptions, $clientConfig = null)
    {
        $accounts = [];
        foreach ($this->getTwitterAccounts() as $account) {
            if ($account->isTokenValid($oauthOptions, $clientConfig)) {
                $accounts[] = $account;
            }
        }
        
        return $accounts;
    }
    
    /**
     * 
     * @param string $id
     * @param string $username
     * @param  \ZendOAuth\Token\Access $token
     * @return \Wanawork\UserBundle\Entity\Social\Twitter
     */
    public function addTwitterAccount($id, $username, \ZendOAuth\Token\Access $token)
    {
        $account = $this->getTwitterAccounts()->get($id);
        if ($account === null) {
            $account = new Twitter($id, $username, $token, $this);    
        } else {
            $account->setToken($token);
            $account->setUsername($username);
        }

        $this->getTwitterAccounts()->set($id, $account);
        
        return $account;
    }
    
    public function getNumberOfUnreadEmails()
    {
        $totalAmount = 0;
        if ($this->getDefaultProfile()) {
            $threads = array();
            foreach ($this->getDefaultProfile()->getCvRequests() as $cvRequest) {
                $thread = $cvRequest->getMailThread();
                if ($thread instanceof MailThread) {
                    $totalAmount += sizeof($thread->getUnreadMessages($this->getDefaultProfile()));
                }
            }
        }
        
        return $totalAmount;
    }
}
