<?php
namespace Wanawork\UserBundle\Entity;

use Wanawork\MainBundle\Entity\Language;
use Wanawork\MainBundle\Entity\County;
use Wanawork\MainBundle\Services\UUID;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * An entity that represents a search
 * @author Ilya Biryukov <ilya@wanawork.ie>
 * @ORM\Entity(repositoryClass="Wanawork\UserBundle\Repository\SearchRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Search
{
	
	/**
	 * Ad ID
	 * @ORM\Column(type="string")
	 * @ORM\Id
	 */
	protected $id;

	/**
	 * Profession in which the user wishes to work
	 * @var \Wanawork\UserBundle\Entity\SectorJob
	 * @ORM\ManyToOne(targetEntity="Wanawork\UserBundle\Entity\SectorJob")
	 * @Assert\NotNull(message="Please pick required profession")
	 * @Assert\Type(type="\Wanawork\UserBundle\Entity\SectorJob", message="Please pick required profession")
	 * @Assert\Valid()
	 */
	protected $profession;
	
	/**
	 * List of industries where the candidate wants to work
	 * @var \Doctrine\Common\Collections\ArrayCollection
	 * @ORM\ManyToMany(targetEntity="Wanawork\UserBundle\Entity\Sector")
	 * @Assert\Valid()
	 */
	protected $industries;
	
	/**
	 * Positions, that were searched for
	 * @var Doctrine\Common\Collections\ArrayCollection
	 * @ORM\ManyToMany(targetEntity="PositionType", cascade={"persist"})
	 */
	protected $positions;
	
	/**
	 * Languages, that were searched for
	 * @var Doctrine\Common\Collections\ArrayCollection
	 * @ORM\ManyToMany(targetEntity="Wanawork\MainBundle\Entity\Language", cascade={"persist"})
	 */
	protected $languages;
	
	/**
	 * Locations, that were searched for
	 * @var Doctrine\Common\Collections\ArrayCollection
	 * @ORM\ManyToMany(targetEntity="Wanawork\MainBundle\Entity\County", cascade={"persist"})
	 */
	protected $locations;
	
	/**
	 * Experience that the candidate has in the above profession
	 * @var \Wanawork\UserBundle\Entity\SectorExperience
	 * @ORM\ManyToOne(targetEntity="Wanawork\UserBundle\Entity\SectorExperience")
	 * @Assert\Type(type="\Wanawork\UserBundle\Entity\SectorExperience", message="Please choose required experience")
	 * @Assert\Valid()
	 */
	protected $experience;
	
	/**
	 * Level of education
	 * @var \Wanawork\UserBundle\Entity\Qualification
	 * @ORM\ManyToOne(targetEntity="Wanawork\UserBundle\Entity\Qualification")
	 *
	 * @Assert\Valid()
	 */
	protected $educationLevel;
	
	/**
	 * Who did the search
	 * @var User
	 * @ORM\ManyToOne(targetEntity="User", cascade={"persist"})
	 */
	protected $user;
	
	/**
	 * When the search was done
	 * @var \DateTime
	 * @ORM\Column(type="datetime")
	 */
	protected $date;
	
	/**
	 * The last time the search was (re-)run
	 * @var \DateTime
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	protected $updateDate;
	
	/**
	 * Does this search requires notification of new ads?
	 * @var Wanawork\UserBundle\Entity\SearchNotification
	 * @ORM\OneToOne(targetEntity="Wanawork\UserBundle\Entity\SearchNotification", mappedBy="search")
	 */
	protected $notification;
	
	/**
	 * Show we show expired ads in the results?
	 * For admin purposes only.
	 * @var boolean
	 * @ORM\Column(type="boolean", nullable=true)
	 */
	protected $includeExpired = false;
	
	/**
	 * Page Stats
	 * @var \Doctrine\Common\Collections\ArrayCollection
	 * @ORM\OneToMany(targetEntity="SearchPageStat", mappedBy="search", cascade={"persist", "remove"})
	 */
	protected $pageStats;
	
	/**
	 * Number of fields used
	 * @var integer
	 * @ORM\Column(type="integer", nullable=true)
	 */
	protected $countOfUsedFields;
	
	public function __construct()
	{
		$this->date = new \DateTime();
		$this->id = sha1(uniqid(UUID::v4(), true));
		$this->pageStats = new ArrayCollection();
		$this->industries = new ArrayCollection();
		$this->positions = new ArrayCollection();
		$this->languages = new ArrayCollection();
		$this->locations = new ArrayCollection();
		$this->countOfUsedFields = 0;
	}
	
	/**
	 * 
	 * @return \Doctrine\Common\Collections\ArrayCollection
	 */
	public function getLanguages()
	{
		return $this->languages;
	}
	
	public function setLanguages(array $languages)
	{
		foreach ($this->getLanguages() as $language) {
		    $this->removeLanguage($language);
		}
		
		foreach ($languages as $language) {
		    $this->addLanguage($language);
		}
	}
	
	public function addLanguage(Language $language)
	{
	    if (!$this->getLanguages()->contains($language)) {    
	        $this->getLanguages()->add($language);
	    }
	}
	
	public function removeLanguage(Language $language)
	{
	    if ($this->getLanguages()->contains($language)) {
	        $this->getLanguages()->removeElement($language);
	    }
	}

	/**
	 * 
	 * @return \Doctrine\Common\Collections\ArrayCollection
	 */
	public function getLocations() {
		return $this->locations;
	}
	
	
	public function setLocations(array $locations)
	{
	    foreach ($this->getLocations() as $location) {
	        $this->removeLocation($location);
	    }
	
	    foreach ($locations as $location) {
	        $this->addLocation($location);
	    }
	}
	
	public function addLocation(County $location)
	{
	    if (!$this->getLocations()->contains($location)) {
	        $this->getLocations()->add($location);
	    }
	}
	
	public function removeLocation(County $location)
	{
	    if ($this->getLocations()->contains($location)) {
	        $this->getLocations()->removeElement($location);
	    }
	}

	/**
	 * 
	 * @return \Doctrine\Common\Collections\ArrayCollection
	 */
	public function getPositions() {
		return $this->positions;
	}
	
	public function setPositions(array $positions)
	{
		foreach ($this->getPositions() as $position) {
		    $this->removePosition($position);
		}
		
		foreach ($positions as $position) {
		    $this->addPosition($position);
		}
	}
	
	public function addPosition(PositionType $position)
	{
	    if (!$this->getPositions()->contains($position)) {
	        $this->getPositions()->add($position);
	    }
	}
	
	public function removePosition(PositionType $position)
	{
	    if ($this->getPositions()->contains($position)) {
	        $this->getPositions()->removeElement($position);
	    }
	}
	
	public function setUser(User $user)
	{
		$this->user = $user;
	}
	
	/**
	 * 
	 * @return \Wanawork\UserBundle\Entity\User
	 */
	public function getUser()
	{
		return $this->user;
	}
	
	public function getId()
	{
		return $this->id;
	}

	/**
	 * 
	 * @return \Wanawork\UserBundle\Entity\SearchNotification
	 */
	public function getNotification()
	{
		return $this->notification;
	}
	
	/**
	 * 
	 * @return boolean
	 */
	public function hasNotifier()
	{
		return $this->getNotification() instanceof SearchNotification;
	}
	
    	
	public function setNotification(SearchNotification $notification) 
	{
		if($this->getNotification()) {
			throw new \Exception("Notifier is alredy registered");
		}
		
		$this->notification = $notification;
	}
	
	
	public function getIncludeExpired()
	{
	    return $this->includeExpired;
	}
	
	public function setIncludeExpired($flag)
	{
	    $this->includeExpired = $flag;
	}

    /**
     *
     * @return \Wanawork\UserBundle\Entity\SectorJob
     */
    public function getProfession()
    {
        return $this->profession;
    }

    /**
     *
     * @param $profession
     */
    public function setProfession($profession)
    {
        $this->profession = $profession;
    }

    /**
     * 
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getIndustries()
    {
        return $this->industries;
    }

    /**
     *
     * @param $industries
     */
    public function setIndustries(array $industries)
    {
        foreach ($this->getIndustries() as $industry) {
            $this->removeIndustry($industry);
        }
        
        foreach ($industries as $industry) {
            $this->addIndustry($industry);
        }
    }
    
    public function addIndustry(Sector $industry) 
    {
        if (!$this->getIndustries()->contains($industry)) {
        	$this->getIndustries()->add($industry);
        }
    }
    
    public function removeIndustry(Sector $industry)
    {
        if ($this->getIndustries()->contains($industry)) {
            $this->getIndustries()->removeElement($industry);
        }
    }

    /**
     *
     * @return the SectorExperience
     */
    public function getExperience()
    {
        return $this->experience;
    }

    /**
     *
     * @param $experience
     */
    public function setExperience($experience)
    {
        $this->experience = $experience;
    }

    /**
     *
     * @return the Qualification
     */
    public function getEducationLevel()
    {
        return $this->educationLevel;
    }

    /**
     *
     * @param $educationLevel
     */
    public function setEducationLevel($educationLevel)
    {
        $this->educationLevel = $educationLevel;
    }

    public function getUpdateDate()
    {
        $date = null;
        if ($this->updateDate) {
            $date = clone $this->updateDate;
        }
        return $date;
    }

    /**
     * @param \DateTime $updateDate
     * @ORM\PreUpdate
     */
    public function setUpdateDate($updateDate = null)
    {
        if (!$updateDate instanceof \DateTime) {
            $updateDate = new \DateTime(); 
        }
        $this->updateDate = $updateDate;
    }
    
    
    /**
     * Set the ads that were found
     * @param array $ads        list of found ads
     * @param integer $page     current page number
     * @param integer $perPage  How many ads displayed per page
     * @param integer $totalFound How many ads found in total on all pages
     * 
     * @return \Wanawork\UserBundle\Entity\SearchPageStat
     */
    public function setAds(array $ads, $page, $perPage, $totalFound)
    {
        $this->setUpdateDate();
        $i = 1;
        $scores = array();
        foreach ($ads as $ad) {
            
            // collect scores
            $scores[] = $ad->getMatch();
            
            // Create ad view
            $user = $this->getUser();
            $employer = null;
            if($user && $user->getDefaultProfile() instanceof EmployerProfile) {
                $employer = $user->getDefaultProfile();
            }
            
            $position = ($page - 1) * $perPage + $i;
            $ad->addView(AdView::VIEW_IN_SEARCH, $user, $employer, $position, $this);
            
            ++$i;
        }
        
        $stat = new SearchPageStat($this, $page, $scores, $totalFound);
        $this->getPageStats()->add($stat);
        return $stat;
    }

    public function getPageStats()
    {
        return $this->pageStats;
    }
    
    /** @ORM\PrePersist */
    public function setNumberOfUsedFields()
    {
        $i = 0;
        if($this->getProfession()) {
            ++$i;
        }
        
        if($this->getIndustries()->count()) {
            ++$i;
        }
        
        if($this->getExperience()) {
            ++$i;
        }
        
        if($this->getPositions()->count()) {
            ++$i;
        }
        
        if($this->getEducationLevel()) {
            ++$i;
        }
        
        if($this->getLanguages()->count()) {
            ++$i;
        }
        
        if($this->getLocations()->count()) {
            ++$i;
        }
        
        $this->countOfUsedFields = $i;
    }
    
    public function getDate()
    {
        return clone $this->date;
    }
    
    public function getCountOfUsedFields()
    {
        return $this->countOfUsedFields;
    }
}
