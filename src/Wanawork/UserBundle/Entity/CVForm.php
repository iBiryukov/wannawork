<?php
namespace Wanawork\UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CV That came from a cv builder
 * @author Ilya Biryukov <ilya@wanawork.ie>
 * @ORM\Entity(repositoryClass="Wanawork\UserBundle\Repository\CVFormRepository")
 */
class CVForm extends CV
{
    /**
     * List of completed educations
     * @var Doctrine\Common\Collections\ArrayCollection
     * @ORM\OneToMany(targetEntity="Wanawork\UserBundle\Entity\Education", mappedBy="cv", cascade={"all"})
     * @ORM\OrderBy({"id" = "ASC"})
     * @Assert\Count(min=1, minMessage="Please add at least one education")
     * @Assert\Valid()
     */
    protected $educations;
    
    /**
     * List of references from previous employment
     * @var Doctrine\Common\Collections\ArrayCollection
     * @ORM\OneToMany(targetEntity="Wanawork\UserBundle\Entity\Reference", mappedBy="cv", cascade={"all"})
     * @ORM\OrderBy({"id" = "ASC"})
     * @Assert\Valid()
     */
    protected $references;
    
    /**
     * Work experience
     * @var Doctrine\Common\Collections\ArrayCollection
     * @ORM\OneToMany(targetEntity="Wanawork\UserBundle\Entity\WorkExperience", mappedBy="cv", cascade={"all"})
     * @ORM\OrderBy({"id" = "ASC"})
     * @Assert\Valid()
     */
    protected $jobs;
    
    /**
     * Achievements/Awards/Hobbies
     * @var Doctrine\Common\Collections\ArrayCollection
     *   \\ORM\OneToMany(targetEntity="Wanawork\UserBundle\Entity\Achievement", mappedBy="cv", cascade={"all"})
     *
     * @ORM\Column(type="text")
     * @Assert\NotBlank(message="Please enter some of your achievements")
     * @Assert\Length(max=2000, maxMessage="Achievements cannot be longer than {{ limit }} characters")
     */
    protected $achievements;
    
    /**
     * Cover Note
     * @var string
     * @ORM\Column(type="text")
     * @Assert\NotBlank(message="Please enter your cover note")
     * @Assert\Length(max=2000, maxMessage="Cover Note cannot be longer than {{ limit }} characters")
     */
    protected $coverNote;
    
    public function __construct()
    {
        parent::__construct();
        $this->educations = new ArrayCollection();
        $this->references = new ArrayCollection();
        $this->jobs = new ArrayCollection();
    }
    
    /**
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }
    
    /**
     * @return string $achievements
     */
    public function getAchievements()
    {
        return $this->achievements;
    }
    
    /**
     * @param string $achievements
     */
    public function setAchievements($achievements)
    {
        $this->achievements = $achievements;
    }
    
    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $educations
     */
    public function setEducations(array $educations)
    {
        foreach ($this->getEducations() as $education) {
            $this->removeEducation($education);
        }
         
        foreach ($educations as $education) {
            $this->addEducation($education);
        }
    }
    
    /**
     * @return \Doctrine\Common\Collections\ArrayCollection $educations
     */
    public function getEducations()
    {
        return $this->educations;
    }
    
    public function addEducation(Education $education)
    {
        if(!$this->getEducations()->contains($education)) {
            $this->getEducations()->add($education);
            $education->setCv($this);
        }
    }
    
    public function removeEducation(Education $education)
    {
        if($this->getEducations()->contains($education)) {
            $education->setCv(null);
            $this->getEducations()->removeElement($education);
        }
    }
    
    /**
     * @return \Doctrine\Common\Collections\ArrayCollection $jobs
     */
    public function getJobs()
    {
        return $this->jobs;
    }
    
    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $jobs
     */
    public function setJobs(array $jobs)
    {
        foreach ($this->getJobs() as $job) {
            $this->removeJob($job);
        }
         
        foreach ($jobs as $job) {
            $this->addJob($job);
        }
         
    }
    
    public function addJob(WorkExperience $job)
    {
        if(!$this->getJobs()->contains($job)) {
            $this->getJobs()->add($job);
            $job->setCv($this);
        }
    }
    
    public function removeJob(WorkExperience $job)
    {
        if ($this->getJobs()->contains($job)) {
            $this->getJobs()->removeElement($job);
            $job->setCv(null);
        }
    }
    
    /**
     * @return \Doctrine\Common\Collections\ArrayCollection $references
     */
    public function getReferences()
    {
        return $this->references;
    }
    
    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $references
     */
    public function setReferences(array $references)
    {
        foreach ($this->getReferences() as $reference) {
            $this->removeReference($reference);
        }
         
        foreach ($references as $reference) {
            $this->addReference($reference);
        }
    }
    
    public function addReference(Reference $reference)
    {
        if (!$this->getReferences()->contains($reference)) {
            $this->getReferences()->add($reference);
            $reference->setCv($this);
        }
    }
    
    public function removeReference(Reference $reference)
    {
        if ($this->getReferences()->contains($reference)) {
            $this->getReferences()->removeElement($reference);
            $reference->setCv(null);
        }
    }
    
    /**
     * @return the $coverNote
     */
    public function getCoverNote()
    {
        return $this->coverNote;
    }
    
    /**
     * @param string $coverNote
     */
    public function setCoverNote($coverNote)
    {
        $this->coverNote = $coverNote;
    }
    
    public function getType()
    {
        return self::TYPE_FORM;
    }
}