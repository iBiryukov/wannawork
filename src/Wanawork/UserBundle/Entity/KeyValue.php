<?php
namespace Wanawork\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Key Value Entity
 * @author Ilya Biryukov <ilya@wanawork.ie>
 * @ORM\Entity
 * 
 */
class KeyValue
{
    /**
     * Key To Store
     * @var string
     * 
     * @ORM\Column(type="string", length=250)
     * @ORM\Id
     * @Assert\NotBlank
     * @Assert\Length(max=250)
     * 
     */
    private $key;
    
    /**
     * Value to store
     * @var string
     * @ORM\Column(type="string", length=250)
     * @Assert\NotBlank
     * @Assert\Length(max=250)
     */
    private $value;
    
    /**
     * Owner
     * @var \Wanawork\UserBundle\Entity\User
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="User", inversedBy="settings")
     * @Assert\NotNull
     */
    private $owner;
    
    public function __construct($key, $value, User $owner)
    {
        $this->key = $key;
        $this->value = $value;
        $this->owner = $owner;
    }

    /**
     *
     * @return the string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     *
     * @return the string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     *
     * @param string $value            
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     *
     * @return the User
     */
    public function getOwner()
    {
        return $this->owner;
    }
    
}