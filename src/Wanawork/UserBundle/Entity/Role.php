<?php
namespace Wanawork\UserBundle\Entity;

use Symfony\Component\Security\Core\Role\RoleInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Role Entity
 * @author Ilya Biryukov <ilya@wannawork.ie>
 *
 * @ORM\Entity(repositoryClass="Wanawork\UserBundle\Repository\RoleRepository")
 * 
 */
class Role extends \Symfony\Component\Security\Core\Role\Role
{
	CONST ADMIN = 'ROLE_ADMIN';
	const EMPLOYEE = 'ROLE_EMPLOYEE';
	const EMPLOYER = 'ROLE_EMPLOYER';
	const USER = 'ROLE_USER';

	/**
	 * Role ID
	 * @var string 
	 * @ORM\Column(type="string", length=255, unique=true)
	 * @ORM\Id
	 * @Assert\NotBlank()
	 */
	protected $id;

	/**
	 * Role name (description)
	 * @var string
	 * @ORM\Column(type="string")
	 * @Assert\NotBlank()
	 */
	protected $name;
	
	/**
 	 *
 	 * @ORM\ManyToMany(targetEntity="Wanawork\UserBundle\Entity\User", mappedBy="roles")
 	 */
	protected $users;
	
	public function __construct($name, $role)
	{
		$this->setName($name);
		$this->setRole($role);
	}
	
	public function setName($name)
	{
		$this->name = $name;
	}
	
	public function getRole()
	{
		return $this->id;
	}
	
	
	public function getName()
	{
		return $this->name;
	}
	
	public function __toString()
	{
		return $this->getName();
	}
	
    public function setRole($role)
    {
        $this->id = $role;
    }
	
	public function __toArray()
	{
        return array(
            'name' => $this->getName(),
            'role' => $this->getRole(),
        );	    
	}
}