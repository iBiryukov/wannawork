<?php
namespace Wanawork\UserBundle\Entity\Social;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Wanawork\UserBundle\Entity\User;

/**
 * Represents a connected twitter account
 * @author Ilya Biryukov <ilya@wannawork.ie>
 * @ORM\Entity()
 * @ORM\Table(name="social_twitter")
 */
class Twitter
{
    use \Gedmo\Timestampable\Traits\TimestampableEntity;
    
    /**
     * ID of twitter account
     * (comes from twitter)
     * @var string
     * @ORM\Column(type="string", length=100)
     * @ORM\Id
     */
    private $id;
    
    /**
     * Twitter username
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $username;
    
    /**
     * Access Token Token
     * @var \ZendOAuth\Token\Access
     * @ORM\Column(type="object_bytea")
     */
    private $token;
    
    /**
     * User who owns this twitter account
     * @var \Wanawork\UserBundle\Entity\User
     * @ORM\ManyToOne(targetEntity="\Wanawork\UserBundle\Entity\User", inversedBy="twitterAccounts")
     * @ORM\Id
     */
    private $user;
    
    /**
     * Fresh user's data from verify_credintials
     * @var array
     */
    private $userData;
    
    /**
     * Constructor 
     * 
     * @param string                            $id         Twitter ID
     * @param string                            $username   Twitter Username
     * @param \ZendOAuth\Token\Access           $token      Twitter Access Token
     * @param \Wanawork\UserBundle\entity\User  $user       Owner of this Twitter Account
     */
    public function __construct($id, $username, \ZendOAuth\Token\Access $token, User $user)
    {
        $this->id = $id;
        $this->setUsername($username);
        $this->setToken($token);
        $this->user = $user;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return \ZendOAuth\Token\Access
     */
    public function getToken()
    {
        return $this->token;
    }

    public function setToken(\ZendOAuth\Token\Access $token)
    {
        $this->token = $token;
    }
    
    /**
     * @return \Wanawork\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
    
    /**
     * Check if the auth token is still valid
     * @param array $oauthOptions
     * 
     * @return boolean
     */
    public function isTokenValid(array $oauthOptions, $clientConfig = null)
    {
        $valid = true;
        try {
            $client = $this->getToken()->getHttpClient($oauthOptions, $url = null, $clientConfig);
            $client->setMethod('GET');
            $client->setUri('https://api.twitter.com/1.1/account/verify_credentials.json');
            $client->setParameterGet(array(
                'skip_status' => true,
                'include_entities' => true,
            ));

            $response = $client->send();
            $valid = $response->getStatusCode() === 200;
            
            if ($valid) {
                $this->userData = json_decode($response->getBody(), true);
            }
            
        } catch (\Exception $e) {
            $valid = false;
        }
        
        return $valid;
    }
    
    /**
     * Get url for profile picture
     * 
     * @param array $oauthOptions
     * @return string | null
     */
    public function getProfilePicture(array $oauthOptions, $clientConfig = null)
    {
        $picture = null;
        
        if (!$this->userData) {
            $this->isTokenValid($oauthOptions, $clientConfig);
        }
        
        if (is_array($this->userData) && array_key_exists('profile_image_url_https', $this->userData)) {
            $picture = $this->userData['profile_image_url_https'];
        }
        
        return $picture;
    }
    
    public function getFollowersCount(array $oauthOptions, $clientConfig = null)
    {
        $count = null;
        if (!$this->userData) {
            $this->isTokenValid($oauthOptions, $clientConfig);
        }
        
        if (is_array($this->userData) && array_key_exists('followers_count', $this->userData)) {
            $count = (int)$this->userData['followers_count'];
        }
        return $count;
    }
}