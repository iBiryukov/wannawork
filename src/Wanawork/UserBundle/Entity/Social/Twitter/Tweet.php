<?php
namespace Wanawork\UserBundle\Entity\Social\Twitter;

use Wanawork\UserBundle\Entity\Social\Twitter\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * @author Ilya Biryukov <ilya@wannawork.ie>
 * @ORM\Entity
 */
class Tweet
{
    /**
     * Tweet ID
     * @var string
     * @ORM\Id
     * @ORM\Column(type="string")
     */
    private $id;
    
    /**
     * Text of the tweet
     * @var string
     * @ORM\Column(type="string", length=350)
     */
    private $text;
    
    /**
     * Retweet?
     * @var \Wanawork\UserBundle\Entity\Social\Twitter\Tweet | null
     * @ORM\ManyToOne(targetEntity="Wanawork\UserBundle\Entity\Social\Twitter\Tweet")
     */
    private $retweet;
    
    /**
     * User that posted the tweet
     * @var \Wanawork\UserBundle\Entity\Social\Twitter\User
     * @ORM\ManyToOne(targetEntity="Wanawork\UserBundle\Entity\Social\Twitter\User")
     */
    private $user;
    
    /**
     * When the tweet was posted
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $createdAt;
    
    
    public function __construct($id, $text, User $user, \DateTime $createAt, Tweet $retweet = null)
    {
        $this->id = $id;
        $this->text = $text;
        $this->retweet = $retweet;
        $this->user = $user;
        $this->createdAt = $createAt;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }
    
    public function getHtmlText()
    {
        $text = $this->text;
        $text = preg_replace_callback('/@(\w+)/', function($matches){
            $username = $matches[1];
        	return '<a href="https://twitter.com/'.$username.'" target="_blank">' . $matches[0] . '</a>';
        }, $text);
        
        $text = preg_replace_callback('/http:\/\/t.co\/\w+/', function($matches){
            return '<a href="'.$matches[0].'" target="_blank">' . $matches[0] . '</a>';
        }, $text);
        
        return $text;
    }

    /**
     * @return \Wanawork\UserBundle\Entity\Social\Twitter\Tweet
     */
    public function getRetweet()
    {
        return $this->retweet;
    }

    /**
     * @return \Wanawork\UserBundle\Entity\Social\Twitter\User
     */
    public function getUser()
    {
        return $this->user;
    }
    
    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
    
}