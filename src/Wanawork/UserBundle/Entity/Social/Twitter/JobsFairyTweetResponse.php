<?php
namespace Wanawork\UserBundle\Entity\Social\Twitter;

use Doctrine\ORM\Mapping as ORM;
use Wanawork\UserBundle\Entity\Social\Twitter;

/**
 * @author Ilya Biryukov <ilya@wannawork.ie>
 * ORM\Entity
 */
class JobsFairyTweetResponse
{
    /**
	 * ID
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;
    
    /**
     * Tweet to respond to
     * @var \Wanawork\UserBundle\Entity\Social\Twitter\JobsFairyTweet
     * @ORM\ManyToOne(targetEntity="Wanawork\UserBundle\Entity\Social\Twitter\JobsFairyTweet", inversedBy="responses")
     */
    private $tweet;
    
    /**
     * When the tweet was posted
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $postedOn;
    
    /**
     * Text to respond with
     * @var string
     * @ORM\Column(type="string", length=140)
     */
    private $text;
    
    /**
     * Account to post from
     * @var \Wanawork\UserBundle\Entity\Social\Twitter
     * @ORM\ManyToOne(targetEntity="Wanawork\UserBundle\Entity\Social\Twitter")
     */
    private $account;
    
    public function __construct(JobsFairyTweet $tweet, $text, Twitter $account = null)
    {
        $this->tweet = $tweet;
        $this->account = $account;
        $this->text = $text;
        $this->postedOn = null;
    }
    
    public function setAccount(Twitter $account)
    {
        $this->account = $account;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTweet()
    {
        return $this->tweet;
    }

    public function getPostedOn()
    {
        return $this->postedOn;
    }

    public function getText()
    {
        return $this->text;
    }

    public function getAccount()
    {
        return $this->account;
    }
    
}