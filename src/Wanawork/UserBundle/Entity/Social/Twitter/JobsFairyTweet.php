<?php
namespace Wanawork\UserBundle\Entity\Social\Twitter;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Wanawork\UserBundle\Entity\SectorJob;

/**
 * @author Ilya Biryukov <ilya@wannawork.ie>
 * @ORM\Entity
 */
class JobsFairyTweet
{
    /**
     * Tweet
     * @var Wanawork\UserBundle\Entity\Social\Twitter\Tweet
     * @ORM\OneToOne(targetEntity="Wanawork\UserBundle\Entity\Social\Twitter\Tweet")
     * @ORM\Id
     * @ORM\JoinColumn(name="id", referencedColumnName="id")
     */
    private $tweet;
    
    /**
     * List of professions we matched for this tweet
     * @var \Doctrine\Common\Collections\ArrayCollection
     * @ORM\ManyToMany(targetEntity="Wanawork\UserBundle\Entity\SectorJob")
     */
    private $matchedProfessions;
    
    /**
     * List of matched ads
     * @var \Doctrine\Common\Collections\ArrayCollection 
     * @ORM\ManyToMany(targetEntity="Wanawork\UserBundle\Entity\Ad")
     */
    private $matchedAds;
    
    /**
     * List of responses to this tweet by us
     * @var \Doctrine\Common\Collections\ArrayCollection
     * ORM\OneToMany(targetEntity="Wanawork\UserBundle\Entity\Social\Twitter\JobsFairyTweetResponse", mappedBy="tweet")
     */
    private $responses;
    
    public function __construct(Tweet $tweet)
    {
        $this->tweet = $tweet;
        $this-> matchedProfessions = new ArrayCollection();
        $this->matchedAds = new ArrayCollection();
        $this->responses = new ArrayCollection();
    }

    /**
     * @return \Wanawork\UserBundle\Entity\Social\Twitter\Tweet
     */
    public function getTweet()
    {
        return $this->tweet;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getMatchedProfessions()
    {
        return $this->matchedProfessions;
    }
    
    public function addProfession(SectorJob $profession)
    {
        if (!$this->getMatchedProfessions()->contains($profession)) {
            $this->getMatchedProfessions()->add($profession);
        }
    }
    
    public function removeProfession(SectorJob $profession)
    {
        if ($this->getMatchedProfessions()->contains($profession)) {
            $this->getMatchedProfessions()->removeElement($profession);
        }
    }
    
    public function setProfessions(array $professions)
    {
        foreach ($this->getMatchedProfessions() as $profession) {
            $this->removeProfession($profession);
        }
        
        foreach ($professions as $profession) {
            $this->addProfession($profession);
        }
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getMatchedAds()
    {
        return $this->matchedAds;
    }
    
}