<?php
namespace Wanawork\UserBundle\Entity\Social\Twitter;

use Doctrine\ORM\Mapping as ORM;

/**
 * @author Ilya Biryukov <ilya@wannawork.ie>
 * @ORM\Entity
 * @ORM\Table(name="twitter_user")
 */
class User
{
    /**
     * ID
     * @var string
     * @ORM\Id
     * @ORM\Column(type="string")
     */
    private $id;
    
    /**
     * Full Name
     * @var string
     * @ORM\Column(type="string")
     */
    private $name;
    
    /**
     * Twitter handle
     * @var string
     * @ORM\Column(type="string")
     */
    private $screenName;
    
    /**
     * URL to avatar
     * @var string
     * @ORM\Column(type="string")
     */
    private $avatarUrl;
    
    public function __construct($id, $name, $screenName, $avatarUrl)
    {
        $this->id = $id;
        $this->name = $name;
        $this->screenName= $screenName;
        $this->avatarUrl = $avatarUrl;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getScreenName()
    {
        return $this->screenName;
    }

    public function getAvatarUrl()
    {
        return $this->avatarUrl;
    }
    
}