<?php
namespace Wanawork\UserBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Wanawork\MainBundle\Entity\County;
use Wanawork\MainBundle\Entity\Language;
use Doctrine\Common\Collections\Criteria;
use Wanawork\UserBundle\Entity\Billing\AdOrder;
use Wanawork\UserBundle\Entity\Billing\Order;
use Gedmo\Mapping\Annotation as Gedmo;
use Wanawork\UserBundle\Entity\Billing\OrderItem;

/**
 * Advertisiment Entity
 * @author Ilya Biryukov <ilya@goparty.ie>
 * @ORM\Entity(repositoryClass="Wanawork\UserBundle\Repository\AdRepository")
 * @ORM\Table(
 *   indexes={
 *     @ORM\Index(columns={"isPublished", "expiryDate"})
 *   }
 * )
 * 
 * @Assert\GroupSequence({"Ad", "step1"})
 */
class Ad 
{
    use \Gedmo\Timestampable\Traits\TimestampableEntity;
    
    const PLAN_STANDARD = 1;
    const PLAN_PREMIUM = 2;
    
    private static $validPricePlans = array(
    	'standard' => self::PLAN_STANDARD,
        'premium' => self::PLAN_PREMIUM,
    );
    
    /**
     * When to consider an ad as 'soon expiring'
     * @var integer
     */
    const NEAR_EXPIRY_IN_DAYS = 10;
    
	/**
	 * Ad ID
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * Ad Title
	 * @var string 
	 * @ORM\Column(type="string")
	 * @Assert\NotBlank(message="Please enter the headline", groups={"step1"}) 
	 * @Assert\Length(max=70, maxMessage="Headline is too long. Max {{ limit }} characters", groups={"step1"})
	 */
	protected $headline;

	/** 
	 * Ad pitch
	 * @var string 
	 * @ORM\Column(type="text")
	 * @Assert\NotBlank(message="Please enter the pitch", groups={"step1"})
	 * @Assert\Length(max=240, maxMessage="Pitch is too long. Max {{ limit }} characters", groups={"step1"})
	 */
	protected $pitch;

	/** 
	 * Position types
	 * @var Doctrine\Common\Collections\ArrayCollection
	 * @ORM\ManyToMany(targetEntity="Wanawork\UserBundle\Entity\PositionType")
	 * @Assert\Count(min=1, minMessage="Please enter at least one position", groups={"step1"})
	 */
	protected $positions;

	/** 
	 * A cv attached to this ad
	 * @var Wanawork\UserBundle\Entity\CV
	 * @ORM\ManyToOne(targetEntity="Wanawork\UserBundle\Entity\CV", inversedBy="ads")
	 * 
	 * @Assert\Type(type="\Wanawork\UserBundle\Entity\CV", message="Please select a CV", groups={"step1"})
	 * @Assert\Valid()
	 */
	protected $cv;

	/** 
	 * Locations, where the candidate is willing to work
	 * @var Doctrine\Common\Collections\ArrayCollection
	 * @ORM\ManyToMany(targetEntity="Wanawork\MainBundle\Entity\County")
	 * @ORM\OrderBy({"name"="ASC"})
	 * @Assert\NotNull(message="Please enter at least one location", groups={"step1"})
	 * @Assert\Count(min=1, minMessage="Please enter at least one location", groups={"step1"})
	 */
	protected $locations;

	/**
	 * When the ad expires
	 * @var \DateTime
	 * @ORM\Column(type="datetime", nullable=true)
	 * @Assert\DateTime(groups={"step1"})
	 */
	protected $expiryDate;

	/**
	 * Profession in which the user wishes to work
	 * @var \Wanawork\UserBundle\Entity\SectorJob
	 * @ORM\ManyToOne(targetEntity="Wanawork\UserBundle\Entity\SectorJob")
	 * @Assert\NotNull(message="Please pick your profession", groups={"step1"})
	 * @Assert\Type(type="\Wanawork\UserBundle\Entity\SectorJob", message="Please pick your profession", groups={"step1"})
	 * @Assert\Valid()
	 */
	protected $profession;
	
	/**
	 * List of industries where the candidate wants to work
	 * @var \Doctrine\Common\Collections\ArrayCollection
	 * @ORM\ManyToMany(targetEntity="Wanawork\UserBundle\Entity\Sector")
	 * @Assert\Valid()
	 */
	protected $industries;
	
	/**
	 * Experience that the candidate has in the above profession
	 * @var \Wanawork\UserBundle\Entity\SectorExperience
	 * @ORM\ManyToOne(targetEntity="Wanawork\UserBundle\Entity\SectorExperience")
	 * @Assert\NotNull(message="Please choose your experience", groups={"step1"})
	 * @Assert\Type(type="\Wanawork\UserBundle\Entity\SectorExperience", message="Please choose your experience", groups={"step1"})
	 * @Assert\Valid()
	 */
	protected $experience;

	/**
	 * List of requests by employers to access full CV
	 * @var \Doctrine\Common\Collections\ArrayCollection
	 * @ORM\OneToMany(targetEntity="CvRequest", mappedBy="ad", cascade={"all"}, indexBy="employerProfile_id")
	 * @Assert\Valid()
	 */
	protected $accessRequests;

	/**
	 * View records for this ad
	 * @var \Doctrine\Common\Collections\ArrayCollection 
	 * @ORM\OneToMany(targetEntity="AdView", mappedBy="ad", cascade={"all"}, fetch="EXTRA_LAZY")
	 * @ORM\OrderBy({"timestamp"="ASC"});
	 */
	protected $views;

	/**
	 * Badges that apply to this ad
	 * @var array
	 * @ORM\Column(type="string", nullable=true)
	 * @Assert\Length(max=100, maxMessage="Badge cannot be longer that {{ limit }} characters", groups={"Default", "step1"})
	 */
	protected $badge;
	
	/**
	 * Languages that the user speaks
	 * @var \Doctrine\Common\Collections\ArrayCollection
	 * @ORM\ManyToMany(targetEntity="Wanawork\MainBundle\Entity\Language")
	 * 
	 * @Assert\NotNull
	 * @Assert\Count(min=1, minMessage="Please specify at least {{ limit }} language", groups={"step1"})
	 */
	protected $languages;
	
	/**
	 * Level of education
	 * @var \Wanawork\UserBundle\Entity\Qualification
	 * @ORM\ManyToOne(targetEntity="Wanawork\UserBundle\Entity\Qualification")
	 * 
	 * @Assert\NotNull(message="Please choose your education level", groups={"step1"})
	 * @Assert\Valid()
	 */
	protected $educationLevel;
	
	/**
	 * Profile that posted this ad
	 * @var \Wanawork\UserBundle\Entity\EmployeeProfile
	 * @ORM\ManyToOne(targetEntity="Wanawork\UserBundle\Entity\EmployeeProfile", inversedBy="ads")
	 * @Assert\NotNull(message="A profile is required for the ad", groups={"step1"})
	 * @Assert\Valid()
	 */
	protected $profile;
	
	/**
	 * Is the ad published?
	 * @var boolean
	 * @ORM\Column(type="boolean")
	 */
	protected $isPublished = false;
	
	/**
	 * Percentage match
	 * For the purposes of the search
	 * @var integer
	 */
	protected $match;
	
	/**
	 * List of made orders for this ad
	 * @var \Doctrine\Common\Collections\ArrayCollection
	 * @ORM\OneToMany(targetEntity="Wanawork\UserBundle\Entity\Billing\AdOrder", mappedBy="ad", cascade={"persist"})
	 * @ORM\OrderBy({"createdAt"="ASC"})
	 */
	protected $orders;
	
	/**
	 * How many bumps are allowed on this ad
	 * @var integer
	 * @ORM\Column(type="smallint", nullable=true)
	 */
	private $bumpsAllowed;
	
	/**
	 * List of bump dates
	 * @var array
	 * @ORM\Column(type="array", nullable=true)
	 */
	private $bumps = array();
	
	/**
	 * Date of the last bump
	 * @var \DateTime
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	private $lastBump;
	
	/**
	 * Slug for url
	 * @Gedmo\Slug(fields={"headline"})
	 * @Doctrine\ORM\Mapping\Column(length=275, unique=true)
	 */
	private $slug;
	
	public function __construct() 
	{
	    $this->setCreatedAt(new \DateTime());
		$this->positions = new ArrayCollection();
		$this->locations = new ArrayCollection();
		$this->accessRequests = new ArrayCollection();
		$this->views = new ArrayCollection();
		$this->industries = new ArrayCollection();
		$this->languages = new ArrayCollection();
		$this->orders = new ArrayCollection();
	}

	/**
	 * @return the $id
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @return the $headline
	 */
	public function getHeadline()
	{
		return $this->headline;
	}

	/**
	 * @return the $pitch
	 */
	public function getPitch()
	{
		return $this->pitch;
	}

	/**
	 * @return \Doctrine\Common\Collections\ArrayCollection $positions
	 */
	public function getPositions()
	{
		return $this->positions;
	}

	/**
	 * @return \Wanawork\UserBundle\Entity\CV
	 */
	public function getCv()
	{
		return $this->cv;
	}
	
	public function hasCv(CV $cv)
	{
	    return $this->getCv() === $cv;
	}

	/**
	 * @return \Doctrine\Common\Collections\ArrayCollection $locations
	 */
	public function getLocations()
	{
		return $this->locations;
	}

	/**
	 * 
	 * @return \DateTime
	 */
	public function getPostDate()
	{
		return $this->getCreatedAt();
	}

	/**
	 * When the ad expires
	 * @return \DateTime
	 */
	public function getExpiryDate()
	{
		if ($this->expiryDate) {
		   return clone $this->expiryDate; 
		}
		return null;
	}
	
	/**
	 * Get the number of days left till expiry
	 * @return number
	 */
	public function getExpiryDateInDays()
	{
	    $days = 0;
	    if ($this->getExpiryDate() instanceof \DateTime) {
	        $now = new \DateTime();
	        $expiry = $this->getExpiryDate();
	        $days = (int)ceil(($expiry->getTimestamp() - $now->getTimestamp()) / 86400);
	    }
	    return $days;
	}
	
	/**
	 * Get the number of days the ad is running since last payment
	 * @return number
	 */
	public function getRunningSpanSinceLastPayment()
	{
	    $days = 0;
	    if($this->getExpiryDate() && sizeof($this->getOrders(Order::STATUS_PAID))) {
	        $days = floor(($this->getExpiryDate()->getTimestamp() - 
	            $this->getOrders(Order::STATUS_PAID)->last()->getCreatedAt()->getTimestamp()) / 86400);
	    }
	    return $days;
	}

	/**
	 * Check if the ad expired
	 * @return boolean
	 */
	public function isExpired()
	{
		return $this->getExpiryDate()->getTimestamp() < time();
	}

	/**
	 * @param string $headline
	 */
	public function setHeadline($headline)
	{
		$this->headline = $headline;
	}

	/**
	 * @param string $pitch
	 */
	public function setPitch($pitch)
	{
		$this->pitch = $pitch;
	}

	/**
	 * @param array $positions
	 */
	public function setPositions(array $positions)
	{
		foreach ($this->getPositions() as $position) {
		    $this->removePosition($position);
		}
		
		foreach($positions as $position) {
		    $this->addPosition($position);
		}
	}

	/**
	 * Add new position to the ad
	 * @param PositionType $position
	 */
	public function addPosition(PositionType $position)
	{
	    if(!$this->getPositions()->contains($position)) {
	        $this->getPositions()->add($position);
	    }
	}
	
	/**
	 * Remove the given position
	 * @param PositionType $position
	 */
    public function removePosition(PositionType $position)
    {
        $this->getPositions()->removeElement($position);
    }
    
	/**
	 * @param \Wanawork\UserBundle\Entity\CV $cv
	 */
	public function setCv(CV $cv = null) {
		$this->cv = $cv;
	}

	/**
	 * @param array $locations
	 */
	public function setLocations(array $locations) 
	{
        foreach ($this->getLocations() as $location) {
            $this->removeLocation($location);
        }
        
        foreach ($locations as $location) {
            $this->addLocation($location);
        }
	}

	public function addLocation(County $location)
	{
	   if (!$this->getLocations()->contains($location)) {
	      $this->getLocations()->add($location); 
	   }
	}
	
	public function removeLocation(County $location)
	{
	    $this->getLocations()->removeElement($location);
	}
	
	/**
	 * @param \DateTime $expiryDate
	 */
	public function setExpiryDate($expiryDate) {
		$this->expiryDate = $expiryDate;
	}

	/**
	 *
	 * @return \Doctrine\Common\Collections\ArrayCollection
	 */
	public function getAccessRequests()
	{
		return $this->accessRequests;
	}

	public function addAccessRequest(CvRequest $cvRequest)
	{
// 	    if (!$cvRequest->getEmployerProfile() || !$cvRequest->getEmployerProfile()->getId()) {
// 	        throw new \Exception('CV request must contain an employer profile');
// 	    }
	    
	    if (!$this->getAccessRequests()->contains($cvRequest)) {
	        $this->getAccessRequests()->add($cvRequest);
	        $cvRequest->setAd($this);
	    }
	}
	
	public function removeAccessRequest(CvRequest $cvRequest)
	{
	    if ($this->getAccessRequests()->contains($cvRequest)) {
	        $this->getAccessRequests()->removeElement($cvRequest);
	    }
	}
	
	public function hasCvRequestFromEmployer($profile)
	{
	    return $this->getAccessRequests()->exists(function($i, CvRequest $request) use($profile){
	    	return $request->getEmployerProfile() === $profile;
	    });
	}
	
	/**
	 * Get a list of cv requests submitted today
	 * @param string $status
	 * @throws \Exception
	 * @return \Doctrine\Common\Collections\ArrayCollection
	 */
	public function getTodayAccessRequests($status = null)
	{
	    if($status !== null && !in_array($status, CvRequest::$statuses)) {
            $msg = "Invalid status provided: '$status'. Valid values: " . implode(', ', CvRequest::$statuses);
	        throw new \Exception($msg);
	    }
	    
	    $dateStart = new \DateTime();
	    $dateStart->setTime($hour = 0, $minute = 0, $second = 0);
	     
	    $dateEnd = new \DateTime();
	    $dateEnd->setTimestamp($dateStart->getTimestamp() + 86399);
	    
	    return 
	    $this->getAccessRequests()->filter(function(CvRequest $request) use($status, $dateStart, $dateEnd){
	    	return $dateStart <= $request->getCreateDate() &&
	    	  $request->getCreateDate() <= $dateEnd && ($status === null || $status === $request->getStatus());
	    });
	}
	
	public function getTodayPendingAccessRequests()
	{
	    return $this->getTodayAccessRequests(CvRequest::AWAITING);
	}

	/**
	 * @return \Doctrine\Common\Collections\ArrayCollection
	 */
	public function getViews()
	{
		return $this->views;
	}

	/**
	 * Get all views of particular type
	 * @param integer $type
	 * @return \Doctrine\Common\Collections\ArrayCollection
	 */
	public function getViewsByType($type)
	{
		return $this->getViews()->filter(function (AdView $view) use ($type) {
		    return $view->getType() === $type;
	    });
	}

	/**
	 * Get the array of months the ad's been running
	 * @return array (months number => month name)
	 */
	public function getRunMonths()
	{
	    $months = array();
	    // @todo fix this to use the first payment date
	    if($this->getPostDate() && $this->getExpiryDate()) {
	        $startDate = $this->getPostDate();
	        $finishDate = new \DateTime();
	        if ($finishDate > $this->getExpiryDate()) {
	            $finishDate = $this->getExpiryDate();
	        }
	        
	        while ($startDate <= $finishDate) {
                $months[$startDate->format('n-Y')] = $startDate->format('F');
                $currentMonth = $startDate->format('n');
                while ($currentMonth === $startDate->format('n')) {
                    $startDate->modify('+1 day');
                }	            
	        }
	        
	    }
		return $months;
	}

	public function getDailyViews($month, $year, $type = null)
	{
	    if (!preg_match('/^([1-9]|1[0-2])$/', $month)) {
	        throw new \Exception("Invalid month provided: '$month'");
	    }
	    
	    if (!preg_match('/^((19\d{2})|(20\d{2}))$/', $year)) {
	        throw new \Exception("Invalid year provided: '$year'");
	    }
	    
	    $views = 
        $this->getViews()->filter(function(AdView $adView) use($type, $month, $year){
        	return ($month === null || $month === $adView->getTimestamp()->format('n')) && 
        	       ($year === null || $year === $adView->getTimestamp()->format('Y')) &&
        	       ($type === null || $type === $adView->getType());
        });	
        
        $now = new \DateTime();
        $now->setTime(0, 0, 0);
        
        $dailyViews = array();
        $days = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        for($i = 1; $i <= $days; $i++) {
            if ($now >= new \DateTime("{$year}-{$month}-" . sprintf("%02d", $i))) {
                $date = sprintf("%02d", $i) . "-$month-$year";
                $dailyViews[$date] = array();
            }
        }
        
        foreach ($views as $view) {
            $date = $view->getTimestamp()->format('d-m-Y');
            $dailyViews[$date][] = $view;
        }
        
        return $dailyViews;
	}
	
	/**
	 * Get the list of views that happened today
	 * @param string $type integer | null
	 * 
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getTodayViews($type = null)
	{
	    $views = $this->getViews();
	    
	    $dateStart = new \DateTime();
	    $dateStart->setTime($hour = 0, $minute = 0, $second = 0);
	    
	    $dateEnd = new \DateTime();
	    $dateEnd->setTimestamp($dateStart->getTimestamp() + 86399);
	    
	    $criteria = Criteria::create();
	    $criteria->where(Criteria::expr()->gte('timestamp', $dateStart));
	    $criteria->andWhere(Criteria::expr()->lte('timestamp', $dateEnd));
	    
	    if($type !== null) {
	        if (!in_array($type, AdView::$viewTypes)) {
	            $msg = "Invalid type provided: '$type'. Valid Types: " . implode(', ', array_values(AdView::$viewTypes));
	            throw new \Exception($msg);
	        }
	        
	        $criteria->andWhere(Criteria::expr()->eq('type', $type));
	    }
	    
	    return $views->matching($criteria);
	}
	
	public function getTodayViewsPDF()
	{
	    return $this->getTodayViews(AdView::VIEW_PDF);
	}

	/**
	 * Get all pdf views
	 * @return \Doctrine\Common\Collections\ArrayCollection
	 */
	public function getPdfViews()
	{
		return $this->getViewsByType(AdView::VIEW_PDF);
	}

	public function getViewsGroupedByDay($type = null)
	{
		$groupedViews = array();
		$views = $this->getViews();

		if ($type !== null) {
			$views = array_filter($views,
					function (AdView $view) use ($type) {
						return $view->getType() === $type;
					});
		}

		foreach ($views as $view) {
			$date = $view->getTimestamp()->format('Y-m-d');
			if (!isset($groupedViews[$date])) {
				$groupedViews[$date] = 0;
			}
			++$groupedViews[$date];
		}
		return $groupedViews;
	}

	/**
	 * Record a view
	 * @param User $user
	 * @param integer $type
	 * @return \Wanawork\UserBundle\Entity\AdView | null
	 */
	public function addView($type, User $user = null, EmployerProfile $profile = null, $position = null)
	{
	    $typesRequireUser = array(
	    	AdView::VIEW_FULL,
	        AdView::VIEW_PDF,
	    );
	    
	    if ($user === null && in_array($type, $typesRequireUser, true)) {
	       throw new \InvalidArgumentException("User cannot be null for this type of view");
	    }    
	    
	    $view = null;
	    
	    if ($user === null || !$user->hasRoleName(Role::ADMIN)) {
	        $view = new AdView($this, $type, $user, $profile, $position);
	        $this->getViews()->add($view);
	    }
		
		return $view;
	}
	
	/**
	 * @return string
	 */
	public function getBadge()
	{
	    return $this->badge;
	}
	
	public function setBadge($badge)
	{
	    $this->badge = $badge;
	}

	/**
	 * @return \Wanawork\UserBundle\Entity\SectorJob
	 */
	public function getProfession() {
		return $this->profession;
	}
	
	/**
	 * @param $profession
	 */
	public function setProfession(SectorJob $profession = null) {
		$this->profession = $profession;
	}
	
	/**
	 * @return \Doctrine\Common\Collections\ArrayCollection
	 */
	public function getIndustries() {
		return $this->industries;
	}
	
	/**
	 * @param  array $industries
	 */
	public function setIndustries(array $industries) {
		foreach ($this->getIndustries() as $industry) {
		    $this->removeIndustry($industry);
		}
		
		foreach ($industries as $industry) {
		    $this->addIndustry($industry);
		}
	}
	
	public function addIndustry(Sector $industry)
	{
	    if(!$this->getIndustries()->contains($industry)) {
	        $this->getIndustries()->add($industry);
	    }
	}
	
	public function removeIndustry(Sector $industry)
	{
	    $this->getIndustries()->removeElement($industry);
	}
	
	/**
	 * @return \Wanawork\UserBundle\Entity\SectorExperience
	 */
	public function getExperience() {
		return $this->experience;
	}
	
	/**
	 * @param $experience
	 */
	public function setExperience(SectorExperience $experience = null)
	{
		$this->experience = $experience;
	}

	/**
	 * @return \Wanawork\UserBundle\Entity\Qualification
	 */
	public function getEducationLevel() 
	{
		return $this->educationLevel;
	}
	
	public function setEducationLevel(Qualification $educationLevel = null)
	{
		$this->educationLevel = $educationLevel;
	}

	/**
	 * 
	 * @return \Wanawork\UserBundle\Entity\EmployeeProfile
	 */
	public function getProfile()
	{
		return $this->profile;
	}
	
	public function setProfile(EmployeeProfile $profile = null) 
	{
		$this->profile = $profile;
		
		if ($profile !== null) {
		  $profile->addAd($this);
		}
	}

	/**
	 * 
	 * @return \Doctrine\Common\Collections\ArrayCollection
	 */
	public function getLanguages()
	{
		return $this->languages;
	}
	
	public function addLanguage(Language $language)
	{
	    if (!$this->getLanguages()->contains($language)) {
	        $this->getLanguages()->add($language);
	    }
	}

	public function removeLanguage(Language $language)
	{
	    $this->getLanguages()->removeElement($language);
	}
	
	public function setLanguages(array $languages)
	{
	    foreach ($this->getLanguages() as $language) {
	        $this->removeLanguage($language);
	    }
	    
	    foreach ($languages as $language) {
	        $this->addLanguage($language);
	    }
	}
	
	/**
	 * @return boolean
	 */
	public function isPublished()
	{
	    return $this->isPublished;
	}
	
	public function markPublished($flag)
	{
	    $this->isPublished = $flag;
	}

    /**
     * Get a list of valid price plans
     * @return array
     */
    public static function getPricePlans()
    {
        return self::$validPricePlans;
    }
    
    public function canDisplayBadge()
    {
        return $this->isPremium();   
    }
    
    public function isPremium()
    {
        $isPremium = false;
        $paidPremiumOrders = 
        $this->getOrders()->filter(function(AdOrder $order){
        	return $order->getStatus() === Order::STATUS_PAID && 
        	       $order->getPlan() === Ad::PLAN_PREMIUM;
        });
        
        $now = new \DateTime();
        foreach ($paidPremiumOrders as $order) {
            $runFrom = $order->getRunFrom();
            $expiry = $order->getExpiry();
            
            if($runFrom && $expiry && $now >= $runFrom && $now <= $expiry) {
                $isPremium = true;
                break;
            }
        }
        
        return $isPremium;
    }

    /**
     *
     * @return the integer
     */
    public function getMatch()
    {
        return $this->match;
    }

    /**
     *
     * @param $match
     */
    public function setMatch($match)
    {
        $this->match = $match;
    }
    
    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getOrders($status = null)
    {
        $orders = $this->orders;
        if ($status !== null) {
            $orders = $orders->filter(function(Order $order) use($status){
            	return $order->getStatus() === $status;
            });
        }
        return $orders;
    }
    
    public function addOrder(AdOrder $order)
    {
        if (!$this->getOrders()->contains($order)) {
            $this->getOrders()->add($order);
            $order->setAd($this);
        }
    }
    
    public function getTotalPayAmount()
    {
        $amount = 0;
        $orders = $this->getOrders(Order::STATUS_PAID);
        foreach ($orders as $order) {
            $amount += $order->getAmountIncludingVat();
        }
        
        return $amount;
    }
    
    public function getBumps()
    {
        return is_array($this->bumps) ? $this->bumps : array();
    }
    
    /**
     * When the ad was bumped last time
     * @return DateTime|NULL
     */
    public function getLastBumpDate()
    {
        $bumps = $this->getBumps();
        $date = null;
        if (sizeof($bumps)) {
            $date = $bumps[sizeof($bumps) - 1];
        }
        return $date;
    }
    
    public function setCountBumpsAlowed($count)
    {
        $this->bumpsAllowed = $count;
    }
    
    /**
     * How many bumps allowed
     * @return number
     */
    public function getCountBumpsAllowed()
    {
        return is_int($this->bumpsAllowed) ? $this->bumpsAllowed : 0;
    }
    
    /**
     *
     * @return number
     */
    public function getCountBumpsUsed()
    {
        return sizeof($this->getBumps());
    }
    
    /**
     * How many bumps left
     * @return number
     */
    public function getCountBumpsLeft()
    {
        return $this->getCountBumpsAllowed() - $this->getCountBumpsUsed();
    }
    
    /**
     * Can we do a bump?
     * @return boolean
     */
    public function canBump()
    {
        return $this->getCountBumpsLeft() > 0;
    }
    
    /**
     * Do a bump of the ad
     * @return \DateTime | null - when the bump was done 
     */
    public function bump()
    {
        if (!is_array($this->bumps)) {
            $this->bumps = array();
        }
        
        $date = null;
        if ($this->canBump()) {
            $this->lastBump = $this->bumps[] = $date = new \DateTime();
        }
        return $date;
    }
    
    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }
    
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }
    
    /**
     * Publish the ad using a free plan
     * @param integer $duration
     * @param integer $bumps
     * @throws \InvalidArgumentException
     * @return Wanawork\UserBundle\Billing\AdOrder
     */
    public function publishStandard($duration, $bumps, $runFrom = null)
    {
        if (!preg_match('/^\d+$/', $duration)) {
            throw new \InvalidArgumentException(
            	sprintf("Duration must be a number. Given: '%s'", $duration)
            );
        }
        
        if ($duration <= 0) {
            throw new \InvalidArgumentException(
            	sprintf("Duration must be greater than 0. Given: '%d'", $duration)
            );
        }
        
        if (!preg_match('/^\d+$/', $bumps)) {
            throw new \InvalidArgumentException(
                sprintf("Bumps must be a number. Given: '%s'", $bumps)
            );
        }
        
        $ad = $this;
        $amount = 0;
        $vatRate = 0;
        $plan = self::PLAN_STANDARD;
        $isExtension = $this->isPublished();
        
        $order = new AdOrder($ad, $amount, $vatRate, $plan, $duration, $bumps, $isExtension);
        $order->setStatus(AdOrder::STATUS_PAID);
        
        if ($runFrom === null) {
            $runFrom = new \DateTime();
            // expiry date in the future? Run from that date
            if ($this->getExpiryDate() instanceof \DateTime && $this->getExpiryDate() > new \DateTime()) {
                $runFrom = $this->getExpiryDate();
            }
        }
        
        $order->setRunFrom($runFrom);
        $ad->setExpiryDate($order->getExpiry());
        $countAllowedBumps = $this->getCountBumpsAllowed() + $order->getBumps();
        $ad->setCountBumpsAlowed($countAllowedBumps);
        
        $planName = ucfirst(array_search(self::PLAN_STANDARD, self::$validPricePlans));
        $orderItem = new OrderItem(
            $name = "$planName subscription for $duration days",
            $price = $amount,
            $quantity = 1,
            $order
        );
        $order->addItem($orderItem);
        
        if ($bumps > 0) {
            $name = ("Free bump") . ($bumps == 1 ? '' : 's') . (' included') ;
            $orderItem = new OrderItem(
                $name,
                $price = 0,
                $quantity = $bumps,
                $order
            );
            $order->addItem($orderItem);
        }
        
        $this->markPublished(true);
        return $order;
    }
    
    public function publishPremium(AdOrder $order)
    {
        if ($order->getStatus() !== Order::STATUS_PAID) {
            throw new \Exception("Cannot publish an advert with unpaid order");
        }
        
        if ($order->getPlan() !== self::PLAN_PREMIUM) {
            throw new \Exception("Order must be for a premium plan");
        }
        
        $this->markPublished(true);
        $runFrom = new \DateTime();
        if ($this->getExpiryDate() instanceof \DateTime && $this->getExpiryDate() > new \DateTime() && $this->isPremium()) {
            $runFrom = $this->getExpiryDate();
        }
        $order->setRunFrom($runFrom);
        $this->setExpiryDate($order->getExpiry());
        
        $countAllowedBumps = $this->getCountBumpsAllowed() + $order->getBumps();
        $this->setCountBumpsAlowed($countAllowedBumps);
    }
    
    public function __clone()
    {
        $this->orders = new ArrayCollection();        
    }
    
    /**
     * Is the ad nearing expiry?
     * @return boolean
     */
    public function isNearExpiry()
    {
        return $this->isPublished() && $this->getExpiryDateInDays() <= self::NEAR_EXPIRY_IN_DAYS;
    }
    
    
}
