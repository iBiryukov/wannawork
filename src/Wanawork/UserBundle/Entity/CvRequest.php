<?php
namespace Wanawork\UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Wanawork\UserBundle\Exception\UserAndEmployerProfileMisMatchException;
use Symfony\Component\Validator\ExecutionContextInterface;

/**
 * CV Request 
 * @author Ilya Biryukov <ilya@wanawork.ie>
 * @ORM\Entity(repositoryClass="Wanawork\UserBundle\Repository\CvRequestRepository")
 * @ORM\Table(
 *  uniqueConstraints={
 *      @ORM\UniqueConstraint(name="employer_ad", columns={"employerprofile_id", "ad_id"})
 *  }
 * )
 * @Assert\Callback(methods={"validate"})
 */
class CvRequest
{

    const AWAITING = 1;

    const GRANTED = 2;

    const DENIED = 3;
    
    static $statuses = array(
        'pending' => self::AWAITING,
        'approved' => self::GRANTED,
        'denied' => self::DENIED,
    );

    /**
     * Request ID
     * 
     * @var integer 
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * The user who requested access
     * 
     * @var \Wanawork\UserBundle\Entity\User 
     * 
     * @ORM\ManyToOne(targetEntity="User")
     * @Assert\NotNull()
     * @Assert\Type(type="\Wanawork\UserBundle\Entity\User", message="CV Request requires a valid user")
     */
    protected $user;

    /**
     * The employer profile which made the request
     * 
     * @var \Wanawork\UserBundle\Entity\EmployerProfile 
     * @ORM\ManyToOne(targetEntity="Wanawork\UserBundle\Entity\EmployerProfile", inversedBy="cvRequests")
     * @Assert\NotNull()
     * @Assert\Type(type="\Wanawork\UserBundle\Entity\EmployerProfile", message="CV Request requires a valid employer profile")
     */
    protected $employerProfile;

    /**
     * Ad to which request was submitted
     * 
     * @var Ad 
     * @ORM\ManyToOne(targetEntity="Ad", inversedBy="accessRequests")
     * @Assert\NotNull()
     * @Assert\Type(type="\Wanawork\UserBundle\Entity\Ad", message="CV Request requires a valid ad")
     */
    protected $ad;

    /**
     * Request status
     * 
     * @var integer 
     * @ORM\Column(type="integer")
     * @Assert\NotNull
     * @Assert\Choice(callback = "getStatuses")
     * 
     */
    protected $status;

    /**
     * When the request was submitted
     * 
     * @var \DateTime 
     * @ORM\Column(type="datetime")
     * 
     * @Assert\NotNull()
     * @Assert\Type(type="\DateTime", message="Creating date must be a valid date/time")
     */
    protected $createDate;

    /**
     * The date when a response was given
     * 
     * @var \DateTime 
     * @ORM\Column(type="datetime", nullable=true)
     * @Assert\Type(type="\DateTime", message="Response date must be a valid date/time")
     */
    protected $responseDate;

    /**
     * The mail communication for this request
     * 
     * @var Wanawork\UserBundle\Entity\MailThread
     * @ORM\OneToOne(targetEntity="Wanawork\UserBundle\Entity\MailThread", mappedBy="cvRequest", cascade={"all"})
     */
    protected $mailThread;
    
    /**
     * Job for which this request is
     * @var Wanawork\UserBundle\Entity\JobSpec
     * @ORM\ManyToOne(targetEntity="Wanawork\UserBundle\Entity\JobSpec")
     * @Assert\Valid()
     */
    protected $job;
    
    /**
     * Message to use
     * @var string
     * @ORM\Column(type="text", nullable=true)
     * @Assert\Length(max=2000, maxMessage="Your message cannot be longer that {{ limit }} characters")
     */
    protected $message;

    /**
     * 
     * @param User $user
     * @param Ad $ad
     * @param EmployerProfile $profile
     * @throws UserAndEmployerProfileMisMatchException
     */
    public function __construct(User $user = null, Ad $ad = null, EmployerProfile $profile = null, JobSpec $job = null, $message = null)
    {
        $this->employerProfile = $profile;
        $this->job = $job;
        $this->setUser($user);
        $this->setAd($ad);
        $this->createDate = new \DateTime();
        $this->responseDate = null;
        $this->status = self::AWAITING;
    }

    /**
     *
     * @return \Wanawork\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     *
     * @return \Wanawork\UserBundle\Entity\Ad
     */
    public function getAd()
    {
        return $this->ad;
    }

    public function setAd($ad)
    {
        $this->ad = $ad;
        
        if ($ad instanceof Ad) {
            $ad->addAccessRequest($this);
        }
    }

    /**
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * 
     * @return \DateTime
     */
    public function getResponseDate()
    {
        return $this->responseDate;
    }
    
    /**
     * Get the list of valid statuses
     * 
     * @return array
     */
    public function getStatuses()
    {
        return self::$statuses;
    }

    /**
     * @return number
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getStatusName()
    {
        return array_search($this->getStatus(), self::$statuses);;
    }

    /**
     * Check if the access is granted
     * 
     * @return boolean
     */
    public function isGranted()
    {
        return $this->getStatus() === self::GRANTED;
    }

    public function isPending()
    {
        return $this->getStatus() === self::AWAITING;
    }

    public function isDenied()
    {
        return $this->getStatus() === self::DENIED;
    }

    public function setStatus($status)
    {
        if (! in_array($status, self::$statuses)) {
            throw new \InvalidArgumentException("Invalid Status Code Given: '$status'");
        }
        
        if ($status === self::GRANTED) {
            $this->createMailThread();
        }
        
        $this->status = $status;
        $this->responseDate = new \DateTime();
    }

    public function approve()
    {
        $this->setStatus(self::GRANTED);
    }

    public function deny()
    {
        $this->setStatus(self::DENIED);
    }

    /**
     *
     * @return \Wanawork\UserBundle\Entity\MailThread
     */
    public function getMailThread()
    {
        return $this->mailThread;
    }

    /**
     * @return \Wanawork\UserBundle\Entity\MailThread
     */
    public function createMailThread()
    {
        $thread = $this->getMailThread();
        
        if(!$thread) {
            $thread = $this->mailThread = new MailThread($this);
        }
        
        return $thread;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setUser($user)
    {
        $this->user = $user;
    }

    public function getJob()
    {
        return $this->job;
    }

    public function setJob($job)
    {
        $this->job = $job;
    }

    public function getEmployerProfile()
    {
        return $this->employerProfile;
    }

    public function setEmployerProfile($employerProfile)
    {
        $this->employerProfile = $employerProfile;
    }
    
    public function validate(ExecutionContextInterface $context)
    {
        if (!strlen($this->getMessage()) && !$this->getJob()) {
            $context->addViolation('Please provide either a message or select a job description');
        }
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function setMessage($message)
    {
        $this->message = $message;
    }
    
    
}
