<?php
namespace Wanawork\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entity records "open" events when a user opens our email
 * @author Ilya Biryukov <ilya@wannawork.ie>
 * 
 * @ORM\Entity
 */
class SystemEmailTracker
{
    /**
     * ID
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * Email to track
     * @var \Wanawork\UserBundle\Entity\SystemEmail
     * @ORM\ManyToOne(targetEntity="SystemEmail", inversedBy="trackers")
     */
    private $systemEmail;
    
    /**
     * When the email was opened
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * The ip from which the email was opened
     * @var string
     * @ORM\Column(type="string")
     */
    private $ip;
    
    /**
     * 
     * @param SystemEmail    $systemEmail
     * @param string         $ip
     */
    public function __construct(SystemEmail $systemEmail, $ip)
    {
        $this->systemEmail = $systemEmail;
        $this->ip = $ip;
        $this->date = new \DateTime();
    }

    /**
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     *
     * @return \Wanawork\UserBundle\Entity\SystemEmail
     */
    public function getSystemEmail()
    {
        return $this->systemEmail;
    }

    /**
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     *
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }
}