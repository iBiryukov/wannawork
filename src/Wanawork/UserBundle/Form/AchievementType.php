<?php

namespace Wanawork\UserBundle\Form;

use Wanawork\UserBundle\Form\DataTransformers\CvIdToEntityTransformer;

use Wanawork\UserBundle\Entity\CV;

use Doctrine\Common\Persistence\ObjectManager;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AchievementType extends AbstractType
{
	
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('description');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Wanawork\UserBundle\Entity\Achievement'
        ));
    }

    public function getName()
    {
        return 'wanawork_userbundle_achievementtype';
    }
}
