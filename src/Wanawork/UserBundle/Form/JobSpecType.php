<?php

namespace Wanawork\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Wanawork\UserBundle\Repository\SectorRepository;
use Wanawork\UserBundle\Repository\SectorJobRepository;

class JobSpecType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, array(
            ))
            ->add('profession', null, array(
            	'class' => 'WanaworkUserBundle:SectorJob',
				'query_builder' => function(SectorJobRepository $er) {
					$qb = $er->createQueryBuilder('profession');
					$qb->addOrderBy('profession.name', 'ASC');
					return $qb;
				},
				'attr' => array(
				    'data-placeholder' => 'Profession'
				),
				'empty_value' => ''
            ))
            ->add('description')
            ->add('public')
            ->add('industries', 'entity', array(
				'class' => 'WanaworkUserBundle:Sector',
				'property' => 'name',
				'query_builder' => function(SectorRepository $rep) {
					return $rep->createQueryBuilder('industry')->orderBy('industry.name');
				},
				'multiple' => true,
				'empty_value' => 'Industries',
				'attr' => array(
				    'data-placeholder' => 'Industries'
				),
				'required' => false,
		  ))
            ->add('positions', null, array(
                'empty_value' => 'Positions',
                'attr' => array(
                    'data-placeholder' => 'Positions'
                ),
            ))
            ->add('languages', null, array(
                'empty_value' => 'Languages',
                'attr' => array(
                    'data-placeholder' => 'Languages'
                ),
            ))
            ->add('locations', null, array(
                'empty_value' => 'Locations',
                'attr' => array(
                    'data-placeholder' => 'Locations'
                ),
            ))
            ->add('experience', null, array(
                'empty_value' => 'Experience',
                'attr' => array(
                    'data-placeholder' => 'Experience'
                ),
            ))
            ->add('educationLevel', null, array(
                'empty_value' => 'Education Level',
                'attr' => array(
                    'data-placeholder' => 'Education Level'
                ),
            ))
            ->add('salary')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Wanawork\UserBundle\Entity\JobSpec'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'wanawork_userbundle_jobspec';
    }
}
