<?php
namespace Wanawork\UserBundle\Form;

use Doctrine\ORM\EntityRepository;

use Wanawork\UserBundle\Form\DataTransformers\MonthYearToFullDateTransformer;

use Wanawork\UserBundle\Entity\Education;

use Wanawork\UserBundle\Form\DataTransformers\CvIdToEntityTransformer;

use Doctrine\Common\Persistence\ObjectManager;

use Wanawork\UserBundle\Form\DataTransformers\StringToDateTransformer;

use Wanawork\UserBundle\Entity\CV;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EducationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
    	$dateTransformer = new MonthYearToFullDateTransformer();
        
        $builder
            ->add(
            	$builder->create('start', 'text', array(
            	    'label' => 'Start Date', 
            	    'attr' => array(
            	        'class' => 'education-date-input education-start-date masked-date',
            		    'placeholder' => 'mm/yyyy',
            		    'data-end' => date('Y'),
            		    'data-start' =>  date('Y') - 60
            	    )
            	))
                ->addModelTransformer($dateTransformer)
            )        
            ->add(
            	$builder->create('finish', 'text', array(
            	    'label' => 'Finish Date', 
            	    'attr' => array(
            	        'class' => 'education-date-input education-finish-date masked-date',
            		    'data-end' => date('Y'),
            		    'data-start' =>  date('Y') - 60,
            		    'placeholder' => 'mm/yyyy'
            	    ),
            	))	
            		->addModelTransformer($dateTransformer)
            )
            ->add('college', 'text', array(
                'label' => 'Institute', 
                'attr' => array(
            	   'placeholder' => 'Name of the school/college you attended',
                   'class' => 'education-college'
                )		
             ))
            ->add('course', null, array(
            	'attr' => array(
            	    'class' => 'education-course',
            	    'placeholder' => 'Name of the course you did'
                ),
            ))
            ->add('award', null, array(
                'label' => 'Received Degree/Qualification', 
                'attr' => array(
                    'class' => 'education-award',
                    'placeholder' => 'Degree/Certificate/Leaving Cert/etc'
                ),	
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Wanawork\UserBundle\Entity\Education'
        ));
    }

    public function getName()
    {
        return 'wanawork_userbundle_educationtype';
    }
}
