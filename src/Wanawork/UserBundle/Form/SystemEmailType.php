<?php
namespace Wanawork\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Wanawork\UserBundle\Entity\SystemEmail;

class SystemEmailType extends AbstractType
{
    private $mailers;
    
    public function __construct(array $mailers)
    {
        $this->mailers = $mailers;        
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $types = array();
        foreach (SystemEmail::$templates as $type => $template) {
            $types[$type] = ucfirst(SystemEmail::$validTypes[$type]);
        }
            
        
        $builder
           ->add('id', 'hidden')
           ->add('subject')
           ->add('email', null, array(
        	   'label' => "To (email)"
           ))
           ->add('type', 'choice', array(
               'choices' => $types,
           ))
           ->add('mail_account', 'choice', array(
               'mapped' => false,
               'choices' => array_flip($this->mailers), 
               'label' => 'Send From Account'
           ))
           ->add('text')
           ->add("Send", "submit", array(
        	  'attr' => array(
           	      'class' => 'btn btn-primary',
              )
           ))
           ;
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Wanawork\UserBundle\Entity\SystemEmail'
        ));
    }
    
    public function getName()
    {
        return 'wanawork_userbundle_systememail';
    }
}