<?php

namespace Wanawork\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Wanawork\UserBundle\Repository\SectorJobRepository;

class SectorType extends AbstractType
{
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id')
            ->add('name')
            ->add('jobs', null, array(
        	    'attr' => array(
            	    'class' => 'chosen'
                ),
                'by_reference' => false,
                //'class' => 'WanaworkUserBundle:SectorJob',
                'query_builder' => function(SectorJobRepository $er) {
                    $qb = $er->createQueryBuilder('profession');
                    $qb->addOrderBy('profession.name', 'ASC');
                    return $qb;
                },
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Wanawork\UserBundle\Entity\Sector'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'wanawork_userbundle_sector';
    }
}
