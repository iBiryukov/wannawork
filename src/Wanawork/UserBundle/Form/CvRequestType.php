<?php

namespace Wanawork\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Wanawork\UserBundle\Entity\EmployerProfile;
use Doctrine\ORM\EntityRepository;

class CvRequestType extends AbstractType
{
    private $profile;
    
    public function __construct(EmployerProfile $profile)
    {
        $this->profile = $profile;        
    }
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $profile = $this->profile;
        
        $builder
            ->add('message')
            ->add('job', null, array(
        	    'property' => 'title',
                'query_builder' => function(EntityRepository $er) use($profile){
            	    $qb = $er->createQueryBuilder('j');
            	    $qb->where('j.employer=:employer');
            	    $qb->setParameter('employer', $profile);
            	    return $qb;
                }
            ));
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Wanawork\UserBundle\Entity\CvRequest'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'wanawork_userbundle_cvrequest';
    }
}
