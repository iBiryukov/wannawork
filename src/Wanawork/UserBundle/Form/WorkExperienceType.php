<?php
namespace Wanawork\UserBundle\Form;

use Wanawork\UserBundle\Form\DataTransformers\MonthYearToFullDateTransformer;

use Wanawork\UserBundle\Form\DataTransformers\StringToDateTransformer;

use Wanawork\UserBundle\Form\DataTransformers\CvIdToEntityTransformer;

use Doctrine\Common\Persistence\ObjectManager;

use Wanawork\UserBundle\Entity\CV;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class WorkExperienceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
    	$dateOptions = array(
    		'attr' => array(
    			'placeholder' => 'mm/yyyy',
    			'data-end' => date('Y'),
    			'data-start' =>  date('Y') - 60
    		)
    	);
    	$dateTransformer = new MonthYearToFullDateTransformer();
    	
        $builder
        	->add('company', null, array(
        		'label' => 'Employer',
        	    'attr' => array(
        		    'placeholder' => 'The company you worked for',
        	        'class' => 'work-experience-company'
        	    ),
        	))
        	->add('position', null, array(
        		'attr' => array(
        		    'placeholder' => 'Position you held at the company',
        		    'class' => 'work-experience-position',
        	    ),
        	)) 
            ->add(
            	$builder->create('start', 'text', array(
        	       'attr' => array(
        	           'placeholder' => 'mm/yyyy',
            		   'class' => 'work-experience-date-input work-experience-start masked-date',  
            	   ),
                ))->addModelTransformer($dateTransformer)
           	)
            ->add(
                $builder->create('finish', 'text', array(
        	        'attr' => array(
        	            'placeholder' => 'mm/yyyy',
        	            'class' => 'work-experience-date-input work-experience-finish masked-date',
                    )
                ))->addModelTransformer($dateTransformer)		
           	)
            //->add('currently_here', 'checkbox', array('label' => 'Still here'))
            ->add('description', null, array(
        		'attr' => array(
            	   'placeholder' => 'Outline your duties & achievements',
        		    'class' => 'work-experience-description',
                )
        	));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Wanawork\UserBundle\Entity\WorkExperience'
        ));
    }

    public function getName()
    {
        return 'wanawork_userbundle_workexperiencetype';
    }
}
