<?php
namespace Wanawork\UserBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
class EmployerWithGeneratedPasswordType extends EmployerType
{
    public function buildForm(FormBuilderInterface $builder, array $options) 
    {
        parent::buildForm($builder, $options);
        $builder->remove('password');
        $builder->add('password');
    }
}