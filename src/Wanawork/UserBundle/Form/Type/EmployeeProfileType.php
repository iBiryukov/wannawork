<?php
namespace Wanawork\UserBundle\Form\Type;

use Wanawork\UserBundle\Form\DataTransformers\UserIdToEntityTransformer;
use Doctrine\Common\Persistence\ObjectManager;
use Wanawork\UserBundle\Entity\User;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;

class EmployeeProfileType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title');
        $builder->add('name', null, array(
        	'attr' => array(
        	   'placeholder' => 'Full Name'
            )
        ));
        
        $builder->add('dob', 'birthday', array(
            'widget' => 'choice',
            'format' => 'dd-MMMM-yyyy',
            'years' => range(date('Y') - 65, date('Y') - 16)
        ));
        
        $builder->add('address');
        $builder->add('city');
        $builder->add('county', null, array(
            'attr' => array(
                'data-placeholder' => ' '
            ),
            'empty_value' => 'Pick your county...'
        ));
        
        $builder->add('phoneNumber', null, array(
            'label' => 'Phone Number'
        ));
        
        $builder->add('skype', null, array(
            'label' => 'Skype Username'
        ));
        
        $builder->add('avatarFile', null, array(
            'label' => 'Profile Picture'
        ));
        
        $builder->add('next', 'hidden', array(
            'mapped' => false
        ));
        
        $builder->add('temp_avatar', 'hidden', array(
            'mapped' => false
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Wanawork\UserBundle\Entity\EmployeeProfile'
        ));
    }

    public function getName()
    {
        return 'employee_profile';
    }
}

