<?php
namespace Wanawork\UserBundle\Form\Type;

class EmployerType extends UserType
{
    public function getName()
    {
        return 'employer';
    }    
}