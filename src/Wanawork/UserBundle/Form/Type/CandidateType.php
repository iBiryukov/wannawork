<?php
namespace Wanawork\UserBundle\Form\Type;

class CandidateType extends UserType
{
    public function getName()
    {
        return 'candidate';
    }
}