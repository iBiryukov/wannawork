<?php
namespace Wanawork\UserBundle\Form\Type;

use Wanawork\UserBundle\Entity\Role;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;

abstract class UserType extends AbstractType {

	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder->add('name');
		$builder->add('email', 'email');
		$builder->add('password', 'password');
	}
	
	
	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver) {
		$resolver->setDefaults(array(
			'data_class' => 'Wanawork\UserBundle\Entity\User'		
		));
	}
}
