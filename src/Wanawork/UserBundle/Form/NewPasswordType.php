<?php
namespace Wanawork\UserBundle\Form;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Form\AbstractType;

class NewPasswordType extends AbstractType {

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		 
		$builder->add('password', 'repeated', array(
			'first_name' => 'password',
			'second_name' => 'confirm',
			'type' => 'password',
			'invalid_message' => "The passwords don't match"
		));
	}
	
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
				'data_class' => 'Wanawork\UserBundle\Entity\Containers\PasswordContainer'
		));
	}
	
	public function getName()
	{
		return 'wanawork_userbundle_containers_passwordcontainer';
	}
}
