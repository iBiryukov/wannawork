<?php

namespace Wanawork\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Wanawork\UserBundle\Repository\SectorRepository;
use Wanawork\UserBundle\Repository\SectorJobRepository;

class EmployerProfileType extends AbstractType
{
	
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('companyName', null, array('label' => 'Company Name'))
            ->add('companyNumber', "text", array('label' => 'CRO Number', 'required' => false))
            ->add('contactName', null, array('label' => 'Contact name'))
            
            ->add('industries',  null, array('label' => 'Industries, where your company operates',
                'query_builder' => function(SectorRepository $rep) {
                    return $rep->createQueryBuilder('industry')->orderBy('industry.name', 'ASC');
                },
            ))
                
            ->add('professions', null, array('label' => 'Professions, you are looking for hire',
                'query_builder' => function(SectorJobRepository $er){
                    return $er->createQueryBuilder('profession')->addOrderBy('profession.name', 'ASC');
                },
            ))

            ->add('email')
            ->add('website')
            ->add('facebook')
            ->add('twitter')
            ->add('linkedin')
            ->add('skype')
            ->add('about', null, array('label' => 'Company Description'))
            //->add('isVerified')
        
            ->add('phoneNumber', null, array('label' => 'Phone Number'))
            ->add('address')
            ->add('city')
            //->add('verifiedBy')
            ->add('county', null, array('empty_value' => 'Please select'))
            ->add('avatarFile', null, array(
            		'label' => 'Company Logo'
            ));
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Wanawork\UserBundle\Entity\EmployerProfile'
        ));
    }

    public function getName()
    {
        return 'wanawork_userbundle_employerprofiletype';
    }
}
