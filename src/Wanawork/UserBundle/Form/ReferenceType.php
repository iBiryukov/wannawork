<?php
namespace Wanawork\UserBundle\Form;

use Wanawork\UserBundle\Form\DataTransformers\CvIdToEntityTransformer;

use Doctrine\Common\Persistence\ObjectManager;

use Wanawork\UserBundle\Entity\CV;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ReferenceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('employer', null, array(
                'label' => 'Company',
                'attr' => array(
            	    'placeholder' => 'Company you worked for',
                    'class' => 'company',
                ),
            ))
            ->add('contact', null, array(
                'label' => 'Contact name',
                'attr' => array(
            	   'placeholder' => 'Contact in the company',
                    'class' => 'contact-name',
                ),
            ))
            ->add('telephone', null, array(
                'label' => 'Phone Number',
                'attr' => array(
            	   'placeholder' => 'Their phone number',
                    'class' => 'phone-number',
                ),
            ))
            ->add('email', null, array(
                'label' => 'Email',
                'attr' => array(
            	   'placeholder' => 'Their email address',
                    'class' => 'email'
                ),
            ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Wanawork\UserBundle\Entity\Reference'
        ));
    }

    public function getName()
    {
        return 'wanawork_userbundle_referencetype';
    }
}
