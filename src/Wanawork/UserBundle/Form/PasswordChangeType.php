<?php
namespace Wanawork\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PasswordChangeType extends AbstractType
{
    private $isAdmin;
    
    public function __construct($isAdmin = false)
    {
        $this->isAdmin = $isAdmin;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ($this->isAdmin !== true) {
            $builder->add('currentPassword', 'password');
        }
        
        $builder->add('newPassword', 'repeated', array(
            'first_name' => 'newPassword',
            'second_name' => 'confirmNewPassword',
            'type' => 'password',
            'invalid_message' => "The passwords don't match"
        ));
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Wanawork\UserBundle\Entity\Containers\ChangePasswordContainer',
            'cascade_validation' => true,
        ));
    }
    
    public function getName()
    {
        return 'wanawork_userbundle_changepassword';
    }
}