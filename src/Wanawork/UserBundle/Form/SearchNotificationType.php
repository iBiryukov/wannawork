<?php

namespace Wanawork\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Wanawork\UserBundle\Entity\User;

class SearchNotificationType extends AbstractType
{
    /**
     * User, for whom the notification is
     * @var \Wanawork\UserBundle\Entity\User | null
     */
    private $user;
    
    public function __construct(User $user = null)
    {
        $this->user = $user;    
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('searchName', 'text', array(
            	'label' => 'Name your search',
            ));
            
        if (!$this->user) {
            $builder->add('email', null, array(
            	'label' => "Your email"
            ));
        }
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Wanawork\UserBundle\Entity\SearchNotification'
        ));
    }

    public function getName()
    {
        return 'wanawork_userbundle_searchnotificationtype';
    }
}
