<?php

namespace Wanawork\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Wanawork\UserBundle\Repository\SectorJobRepository;
use Wanawork\UserBundle\Repository\SectorRepository;

class SectorJobType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id')
            ->add('name')
            ->add('sectors', null, array(
        	    'attr' => array(
            	    'class' => 'chosen'
                ),
                'by_reference' => false,
                //'class' => 'WanaworkUserBundle:Sector',
                'query_builder' => function(SectorRepository $er){
                    $qb = $er->createQueryBuilder('job');
                    $qb->addOrderBy('job.name', 'ASC');
                    return $qb;
                },
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Wanawork\UserBundle\Entity\SectorJob'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'wanawork_userbundle_sectorjob';
    }
}
