<?php

namespace Wanawork\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MessageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('text', null, array(
            	'label' => "Message",
            	'attr' => array(
            		'class' => 'ckeditor',
            	    'placeholder' => 'Your message'
            	)		
           	))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Wanawork\UserBundle\Entity\Message'
        ));
    }

    public function getName()
    {
        return 'wanawork_userbundle_messagetype';
    }
}
