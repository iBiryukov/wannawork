<?php

namespace Wanawork\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends AbstractType
{
    /**
     * Is the user, using form an admin?
     * @var boolean
     */
    private $isAdmin = false;
    
    /**
     * @param boolean $isAdmin
     */
    public function __construct($isAdmin = false)
    {
        $this->isAdmin = $isAdmin;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $isAdmin = $this->isAdmin;
        
        $builder->add('email');
        $builder->add('name');
//         if ($isAdmin) {
//             $builder->add('password');
//         }
        
//        $builder->add('isActive');

        if ($isAdmin) {
            $builder->add('roles', 'entity', array(
                'class' => 'Wanawork\UserBundle\Entity\Role',
                'multiple' => true,
            ));
        }
        
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Wanawork\UserBundle\Entity\User'
        ));
    }

    public function getName()
    {
        return 'wanawork_userbundle_usertype';
    }
}
