<?php

namespace Wanawork\UserBundle\Form;

use Doctrine\ORM\EntityRepository;

use Wanawork\UserBundle\Entity\User;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Wanawork\UserBundle\Repository\SectorRepository;
use Wanawork\UserBundle\Repository\SectorJobRepository;
use Wanawork\UserBundle\Form\Type\EmployeeProfileType;
use Wanawork\UserBundle\Form\DataTransformers\CvIdToEntityTransformer;

class AdType extends AbstractType
{
	
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('headline')
            ->add('pitch')
            ->add('badge', 'text', array(
                'attr' => array(
            	   'placeholder' => 'Badges'
                )
            ))
            //->add('qualifications')
            ->add('positions', 'entity', array(
            	'label' => 'Role Types',
            	'class' => 'Wanawork\UserBundle\Entity\PositionType',
                'query_builder' => function(EntityRepository $repository) {
                    $qb = $repository->createQueryBuilder('p');
                    $qb->orderBy('p.order', "ASC");
                    return $qb;
                },
            	'multiple' => true,
            	'expanded' => false,
            ))
            ->add('locations')
            ->add('profession', 'entity', array(
                'class' => 'WanaworkUserBundle:SectorJob',
                'query_builder' => function(SectorJobRepository $er){
                    $qb = $er->createQueryBuilder('profession');
                    $qb->addOrderBy('profession.name', 'ASC');
                    return $qb;
                },
                'empty_value' => 'Profession',
            ))
            ->add('industries', 'entity', array(
                'class' => 'WanaworkUserBundle:Sector',
                'property' => 'name',
                'query_builder' => function(SectorRepository $rep) {
                    return $rep->createQueryBuilder('industry')->orderBy('industry.name');
                },
                'multiple' => true,
                'attr' => array(
                    'data-placeholder' => 'Industry/Industries'
                ),
                'required' => false,
            ))
            ->add('educationLevel', null, array(
                'empty_value' => 'Level of your education',
            ))
            ->add('experience', null, array(
                'empty_value' => 'Your experience',
            ))
            ->add(
                $builder->create('cv', 'hidden', [
                    'required' => false,
                ])
                ->addModelTransformer(new CvIdToEntityTransformer($options['em']))
            )
            ->add('languages', null, array(
                'attr' => array(
                    'data-placeholder' => 'Pick your language(s)'
                ),
                'query_builder' => function(EntityRepository $repository) {
                    $qb = $repository->createQueryBuilder('language');
                    $qb->addOrderBy('language.name', "ASC");
                    return $qb;
                },
            ))
            ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Wanawork\UserBundle\Entity\Ad',
            'cascade_validation' => true,
        ));
        
        $resolver->setRequired([
            'em',
        ])
        ->setAllowedTypes([
        	'em' => 'Doctrine\Common\Persistence\ObjectManager'
        ]);
    }

    public function getName()
    {
        return 'wanawork_userbundle_adtype';
    }
}
