<?php
namespace Wanawork\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CVFileType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', null, array(
            'label' => 'CV Name',
            'attr' => array(
                'placeholder' => 'CV Name, for you own reference',
            )
        ))
        ->add('file')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Wanawork\UserBundle\Entity\CVFile'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'wanawork_userbundle_cvfile';
    }
}
