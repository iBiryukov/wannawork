<?php
namespace Wanawork\UserBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Wanawork\UserBundle\Repository\SectorRepository;
use Wanawork\UserBundle\Repository\SectorJobRepository;

/**
 * Search Form
 * @author Ilya Biryukov <ilya@wanawork.ie>
 */
class SearchType extends AbstractType 
{
    /**
     * Is the user requesting the form an admin?
     * @var boolean
     */
    private $isAdmin;
    
    public function __construct($isAdmin)
    {
        $this->isAdmin = $isAdmin;
    }
    
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder->add('positions', 'entity', array(
			'class' => 'Wanawork\UserBundle\Entity\PositionType',
			'query_builder' => function(EntityRepository $repository) {
				return $repository->createQueryBuilder('p')
						->addOrderBy('p.order', 'asc')
						->addOrderBy('p.name', 'asc');
			},
			'multiple' => true,
			'attr' => array(
				'data-placeholder' => 'Role types'
			),
			'required' => false,
		));
		
		$builder->add('languages', 'entity', array(
			'class' => 'Wanawork\MainBundle\Entity\Language',
			'query_builder' => function(EntityRepository $repository) {
				return $repository->createQueryBuilder('p')
					->addOrderBy('p.name', 'asc');
			},
			'multiple' => true,
			'attr' => array(
			    
			),
			'required' => false,
		));
		
		$builder->add('profession', 'entity', array(
            'class' => 'WanaworkUserBundle:SectorJob',
            'query_builder' => function(SectorJobRepository $er){
                $qb = $er->createQueryBuilder('profession');
                $qb->addOrderBy('profession.name', 'ASC');
                return $qb;
            },
            'group_by' => 'sectorName',
            'attr' => array(
                'data-placeholder' => 'Profession'
            ),
            'empty_value' => ''
        ));
		
		$builder->add('experience', null, array(
		    'required' => false,
			'attr' => array(
			    'data-placeholder' => 'Experience'
		    ),
		));
		
        $builder->add('industries', 'entity', array(
            'class' => 'WanaworkUserBundle:Sector',
            'property' => 'name',
            'query_builder' => function(SectorRepository $rep) {
                return $rep->createQueryBuilder('industry')->orderBy('industry.name');
            },
            'multiple' => true,
            'empty_value' => 'Industries',
            'attr' => array(
                'data-placeholder' => 'Industries'
            ),
            'required' => false,
        ));
		
		$builder->add('locations', 'entity', array(
			'class' => 'Wanawork\MainBundle\Entity\County',
			'query_builder' => function(EntityRepository $repository) {
				return $repository->createQueryBuilder('p')
					->addOrderBy('p.name', 'asc');
			},
			'multiple' => true,
			'attr' => array(
					'class' => 'chosen'
			),
			'required' => false,
		));
		
		$builder->add('educationLevel', null, array(
		    'required' => false,
		));
		
		if($this->isAdmin) {
		    $builder->add('includeExpired', 'checkbox');
		}
	}
	
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'Wanawork\UserBundle\Entity\Search'
		));
	}
	
	public function getName()
	{
		return 'wanawork_userbundle_searchtype';
	}
}
