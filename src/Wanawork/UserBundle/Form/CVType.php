<?php
namespace Wanawork\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CVType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
    {
    	$builder->add('name', null, array(
    		'label' => 'CV Name',
    	    'attr' => array(
    		  'placeholder' => 'CV Name, for you own reference'
    	      
    	    )
    	));
    	
    	$builder->add('coverNote', 'textarea', array(
    	    'label' => 'Covert Note',
    	    'attr' => array(
    		   'placeholder' => 'Covert note for your CV'  
    	    )
    	));
    	
        $builder->add('educations', 'collection', array(
        	'type' => new EducationType(),
        	'allow_add' => true,
        	'by_reference' => false,
        	'allow_delete' => true
        ));
        
        $builder->add('jobs', 'collection', array(
        	'type' => new WorkExperienceType(),
        	'allow_add' => true,
        	'by_reference' => false,
        	'allow_delete' => true
        ));
        
        $builder->add('references', 'collection', array(
        	'type' => new ReferenceType(),
        	'allow_add' => true,
        	'by_reference' => false,
            'allow_delete' => true
        ));
        
        $builder->add('achievements');
        
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Wanawork\UserBundle\Entity\CVForm'
        ));
    }

    public function getName()
    {
        return 'wanawork_userbundle_cvtype';
    }
}
