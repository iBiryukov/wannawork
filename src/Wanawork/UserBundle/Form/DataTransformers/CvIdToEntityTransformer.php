<?php
namespace Wanawork\UserBundle\Form\DataTransformers;

use Symfony\Component\Form\Exception\TransformationFailedException;
use Wanawork\UserBundle\Entity\CV;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\DataTransformerInterface;

class CvIdToEntityTransformer implements DataTransformerInterface
{

    /**
     * Doctrine
     * @var \Doctrine\Common\Persistence\ObjectManage
     */
    private $om;

    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    public function transform($cv)
    {
        if ($cv instanceof CV) {
            return $cv->getId();
        }
        
        return "";
    }

    public function reverseTransform($cvId)
    {
        if (!$cvId) {
            return null;
        }
        
        $cv = $this->om->getRepository('WanaworkUserBundle:CV')->find($cvId);
        if (! $cv instanceof CV) {
            throw new TransformationFailedException("CV with id '$cvId' doesn't exist");
        }
        
        return $cv;
    }
}
