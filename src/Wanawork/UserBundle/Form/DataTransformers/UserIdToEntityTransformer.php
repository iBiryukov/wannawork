<?php
namespace Wanawork\UserBundle\Form\DataTransformers;

use Symfony\Component\Form\DataTransformerInterface;

use Symfony\Component\Form\Exception\TransformationFailedException;

use Wanawork\UserBundle\Entity\User;

use Doctrine\Common\Persistence\ObjectManager;

class UserIdToEntityTransformer implements DataTransformerInterface {
	private $om;
	
	public function __construct(ObjectManager $om) {
		$this->om = $om;
	}
	
	public function transform($user) {
		if($user instanceof User) {
			return $user->getId();
		}
	
		return "";
	}
	
	public function reverseTransform($userId) {
		if(!$userId) {
			return null;
		}
	
		$user = $this->om->getRepository('WanaworkUserBundle:User')->find($userId);
		if(!$user instanceof User) {
			throw new TransformationFailedException("User with id '$userId' doesn't exist");
		}
		return $user;
	}
}

