<?php
namespace Wanawork\UserBundle\Form\DataTransformers;

use Symfony\Component\Form\DataTransformerInterface;

class MonthYearToFullDateTransformer implements DataTransformerInterface
{
	
	public function transform($dateTime)
	{
		
		if ($dateTime instanceof \DateTime) {
			return $dateTime->format('m/Y');
		}
		return $dateTime;
	}
	
	public function reverseTransform($string)
	{
		if (!$this->isValidDate($string)) {
			return null;
		}
		
		$dateBits = explode('/', $string);
		$year = $dateBits[1];
		$month = $dateBits[0];
		$day = '1';
		
		$date = new \DateTime();
		$date->setDate($year, $month, $day);
		return $date;
	}
	
	public function isValidDate($string)
	{
	    return (bool)preg_match('/^(0[1-9]|1[0-2])\/((19\d{2})|(20\d{2}))$/', $string);
	}
}

