<?php
namespace Wanawork\UserBundle\Form\DataTransformers;

use Doctrine\Common\Persistence\ObjectManager;

use Symfony\Component\Form\DataTransformerInterface;

class StringToDateTransformer implements DataTransformerInterface {

	private $om;
	
	public function __construct(ObjectManager $om) {
		$this->om = $om;
	}
	
	public function transform($dateTime) {
		$string = "";
		if($dateTime instanceof \DateTime) {
			$string = $dateTime->format('d-M-Y');
		}
		return $string;
	}
	
	public function reverseTransform($string) {
		if(!$string) {
			return null;
		}
		return new \DateTime($string);
	}
}

?>