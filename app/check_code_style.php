<?php
$output = explode(PHP_EOL, `git status --short`);

$files = array();
foreach ($output as $line) {
    $line = trim($line);
    if(strlen($line) && $line{0} !== 'D' && preg_match('/php$/', $line)) {
        $linebits = explode(' ', $line);
        $files[] = $linebits[1];
    }
}

$argument = implode(' ', $files);
echo `phpcs $argument`;
