<?php
if (! function_exists('json_last_error_msg')) {

    function json_last_error_msg()
    {
        switch (json_last_error()) {
            default:
                $error = 'Unknown';
                return;
            case JSON_ERROR_DEPTH:
                $error = 'Maximum stack depth exceeded';
                break;
            case JSON_ERROR_STATE_MISMATCH:
                $error = 'Underflow or the modes mismatch';
                break;
            case JSON_ERROR_CTRL_CHAR:
                $error = 'Unexpected control character found';
                break;
            case JSON_ERROR_SYNTAX:
                $error = 'Syntax error, malformed JSON';
                break;
            case JSON_ERROR_UTF8:
                $error = 'Malformed UTF-8 characters, possibly incorrectly encoded';
                break;
        }
        return $error;
    }
}

function prettyPrint($json)
{
    $result = '';
    $level = 0;
    $prev_char = '';
    $in_quotes = false;
    $ends_line_level = NULL;
    $json_length = strlen($json);
    
    for ($i = 0; $i < $json_length; $i ++) {
        $char = $json[$i];
        $new_line_level = NULL;
        $post = "";
        if ($ends_line_level !== NULL) {
            $new_line_level = $ends_line_level;
            $ends_line_level = NULL;
        }
        if ($char === '"' && $prev_char != '\\') {
            $in_quotes = ! $in_quotes;
        } else 
            if (! $in_quotes) {
                switch ($char) {
                    case '}':
                    case ']':
                        $level --;
                        $ends_line_level = NULL;
                        $new_line_level = $level;
                        break;
                    
                    case '{':
                    case '[':
                        $level ++;
                    case ',':
                        $ends_line_level = $level;
                        break;
                    
                    case ':':
                        $post = " ";
                        break;
                    
                    case " ":
                    case "\t":
                    case "\n":
                    case "\r":
                        $char = "";
                        $ends_line_level = $new_line_level;
                        $new_line_level = NULL;
                        break;
                }
            }
        if ($new_line_level !== NULL) {
            $result .= "\n" . str_repeat("\t", $new_line_level);
        }
        $result .= $char . $post;
        $prev_char = $char;
    }
    
    return $result;
}

function uploadErrorToMessage($code)
{
    $message = '';
    switch ($code) {
    	case UPLOAD_ERR_INI_SIZE:
    	    $message = "The uploaded file exceeds the maximum allowed size. Please upload a smaller file";
    	    break;
    	case UPLOAD_ERR_FORM_SIZE:
    	    $message = "The uploaded file exceeds the maximum allowed size. Please upload a smaller file";
    	    break;
    	case UPLOAD_ERR_PARTIAL:
    	    $message = "The uploaded file was only partially uploaded. Please try again";
    	    break;
    	case UPLOAD_ERR_NO_FILE:
    	    $message = "No file was uploaded";
    	    break;
    	case UPLOAD_ERR_NO_TMP_DIR:
    	    $message = "Missing a temporary folder. Please try again";
    	    break;
    	case UPLOAD_ERR_CANT_WRITE:
    	    $message = "Failed to save the file. Please try again";
    	    break;
    	case UPLOAD_ERR_EXTENSION:
    	    $message = "File upload stopped by extension";
    	    break;

    	default:
    	    $message = "Unknown upload error";
    	    break;
    }
    return $message;
}

if ( ! function_exists('glob_recursive'))
{
    // Does not support flag GLOB_BRACE

    function glob_recursive($pattern, $flags = 0)
    {
        $files = glob($pattern, $flags);

        foreach (glob(dirname($pattern).'/*', GLOB_ONLYDIR|GLOB_NOSORT) as $dir)
        {
            $files = array_merge($files, glob_recursive($dir.'/'.basename($pattern), $flags));
        }

        return $files;
    }
}

function is_dir_empty($dir) {
    if (!is_readable($dir)) return NULL;
    $handle = opendir($dir);
    while (false !== ($entry = readdir($handle))) {
        if ($entry != "." && $entry != "..") {
            return FALSE;
        }
    }
    return TRUE;
}