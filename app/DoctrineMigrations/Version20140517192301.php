<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140517192301 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != "postgresql", 
            "Migration can only be executed safely on 'postgresql'."
        );
        
        $this->addSql("CREATE SEQUENCE JobsFairyTweetResponse_id_seq INCREMENT BY 1 MINVALUE 1 START 1");
        $this->addSql("CREATE TABLE JobsFairyTweet (id VARCHAR(255) NOT NULL, PRIMARY KEY(id))");
        $this->addSql(
            "CREATE TABLE jobsfairytweet_sectorjob 
            (jobsfairytweet_id VARCHAR(255) NOT NULL, 
            sectorjob_id INT NOT NULL, PRIMARY KEY(jobsfairytweet_id, sectorjob_id))"
        );
        $this->addSql("CREATE INDEX IDX_87DAE514AE9C603B ON jobsfairytweet_sectorjob (jobsfairytweet_id)");
        $this->addSql("CREATE INDEX IDX_87DAE51456031EA4 ON jobsfairytweet_sectorjob (sectorjob_id)");
        $this->addSql(
            "CREATE TABLE jobsfairytweet_ad 
             (jobsfairytweet_id VARCHAR(255) NOT NULL, ad_id INT NOT NULL, PRIMARY KEY(jobsfairytweet_id, ad_id))"
        );
        $this->addSql("CREATE INDEX IDX_6645EA33AE9C603B ON jobsfairytweet_ad (jobsfairytweet_id)");
        $this->addSql("CREATE INDEX IDX_6645EA334F34D596 ON jobsfairytweet_ad (ad_id)");
        $this->addSql(
            "CREATE TABLE JobsFairyTweetResponse 
             (id INT NOT NULL, tweet_id VARCHAR(255) DEFAULT NULL, 
              account_id VARCHAR(100) DEFAULT NULL, postedOn TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, 
              text VARCHAR(140) NOT NULL, PRIMARY KEY(id))"
        );
        $this->addSql("CREATE INDEX IDX_2ABD2BA81041E39B ON JobsFairyTweetResponse (tweet_id)");
        $this->addSql("CREATE INDEX IDX_2ABD2BA89B6B5FBA ON JobsFairyTweetResponse (account_id)");
        $this->addSql(
            "CREATE TABLE Tweet (id VARCHAR(255) NOT NULL, 
             retweet_id VARCHAR(255) DEFAULT NULL, user_id VARCHAR(255) DEFAULT NULL, 
            text VARCHAR(350) NOT NULL, createdAt TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))"
        );
        
        $this->addSql("CREATE INDEX IDX_FCA7253F72A1C5CA ON Tweet (retweet_id)");
        $this->addSql("CREATE INDEX IDX_FCA7253FA76ED395 ON Tweet (user_id)");
        $this->addSql(
            "CREATE TABLE twitter_user 
              (id VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, 
               screenName VARCHAR(255) NOT NULL, avatarUrl VARCHAR(255) NOT NULL, PRIMARY KEY(id))"
        );
        
        $this->addSql(
            "ALTER TABLE JobsFairyTweet ADD CONSTRAINT FK_B2588B1ABF396750 
              FOREIGN KEY (id) REFERENCES Tweet (id) NOT DEFERRABLE INITIALLY IMMEDIATE"
        );
        
        $this->addSql(
            "ALTER TABLE jobsfairytweet_sectorjob 
             ADD CONSTRAINT FK_87DAE514AE9C603B 
             FOREIGN KEY (jobsfairytweet_id) REFERENCES JobsFairyTweet (id) 
             ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE"
        );
        
        $this->addSql(
            "ALTER TABLE jobsfairytweet_sectorjob 
             ADD CONSTRAINT FK_87DAE51456031EA4 FOREIGN KEY (sectorjob_id) 
            REFERENCES SectorJob (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE"
        );
        
        $this->addSql(
            "ALTER TABLE jobsfairytweet_ad ADD CONSTRAINT FK_6645EA33AE9C603B 
             FOREIGN KEY (jobsfairytweet_id) REFERENCES JobsFairyTweet (id) 
             ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE"
        );
        
        $this->addSql(
            "ALTER TABLE jobsfairytweet_ad ADD CONSTRAINT 
             FK_6645EA334F34D596 FOREIGN KEY (ad_id) 
             REFERENCES Ad (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE"
        );
        
        $this->addSql(
            "ALTER TABLE JobsFairyTweetResponse 
             ADD CONSTRAINT FK_2ABD2BA81041E39B FOREIGN KEY (tweet_id) 
             REFERENCES JobsFairyTweet (id) NOT DEFERRABLE INITIALLY IMMEDIATE"
        );
        
//         $this->addSql(
//             "ALTER TABLE JobsFairyTweetResponse 
//              ADD CONSTRAINT FK_2ABD2BA89B6B5FBA FOREIGN KEY (account_id) 
//              REFERENCES social_twitter (id) NOT DEFERRABLE INITIALLY IMMEDIATE"
//         );
        
        $this->addSql(
            "ALTER TABLE Tweet ADD CONSTRAINT FK_FCA7253F72A1C5CA FOREIGN KEY (retweet_id) 
             REFERENCES Tweet (id) NOT DEFERRABLE INITIALLY IMMEDIATE"
        );
        
        $this->addSql(
            "ALTER TABLE Tweet ADD CONSTRAINT FK_FCA7253FA76ED395 FOREIGN KEY (user_id) 
            REFERENCES twitter_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE"
        );
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != "postgresql", 
            "Migration can only be executed safely on 'postgresql'."
        );
        
        $this->addSql("ALTER TABLE jobsfairytweet_sectorjob DROP CONSTRAINT FK_87DAE514AE9C603B");
        $this->addSql("ALTER TABLE jobsfairytweet_ad DROP CONSTRAINT FK_6645EA33AE9C603B");
        $this->addSql("ALTER TABLE JobsFairyTweetResponse DROP CONSTRAINT FK_2ABD2BA81041E39B");
        $this->addSql("ALTER TABLE JobsFairyTweet DROP CONSTRAINT FK_B2588B1ABF396750");
        $this->addSql("ALTER TABLE Tweet DROP CONSTRAINT FK_FCA7253F72A1C5CA");
        $this->addSql("ALTER TABLE Tweet DROP CONSTRAINT FK_FCA7253FA76ED395");
        $this->addSql("DROP SEQUENCE JobsFairyTweetResponse_id_seq CASCADE");
        $this->addSql("DROP TABLE JobsFairyTweet");
        $this->addSql("DROP TABLE jobsfairytweet_sectorjob");
        $this->addSql("DROP TABLE jobsfairytweet_ad");
        $this->addSql("DROP TABLE JobsFairyTweetResponse");
        $this->addSql("DROP TABLE Tweet");
        $this->addSql("DROP TABLE twitter_user");
    }
}
