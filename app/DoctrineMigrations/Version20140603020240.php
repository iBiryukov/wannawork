<?php

namespace Application\Migrations;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Wanawork\MainBundle\Security\AclManager;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140603020240 extends AbstractMigration implements ContainerAwareInterface
{
    private $container;
    
    public function up(Schema $schema)
    {
        $container = $this->container;
        $aclManager = new AclManager($container->get('security.acl.provider'), $container->get('security.context'));
        
        $date = new \DateTime('2014-05-26');
        $dql = "SELECT profile 
                FROM WanaworkUserBundle:EmployeeProfile profile 
                JOIN profile.user user
                WHERE user.registrationDate >= :date";
        
        $doctrine = $container->get('doctrine')->getManager();
        $query = $doctrine->createQuery($dql);
        $query->setParameter('date', $date);
        $profiles = $query->getResult();
        
        foreach ($profiles as $profile) {
            $this->write("Adding permissiosn to profile: {$profile->getId()}\n");
            $aclManager->grant($profile, MaskBuilder::MASK_OPERATOR, $profile->getUser());
        }
    }

    public function down(Schema $schema)
    {
        //nothing to do
    }
    
    /*
     * (non-PHPdoc) @see \Symfony\Component\DependencyInjection\ContainerAwareInterface::setContainer()
    */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
}
