<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140325032441 extends AbstractMigration
{
    private $changes = array(
        "Vouchers"
    );
    
    public function preDown(Schema $schema)
    {
        foreach ($this->changes as $change) {
            $this->write("\t--" . $change);
        }
    }
    
    public function preUp(Schema $schema)
    {
        foreach ($this->changes as $change) {
            $this->write("\t--" . $change);
        }
    }
    
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
        
        $this->addSql(
            "CREATE TABLE Voucher (
                id VARCHAR(100) NOT NULL, 
                discr VARCHAR(255) NOT NULL, 
                PRIMARY KEY(id)
            )"
        );
        $this->addSql(
            "CREATE TABLE voucher_payment (
                voucher_id VARCHAR(100) NOT NULL, 
                payment_id INT NOT NULL, 
                PRIMARY KEY(voucher_id, payment_id)
            )"
        );
        $this->addSql("CREATE INDEX IDX_AEE5AE0A28AA1B6F ON voucher_payment (voucher_id)");
        $this->addSql("CREATE INDEX IDX_AEE5AE0A4C3A3BB ON voucher_payment (payment_id)");
        $this->addSql(
            "CREATE TABLE AdVoucher (
                id VARCHAR(100) NOT NULL, 
                globalLimit INT DEFAULT NULL, 
                userLimit INT DEFAULT NULL, 
                PRIMARY KEY(id)
            )"
        );
        $this->addSql(
            "ALTER TABLE voucher_payment 
                ADD CONSTRAINT FK_AEE5AE0A28AA1B6F 
                FOREIGN KEY (voucher_id) 
                REFERENCES Voucher (id) 
                ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE"
        );
        $this->addSql(
            "ALTER TABLE voucher_payment 
                ADD CONSTRAINT FK_AEE5AE0A4C3A3BB 
                FOREIGN KEY (payment_id) 
                REFERENCES Payment (id) 
                ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE"
        );
        $this->addSql(
            "ALTER TABLE AdVoucher 
                ADD CONSTRAINT FK_3A02EE91BF396750 
                FOREIGN KEY (id) 
                REFERENCES Voucher (id) 
                ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE"
        );
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
        
        $this->addSql("ALTER TABLE voucher_payment DROP CONSTRAINT FK_AEE5AE0A28AA1B6F");
        $this->addSql("ALTER TABLE AdVoucher DROP CONSTRAINT FK_3A02EE91BF396750");
        $this->addSql("DROP TABLE Voucher");
        $this->addSql("DROP TABLE voucher_payment");
        $this->addSql("DROP TABLE AdVoucher");
    }
}
