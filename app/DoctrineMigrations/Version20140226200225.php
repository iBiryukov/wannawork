<?php
namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140226200225 extends AbstractMigration
{
    private $changes = array(
        "Job Spec - Salary field"
    );

    public function preDown(Schema $schema)
    {
        foreach ($this->changes as $change) {
            $this->write("\t--" . $change);
        }
    }

    public function preUp(Schema $schema)
    {
        foreach ($this->changes as $change) {
            $this->write("\t--" . $change);
        }
    }

    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()
            ->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
        
        $this->addSql("ALTER TABLE jobspec ADD salary VARCHAR(250) DEFAULT NULL");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()
            ->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
        
        $this->addSql("ALTER TABLE JobSpec DROP salary");
    }
}
