<?php
namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140303020608 extends AbstractMigration implements ContainerAwareInterface
{

    private $container;

    private $changes = array(
        "Sector - Slug Field"
    );

    public function preDown(Schema $schema)
    {
        foreach ($this->changes as $change) {
            $this->write("\t--" . $change);
        }
    }

    public function preUp(Schema $schema)
    {
        foreach ($this->changes as $change) {
            $this->write("\t--" . $change);
        }
    }

    public function postUp(Schema $schema)
    {
        $this->write("Generating Slugs");
        $doctrine = $this->container->get('doctrine')->getManager();
        $sectors = $doctrine->getRepository("WanaworkUserBundle:Sector")->findAll();
        
        foreach ($sectors as $sector) {
            if ($sector->getSlug() === null) {
                $sector->setSlug($sector->getName());
                $doctrine->persist($sector);
            }
        }
        
        $doctrine->flush();
        
        $this->connection->executeQuery("ALTER TABLE sector alter column slug DROP DEFAULT");
        $this->connection->executeQuery("ALTER TABLE sector alter column slug SET NOT NULL");
        $this->connection->executeQuery("CREATE UNIQUE INDEX UNIQ_4C0FDCDE989D9B62 ON sector (slug)");
    }

    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()
            ->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
        
        $this->addSql("ALTER TABLE sector ADD slug VARCHAR(255) DEFAULT NULL");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()
            ->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
        
        $this->addSql("DROP INDEX IF EXISTS UNIQ_4C0FDCDE989D9B62");
        $this->addSql("ALTER TABLE Sector DROP slug");
    }
    
    /*
     * (non-PHPdoc) @see \Symfony\Component\DependencyInjection\ContainerAwareInterface::setContainer()
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
}
