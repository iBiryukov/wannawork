<?php
namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140407000803 extends AbstractMigration
{
    private $changes = array(
        "Twitter Account for user account"
    );
    
    public function preDown(Schema $schema)
    {
        foreach ($this->changes as $change) {
            $this->write("\t--" . $change);
        }
    }
    
    public function preUp(Schema $schema)
    {
        foreach ($this->changes as $change) {
            $this->write("\t--" . $change);
        }
    }

    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()
            ->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
        
        $this->addSql(
            "CREATE TABLE social_twitter (
                id VARCHAR(100) NOT NULL, 
                user_id INT DEFAULT NULL, 
                username VARCHAR(255) NOT NULL, 
                token BYTEA NOT NULL, 
                createdAt TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, 
                updatedAt TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, 
                PRIMARY KEY(id, user_id)
            )"
        );
        $this->addSql("CREATE INDEX IDX_9911AE2BA76ED395 ON social_twitter (user_id)");
        $this->addSql("COMMENT ON COLUMN social_twitter.token IS '(DC2Type:object)'");
        $this->addSql(
            "ALTER TABLE social_twitter 
                ADD CONSTRAINT FK_9911AE2BA76ED395 
                FOREIGN KEY (user_id) 
                REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE"
        );
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()
            ->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
        
        $this->addSql("DROP TABLE social_twitter");
    }
}
