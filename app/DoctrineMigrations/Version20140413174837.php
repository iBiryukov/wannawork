<?php
namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Wanawork\UserBundle\Entity\EmployerProfile;
use Wanawork\UserBundle\Entity\EmployeeProfile;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140413174837 extends AbstractMigration implements ContainerAwareInterface
{
    private $container;
    
    private $changes = array(
        "EmployerProfile - Slug Field",
        "Employer & Employee Profile - created & updated fields"
    );
    
    public function preDown(Schema $schema)
    {
        foreach ($this->changes as $change) {
            $this->write("\t--" . $change);
        }
    }
    
    public function preUp(Schema $schema)
    {
        foreach ($this->changes as $change) {
            $this->write("\t--" . $change);
        }
    }
    
    public function postUp(Schema $schema)
    {
        $this->write("Generating Slugs");
        $doctrine = $this->container->get('doctrine')->getManager();
        $profiles = $doctrine->getRepository("WanaworkUserBundle:AbstractProfile")->findAll();
    
        foreach ($profiles as $profile) {
            if ($profile instanceof EmployerProfile && $profile->getSlug() === null) {
                $profile->setSlug($profile->getCompanyName());
                
                if (sizeof($profile->getAdminUsers()) > 0) {
                    $date = $profile->getAdminUsers()->first()->getRegistrationDate();
                    $profile->setCreatedAt($date);
                    $profile->setUpdatedAt($date);
                }
            } elseif ($profile instanceof EmployeeProfile) {
                $date = $profile->getUser()->getRegistrationDate();
                $profile->setCreatedAt($date);
                $profile->setUpdatedAt($date);
            }
        }
    
        $doctrine->flush();
    
        $this->connection->executeQuery("ALTER TABLE EmployerProfile alter column slug DROP DEFAULT");
        $this->connection->executeQuery("ALTER TABLE EmployerProfile alter column slug SET NOT NULL");
        $this->connection->executeQuery("CREATE UNIQUE INDEX UNIQ_6053B629989D9B62 ON employerprofile (slug)");
        
        $this->connection->executeQuery("ALTER TABLE abstractprofile alter column createdAt DROP DEFAULT");
        $this->connection->executeQuery("ALTER TABLE abstractprofile alter column createdAt SET NOT NULL");
        
        $this->connection->executeQuery("ALTER TABLE abstractprofile alter column updatedAt DROP DEFAULT");
        $this->connection->executeQuery("ALTER TABLE abstractprofile alter column updatedAt SET NOT NULL");
    
    }

    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()
            ->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
        
        $this->addSql("ALTER TABLE employerprofile ADD slug VARCHAR(275) DEFAULT NULL");
        
        $this->addSql("ALTER TABLE abstractprofile ADD createdAt TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL");
        $this->addSql("ALTER TABLE abstractprofile ADD updatedAt TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()
            ->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
        
        $this->addSql("DROP INDEX UNIQ_6053B629989D9B62");
        $this->addSql("ALTER TABLE EmployerProfile DROP slug");
        
        $this->addSql("ALTER TABLE AbstractProfile DROP createdAt");
        $this->addSql("ALTER TABLE AbstractProfile DROP updatedAt");
    }
    
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
}
