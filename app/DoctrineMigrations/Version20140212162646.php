<?php
namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140212162646 extends AbstractMigration
{

    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()
            ->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
        
        $this->addSql("CREATE SEQUENCE SearchNotificationCheck_id_seq INCREMENT BY 1 MINVALUE 1 START 1");
        $this->addSql(
            "CREATE TABLE SearchNotificationCheck 
            (id INT NOT NULL, 
            search_id VARCHAR(255) DEFAULT NULL, 
            date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, 
            newAdsCount INT NOT NULL, PRIMARY KEY(id))"
        );
        $this->addSql("CREATE INDEX IDX_3700037A650760A9 ON SearchNotificationCheck (search_id)");
        $this->addSql(
            "CREATE TABLE searchnotificationcheck_ad 
            (searchnotificationcheck_id INT NOT NULL, 
            ad_id INT NOT NULL,
             PRIMARY KEY(searchnotificationcheck_id, ad_id))"
        );
        
        $this->addSql("CREATE INDEX IDX_B08C73A67855F9AE ON searchnotificationcheck_ad (searchnotificationcheck_id)");
        $this->addSql("CREATE INDEX IDX_B08C73A64F34D596 ON searchnotificationcheck_ad (ad_id)");
        $this->addSql(
            "ALTER TABLE SearchNotificationCheck 
            ADD CONSTRAINT FK_3700037A650760A9 FOREIGN KEY (search_id) 
            REFERENCES SearchNotification (search_id) NOT DEFERRABLE INITIALLY IMMEDIATE"
        );
        $this->addSql(
            "ALTER TABLE searchnotificationcheck_ad 
            ADD CONSTRAINT FK_B08C73A67855F9AE 
            FOREIGN KEY (searchnotificationcheck_id) 
            REFERENCES SearchNotificationCheck (id) 
            ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE"
        );
        
        $this->addSql(
            "ALTER TABLE searchnotificationcheck_ad 
            ADD CONSTRAINT FK_B08C73A64F34D596 FOREIGN KEY (ad_id) 
            REFERENCES Ad (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE"
        );
        
        $this->addSql("ALTER TABLE searchnotification ADD user_id INT DEFAULT NULL");
        $this->addSql("ALTER TABLE searchnotification ADD email VARCHAR(250) DEFAULT NULL");
        $this->addSql("ALTER TABLE searchnotification ADD active BOOLEAN NOT NULL");
        $this->addSql("ALTER TABLE searchnotification DROP lastupdated");
        $this->addSql("ALTER TABLE searchnotification DROP newlyfoundads");
        $this->addSql("ALTER TABLE searchnotification RENAME COLUMN newlyavailable TO employer_id");
        $this->addSql(
            "ALTER TABLE searchnotification ADD CONSTRAINT FK_DFF73A6241CD9E7A 
            FOREIGN KEY (employer_id) REFERENCES EmployerProfile (id) 
            NOT DEFERRABLE INITIALLY IMMEDIATE"
        );
        $this->addSql(
            "ALTER TABLE searchnotification 
            ADD CONSTRAINT FK_DFF73A62A76ED395 FOREIGN KEY (user_id) 
            REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE"
        );
        $this->addSql("CREATE INDEX IDX_DFF73A6241CD9E7A ON searchnotification (employer_id)");
        $this->addSql("CREATE INDEX IDX_DFF73A62A76ED395 ON searchnotification (user_id)");
        $this->addSql("ALTER TABLE search DROP foundadcount");
        $this->addSql("ALTER TABLE searchpagestat ADD totalFound INT DEFAULT NULL");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()
            ->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
        
        $this->addSql("ALTER TABLE searchnotificationcheck_ad DROP CONSTRAINT FK_B08C73A67855F9AE");
        $this->addSql("DROP SEQUENCE SearchNotificationCheck_id_seq CASCADE");
        $this->addSql("DROP TABLE SearchNotificationCheck");
        $this->addSql("DROP TABLE searchnotificationcheck_ad");
        $this->addSql("ALTER TABLE Search ADD foundadcount INT DEFAULT NULL");
        $this->addSql("ALTER TABLE SearchNotification DROP CONSTRAINT FK_DFF73A6241CD9E7A");
        $this->addSql("ALTER TABLE SearchNotification DROP CONSTRAINT FK_DFF73A62A76ED395");
        $this->addSql("DROP INDEX IDX_DFF73A6241CD9E7A");
        $this->addSql("DROP INDEX IDX_DFF73A62A76ED395");
        $this->addSql("ALTER TABLE SearchNotification ADD lastupdated TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL");
        $this->addSql("ALTER TABLE SearchNotification ADD newlyavailable INT DEFAULT NULL");
        $this->addSql("ALTER TABLE SearchNotification ADD newlyfoundads TEXT NOT NULL");
        $this->addSql("ALTER TABLE SearchNotification DROP employer_id");
        $this->addSql("ALTER TABLE SearchNotification DROP user_id");
        $this->addSql("ALTER TABLE SearchNotification DROP email");
        $this->addSql("ALTER TABLE SearchNotification DROP active");
        $this->addSql("COMMENT ON COLUMN SearchNotification.newlyfoundads IS '(DC2Type:array)'");
        $this->addSql("ALTER TABLE SearchPageStat DROP totalFound");
    }
}
