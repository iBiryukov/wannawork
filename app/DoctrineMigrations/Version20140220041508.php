<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140220041508 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
        
        $this->addSql("CREATE SEQUENCE SystemEmailLinkClick_id_seq INCREMENT BY 1 MINVALUE 1 START 1");
        $this->addSql("CREATE TABLE SystemEmailLink 
            (id VARCHAR(255) NOT NULL, 
            href VARCHAR(250) NOT NULL, text VARCHAR(250) NOT NULL, 
            systemEmail_id VARCHAR(255) DEFAULT NULL, 
            PRIMARY KEY(id))");
        
        $this->addSql("CREATE INDEX IDX_1F2F47ACC97F3E71 ON SystemEmailLink (systemEmail_id)");
        
        $this->addSql("CREATE TABLE SystemEmailLinkClick 
            (id BIGINT NOT NULL, link_id VARCHAR(255) DEFAULT NULL, 
            ip VARCHAR(255) NOT NULL, 
            date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, 
            PRIMARY KEY(id))");
        
        $this->addSql("CREATE INDEX IDX_3668F074ADA40271 ON SystemEmailLinkClick (link_id)");
        
        $this->addSql("ALTER TABLE SystemEmailLink ADD CONSTRAINT FK_1F2F47ACC97F3E71 
            FOREIGN KEY (systemEmail_id) 
            REFERENCES SystemEmail (id) NOT DEFERRABLE INITIALLY IMMEDIATE");
        
        $this->addSql("ALTER TABLE SystemEmailLinkClick 
            ADD CONSTRAINT FK_3668F074ADA40271 
            FOREIGN KEY (link_id) 
            REFERENCES SystemEmailLink (id) NOT DEFERRABLE INITIALLY IMMEDIATE");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
        
        $this->addSql("ALTER TABLE SystemEmailLinkClick DROP CONSTRAINT FK_3668F074ADA40271");
        $this->addSql("DROP SEQUENCE SystemEmailLinkClick_id_seq CASCADE");
        $this->addSql("DROP TABLE SystemEmailLink");
        $this->addSql("DROP TABLE SystemEmailLinkClick");
    }
}
