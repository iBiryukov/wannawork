<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140403162256 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
        
        $this->addSql("CREATE TABLE employerprofile_sector (employerprofile_id INT NOT NULL, sector_id INT NOT NULL, PRIMARY KEY(employerprofile_id, sector_id))");
        $this->addSql("CREATE INDEX IDX_25481A4F33B0AAF ON employerprofile_sector (employerprofile_id)");
        $this->addSql("CREATE INDEX IDX_25481A4FDE95C867 ON employerprofile_sector (sector_id)");
        $this->addSql("ALTER TABLE employerprofile_sector ADD CONSTRAINT FK_25481A4F33B0AAF FOREIGN KEY (employerprofile_id) REFERENCES EmployerProfile (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE");
        $this->addSql("ALTER TABLE employerprofile_sector ADD CONSTRAINT FK_25481A4FDE95C867 FOREIGN KEY (sector_id) REFERENCES Sector (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
        $this->addSql("DROP TABLE employerprofile_sector");
    }
}
