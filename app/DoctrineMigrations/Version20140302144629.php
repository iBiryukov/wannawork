<?php
namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140302144629 extends AbstractMigration implements ContainerAwareInterface
{
    private $container;

    private $changes = array(
        "Sector Job - Slug Field"
    );

    public function preDown(Schema $schema)
    {
        foreach ($this->changes as $change) {
            $this->write("\t--" . $change);
        }
    }

    public function preUp(Schema $schema)
    {
        foreach ($this->changes as $change) {
            $this->write("\t--" . $change);
        }
    }
    
    public function postUp(Schema $schema)
    {
        $this->write("Generating Slugs");
        $doctrine = $this->container->get('doctrine')->getManager();
        $sectorJobs = $doctrine->getRepository("WanaworkUserBundle:SectorJob")->findAll();
        
        foreach($sectorJobs as $sectorJob) {
            if ($sectorJob->getSlug() === null) {
                $sectorJob->setSlug($sectorJob->getName());
                $doctrine->persist($sectorJob);
            }
        }
        
        $doctrine->flush();
        
        $this->connection->executeQuery(
            "ALTER TABLE sectorjob alter column slug DROP DEFAULT"
        );
        
        $this->connection->executeQuery(
            "ALTER TABLE sectorjob alter column slug SET NOT NULL"
        );
        $this->connection->executeQuery("CREATE UNIQUE INDEX UNIQ_cf85a9659c8606cf76d3113fc885cab8 ON sectorjob (slug)");
    }
    
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()
            ->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
        
        $this->addSql("ALTER TABLE sectorjob ADD slug VARCHAR(255) DEFAULT NULL");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()
            ->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
        
        $this->addSql("ALTER TABLE SectorJob DROP slug");
    }
    
	/* (non-PHPdoc)
	 * @see \Symfony\Component\DependencyInjection\ContainerAwareInterface::setContainer()
	 */
	public function setContainer(ContainerInterface $container = null)
	{
        $this->container = $container;
	}

}
