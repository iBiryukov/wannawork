<?php
namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20140224181622 extends AbstractMigration
{
    private $changes = array(
    	"Job specs entity now has fields that are present in the search"
    );
    
    public function preDown(Schema $schema)
    {
        foreach ($this->changes as $change) {
            $this->write("\t--" . $change);
        }
    }
    
    public function preUp(Schema $schema)
    {
        foreach ($this->changes as $change) {
            $this->write("\t--" . $change);
        }
    }

    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()
            ->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
        
        $this->addSql(
            "CREATE TABLE jobspec_sector (
                jobspec_id INT NOT NULL, 
                sector_id INT NOT NULL, 
                PRIMARY KEY(jobspec_id, sector_id)
            )"
        );
        
        $this->addSql("CREATE INDEX IDX_945EE2453B86EF9 ON jobspec_sector (jobspec_id)");
        $this->addSql("CREATE INDEX IDX_945EE245DE95C867 ON jobspec_sector (sector_id)");
        
        $this->addSql(
            "CREATE TABLE jobspec_positiontype (
                jobspec_id INT NOT NULL, 
                positiontype_id INT NOT NULL, 
                PRIMARY KEY(jobspec_id, positiontype_id)
            )"
        );
        
        $this->addSql("CREATE INDEX IDX_B4A9711B3B86EF9 ON jobspec_positiontype (jobspec_id)");
        $this->addSql("CREATE INDEX IDX_B4A9711BC491059D ON jobspec_positiontype (positiontype_id)");
        
        $this->addSql(
            "CREATE TABLE jobspec_language (
                jobspec_id INT NOT NULL, 
                language_id SMALLINT NOT NULL, 
                PRIMARY KEY(jobspec_id, language_id)
            )"
        );
        
        $this->addSql("CREATE INDEX IDX_7E7985C83B86EF9 ON jobspec_language (jobspec_id)");
        $this->addSql("CREATE INDEX IDX_7E7985C882F1BAF4 ON jobspec_language (language_id)");
        
        $this->addSql(
            "CREATE TABLE jobspec_county (
                jobspec_id INT NOT NULL, 
                county_id INT NOT NULL, 
                PRIMARY KEY(jobspec_id, county_id)
            )"
        );
        $this->addSql("CREATE INDEX IDX_871FC4883B86EF9 ON jobspec_county (jobspec_id)");
        $this->addSql("CREATE INDEX IDX_871FC48885E73F45 ON jobspec_county (county_id)");
        
        $this->addSql(
            "ALTER TABLE jobspec_sector 
            ADD CONSTRAINT FK_945EE2453B86EF9 
            FOREIGN KEY (jobspec_id) 
            REFERENCES JobSpec (id) 
            ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE"
        );
        
        $this->addSql(
            "ALTER TABLE jobspec_sector 
            ADD CONSTRAINT FK_945EE245DE95C867 
            FOREIGN KEY (sector_id) REFERENCES Sector (id) 
            ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE"
        );
        
        $this->addSql(
            "ALTER TABLE jobspec_positiontype 
            ADD CONSTRAINT FK_B4A9711B3B86EF9 
            FOREIGN KEY (jobspec_id) REFERENCES JobSpec (id) 
            ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE"
        );
        
        $this->addSql(
            "ALTER TABLE jobspec_positiontype 
            ADD CONSTRAINT FK_B4A9711BC491059D 
            FOREIGN KEY (positiontype_id) 
            REFERENCES PositionType (id) 
            ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE"
        );
        
        $this->addSql(
            "ALTER TABLE jobspec_language 
            ADD CONSTRAINT FK_7E7985C83B86EF9 
            FOREIGN KEY (jobspec_id) 
            REFERENCES JobSpec (id) 
            ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE"
        );
        
        $this->addSql(
            "ALTER TABLE jobspec_language 
            ADD CONSTRAINT FK_7E7985C882F1BAF4 
            FOREIGN KEY (language_id) 
            REFERENCES Language (id) 
            ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE"
        );
        
        $this->addSql(
            "ALTER TABLE jobspec_county 
            ADD CONSTRAINT FK_871FC4883B86EF9 
            FOREIGN KEY (jobspec_id) 
            REFERENCES JobSpec (id) 
            ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE"
        );
        
        $this->addSql(
            "ALTER TABLE jobspec_county 
            ADD CONSTRAINT FK_871FC48885E73F45 
            FOREIGN KEY (county_id) 
            REFERENCES County (id) 
            ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE"
        );
        
        $this->addSql("ALTER TABLE jobspec ADD profession_id INT DEFAULT NULL");
        $this->addSql("ALTER TABLE jobspec ADD experience_id INT DEFAULT NULL");
        $this->addSql("ALTER TABLE jobspec ADD educationLevel_id INT DEFAULT NULL");
        
        $this->addSql(
            "ALTER TABLE jobspec 
            ADD CONSTRAINT FK_263F0BF7FDEF8996 
            FOREIGN KEY (profession_id) 
            REFERENCES SectorJob (id) NOT DEFERRABLE INITIALLY IMMEDIATE"
        );
        
        $this->addSql(
            "ALTER TABLE jobspec 
            ADD CONSTRAINT FK_263F0BF746E90E27 
            FOREIGN KEY (experience_id) 
            REFERENCES SectorExperience (id) 
            NOT DEFERRABLE INITIALLY IMMEDIATE"
        );
        $this->addSql(
            "ALTER TABLE jobspec 
            ADD CONSTRAINT FK_263F0BF7CEABA754 
            FOREIGN KEY (educationLevel_id) 
            REFERENCES Qualification (id) NOT DEFERRABLE INITIALLY IMMEDIATE"
        );
        $this->addSql("CREATE INDEX IDX_263F0BF7FDEF8996 ON jobspec (profession_id)");
        $this->addSql("CREATE INDEX IDX_263F0BF746E90E27 ON jobspec (experience_id)");
        $this->addSql("CREATE INDEX IDX_263F0BF7CEABA754 ON jobspec (educationLevel_id)");
    }

    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()
            ->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
        
        $this->addSql("DROP TABLE jobspec_sector");
        $this->addSql("DROP TABLE jobspec_positiontype");
        $this->addSql("DROP TABLE jobspec_language");
        $this->addSql("DROP TABLE jobspec_county");
        $this->addSql("ALTER TABLE JobSpec DROP CONSTRAINT FK_263F0BF7FDEF8996");
        $this->addSql("ALTER TABLE JobSpec DROP CONSTRAINT FK_263F0BF746E90E27");
        $this->addSql("ALTER TABLE JobSpec DROP CONSTRAINT FK_263F0BF7CEABA754");
        $this->addSql("DROP INDEX IDX_263F0BF7FDEF8996");
        $this->addSql("DROP INDEX IDX_263F0BF746E90E27");
        $this->addSql("DROP INDEX IDX_263F0BF7CEABA754");
        $this->addSql("ALTER TABLE JobSpec DROP profession_id");
        $this->addSql("ALTER TABLE JobSpec DROP experience_id");
        $this->addSql("ALTER TABLE JobSpec DROP educationLevel_id");
    }
}
