<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Wanawork\UserBundle\Entity\Ad;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140413005656 extends AbstractMigration implements ContainerAwareInterface
{
    private $container;
    
    private $changes = array(
        "Advert - Slug Field",
        "Advert - CreatedAt & UpdatedAt columns"
    );
    
    public function preDown(Schema $schema)
    {
        foreach ($this->changes as $change) {
            $this->write("\t--" . $change);
        }
    }
    
    public function preUp(Schema $schema)
    {
        foreach ($this->changes as $change) {
            $this->write("\t--" . $change);
        }
    }
    
    public function postUp(Schema $schema)
    {
        $this->write("Generating Slugs");
        $doctrine = $this->container->get('doctrine')->getManager();
        $ads = $doctrine->getRepository("WanaworkUserBundle:Ad")->findAll();
    
        foreach ($ads as $ad) {
            if ($ad instanceof Ad && $ad->getSlug() === null) {
                $ad->setSlug($ad->getHeadline());
                $doctrine->persist($ad);
            }
        }
    
        $doctrine->flush();
    
        $this->connection->executeQuery("ALTER TABLE ad alter column slug DROP DEFAULT");
        $this->connection->executeQuery("ALTER TABLE ad alter column slug SET NOT NULL");
        $this->connection->executeQuery("CREATE UNIQUE INDEX UNIQ_E264C9FA989D9B62 ON ad (slug)");
        
        $this->connection->executeQuery('UPDATE ad SET updatedAt=createdAt');
        $this->connection->executeQuery("ALTER TABLE ad alter column updatedAt DROP DEFAULT");
        $this->connection->executeQuery("ALTER TABLE ad alter column updatedAt SET NOT NULL");
    }
    
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()
            ->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
    
        $this->addSql("ALTER TABLE ad ADD slug VARCHAR(275) DEFAULT NULL");
        $this->addSql("ALTER TABLE ad ADD updatedAt TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL");
        $this->addSql("ALTER TABLE ad RENAME COLUMN postdate TO createdAt");
    }
    
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()
            ->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
    
        $this->addSql("DROP INDEX UNIQ_E264C9FA989D9B62");
        $this->addSql("ALTER TABLE Ad DROP slug");
        
        $this->addSql("ALTER TABLE ad RENAME COLUMN createdAt TO postdate");
        $this->addSql("ALTER TABLE Ad DROP updatedAt");
    }
    
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
}
