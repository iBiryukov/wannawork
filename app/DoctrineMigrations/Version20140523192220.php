<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140523192220 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
        
        $this->addSql("DROP SEQUENCE jobsfairytweetresponse_id_seq CASCADE");
        $this->addSql("DROP TABLE jobsfairytweetresponse");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
        
        $this->addSql("CREATE SEQUENCE jobsfairytweetresponse_id_seq INCREMENT BY 1 MINVALUE 1 START 1");
        $this->addSql("CREATE TABLE jobsfairytweetresponse (id INT NOT NULL, tweet_id VARCHAR(255) DEFAULT NULL, account_id VARCHAR(100) DEFAULT NULL, postedon TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, text VARCHAR(140) NOT NULL, PRIMARY KEY(id))");
        $this->addSql("CREATE INDEX idx_2abd2ba81041e39b ON jobsfairytweetresponse (tweet_id)");
        $this->addSql("CREATE INDEX idx_2abd2ba89b6b5fba ON jobsfairytweetresponse (account_id)");
        $this->addSql("ALTER TABLE jobsfairytweetresponse ADD CONSTRAINT fk_2abd2ba81041e39b FOREIGN KEY (tweet_id) REFERENCES jobsfairytweet (id) NOT DEFERRABLE INITIALLY IMMEDIATE");
    }
}
