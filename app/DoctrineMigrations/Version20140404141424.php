<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140404141424 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
        
        $this->addSql("CREATE TABLE employerprofile_sectorjob (employerprofile_id INT NOT NULL, sectorjob_id INT NOT NULL, PRIMARY KEY(employerprofile_id, sectorjob_id))");
        $this->addSql("CREATE INDEX IDX_4834195933B0AAF ON employerprofile_sectorjob (employerprofile_id)");
        $this->addSql("CREATE INDEX IDX_4834195956031EA4 ON employerprofile_sectorjob (sectorjob_id)");
        $this->addSql("ALTER TABLE employerprofile_sectorjob ADD CONSTRAINT FK_4834195933B0AAF FOREIGN KEY (employerprofile_id) REFERENCES EmployerProfile (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE");
        $this->addSql("ALTER TABLE employerprofile_sectorjob ADD CONSTRAINT FK_4834195956031EA4 FOREIGN KEY (sectorjob_id) REFERENCES SectorJob (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
        $this->addSql("DROP TABLE employerprofile_sectorjob");
    }
}
