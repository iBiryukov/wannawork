<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140505012732 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
        
        $this->addSql("ALTER TABLE abstractprofile ALTER phonenumber DROP NOT NULL");
        $this->addSql("ALTER TABLE abstractprofile ALTER address DROP NOT NULL");
        $this->addSql("ALTER TABLE abstractprofile ALTER city DROP NOT NULL");
        $this->addSql("ALTER TABLE employeeprofile ALTER dob DROP NOT NULL");
        $this->addSql("DROP INDEX search_idx");
        $this->addSql("ALTER TABLE ad DROP processid");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
        
        $this->addSql("ALTER TABLE EmployeeProfile ALTER dob SET NOT NULL");
        $this->addSql("ALTER TABLE Ad ADD processid INT DEFAULT NULL");
        $time = time();
        $this->addSql("UPDATE Ad set processid=(id+$time)");
        $this->addSql("CREATE UNIQUE INDEX search_idx ON Ad (profile_id, processid)");
        $this->addSql("ALTER TABLE AbstractProfile ALTER phoneNumber SET NOT NULL");
        $this->addSql("ALTER TABLE AbstractProfile ALTER address SET NOT NULL");
        $this->addSql("ALTER TABLE AbstractProfile ALTER city SET NOT NULL");
    }
}
