<?php
header('Content-Type: application/json');
$firstName = isset($_POST['firstName']) ? $_POST['firstName'] : '';
$lastName = isset($_POST['lastName']) ? $_POST['lastName'] : '';
$email = isset($_POST['email']) ? $_POST['email'] : '';

$error = '';
if(empty($firstName)) {
	$error = 'Please enter your first name';
} elseif(empty($lastName)) {
	$error = 'Please enter your last name';
} elseif(empty($email)) {
	$error = 'Please enter your email';
}

$return = array(
	'outcome' => false,
	'msg' => '',
);

if($error) {
	$return['msg'] = $error;
} else {
	$fp = fopen('./contacts.txt', 'a');
	fwrite($fp, "$firstName $lastName <$email>\n");
	fclose($fp);
	$return['outcome'] = true;
}

echo json_encode($return);