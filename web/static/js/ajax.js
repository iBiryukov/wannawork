$(function(){
	$.ajaxSetup({
		beforeSend: function(){
			$('#loading-indicator').show();
		},
		complete: function(){
			$('#loading-indicator').hide();
		},
		error: function(event, jqXHR, ajaxSettings, thrownError){
			alert('An error occurred. Please try again later');
		}
	});
});