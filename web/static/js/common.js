CKEDITOR_BASEPATH = '/vendor/ckeditor/';
//CKConfig.ForcePasteAsPlainText = true ;

wanawork.dateStartYear = function() {
	return wanawork.dateEndYear() - 60;
};

wanawork.dateEndYear = function() {
	return new Date().getFullYear();
};

wanawork.blocksToClose = [];

Date.prototype.getWeek = function() {
    var onejan = new Date(this.getFullYear(),0,1);
    return Math.ceil((((this - onejan) / 86400000) + onejan.getDay()+1)/7);
};

function firstDayOfWeek(week, year) {

    if (typeof year !== 'undefined') {
        year = (new Date()).getFullYear();
    }

    var date       = firstWeekOfYear(year),
        weekTime   = weeksToMilliseconds(week),
        targetTime = date.getTime() + weekTime - 86400000;

    var result = new Date(targetTime)

    return result; 
}

function weeksToMilliseconds(weeks) {
    return 1000 * 60 * 60 * 24 * 7 * (weeks - 1);
}

function firstWeekOfYear(year) {
    var date = new Date();
    date = firstDayOfYear(date,year);
    date = firstWeekday(date);
    return date;
}

function firstDayOfYear(date, year) {
    date.setYear(year);
    date.setDate(1);
    date.setMonth(0);
    date.setHours(0);
    date.setMinutes(0);
    date.setSeconds(0);
    date.setMilliseconds(0);
    return date;
}

function firstWeekday(date) {

    var day = date.getDay(),
        day = (day === 0) ? 7 : day;

    if (day > 3) {

        var remaining = 8 - day,
            target    = remaining + 1;

        date.setDate(target);
    }

    return date;
}

function monthDiff(d1, d2) {
    var months;
    months = (d2.getFullYear() - d1.getFullYear()) * 12;
    months -= d1.getMonth() + 1;
    months += d2.getMonth();
    return months <= 0 ? 0 : months;
}

function persistKeyValue(element)
{
	var $el = $(element),
		key = $el.data('key'),
		value = $el.data('value'),
		url = $el.data('store');
	
	$.post(url, {
		'key': key, 
		'value': value
	});
}

(function( $ ) {
	
	function hideOverlays(e){
		for(var i = 0; i < wanawork.blocksToClose.length; i++) {
			
			var element = $(wanawork.blocksToClose[i]);
			var target = $(e.target);
			
			if(element[0] !== target[0] && element.has(target).length === 0) {
				element.hide();
				delete wanawork.blocksToClose[i];
			}
		}
	}
	$(document).click(hideOverlays);
	
	$('#login-link').click(function(e){
		e.preventDefault();
		e.stopPropagation();
		
		var blockToShow = $('#loginHolder');
		
		if(blockToShow.is(":visible")) {
			blockToShow.slideUp('fast');
		} else {
			hideOverlays(e);
			blockToShow.slideDown();
			wanawork.blocksToClose.push(blockToShow);
		}
	});
	
	$('#headerBell').click(function(e){
		e.preventDefault();
		e.stopPropagation();
		
		var blockToShow = $('#cvRequestHolder');
		
		if(blockToShow.is(":visible")) {
			blockToShow.slideUp('fast');
		} else {
			console.log(this);
			hideOverlays(e);
			$(this).tooltip('hide');
//			blockToShow.position({
//				'at': 'center bottom',
//				'my': 'center top',
//				'of': this
//			});
			blockToShow.slideDown();
			wanawork.blocksToClose.push(blockToShow);
		}
	});
	
//	$('#headerMail').click(function(e){
//		e.preventDefault();
//		e.stopPropagation();
//		
//		var blockToShow = $('#mailHolder');
//		
//		if(blockToShow.is(":visible")) {
//			blockToShow.slideUp('fast');
//		} else {
//			hideOverlays(e);
//			$(this).tooltip('hide');
//			blockToShow.slideDown();
//			wanawork.blocksToClose.push(blockToShow);
//		}
//		
//	});
	
	$('input, textarea').placeholder();
	
	function setupTooltips()
	{
		$(".smallTooltip").tooltip({
			container: 'body'
		});
		$('.withTooltip').popover();
	}
	setupTooltips();
	
	$.timeago.settings.allowFuture = true;
	$(".timeago").timeago();
	$('.trim-line').textOverflow();
	
	setTimeout(function(){
		$('.top-alert').alert('close');
	}, 10000);
	
	
	jQuery.fn.slideLeftHide = function(speed, callback) { 
		  this.animate({ 
		    width: "hide", 
		    paddingLeft: "hide", 
		    paddingRight: "hide", 
		    marginLeft: "hide", 
		    marginRight: "hide" 
		  }, speed, callback);
		};

		jQuery.fn.slideLeftShow = function(speed, callback) { 
		  this.animate({ 
		    width: "show", 
		    paddingLeft: "show", 
		    paddingRight: "show", 
		    marginLeft: "show", 
		    marginRight: "show" 
		  }, speed, callback);
		};
		
    
    $(document).on('chosen:ready', function(e){
    	var $target = $(e.target),
    		targetData = $target.data(),
    		$parent = $target.parent(),
    		$chosenContainer = $parent.find('.chosen-container');
    	
    	if('popover' in targetData && !('popover' in $chosenContainer.data())) {
    		var popover = targetData['popover'];
    		var options = targetData['popover'].options;
    		options.trigger = 'hover';
    		options.container = 'body';
    		if($chosenContainer.hasClass('chosen-container-multi')) {
    			$chosenContainer.find('.chosen-choices').popover(options);
    		} else {
    			$chosenContainer.find('.chosen-single').popover(options);
    		}
    		
    	}
    });
    
    setTimeout(function(){
    	$('.top-message').each(function(){
        	$(this).slideUp(400, function(){
        		$(this).remove();
        	});
        });
    }, 5000);
    
    $('#agree-to-cookies').on('click', function(e){
    	e.preventDefault();
    	$('#cookie-panel').remove();
    	$.cookie('cookie_policy_agreed', '1', { 
    		expires: 365
        });
    });
    
})( jQuery );